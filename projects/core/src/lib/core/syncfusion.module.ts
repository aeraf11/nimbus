import { NgModule } from '@angular/core';
import {
  AccumulationChartModule,
  AccumulationDataLabelService,
  AccumulationLegendService,
  AccumulationTooltipService,
  BarSeriesService,
  CategoryService,
  ChartAllModule,
  ChartModule,
  ColumnSeriesService,
  PieSeriesService,
  StackingBarSeriesService,
} from '@syncfusion/ej2-angular-charts';

import { RichTextEditorModule } from '@syncfusion/ej2-angular-richtexteditor';
import { DocumentEditorContainerModule, DocumentEditorModule } from '@syncfusion/ej2-angular-documenteditor';
import { ProgressBarAllModule } from '@syncfusion/ej2-angular-progressbar';
import { CircularGaugeModule } from '@syncfusion/ej2-angular-circulargauge';
import { UploaderModule } from '@syncfusion/ej2-angular-inputs';

@NgModule({
  exports: [
    ChartModule,
    AccumulationChartModule,
    ChartAllModule,
    UploaderModule,
    CircularGaugeModule,
    RichTextEditorModule,
    ProgressBarAllModule,
    DocumentEditorModule,
    DocumentEditorContainerModule,
  ],
  providers: [
    PieSeriesService,
    AccumulationDataLabelService,
    AccumulationLegendService,
    AccumulationTooltipService,
    CategoryService,
    ColumnSeriesService,
    BarSeriesService,
    StackingBarSeriesService,
  ],
})
export class SyncfusionModule {}
