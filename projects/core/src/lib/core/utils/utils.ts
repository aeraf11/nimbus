import { Observable, of } from 'rxjs';
import { FormDefinition, MetaDataCollection, MetaDataItem } from '../models';

export function isObject(x: unknown) {
  return x && typeof x === 'object' && !Array.isArray(x);
}

export function defineHiddenProp(field: any, prop: string, defaultValue: any) {
  Object.defineProperty(field, prop, { enumerable: false, writable: true, configurable: true });
  field[prop] = defaultValue;
}

interface IObserveTarget<T> {
  [prop: string]: any;
  _observers?: {
    [prop: string]: {
      value: T;
      onChange: IObserveFn<T>[];
    };
  };
}

type IObserveFn<T> = (change: { currentValue: T; previousValue?: T; firstChange: boolean }) => void;

export interface IObserver<T> {
  setValue: (value: T) => void;
  unsubscribe: () => void;
}

export function observe<T = any>(
  o: IObserveTarget<T>,
  paths: string[],
  setFn: IObserveFn<T>
): IObserver<T> | undefined {
  if (!o._observers) {
    defineHiddenProp(o, '_observers', {});
  }
  if (o._observers) {
    let target = o;
    for (let i = 0; i < paths.length - 1; i++) {
      if (!target[paths[i]] || !isObject(target[paths[i]])) {
        target[paths[i]] = /^\d+$/.test(paths[i + 1]) ? [] : {};
      }
      target = target[paths[i]];
    }

    const key = paths[paths.length - 1];
    const prop = paths.join('.');
    if (!o._observers[prop]) {
      o._observers[prop] = { value: target[key], onChange: [] };
    }

    const state = o._observers[prop];
    if (state.onChange?.indexOf(setFn) === -1) {
      state.onChange.push(setFn);
      setFn({ currentValue: state.value, firstChange: true });
      if (state.onChange.length === 1) {
        const { enumerable } = Object.getOwnPropertyDescriptor(target, key) || { enumerable: true };
        Object.defineProperty(target, key, {
          enumerable,
          configurable: true,
          get: () => state.value,
          set: (currentValue) => {
            if (currentValue !== state.value) {
              const previousValue = state.value;
              state.value = currentValue;
              state.onChange.forEach((changeFn) => changeFn({ previousValue, currentValue, firstChange: false }));
            }
          },
        });
      }
    }

    return {
      setValue(value: T) {
        state.value = value;
      },
      unsubscribe() {
        if (state.onChange) state.onChange = state.onChange.filter((changeFn) => changeFn !== setFn);
      },
    };
  } else return undefined;
}

export const getInitials = (name: string): string | undefined => {
  if (!name) return undefined;
  const parts = name.split(' ');
  const initials = parts.map((part) => part[0]?.toUpperCase());
  return initials.join('');
};

export const getWidthFromString = (strWidth: string, totalWidth: number): number => {
  if (strWidth.includes('%')) {
    const widthPercent = +strWidth.replace('%', '');
    return (totalWidth * widthPercent) / 100;
  } else if (strWidth.includes('px')) {
    return +strWidth.replace('px', '');
  } else {
    return +strWidth;
  }
};

export const mergeDeep = (target: any, ...sources: any[]): any => {
  if (!sources.length) {
    return target;
  }
  const source = sources.shift();

  if (isObject(target) && isObject(source)) {
    for (const key in source) {
      if (isObject(source[key])) {
        if (!target[key]) Object.assign(target, { [key]: {} });
        mergeDeep(target[key], source[key]);
      } else {
        Object.assign(target, { [key]: source[key] });
      }
    }
  }

  return mergeDeep(target, ...sources);
};

export const autopopulateModel = (
  currentModel: any,
  fetchedModel: any,
  props: { autoFillModelSkipProps: string[]; autoFillModelOverrides: string[] }
): any => {
  const skipProps = props.autoFillModelSkipProps;

  const modelPreseved = JSON.parse(JSON.stringify(currentModel));
  const tempModel = JSON.parse(JSON.stringify(currentModel));
  const newModel = mergeDeep(tempModel, fetchedModel);
  for (const prop in newModel) {
    if (!modelPreseved.hasOwnProperty(prop)) {
      delete newModel[prop];
    }
  }

  if (skipProps) {
    skipModelProps(skipProps, modelPreseved, newModel);
  }

  if (props.autoFillModelOverrides) {
    props.autoFillModelOverrides.forEach((element: any) => {
      if (element) {
        let tempmodel: any = null;
        element.path.forEach((prop: any) => {
          if (newModel[prop] || tempmodel[prop]) {
            if (!tempmodel) {
              tempmodel = newModel[prop];
            } else {
              tempmodel = tempmodel[prop];
            }
          }
        });
        if (tempmodel && element.property) {
          element.property.forEach((property: any) => {
            for (const key in property) {
              if (Array.isArray(tempmodel)) {
                tempmodel.forEach((arrItem: any) => {
                  arrItem[key] = property[key];
                });
              } else {
                tempmodel[key] = property[key];
              }
            }
          });
        }
      }
    });
  }

  return newModel;
};

function skipModelProps(skipProps: string[], currentModel: any, newModel: any) {
  skipProps.forEach((prop) => {
    const props = prop.split('.');
    skipPropsRecursive(props, currentModel, newModel);
  });
}

function skipPropsRecursive(props: string[], currentModel: any, newModel: any) {
  if (props.length === 1) {
    newModel[props[0]] = currentModel[props[0]];
  } else if (props.length > 0) {
    const levelProp = props.shift();
    const levelCurrentModel = currentModel[levelProp + ''];
    const levelNewModel = newModel[levelProp + ''];
    if (levelCurrentModel && !levelNewModel) {
      newModel[levelProp + ''] = {};
    }
    if (levelCurrentModel) {
      skipPropsRecursive(props, levelCurrentModel, levelNewModel);
    }
  }
}

export const formsSectionLoader = (forms: FormDefinition[]): FormDefinition[] => {
  if (forms) {
    forms.forEach((form: FormDefinition) => {
      traverseAndReplaceSectionTypes(form, 'sectionType', forms);
    });
  }

  return forms;
};

export const traverseAndReplaceSectionTypes = (jsonObj: any, nodeId: string, forms: FormDefinition[]): any => {
  if (jsonObj !== null && typeof jsonObj == 'object') {
    Object.entries(jsonObj).forEach(([key, value]) => {
      if (key === nodeId) {
        const form = forms.find((form) => form.id === value);

        if (form && form.fields) {
          jsonObj['fieldGroup'] = form?.fields;
        }
        return jsonObj;
      }
      return traverseAndReplaceSectionTypes(value, nodeId, forms);
    });
  }
};

export function evalStringExpression<T>(
  expression: string,
  argNames: string[]
): ((...args: unknown[]) => T) | undefined {
  try {
    return Function(...argNames, `return ${expression};`) as (...args: unknown[]) => T;
  } catch (error) {
    console.error(error);
    return undefined;
  }
}

export function evalStringFormExpression<T>(expression: string): ((...args: unknown[]) => T) | undefined {
  return evalStringExpression(expression, ['rootModel', 'model', 'context', 'helper']);
}

export function resizeObservable(elem: HTMLElement): Observable<ResizeObserverEntry[] | undefined> {
  if (!elem) {
    return of();
  }

  return new Observable((subscriber) => {
    const ro = new ResizeObserver((entries) => {
      subscriber.next(entries);
    });

    ro.observe(elem);
    return function unsubscribe() {
      ro.unobserve(elem);
    };
  });
}

export function resolveObjectPath(path: string, obj: unknown, separator = '.') {
  const properties: any[] = Array.isArray(path) ? path : path.split(separator);
  return properties.reduce((prev: any, curr: any) => prev && prev[curr], obj);
}

export const setDeepPropertyByPath = (path: string, obj: any, value: any): any => {
  const pathList = path.split('.');
  const key = pathList.pop();

  const pointer: any = pathList.reduce((accumulator, currentValue) => {
    if (accumulator[currentValue] === undefined) {
      accumulator[currentValue] = {};
    }
    return accumulator[currentValue];
  }, obj);

  if (key) {
    pointer[key] = value;
  }

  return obj;
};

export const flattenObject = (obj: any) => {
  const flattened: any = {};

  Object.keys(obj).forEach((key) => {
    const value = obj[key];

    if (typeof value === 'object' && value !== null && !Array.isArray(value)) {
      Object.assign(flattened, flattenObject(value));
    } else {
      flattened[key] = value;
    }
  });

  return flattened;
};

export const toCamel = (obj: any) => {
  let camelObject: any;
  let originalKey;
  let newKey;
  let value;

  if (obj instanceof Array) {
    return obj.map((value) => {
      if (typeof value === 'object') {
        value = toCamel(value);
      }
      return value;
    });
  } else {
    camelObject = {};
    for (originalKey in obj) {
      if (obj.hasOwnProperty(originalKey)) {
        newKey = (originalKey.charAt(0).toLowerCase() + originalKey.slice(1) || originalKey).toString();
        value = obj[originalKey];
        if (value instanceof Array || (value !== null && value.constructor === Object)) {
          value = toCamel(value);
        }
        camelObject[newKey] = value;
      }
    }
  }
  return camelObject;
};

export const stringToCamel = (str: string): string => {
  if (!str) {
    return '';
  }
  return str.toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, (m, chr) => chr.toUpperCase());
};

export function keysToUpperCase(val: object) {
  const result = Object.fromEntries(
    Object.entries(val).map((e) => [[e[0].charAt(0).toUpperCase() + e[0].slice(1)], e[1]])
  );
  return result;
}

export function keysToLowerCase(val: object) {
  const result = Object.fromEntries(
    Object.entries(val).map((e) => [[e[0].charAt(0).toLowerCase() + e[0].slice(1)], e[1]])
  );
  return result;
}

export function parseMetaJson(metaJson: string | undefined): MetaDataItem[] {
  if (!metaJson) {
    return [];
  }

  try {
    const metaObject: MetaDataCollection = JSON.parse(metaJson);
    if (metaObject) {
      // MetaJson always seems to be 1 level down in an Items property so we return that
      // if it is present, otherwise we assume that the metaObject is the array itself
      return metaObject.Items ? metaObject.Items : (metaObject as MetaDataItem[]);
    }
    return [];
  } catch {
    throw new Error('Could not parse MetaData JSON object');
  }
}
