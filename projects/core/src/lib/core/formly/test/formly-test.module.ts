import { TestModuleMetadata } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ConfigOption, FormlyModule } from '@ngx-formly/core';
import { FormlySelectModule } from '@ngx-formly/core/select';
import { FormlyTestComponent } from './formly-test.component';

export interface FormlyTestModuleConfig {
  config: ConfigOption | undefined;
  declarations?: unknown[];
  imports?: unknown[];
  providers?: unknown[];
}

export const buildFormlyTestModule = (config: FormlyTestModuleConfig): TestModuleMetadata => {
  if (!config) {
    throw new Error('Building a formly test module required a config');
  }

  return {
    declarations: [...(config.declarations ?? []), FormlyTestComponent],
    imports: [
      ...(config.imports ?? []),
      ReactiveFormsModule,
      NoopAnimationsModule,
      FormlySelectModule,
      FormlyModule.forRoot(config.config),
    ],
    providers: [...(config.providers ?? [])],
  };
};
