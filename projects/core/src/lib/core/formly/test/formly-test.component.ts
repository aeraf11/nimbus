import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';

@Component({
  selector: 'ni-formly-test',
  template: `<formly-form [form]="form" [fields]="fields" [model]="model" [options]="options"></formly-form>`,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormlyTestComponent {
  form = new UntypedFormGroup({});
  fields: FormlyFieldConfig[] = [{ options: {} }];
  model: any;
  options: FormlyFormOptions = {};
}
