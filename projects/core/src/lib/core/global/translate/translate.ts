﻿export function translateInit() {
  globalThis.$translate = (stringParts: TemplateStringsArray, ...expressions: any[]): string => {
    const searchString = '::';
    const translationExpressionRegex = /{(\d)}/g;
    let contextKey = '';
    let token = '';

    if (stringParts.length) {
      if (expressions.length) {
        const tokenParts: string[] = stringParts.map((item, index) => {
          const parts: string[] = item.split('::');

          if (index === 0 && item.includes(searchString)) {
            contextKey = parts[0];
          }

          return parts.length > 1 ? parts[1] : item;
        });

        tokenParts.forEach((stringPart: string, index: number): void => {
          if (!expressions[index]) {
            token += stringPart;
          } else {
            token += stringPart + `{${index}}`;
          }
        });
      } else if (stringParts[0].includes(searchString)) {
        const parts: string[] = stringParts[0].split('::');

        contextKey = parts[0];
        token = parts[1];
      } else {
        token = stringParts[0];
      }
    }

    if (token) {
      const translation: string = $getTranslation(contextKey, token);

      return translation.replace(translationExpressionRegex, (match: string, captureGroup1: string): string => {
        return expressions[parseInt(captureGroup1)];
      });
    } else {
      return '';
    }
  };
}
