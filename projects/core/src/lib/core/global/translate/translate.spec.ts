import { TestBed } from '@angular/core/testing';
import { MockStateService, StateService, Translations, TranslationService } from '@nimbus/core/src/lib/core';

beforeEach(() => {
  TestBed.configureTestingModule({
    providers: [{ provide: StateService, useClass: MockStateService }],
  });
});

describe('$translate', () => {
  const mockTranslationState: Translations = {
    _root: {
      'Oliver is logged in as {0} with {1} rights': 'Luke is signed in as {0} with {1} rights',
    },
    operation: {
      'Oliver has logged {0} items': 'Luke has logged {0} articles',
    },
  };

  beforeEach(() => {
    const translationService: TranslationService = TestBed.inject(TranslationService);
    translationService.translationState = mockTranslationState;
  });

  it('should be defined', () => {
    expect($translate).toBeDefined();
  });

  it('should translate the string and expressions without a context passed in', () => {
    const userName = 'tester1';
    const adminRights = 'full admin';

    const result: string = $translate`Oliver is logged in as ${userName} with ${adminRights} rights`;

    expect(result).toEqual('Luke is signed in as tester1 with full admin rights');
  });

  it('should translate the contextKey, string and expression passed in', () => {
    const itemsLoggedCount = 10;

    const result: string = $translate`operation::Oliver has logged ${itemsLoggedCount} items`;

    expect(result).toEqual('Luke has logged 10 articles');
  });

  describe('Performance Test', () => {
    it('should measure the execution time of $translate when called 1000 times', () => {
      const startTime = Date.now();
      const userName = 'tester1';
      const adminRights = 'full admin';
      const itemsLoggedCount = 10;
      let result = '';

      for (let index = 0; index < 1000; index++) {
        if (index % 2 === 0) {
          result = $translate`Oliver is logged in as ${userName} with ${adminRights} rights`;
        } else {
          result = $translate`operation::Oliver has logged ${itemsLoggedCount} items`;
        }
      }

      const endTime = Date.now();
      const executionTime = endTime - startTime;

      console.log(`Execution time: ${executionTime} ms`);
      expect(result).toEqual(`Luke has logged ${itemsLoggedCount} articles`);
      expect(executionTime).toBeLessThan(1000);
    });
  });
});
