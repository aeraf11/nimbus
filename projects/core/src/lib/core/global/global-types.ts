﻿declare global {
  function $translate(stringParts: TemplateStringsArray, ...expressions: any[]): string;
  function $getTranslation(contextKey: string, token: string): string;
}

export {};
