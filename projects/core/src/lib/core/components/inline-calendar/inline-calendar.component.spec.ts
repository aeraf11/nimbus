import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Platform } from '@angular/cdk/platform';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DateAdapter, MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { DateTimeAdapter, MatDatepickerModule } from '@nimbus/material';
import { AppDateTimeAdapter, StateService } from '../../services';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { InlineCalendarComponent } from './inline-calendar.component';

describe('InlineCalendarComponent', () => {
  let fixture: ComponentFixture<InlineCalendarComponent>;
  let component: InlineCalendarComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatDatepickerModule, MatNativeDateModule, NoopAnimationsModule],
      providers: [
        {
          provide: StateService,
          useClass: MockStateService,
        },
        {
          provide: DateTimeAdapter,
          useClass: AppDateTimeAdapter,
          deps: [MAT_DATE_LOCALE, Platform, StateService],
        },
        {
          provide: DateAdapter,
          useClass: AppDateTimeAdapter,
          deps: [MAT_DATE_LOCALE, Platform, StateService],
        },
        {
          provide: AppDateTimeAdapter,
          useClass: AppDateTimeAdapter,
          deps: [MAT_DATE_LOCALE, Platform, StateService],
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
