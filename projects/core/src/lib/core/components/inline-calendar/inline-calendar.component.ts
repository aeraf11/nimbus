import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  NgZone,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { DateTime, MatCalendarCellCssClasses, MatCalendar } from '@nimbus/material';
import { untilDestroyed } from '@ngneat/until-destroy';
import { AppDateTimeAdapter, utils } from '@nimbus/core/src/lib/core';
import { debounceTime } from 'rxjs';
import { UntilDestroy } from '@ngneat/until-destroy';
@UntilDestroy()
@Component({
  selector: 'ni-inline-calendar',
  templateUrl: './inline-calendar.component.html',
  styleUrls: ['./inline-calendar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class InlineCalendarComponent implements OnInit, AfterViewInit {
  @Input() todayFormat: string | undefined = 'dd MMMM yyyy';
  @Input() activeFormat: string | undefined = 'MMMM yyyy';
  @Input() isDynamicHeight: boolean | undefined = false;
  @Input() width: string | undefined;
  @Input() showTitle: boolean | undefined;
  @Input() subtitleFontSize: string | undefined;
  @Input() subtitleLineHeight: boolean | undefined;
  @Input() titleFontSize: string | undefined;
  @Input() titleLineHeight: string | undefined;
  @Input() activeMonth: any;
  @Input() wrapperMaxWidth = '500px';
  @Input() calculating = true;
  @Input() activeMonthInit = false;
  @Input() formControl: any;
  @Input() change: any;
  today: any;
  @ViewChild('position') private position?: ElementRef;
  @ViewChild('calendar') private calendar?: MatCalendar<DateTime>;
  constructor(private zone: NgZone, private appDateTimeAdapter: AppDateTimeAdapter) {}
  ngAfterViewInit(): void {
    if (this.isDynamicHeight) {
      utils
        .resizeObservable(this.position?.nativeElement)
        .pipe(debounceTime(500), untilDestroyed(this))
        .subscribe(() => {
          this.zone.run(() => this.calculateCalendarSize());
          if (!this.activeMonthInit) {
            this.zone.run(() => this.setupActiveMonth());
          }
          this.calculating = false;
          this.change.markForCheck();
        });
    }
  }

  private calculateCalendarSize(): void {
    this.calculating = false;
    this.change.markForCheck();
  }

  private setupActiveMonth(): void {
    this.activeMonth = this.calendar?.activeDate?.dateTime;

    this.calendar?.stateChanges.pipe(untilDestroyed(this)).subscribe(() => {
      this.activeMonth = this.calendar?.activeDate?.dateTime;
    });
    this.change.markForCheck();
    this.activeMonthInit = true;
  }

  ngOnInit(): void {
    this.today = new Date();
  }

  dateClass(): any {
    return (date: DateTime): MatCalendarCellCssClasses => {
      if (date.dateTime) {
        const taskOnDate = this.formControl?.value?.table1?.results?.filter((task: any) => {
          const taskDateTime = {
            dateTime: task.requiredCompletionDate ?? task.end ?? task.creationDate,
            timeZoneId: null,
          };
          return (
            this.appDateTimeAdapter.formatDateTime(taskDateTime, false, false) ===
            this.appDateTimeAdapter.formatDateTime(date, false, false)
          );
        })?.[0];

        if (taskOnDate) {
          if (this.isTaskOverdue(taskOnDate)) {
            return 'task-date overdue past';
          } else {
            if (this.compareDateToToday(date) < 0) {
              return 'task-date past';
            } else {
              return 'task-date';
            }
          }
        }

        if (this.compareDateToToday(date) < 0) {
          return 'past';
        }
      }
      return '';
    };
  }

  private compareDateToToday(date: DateTime): number {
    return this.appDateTimeAdapter.compareDate(date, this.appDateTimeAdapter.today());
  }

  private isTaskOverdue(task: any): boolean {
    if (!task) {
      return false;
    }

    // It does not require completion
    if (!task.requiresCompletion) {
      return false;
    }

    // It has a completion date so is no longer overdue
    if (task.actualCompletionDate) {
      return false;
    }

    // Return true if required completion date is before today
    if (task.requiredCompletionDate) {
      return (
        this.compareDateToToday({
          dateTime: task.requiredCompletionDate,
          timeZoneId: null,
        }) < 0
      );
    }

    return false;
  }

  onDateClick(date: DateTime | null): void {
    if (date) {
      const taskId = this.getTaskFromDate(date)?.entityCalendarItemId;

      if (taskId) {
        alert('Navigate to internal page - Task with id: ' + taskId);
      }
    }
  }

  private getTaskFromDate(date: DateTime | null): any | null {
    if (!date) {
      return null;
    }

    this.formControl?.value?.table1?.results?.filter((task: any) => {
      const taskDateFormatted = this.appDateTimeAdapter.formatDateTime(
        { dateTime: task.requiredCompletionDate ?? task.end ?? task.creationDate, timeZoneId: null },
        false,
        true
      );
      const selectedDateFormatted = this.appDateTimeAdapter.formatDateTime(date, false, true);
      return taskDateFormatted === selectedDateFormatted;
    })[0] ?? null;
  }
}
