import { UntypedFormControl } from '@angular/forms';
import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { LookupNode } from '@nimbus/core/src/lib/core';
import { OnChange } from 'property-watch-decorator';

@Component({
  selector: 'ni-lookup-level',
  templateUrl: './lookup-level.component.html',
  styleUrls: ['./lookup-level.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LookupLevelComponent {
  selectedLookup: LookupNode | null = null;
  selectedIdControl = new UntypedFormControl();
  levelId!: string;

  @OnChange<string>(function (this: LookupLevelComponent, value, change) {
    this.setSelectedLookup();
  })
  @Input()
  lookups!: LookupNode[] | null;

  @OnChange<string>(function (this: LookupLevelComponent, value, change) {
    this.setSelectedLookup();
  })
  @Input()
  selectedIds: string[] | null = null;

  @OnChange<string>(function (this: LookupLevelComponent, value, change) {
    this.setSelectedLookup();
  })
  @Input()
  level = 0;

  @Input() addDefaultOption = true;

  @Output() itemSelected = new EventEmitter<{ id: string; level: number }>();
  @Output() touched = new EventEmitter<null>();

  _onItemSelected(id: string, level: number) {
    this.onItemSelected({ id, level });
  }

  onItemSelected(event: { id: string; level: number }) {
    if (!event.id && event.level > this.level) {
      this.itemSelected.emit({ id: this.levelId, level: event.level });
    } else {
      this.itemSelected.emit({ id: event.id, level: event.level });
    }

    if (event.level === this.level)
      this.selectedLookup = this.lookups?.find((l) => l.item.lookupId === event.id) || null;
  }

  onClosed() {
    this.touched.emit();
  }

  setSelectedLookup() {
    if (this.lookups && this.selectedIds) {
      this.levelId = this.selectedIds[this.level];
      this.selectedLookup = this.lookups.find((l) => l.item.lookupId === this.levelId) || null;
      this.selectedIdControl.setValue(this.selectedLookup?.item.lookupId);
    }
  }
}
