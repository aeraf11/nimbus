import { ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { LookupLevelComponent } from './lookup-level.component';

describe('LookupLevelComponent', () => {
  let component: LookupLevelComponent;
  let fixture: ComponentFixture<LookupLevelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LookupLevelComponent],
      imports: [MatFormFieldModule, MatInputModule, MatSelectModule, NoopAnimationsModule, ReactiveFormsModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LookupLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
