import { ComponentFixture, TestBed } from '@angular/core/testing';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { QuickSearchWrapperComponent } from './quick-search-wrapper.component';
import { FormlyTestComponent } from '../../formly/test/formly-test.component';

describe('QuickSearchWrapperComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          wrappers: [
            {
              name: 'quicksearch',
              component: QuickSearchWrapperComponent,
            },
          ],
        },
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'quicksearch',
        wrappers: ['quicksearch'],
        fieldGroup: [
          {
            template: 'test',
          },
        ],
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
