import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FieldWrapper } from '@ngx-formly/core';
import {
  Component,
  ChangeDetectionStrategy,
  AfterViewInit,
  ViewChild,
  ElementRef,
  EventEmitter,
  Output,
} from '@angular/core';
import { debounceTime, distinctUntilChanged, fromEvent, tap } from 'rxjs';
import { DialogFilterAction, DialogFilterResult, FilterColumn } from '@nimbus/core/src/lib/core';
import { Router } from '@angular/router';

@UntilDestroy()
@Component({
  selector: 'ni-quick-search-wrapper',
  templateUrl: './quick-search-wrapper.component.html',
  styleUrls: ['./quick-search-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuickSearchWrapperComponent extends FieldWrapper implements AfterViewInit {
  public defaults = {
    width: '100%',
  };

  @ViewChild('input') input?: ElementRef;
  @Output() filterRemoved = new EventEmitter<DialogFilterResult>();

  quickSearchChange = new EventEmitter<DialogFilterResult>();
  hasSearched = false;
  hasFilters = false;
  showFilters: FilterColumn[] = [];
  allFilters: FilterColumn[] = [];
  defaultFilters: FilterColumn[] = [];
  constructor(private router: Router) {
    super();
  }

  ngAfterViewInit(): void {
    if (this.props['autoFocus']) {
      this.input?.nativeElement?.focus();
    }

    if (this.input) {
      fromEvent(this.input.nativeElement, 'keyup')
        .pipe(
          untilDestroyed(this),
          debounceTime(600),
          distinctUntilChanged(),
          tap(() => {
            this.quickSearchChange.emit({
              action: DialogFilterAction.Filter,
              quickSearch: this.input?.nativeElement.value ?? '',
            });
            if (!this.hasSearched) {
              this.hasSearched = true;
            }
          })
        )
        .subscribe();
    }
  }

  nav(url: string) {
    this.router.navigateByUrl(url);
  }

  setCurrentFilters(dialogFilters: DialogFilterResult): void {
    if (this.defaultFilters.length === 0) {
      this.defaultFilters = dialogFilters?.filterColumns?.filter((f) => f.defaultValue !== undefined) ?? [];
    }
    const filters = dialogFilters?.filterColumns ?? [];
    this.allFilters = filters;
    this.hasFilters = filters?.length > 0;

    this.showFilters = filters.filter((f) => f.userCreated && f.defaultValue !== f.stringFilter);
  }

  getFilterText(filter: FilterColumn) {
    const col = this.showFilters.find((col: FilterColumn) => filter.column === col.column);
    if (!col) {
      return;
    }
    let display = `: ${col?.displayValue}`;
    if (col?.rawFilter === 'true' || col?.rawFilter === 'false') {
      display = '';
    }
    const columnAlias = `${col?.columnAlias !== 'UnitName' ? col?.columnAlias : 'Division'}`;

    return `${
      col?.columnAlias
        ? columnAlias
        : col?.column
            .split(' ')
            .map((column) => column[0].toUpperCase() + column.substring(1))
            .join(' ')
    }${display}`;
  }

  removeFilter(filter: FilterColumn) {
    const defaultFilter = this.defaultFilters.find((f) => f.column === filter.column);
    let filters = this.allFilters?.filter((f) => f.column !== filter.column);
    if (defaultFilter) {
      filters = filters.concat([
        { ...defaultFilter, stringFilter: defaultFilter.defaultValue, displayValue: defaultFilter.defaultValue },
      ]);
    }

    this.filterRemoved.emit({
      action: DialogFilterAction.Filter,
      quickSearch: this.input?.nativeElement.value ?? '',
      filterColumns: filters,
    });
  }
}
