import { Platform } from '@angular/cdk/platform';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { DateAdapter, MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AppDateTimeAdapter, StateService } from '@nimbus/core/src/lib/core';
import { DateTimeAdapter, MatDatepickerModule } from '@nimbus/material';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { DateRangePickerComponent } from './date-range-picker.component';

describe('DateRangePickerComponent', () => {
  let component: DateRangePickerComponent;
  let fixture: ComponentFixture<DateRangePickerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DateRangePickerComponent],
      imports: [
        MatFormFieldModule,
        MatInputModule,
        NoopAnimationsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        ReactiveFormsModule,
      ],
      providers: [
        {
          provide: StateService,
          useClass: MockStateService,
        },
        {
          provide: DateTimeAdapter,
          useClass: AppDateTimeAdapter,
          deps: [MAT_DATE_LOCALE, Platform, StateService],
        },
        {
          provide: DateAdapter,
          useClass: AppDateTimeAdapter,
          deps: [MAT_DATE_LOCALE, Platform, StateService],
        },
        {
          provide: AppDateTimeAdapter,
          useClass: AppDateTimeAdapter,
          deps: [MAT_DATE_LOCALE, Platform, StateService],
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateRangePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
