import { LiveAnnouncer } from '@angular/cdk/a11y';
import { ChangeDetectionStrategy, Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, UntypedFormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Component({
  selector: 'ni-date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styleUrls: ['./date-range-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DateRangePickerComponent),
      multi: true,
    },
  ],
})
export class DateRangePickerComponent implements ControlValueAccessor, OnInit {
  @Input() label = 'Date Range';
  @Input() required = false;
  @Input() hint!: string;
  @Input() field!: FormlyFieldConfig;
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() color = 'primary';
  @Input() showQuickLinks = false;
  @Input() showQuickLinkScope = true;
  @Input() readonly = false;
  @Input() ariaLabel?: string;

  disabled = false;
  value: any;
  startDate = 'Start date';
  endDate = 'End date';
  radioAnnounceText = ' Radio button for within range. Radio button for outside range';
  start = new UntypedFormControl();
  end = new UntypedFormControl();
  negative = new UntypedFormControl();
  model!: any;
  private onChange!: (value: any) => void;
  private onTouched!: () => void;

  constructor(private liveAnnouncer: LiveAnnouncer) {}

  ngOnInit(): void {
    this.liveAnnouncer.announce(
      `Input a ${this.startDate} and a ${this.endDate}. Date picker toggle icon. ${
        this.showQuickLinks ? this.radioAnnounceText : ''
      }`
    );
  }

  onDateSelection() {
    this.onTouched();
    this.updateModel();
    this.onChange(this.model);
  }

  updateModel() {
    this.model = {
      start: this.start.value,
      end: this.end.value,
      negative: this.negative.value,
    };
  }

  writeValue(value: any): void {
    this.model = value;
    this.start.setValue(value?.start);
    this.end.setValue(value?.end);
    this.negative.setValue(value?.negative ?? false);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  changeNegative(event: any): void {
    const value = {
      ...this.model,
      ...{ negative: !event.value },
    };
    this.writeValue(value);
    this.onDateSelection();
  }
}
