import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  DialogFilterAction,
  DialogFilterComponent,
  DialogFilterConfig,
  DialogFilterData,
  DialogFilterResult,
  FilterColumn,
} from '@nimbus/core/src/lib/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, tap } from 'rxjs/operators';

@UntilDestroy()
@Component({
  selector: 'ni-filter-bar',
  templateUrl: './filter-bar.component.html',
  styleUrls: ['./filter-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class FilterBarComponent implements AfterViewInit {
  @ViewChild('input') input!: ElementRef;
  @HostBinding('class.filter-bar') hostClass = true;

  @Input() quickSearch?: string;
  @Input() filterText = 'Filter';
  @Input() filterTitle = 'Filter Results';
  @Input() cancelText = 'Cancel';
  @Input() clearText = 'Clear';
  @Input() filterDisabled = false;
  @Input() dialogWidth? = undefined;
  @Input() showQuickSearch = false;
  @Input() listName = '';

  private _lastFilters: Array<FilterColumn> = [];
  @Input()
  public get lastFilters(): Array<FilterColumn> {
    return this._lastFilters;
  }

  public set lastFilters(value: Array<FilterColumn>) {
    this.userFilters = value.filter((filter) => filter.userCreated);
    this._lastFilters = value;
  }

  @Input() set columnDefinitions(val: any) {
    this.filterableCols = val.filter((col: any) => col.filterParams || col.filterType);
  }

  @Output() filterChange = new EventEmitter<DialogFilterResult>();

  filterableCols: any;
  loadingMatches = false;
  userFilters: any;
  disabled = false;

  constructor(public dialog: MatDialog) {}

  ngAfterViewInit(): void {
    if (this.input) {
      fromEvent<KeyboardEvent>(this.input.nativeElement, 'keyup')
        .pipe(
          debounceTime(600),
          // filter out Tab and Shift + Tab as we don't want to trigger a filter change on keyboard navigation to control
          filter((key: KeyboardEvent) => key.key !== 'Tab' && key.key !== 'Shift'),
          distinctUntilChanged(),
          tap(() => {
            const result = {
              action: DialogFilterAction.Filter,
              filterColumns: this.lastFilters,
              quickSearch: this.input?.nativeElement.value ?? '',
            };
            this.filterChange.emit(result);
          }),
          untilDestroyed(this)
        )
        .subscribe();
    }
  }

  openDialog(): void {
    const { filterableCols } = this;
    if (filterableCols) {
      const config: DialogFilterConfig = {
        filterText: this.filterText,
        cancelText: this.cancelText,
        clearText: this.clearText,
        title: this.filterTitle,
        columnDefinitions: filterableCols,
        lastFilters: this.lastFilters,
      };
      const data: DialogFilterData = {
        config,
        disabled: this.filterDisabled,
      };
      const dialogConfig: MatDialogConfig<DialogFilterData> = { data };
      if (this.dialogWidth) {
        dialogConfig.width = this.dialogWidth;
        dialogConfig.minWidth = '';
        dialogConfig.maxWidth = '';
      }
      const dialogRef = this.dialog.open(DialogFilterComponent, dialogConfig);
      dialogRef.afterClosed().subscribe((result: DialogFilterResult) => {
        if (result.action === DialogFilterAction.Filter || result.action === DialogFilterAction.Clear) {
          result.filterColumns = this.mergeFilters(result.filterColumns ?? []);
          result.quickSearch = this.input?.nativeElement.value ?? '';

          this.filterChange.emit(result);
        }
      });
    }
  }

  mergeFilters(filterResult: FilterColumn[]) {
    let existingFilters = this.lastFilters?.filter((filter) => !filter.userCreated);
    existingFilters = existingFilters.concat(filterResult);
    return existingFilters;
  }

  removeFilter(filter: FilterColumn) {
    this.lastFilters = this.lastFilters?.filter(function (f: FilterColumn) {
      return f.column !== filter.column;
    });
    this.filterChange.emit({
      action: DialogFilterAction.Filter,
      filterColumns: this.lastFilters,
      quickSearch: this.input?.nativeElement.value ?? '',
    });
  }

  getFilterText(filter: FilterColumn) {
    const col = this.filterableCols.find((col: any) => filter.column === col.id);
    if (!col) {
      return;
    }
    return `${col?.value}: ${filter.displayValue}`;
  }
}
