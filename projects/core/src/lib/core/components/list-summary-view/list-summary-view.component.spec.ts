import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { ListService, StateService } from '@nimbus/core/src/lib/core';
import { ListSummaryViewComponent } from './list-summary-view.component';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { MockListService } from '../../services/list.service.spec';

describe('ListSummaryViewComponent', () => {
  let fixture: ComponentFixture<ListSummaryViewComponent>;
  let component: ListSummaryViewComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListSummaryViewComponent],
      imports: [MatDialogModule],
      providers: [
        { provide: StateService, useClass: MockStateService },
        { provide: ListService, useClass: MockListService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListSummaryViewComponent);
    component = fixture.componentInstance;
    component.listName = 'Test';

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
