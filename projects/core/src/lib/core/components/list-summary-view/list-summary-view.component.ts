import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Optional } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {
  CardListWrapperComponent,
  DialogListComponent,
  EntityMediaRequestComponent,
  FormsHelperService,
  IndexedDbService,
  ListFilterComponent,
  ListFilterService,
  ListService,
  QuickSearchWrapperComponent,
  SignalRService,
  StateService,
} from '@nimbus/core/src/lib/core';
import { DataListBaseComponent } from '../data-list-base/data-list-base.component';

@Component({
  selector: 'ni-list-summary-view',
  templateUrl: './list-summary-view.component.html',
  styleUrls: ['./list-summary-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ListSummaryViewComponent),
      multi: true,
    },
  ],
})
export class ListSummaryViewComponent extends DataListBaseComponent implements ControlValueAccessor {
  constructor(
    change: ChangeDetectorRef,
    service: ListService,
    state: StateService,
    public override dialog: MatDialog,
    formsHelper: FormsHelperService,
    @Optional() cardListWrapper: CardListWrapperComponent,
    listFilterService: ListFilterService,
    @Optional() entityMediaRequest: EntityMediaRequestComponent,
    @Optional() listFilter: ListFilterComponent,
    @Optional() quickSearchWrapper: QuickSearchWrapperComponent,
    indexedDbService: IndexedDbService,
    signalR: SignalRService
  ) {
    super(
      service,
      change,
      state,
      formsHelper,
      cardListWrapper,
      listFilterService,
      dialog,
      entityMediaRequest,
      listFilter,
      quickSearchWrapper,
      indexedDbService,
      signalR
    );
  }

  view(): void {
    const config = {
      data: {
        items: this.items,
        listName: this.listName,
        icon: this.icon,
      },
      disableClose: false,
      panelClass: 'no-dialog-padding',
      maxWidth: '900px',
      width: '100%',
    };
    this.dialog.open(DialogListComponent, config);
  }
}
