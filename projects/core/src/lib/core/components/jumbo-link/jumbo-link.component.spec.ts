import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JumboLinkComponent } from './jumbo-link.component';

describe('JumboLinkComponent', () => {
  let component: JumboLinkComponent;
  let fixture: ComponentFixture<JumboLinkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JumboLinkComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JumboLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
