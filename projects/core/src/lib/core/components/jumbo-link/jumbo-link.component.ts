import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { StateService } from '../../services';
import { Router } from '@angular/router';

@Component({
  selector: 'ni-jumbo-link',
  templateUrl: './jumbo-link.component.html',
  styleUrls: ['./jumbo-link.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JumboLinkComponent {
  @Input() blobId: string | undefined;
  @Input() enableBookmark?: string;
  @Input() bookmarkRouteId?: string;
  @Input() defaultIfEmpty?: string;
  @Input() jumboLinkControl?: UntypedFormControl;
  @Input() fill?: boolean;
  @Input() isBookmarkSaved?: boolean;

  constructor(private state: StateService, private router: Router) {}

  navigateToRoute(): void {
    if (this.jumboLinkControl?.value.url) {
      this.router.navigateByUrl(this.jumboLinkControl?.value.url);
    }
  }

  bookmarkLink(): void {
    if (this.enableBookmark && this.bookmarkRouteId && this.jumboLinkControl?.value) {
      this.state.saveBookmark(this.bookmarkRouteId, this.jumboLinkControl.value);
    }
  }
}
