import { LiveAnnouncer } from '@angular/cdk/a11y';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { UntilDestroy } from '@ngneat/until-destroy';
import { DateTime } from '@nimbus/material';
import { AppDateTimeAdapter } from '../../services';

@UntilDestroy()
@Component({
  selector: 'ni-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss'],
})
export class DateComponent implements OnInit {
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() color: ThemePalette;
  @Input() currentDate = '';
  @Input() dateControl = new UntypedFormControl();
  @Input() dateFormat = '';
  @Input() defaultTime = { hours: 0, minutes: 0, seconds: 0 };
  @Input() disableMinutes = true;
  @Input() dateTimeFormat = '';
  @Input() enableFuture: DateTime | boolean | null | undefined;
  @Input() id = '';
  @Input() openOnInputFocus = true;
  @Input() placeholder = '';
  @Input() placeholderType = '';
  @Input() required = false;
  @Input() stepSeconds = 0;
  @Input() stepMinutes = 0;
  @Input() stepHours = 0;
  @Input() showMeridian = true;
  @Input() showSeconds = false;
  @Input() showSpinners = true;
  @Input() showTime = true;
  @Input() showTimeZone = true;
  @Input() timeFormat = '';
  @Input() timeZones = this.dateTimeAdapter.timeZones;
  @Input() touchUi = false;
  @Input() readonly = false;
  @Input() ariaLabel?: string;
  @Output() valueChange = new EventEmitter<string>();

  constructor(
    private change: ChangeDetectorRef,
    public dateTimeAdapter: AppDateTimeAdapter,
    private liveAnnouncer: LiveAnnouncer
  ) {
    this.currentDate = new Date().toISOString();
    this.dateFormat = this.dateTimeAdapter.getDateFormat();
    this.timeFormat = this.dateTimeAdapter.getTimeFormat();
  }

  get enableFutureDate(): DateTime | null {
    return this.enableFuture ? null : this.dateTimeAdapter.today();
  }

  ngOnInit() {
    this.dateTimeFormat = this.showTime ? `${this.dateFormat} ${this.timeFormat}` : this.dateFormat;
    this.placeholderType = this.showTime ? 'date and time' : 'date';
    const currentDateTime: DateTime = { dateTime: this.currentDate, timeZoneId: null };
    const formattedDt = this.dateTimeAdapter.formatDateTime(currentDateTime, this.showTime);
    this.liveAnnouncer.announce(
      `Please input a ${this.placeholderType} for example ${formattedDt}. Date picker toggle icon.`
    );
  }

  onChange() {
    this.valueChange.emit(this.dateControl.value);
    this.change.markForCheck();
    return this.dateControl.value;
  }
}
