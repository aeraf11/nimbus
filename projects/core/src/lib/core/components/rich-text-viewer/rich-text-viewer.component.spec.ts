import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SafeHtmlPipe } from './../../pipes/safe-html.pipe';
import { RichTextViewerComponent } from './rich-text-viewer.component';

xdescribe('RichTextViewerComponent', () => {
  let component: RichTextViewerComponent;
  let fixture: ComponentFixture<RichTextViewerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RichTextViewerComponent, SafeHtmlPipe],
    }).compileComponents();

    fixture = TestBed.createComponent(RichTextViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
