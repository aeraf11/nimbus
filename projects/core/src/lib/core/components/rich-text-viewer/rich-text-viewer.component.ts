import { Component, Input } from '@angular/core';

@Component({
  selector: 'ni-rich-text-viewer',
  templateUrl: './rich-text-viewer.component.html',
  styleUrls: ['./rich-text-viewer.component.scss'],
})
export class RichTextViewerComponent {
  @Input() value = '';
}
