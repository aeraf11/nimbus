import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { DataListColumnDefinition, FilterColumn, ItemsUi } from '../../models';
import { DataCardsComponent } from '../data-cards/data-cards.component';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { OptionsService, StateService } from '../../services';
import { UntypedFormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Component({
  selector: 'ni-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('expandCollapse', [
      state(
        'expand',
        style({
          height: '*',
          padding: '*',
        })
      ),
      state(
        'collapse',
        style({
          height: '0',
          padding: '0',
        })
      ),
      transition('expand => collapse', animate('200ms ease-in-out')),
      transition('collapse => expand', animate('200ms ease-in-out')),
    ]),
  ],
})
export class MessagesComponent implements OnInit {
  @Input() filterColumns?: FilterColumn[];
  @Input() allowAddMessage = false;
  @Input() collapse = false;
  @Input() collapseSaveId: string | undefined;
  @Input() icon: string | undefined;
  @Input() title: string | undefined;
  @Input() control!: UntypedFormControl;
  @Input() columnDefinitions: DataListColumnDefinition[] = [];
  @Input() context: unknown | undefined;
  @Input() contextChanged: Subject<string> | undefined;
  @Input() additionalParams: { [key: string]: string } | undefined;
  @Input() idColumn = '';
  @Input() listName: string | undefined;
  @Input() pageIndex = 0;
  @Input() pageSize = -1;
  @Input() savedOptionsId: string | undefined;
  @Input() selectMode: 'none' | 'single' | 'multi' = 'none';
  @Input() sortColumn: string | undefined;
  @Input() sortDirection = 'asc';
  @Input() field: FormlyFieldConfig | undefined;
  @Input() itemsUi: ItemsUi | undefined;
  @Input() noResultsText = '';
  @Input() maxHeight = '';
  @Input() fullWidthContent = false;
  @Input() cardWrapper = false;
  @Input() refreshInterval = 0;
  @Input() refreshSignalR: { hub: string; event: string; group: string } | undefined;
  @Input() showPaging = true;
  @Output() toggleMessageBox = new EventEmitter<boolean>();
  @Output() sendComplete = new EventEmitter<null>();

  @ViewChild('messageslist') messagesListComponent!: DataCardsComponent;

  messageBoxActive = false;
  itemCount = 0;
  isCardCollapsed = false;
  animationState = 'expand';

  constructor(private state: StateService, private service: OptionsService) {}

  ngOnInit(): void {
    this.loadInitialCollapseState();
  }

  onToggleMessageBox(isActive: boolean): void {
    this.messageBoxActive = isActive;
  }

  onSendComplete() {
    this.messagesListComponent.getItems();
  }

  onItemsUpdate(items: { items: any[]; count: number }): void {
    this.itemCount = items.count;
  }

  setIsCardCollapsed(): void {
    this.isCardCollapsed = !this.isCardCollapsed;
    this.animationState = this.isCardCollapsed ? 'collapse' : 'expand';

    if (this.collapse && this.collapseSaveId) {
      this.state.saveCardOptions({ id: this.collapseSaveId, isCardCollapsed: this.isCardCollapsed });
    }
  }

  private loadInitialCollapseState(): void {
    if (this.collapse && this.collapseSaveId) {
      const options = this.service.loadOptions(this.collapseSaveId);
      if (options) {
        this.isCardCollapsed = options.isCardCollapsed;
        this.animationState = this.isCardCollapsed ? 'collapse' : 'expand';
      }
    }
  }
}
