import { Component, Input } from '@angular/core';
import { ProfileSize } from '../../models';
import { Person } from '../../models/person';
import { FormlyFieldConfig, FormlyFieldProps } from '@ngx-formly/core';

@Component({
  selector: 'ni-team-member-view',
  templateUrl: './team-member-view.component.html',
  styleUrls: ['./team-member-view.component.scss'],
})
export class TeamMemberViewComponent {
  @Input() person!: Person;
  @Input() href?: string;
  @Input() title: string = '';
  @Input() imageSize: ProfileSize = 'medium';
  @Input() photoProp: string = '';
  @Input() initialsProp: string = '';
  @Input() fields?: FormlyFieldConfig<FormlyFieldProps & { [additionalProperties: string]: any }>[];
  @Input() rank?: string;
}
