import { Component, Input } from '@angular/core';
import { ButtonListButton } from '../../models/button-list-button';
import { UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'ni-button-list',
  templateUrl: './button-list.component.html',
  styleUrls: ['./button-list.component.scss'],
})
export class ButtonListComponent {
  @Input() enableClear = false;
  @Input() clearText = '';
  @Input() buttonsToShow: ButtonListButton[] = [];
  @Input() formControl?: UntypedFormControl;

  setValue(value: string): void {
    if (value) {
      this.formControl?.setValue(value);
    }
  }

  clearValue(): void {
    this.formControl?.reset();
  }
}
