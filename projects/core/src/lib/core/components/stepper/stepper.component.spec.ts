import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StepperComponent } from './stepper.component';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { Location } from '@angular/common';

describe('StepperComponent', () => {
  let component: StepperComponent;
  let fixture: ComponentFixture<StepperComponent>;
  const spyField = jasmine.createSpyObj('field', [], ['fieldGroup']);
  const spyLocation = jasmine.createSpyObj('location', ['go', 'path']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StepperComponent],
      providers: [{ provide: Location, useValue: spyLocation }],
    }).compileComponents();

    fixture = TestBed.createComponent(StepperComponent);
    component = fixture.componentInstance;

    component.field = spyField;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngAfterViewInit', () => {
    it('should set initializing to false', () => {
      // Arrange
      component.initializing = true;

      // Act
      component.ngAfterViewInit();

      // Assert
      expect(component.initializing).toBeFalse();
    });
  });

  describe('ngOnInit', () => {
    it('should call removeHiddenFields', () => {
      // Arrange
      spyOn(component, 'removeHiddenFields');

      // Act
      component.ngOnInit();

      // Assert
      expect(component.removeHiddenFields).toHaveBeenCalled();
    });
  });

  describe('isValid', () => {
    it('should return true if field contains a key and is valid', () => {
      // Arrange
      const confField: FormlyFieldConfig = <Partial<FormlyFieldConfig>>{ key: 'testKey', formControl: { valid: true } };

      // Act
      const result = component.isValid(confField);

      // Assert
      expect(result).toBeTrue();
    });
    it('should return false if field does not contain a key or any children', () => {
      // Arrange
      const confField: FormlyFieldConfig = <Partial<FormlyFieldConfig>>{ formControl: { valid: false } };

      // Act
      const result = component.isValid(confField);

      // Assert
      expect(result).toBeFalse();
    });
  });

  describe('stepChanged', () => {
    it('should set the selectedStep.interacted property to false', () => {
      // Arrange
      const confEvent: StepperSelectionEvent = <any>{
        selectedStep: { interacted: true },
        previouslySelectedStep: { interacted: true },
        selectedIndex: 1,
      };

      // Act
      component.stepChanged(confEvent);

      // Assert
      expect(confEvent.selectedStep.interacted).toBeFalse();
    });
    it('should set the previouslySelectedStep.interacted property to false', () => {
      // Arrange
      const confEvent: StepperSelectionEvent = <any>{
        selectedStep: { interacted: true },
        previouslySelectedStep: { interacted: true },
        selectedIndex: 1,
      };

      // Act
      component.stepChanged(confEvent);

      // Assert
      expect(confEvent.previouslySelectedStep.interacted).toBeFalse();
    });
    it('should set the component.selectedIndex to be the same as the event.selectedIndex', () => {
      // Arrange
      const confEvent: StepperSelectionEvent = <any>{
        selectedStep: { interacted: true },
        previouslySelectedStep: { interacted: true },
        selectedIndex: 1,
      };

      // Act
      component.stepChanged(confEvent);

      // Assert
      expect(component.selectedIndex).toEqual(confEvent.selectedIndex);
    });
    it('should call location.go', () => {
      // Arrange
      const confEvent: StepperSelectionEvent = <any>{
        selectedStep: { interacted: true },
        previouslySelectedStep: { interacted: true },
        selectedIndex: 1,
      };

      // Act
      component.stepChanged(confEvent);

      // Assert
      expect(spyLocation.go).toHaveBeenCalled();
    });
  });
  describe('removeHiddenFields', () => {
    it('should set stepsToShow to an empty array if field.fieldGroup is undefined', () => {
      // Arrange
      const confField: FormlyFieldConfig = <Partial<FormlyFieldConfig>>{};
      component.field = confField;

      // Act
      component.removeHiddenFields();

      // Assert
      expect(component.stepsToShow).toEqual([]);
    });
    it('should set stepsToShow to an array of fields where field.hidden is false', () => {
      // Arrange
      const confField: FormlyFieldConfig = <any>{
        fieldGroup: [{ props: { hidden: false } }, { props: { hidden: true } }, { props: { hidden: false } }],
      };
      component.field = confField;

      // Act
      component.removeHiddenFields();

      // Assert
      expect(component.stepsToShow).toEqual([confField.fieldGroup![0], confField.fieldGroup![2]]);
    });
  });
});
