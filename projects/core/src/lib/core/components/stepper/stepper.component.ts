import { AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { MediaObserver } from '@angular/flex-layout';
import { Location } from '@angular/common';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { StepperSelectionEvent } from '@angular/cdk/stepper';

@UntilDestroy()
@Component({
  selector: 'ni-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StepperComponent implements OnInit, AfterViewInit {
  @Input() isLinear!: boolean;
  @Input() options?: FormlyFormOptions;
  @Input() field!: FormlyFieldConfig;

  constructor(private mediaObserver: MediaObserver, private change: ChangeDetectorRef, private location: Location) {}

  isSmallBreakpoint$ = this.mediaObserver.asObservable().pipe(
    map((media) => {
      const match = media.find((m) => m.mqAlias === 'lt-md');
      return !!match;
    })
  );

  selectedIndex = 0;
  stepsToShow: FormlyFieldConfig[] = [];
  baseUrl = '';
  initializing = true;

  ngAfterViewInit(): void {
    // this is in to fix an issue with certain 3rd party controls (suncFusion) which don't work with complex formly configs.
    this.initializing = false;
    this.change.markForCheck();
  }

  ngOnInit(): void {
    this.removeHiddenFields();
    this.options?.fieldChanges?.pipe(untilDestroyed(this)).subscribe(() => this.removeHiddenFields());
    this.baseUrl = this.location.path();
  }

  isValid(field: FormlyFieldConfig): boolean | undefined {
    const childrenLength = field.fieldGroup?.length ?? 0;

    if (field.key || childrenLength < 1) {
      return field.formControl?.valid;
    }

    return field.fieldGroup?.every((f) => this.isValid(f));
  }

  stepChanged(event: StepperSelectionEvent): void {
    // Changing stepper steps was causing revalidation on mat-form-fields
    // so red outlines were being applied on controls that were not touched
    // but invalid. Here we set interacted false if going back to a tab
    // so that it does re-validate the controls.
    // https://stackoverflow.com/q/51368437

    // Take the stepper out of the validation process as the form cannot be
    // submitted if invalid anyway. As we allow stepper presses on possibly
    // invalid steps, this was leaving invalid borders on reset controls.
    event.selectedStep.interacted = false;
    event.previouslySelectedStep.interacted = false;
    this.selectedIndex = event.selectedIndex;

    // The dialog will auto close, but it still steps back onto previous page
    // adding a partial will keep it on the same step when navigating back
    // with browswer controls.
    this.location.go(this.baseUrl + '#step');
  }

  removeHiddenFields(): void {
    // The property "hidden" is auto placed into props when the
    // field has a hideExpression set
    this.stepsToShow = this.field.fieldGroup?.filter((field) => !field.props?.['hidden']) ?? [];
  }
}
