import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ni-pips',
  templateUrl: './pips.component.html',
  styleUrls: ['./pips.component.scss'],
})
export class PipsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    for (let i = 0; i < this.length; i++) {
      if (i == this.currIndex) {
        this.pips.push({ active: true });
      } else {
        this.pips.push({ active: false });
      }
    }
  }

  @Input() length!: any;
  @Input() currIndex!: any;

  @Output() pipClicked = new EventEmitter<number>();

  pips: { active: boolean }[] = [];

  handlePipClick(index: number) {
    this.pipClicked.emit(index);
  }
}
