import { Component, Input } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormatComponent } from './format.component';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: '[ni-format]',
  template: '',
})
export class MockFormatComponent {
  @Input() value: unknown;
  @Input() dataType: unknown;
}

describe('FormatComponent', () => {
  let component: FormatComponent;
  let fixture: ComponentFixture<FormatComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormatComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
