import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { OnChange } from 'property-watch-decorator';
import { convertToDataType, DataType } from '../../models';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: '[ni-format]',
  templateUrl: './format.component.html',
  styleUrls: ['./format.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormatComponent {
  @Input() value: any;

  @OnChange<DataType>(function (this: FormatComponent, value) {
    this.dataTypeClean = convertToDataType(value);
  })
  @Input()
  dataType?: DataType;

  DataType = DataType;
  dataTypeClean?: DataType;
}
