import { Component } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { IconDirective } from '@nimbus/core/src/lib/core';
import { ChatService } from './../../../core/services/chats.service';
import { MockChatService } from './../../../core/services/chats.service.spec';
import { MessageBarComponent } from './message-bar.component';

@Component({
  selector: 'ni-message-bar',
  templateUrl: './message-bar.component.html',
  styleUrls: ['./message-bar.component.scss'],
})
export class MockMessageBarComponent {}

describe('MessageBarComponent', () => {
  let component: MessageBarComponent;
  let fixture: ComponentFixture<MessageBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MessageBarComponent, IconDirective],
      imports: [MatDialogModule, MatIconModule],
      providers: [{ provide: ChatService, useClass: MockChatService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
