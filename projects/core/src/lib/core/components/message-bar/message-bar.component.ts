import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FilterColumn, DialogFormAction, DialogFormComponent } from '@nimbus/core/src/lib/core';
import { ChatService } from '@nimbus/core/src/lib/core';
import { DialogFormData, DialogFormResult } from '@nimbus/core/src/lib/core';

@UntilDestroy()
@Component({
  selector: 'ni-message-bar',
  templateUrl: './message-bar.component.html',
  styleUrls: ['./message-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class MessageBarComponent {
  @Input() messageCount = 0;
  @Input() filterColumns?: FilterColumn[];
  @Output() toggleMessageBox = new EventEmitter<boolean>();
  @Output() sendComplete = new EventEmitter<null>();

  messageText = new UntypedFormControl('');
  model: any = {};
  fields: FormlyFieldConfig[] = [];
  files = [];
  context = {};
  isOpen = false;

  constructor(private messageService: ChatService, private change: ChangeDetectorRef, public dialog: MatDialog) {
    this.fields = [
      {
        fieldGroup: [
          {
            key: 'upload',
            type: 'uploader',
            props: {
              required: true,
              enableModelSync: true,
            },
          },
        ],
      },
    ];
  }

  newMessage() {
    this.isOpen = !this.isOpen;
    this.toggleMessageBox.emit(this.isOpen);
  }

  attachFile() {
    const context = this.model ? { isEdit: true } : {};
    this.change.markForCheck();

    const data: DialogFormData = {
      model: undefined,
      fieldConfig: this.fields,
      config: {
        enableEdit: false,
        enableDelete: false,
        addText: 'Add',
        deleteText: '',
        cancelText: 'Cancel',
        saveText: '',
        titleTemplate: 'Attachments',
        isChildForm: true,
      },
      disabled: false,
      options: undefined,
      context: context,
    };
    const dialogConfig: MatDialogConfig<DialogFormData> = { data };
    dialogConfig.width = '500px';
    dialogConfig.minWidth = '';
    dialogConfig.maxWidth = '';

    const dialogRef = this.dialog.open(DialogFormComponent, dialogConfig);

    dialogRef
      .afterClosed()
      .pipe(untilDestroyed(this))
      .subscribe((result: DialogFormResult) => {
        switch (result.action) {
          case DialogFormAction.Add:
            this.files = result.model.upload;

            this.addConversationItem();
            break;

          case DialogFormAction.Cancel:
            break;
        }

        this.change.markForCheck();
      });
  }

  sendMessage() {
    if (this.messageText.value) {
      this.addConversationItem();
    }
  }

  close() {
    this.isOpen = false;
  }

  addConversationItem() {
    const filter = this.filterColumns?.find((f) => !f.column && f.entityTypeId);
    if (filter) {
      this.messageService
        .addMessage({
          messageHtml: this.messageText.value,
          entityId: filter?.stringFilter || '',
          entityTypeId: filter?.entityTypeId || 0,
          files: this.files,
        })
        .pipe(untilDestroyed(this))
        .subscribe(() => {
          this.sendComplete.emit();
          this.messageText.patchValue('');
          this.files = [];
          this.isOpen = !this.isOpen;
        });
    }
  }
}
