import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ni-entity-add',
  templateUrl: './entity-add.component.html',
  styleUrls: ['./entity-add.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntityAddComponent {
  @Input() entities: any[] = [];

  @Output() entityClick: EventEmitter<any> = new EventEmitter();

  addOrSearch(entity: any): void {
    this.entityClick.emit({ type: entity.text, formId: entity.formId, dialogConfig: entity.dialogConfig });
  }
}
