import { Component, Input, EventEmitter, Output, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'ni-chip',
  templateUrl: './chip.component.html',
  styleUrls: ['./chip.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChipComponent {
  @Input() listAriaLabel = '';
  @Input() colour = 'primary';
  @Input() text = '';
  @Input() enableRemove = false;
  @Input() removeButtonAriaLabel = 'List Items';
  @Input() ariaLabel = 'Remove Chip';
  @Output() removed = new EventEmitter();

  constructor() {}

  onRemove(): void {
    this.removed.emit();
  }
}
