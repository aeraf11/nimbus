import { Input, Component } from '@angular/core';
import { ThemePalette } from '@angular/material/core';

@Component({
  selector: 'ni-small-button',
  templateUrl: './small-button.component.html',
  styleUrls: ['./small-button.Component.scss'],
})
export class SmallButtonComponent {
  @Input() cssClass: string | undefined;
  @Input() text: string | undefined;
  @Input() color: ThemePalette;
  @Input() disabled = false;
  @Input() title = '';
  @Input() ariaLabel = '';
}
