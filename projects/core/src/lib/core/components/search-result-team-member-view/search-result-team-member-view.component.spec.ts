import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchResultTeamMemberViewComponent } from './search-result-team-member-view.component';

describe('SearchResultTeamMemberViewComponent', () => {
  let component: SearchResultTeamMemberViewComponent;
  let fixture: ComponentFixture<SearchResultTeamMemberViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchResultTeamMemberViewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SearchResultTeamMemberViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
