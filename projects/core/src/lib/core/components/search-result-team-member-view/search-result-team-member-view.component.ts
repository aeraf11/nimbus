import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { Person } from '../../models/person';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { ProfileSize } from '../../models';

@Component({
  selector: 'ni-search-result-team-member-view',
  templateUrl: './search-result-team-member-view.component.html',
  styleUrls: ['./search-result-team-member-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultTeamMemberViewComponent {
  @Input() person!: Person;
  @Input() imageSize: ProfileSize = 'medium';
  @Input() fields?: FormlyFieldConfig[];
  @Input() photoProp: string = '';
  @Input() initialsProp: string = '';
  @Input() rank?: string;
  @Input() userProfileId?: string;
}
