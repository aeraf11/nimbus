import { ChangeDetectionStrategy, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ColourService } from '../../services/colour.service';
import { ChartDataItem } from '../../models/chart-data-item';
import { ChartDataService } from '../../services/chart-data.service';
import { IPointEventArgs, IPointRenderEventArgs, Points } from '@syncfusion/ej2-charts';

export type BarChartType = 'Bar' | 'Column';

@Component({
  selector: 'ni-chart-bar',
  templateUrl: './chart-bar.component.html',
  styleUrls: ['./chart-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ChartBarComponent implements OnInit {
  @Input() data?: ChartDataItem[] | null | undefined;
  @Input() hasData?: boolean | null;
  @Input() flexPosition?: string;
  @Input() width?: string;
  @Input() height?: string;
  @Input() title?: string;
  @Input() subTitle?: string;
  @Input() enableAnimation?: boolean;
  @Input() columnWidth?: number;
  @Input() columnSpacing?: number;
  @Input() chartArea?: any;
  @Input() xAxis?: any;
  @Input() yAxis?: any;
  @Input() barType: string | undefined;
  @Input() direction?: string;
  @Input() params?: any;
  @Input() useThemeForLabels: any;
  @Input() marker?: any;
  @Input() tooltip?: any;
  @Input() formState?: any;

  singleColumnWidth = 0.2;

  constructor(private service: ChartDataService, private colour: ColourService, private router: Router) {}

  ngOnInit() {
    this.useThemeForLabels = this.service.useThemeForLabels(this.xAxis, this.yAxis);
  }

  get barTypeGetter() {
    return this.direction === 'horizontal' ? 'Bar' : 'Column';
  }

  get xAxisGetter() {
    const xAxis: any = {
      title: this.xAxis.title,
      majorGridLines: { width: this.xAxis.lineWidth },
      majorTickLines: { width: this.xAxis.tickWidth },
      lineStyle: { width: this.xAxis.lineWidth },
      valueType: 'Category',
      visible: this.xAxis.visible,
      labelFormat: this.xAxis.labelFormat,
      labelStyle: this.xAxis.labelStyle,
      labelIntersectAction: 'Wrap',
    };

    if (this.direction === 'horizontal') {
      xAxis.enableTrim = true;
      xAxis.maximumLabelWidth = '60';
    }
    return xAxis;
  }

  get yAxisGetter() {
    return {
      title: this.yAxis.title,
      majorGridLines: { width: this.yAxis.lineWidth },
      majorTickLines: { width: this.yAxis.tickWidth },
      lineStyle: { width: this.yAxis.lineWidth },
      labelFormat: this.yAxis.labelFormat,
      labelStyle: this.yAxis.labelStyle,
      visible: this.yAxis.visible,
      rangePadding: 'None',
      minimum: this.yAxis.minimum,
    };
  }


  onPointClick(event: IPointEventArgs) {
    const item = this.getPointData(event.point);
    if (item && item.url) {
      this.router.navigateByUrl(item.url);
    }
  }

  pointRender(args: IPointRenderEventArgs): void {
    if (!this.dataHasColour(args)) {
      const colours = this.colour.colours;
      args.fill = colours[args.point.index % colours.length];
    }
  }

  dataHasColour(args: IPointRenderEventArgs) {
    const data = <ChartDataItem[]>args.series.dataSource;
    if (data) {
      return data.filter((data) => data.colour).length > 0;
    }
    return false;
  }

  getPointData(point: Points) {
    if (!point || !point.x) return null;
    return this.data?.find((item: any) => item.name === point.x) ?? null;
  }
}
