import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartBarComponent } from './chart-bar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChartDataService } from '../../services';
import { MockChartDataService } from '../../services/chart-data.service.spec';

describe('ChartBarComponent', () => {
  let component: ChartBarComponent;
  let fixture: ComponentFixture<ChartBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChartBarComponent],
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: ChartDataService,
          useClass: MockChartDataService,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ChartBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
