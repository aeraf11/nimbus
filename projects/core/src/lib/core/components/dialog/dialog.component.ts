import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Subscription } from 'rxjs';
import { DialogConfig, DialogType } from '../../models';

@Component({
  selector: 'ni-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent {
  public title!: string;
  public text!: string;
  public type: DialogType = DialogType.OK;
  public afterClosed?: Subscription;
  public dialogTypes = DialogType;
  constructor(private dialogRef: MatDialogRef<DialogComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogConfig) {
    if (data) {
      this.title = data.title;
      this.text = data.bodyText;
      this.type = data.type;
    }
  }

  close() {
    this.dialogRef.close();
  }

  confirm() {
    this.dialogRef.close(true);
  }
}
