import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { StateService } from '../../services';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { PageBannerComponent } from './page-banner.component';

describe('PageBannerComponent', () => {
  let component: PageBannerComponent;
  let fixture: ComponentFixture<PageBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PageBannerComponent],
      imports: [MatSnackBarModule],
      providers: [{ provide: StateService, useClass: MockStateService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
