import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { UntilDestroy } from '@ngneat/until-destroy';
import { DynamicRoutePageBanner } from '../../models';
import { StateService } from '../../services';

@UntilDestroy()
@Component({
  selector: 'ni-page-banner',
  templateUrl: './page-banner.component.html',
  styleUrls: ['./page-banner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageBannerComponent implements OnInit {
  @Input() config?: DynamicRoutePageBanner;

  isDarkTheme = false;
  height = '';
  marginBottom = '';
  bannerImage = '';
  position = '';
  size = '';
  logoImage = '';
  logoAlt = '';
  logoWidth = '';
  logoHeight = '';
  logoWrapperHeight?: SafeStyle;

  private defaultBannerImageLight = '/assets/images/phaneros-brand-banner-light.png';
  private defaultLogoImageLight = '/assets/images/phaneros-logo-light.png';
  private defaultBannerImageDark = '/assets/images/phaneros-brand-banner-dark.png';
  private defaultLogoImageDark = '/assets/images/phaneros-logo-dark.png';
  private defaultHeight = '35vh';
  private defaultMarginBottom = '0';
  private defaultPosition = 'center center';
  private defaultSize = 'cover';
  private defaultLogoAltText = 'Phaneros Logo';
  private defaultLogoWidth = 'auto';
  private defaultLogoHeight = 'auto';

  constructor(private state: StateService, private change: ChangeDetectorRef, private sanitiser: DomSanitizer) {}

  ngOnInit(): void {
    this.state.selectSelectedThemeIsDark().subscribe((isDark) => this.setImages(isDark));
    this.height = this.config?.background?.height ?? this.config?.height ?? this.defaultHeight;
    this.marginBottom = this.config?.background?.marginBottom ?? this.config?.marginBottom ?? this.defaultMarginBottom;
    this.position = this.config?.background?.position ?? this.config?.backgroundPosition ?? this.defaultPosition;
    this.size = this.config?.background?.size ?? this.config?.backgroundSize ?? this.defaultSize;
    this.logoAlt = this.config?.logo?.altText ?? this.config?.bannerLogoAlt ?? this.defaultLogoAltText;
    this.logoWidth = this.config?.logo?.width ?? this.defaultLogoWidth;
    this.logoHeight = this.config?.logo?.height ?? this.defaultLogoHeight;
    this.setLogoWrapperHeight();
  }

  private setLogoWrapperHeight(): void {
    const style = this.marginBottom.includes('-') ? `calc(100% - ${this.marginBottom.replace('-', '')})` : `100%`;
    this.logoWrapperHeight = this.sanitiser.bypassSecurityTrustStyle(style);
  }

  private setImages(isDark: boolean | undefined): void {
    this.isDarkTheme = isDark ?? false;
    this.setBannerBackgroundImage();
    this.setBannerLogoImage();
    this.change.markForCheck();
  }

  private setBannerBackgroundImage(): void {
    this.bannerImage = this.isDarkTheme ? this.getDarkThemeBannerImage() : this.getLightThemeBannerImage();
  }

  private setBannerLogoImage(): void {
    this.logoImage = this.isDarkTheme ? this.getDarkThemeLogo() : this.getLightThemeLogo();
  }

  private returnImageUrl(blobOrUrl: string | undefined): string | null {
    if (!blobOrUrl) {
      return null;
    }
    return blobOrUrl.includes('http') ? blobOrUrl : `/api/v2/media/photo?id=${blobOrUrl}`;
  }

  private getDarkThemeBannerImage(): string {
    return (
      this.returnImageUrl(this.config?.background?.darkImage) ??
      this.returnImageUrl(this.config?.background?.image) ?? // fallback to light image if no dark image
      this.config?.bannerImageUrl ?? // fallback to the old config
      this.defaultBannerImageDark
    );
  }

  private getLightThemeBannerImage(): string {
    return (
      this.returnImageUrl(this.config?.background?.image) ??
      this.config?.bannerImageUrl ?? // fallback to the old config
      this.defaultBannerImageLight
    );
  }

  private getDarkThemeLogo(): string {
    return (
      this.returnImageUrl(this.config?.logo?.darkImage) ??
      this.returnImageUrl(this.config?.logo?.image) ?? // fallback to light logo if no dark logo
      this.config?.bannerLogoUrl ?? // fallback to the old config
      this.defaultLogoImageDark
    );
  }

  private getLightThemeLogo(): string {
    return (
      this.returnImageUrl(this.config?.logo?.image) ??
      this.config?.bannerLogoUrl ?? // fallback to the old config
      this.defaultLogoImageLight
    );
  }
}
