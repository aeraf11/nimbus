import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  OnInit,
  Optional,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  AppDateTimeAdapter,
  BlobService,
  convertToDataType,
  DataListColumnDefinition,
  DataTableAction,
  DataType,
  EntityMediaRequestComponent,
  FileService,
  FormsHelperService,
  IndexedDbService,
  ListFilterComponent,
  ListFilterService,
  ListService,
  MediaService,
  SignalRService,
  StateService,
  utils,
} from '@nimbus/core/src/lib/core';
import { take } from 'rxjs/operators';
import { CardListWrapperComponent } from '@nimbus/core/src/lib/core';
import { QuickSearchWrapperComponent } from '@nimbus/core/src/lib/core';
import { DataListBaseComponent } from '../data-list-base/data-list-base.component';

@UntilDestroy()
@Component({
  selector: 'ni-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DataTableComponent),
      multi: true,
    },
  ],
})
export class DataTableComponent extends DataListBaseComponent implements OnInit, ControlValueAccessor {
  @ViewChildren(MatSort) sorts!: QueryList<MatSort>;

  @Input() actionsMinWidth = '';
  @Input() actionsMaxWidth = '';
  @Input() actionsTabs = '';
  @Input() isFlat = false;
  @Input() selectPosition = 0;
  @Input() tableActions: DataTableAction[] = [];
  @Input() ariaTableName = '';
  @Input() ariaLabelField = '';
  @Input() ariaColumnDefinitions: string[] = [];

  isTabbed = false;
  currentActionsTab = '';
  tabIds: string[] = [];
  tabbedColumnIds: Map<string, string[]> = new Map<string, string[]>();
  tabbedColumns: Map<string, any[]> = new Map<string, any[]>();
  ariaLabel = '';

  constructor(
    service: ListService,
    change: ChangeDetectorRef,
    private router: Router,
    state: StateService,
    public override formsHelper: FormsHelperService,
    private blobService: BlobService,
    @Optional() cardListWrapper: CardListWrapperComponent,
    listFilterService: ListFilterService,
    dialog: MatDialog,
    @Optional() entityMediaRequest: EntityMediaRequestComponent,
    @Optional() listFilter: ListFilterComponent,
    @Optional() quickSearchWrapper: QuickSearchWrapperComponent,
    private mediaService: MediaService,
    private changes: ChangeDetectorRef,
    indexedDbService: IndexedDbService,
    private appDateTimeAdapter: AppDateTimeAdapter,
    signalR: SignalRService,
    private fileService: FileService
  ) {
    super(
      service,
      change,
      state,
      formsHelper,
      cardListWrapper,
      listFilterService,
      dialog,
      entityMediaRequest,
      listFilter,
      quickSearchWrapper,
      indexedDbService,
      signalR
    );
  }

  override ngOnInit(): void {
    super.ngOnInit();

    this.ariaLabel = this.ariaTableName ? this.ariaTableName : `Table of ${this.listName ?? 'data'}`;
    this.isTabbed = this.columnDefinitions.filter((col) => col.tab || col.tabs).length > 0;

    if (this.actions?.length ?? 0 > 0) {
      this.columnIds.push('actions');
    }

    if (this.selectMode !== 'none') {
      this.columnIds.splice(this.selectPosition, 0, 'select');
    }

    if (this.isTabbed) {
      this.mediaService
        .getBreakpoint()
        .pipe(untilDestroyed(this))
        .subscribe((breakpoint) => {
          this.tabbedColumns.clear();
          this.tabbedColumnIds.clear();
          this.tabIds = [];

          this.columnDefinitions.forEach((col) => {
            this.configurateConstantTabs(col);
            this.configureBreakpointTabs(col, breakpoint);
          });

          this.tabbedColumns.forEach((value, key) => {
            if (key) {
              this.tabIds.push(key);
            }
          });

          if (this.actions?.length ?? 0 > 0) {
            const tabLabel = this.mediaService.getBreakpointValueString(this.actionsTabs, breakpoint)?.trim();
            this.currentActionsTab = tabLabel;
            if (tabLabel) {
              const fields = this.tabbedColumns.get(tabLabel) ?? [];
              this.tabbedColumnIds.set(tabLabel, [...fields.map((f) => f.id), 'actions']);
            }
          }

          this.changes.markForCheck();
          this.changes.detectChanges();
        });
    }
  }

  sortChange(event: any): void {
    this.pageIndex = 0;
    this.sortColumn = event.active;
    this.sortDirection = event.direction;
    this.getItems();
    this.selectionChanged();
    this.saveOptions();
  }

  onRowClick(row: any, event: Event) {
    if (this.detailDialogFormDefinition) return;
    this.selectItem(row);
    event.preventDefault();
  }

  onSelectClick(row: any, event: any) {
    if (this.detailDialogFormDefinition) {
      this.selectItem(row);
      return;
    }
    event.stopPropagation();
    return false;
  }

  onActionClick(action: DataTableAction, item: any): void {
    if (this.getActionDisabled(action, item)) {
      return;
    }

    if (action.url) {
      const url = this.formsHelper.parseTemplate(action.url, item, action.fallback ?? '');
      const queryParams = action.params;

      if (queryParams) {
        this.router.navigate([url], { queryParams });
      } else {
        this.router.navigateByUrl(url);
      }
    }

    if (action.download) {
      if (action.download.blobId && action.download.fileName) {
        const blobId = this.formsHelper.parseTemplate(action.download.blobId, item, action.fallback ?? '');
        const fileName = this.formsHelper.parseTemplate(action.download.fileName, item, action.fallback ?? '');

        this.blobService
          .getBlob(`/media/photo?id=${encodeURIComponent(blobId)}`)
          .pipe(untilDestroyed(this), take(1))
          .subscribe((blob: Blob) => this.blobService.downloadBlob(blob, fileName));
      }
      if (action.download.attachmentId) {
        const attachmentId = this.formsHelper.parseTemplate(action.download.attachmentId, item, action.fallback ?? '');
        this.fileService.downloadAttachmentById(attachmentId || '', action.download.logAction);
      }
    }

    if (action.formAction) {
      let onSuceess = undefined;
      if (action.successExpression) {
        const expression = utils.evalStringFormExpression<boolean>(action.successExpression);
        if (expression) {
          onSuceess = () => {
            expression(this.formState.rootModel, item, this.formState.context, this.formsHelper);
            this.changes.markForCheck();
          };
        }
      }

      this.formState.executeAction(
        action.formAction,
        item.payload ?? item, // Payload required for offline as the item has been flattened
        item.routeId ?? undefined,
        onSuceess,
        action.showErrorSnack
      );
    }
  }

  getActionVisible(action: DataTableAction, item: any): boolean {
    if (action.visibility) {
      return this.formsHelper.parseTemplateBool(action.visibility, item, 'true');
    }
    if (action.visibilityExpression) {
      const expression = utils.evalStringFormExpression<boolean>(action.visibilityExpression);
      if (expression) {
        return expression(this.formState.rootModel, item, this.formState.context, this.formsHelper);
      }
    }
    return true;
  }

  getActionDisabled(action: DataTableAction, item: any): boolean {
    if (action.disabled) {
      return this.formsHelper.parseTemplateBool(action.disabled, item, 'true');
    }
    if (action.disabledExpression) {
      const expression = utils.evalStringFormExpression<boolean>(action.disabledExpression);
      if (expression) {
        return expression(this.formState.rootModel, item, this.formState.context, this.formsHelper);
      }
    }
    return false;
  }

  getActionTitle(action: DataTableAction, item: any): string {
    let disabled = action.disabled;
    let disabledTitle = action.disabledTitle;

    if (action.disabledExpression) {
      const expression = utils.evalStringFormExpression<string>(action.disabledExpression);
      if (expression) {
        disabled = expression(this.formState.rootModel, item, this.formState.context, this.formsHelper);
      }
    }

    if (action.disabledTitleExpression) {
      const expression = utils.evalStringFormExpression<string>(action.disabledTitleExpression);
      if (expression) {
        disabledTitle = expression(this.formState.rootModel, item, this.formState.context, this.formsHelper);
      }
    }

    if (disabled && disabledTitle) {
      return disabledTitle;
    }

    return '';
  }

  getActionAriaLabel(action: DataTableAction, item: any): string {
    const columnData: string[] = [];

    if (action.ariaLabel) {
      return this.formsHelper.parseTemplate(action.ariaLabel, item, '');
    } else if (this.ariaColumnDefinitions !== undefined && this.ariaColumnDefinitions.length > 0) {
      //If column definitions are provided as a template option.
      this.ariaColumnDefinitions.forEach((ariaColDef) => {
        //Play it safe here with lowerCase because otherwise everything will be case sensitive so more chance of errors.
        const columnDefinition = this.columnDefinitions.find((cd) => cd.id.toLowerCase() === ariaColDef.toLowerCase());
        if (columnDefinition) {
          const column = this.setAriaColumnData(columnDefinition, item);
          if (column) {
            columnData.push(column);
          }
        } else {
          console.error('The aria column definition', ariaColDef, 'could not be found in the column definitions');
        }
      });
    } else {
      //If no options are provided, default to all columns.
      this.columnDefinitions.forEach((cd) => {
        const column = this.setAriaColumnData(cd, item);
        if (column) {
          columnData.push(column);
        }
      });
    }
    return item.unreadMessages > 0
      ? `${action.name} ${columnData.join(' ')} ${item.unreadMessages} unread messages`
      : `${action.name} ${columnData.join(' ')}`;
  }

  setAriaColumnData(column: DataListColumnDefinition, item: any) {
    if (column.value && item[column.id]) {
      try {
        const dataTypeClean = convertToDataType(column.dataType);
        switch (dataTypeClean) {
          case DataType.date: {
            const formattedDate = this.appDateTimeAdapter.formatDateTime(item[column.id], true);
            return `${column.value} ${formattedDate}`;
          }
          case DataType.dateTimeNoTz: {
            const formattedDate = this.appDateTimeAdapter.formatDateTime(item[column.id], false);
            return `${column.value} ${formattedDate}`;
          }
          case DataType.dateTimeWithSeconds: {
            const formattedDate = this.appDateTimeAdapter.formatDateTime(item[column.id], true, true);
            return `${column.value} ${formattedDate}`;
          }
          case DataType.dateTimeWithSecondsNoTz: {
            const formattedDate = this.appDateTimeAdapter.formatDateTime(item[column.id], false, true);
            return `${column.value} ${formattedDate}`;
          }
        }
      } catch {
        console.warn('There was a problem setting the aria data on', item);
      }
    }
    return `${column.value} ${item[column.id]}`;
  }

  private configurateConstantTabs(col: DataListColumnDefinition, fallback = ''): void {
    if (!col.tabs) {
      this.setTabbedColumns(col.tab ?? fallback, col);
    }
  }

  private configureBreakpointTabs(col: DataListColumnDefinition, breakpoint: string): void {
    if (col?.tabs) {
      const tabLabel = this.mediaService.getBreakpointValueString(col.tabs, breakpoint)?.trim();
      const label = tabLabel === '!' ? '' : tabLabel ?? '';
      const tabId = label ?? null;

      this.setTabbedColumns(tabId, col);
    }
  }

  private setTabbedColumns(tabId: string, col: DataListColumnDefinition): void {
    if (tabId) {
      const fields = this.tabbedColumns.get(tabId) ?? [];
      this.tabbedColumns.set(tabId, [...fields, col]);
      this.tabbedColumnIds.set(tabId, [...fields.map((f) => f.id), col.id]);
    }
  }
}
