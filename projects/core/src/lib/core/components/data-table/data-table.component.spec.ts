import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { RouterTestingModule } from '@angular/router/testing';
import {
  BlobService,
  IconDirective,
  ListService,
  LoadingComponent,
  SiteConfig,
  StateService,
} from '@nimbus/core/src/lib/core';
import { FilterBarComponent } from './../filter-bar/filter-bar.component';
import { DataTableComponent } from './data-table.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { MockListService } from '../../services/list.service.spec';
import { MockBlobService } from '../../services/blob.service.spec';

describe('DataTableComponent', () => {
  let component: DataTableComponent;
  let fixture: ComponentFixture<DataTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DataTableComponent, LoadingComponent, FilterBarComponent, IconDirective],
      imports: [RouterTestingModule, MatDialogModule, MatIconModule, HttpClientTestingModule],
      providers: [
        { provide: StateService, useClass: MockStateService },
        { provide: ListService, useClass: MockListService },
        { provide: BlobService, useClass: MockBlobService },
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataTableComponent);
    component = fixture.componentInstance;
    component.listName = 'Test';

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
