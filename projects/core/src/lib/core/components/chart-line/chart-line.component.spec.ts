import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChartLineComponent } from './chart-line.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChartDataService } from '../../services';
import { MockChartDataService } from '../../services/chart-data.service.spec';

describe('ChartLineComponent', () => {
  let component: ChartLineComponent;
  let fixture: ComponentFixture<ChartLineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChartLineComponent],
      imports: [HttpClientTestingModule],
      providers: [{ provide: ChartDataService, useClass: MockChartDataService }],
    }).compileComponents();

    fixture = TestBed.createComponent(ChartLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
