import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Chart, ChartComponent } from '@syncfusion/ej2-angular-charts';
import { Observable, debounceTime } from 'rxjs';
import { ChartDataItem, utils } from '../..';

@UntilDestroy()
@Component({
  selector: 'ni-chart-line',
  templateUrl: './chart-line.component.html',
  styleUrls: ['./chart-line.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ChartLineComponent implements OnInit, AfterViewInit {
  @Input() areaColour?: string;
  @Input() chartArea?: any;
  @Input() columnSpacing?: number;
  @Input() columnWidth?: number;
  @Input() data?: ChartDataItem[] | null | undefined;
  @Input() direction?: string;
  @Input() enableAnimation?: boolean;
  @Input() flexPosition?: string;
  @Input() formState?: any;
  @Input() hasData?: boolean | null;
  @Input() height?: string;
  @Input() isAreaGradient?: boolean;
  @Input() label?: string;
  @Input() lineColour?: string;
  @Input() lineWidth?: number;
  @Input() marker?: any;
  @Input() maxWidth?: string;
  @Input() subTitle?: string;
  @Input() params?: any;
  @Input() title?: string;
  @Input() titleAlignment?: string;
  @Input() titleColour?: string;
  @Input() tooltip?: any;
  @Input() width?: string;
  @Input() xAxis?: any;
  @Input() yAxis?: any;

  useThemeForLabels = true;
  useThemeForTitle = true;
  seriesBorder?: any;
  resize$?: Observable<ResizeObserverEntry[] | undefined>;

  @ViewChild('outer')
  public outer?: ElementRef;

  @ViewChild('line')
  public line?: ChartComponent | Chart;

  constructor(public change: ChangeDetectorRef, private element: ElementRef) {}

  ngOnInit(): void {
    this.useThemeForLabels = this.useThemeLabels(this.xAxis, this.yAxis);
    this.useThemeForTitle = !this.titleColour;

    this.buildXAxis();
    this.buildYAxis();
    this.buildSeriesBorder();

    this.setAreaGradientColour();
  }

  ngAfterViewInit(): void {
    this.resize$ = utils.resizeObservable(this.outer?.nativeElement).pipe(untilDestroyed(this));
    this.resize$.pipe(untilDestroyed(this), debounceTime(500)).subscribe(() => {
      if (this.line) {
        this.line.refresh();
      }
    });
  }

  buildXAxis() {
    this.xAxis = {
      title: this.xAxis?.title,
      titleStyle: this.xAxis?.titleStyle,
      majorGridLines: { width: this.xAxis?.gridWidth },
      majorTickLines: { width: this.xAxis?.tickWidth },
      lineStyle: { width: this.xAxis?.lineWidth },
      labelPadding: this.xAxis?.labelPadding,
      labelPlacement: this.xAxis?.labelPlacement,
      labelStyle: this.xAxis?.labelStyle,
      labelFormat: this.xAxis?.labelFormat,
      valueType: this.xAxis?.valueType || 'Category',
      interval: this.xAxis?.interval,
      intervalType: this.xAxis?.intervalType,
      rangePadding: this.xAxis?.rangePadding,
      visible: this.xAxis?.visible,
    };
  }

  buildYAxis() {
    this.yAxis = {
      title: this.yAxis?.title,
      majorGridLines: { width: this.yAxis?.gridWidth },
      majorTickLines: { width: this.yAxis?.tickWidth },
      lineStyle: { width: this.yAxis?.lineWidth },
      labelFormat: this.yAxis?.labelFormat,
      labelStyle: this.yAxis?.labelStyle,
      minimum: this.yAxis?.minimum,
      visible: this.yAxis?.visible,
    };
  }

  buildSeriesBorder(): any {
    this.seriesBorder = {
      width: this.lineWidth,
      color: this.lineColour,
    };
  }

  private useThemeLabels(xAxis: any, yAxis: any): boolean {
    return xAxis?.labelStyle?.color && yAxis?.labelStyle?.color;
  }

  private setAreaGradientColour(): void {
    if (this.isAreaGradient) {
      this.element.nativeElement.style.setProperty('--gradient-color', this.areaColour);
    }
  }
}
