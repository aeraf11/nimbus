import { LiveAnnouncer } from '@angular/cdk/a11y';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Input,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, UntypedFormControl } from '@angular/forms';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatDialog } from '@angular/material/dialog';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { OnChange } from 'property-watch-decorator';
import { Observable, of } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DialogTreeAction, DialogTreeData, DialogTreeResult, FilterColumn, Lookup, LookupNode } from '../../models';
import { ListService, LookupService, StateService } from '../../services';
import { DialogTreeComponent } from '../dialog-tree/dialog-tree.component';

@UntilDestroy()
@Component({
  selector: 'ni-multi-choice',
  templateUrl: './multi-choice.component.html',
  styleUrls: ['./multi-choice.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultiChoiceComponent),
      multi: true,
    },
  ],
})
export class MultiChoiceComponent implements ControlValueAccessor, AfterViewInit {
  @OnChange<string>(function (this: MultiChoiceComponent, value, change) {
    if (this.lookupKey) {
      this.setLookups();
    }
  })
  @Input()
  lookupKey: string | null = null;

  @OnChange<string>(function (this: MultiChoiceComponent, value, change) {
    if (value) {
      // Having to use setTimeout here as this was firing before any of the @Inputs
      // had been set from the lookup-type props
      setTimeout(() => {
        this.removeAll();
        this.setLookups();
      }, 0);
    }
  })
  @Input()
  parentLookupId = '';

  @OnChange<string[]>(function (this: MultiChoiceComponent, value, change) {
    this.setLookups();
  })
  @Input()
  lookupFilters: string[] = [];

  @Input() id = '';
  @Input() selectable = true;
  @Input() removable = true;
  @Input() addCustom = false;
  @Input() leafSelection = true;
  @Input() enableTree: boolean | 'auto' = 'auto';
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() placeholder = 'Type to add a new item';
  @Input() newItemLabel = 'Add new item...';
  @Input() okText = 'Ok';
  @Input() cancelText = 'Cancel';
  @Input() isFilterSearchMode = false;
  @Input() outputDisplayValue = false;
  /* Dynamic lists */
  @Input() valueProp = 'id';
  @Input() labelProp = 'displayValue';
  @Input() parentIdProp = null;
  @Input() listName: string | null = null;
  @Input() filterColumns: Array<FilterColumn> = [];
  @Input() selectColumns: string[] = [];
  @Input() sortColumn = '';
  @Input() prefixColumn = '';
  @Input() ariaLabel = '';
  @Input() lookups: Lookup[] = [];
  @Input() readonly = false;
  @ViewChild('itemInput') itemInput!: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete!: MatAutocomplete;

  separatorKeysCodes: number[] = [ENTER, COMMA];
  disabled = false;
  itemControl = new UntypedFormControl();
  negative = new UntypedFormControl();
  filteredItems!: Observable<Lookup[]>;
  selectedItems: Lookup[] = [];
  isNegative = false;
  lookupsFlat: Lookup[] | null = null;
  lookupsHierachy$!: Observable<LookupNode[] | null>;
  isTree = false;
  pendingValue: string[] | null = null;
  loadingMatches = false;

  private onChange!: (value: any) => void;
  private onTouched!: () => void;

  constructor(
    private change: ChangeDetectorRef,
    private lookUpService: LookupService,
    public dialog: MatDialog,
    private state: StateService,
    private listService: ListService,
    private liveAnnouncer: LiveAnnouncer
  ) {
    this.filteredItems = this.itemControl.valueChanges.pipe(
      startWith(null),
      map((value: string | null) => (value ? this._filter(value) : this.lookupsFlat ? this.lookupsFlat.slice() : []))
    );
  }

  ngAfterViewInit(): void {
    if (this.listName) {
      this.getList();
    }
    if (this.lookups && this.lookups.length > 0) {
      this.buildList(this.lookups);
    }
  }

  remove(item: Lookup) {
    this.selectedItems = this.selectedItems.filter((i) => i.lookupId !== item.lookupId);
    this.setChange();
    this.change.markForCheck();
    this.liveAnnouncer.announce(`Removed ${item.hierarchicalDisplayValue}`);
  }

  removeAll(): void {
    this.selectedItems = [];
    this.setChange();
    this.change.markForCheck();
    this.liveAnnouncer.announce(`Removed all items`);
  }

  add(event: MatChipInputEvent) {
    if (this.addCustom) {
      event.input.value = '';
      this.itemControl.setValue(null);
      const item = this.getItem('', event.value);
      this.selectedItems.push(item);
      this.setChange();
    }
  }

  blur() {
    this.onTouched();
  }

  selected(event: MatAutocompleteSelectedEvent) {
    this.itemInput.nativeElement.value = '';
    this.itemControl.setValue(null);
    const item = this.getItem(event.option.value, '');
    this.selectedItems.push(item);
    this.setChange();
    this.liveAnnouncer.announce(`Selected ${item.hierarchicalDisplayValue}`);
  }

  openDialog(): void {
    const data: DialogTreeData = {
      items: this.lookupsHierachy$,
      selectedItems: this.selectedItems,
      addCustom: this.addCustom,
      placeholder: this.placeholder,
      newItemLabel: this.newItemLabel,
      disabled: this.disabled,
      okText: this.okText,
      cancelText: this.cancelText,
      leafSelection: this.leafSelection,
    };
    const dialogRef = this.dialog.open(DialogTreeComponent, {
      data,
    });
    dialogRef.componentInstance.itemAdded.pipe(untilDestroyed(this)).subscribe((lookup: Lookup) => {
      this.state.addLookup({ ...lookup, lookupKey: this.lookupKey || '' });
    });
    dialogRef.afterClosed().subscribe((result: DialogTreeResult) => {
      if (!this.disabled) {
        if (result?.action === DialogTreeAction.Ok) {
          this.selectedItems = result.items;
          this.setChange();
          this.onTouched();
          this.change.markForCheck();
        }
      }
    });
  }

  setChange() {
    let obj = null;
    if (this.isFilterSearchMode) {
      obj = {
        items: this.getSelectedIds(),
        negative: this.isNegative,
      };
    } else {
      obj = this.getSelectedIds();
    }
    this.onChange(obj);
  }

  changeNegative(event: any): void {
    this.isNegative = !event.value;
    this.setChange();
  }

  getSelectedIds(): any[] {
    if (!this.selectedItems) {
      return [];
    }
    if (this.leafSelection) {
      return this.selectedItems.filter((lookup) => lookup.isLeaf).map((item) => this.getOuputItem(item));
    }
    return this.selectedItems.map((item) => this.getOuputItem(item));
  }

  getOuputItem(item: Lookup) {
    if (this.isFilterSearchMode) {
      return {
        lookupId: item.lookupId,
        displayValue: item.displayValue,
      };
    }
    return this.outputDisplayValue ? item.hierarchicalDisplayValue : item.lookupId;
  }

  getItem(id: string, name: string): Lookup {
    let lookup = null;
    if (name) {
      lookup = this.lookupsFlat?.find((l) => l.hierarchicalDisplayValue === name);
    } else {
      lookup = this.lookupsFlat?.find((l) => l.lookupId === id);
    }
    return (
      lookup || {
        displayValue: name,
        hierarchicalDisplayValue: name,
        lookupId: name,
        lookupKey: '',
        cssClass: '',
        isLeaf: true,
        level: 0,
      }
    );
  }

  writeValue(ids: any): void {
    if (this.isFilterSearchMode) {
      this.isNegative = ids.negative ?? false;
      if (ids) {
        ids = ids.items.map((val: any) => val.lookupId);
      }
      this.negative.setValue(this.isNegative);
    }
    this.pendingValue = ids;
    this.setItems();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    if (isDisabled) {
      this.itemControl.disable();
    } else {
      this.itemControl.enable();
    }
  }

  setLookups(): void {
    if (this.lookupKey) {
      if (this.parentLookupId) {
        this.lookUpService
          .getLookupsHierachy(this.lookupKey, this.lookupFilters)
          .pipe(untilDestroyed(this))
          .subscribe((lookups) => {
            if (lookups) {
              this.lookupsFlat = this.getLookupsByParentId(lookups, this.parentLookupId).map((l) => l.item);
              this.filteredItems = of(this.lookupsFlat);
              this.setItems();
            }
          });
      } else {
        this.lookUpService
          .getLookupsFlat(this.lookupKey, this.leafSelection, this.lookupFilters)
          .pipe(untilDestroyed(this))
          .subscribe((lookups) => {
            this.lookupsFlat = lookups;
            this.setItems();
          });
        this.lookupsHierachy$ = this.lookUpService.getLookupsHierachy(this.lookupKey, this.lookupFilters);
        this.lookupsHierachy$.pipe(untilDestroyed(this)).subscribe((lookups) => {
          this.isTree = lookups?.some((l) => l.children) || false;
        });
      }
      this.change.markForCheck();
    }
  }

  getList() {
    if (this.listName) {
      this.loadingMatches = true;
      this.change.markForCheck();
      this.listService
        .getTypeAheadList(this.listName, '', this.sortColumn, this.selectColumns, this.filterColumns, 1000)
        .pipe(untilDestroyed(this))
        .subscribe((lists) => {
          this.lookupsFlat = lists.map((item) => ({
            parentLookupId: this.parentIdProp ? item[this.parentIdProp || ''] : null,
            displayValue: item[this.labelProp],
            hierarchicalDisplayValue: item[this.labelProp],
            lookupId: item[this.valueProp],
            lookupKey: '',
            cssClass: '',
            isLeaf: true,
            level: 0,
            isTopLevel: item.isHierarchyTopLevel,
            prefix: this.prefixColumn ? item[this.prefixColumn] : '',
            entityType: item.entityType,
          }));
          this.setItems();
          this.loadingMatches = false;
          this.itemControl.setValue(null);
          this.change.markForCheck();
          if (this.lookupsFlat) {
            if (this.parentIdProp) {
              this.lookupsHierachy$ = this.listService.getHierarchy(this.lookupsFlat);
              this.lookupsFlat = this.lookupsFlat.filter((lookup) => !lookup.isTopLevel);
            } else {
              this.lookupsHierachy$ = of(this.lookupsFlat.map((item) => ({ item: item })));
            }
            this.lookupsHierachy$.pipe(untilDestroyed(this)).subscribe(() => {
              this.isTree = true;
              this.change.markForCheck();
            });
          }
        });
    }
  }

  buildList(lookups: Lookup[]) {
    this.lookupsFlat = lookups;
    this.setItems();
    this.itemControl.setValue(null);
    this.isTree = false;
    this.change.markForCheck();
  }

  setItems() {
    if (this.lookupsFlat && this.pendingValue) {
      this.selectedItems = this.outputDisplayValue
        ? this.lookupsFlat.filter((l) => this.pendingValue?.includes(l.hierarchicalDisplayValue)) || []
        : this.lookupsFlat.filter((l) => this.pendingValue?.includes(l.lookupId)) || [];
      this.pendingValue = null;
    }
  }

  private _filter(value: string): Lookup[] {
    const filterValue = value.toLowerCase();
    return this.lookupsFlat
      ? this.lookupsFlat.filter((item) => item.hierarchicalDisplayValue.toLowerCase().indexOf(filterValue) !== -1)
      : [];
  }

  private getLookupsByParentId(lookups: LookupNode[], parentId: string): LookupNode[] {
    const lookupNodes: LookupNode[] = [];

    lookups.forEach((lookup) => {
      if (lookup?.item?.parentLookupId === parentId) {
        lookupNodes.push(lookup);
      }
      if (lookup?.children && lookup.children?.length > 0) {
        return lookupNodes.push(...this.getLookupsByParentId(lookup.children, parentId));
      }

      return;
    });

    return lookupNodes;
  }
}
