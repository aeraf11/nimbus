import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation, forwardRef } from '@angular/core';
import { FormsHelperService } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ni-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true,
    },
  ],
})
export class SelectComponent implements ControlValueAccessor {
  localControl = new FormControl();
  @Input() id?: any;
  @Input() empty?: boolean;
  @Input() model: any;
  @Input() form: any;
  @Input() formField: any;
  @Input() listName?: any;
  @Input() valueProp?: any;
  @Input() labelProp?: any;
  @Input() groupProp?: any;
  @Input() disabledProp?: any;
  @Input() addDefaultOption?: boolean;
  @Input() overrideFormReset?: boolean;
  @Input() version?: any;
  @Input() labelTemplate?: string;
  @Input() enableModelSync?: boolean;
  @Input() modelSyncExclusions?: any;
  @Input() attributes?: any;
  @Input() selectAllOption?: any;
  @Input() placeholder?: string;
  @Input() tabindex?: number;
  @Input() multiple?: boolean;
  @Input() disableOptionCentering?: boolean;
  @Input() appearance?: any;
  @Input() options?: any;
  @Input() value: any;
  @Input() required = false;
  @Input() loadingMatches?: boolean;
  @Input() items?: any;
  @Input() errorStateMatcher: any;

  private selectAllValue!: { options: any; value: any[] };
  private optionLabelProp = 'displayValue';
  private optionValueProp = 'id';

  private onChange!: (value: any) => void;
  private onTouched!: () => void;

  constructor(private formsHelper: FormsHelperService) {}

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  getLabel(item: any) {
    if (this.version === 1) return item.displayName;
    if (item === null && (this.addDefaultOption === undefined || this.addDefaultOption)) {
      return '---choose---';
    }
    return this.labelTemplate
      ? this.formsHelper.simpleTemplate(this.labelTemplate, item, '')
      : item[this.labelProp || 'label'];
  }

  getSelectAllState(options: any[]) {
    if (this.empty || this.value.length === 0) {
      return 'unchecked';
    }

    return this.value.length !== this.getSelectAllValue(options).length ? 'indeterminate' : 'checked';
  }

  toggleSelectAll(options: any[]) {
    const selectAllValue = this.getSelectAllValue(options);
    const newValue =
      this.value === undefined || this.value.length !== selectAllValue.length ? selectAllValue : undefined;
    this.onSelectionChange(newValue);
  }

  onSelectionChange(value: any) {
    if (value) {
      const model: any = {};
      if (this.enableModelSync) {
        for (const prop in value) {
          if (!this.modelSyncExclusions.includes(prop)) {
            this.model[prop] = model[prop] = value[prop];
          }
        }
        this.form?.patchValue(model);
      }
    }

    this.setValue(value);
    this.onChange(this.value);
  }

  writeValue(value: any): void {
    this.setValue(value);
  }

  onTouch() {
    this.onTouched();
  }

  setValue(value: any) {
    this.value = value;
    this.localControl.setValue(this.value);
  }

  private getSelectAllValue(options: any[]) {
    if (!this.selectAllValue || options !== this.selectAllValue.options) {
      const flatOptions: any[] = [];
      options.forEach((o) => (o.group ? flatOptions.push(...o.group) : flatOptions.push(o)));

      this.selectAllValue = {
        options,
        value: flatOptions.filter((o) => !o.disabled).map((o) => o.value),
      };
    }
    return this.selectAllValue.value;
  }

  _getAriaLabelledby() {
    if (this.attributes?.['aria-labelledby']) {
      return this.attributes['aria-labelledby'] + '';
    }
    return this.formField?._labelId;
  }

  compareWithFn = (option: any | string, selected: any | string): boolean => {
    if (selected == null) return false;
    if (typeof option === 'string' && typeof selected === 'string') {
      return option === selected;
    } else {
      return (
        option[this.optionValueProp] === selected[this.optionValueProp] ||
        (option[this.optionValueProp] === null && option[this.optionLabelProp] === selected[this.optionLabelProp])
      );
    }
  };
}
