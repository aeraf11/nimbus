import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { EntityBreadcrumb } from '@nimbus/core/src/lib/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@UntilDestroy()
@Component({
  selector: 'ni-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NoteComponent implements OnInit, AfterViewInit {
  @ViewChild('expander') expander?: ElementRef;
  @Input() item: any;
  isOpen = false;
  showExpander = false;
  breadcrumbs: EntityBreadcrumb[] = [];

  constructor(private change: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.parseEntityBreadcrumb();
  }

  ngAfterViewInit(): void {
    this.setShouldShowExpander();
    fromEvent(window, 'resize')
      .pipe(debounceTime(400), distinctUntilChanged(), untilDestroyed(this))
      .subscribe(() => this.setShouldShowExpander());
  }

  private setShouldShowExpander(): void {
    const element = this.expander?.nativeElement;
    if (element) {
      this.showExpander = element.offsetHeight < element.scrollHeight || element.offsetWidth < element.scrollWidth;
      this.change.detectChanges();
    }
  }

  private parseEntityBreadcrumb(): void {
    if (this.item.breadcrumbsJSON) {
      this.breadcrumbs = JSON.parse(this.item.breadcrumbsJSON);
    }
  }
}
