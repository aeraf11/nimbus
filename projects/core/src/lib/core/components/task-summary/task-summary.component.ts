import { Component, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'ni-task-summary',
  templateUrl: './task-summary.component.html',
  styleUrls: ['./task-summary.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TaskSummaryComponent {
  @Input() description: string | undefined;
  @Input() entityId: string | undefined;
  @Input() entityTypeId: string | undefined;
  @Input() overdue: string | undefined;
  @Input() requiredCompletionDate: string | undefined;
  @Input() actionLinked: string | undefined;
}
