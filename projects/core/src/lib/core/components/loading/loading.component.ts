import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { utils } from '../../utils';

@UntilDestroy()
@Component({
  selector: 'ni-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadingComponent implements OnInit {
  loaderSize = '';

  constructor(private elementRef: ElementRef, private change: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.calculateDiameter();
    utils
      .resizeObservable(this.elementRef.nativeElement)
      .pipe(untilDestroyed(this))
      .subscribe(() => this.calculateDiameter());
  }

  calculateDiameter(): void {
    let diameter: number;

    const el = this.elementRef.nativeElement;
    if (el) {
      diameter = el.offsetWidth < el.offsetHeight ? el.offsetWidth : el.offsetHeight;
      diameter = Math.floor(diameter * 0.2);
    } else {
      diameter = 100;
    }

    this.loaderSize = `${diameter}px`;

    this.elementRef.nativeElement.style.setProperty('--loader-border', `${diameter * 0.12}px`);
    this.elementRef.nativeElement.style.setProperty('--loader-margin-before-after', `${diameter * 0.2}px`);
    this.elementRef.nativeElement.style.setProperty('--loader-margin-after', `${diameter * 0.04}px`);

    this.change.markForCheck();
  }
}
