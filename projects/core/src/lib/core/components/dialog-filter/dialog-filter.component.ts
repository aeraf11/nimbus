import { Component, ChangeDetectionStrategy, Inject, ViewChild, ElementRef, HostListener } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  FilterColumn,
  AppDateTimeAdapter,
  FormDefinition,
  DialogFilterConfig,
  DialogFilterAction,
  FormDataService,
  DialogFilterData,
} from '@nimbus/core/src/lib/core';
@Component({
  selector: 'ni-dialog-filter',
  templateUrl: './dialog-filter.component.html',
  styleUrls: ['./dialog-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogFilterComponent {
  DialogFilterAction = DialogFilterAction;
  form: UntypedFormGroup = new UntypedFormGroup({});

  config: DialogFilterConfig = {
    title: 'Filter Results',
    filterText: 'Filter',
    cancelText: 'Cancel',
    clearText: 'Clear',
    columnDefinitions: null,
    lastFilters: [],
  };

  filterableCols: any;
  model: any = {};
  definition: FormDefinition = { id: 'filter-dialog', fields: [], options: {} };
  context = {};

  @ViewChild('content') content!: ElementRef;
  constructor(
    public dialogRef: MatDialogRef<DialogFilterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogFilterData,
    private formDataService: FormDataService,
    private dateTimeAdapter: AppDateTimeAdapter
  ) {
    if (data.config) this.config = { ...this.config, ...data.config };
    this.context = data.options?.formState?.context ?? {};

    if (this.config.columnDefinitions) {
      this.filterableCols = this.config.columnDefinitions;
      this.filterableCols.forEach((element: any) => {
        let searchVal = '';
        if (this.config.lastFilters && this.config.lastFilters.length > 0) {
          const match = this.config.lastFilters.find(
            (filter: any) => filter.column === element.id && filter.stringFilter
          );
          if (match) {
            if (match.rawFilter) {
              searchVal = JSON.parse(match.rawFilter);
            } else {
              searchVal = match.stringFilter;
            }
          }
        }

        const inputType = element.filterParams.formlyType || element.filterType;
        this.definition.fields.push(
          this.formDataService.getFormlyComponentTemplate(
            inputType,
            element.id,
            element.value,
            element.filterParams?.props
          )
        );
        this.model[element.id] = searchVal;
      });
    }
  }

  @HostListener('window:keyup.Enter', [])
  onClose(action: DialogFilterAction = DialogFilterAction.Filter): void {
    const filterColumns = [];
    if (action === DialogFilterAction.Filter) {
      for (const prop in this.model) {
        const details = this.getFormattedSearchDetails(prop);
        if (details.value || details.valueNotNull) {
          filterColumns.push(<FilterColumn>{
            column: prop,
            columnAlias: details.columnAlias,
            stringFilter: details.value,
            constantFilter: true,
            userCreated: true,
            displayValue: details.displayValue || '',
            rawFilter: details.rawFilter,
            isJsonFilter: details.isJsonFilter,
            defaultValue: details.defaultValue,
          });
        }
      }
    } else if (action === DialogFilterAction.Clear) {
      for (const prop in this.model) {
        filterColumns.push(<FilterColumn>{ column: prop, stringFilter: '', constantFilter: true });
      }
    }
    this.dialogRef.close({ action, filterColumns });
  }

  getFormattedSearchDetails(prop: string) {
    const col = this.filterableCols.find((col: any) => col.id === prop);
    let value = this.model[prop];
    let rawFilter;
    let isJsonFilter = false;
    let columnAlias = null;
    let valueNotNull = false;
    const defaultValue = col.filterParams.defaultValue;
    let negativeMarker = '';
    if (col.filterParams?.filterAlias) {
      columnAlias = col.filterParams.filterAlias;
    }
    let displayValue = value;

    if (value) {
      rawFilter = JSON.stringify(value);
      switch (col.filterParams?.formlyType) {
        case 'daterange':
          const range = this.dateTimeAdapter.getDateRange({ start: value.start, end: value.end });
          displayValue = `${value.negative ? '! ' : ''}${range.start?.displayDate} - ${range.end?.displayDate ?? ''}`;
          value = JSON.stringify({ start: range.start?.date, end: range.end?.date, negative: value.negative ?? false });
          isJsonFilter = true;
          break;
        case 'multichoice':
          if (value.length === 0) {
            value = null;
          } else {
            const displayValues = value.items.map((lookup: any) => lookup.displayValue);
            if (value.negative) {
              negativeMarker = '!';
            }
            displayValue = displayValues.map((val: any) => `${negativeMarker}${val}`).join(', ');
            const valueForDBQuery = displayValues.map((val: any) => `${negativeMarker}"${val}"`).join(' ');
            value = valueForDBQuery;
            isJsonFilter = true;
          }
          break;
        case 'select2':
          if (value.length === 0) {
            value = null;
          } else {
            const searchValue = value.map((prop: any) => prop[col.filterParams.searchValueColumn]);
            displayValue = searchValue.join(', ');
            value = JSON.stringify({
              Type: 3,
              ValueForDBQuery: searchValue.join(' '),
              DisplayValue: searchValue.join(' '),
              FromUrlQuery: false,
              Value: searchValue,
              LookUpName: '',
              SearchableOnly: false,
              OptionsIsNegative: value.negative,
            });
            isJsonFilter = true;
          }

          break;
        default:
          break;
      }
    } else if (col.filterParams?.formlyType === 'checkbox') {
      valueNotNull = true;
      displayValue = null;
    }

    return {
      displayValue: displayValue,
      value: value,
      rawFilter: rawFilter,
      columnAlias: columnAlias,
      isJsonFilter: isJsonFilter,
      valueNotNull: valueNotNull,
      defaultValue: defaultValue,
    };
  }

  scrollToBottom() {
    this.content.nativeElement.scrollTop = this.content.nativeElement.scrollHeight;
  }

  scrollToTop() {
    this.content.nativeElement.scrollTop = 0;
  }

  clearForm(): void {
    for (const member in this.model) this.model[member] = '';
    this.onClose(DialogFilterAction.Clear);
  }
}
