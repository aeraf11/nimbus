import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormComponent, FormDataService, StateService } from '@nimbus/core/src/lib/core';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { DialogFilterComponent } from './dialog-filter.component';
import { MockFormsDataService } from '../../../core/services/form-data-service/form-data.service.spec';

describe('DialogFilterComponent', () => {
  let component: DialogFilterComponent;
  let fixture: ComponentFixture<DialogFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DialogFilterComponent, FormComponent],
      imports: [MatDialogModule, ReactiveFormsModule],
      providers: [
        { provide: MatDialogRef, useFactory: () => jasmine.createSpyObj('MatDialogRef', ['close', 'afterClosed']) },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        { provide: StateService, useClass: MockStateService },
        { provide: FormDataService, useClass: MockFormsDataService },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
