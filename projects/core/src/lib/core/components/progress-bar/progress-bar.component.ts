import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { ProgressBar } from '@syncfusion/ej2-angular-progressbar';
import { AnimationModel } from '@syncfusion/ej2-charts';

@Component({
  selector: 'ni-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgressBarComponent {
  @Input() type = 'Circular';
  @Input() width = '250px';
  @Input() height = '250px';
  @Input() max = 100;
  @Input() min = 0;
  @Input() value = 0;
  @Input() subLabel = '';
  @Input() conjunction = 'of';
  @Input() animation: AnimationModel = { enable: true, duration: 500, delay: 0 };
  @Input() thickness = 12;
  @Input() progressColour = '';
  @Input() trackColour = '';
  @Input() isProgressRounded = true;

  @ViewChild('progress') private progress?: ProgressBar;

  // This looks to be required because going backwards in value doesn't seem to be supported
  // so when a user goes from 3 -> 2, the refresh is required to update the progress to go backwards
  refresh(event: any): void {
    if (event.value !== this.value) {
      this.progress?.refresh();
    }
  }
}
