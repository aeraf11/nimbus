import { ChangeDetectionStrategy, Component, Input, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'ni-image-view',
  templateUrl: './image-view.component.html',
  styleUrls: ['./image-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageViewComponent {
  @Input() width: string | undefined;
  @Input() height: string | undefined;
  @Input() margins: string | undefined;
  @Input() text: string | undefined;
  @Input() watermark: boolean | undefined;
  @Input() maintainAspect: boolean | undefined;
  @Input() imageUrl: string | undefined;
  @Input() value: any;

  constructor(private change: ChangeDetectorRef) {}
}
