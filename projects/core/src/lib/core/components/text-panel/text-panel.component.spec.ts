import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SafeHtmlPipe } from './../../pipes/safe-html.pipe';
import { TextPanelComponent } from './text-panel.component';

describe('TextPanelComponent', () => {
  let component: TextPanelComponent;
  let fixture: ComponentFixture<TextPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TextPanelComponent, SafeHtmlPipe],
    }).compileComponents();

    fixture = TestBed.createComponent(TextPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
