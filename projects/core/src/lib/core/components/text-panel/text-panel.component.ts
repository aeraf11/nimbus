import { Component, Input } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatFormFieldAppearance } from '@angular/material/form-field';

@Component({
  selector: 'ni-text-panel',
  templateUrl: './text-panel.component.html',
  styleUrls: ['./text-panel.component.scss'],
})
export class TextPanelComponent {
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() textControl = new UntypedFormControl();
  @Input() ariaLabel = '';
  @Input() color = '';
}
