import { Injectable, OnDestroy } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Subject, Subscription } from 'rxjs';

export enum DocumentEditorEvent {
  None,
  SerialiseData,
  Download,
  Print,
  Cancel,
  Save,
  SaveStarted,
  SaveSuccessful,
  SaveCancelled,
  SaveErrored,
}

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class DocEditorEventService implements OnDestroy {
  docEventSubject: Subject<DocumentEditorEvent> = new Subject<DocumentEditorEvent>();

  ngOnDestroy(): void {
    this.docEventSubject.unsubscribe();
  }

  public Subscribe(func: (data: DocumentEditorEvent) => void): Subscription {
    return this.docEventSubject.pipe(untilDestroyed(this)).subscribe((data) => func(data));
  }

  public Unsubscribe(subscription: Subscription) {
    subscription.unsubscribe();
  }

  public SerialiseData() {
    this.Event(DocumentEditorEvent.SerialiseData);
  }

  public Download() {
    this.Event(DocumentEditorEvent.Download);
  }

  public Print() {
    this.Event(DocumentEditorEvent.Print);
  }

  public Cancel() {
    this.Event(DocumentEditorEvent.Cancel);
  }

  public Save() {
    this.Event(DocumentEditorEvent.Save);
  }

  public SaveStarted() {
    this.Event(DocumentEditorEvent.SaveStarted);
  }

  public SaveSuccessful() {
    this.Event(DocumentEditorEvent.SaveSuccessful);
  }

  public SaveCancelled() {
    this.Event(DocumentEditorEvent.SaveCancelled);
  }

  public SaveErrored() {
    this.Event(DocumentEditorEvent.SaveErrored);
  }

  private Event(data: DocumentEditorEvent) {
    this.docEventSubject.next(data);
  }
}
