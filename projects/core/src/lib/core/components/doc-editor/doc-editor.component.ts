// Note: It is best not to use CVA with the Syncfusion Document Editor component.
// The document editor component uses serialisation to read the document data,
// to support CVA would require serialisation on each content change, this would introduce a performance issue.

import { createSpinner, showSpinner, hideSpinner } from '@syncfusion/ej2-popups';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  CustomToolbarItemModel,
  DocumentEditorContainerComponent,
  FormatType,
  ToolbarItem,
  ToolbarService,
} from '@syncfusion/ej2-angular-documenteditor';
import { ClickEventArgs } from '@syncfusion/ej2-navigations';
import { DocumentEditorEvent, DocEditorEventService } from './doc-editor-event.service';
import { Subscription } from 'rxjs';
import { TAB, ENTER } from '@angular/cdk/keycodes';

@Component({
  selector: 'ni-doc-editor',
  templateUrl: './doc-editor.component.html',
  styleUrls: ['./doc-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ToolbarService],
})
export class DocEditorComponent implements OnChanges {
  public saveToolbarItem: CustomToolbarItemModel = {
    prefixIcon: 'e-icons e-save',
    tooltipText: 'Save document',
    text: 'Save',
    id: 'Save',
  };

  public cancelToolbarItem: CustomToolbarItemModel = {
    prefixIcon: 'e-icons e-changes-reject',
    tooltipText: 'Cancel changes',
    text: 'Cancel',
    id: 'Cancel',
  };

  public downloadToolbarItem: CustomToolbarItemModel = {
    prefixIcon: 'e-icons e-download',
    tooltipText: 'Download',
    text: 'Download',
    id: 'Download',
  };

  public printToolbarItem: CustomToolbarItemModel = {
    prefixIcon: 'e-icons e-print',
    tooltipText: 'Print',
    text: 'Print',
    id: 'Print',
  };

  private defaultToolbarItems: (ToolbarItem | CustomToolbarItemModel)[] = [
    'Undo',
    'Redo',
    'Separator',
    'Image',
    'Table',
    'Separator',
    'Header',
    'Footer',
    'PageSetup',
    'PageNumber',
    'Break',
    'InsertFootnote',
    'InsertEndnote',
    'Separator',
    'Find',
    'Separator',
    'Comments',
    'TrackChanges',
    'Separator',
    'LocalClipboard',
  ];

  @Input() height = '';
  @Input() width = '';
  @Input() readOnly = false;
  @Input() ariaLabel = '';
  @Input() enableToolbar = true;
  @Input() enableSave = true;
  @Input() enableDownload = true;
  @Input() enablePrint = true;
  @Input() toolbarItems = this.defaultToolbarItems;
  @Input() showSpinner = true;
  @Input() fileName = '';
  @Input() fileData = '';
  @Input() eventService: DocEditorEventService | undefined;

  @Output() saved = new EventEmitter<string>();
  @Output() cancelled = new EventEmitter<string>();
  @Output() contentSerialised = new EventEmitter<string>();
  @Output() contentChanged = new EventEmitter();
  @Output() contentHasChanges = new EventEmitter<boolean>();

  @ViewChild('editorcontainer')
  public editorContainer!: DocumentEditorContainerComponent;

  private documentHasChanges = false;
  private documentEditorCreated = false;

  public editorToolbarItems: (ToolbarItem | CustomToolbarItemModel)[] = [];
  private toolbarSaveButtonIndex = 0;
  private toolbarCancelButtonIndex = 1;

  private fileNameNoExt = '';
  private fileFormatType: FormatType | undefined = undefined;

  private eventServiceSubscription: Subscription | undefined = undefined;

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.fileData) {
      this.openDocument();
    }
    if (changes.fileName) {
      this.setFileNameVars();
      this.enableDisableDownloadToolbar();
    }
    if (changes.enableToolbar || changes.enableSave || changes.enableDownload || changes.enablePrint) {
      this.createToolbarMenu();
    }
    if (changes.eventService) {
      this.subscribeToEvents();
    }
  }

  private subscribeToEvents() {
    if (this.eventServiceSubscription !== undefined) {
      this.eventServiceSubscription.unsubscribe();
    }
    this.eventServiceSubscription = this.eventService?.Subscribe((data) => {
      switch (data) {
        case DocumentEditorEvent.SerialiseData:
          this.serialiseDataAndEmit();
          break;
        case DocumentEditorEvent.Download:
          this.download();
          break;
        case DocumentEditorEvent.Print:
          this.print();
          break;
        case DocumentEditorEvent.Cancel:
          this.cancelChanges();
          break;
        case DocumentEditorEvent.Save:
          this.saveDocument();
          break;
        case DocumentEditorEvent.SaveStarted:
          this.disableSaveCancelToolbar();
          this.displaySpinner();
          break;
        case DocumentEditorEvent.SaveSuccessful:
          this.setHasChanges(false);
          this.disableSaveCancelToolbar();
          this.hideSpinner();
          break;
        case DocumentEditorEvent.SaveCancelled:
        case DocumentEditorEvent.SaveErrored:
          this.setHasChanges(true);
          this.enableSaveCancelToolbar();
          this.hideSpinner();
          break;
      }
    });
  }

  public onCreate(): void {
    this.documentEditorCreated = true;
    this.editorContainer.documentEditor.keyDown = this.onDocumentEditorKeyDown;
    this.editorContainer.propertiesPaneContainer.onkeydown = this.onPropertyPaneContainerKeyDown;
    this.editorContainer.toolbarContainer.onkeydown = this.onToolbarContainerKeyDown;
    this.enableDisableDownloadToolbar();
    this.createSpinner();
    this.openDocument();
  }

  public onDocumentChange(): void {
    this.restrictIfMobile();
  }

  public onContentChange(): void {
    this.setHasChanges(true);
    this.enableSaveCancelToolbar();
    if (this.contentChanged) {
      this.contentChanged.emit();
    }
  }

  public onToolbarClick(args: ClickEventArgs): void {
    switch (args.item.id) {
      case 'Save':
        this.saveDocument();
        break;
      case 'Cancel':
        this.cancelChanges();
        break;
      case 'Download':
        this.download();
        break;
      case 'Print':
        this.print();
        break;
    }
  }

  // It has been necessary to add the following onDocumentEditorKeyDown method to prevent the default behavior of
  // the tab key for the syncfusion document editor component to allow accessibility requirements to be fulfilled.
  // The tab key overide is a workaround to allow the user to tab out of the document editor component.
  // It has introduced the limitation of not being able to add tabs to the document text when editing,
  // the same limitation is within the Core product.
  // Ticket #15440 has been raised to consider alternative approaches.

  public onDocumentEditorKeyDown(args: any): void {
    const keyCode = args.event.which || args.event.keyCode;
    if (keyCode === TAB) {
      // tab key, prevent syncfusion behavior and allow the tab
      // to be handled by the browser, e.g. to move focus to the next element
      args.isHandled = true;
    }
  }

  // It has been necessary to add the methods onPropertyPaneContainerKeyDown and onToolbarContainerKeyDown to
  // support the enter key for the syncfusion property pane and toolbar buttons and fulfill accessibility requirements.
  // The following two methods simulate a click on the button to trigger the button action.
  // This is an unpleasant workaround for syncfusion issues which may be fixed in future releases; without this workaround syncfusion reguires Shift+Enter.

  public onPropertyPaneContainerKeyDown(event: any) {
    if (event.keyCode === ENTER) {
      const button = event.target as HTMLButtonElement;
      if (button && button.localName === 'button' && button.ariaLabel !== 'dropdownbutton') {
        // Simulate a click on the button to trigger the button action
        event.preventDefault();
        button.click();
      }
    }
  }

  // Syncfusion does not support actioning of the basic toolbar buttons with Enter key press, dropdown and dialog initiator buttons are supported.
  public onToolbarContainerKeyDown(event: any) {
    if (event.keyCode === ENTER) {
      const button = event.target as HTMLButtonElement;
      if (
        button &&
        button.localName === 'button' &&
        button.ariaLabel !== 'Image' &&
        button.ariaLabel !== 'Break' &&
        button.ariaLabel !== 'Page Setup'
      ) {
        // Simulate a click on the button to trigger the button action
        event.preventDefault();
        button.click();
      }
    }
  }

  private setHasChanges(newVal: boolean) {
    if (this.documentHasChanges !== newVal) {
      this.documentHasChanges = newVal;
      if (this.contentHasChanges) {
        this.contentHasChanges.emit(this.documentHasChanges);
      }
    }
  }

  private openDocument() {
    if (this.documentEditorCreated) {
      this.setHasChanges(false);
      this.disableSaveCancelToolbar();
      if (this.fileDataValid(this.fileData)) {
        this.editorContainer.documentEditor.open(this.fileData);
      } else {
        this.editorContainer.documentEditor.openBlank();
      }
    }
  }

  public saveDocument() {
    if (this.saved && this.documentHasChanges && !this.readOnly) {
      this.saved.emit(this.serialiseData());
    }
  }

  private serialiseData(): string {
    return this.editorContainer.documentEditor.serialize();
  }

  public serialiseDataAndEmit() {
    const serialisedData = this.serialiseData();
    if (this.contentSerialised) {
      this.contentSerialised.emit(serialisedData);
    }
  }

  private cancelChanges() {
    this.openDocument();
    if (this.cancelled) {
      this.cancelled.emit();
    }
  }

  private download() {
    if (this.fileFormatType != undefined) {
      this.editorContainer.documentEditor.save(this.fileNameNoExt, this.fileFormatType);
    }
  }

  private print() {
    this.editorContainer.documentEditor.print();
  }

  private createToolbarMenu() {
    if (this.enableToolbar) {
      const newToolbarItems: (ToolbarItem | CustomToolbarItemModel)[] = [];

      if (this.enableSave) {
        newToolbarItems.push(this.saveToolbarItem);
        newToolbarItems.push(this.cancelToolbarItem);
        if (!this.enableDownload && !this.enablePrint) {
          newToolbarItems.push('Separator');
        }
      }

      if (this.enableDownload) {
        if (this.enableSave) {
          newToolbarItems.push('Separator');
        }
        newToolbarItems.push(this.downloadToolbarItem);
        if (!this.enablePrint) {
          newToolbarItems.push('Separator');
        }
      }

      if (this.enablePrint) {
        if (!this.enableDownload && this.enableSave) {
          newToolbarItems.push('Separator');
        }
        newToolbarItems.push(this.printToolbarItem);
        newToolbarItems.push('Separator');
      }

      if (this.toolbarItems !== undefined) {
        newToolbarItems.push(...this.toolbarItems);
      } else {
        newToolbarItems.push(...this.defaultToolbarItems);
      }

      this.editorToolbarItems = newToolbarItems;

      this.enableDisableDownloadToolbar();
    }
  }

  private enableSaveCancelToolbar() {
    if (
      !this.readOnly &&
      this.enableToolbar &&
      this.enableSave &&
      this.editorContainer?.toolbar.toolbar.items.length > this.toolbarCancelButtonIndex
    ) {
      this.editorContainer.toolbar.enableItems(this.toolbarSaveButtonIndex, true);
      this.editorContainer.toolbar.enableItems(this.toolbarCancelButtonIndex, true);
    }
  }

  private disableSaveCancelToolbar() {
    if (
      this.enableToolbar &&
      this.enableSave &&
      this.editorContainer?.toolbar.toolbar.items.length > this.toolbarCancelButtonIndex
    ) {
      this.editorContainer.toolbar.enableItems(this.toolbarSaveButtonIndex, false);
      this.editorContainer.toolbar.enableItems(this.toolbarCancelButtonIndex, false);
    }
  }

  private enableDisableDownloadToolbar() {
    if (this.enableToolbar && this.enableDownload) {
      const enableDownloadIndex = this.editorContainer?.toolbar.toolbar.items.findIndex(
        (item) => item.id === 'Download'
      );
      if (enableDownloadIndex > 0) {
        this.editorContainer.toolbar.enableItems(
          enableDownloadIndex,
          this.enableDownload && this.fileFormatType !== undefined
        );
      }
    }
  }

  private setFileNameVars() {
    const fileNameSplit = this.fileName?.split('.');
    this.fileNameNoExt = fileNameSplit?.shift() ?? '';
    this.fileFormatType = this.getDownloadFormatType(fileNameSplit?.pop() ?? '');
  }

  private getDownloadFormatType(fileType: string): FormatType | undefined {
    let formatFileType: FormatType | undefined = undefined;
    if (fileType !== undefined && fileType !== '') {
      const lowerCaseStr = fileType.toLocaleLowerCase();
      switch (lowerCaseStr) {
        case 'docx':
          formatFileType = 'Docx';
          break;
        case 'html':
          formatFileType = 'Html';
          break;
        case 'txt':
          formatFileType = 'Txt';
          break;
        case 'sfdt':
          formatFileType = 'Sfdt';
          break;
      }
    }
    return formatFileType;
  }

  public restrictIfMobile(): void {
    // Have to set the document editor to read only on mobile devices due to Syncfusion limitations - see below
    // Syncfusion documention:
    // At present, Document editor component is not responsive for mobile, and we haven’t ensured the
    // editing functionalities in mobile browsers. Whereas it works properly as a document viewer in mobile browsers.
    // Hence, it is recommended to switch the Document editor component as read-only in mobile browsers
    // https://ej2.syncfusion.com/angular/documentation/document-editor/how-to/deploy-document-editor-component-for-mobile

    const isMobileDevice: boolean = /Android|Windows Phone|webOS/i.test(navigator.userAgent);
    if (isMobileDevice) {
      this.editorContainer.restrictEditing = true;
      //this.enableToolbar = false;
      setTimeout(() => {
        this.editorContainer.documentEditor.fitPage('FitPageWidth');
      }, 50);
    }
  }

  private createSpinner() {
    createSpinner({
      target: this.editorContainer.element,
    });
  }

  private hideSpinner() {
    if (this.editorContainer?.element) {
      setTimeout(() => {
        hideSpinner(this.editorContainer.element);
      }, 500);
    }
  }

  private displaySpinner() {
    if (this.showSpinner && this.editorContainer?.element) {
      showSpinner(this.editorContainer.element);
    }
  }

  private fileDataValid(fileData: string): boolean {
    return fileData !== undefined && fileData !== '';
  }
}
