import {
  Component,
  forwardRef,
  Input,
  ViewChild,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  EventEmitter,
  Output,
  AfterViewInit,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import {
  ToolbarService,
  LinkService,
  HtmlEditorService,
  RichTextEditorComponent as SyncFusionRichTextEditorComponent,
  CountService,
  ChangeEventArgs,
  ToolbarType,
} from '@syncfusion/ej2-angular-richtexteditor';

@Component({
  selector: 'ni-rich-text',
  templateUrl: './rich-text.component.html',
  styleUrls: ['./rich-text.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    ToolbarService,
    LinkService,
    HtmlEditorService,
    CountService,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RichTextComponent),
      multi: true,
    },
  ],
})
export class RichTextComponent implements ControlValueAccessor, AfterViewInit {
  @Input() defaultOptions = {
    props: {
      tools: [
        'Formats',
        'FontName',
        'Bold',
        'Italic',
        'Underline',
        'StrikeThrough',
        'CreateLink',
        'Alignments',
        'OrderedList',
        'UnorderedList',
        'Indent',
        'Outdent',
        'ClearFormat',
      ],
    },
  };

  @Input() height = '';
  @Input() width = '';
  @Input() maxWidth = '';
  @Input() required = false;
  @Input() readOnly = false;
  @Input() enableResize = false;
  @Input() showCharCount = false;
  @Input() ariaLabel = '';
  @Input() toolBarType = 'MultiRow';
  @Output() richTextValueUpdated = new EventEmitter<string>();

  public iframe: any = {
    enable: true,
  };

  private _tools?: string[] = [];

  public get tools() {
    return this._tools;
  }

  @Input()
  public set tools(value) {
    if (value && value.length > 0) {
      this._tools = value;
      this.changes.markForCheck();
    }
  }

  @ViewChild('richtexteditor') public richtexteditor!: SyncFusionRichTextEditorComponent;

  constructor(private changes: ChangeDetectorRef) {
    this.tools = this.defaultOptions.props.tools;
  }

  ngAfterViewInit(): void {
    if (this.richtexteditor) {
      this.richtexteditor.toolbarSettings.type =
        this.toolBarType === 'Expand' ? ToolbarType.Expand : ToolbarType.MultiRow;
    }
  }

  private onChange!: (value: any) => void;
  private onTouched!: () => void;
  private value?: string;

  writeValue(value: string): void {
    if (this.richtexteditor) {
      this.richtexteditor.value = value;
    }
    this.value = value;
    this.changes.markForCheck();
  }

  onChanged(value: ChangeEventArgs) {
    this.setEditor(value.value);
  }

  setEditor(value: string) {
    this.value = value;
    this.onChange(value);
    this.onTouched();
    this.richTextValueUpdated.emit(value);
    this.changes.markForCheck();
  }

  onBlur() {
    this.setEditor(this.richtexteditor.value);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  onCreation() {
    this.richtexteditor.value = this.value || '';
  }
}
