import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { FileService } from '../../services';

@UntilDestroy()
@Component({
  selector: 'ni-downloader',
  templateUrl: './downloader.component.html',
  styleUrls: ['./downloader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DownloaderComponent {
  @Input() attachment: any;

  constructor(private fileService: FileService) {}

  download(): void {
    this.fileService.downloadAttachmentById(this.attachment.attachmentId);
  }
}
