import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SiteConfig } from '../../models';
import { BlobService } from './../../services/blob.service';
import { MockBlobService } from './../../services/blob.service.spec';
import { DownloaderComponent } from './downloader.component';

describe('DownloaderComponent', () => {
  let component: DownloaderComponent;
  let fixture: ComponentFixture<DownloaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DownloaderComponent],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: BlobService, useClass: MockBlobService },
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloaderComponent);
    component = fixture.componentInstance;
    component.attachment = {};

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
