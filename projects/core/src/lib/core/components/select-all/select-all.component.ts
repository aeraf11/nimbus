import { Component, ChangeDetectionStrategy, ViewEncapsulation, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'ni-select-all',
  templateUrl: './select-all.component.html',
  styleUrls: ['./select-all.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class SelectAllComponent {
  @Input() isAllSelected = false;

  @Output() selectChanged = new EventEmitter<void>();

  onChange(): void {
    this.selectChanged.emit();
  }
}
