import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { Time } from '@nimbus/material/lib/core/date-time-adapter';

@Component({
  selector: 'ni-job-time',
  templateUrl: './job-time.component.html',
  styleUrls: ['./job-time.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobTimeComponent {
  @Input() formControl: any;
  @Input() showSpinners: boolean = false;
  @Input() stepHours: number = 1;
  @Input() stepMinutes: number = 1;
  @Input() stepSeconds: number = 1;
  @Input() showSeconds: boolean = false;
  @Input() disableMinutes: boolean = false;
  @Input() showMeridian: boolean = false;
  @Input() color: ThemePalette = 'primary';
  @Input() defaultTime!: Time;
  @Input() required: boolean = false;
  @Input() jobId?: string;
}
