import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IdentificationComponent } from './identification.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SiteConfig } from '../../models/site-config';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { StateService } from '../../services';
import { MatDialogModule } from '@angular/material/dialog';
import { DataCardsComponent } from '../data-cards/data-cards.component';

describe('IdentificationComponent', () => {
  let component: IdentificationComponent;
  let fixture: ComponentFixture<IdentificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IdentificationComponent],
      imports: [HttpClientTestingModule, MatDialogModule],
      providers: [
        DataCardsComponent,
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
        { provide: StateService, useClass: MockStateService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentificationComponent);
    component = fixture.componentInstance;
    component.item = {
      actionRequired: false,
      defaultEntityMediaBlobId: '',
      defaultPhotoThumbnailBlobId: '',
      hasAttachments: true,
      identId: '',
      isDisabled: true,
      isEditable: true,
      nameOfIndividual: '',
      reference: '',
      unidentifiedSubjectId: '',
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
