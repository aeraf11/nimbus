import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DialogFormComponent } from '@nimbus/core/src/lib/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FileService } from '../../services';
import { DialogFormData, EditorMode } from '@nimbus/core/src/lib/core';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { DataCardsItemBaseComponent } from '../data-cards-item-base/data-cards-item-base.component';
import { DataCardsComponent } from '../data-cards/data-cards.component';

@Component({
  selector: 'ni-identification',
  templateUrl: './identification.component.html',
  styleUrls: ['./identification.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => IdentificationComponent),
      multi: true,
    },
  ],
})
export class IdentificationComponent extends DataCardsItemBaseComponent implements ControlValueAccessor {
  @Input() formDefinition: FormlyFieldConfig[] | undefined | null;
  @Input() options: FormlyFormOptions | undefined;
  @Input() downloadText = 'Download';
  @Input() cancelText = 'Cancel';
  @Input() dialogWidth?: string = undefined;
  @Input() viewType = 'grid';

  photoEndpoint = '/api/v2/media/photo?id=';
  downloadButtonText = 'DOWNLOAD ID STATEMENT';
  actionIsRequired = true;

  constructor(
    public override parent: DataCardsComponent,
    private change: ChangeDetectorRef,
    public dialog: MatDialog,
    public fileService: FileService
  ) {
    super(parent, change);
  }

  getFormDefinition(identId: string): FormlyFieldConfig[] | undefined | null {
    return [
      {
        key: 'attachmentAction',
        type: 'datatable',
        props: {
          actions: [
            {
              name: 'Download',
              download: {
                attachmentId: '${attachmentId}',
                logAction: { entityId: identId, entityTypeId: 121, action: 'IdentStatement' },
              },
            },
          ],
          filterColumns: [
            {
              column: '',
              stringFilter: identId,
              entityTypeId: 121,
            },
          ],
          label: '',
          savedOptionsId: 'submissions',
          listName: 'Attachments',
          selectMode: 'none',
          sortColumn: 'creationDate',
          columnDefinitions: [
            {
              id: 'attachmentId',
              isHidden: true,
            },
            {
              id: 'fileName',
              value: 'File Name',
            },
            {
              id: 'description',
              value: 'Description',
            },
            {
              id: 'createdByFullName',
              value: 'Created By',
            },
            {
              id: 'creationDate',
              dataType: 'date',
              value: 'Creation Date',
            },
          ],
          idColumn: 'attachmentId',
          showPaging: true,
          showQuickSearch: true,
          filterTitle: 'Filter Attachments',
          filterText: 'Filter',
          cancelText: 'Cancel',
          clearText: 'Clear',
          enableLoadingIndicator: true,
          pageSize: 10,
        },
      },
    ];
  }

  openDialog(item: UserIdent): void {
    this.change.markForCheck();
    const { downloadText, cancelText } = this;

    const data: DialogFormData = {
      model: {},
      fieldConfig: this.getFormDefinition(item.identId),
      config: {
        saveText: downloadText,
        cancelText,
        enableDelete: false,
        enableEdit: false,
        isChildForm: true,
        title: 'Attachments',
      },
      editorMode: EditorMode.Select,
      disabled: false,
      options: this.options,
    };
    const dialogConfig: MatDialogConfig<DialogFormData> = { data };
    if (this.dialogWidth) {
      dialogConfig.width = this.dialogWidth;
      dialogConfig.minWidth = '';
      dialogConfig.maxWidth = '';
    }
    const dialogRef = this.dialog.open(DialogFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(() => {
      this.parent.getItems();
    });
  }
}

export interface UserIdent {
  actionRequired: boolean;
  defaultEntityMediaBlobId?: string;
  defaultPhotoThumbnailBlobId?: string;
  hasAttachments: boolean;
  identId: string;
  isDisabled?: boolean;
  isEditable?: boolean;
  nameOfIndividual?: string;
  reference?: string;
  unidentifiedSubjectId: string;
}
