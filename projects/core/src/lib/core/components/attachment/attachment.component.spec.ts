import { SiteConfig } from '@nimbus/core/src/lib/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DownloaderComponent } from '../../../core';
import { ProfilePictureComponent } from './../../../core/components/profile-picture/profile-picture.component';
import { BlobService } from './../../../core/services/blob.service';
import { MockBlobService } from './../../../core/services/blob.service.spec';
import { AttachmentComponent } from './attachment.component';

describe('AttachmentComponent', () => {
  let component: AttachmentComponent;
  let fixture: ComponentFixture<AttachmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AttachmentComponent, ProfilePictureComponent, DownloaderComponent],
      imports: [HttpClientTestingModule],
      providers: [
        { provide: BlobService, useClass: MockBlobService },
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentComponent);
    component = fixture.componentInstance;
    component.attachment = {};

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
