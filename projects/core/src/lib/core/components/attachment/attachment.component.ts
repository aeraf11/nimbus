import { Component, ChangeDetectionStrategy, Input, OnChanges } from '@angular/core';
import { BlobService } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ni-attachment',
  templateUrl: './attachment.component.html',
  styleUrls: ['./attachment.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AttachmentComponent implements OnChanges {
  @Input() attachment: any;
  relatedEntities = '';

  constructor(private blobService: BlobService) {}

  ngOnChanges(): void {
    this.relatedEntities = this.getRelatedEntities();
  }

  private getRelatedEntities(): string {
    if (this.attachment.relatesToJson) {
      const unescapedJson = this.attachment.relatesToJson.replace(/\"/g, '"');
      return (
        JSON.parse(unescapedJson)
          ?.map((related: any) => this.getDisplayValue(related.RelatesTo))
          ?.join(', ') ?? ''
      );
    }
    return '';
  }

  private getDisplayValue(input: string): string {
    return input?.split('>')?.[1]?.trim() ?? '';
  }
}
