import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';
import { MetaDataItem } from '../../../models';
import { MetaDataTextAreaComponent } from './meta-data-text-area.component';

describe('MetaDataTextAreaComponent', () => {
  let component: MetaDataTextAreaComponent;
  let fixture: ComponentFixture<MetaDataTextAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataTextAreaComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataTextAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should provide the same string value as the item value ', () => {
    // Arrange
    const item = <MetaDataItem>{ Label: 'Test', Value: 'this is a test string' };
    component.metaDataItemControl = <FormControl<MetaDataItem>>new FormControl<MetaDataItem>(item);

    // Assert
    expect(component.value).toBe('this is a test string');
  });

  it('should update the item value when the control value changes', () => {
    // Arrange
    const item = <MetaDataItem>{ Label: 'Test', Value: 'this is a test string' };
    component.metaDataItemControl = <FormControl<MetaDataItem>>new FormControl<MetaDataItem>(item);

    // Act
    const newControlValue = 'new value';
    component.metaDataItemControl.setValue({ ...item, Value: newControlValue });

    // Assert
    expect(component.metaDataItemControl.value.Value).toBe(newControlValue);
  });
});
