import { MetaDataBaseComponent } from './../meta-data-base/meta-data-base.component';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { MatFormFieldAppearance } from '@angular/material/form-field';

@Component({
  selector: 'ni-meta-data-text-area',
  templateUrl: './meta-data-text-area.component.html',
  styleUrls: ['./meta-data-text-area.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataTextAreaComponent extends MetaDataBaseComponent implements OnInit {
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() ariaLabel?: string;
  @Input() autoComplete?: string;
  @Input() autosize = false;
  @Input() autosizeMaxRows?: number;
  @Input() autosizeMinRows?: number;
  @Input() cols?: number;
  @Input() name = '';
  @Input() placeholder = '';
  @Input() rows?: number;
  @Input() type = 'text';

  constructor(public override change: ChangeDetectorRef) {
    super(change);
  }

  override ngOnInit() {
    super.ngOnInit();

    if (this.metaDataItemControl && this.ariaLabel === '') {
      this.ariaLabel = this.metaDataItemControl.value.Label ?? '';
      if (
        this.metaDataItemControl.value.Instructions &&
        this.metaDataItemControl.value.Instructions !== '' &&
        !this.readonly
      ) {
        this.ariaLabel += ' ' + this.metaDataItemControl.value.Instructions;
      }
    }
  }
}
