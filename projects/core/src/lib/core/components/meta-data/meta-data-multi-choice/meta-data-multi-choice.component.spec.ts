import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetaDataMultiChoiceComponent } from './meta-data-multi-choice.component';

describe('MetaDataMultiChoiceComponent', () => {
  let component: MetaDataMultiChoiceComponent;
  let fixture: ComponentFixture<MetaDataMultiChoiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataMultiChoiceComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataMultiChoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
