import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Lookup } from './../../../models';
import { MetaDataBaseComponent } from './../meta-data-base/meta-data-base.component';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { AbstractControl, ValidationErrors } from '@angular/forms';

@UntilDestroy()
@Component({
  selector: 'ni-meta-data-multi-choice',
  templateUrl: './meta-data-multi-choice.component.html',
  styleUrls: ['./meta-data-multi-choice.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataMultiChoiceComponent extends MetaDataBaseComponent implements OnInit {
  @Input() addCustom = false;
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() ariaLabel = '';
  @Input() enableTree: boolean | 'auto' = 'auto';
  @Input() newItemLabel = 'Add new item...';
  @Input() placeholder = '';
  @Input() removeable = true;
  @Input() selectable = true;

  constructor(public override change: ChangeDetectorRef) {
    super(change);
  }

  lookups: Lookup[] = [];

  override ngOnInit(): void {
    super.ngOnInit();
    this.lookups =
      this.metaDataItemControl?.value?.Choices?.map((choice) => {
        return {
          displayValue: choice.Label || '',
          lookupId: choice.Value || '',
          hierarchicalDisplayValue: choice.Label || '',
          level: 0,
          lookupKey: '',
        };
      }) || [];

    if (this.metaDataItemControl && this.ariaLabel === '') {
      this.ariaLabel = this.metaDataItemControl.value.Label ?? '';
      if (
        this.metaDataItemControl.value.Instructions &&
        this.metaDataItemControl.value.Instructions !== '' &&
        !this.readonly
      ) {
        this.ariaLabel += ' ' + this.metaDataItemControl.value.Instructions;
      }
    }

    this.localControl?.valueChanges?.pipe(untilDestroyed(this)).subscribe((value) => this.onChange(value));
  }

  override get value(): any {
    return this.metaDataItemControl?.value?.Value?.split('¬');
  }

  override onChange(value: string[]) {
    this.markAsTouched();
    this.updateItem(value.join('¬'));
  }

  override requiredValidator(control: AbstractControl<any, any>): ValidationErrors | null {
    if (control.value === undefined || control.value === null || control.value.length === 0) {
      return { customRequired: 'This field is required' };
    }
    return null;
  }
}
