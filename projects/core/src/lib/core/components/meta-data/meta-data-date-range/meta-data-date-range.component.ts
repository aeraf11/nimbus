import { ChangeDetectionStrategy, Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { MetaDataBaseComponent } from '../meta-data-base/meta-data-base.component';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AppDateTimeAdapter } from '../../../services';

@UntilDestroy()
@Component({
  selector: 'ni-meta-data-date-range',
  templateUrl: './meta-data-date-range.component.html',
  styleUrls: ['./meta-data-date-range.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataDateRangeComponent extends MetaDataBaseComponent implements OnInit {
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() formControl = new UntypedFormGroup({});
  @Input() disabled = false;
  @Input() field!: FormlyFieldConfig;
  @Input() ariaLabel?: string;
  parsedDate: any;

  constructor(change: ChangeDetectorRef, private dateTimeAdapter: AppDateTimeAdapter) {
    super(change);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    if (this.localControl) {
      this.localControl.valueChanges.pipe(untilDestroyed(this)).subscribe((value) => {
        this.onChange(value);
      });
    }

    if (this.metaDataItemControl && (this.ariaLabel === '' || !this.ariaLabel)) {
      this.ariaLabel = this.metaDataItemControl.value.Label ?? '';
      if (
        this.metaDataItemControl.value.Instructions &&
        this.metaDataItemControl.value.Instructions !== '' &&
        !this.readonly
      ) {
        this.ariaLabel += ' ' + this.metaDataItemControl.value.Instructions;
      }
    }
  }

  parseMetaDataDate(): string {
    const parseValue = this.metaDataItemControl?.value?.Value;
    this.parsedDate = typeof parseValue === 'string' ? JSON.parse(parseValue) : parseValue;
    return this.parsedDate;
  }

  override get value(): any {
    this.parseMetaDataDate();
    return this.dateTimeAdapter.convertFromCoreDateRange(this.parsedDate);
  }

  override onChange(value: any) {
    const dateRange = this.dateTimeAdapter.convertToCoreDateRange(value);
    this.updateItem(JSON.stringify({ ...dateRange, TimeZoneId: dateRange?.TimeZoneId ?? this.parsedDate?.TimeZoneId }));
  }
}
