import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MetaDataDateRangeComponent } from './meta-data-date-range.component';
import { StateService } from '../../../services';
import { MockStateService } from '@nimbus/core/src/lib/core';

describe('MetaDataDateRangeComponent', () => {
  let component: MetaDataDateRangeComponent;
  let fixture: ComponentFixture<MetaDataDateRangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataDateRangeComponent],
      providers: [{ provide: StateService, useClass: MockStateService }],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataDateRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
