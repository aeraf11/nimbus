import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MetaDataTextPanelComponent } from './meta-data-text-panel.component';

describe('MetaDataTextPanelComponent', () => {
  let component: MetaDataTextPanelComponent;
  let fixture: ComponentFixture<MetaDataTextPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataTextPanelComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataTextPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
