import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { MetaDataBaseComponent } from '../meta-data-base/meta-data-base.component';

@Component({
  selector: 'ni-meta-data-text-panel',
  templateUrl: './meta-data-text-panel.component.html',
  styleUrls: ['./meta-data-text-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataTextPanelComponent extends MetaDataBaseComponent implements OnInit {
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() ariaLabel?: string = '';
  @Input() textControl = new UntypedFormControl();
  metaDataCssClass: string | null | undefined;

  override ngOnInit(): void {
    super.ngOnInit();
    this.metaDataCssClass = this.metaDataItemControl?.value?.CssClass;

    if (this.metaDataItemControl && this.ariaLabel === '') {
      this.ariaLabel = this.metaDataItemControl.value.Label ?? '';
      if (
        this.metaDataItemControl.value.Instructions &&
        this.metaDataItemControl.value.Instructions !== '' &&
        !this.readonly
      ) {
        this.ariaLabel += ' ' + this.metaDataItemControl.value.Instructions;
      }
    }
  }
}
