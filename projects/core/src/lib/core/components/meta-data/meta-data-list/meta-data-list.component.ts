import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input } from '@angular/core';
import { FormArray, FormControl } from '@angular/forms';
import { OnChange } from 'property-watch-decorator';
import { MetaDataItem, MetaDataItemType } from '../../../models';
import { UngroupedMetaData, DefaultUngroupedMetaData } from './../../../models/meta-data-item';

@Component({
  selector: 'ni-meta-data-list',
  templateUrl: './meta-data-list.component.html',
  styleUrls: ['./meta-data-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataListComponent {
  @OnChange<FormArray<FormControl<MetaDataItem>>>(function (this: MetaDataListComponent, value) {
    if (value && this.metaData.items.length > 0) {
      this.groups = this.placeMetaDataItemsIntoGroups();
      this.change.markForCheck();
    }
  })
  @Input()
  formArray?: FormArray<FormControl<MetaDataItem>> | null;

  @OnChange<UngroupedMetaData>(function (this: MetaDataListComponent, value) {
    if (value.buildGroups && value.items.length > 0 && this.formArray) {
      this.groups = this.placeMetaDataItemsIntoGroups();
      this.change.markForCheck();
    }
  })
  @Input()
  metaData: UngroupedMetaData = DefaultUngroupedMetaData;

  @Input() formModel?: unknown;

  @Input() readonly = false;

  @Input() containerType: 'card' | 'panel' | 'none' = 'card';

  @Input() ungroupedTitle?: string;

  groups: { name?: string; items: FormControl<MetaDataItem>[] }[] = [];

  constructor(private change: ChangeDetectorRef) {}

  placeMetaDataItemsIntoGroups(): { name?: string; items: FormControl<MetaDataItem>[] }[] {
    const groupItems = this.metaData.items.filter((item) => item.ItemType === MetaDataItemType.DisplayGroup);
    const groupNames = groupItems.map((group) => group.Label);
    const groups = groupItems
      .map((group) => {
        return {
          name: group.Label,
          items: this.metaData.items
            .filter((f) => f.DisplayGroupName === group.Label)
            .map((item) => this.getFormControlByLabel(item.Label)),
        };
      })
      .filter((group) => group.items.length > 0);

    const ungroupedControls = this.metaData.items
      .filter((item) => item.ItemType !== MetaDataItemType.DisplayGroup && !this.hasGroup(item, groupNames))
      .map((item) => this.getFormControlByLabel(item.Label));

    const ungroup = {
      name: '',
      items: ungroupedControls,
    };

    return [ungroup, ...groups];
  }

  hasGroup(item: MetaDataItem, groupNames: string[]): boolean {
    const groupName = item.DisplayGroupName ?? null;
    if (!groupName) return false;
    return groupNames.includes(groupName);
  }

  getFormControlByLabel(label: string): FormControl<MetaDataItem> {
    if (this.formArray) {
      return this.formArray.controls.find((control) => control.value.Label === label) ?? new FormControl();
    }
    return new FormControl();
  }
}
