import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MetaDataItem, MetaDataItemType } from '../../../models';

@Component({
  selector: 'ni-meta-data-item',
  templateUrl: './meta-data-item.component.html',
  styleUrls: ['./meta-data-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataItemComponent {
  @Input() metaDataItemControl?: FormControl<MetaDataItem>;
  @Input() metaData?: MetaDataItem[];
  @Input() readonly = false;
  @Input() index = -1;
  @Input() model?: unknown;

  ItemType = MetaDataItemType;
  yesNoChoices = [
    { Value: 'Yes', Label: 'Yes', ValueData: null },
    { Value: 'No', Label: 'No', ValueData: null },
  ];
}
