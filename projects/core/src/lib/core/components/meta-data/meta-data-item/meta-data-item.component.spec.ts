import { ChangeDetectionStrategy } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { MetaDataItem } from '@nimbus/core/src/public-api';
import { MetaDataItemType } from '../../../models';
import { MetaDataItemComponent } from './meta-data-item.component';

describe('MetaDataItemComponent', () => {
  let component: MetaDataItemComponent;
  let fixture: ComponentFixture<MetaDataItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataItemComponent],
    })
      .overrideComponent(MetaDataItemComponent, { set: { changeDetection: ChangeDetectionStrategy.Default } })
      .compileComponents();

    fixture = TestBed.createComponent(MetaDataItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.readonly = false;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('template', () => {
    it('renders a ni-meta-data-input if the ItemType property is Text', () => {
      // Arrange
      const item = <MetaDataItem>{ Label: 'Test', ItemType: MetaDataItemType.Text };
      component.metaDataItemControl = <FormControl<MetaDataItem>>new FormControl<MetaDataItem>(item);

      // Act
      // component.ngOnInit();
      fixture.detectChanges();

      // Assert
      expect(fixture.debugElement.query(By.css('ni-meta-data-input'))).toBeTruthy();
    });

    it('renders a ni-meta-data-checkbox if the ItemType property is Checkbox', () => {
      // Arrange
      const item = <MetaDataItem>{ Label: 'Test', ItemType: MetaDataItemType.Checkbox };
      component.metaDataItemControl = <FormControl<MetaDataItem>>new FormControl<MetaDataItem>(item);

      // Act
      // component.ngOnInit();
      fixture.detectChanges();

      // Assert
      expect(fixture.debugElement.query(By.css('ni-meta-data-checkbox'))).toBeTruthy();
    });
  });
});
