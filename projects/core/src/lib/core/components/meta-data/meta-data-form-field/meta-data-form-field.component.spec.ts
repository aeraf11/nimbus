import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetaDataFormFieldComponent } from './meta-data-form-field.component';

describe('MetaDataFormFieldComponent', () => {
  let component: MetaDataFormFieldComponent;
  let fixture: ComponentFixture<MetaDataFormFieldComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataFormFieldComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataFormFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
