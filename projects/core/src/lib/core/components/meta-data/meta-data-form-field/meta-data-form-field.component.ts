import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ChangeDetectionStrategy, Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { of, Observable } from 'rxjs';
import { MediaService } from '../../../services';

@UntilDestroy()
@Component({
  selector: 'ni-meta-data-form-field',
  templateUrl: './meta-data-form-field.component.html',
  styleUrls: ['./meta-data-form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataFormFieldComponent implements OnInit {
  @Input() instructions: string | null | undefined = null;
  @Input() showInstructions?: boolean;
  @Input() control!: FormControl;
  @Input() formFieldLeftWidths = '100%,33.33%';
  @Input() formFieldMiddleWidths = '100%,50%';
  @Input() formFieldRightWidths = '100%,16.66%';
  @Input() formFieldMarginTop = '1.5rem';
  @Input() hideLabel = false;
  @Input() hideRequiredMarker = false;
  @Input() itemTouched!: Observable<void>;
  @Input() fieldId?: string;
  @Input() label?: string;
  @Input() required = false;
  @Input() validPreTouch = false;

  leftFlex$ = of('');
  middleFlex$ = of('');
  rightFlex$ = of('');

  constructor(private mediaService: MediaService, private change: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.leftFlex$ = this.mediaService.getBreakpointValue(this.formFieldLeftWidths);
    this.middleFlex$ = this.mediaService.getBreakpointValue(this.formFieldMiddleWidths);
    this.rightFlex$ = this.mediaService.getBreakpointValue(this.formFieldRightWidths);

    if (this.control) {
      this.control.valueChanges.pipe(untilDestroyed(this)).subscribe(() => this.change.markForCheck());
      if (this.itemTouched) {
        this.itemTouched.pipe(untilDestroyed(this)).subscribe(() => this.change.markForCheck());
      } else {
        console.warn(
          'You must wire up itemTouched in the metadata item control html template, otherwise the form field will not render errors. This is the offending control:',
          this.control
        );
      }
    }
  }

  get errors(): string[] {
    const errors = <any>this.control.errors;
    return Object.keys(errors)
      .filter((key) => key !== 'required')
      .map((key) => errors[key]);
  }
}
