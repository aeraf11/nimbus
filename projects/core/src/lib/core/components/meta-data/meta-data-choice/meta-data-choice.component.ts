import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { MetaDataBaseComponent } from '../meta-data-base/meta-data-base.component';

@Component({
  selector: 'ni-meta-data-choice',
  templateUrl: './meta-data-choice.component.html',
  styleUrls: ['./meta-data-choice.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataChoiceComponent extends MetaDataBaseComponent implements OnInit {
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() ariaLabel?: string;
  @Input() placeholder = '';

  constructor(public override change: ChangeDetectorRef) {
    super(change);
  }

  override ngOnInit(): void {
    super.ngOnInit();

    if (this.metaDataItemControl && this.ariaLabel === '') {
      this.ariaLabel = this.metaDataItemControl.value.Label ?? '';
      if (
        this.metaDataItemControl.value.Instructions &&
        this.metaDataItemControl.value.Instructions !== '' &&
        !this.readonly
      ) {
        this.ariaLabel += ' ' + this.metaDataItemControl.value.Instructions;
      }
    }
  }
}
