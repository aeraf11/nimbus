import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MetaDataItem } from '../../../models';
import { FormControl } from '@angular/forms';
import { MetaDataChoiceComponent } from './meta-data-choice.component';

describe('MetaDataChoiceComponent', () => {
  let component: MetaDataChoiceComponent;
  let fixture: ComponentFixture<MetaDataChoiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataChoiceComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataChoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should provide null when the item value is null', () => {
    // Arrange
    const item = <MetaDataItem>{ Label: 'Test', Value: null };
    component.metaDataItemControl = <FormControl<MetaDataItem>>new FormControl<MetaDataItem>(item);

    // Assert
    expect(component.value).toBe(null);
  });

  it('should provide a valid value when the item value is valid', () => {
    // Arrange
    const item = <MetaDataItem>{ Label: 'Test', Value: 'SomeValidValue' };
    component.metaDataItemControl = <FormControl<MetaDataItem>>new FormControl<MetaDataItem>(item);

    // Assert
    expect(component.value).toBe('SomeValidValue');
  });

  //Can't get this click to work, will need fixing later on.
  xit('should trigger click event', () => {
    // Arrange
    const item = <MetaDataItem>{ Label: 'Test', Value: 'SomeValidValue' };
    component.metaDataItemControl = <FormControl<MetaDataItem>>new FormControl<MetaDataItem>(item);
    const option = fixture.nativeElement.querySelector('mat-option');
    spyOn(component, 'onChange');

    // Act
    option.click();

    // Assert
    expect(component.onChange).toHaveBeenCalled();
  });
});
