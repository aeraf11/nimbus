import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
} from '@angular/core';
import { MetaDataBaseComponent } from '../meta-data-base/meta-data-base.component';
import { MatFormFieldAppearance } from '@angular/material/form-field';

@Component({
  selector: 'ni-meta-data-input',
  templateUrl: './meta-data-input.component.html',
  styleUrls: ['./meta-data-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataInputComponent extends MetaDataBaseComponent implements OnInit {
  @Input() ariaLabel?: string;
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() autoComplete?: string;
  @Input() name = '';
  @Input() placeholder = '';
  @Input() type = 'text';

  @ViewChild('input') input?: ElementRef<HTMLInputElement>;

  constructor(public override change: ChangeDetectorRef) {
    super(change);
  }

  override ngOnInit(): void {
    super.ngOnInit();
  }

  onBlur() {
    this.markAsTouched();
  }

  get ariaLabelInternal(): string {
    return `${this.ariaLabel ?? ''} ${this.metaDataItemControl?.value?.Label ?? ''} ${
      !this.readonly ? this.metaDataItemControl?.value?.Instructions ?? '' : ''
    }`.trim();
  }
}
