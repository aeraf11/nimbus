import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';
import { MetaDataItem } from '../../../models';
import { MetaDataInputComponent } from './meta-data-input.component';

describe('MetaDataInputTypeComponent', () => {
  let component: MetaDataInputComponent;
  let fixture: ComponentFixture<MetaDataInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataInputComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.readonly = false;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('value', () => {
    it('should provide the same string value as the item value ', () => {
      // Arrange
      const item = <MetaDataItem>{ Label: 'Test', Value: 'this is a test string' };
      component.metaDataItemControl = <FormControl<MetaDataItem>>new FormControl<MetaDataItem>(item);
      // Act

      // Assert
      expect(component.value).toBe('this is a test string');
    });
  });
});
