import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { take } from 'rxjs';
import { StorageType } from '../../../models/storage-type';
import { StateService } from '../../../services/state.service';
import { MetaDataBaseComponent } from './../meta-data-base/meta-data-base.component';

interface ValueDataItem {
  EntityMediaId?: any;
  Description: string;
  BlobId?: string;
  ThumbnailBlobId: string;
  WebsizeBlobId: string;
  Height?: number;
  Width?: number;
  IgnoreLoadMetaDataFromImage?: boolean;
}

@UntilDestroy()
@Component({
  selector: 'ni-meta-data-media',
  templateUrl: './meta-data-media.component.html',
  styleUrls: ['./meta-data-media.component.scss'],
})
export class MetaDataMediaComponent extends MetaDataBaseComponent implements OnInit {
  ariaLabel?: string;
  removePath = 'api/v2/media/photo/delete';
  savePath = 'api/v2/media/photo';

  settings = {
    autoUpload: true,
    asyncSettings: {
      chunkSize: 10485760,
      removeUrl: this.removePath,
      retryAfterDelay: 3000,
      retryCount: 1,
      saveUrl: this.savePath,
    },
    maxFileSize: 9007199254740991,
    multiple: true,
  };

  storageType: StorageType = StorageType.Media;
  includeThumbnail = true;
  createWebImage = true;

  constructor(public override change: ChangeDetectorRef, private state: StateService) {
    super(change);

    //Use config settings if possible, but need to overwrite the default save and delete urls.
    this.state
      .selectFileUploadConfig()
      .pipe(take(1))
      .subscribe((config) => {
        if (config) {
          this.settings = {
            autoUpload: config.autoUpload,
            asyncSettings: {
              ...config.asyncSettings,
              removeUrl: this.removePath,
              saveUrl: this.savePath,
            },
            maxFileSize: config.maxFileSize,
            multiple: config.multiple,
          };
        } else {
          console.warn('No configuration for meta data media found. Using defaults.');
        }
      });
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.localControl?.valueChanges?.pipe(untilDestroyed(this)).subscribe((value) => {
      this.onChange(value);
    });

    if (this.metaDataItemControl) {
      this.ariaLabel = this.metaDataItemControl.value.Label ?? '';
      if (
        this.metaDataItemControl.value.Instructions &&
        this.metaDataItemControl.value.Instructions !== '' &&
        !this.readonly
      ) {
        this.ariaLabel += ' ' + this.metaDataItemControl.value.Instructions;
      }
    }
  }

  override get value(): any {
    const valueData = this.metaDataItemControl?.value.ValueData;
    if (valueData) {
      const parsedData = JSON.parse(valueData);
      return parsedData.map((item: ValueDataItem) => {
        return {
          name: item.Description,
          fileId: item.BlobId,
          width: item.Width,
          height: item.Height,
          thumbnailId: item.ThumbnailBlobId,
          websizeId: item.WebsizeBlobId,
        };
      });
    }
    return null;
  }

  override onChange(value: any): void {
    const mappedItems = value.map((item: any) => {
      return {
        EntityMediaId: null,
        Description: item.name,
        BlobId: item.blobId,
        Width: item.width,
        Height: item.height,
        ThumbnailBlobId: item.thumbnailId,
        WebsizeBlobId: item.websizeId,
        IgnoreLoadMetaDataFromImage: item.ignoreLoadMetaDataFromImage,
      };
    });
    const stringItems = JSON.stringify(mappedItems);
    this.markAsTouched();
    this.updateItem(stringItems);
  }

  override updateItem(value: any): void {
    if (this.metaDataItemControl) {
      this.metaDataItemControl.setValue({ ...this.metaDataItemControl.value, ValueData: value });
    }
  }
}
