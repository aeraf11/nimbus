import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StateService } from '@nimbus/core/src/lib/core';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { MetaDataMediaComponent } from './meta-data-media.component';

describe('MetaDataMediaComponent', () => {
  let component: MetaDataMediaComponent;
  let fixture: ComponentFixture<MetaDataMediaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataMediaComponent],
      providers: [{ provide: StateService, useClass: MockStateService }],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataMediaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
