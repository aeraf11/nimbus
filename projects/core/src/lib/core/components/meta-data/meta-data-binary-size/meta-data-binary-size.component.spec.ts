import { MetaDataItem } from '@nimbus/core/src/lib/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetaDataBinarySizeComponent } from './meta-data-binary-size.component';
import { FormControl } from '@angular/forms';

describe('MetaDataBinarySizeComponent', () => {
  let component: MetaDataBinarySizeComponent;
  let fixture: ComponentFixture<MetaDataBinarySizeComponent>;
  const baseItem = <MetaDataItem>{ Label: 'Test', ValueData: '{"EnteredValue": "", "Unit": "MB", "FloatValue": ""}' };
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataBinarySizeComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MetaDataBinarySizeComponent);
    component = fixture.componentInstance;
    component.localControl = new FormControl();
    component.metaDataItemControl = <FormControl<MetaDataItem>>new FormControl<MetaDataItem>(baseItem);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return the parsed MetaData ValueData', () => {
    const parsedValue = {
      EnteredValue: '',
      Unit: 'MB',
      FloatValue: '',
    };
    expect(component.value).toEqual(parsedValue);
  });

  it('should update the MetaData value when the control value is updated', () => {
    const newValue = '{"EnteredValue": "1000", "Unit": "MB", "FloatValue": ""}';
    component.metaDataItemControl?.setValue({ ...baseItem, ValueData: newValue });

    expect(component.metaDataItemControl?.value.ValueData).toEqual(newValue);
  });
});
