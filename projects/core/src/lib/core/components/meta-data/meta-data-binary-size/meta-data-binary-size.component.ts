import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { MetaDataBaseComponent } from '../meta-data-base/meta-data-base.component';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'ni-meta-data-binary-size',
  templateUrl: './meta-data-binary-size.component.html',
  styleUrls: ['./meta-data-binary-size.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataBinarySizeComponent extends MetaDataBaseComponent implements OnInit {
  @Input() ariaLabel?: string;

  override ngOnInit(): void {
    super.ngOnInit();

    if (this.metaDataItemControl && this.ariaLabel === '') {
      this.ariaLabel = this.metaDataItemControl.value.Label ?? '';
      if (
        this.metaDataItemControl.value.Instructions &&
        this.metaDataItemControl.value.Instructions !== '' &&
        !this.readonly
      ) {
        this.ariaLabel += ' ' + this.metaDataItemControl.value.Instructions;
      }
    }

    this.localControl.valueChanges.pipe(untilDestroyed(this)).subscribe((val: any) => {
      this.onChange(val);
    });
  }

  override get value(): any {
    const valueData = this.metaDataItemControl?.value?.ValueData;
    if (valueData) {
      return JSON.parse(valueData);
    }
    return null;
  }

  override onChange(value: any) {
    const formValue = value !== null && value !== undefined ? JSON.stringify(value) : null;
    this.updateItem(formValue);
  }

  override updateItem(value: any): void {
    if (this.metaDataItemControl) {
      this.metaDataItemControl.setValue({ ...this.metaDataItemControl.value, ValueData: value });
    }
  }
}
