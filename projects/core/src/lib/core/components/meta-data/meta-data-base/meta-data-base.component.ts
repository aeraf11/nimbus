import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors } from '@angular/forms';
import { OnChange } from 'property-watch-decorator';
import { MetaDataItem } from '../../../models';
import { utils } from '../../../utils';
import { MetaDataCondition, MetaDataConditionLogic } from './../../../models/meta-data-item';

@Component({
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataBaseComponent implements OnInit {
  @Input() hideRequiredMarker = false;
  @Input() hideLabel = false;
  @Input() id?: string;
  @Input() index = -1;
  @Input() label?: string;
  @Input() metaDataItemControl?: FormControl<MetaDataItem>;
  @Input() readonly = false;
  @Input() required = false;
  @Input() tabIndex?: number;
  @Input() instructions: string | null | undefined = null;

  @OnChange<MetaDataItem[]>(function (this: MetaDataBaseComponent, value) {
    this.updateVisibility();
  })
  @Input()
  metaData?: MetaDataItem[];

  @OnChange<unknown>(function (this: MetaDataBaseComponent, value) {
    this.updateVisibility();
  })
  @Input()
  model?: unknown;

  @Output() itemTouched = new EventEmitter<void>();

  isVisibleConditionally = true;
  localControl!: FormControl;

  // Override this to get the value from the metadata in the format that the localControl wants.
  // Or just leave this default implementation if it requires no change.
  get value(): any {
    return this.metaDataItemControl?.value?.Value;
  }

  constructor(public change: ChangeDetectorRef) {}

  ngOnInit(): void {
    const item = this.metaDataItemControl?.value;
    if (this.metaDataItemControl && item) {
      this.required = item.IsMandatory ?? false;
      this.label = this.id = item.Label;
      this.instructions = item.Instructions;
      this.localControl = new FormControl();
      if (this.required) {
        this.localControl.addValidators([(control: AbstractControl<any, any>) => this.requiredValidator(control)]);
      }
      this.localControl.setValue(this.value);
      this.metaDataItemControl.addValidators([
        (control: AbstractControl<any, any>) => this.localFormControlRelayValidator(control),
      ]);
      this.metaDataItemControl.setValue(item); // Need to reapply value post validator being applied.
    }
  }

  markAsTouched() {
    if (this.metaDataItemControl) {
      this.metaDataItemControl.markAsTouched();
    }
    this.itemTouched.emit();
  }

  onChange(value: any) {
    this.markAsTouched();
    const formValue = value !== null && value !== undefined ? value.toString() : value; // We don't want to stringify null or undefined
    this.updateItem(formValue);
  }

  updateItem(value: any) {
    if (this.metaDataItemControl) {
      this.metaDataItemControl.setValue({ ...this.metaDataItemControl.value, Value: value });
    }
  }

  requiredValidator(control: AbstractControl<any, any>): ValidationErrors | null {
    if (control.value === undefined || control.value === null || control.value === '') {
      return { customRequired: 'This field is required' };
    }
    return null;
  }

  localFormControlRelayValidator(control: AbstractControl<any, any>): ValidationErrors | null {
    return this.localControl.errors;
  }

  private updateVisibility(): void {
    this.metaDataItemControl?.value?.Conditional?.forEach((condition) => {
      if (condition.PropertyOnModel) {
        this.modelCondition(condition);
      } else {
        this.metaDataCondition(condition);
      }
    });
  }

  private modelCondition(condition: MetaDataCondition): void {
    if (!this.model) {
      console.warn(`No model available to check condition of ${this.metaDataItemControl?.value?.Label ?? 'MetaData'}.`);
      return;
    }

    const expr = utils.evalStringExpression<unknown>(`model.${condition.PropertyName}`, ['model']);

    if (expr) {
      const expression = expr(this.model);
      this.isVisibleConditionally = this.metaDataCompare(condition, expression);
    } else {
      this.isVisibleConditionally = false;
    }

    this.change.markForCheck();
  }

  private metaDataCondition(condition: MetaDataCondition): void {
    const metaDataItem = this.metaData?.find((m: MetaDataItem) => m.Label === condition.PropertyName);

    if (metaDataItem) {
      this.isVisibleConditionally = this.metaDataCompare(condition, metaDataItem.Value);
      this.change.markForCheck();
    }
  }

  private metaDataCompare(condition: MetaDataCondition, right: unknown): boolean {
    const left: unknown = condition.PropertyValue;
    const type: MetaDataConditionLogic = condition.LogicType;

    if (!left || !right) {
      return false;
    }

    switch (type) {
      case MetaDataConditionLogic.Equal:
      default:
        return left === right;
      case MetaDataConditionLogic.NotEqual:
        return left !== right;
      case MetaDataConditionLogic.LessThan:
        if (isNaN(left as number) || isNaN(right as number)) {
          return false;
        }
        return (left as number) > (right as number);
      case MetaDataConditionLogic.GreaterThan:
        if (isNaN(left as number) || isNaN(right as number)) {
          return false;
        }
        return (left as number) < (right as number);
    }
  }
}
