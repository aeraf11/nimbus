import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';
import { MetaDataConditionLogic, MetaDataItem } from '../../../models';
import { MetaDataBaseComponent } from './meta-data-base.component';

describe('MetaDataBaseTypeComponent', () => {
  let component: MetaDataBaseComponent;
  let fixture: ComponentFixture<MetaDataBaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataBaseComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataBaseComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('conditional meta data', () => {
    const setupMetaData = (
      propVal: any,
      value: any,
      logic: MetaDataConditionLogic,
      propName = 'Test2',
      onModel = false
    ) => {
      const itemOne = {
        Label: 'Test1',
        Conditional: [
          {
            PropertyName: propName,
            PropertyValue: propVal,
            LogicType: logic,
            PropertyOnModel: onModel,
          },
        ],
        Value: '',
      } as MetaDataItem;

      const itemTwo = {
        Label: 'Test2',
        Value: value,
      } as MetaDataItem;

      const metaControl = new FormControl<MetaDataItem>(itemOne);
      (component.metaDataItemControl as any) = metaControl;
      component.metaData = [itemOne, itemTwo];
    };

    describe('property on meta data', () => {
      describe('equals', () => {
        const logic = MetaDataConditionLogic.Equal;

        it('visible when propertyvalue equals value of meta data item', () => {
          // Arrange
          setupMetaData('Show', 'Show', logic);

          // Act
          fixture.detectChanges();

          // Assert
          expect(component.isVisibleConditionally).toEqual(true);
        });

        it('not visible when propertyvalue does not equal value of meta data item', () => {
          // Arrange
          setupMetaData('Show', 'NotShow', logic);

          // Act
          fixture.detectChanges();

          // Assert
          expect(component.isVisibleConditionally).toEqual(false);
        });
      });

      describe('not equals', () => {
        const logic = MetaDataConditionLogic.NotEqual;

        it('visible when propertyvalue does not equal value of meta data item', () => {
          // Arrange
          setupMetaData('Show', 'PleaseShow', logic);

          // Act
          fixture.detectChanges();

          // Assert
          expect(component.isVisibleConditionally).toEqual(true);
        });

        it('not visible when propertyvalue does not equal value of meta data item', () => {
          // Arrange
          setupMetaData('Hide', 'Hide', logic);

          // Act
          fixture.detectChanges();

          // Assert
          expect(component.isVisibleConditionally).toEqual(false);
        });
      });

      describe('less than', () => {
        const logic = MetaDataConditionLogic.LessThan;

        it('returns false if the PropertyValue is not a number', () => {
          // Arrange
          setupMetaData('NotNumber', 5, logic);

          // Act
          fixture.detectChanges();

          // Assert
          expect(component.isVisibleConditionally).toEqual(false);
        });

        it('returns false if the value of meta data item is not a number', () => {
          // Arrange
          setupMetaData(5, 'NotNumber', logic);

          // Act
          fixture.detectChanges();

          // Assert
          expect(component.isVisibleConditionally).toEqual(false);
        });

        it('visible when value of meta data item is less than the propertyValue', () => {
          // Arrange
          setupMetaData(5, 1, logic);

          // Act
          fixture.detectChanges();

          // Assert
          expect(component.isVisibleConditionally).toEqual(true);
        });

        it('not visible when value of meta item is not less than the propertyValue', () => {
          // Arrange
          setupMetaData(1, 5, logic);

          // Act
          fixture.detectChanges();

          // Assert
          expect(component.isVisibleConditionally).toEqual(false);
        });
      });

      describe('greater than', () => {
        const logic = MetaDataConditionLogic.GreaterThan;

        it('returns false if the PropertyValue is not a number', () => {
          // Arrange
          setupMetaData('NotNumber', 5, logic);

          // Act
          fixture.detectChanges();

          // Assert
          expect(component.isVisibleConditionally).toEqual(false);
        });

        it('returns false if the value of meta data item is not a number', () => {
          // Arrange
          setupMetaData(5, 'NotNumber', logic);

          // Act
          fixture.detectChanges();

          // Assert
          expect(component.isVisibleConditionally).toEqual(false);
        });

        it('visible when value of meta data item is greater than the propertyValue', () => {
          // Arrange
          setupMetaData(1, 5, logic);

          // Act
          fixture.detectChanges();

          // Assert
          expect(component.isVisibleConditionally).toEqual(true);
        });

        it('not visible when value of meta item is not greater than the propertyValue', () => {
          // Arrange
          setupMetaData(5, 1, logic);

          // Act
          fixture.detectChanges();

          // Assert
          expect(component.isVisibleConditionally).toEqual(false);
        });
      });
    });

    describe('property on model', () => {
      it('is visible when property is on the model and matches propertyValue', () => {
        // Arrange
        const someValue = 'Test';
        setupMetaData(someValue, null, MetaDataConditionLogic.Equal, 'some.model.property', true);
        component.model = {
          some: {
            model: {
              property: someValue,
            },
          },
        };

        // Act
        fixture.detectChanges();

        // Assert
        expect(component.isVisibleConditionally).toEqual(true);
      });

      it('is not visible when property is on the model but does not match propertyValue', () => {
        // Arrange
        setupMetaData('Hide', null, MetaDataConditionLogic.Equal, 'some.model.property', true);
        component.model = {
          some: {
            model: {
              property: 'Show',
            },
          },
        };

        // Act
        fixture.detectChanges();

        // Assert
        expect(component.isVisibleConditionally).toEqual(false);
      });

      it('warns when there is no model', () => {
        // Arrange
        spyOn(console, 'warn').and.stub();
        setupMetaData('Hide', null, MetaDataConditionLogic.Equal, 'some.model.property', true);
        component.model = undefined;

        // Act
        fixture.detectChanges();

        // Assert
        expect(console.warn).toHaveBeenCalledWith('No model available to check condition of Test1.');
      });
    });
  });
});
