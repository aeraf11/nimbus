import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetaDataChoiceSimpleComponent } from './meta-data-choice-simple.component';

describe('MetaDataChoiceSimpleComponent', () => {
  let component: MetaDataChoiceSimpleComponent;
  let fixture: ComponentFixture<MetaDataChoiceSimpleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MetaDataChoiceSimpleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MetaDataChoiceSimpleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
