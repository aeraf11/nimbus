import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { MetaDataChoice } from '../../../models/meta-data-item';
import { MetaDataBaseComponent } from '../meta-data-base/meta-data-base.component';

@Component({
  selector: 'ni-meta-data-choice-simple',
  templateUrl: './meta-data-choice-simple.component.html',
  styleUrls: ['./meta-data-choice-simple.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataChoiceSimpleComponent extends MetaDataBaseComponent implements OnInit {
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() ariaLabel?: string;
  @Input() placeholder = '';
  @Input() name = '';
  @Input() choices?: MetaDataChoice[];

  constructor(public override change: ChangeDetectorRef) {
    super(change);
  }

  override updateItem(value: any): void {
    //setTimeout to ensure requiredValidator fires before parent array validator.
    setTimeout(() => super.updateItem(value), 0);
  }

  override ngOnInit(): void {
    super.ngOnInit();
  }

  get ariaLabelInternal(): string {
    return `${this.ariaLabel ?? ''} ${this.metaDataItemControl?.value?.Label ?? ''} ${
      !this.readonly ? this.metaDataItemControl?.value?.Instructions ?? '' : ''
    }`.trim();
  }
}
