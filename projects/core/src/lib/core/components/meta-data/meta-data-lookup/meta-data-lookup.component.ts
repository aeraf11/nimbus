import { Component, Input, OnInit } from '@angular/core';
import { MetaDataBaseComponent } from '../meta-data-base/meta-data-base.component';

@Component({
  selector: 'ni-meta-data-lookup',
  templateUrl: './meta-data-lookup.component.html',
  styleUrls: ['./meta-data-lookup.component.scss'],
})
export class MetaDataLookupComponent extends MetaDataBaseComponent implements OnInit {
  @Input() placeholder = '';
  @Input() ariaLabel?: string;

  override ngOnInit(): void {
    super.ngOnInit();
    if (this.metaDataItemControl && (this.ariaLabel === '' || this.ariaLabel === undefined)) {
      this.ariaLabel = this.metaDataItemControl.value.Label ?? '';
      if (
        this.metaDataItemControl.value.Instructions &&
        this.metaDataItemControl.value.Instructions !== '' &&
        !this.readonly
      ) {
        this.ariaLabel += ' ' + this.metaDataItemControl.value.Instructions;
      }
    }
  }

  onBlur() {
    this.markAsTouched();
  }
}
