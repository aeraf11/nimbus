import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StateService } from '../../../services/state.service';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { MetaDataDateComponent } from './meta-data-date.component';

describe('MetaDataDateComponent', () => {
  let component: MetaDataDateComponent;
  let fixture: ComponentFixture<MetaDataDateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataDateComponent],
      providers: [{ provide: StateService, useClass: MockStateService }],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
