import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { AppDateTimeAdapter, StateService } from '../../../services';
import { MetaDataBaseComponent } from '../meta-data-base/meta-data-base.component';

@UntilDestroy()
@Component({
  selector: 'ni-meta-data-date',
  templateUrl: './meta-data-date.component.html',
  styleUrls: ['./meta-data-date.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataDateComponent extends MetaDataBaseComponent implements OnInit {
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() ariaLabel?: string;
  @Input() currentDate = '';
  @Input() dateControl = new UntypedFormControl();
  @Input() dateFormat = '';
  @Input() disabled = false;
  @Input() disableMinutes = false;
  @Input() enableFuture = true;
  @Input() showMeridian = true;
  @Input() showSeconds = false;
  @Input() showSpinners = true;
  @Input() showTimeZone = false;
  @Input() showTime = false;
  @Input() stepSeconds = 1;
  @Input() stepMinutes = 1;
  @Input() stepHours = 1;
  @Input() timeFormat = '';
  timeZoneId: string | null = null;

  constructor(
    public override change: ChangeDetectorRef,
    public stateService: StateService,
    public dateTimeAdapter: AppDateTimeAdapter
  ) {
    super(change);
    stateService
      .selectDateTimeFormats()
      .pipe(untilDestroyed(this))
      .subscribe(({ timeZoneId }) => (this.timeZoneId = timeZoneId || null));
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.metaDataItemControl?.value?.ShowSeconds ? (this.showSeconds = true) : (this.showSeconds = false);

    if (this.metaDataItemControl && (this.ariaLabel === '' || !this.ariaLabel)) {
      this.ariaLabel = this.metaDataItemControl.value.Label ?? '';
      if (
        this.metaDataItemControl.value.Instructions &&
        this.metaDataItemControl.value.Instructions !== '' &&
        !this.readonly
      ) {
        this.ariaLabel += ' ' + this.metaDataItemControl.value.Instructions;
      }
    }
  }

  override get value(): any {
    const parseValue = this.metaDataItemControl?.value?.Value;
    const parsed = typeof parseValue === 'string' ? JSON.parse(parseValue) : parseValue;
    return this.dateTimeAdapter.convertFromCoreDateTime(parsed);
  }

  override onChange(value: string) {
    this.updateItem(
      JSON.stringify(
        this.dateTimeAdapter.convertToCoreDateTime({
          ...this.localControl.value,
          timeZoneId: this.localControl.value.timeZoneId || this.timeZoneId,
        })
      )
    );
  }
}
