import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MetaDataService } from '@nimbus/core/src/lib/core';
import { MockMetaDataService } from './../../../services/meta-data.service.spec';
import { MetaDataAttachmentComponent } from './meta-data-attachment.component';

describe('MetaDataAttachmentComponent', () => {
  let component: MetaDataAttachmentComponent;
  let fixture: ComponentFixture<MetaDataAttachmentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataAttachmentComponent],
      providers: [{ provide: MetaDataService, useClass: MockMetaDataService }],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataAttachmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
