import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FileInfo } from '@syncfusion/ej2-angular-inputs';
import { take } from 'rxjs';
import { MetaDataAttachment, MetaDataAttachmentResponse } from '../../../models/meta-data-attachment';
import { MetaDataService } from '../../../services';
import { MetaDataBaseComponent } from '../meta-data-base/meta-data-base.component';

@Component({
  selector: 'ni-meta-data-attachment',
  templateUrl: './meta-data-attachment.component.html',
  styleUrls: ['./meta-data-attachment.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataAttachmentComponent extends MetaDataBaseComponent implements OnInit {
  @Input() chunkSize = 1024 * 1024;

  @ViewChild('uploader') uploader!: ElementRef<HTMLInputElement>;

  ariaLabel?: string;
  displayFiles: MetaDataAttachment[] = [];
  attachmentAssignment: { file: FileInfo; attachmentId: string } | undefined;
  settings = {
    chunkSize: 10485760,
    saveUrl: 'api/v2/file',
    removeUrl: 'api/v2/file/delete',
    retryAfterDelay: 3000,
    retryCount: 5,
  };

  constructor(change: ChangeDetectorRef, private metaDataService: MetaDataService) {
    super(change);
  }

  override get value(): any {
    return this.metaDataItemControl?.value?.ValueData;
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.displayFiles = JSON.parse(this.metaDataItemControl?.value?.ValueData || '[]');

    if (this.metaDataItemControl) {
      this.ariaLabel = this.metaDataItemControl.value.Label ?? '';
      if (
        this.metaDataItemControl.value.Instructions &&
        this.metaDataItemControl.value.Instructions !== '' &&
        !this.readonly
      ) {
        this.ariaLabel += ' ' + this.metaDataItemControl.value.Instructions;
      }
    }

    this.change.markForCheck();
  }

  onFileUploadSuccess(file: any): void {
    if (file && file.info && file.fileId) {
      this.metaDataService
        .createMetaDataAttachment(file.info.name, file.info.size, file.fileId)
        .pipe(take(1))
        .subscribe((response) => {
          if (response) {
            this.addItemToValueData(response);
            this.updateAttachmentAssignment(file.info, response.attachmentId);
          }
        });
    }
  }

  onFileRemove(attachmentId: string): void {
    this.removeItemFromValueData(attachmentId);
  }

  override updateItem(value: any): void {
    if (this.metaDataItemControl) {
      this.metaDataItemControl.setValue({ ...this.metaDataItemControl.value, ValueData: value });
    }
  }

  private updateAttachmentAssignment(file: FileInfo, id: string): void {
    this.attachmentAssignment = {
      file: file,
      attachmentId: id,
    };
  }

  private addItemToValueData(item: MetaDataAttachmentResponse | undefined): void {
    if (this.metaDataItemControl && item) {
      let parsedValueData = JSON.parse(this.metaDataItemControl?.value?.ValueData || '[]');
      parsedValueData = parsedValueData.concat([
        { AttachmentId: item.attachmentId, LinkedAttachmentId: item.linkedAttachmentId, Description: item.description },
      ]);
      this.updateControlValues(JSON.stringify(parsedValueData));
    }
  }

  private removeItemFromValueData(attachmentId: string): void {
    if (this.metaDataItemControl) {
      let parsedValueData = JSON.parse(this.metaDataItemControl?.value?.ValueData || '[]');
      parsedValueData = parsedValueData.filter((valueData: any) => valueData.AttachmentId !== attachmentId);
      this.updateControlValues(parsedValueData.length > 0 ? JSON.stringify(parsedValueData) : null);
    }
  }

  private updateControlValues(value: any): void {
    this.localControl.setValue(value);
    this.localControl.markAsTouched();
    this.onChange(value);
  }
}
