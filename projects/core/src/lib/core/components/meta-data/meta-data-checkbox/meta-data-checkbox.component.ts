import { Component, Input, OnInit } from '@angular/core';
import { MetaDataBaseComponent } from '../meta-data-base/meta-data-base.component';

@Component({
  selector: 'ni-meta-data-checkbox',
  templateUrl: './meta-data-checkbox.component.html',
  styleUrls: ['./meta-data-checkbox.component.scss'],
})
export class MetaDataCheckboxComponent extends MetaDataBaseComponent implements OnInit {
  @Input() color = 'accent';
  @Input() indeterminate = false;
  @Input() ariaLabel?: string;
  @Input() labelPosition: 'before' | 'after' = 'before';

  override get value(): any {
    // core represents checkbox values as stringified boolean or null
    return this.metaDataItemControl?.value?.Value === 'true'
      ? true
      : this.metaDataItemControl?.value?.Value === 'false'
      ? false
      : this.metaDataItemControl?.value?.Value;
  }

  get ariaLabelInternal(): string {
    return `${this.ariaLabel ?? ''} ${this.metaDataItemControl?.value?.Label ?? ''} ${
      !this.readonly ? this.metaDataItemControl?.value?.Instructions ?? '' : ''
    }`.trim();
  }
}
