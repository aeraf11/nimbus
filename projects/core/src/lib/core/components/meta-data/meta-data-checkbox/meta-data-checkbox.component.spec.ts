import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl } from '@angular/forms';
import { MetaDataItem } from '../../../models';

import { MetaDataCheckboxComponent } from './meta-data-checkbox.component';

describe('MetaDataCheckboxComponent', () => {
  let component: MetaDataCheckboxComponent;
  let fixture: ComponentFixture<MetaDataCheckboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataCheckboxComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('value', () => {
    it('should provide the boolean true when the item value is "true"', () => {
      // Arrange
      const item = <MetaDataItem>{ Label: 'Test', Value: 'true' };
      component.metaDataItemControl = <FormControl<MetaDataItem>>new FormControl<MetaDataItem>(item);
      // Act

      // Assert
      expect(component.value).toBe(true);
    });
    it('should provide the boolean false when the item value is "false"', () => {
      // Arrange
      const item = <MetaDataItem>{ Label: 'Test', Value: 'false' };
      component.metaDataItemControl = <FormControl<MetaDataItem>>new FormControl<MetaDataItem>(item);
      // Act

      // Assert
      expect(component.value).toBe(false);
    });

    it('should provide null when the item value is null', () => {
      // Arrange
      const item = <MetaDataItem>{ Label: 'Test', Value: null };
      component.metaDataItemControl = <FormControl<MetaDataItem>>new FormControl<MetaDataItem>(item);
      // Act

      // Assert
      expect(component.value).toBe(null);
    });
  });
});
