import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { MetaDataBaseComponent } from '../meta-data-base/meta-data-base.component';

@Component({
  selector: 'ni-meta-data-rich-text-editor',
  templateUrl: './meta-data-rich-text-editor.component.html',
  styleUrls: ['./meta-data-rich-text-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataRichTextEditorComponent extends MetaDataBaseComponent implements OnInit {
  settings = {
    tools: [],
  };

  @Input() height = '';
  @Input() width = '';
  @Input() maxWidth = '';
  @Input() enableResize = false;
  @Input() showCharCount = false;
  @Input() ariaLabel = '';

  @Input() tools = [];

  override ngOnInit() {
    super.ngOnInit();

    if (this.metaDataItemControl && this.ariaLabel === '') {
      this.ariaLabel = this.metaDataItemControl.value.Label ?? '';
      if (
        this.metaDataItemControl.value.Instructions &&
        this.metaDataItemControl.value.Instructions !== '' &&
        !this.readonly
      ) {
        this.ariaLabel += ' ' + this.metaDataItemControl.value.Instructions;
      }
    }
  }

  constructor(public override change: ChangeDetectorRef) {
    super(change);
  }

  updateControlValues(value: any): void {
    this.localControl.setValue(value);
    this.localControl.markAsTouched();
    this.onChange(value);
  }
}
