import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MetaDataRichTextEditorComponent } from './meta-data-rich-text-editor.component';

describe('MetaDataRichTextEditorComponent', () => {
  let component: MetaDataRichTextEditorComponent;
  let fixture: ComponentFixture<MetaDataRichTextEditorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MetaDataRichTextEditorComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MetaDataRichTextEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
