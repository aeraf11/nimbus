import { MatDialog } from '@angular/material/dialog';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, UntypedFormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IndexedDbService, LocalFile } from '@nimbus/core/src/lib/core';
import { v4 as uuidv4 } from 'uuid';
import { FileManagerComponent } from '../file-manager/file-manager.component';
import { LiveAnnouncer } from '@angular/cdk/a11y';

@Component({
  selector: 'ni-uploader-offline',
  templateUrl: './uploader-offline.component.html',
  styleUrls: ['./uploader-offline.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UploaderOfflineComponent),
      multi: true,
    },
  ],
})
export class UploaderOfflineComponent implements OnInit, ControlValueAccessor {
  @Input() formControl = new UntypedFormControl();
  @Input() itemTableName = 'offlineUploads';
  @Input() chunkSize = 1024 * 1024;
  @Input() multiple = false;
  @Input() enableFileManager = true;

  @ViewChild('uploader') uploader!: ElementRef<HTMLInputElement>;
  displayFiles: LocalFile[] = [];

  private onChange!: (value: unknown) => void;
  private onTouched!: () => void;

  multipleFiles = `Or drop ${this.multiple ? 'files' : 'a file'} here`;

  constructor(
    private change: ChangeDetectorRef,
    private dbService: IndexedDbService,
    private matDialog: MatDialog,
    private liveAnnouncer: LiveAnnouncer
  ) {}

  ngOnInit(): void {
    const fileIds = this.formControl.value?.map((value: any) => value.fileId) ?? [];
    this.dbService
      .table(this.itemTableName)
      .where('id')
      .anyOf(fileIds)
      .toArray()
      .then((filesFromStore: LocalFile[]) => {
        const filesWithStatus: LocalFile[] = filesFromStore.map((file) => ({ ...file, status: 'Saved' }));
        this.displayFiles = filesWithStatus;
        this.onChange(
          this.displayFiles.map((f) => ({ fileId: f.id, name: f.file.name, type: f.file.type, size: f.file.size }))
        );
        this.change.markForCheck();
      });

    this.liveAnnouncer.announce(`Browse button ${this.multipleFiles}. File manager icon`);
  }

  onFileDrop(event: DragEvent): void {
    event.preventDefault();
    const dropUploads = event?.dataTransfer?.files;
    const dropUploadsLength = dropUploads?.length ?? 0;

    if (dropUploadsLength > 0) {
      this.onFileChange(dropUploads);
    }
  }

  onFileChange(dropFiles?: FileList): void {
    const uploads = dropFiles ?? this.uploader.nativeElement.files;
    const uploadsLength = uploads?.length ?? 0;

    for (let i = 0; i < uploadsLength; i++) {
      this.processFile(uploads?.[i]);
    }
  }

  // https://stackoverflow.com/q/12030686/988163
  onUploaderClick(): void {
    this.uploader.nativeElement.value = '';
  }

  saveFile(upload: File, file: LocalFile): void {
    this.displayFiles.push(file);
    this.dbService
      .table(this.itemTableName)
      .add({ id: file.id, file: file.file })
      .then(() =>
        this.onChange(
          this.displayFiles.map((f) => ({ fileId: f.id, name: f.file.name, type: f.file.type, size: f.file.size }))
        )
      )
      .then(() =>
        this.splitUploadIntoChunks(upload, file).then(() => {
          file.status = 'Saved';
        })
      );
  }

  removeFileFromQueue(file: LocalFile): void {
    this.dbService.table(this.itemTableName).delete(file.id);
    this.dbService
      .table(`${this.itemTableName}Chunks`)
      .where('fileId')
      .equals(file.id)
      .delete()
      .then(() => {
        this.displayFiles = this.displayFiles.filter((files) => files.id !== file.id);
        this.onChange(
          this.displayFiles.map((f) => ({ fileId: f.id, name: f.file.name, type: f.file.type, size: f.file.size }))
        );
        this.change.markForCheck();
      });
    this.liveAnnouncer.announce(`Removed ${file.file.name}`);
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  writeValue(obj: any): void {}

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  private processFile(upload: File | undefined): void {
    if (!upload) {
      return;
    }

    const file = {
      id: uuidv4(),
      file: {
        name: upload.name,
        size: upload.size,
        type: upload.type,
      },
      status: 'Saving...',
    };

    this.saveFile(upload, file);
  }

  private async splitUploadIntoChunks(upload: File, file: LocalFile): Promise<void> {
    let startPointer = 0;
    const endPointer = upload.size;
    file.progressMax = endPointer;
    file.progressCurrent = 0;

    while (startPointer < endPointer) {
      const newStartPointer = startPointer + this.chunkSize;
      const chunk = upload.slice(startPointer, newStartPointer);
      startPointer = newStartPointer;
      await this.saveChunkToQueue(file, chunk, startPointer);
    }
  }

  private async saveChunkToQueue(file: LocalFile, blob: Blob, current: number): Promise<void> {
    await this.dbService
      .table(`${this.itemTableName}Chunks`)
      .add({ fileId: file.id, blob })
      .then(() => {
        file.progressCurrent = current;
        this.change.markForCheck();
      });
  }

  openDialog() {
    const data = {
      dialogView: true,
      itemTableName: this.itemTableName,
    };
    const dialogRef = this.matDialog.open(FileManagerComponent, { data: data, disableClose: false });

    dialogRef.afterClosed().subscribe(() => {
      this.ngOnInit();
    });
  }
}
