import { MatDialogModule } from '@angular/material/dialog';
import { MockIndexedDbService } from '../../services/indexeddb.service.spec';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { UploaderOfflineComponent } from './uploader-offline.component';
import { IndexedDbService } from '../../services/indexeddb.service';

describe('UploaderOfflineComponent', () => {
  let component: UploaderOfflineComponent;
  let fixture: ComponentFixture<UploaderOfflineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UploaderOfflineComponent],
      imports: [MatDialogModule],
      providers: [{ provide: IndexedDbService, useClass: MockIndexedDbService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploaderOfflineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
