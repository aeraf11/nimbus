import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { ListFilterService, StateService } from '@nimbus/core/src/lib/core';
import { ListService } from '../../../core/services/list.service';
import { MockListService } from '../../../core/services/list.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { MockFormatComponent } from './../../../core/components/format/format.component.spec';
import { ToDateRelativePipe } from './../../../core/pipes/to-date-relative.pipe';
import { EntityMediaComponent } from './entity-media.component';
import { DataCardsComponent } from '../data-cards/data-cards.component';
import { MockListFilterService } from '../../services/list-filter-service/list-filter.service.spec';

describe('EntityMediaComponent', () => {
  let component: EntityMediaComponent;
  let fixture: ComponentFixture<EntityMediaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EntityMediaComponent, ToDateRelativePipe, MockFormatComponent],
      imports: [MatDialogModule, MatCardModule],
      providers: [
        DataCardsComponent,
        { provide: StateService, useClass: MockStateService },
        { provide: ListService, useClass: MockListService },
        { provide: ListFilterService, useClass: MockListFilterService },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntityMediaComponent);
    component = fixture.componentInstance;

    component.item = {
      blobId: 'blobId',
      description: '',
      creationDate: '',
      createdByFullName: '',
    };

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
