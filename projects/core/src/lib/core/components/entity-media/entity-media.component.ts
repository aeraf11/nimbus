import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  ViewEncapsulation,
  OnInit,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DataType } from '@nimbus/core/src/lib/core';
import { DataCardsItemBaseComponent } from '../data-cards-item-base/data-cards-item-base.component';
import { DataCardsComponent } from '../data-cards/data-cards.component';

UntilDestroy();
@Component({
  selector: 'ni-entity-media',
  templateUrl: './entity-media.component.html',
  styleUrls: ['./entity-media.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EntityMediaComponent),
      multi: true,
    },
  ],
})
export class EntityMediaComponent extends DataCardsItemBaseComponent implements ControlValueAccessor, OnInit {
  DataType = DataType;
  linkedEntity = { text: '', icon: '' };

  constructor(public override parent: DataCardsComponent, private change: ChangeDetectorRef) {
    super(parent, change);
  }

  ngOnInit(): void {
    this.itemUpdate.pipe(untilDestroyed(this)).subscribe((item) => {
      if (item) {
        const json = JSON.parse(this.item?.entityMediaEntitiesJson ?? '')?.[0] ?? {};
        this.linkedEntity.text = json.DisplayValue;
        this.linkedEntity.icon = json.EntityTypeIcon;
      }
    });
  }

  onImageClick(item: any, event: Event) {
    if (this.parent.detailDialogFormDefinition) {
      this.parent.itemClicked(item, { id: 'entityMediaId' });
      return;
    }
    event.stopPropagation();
    return false;
  }
}
