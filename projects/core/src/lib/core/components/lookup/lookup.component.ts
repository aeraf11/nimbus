import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { OnChange } from 'property-watch-decorator';
import { LookupNode, MetaDataChoice } from '../../models';
import { LookupService } from '../../services';
import { LookupDialogComponent } from '../lookup-dialog/lookup-dialog.component';

@UntilDestroy()
@Component({
  selector: 'ni-lookup',
  templateUrl: './lookup.component.html',
  styleUrls: ['./lookup.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LookupComponent implements OnInit {
  value!: string;
  lookups: LookupNode[] | null = null;
  isTree = false;

  @OnChange<string>(function (this: LookupComponent, value, change) {
    if (value) {
      // Having to use setTimeout here as this was firing before any of the @Inputs
      // had been set from the lookup-type props
      setTimeout(() => this.setDisplayValue(), 0);
    }
  })
  @Input()
  lookupKey: string | null = null;

  @OnChange<string>(function (this: LookupComponent, value, change) {
    if (value) {
      // Having to use setTimeout here as this was firing before any of the @Inputs
      // had been set from the lookup-type props
      setTimeout(() => this.setDisplayValue(), 0);
    }
  })
  @Input()
  parentLookupId = '';

  @Input() id = '';
  @Input() placeholder: string | undefined;
  @Input() title: string | null = null;
  @Input() required = false;
  @Input() disabled = false;
  @Input() leafSelection = true;
  @Input() addDefaultOption = true;
  @Input() lookupControl!: UntypedFormControl;
  @Input() outputDisplayValue = false;
  @Input() lookupFilters: string[] = [];
  @Input() topLevelOnly = false;
  @Input() minDialogWidth = '';
  @Input() maxDialogWidth = '';
  @Input() enableFieldLabel = false;
  @Input() ariaLabel = '';
  @Input() choices?: { fallback: boolean; items: MetaDataChoice[] };

  @HostBinding('class')
  @Input()
  className = '';

  @ViewChild('lookupInput')
  lookupInput: any;

  @ViewChild(MatAutocompleteTrigger) autocomplete!: MatAutocompleteTrigger;

  @Output() valueChange = new EventEmitter<string>();
  @Output() lookupBlur = new EventEmitter<void>();

  constructor(private change: ChangeDetectorRef, private service: LookupService, public dialog: MatDialog) {}

  ngOnInit(): void {
    if (this.lookupControl?.value) {
      this.onItemSelected(this.lookupControl?.value, true);
    }
  }

  displayStatusName(id: string): string {
    if (!id) return '';
    const lookup = this.getSelectedLookup(this.lookups, id);
    if (this.parentLookupId) {
      // We don't want to keep the parent in the display value when the children
      // are in their own separate lookups
      return lookup?.item?.displayValue ?? '';
    } else {
      return lookup?.item?.hierarchicalDisplayValue ?? '';
    }
  }

  getSelectedLookup(lookups: any | null, id: string): LookupNode | null {
    let selectedLookup = null;
    if (!this.outputDisplayValue) {
      selectedLookup = lookups?.filter((lookup: LookupNode) => lookup?.item?.lookupId === id)[0] ?? null;
    } else {
      selectedLookup = lookups?.filter((lookup: LookupNode) => lookup?.item?.displayValue === id)[0] ?? null;
    }
    if (selectedLookup) return selectedLookup;

    const children = [].concat(...(lookups?.map((lookup: LookupNode) => lookup.children) ?? []));
    return children?.length > 0 ? this.getSelectedLookup(children, id) : null;
  }

  openDialog(): void {
    if (this.isTree && !this.disabled) {
      this.lookupControl.setErrors(null);
      this.change.markForCheck();
      this.change.detach();
      const dialogRef = this.dialog.open(LookupDialogComponent, {
        maxWidth: this.maxDialogWidth,
        minWidth: this.minDialogWidth,
        data: {
          title: this.title,
          key: this.lookupKey,
          value: this.value,
          leafSelection: this.leafSelection,
          addDefaultOption: this.addDefaultOption,
          lookupFilters: this.lookupFilters,
        },
      });

      dialogRef.afterClosed().subscribe((result: { selectedId: string; displayValue: string }) => {
        this.change.reattach();

        if (result) {
          this.onItemSelected(this.outputDisplayValue ? result.displayValue : result.selectedId);
        } else {
          if (!this.lookupControl.value) {
            if (this.required) {
              this.lookupControl.setErrors({ required: true });
            }
          }
        }

        this.change.markForCheck();
      });
    }
  }

  getValue(lookUp: LookupNode): string {
    return this.outputDisplayValue ? lookUp.item.displayValue : lookUp.item.lookupId;
  }

  onItemSelected(value: string, initialSelect = false) {
    this.value = value;
    this.lookupControl?.setValue(this.value);
    this.valueChange.emit(this.value);
    if (!initialSelect) {
      this.lookupControl?.markAsDirty();
    }

    this.lookupInput?.nativeElement.blur();
    this.setDisplayValue();
  }

  pseudoReadOnly(event: KeyboardEvent): boolean {
    // Setting readonly on an input that uses autofocus does not open the autocomplete panel
    // Workaround is that on keydown we just return false to make the input readonly
    // However, we want to ignore Tab and Shift (Shift+Tab) for keyboard navigation otherwise
    // this control becomes a focus trap
    return event.key === 'Tab' || event.key === 'Shift';
  }

  setDisplayValue(): void {
    if (this.lookupKey) {
      this.service
        .getLookupsHierachy(this.lookupKey, this.lookupFilters)
        .pipe(untilDestroyed(this))
        .subscribe((lookups) => {
          this.lookups = lookups;
          if (this.lookups) {
            if (this.parentLookupId) {
              this.lookups = this.getLookupsByParentId(this.lookups, this.parentLookupId);
            }

            const withChild = this.lookups.filter((l) => l.children && l.children?.length > 0);
            if (!this.topLevelOnly) {
              this.isTree = withChild.length > 0;
            }

            this.change.markForCheck();
          } else {
            if (this.choices?.fallback) {
              this.lookups = this.getLookupsByChoices(this.choices.items);
            }
          }
        });
    }
  }

  onBlur(): void {
    // This control does nothing when disabled so we shouldn't emit that it was blurred as this could trigger
    // validation messages. We also set the lookupControl to untouched so that a red border wont appear
    if (this.disabled) {
      this.lookupControl.markAsUntouched();
    } else {
      this.lookupBlur.emit();
    }
  }

  private getLookupsByParentId(lookups: LookupNode[], parentId: string): LookupNode[] {
    const lookupNodes: LookupNode[] = [];

    lookups.forEach((lookup) => {
      if (lookup?.item?.parentLookupId === parentId) {
        lookupNodes.push(lookup);
      }
      if (lookup?.children && lookup.children?.length > 0) {
        return lookupNodes.push(...this.getLookupsByParentId(lookup.children, parentId));
      }

      return;
    });

    return lookupNodes;
  }

  private getLookupsByChoices(choices: MetaDataChoice[]): LookupNode[] | null {
    if (choices && choices.length > 0) {
      return choices.map((choice) => ({
        item: {
          displayValue: choice.Label ?? choice.Value ?? '',
          hierarchicalDisplayValue: choice.Label ?? choice.Value ?? '',
          lookupId: choice.ValueData ?? '',
          lookupKey: this.lookupKey ?? '',
          level: 0,
        },
        children: [] as LookupNode[],
      }));
    }
    return null;
  }
}
