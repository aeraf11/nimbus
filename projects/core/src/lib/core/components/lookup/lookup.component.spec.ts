import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, ReactiveFormsModule, UntypedFormControl } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { LookupNode } from '../../models';
import { LookupService, StateService } from '../../services';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { LookupComponent } from './lookup.component';

// TODO: getSelectedLookup, openDialog and more setDisplayValue coverage
describe('LookupComponent', () => {
  let component: LookupComponent;
  let fixture: ComponentFixture<LookupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LookupComponent],
      imports: [
        MatDialogModule,
        MatAutocompleteModule,
        MatFormFieldModule,
        MatInputModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
      ],
      providers: [{ provide: StateService, useClass: MockStateService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LookupComponent);
    component = fixture.componentInstance;
    component.lookupControl = new UntypedFormControl({});
    component.lookups = [
      {
        item: {
          displayValue: 'test',
          hierarchicalDisplayValue: 'test > test',
          lookupId: 'id',
          lookupKey: 'key',
          level: 0,
        },
        children: [],
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {
    it('calls onItemSelected with initialSelect set to true if lookupControl has a value', () => {
      // Arrange
      const someValue = 'test';
      const someInitialSelect = true;

      component.lookupControl.setValue(someValue);
      spyOn(component, 'onItemSelected').and.callFake(() => null);

      // Act
      component.ngOnInit();

      // Assert
      expect(component.onItemSelected).toHaveBeenCalledWith(someValue, someInitialSelect);
    });

    it('does not call onItemSelected if lookupControl has no value', () => {
      // Arrange
      component.lookupControl = new FormControl();
      spyOn(component, 'onItemSelected').and.callFake(() => null);

      // Act
      component.ngOnInit();

      // Assert
      expect(component.onItemSelected).not.toHaveBeenCalled();
    });
  });

  describe('displayStatusName()', () => {
    it('returns empty string if no id is passed', () => {
      // Arrange
      const id = '';

      // Act
      const result = component.displayStatusName(id);

      // Assert
      expect(result).toEqual('');
    });

    it('returns a lookups hierarchicalDisplayValue', () => {
      // Arrange
      const someHierarchicalDisplayValue = 'test';
      const someLookup = {
        item: {
          hierarchicalDisplayValue: someHierarchicalDisplayValue,
        },
      } as LookupNode;

      spyOn(component, 'getSelectedLookup').and.callFake(() => someLookup);

      // Act
      const result = component.displayStatusName('any');

      // Assert
      expect(result).toEqual(someHierarchicalDisplayValue);
    });

    it('returns a lookups displayValue if there is a parentLookupId present', () => {
      // Arrange
      component.parentLookupId = 'notnull';

      const someDisplayValue = 'test';
      const someLookup = {
        item: {
          hierarchicalDisplayValue: 'i wont be shown',
          displayValue: someDisplayValue,
        },
      } as LookupNode;

      spyOn(component, 'getSelectedLookup').and.callFake(() => someLookup);

      // Act
      const result = component.displayStatusName('any');

      // Assert
      expect(result).toEqual(someDisplayValue);
    });

    it('returns empty string if lookup not found', () => {
      // Arrange
      spyOn(component, 'getSelectedLookup').and.callFake(() => null);

      // Act
      const result = component.displayStatusName('any');

      // Assert
      expect(result).toEqual('');
    });
  });

  describe('getValue', () => {
    it('returns the displayValue is outputDisplayValue is true', () => {
      // Arrange
      const someDisplayValue = 'displayValue';
      const someLookupNode = {
        item: {
          lookupId: 'id',
          displayValue: someDisplayValue,
        },
      } as LookupNode;
      component.outputDisplayValue = true;

      // Act
      const result = component.getValue(someLookupNode);

      // Assert
      expect(result).toEqual(someDisplayValue);
    });

    it('returns the lookupId is outputDisplayValue is false', () => {
      // Arrange
      const someId = 'id';
      const someLookupNode = {
        item: {
          lookupId: someId,
          displayValue: 'displayValue',
        },
      } as LookupNode;
      component.outputDisplayValue = false;

      // Act
      const result = component.getValue(someLookupNode);

      // Assert
      expect(result).toEqual(someId);
    });
  });

  describe('onItemSelected', () => {
    it('sets value', () => {
      // Arrange
      const someValue = 'test';
      component.value = '';

      // Act
      component.onItemSelected(someValue);

      // Assert
      expect(component.value).toEqual(someValue);
    });

    it('lookupControl setValue is called with new value', () => {
      // Arrange
      const someValue = 'test';
      component.value = '';
      spyOn(component.lookupControl, 'setValue').and.callFake(() => null);

      // Act
      component.onItemSelected(someValue);

      // Assert
      expect(component.lookupControl.setValue).toHaveBeenCalledWith(someValue);
    });

    it('emits the value via lookupChange event emitter', () => {
      // Arrange
      const someValue = 'test';
      component.value = '';
      spyOn(component.valueChange, 'emit').and.callFake(() => null);

      // Act
      component.onItemSelected(someValue);

      // Assert
      expect(component.valueChange.emit).toHaveBeenCalledWith(someValue);
    });

    it('lookupControl is marked as dirty if initialSelect is false', () => {
      // Arrange
      spyOn(component.lookupControl, 'markAsDirty').and.callFake(() => null);

      // Act
      component.onItemSelected('');

      // Assert
      expect(component.lookupControl.markAsDirty).toHaveBeenCalled();
    });

    it('lookupControl is not marked as dirty if initialSelect is true', () => {
      // Arrange
      spyOn(component.lookupControl, 'markAsDirty').and.callFake(() => null);

      // Act
      component.onItemSelected('', true);

      // Assert
      expect(component.lookupControl.markAsDirty).not.toHaveBeenCalled();
    });

    it('lookupInput is blurred', () => {
      // Arrange
      spyOn(component.lookupInput.nativeElement, 'blur').and.callFake(() => null);

      // Act
      component.onItemSelected('');

      // Assert
      expect(component.lookupInput.nativeElement.blur).toHaveBeenCalled();
    });

    it('setDisplayValue is called', () => {
      // Arrange
      spyOn(component, 'setDisplayValue').and.callFake(() => null);

      // Act
      component.onItemSelected('');

      // Assert
      expect(component.setDisplayValue).toHaveBeenCalled();
    });
  });

  describe('pseudoReadOnly()', () => {
    it('returns true when event key is "Tab"', () => {
      // Arrange
      const someEvent = { key: 'Tab' } as KeyboardEvent;

      // Act
      const result = component.pseudoReadOnly(someEvent);

      // Assert
      expect(result).toEqual(true);
    });

    it('returns true when event key is "Shift"', () => {
      // Arrange
      const someEvent = { key: 'Shift' } as KeyboardEvent;

      // Act
      const result = component.pseudoReadOnly(someEvent);

      // Assert
      expect(result).toEqual(true);
    });

    it('returns false when event key is is any other key', () => {
      // Arrange
      const someEvent = { key: 'a' } as KeyboardEvent;

      // Act
      const result = component.pseudoReadOnly(someEvent);

      // Assert
      expect(result).toEqual(false);
    });
  });

  describe('setDisplayValue()', () => {
    it('calls getLookupsHierachy with lookupKey and lookupFilters only if there is a lookupKey', () => {
      // Arrange
      const someLookupKey = 'test';
      const someLookupFilters = ['test'];

      component.lookupKey = someLookupKey;
      component.lookupFilters = someLookupFilters;

      const service = TestBed.inject(LookupService);
      spyOn(service, 'getLookupsHierachy').and.callFake(() => of(null));

      // Act
      component.setDisplayValue();

      // Assert
      expect(service.getLookupsHierachy).toHaveBeenCalledWith(someLookupKey, someLookupFilters);
    });

    it('does not call getLookupsHierachy when no lookupKey', () => {
      // Arrange
      component.lookupKey = null;

      const service = TestBed.inject(LookupService);
      spyOn(service, 'getLookupsHierachy').and.callFake(() => of(null));

      // Act
      component.setDisplayValue();

      // Assert
      expect(service.getLookupsHierachy).not.toHaveBeenCalled();
    });
  });

  describe('onBlur()', () => {
    it('lookupBlur emits when disabled is false', () => {
      // Arrange
      component.disabled = false;
      spyOn(component.lookupBlur, 'emit').and.callFake(() => null);

      // Act
      component.onBlur();

      // Assert
      expect(component.lookupBlur.emit).toHaveBeenCalled();
    });

    it('lookupBlur does not emit when disabled is true', () => {
      // Arrange
      component.disabled = true;
      spyOn(component.lookupBlur, 'emit').and.callFake(() => null);

      // Act
      component.onBlur();

      // Assert
      expect(component.lookupBlur.emit).not.toHaveBeenCalled();
    });

    it('lookupControl is marked as untouched if disabled is true', () => {
      // Arrange
      component.disabled = true;
      spyOn(component.lookupControl, 'markAsUntouched').and.callFake(() => null);

      // Act
      component.onBlur();

      // Assert
      expect(component.lookupControl.markAsUntouched).toHaveBeenCalled();
    });

    it('lookupControl is not marked as untouched if disabled is false', () => {
      // Arrange
      component.disabled = false;
      spyOn(component.lookupControl, 'markAsUntouched').and.callFake(() => null);

      // Act
      component.onBlur();

      // Assert
      expect(component.lookupControl.markAsUntouched).not.toHaveBeenCalled();
    });
  });
});
