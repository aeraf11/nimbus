import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import {
  BlobService,
  IconDirective,
  ListFilterService,
  ListService,
  SiteConfig,
  StateService,
} from '@nimbus/core/src/lib/core';
import { DataDownloadablesComponent } from './data-downloadables.component';
import { MockListFilterService } from '../../../core/services/list-filter-service/list-filter.service.spec';
import { MockListService } from '../../services/list.service.spec';
import { MockBlobService } from '../../services/blob.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';

describe('DataDownloadablesComponent', () => {
  let component: DataDownloadablesComponent;
  let fixture: ComponentFixture<DataDownloadablesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DataDownloadablesComponent, IconDirective],
      imports: [MatDialogModule, MatCardModule, MatIconModule, HttpClientTestingModule],
      providers: [
        { provide: StateService, useClass: MockStateService },
        { provide: ListService, useClass: MockListService },
        { provide: BlobService, useClass: MockBlobService },
        { provide: ListFilterService, useClass: MockListFilterService },
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataDownloadablesComponent);
    component = fixture.componentInstance;
    component.listName = 'Test';

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
