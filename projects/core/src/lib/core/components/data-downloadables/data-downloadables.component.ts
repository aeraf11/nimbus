import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, Input, Optional } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import {
  CardListWrapperComponent,
  EntityMediaRequestComponent,
  FileService,
  FormsHelperService,
  IndexedDbService,
  ListFilterComponent,
  ListFilterService,
  ListService,
  SignalRService,
  StateService,
} from '@nimbus/core/src/lib/core';
import { QuickSearchWrapperComponent } from '@nimbus/core/src/lib/core';
import { DataListBaseComponent } from '../data-list-base/data-list-base.component';

@Component({
  selector: 'ni-data-downloadables',
  templateUrl: './data-downloadables.component.html',
  styleUrls: ['./data-downloadables.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DataDownloadablesComponent),
      multi: true,
    },
  ],
})
export class DataDownloadablesComponent extends DataListBaseComponent implements ControlValueAccessor {
  @Input() ariaLabelField = '';

  constructor(
    service: ListService,
    change: ChangeDetectorRef,
    state: StateService,
    private fileService: FileService,
    public override formsHelper: FormsHelperService,
    @Optional() cardListWrapper: CardListWrapperComponent,
    listFilterService: ListFilterService,
    dialog: MatDialog,
    @Optional() entityMediaRequest: EntityMediaRequestComponent,
    @Optional() listFilterWrapper: ListFilterComponent,
    @Optional() quickSearchWrapper: QuickSearchWrapperComponent,
    indexedDbService: IndexedDbService,
    signalR: SignalRService
  ) {
    super(
      service,
      change,
      state,
      formsHelper,
      cardListWrapper,
      listFilterService,
      dialog,
      entityMediaRequest,
      listFilterWrapper,
      quickSearchWrapper,
      indexedDbService,
      signalR
    );
  }

  async download() {
    const downloadIds = this.selectedItems.filter((item) => item.blobId).map((item) => item.attachmentId);
    if (downloadIds) {
      await this.fileService.downloadAttachmentById(downloadIds);
    }
  }
}
