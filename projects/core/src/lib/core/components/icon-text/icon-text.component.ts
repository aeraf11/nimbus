import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'ni-icon-text',
  templateUrl: './icon-text.component.html',
  styleUrls: ['./icon-text.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconTextComponent {
  @Input() icon?: string;
  @Input() iconColour?: string;
  @Input() iconSize?: string;

  @Input() subIcon?: string;
  @Input() subIconColour?: string;
  @Input() subIconSize?: string;

  @Input() label?: string;
  @Input() labelColour?: string;
  @Input() labelSize?: string;

  @Input() text?: string;
  @Input() textColour?: string;
  @Input() textSize?: string;

  @Input() count?: number;
}
