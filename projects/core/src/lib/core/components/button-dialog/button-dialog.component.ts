import { ChangeDetectorRef, Component, Input } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import {
  DialogFormComponent,
  DialogFormConfig,
  DialogFormData,
  EditorMode,
  FormDefinition,
  StateService,
} from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ni-button-dialog',
  templateUrl: './button-dialog.component.html',
  styleUrls: ['./button-dialog.component.scss'],
})
export class ButtonDialogComponent {
  constructor(public change: ChangeDetectorRef, private matDialog: MatDialog, private state: StateService) {}

  @Input() formId?: string;
  @Input() formActionKey = '';
  @Input() formRouteId = '';
  @Input() label = 'Feedback';
  @Input() ariaLabel = '';
  @Input() disabled = false;
  @Input() dialogTitle = '';
  @Input() dialogWidth = '';
  @Input() buttonIcon = 'fa-solid fa-message-dots';
  @Input() buttonClass = 'feedback';
  @Input() showIconText = false;
  @Input() modalDialogConfig: DialogFormConfig = {
    enableDelete: false,
    enableEdit: true,
    isChildForm: false,
  };

  @Input() form?: FormDefinition | null;

  @Input() editorMode = EditorMode.Form;

  openDialog(form: FormDefinition | undefined | null, config: DialogFormConfig, model: any, context: any): void {
    const data: DialogFormData = {
      model,
      fieldConfig: form?.fields,
      config: { ...config, formId: form?.id, titleTemplate: this.dialogTitle ?? 'Add Feedback' },
      disabled: this.disabled,
      context,
      editorMode: this.editorMode,
      formActionKey: this.formActionKey,
      formRouteId: this.formRouteId,
    };
    const dialogConfig: MatDialogConfig<DialogFormData> = { data };
    dialogConfig.width = this.dialogWidth;
    const dialogRef = this.matDialog.open(DialogFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(() => {
      this.change.markForCheck();
    });
  }

  openForm() {
    this.openDialog(this.form, this.modalDialogConfig, {}, {});
  }
}
