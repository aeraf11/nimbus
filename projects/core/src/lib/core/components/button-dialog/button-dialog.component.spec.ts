import { ComponentFixture, TestBed } from '@angular/core/testing';
import { fakeAsync, tick } from '@angular/core/testing';
import { ButtonDialogComponent } from './button-dialog.component';
import { MatDialog } from '@angular/material/dialog';

describe('ButtonDialogComponent', () => {
  let component: ButtonDialogComponent;
  let fixture: ComponentFixture<ButtonDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ButtonDialogComponent],
      providers: [{ provide: MatDialog, useValue: jasmine.createSpyObj<MatDialog>(['open']) }],
    }).compileComponents();

    fixture = TestBed.createComponent(ButtonDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should', fakeAsync(() => {
    spyOn(component, 'openForm');

    const button = fixture.debugElement.nativeElement.querySelector('button');
    button.click();
    tick();
    expect(component.openForm).toHaveBeenCalled();
  }));
});
