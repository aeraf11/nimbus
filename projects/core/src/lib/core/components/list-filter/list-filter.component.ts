import { Component, Input, EventEmitter, Output, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { FilterColumn } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ni-list-filter',
  templateUrl: './list-filter.component.html',
  styleUrls: ['./list-filter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListFilterComponent {
  @Input() addButtonIcon!: string;
  @Input() addText!: string;
  @Input() addUrl?: string;
  @Input() autoFocus!: boolean;
  @Input() enableAdd!: boolean;
  @Input() enableButtonIcons!: boolean;
  @Input() enableSearch!: boolean;
  @Input() iconButtonSize!: string;
  @Input() itemCount!: number;
  @Input() listFilterAddRoute!: string;
  @Input() listFilterAddUrl?: string;
  @Input() listFilterEnableAdd!: boolean;
  @Input() searchButtonIcon!: string;
  @Input() searchDialogMaxWidth!: string;
  @Input() searchDialogWidth!: string;
  @Input() searchIcon!: string;
  @Input() searchResultOptions!: any[];
  @Input() searchSubTitle!: string;
  @Input() searchText!: string;
  @Input() title!: string;
  @Input() userFilters!: any[];

  filters: FilterColumn[] = [];
  @Output() filterRemoved: EventEmitter<FilterColumn> = new EventEmitter<FilterColumn>();
  @Output() dialogOpened: EventEmitter<Event> = new EventEmitter<Event>();

  constructor(private changeDetector: ChangeDetectorRef) {}

  setFilters(filters: FilterColumn[] | undefined): void {
    if (filters) {
      this.filters = filters;
      this.userFilters = filters.filter((filter) => filter.userCreated);
    }
  }

  setItemCount(itemCount: number): void {
    this.itemCount = itemCount;
    this.changeDetector.markForCheck();
  }

  removeFilter(filterToRemove: FilterColumn): void {
    this.filterRemoved.emit(filterToRemove);
  }

  openSearchDialog(): void {
    this.dialogOpened.emit();
  }
}
