import { Directive, Input, OnDestroy, OnInit } from '@angular/core';
import { LiveAnnouncer } from '@angular/cdk/a11y';

@Directive({
  selector: '[niAnnounce]',
})
export class AnnounceDirective implements OnInit, OnDestroy {
  @Input() message = '';

  constructor(private liveAnnouncer: LiveAnnouncer) {}

  ngOnInit(): void {
    if (this.message) {
      this.liveAnnouncer.announce(this.message, 'polite');
    }
  }

  ngOnDestroy() {
    this.liveAnnouncer.clear();
  }
}
