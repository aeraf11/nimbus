import { AnnounceDirective } from './announce.directive';
import { TestBed } from '@angular/core/testing';
import { LiveAnnouncer } from '@angular/cdk/a11y';

describe('AnnounceDirective', () => {
  let directive: AnnounceDirective;
  let liveAnnouncer: LiveAnnouncer;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AnnounceDirective],
      providers: [AnnounceDirective, LiveAnnouncer],
    });

    directive = TestBed.inject(AnnounceDirective);
    liveAnnouncer = TestBed.inject(LiveAnnouncer);
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should call liveAnnouncer.announce on ngOnInit', () => {
    spyOn(liveAnnouncer, 'announce');
    directive.message = 'Test message';
    directive.ngOnInit();
    expect(liveAnnouncer.announce).toHaveBeenCalledWith('Test message', 'polite');
  });

  it('should not call liveAnnouncer.announce if message is empty', () => {
    spyOn(liveAnnouncer, 'announce');
    directive.message = '';
    directive.ngOnInit();
    expect(liveAnnouncer.announce).not.toHaveBeenCalled();
  });

  it('should call liveAnnouncer.clear on ngOnDestroy', () => {
    spyOn(liveAnnouncer, 'clear');
    directive.ngOnDestroy();
    expect(liveAnnouncer.clear).toHaveBeenCalled();
  });
});
