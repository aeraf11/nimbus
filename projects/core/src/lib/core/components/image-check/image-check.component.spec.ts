import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageCheckComponent } from './image-check.component';

describe('ImageCheckComponent', () => {
  let component: ImageCheckComponent;
  let fixture: ComponentFixture<ImageCheckComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageCheckComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ImageCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
