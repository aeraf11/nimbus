import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'ni-image-check',
  templateUrl: './image-check.component.html',
  styleUrls: ['./image-check.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageCheckComponent implements OnInit {
  @Input() watermark: boolean | undefined;
  @Input() margins: string | undefined;
  @Input() minWidth: string | undefined;
  @Input() minHeight: string | undefined;
  @Input() width: string | undefined;
  @Input() height: string | undefined;
  @Input() maintainAspect: boolean | undefined;
  @Input() text: string | undefined;
  @Input() isRemoveOnly: boolean | undefined;
  @Input() value!: string;
  @Input() isSelected: boolean | undefined;
  @Output() removed = new EventEmitter<unknown>();
  @Output() checkChanged = new EventEmitter<void>();
  imageUrl?: string;

  ngOnInit(): void {
    if ((this.watermark === undefined || !this.watermark) && this.value) {
      this.imageUrl = '/api/v2/media/photo?id=' + encodeURIComponent(this.value);
    }
  }

  onCheckChange() {
    this.checkChanged.emit();
  }

  onRemove(): void {
    this.removed.emit(this.value);
  }
}
