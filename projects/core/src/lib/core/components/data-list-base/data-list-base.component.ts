import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Optional,
  Output,
} from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import {
  DataListOptions,
  DialogFormAction,
  FilterColumn,
  IndexedDbService,
  ListFilterService,
  ListService,
  QueueConfig,
  SignalRService,
  StateService,
  User,
  utils,
  FormsHelperService,
  DialogFormComponent,
  EntityMediaRequestComponent,
  ListFilterComponent,
} from '@nimbus/core/src/lib/core';
import { Subject } from 'rxjs';
import { filter, map, take, tap } from 'rxjs/operators';
import {
  DataListColumnDefinition,
  DataTableAction,
  DialogFilterAction,
  DialogFilterResult,
  DialogFormData,
  DialogFormResult,
  EditorMode,
} from '../../models';
import { CardListWrapperComponent } from '@nimbus/core/src/lib/core';
import { QuickSearchWrapperComponent } from '@nimbus/core/src/lib/core';

@UntilDestroy()
@Component({
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export abstract class DataListBaseComponent implements OnChanges, OnInit, OnDestroy {
  defaultPageSize = 10;

  @Input() formControl?: UntypedFormControl;
  @Input() formState?: any;

  @Input() selectMode: 'none' | 'single' | 'multi' = 'none';
  @Input() idColumn = '';
  @Input() columnDefinitions: DataListColumnDefinition[] = [];
  @Input() listName?: string;
  @Input() sortColumn?: string;
  @Input() sortDirection = 'asc';
  @Input() quickSearch = '';
  @Input() pageIndex = 0;
  @Input() pageSize = this.defaultPageSize;
  @Input() filterColumns?: FilterColumn[];
  @Input() additionalParams?: { [key: string]: string };
  @Input() showPaging = true;
  @Input() showQuickSearch = true;
  @Input() filterText = 'Filter';
  @Input() filterTitle = 'Filter Results';
  @Input() cancelText = 'Cancel';
  @Input() clearText = 'Clear';
  @Input() filterDisabled = false;
  @Input() dialogWidth = undefined;
  @Input() contextChanged?: Subject<string>;
  @Input() context?: any;
  @Input() selectedItems: any[] = [];
  @Input() savedOptionsId?: string;
  @Input() noResultsText = 'No results found';
  @Input() noResultsAction: any = undefined;
  @Input() title = '';
  @Input() icon = '';
  @Input() maxHeight = '';
  @Input() fullWidthContent = false;
  @Input() cardWrapper = false;
  @Input() options?: FormlyFormOptions;
  @Input() refreshInterval = 0;
  @Input() refreshSignalR?: { hub: string; event: string; group: string };
  @Input() disabledItems: any[] = [];
  @Input() actions?: DataTableAction[];
  @Input() enableSelectAll = false;
  @Input() metaList?: { targetCount: number; metaFilters: { filterColumns: FilterColumn[] }[] };
  @Input() isSingleContextFilter = false;

  @Input() detailDialogFormId?: string;
  @Input() detailDialogTriggerIds?: string[];
  @Input() detailDialogTitleTemplate?: string;
  @Input() detailDialogMaxWidth?: string;
  @Input() detailDialogMinWidth?: string;
  @Input() detailDialogWidth?: string;
  @Input() detailDialogHeight?: string;
  @Input() detailDialogMaxHeight?: string;
  @Input() detailDialogEnableTitle?: boolean;

  @Input() previewDialogFormId?: string;
  @Input() previewDialogTitle?: string;
  @Input() previewDialogMaxWidth?: string;
  @Input() previewDialogMinWidth?: string;
  @Input() previewDialogWidth?: string;

  @Input() requestDialogFormId?: string;
  @Input() requestDialogTitle?: string;
  @Input() requestDialogMaxWidth?: string;
  @Input() requestDialogMinWidth?: string;
  @Input() requestDialogWidth?: string;
  @Input() requestActionKey?: string;
  @Input() requestActionRouteId?: string;

  @Input() offlineStore: any = null;
  @Input() queueConfig?: QueueConfig;
  @Input() rootModel?: unknown;
  @Input() enableLoadingIndicator = true;
  @Input() isDirtyOnSelectChange = true;
  @Input() refreshId?: string;
  @Input() rowCount?: number;

  @Output() itemSelectionChanged = new EventEmitter();
  @Output() itemsUpdate = new EventEmitter<{ items: any[]; count: number }>();
  @Output() rowCountChanged = new EventEmitter<number>();

  items: any[] = [];
  itemCount = 0;
  columnIds: string[] = [];
  isLoading = false;
  userId = '';
  isAllSelected = false;
  interval: ReturnType<typeof setInterval> | null = null;
  detailDialogFormDefinition: FormlyFieldConfig[] | undefined;
  detailDialogCurrentItem: any;
  detailDialog?: DialogFormComponent;
  detailDialogDeferredItem?: 'first' | 'last';
  previewDialogFormDefinition: FormlyFieldConfig[] | undefined;
  requestDialogFormDefinition: FormlyFieldConfig[] | undefined;
  previewDialog?: DialogFormComponent;
  ignoredContextKeys: string[] = ['isGridExpanded', 'expandGrid'];

  protected onChange!: (result: unknown) => void;
  protected onTouched!: () => void;

  constructor(
    private service: ListService,
    private change: ChangeDetectorRef,
    private state: StateService,
    public formsHelper: FormsHelperService,
    @Optional() private cardListWrapper: CardListWrapperComponent,
    private listFilterService: ListFilterService,
    public dialog: MatDialog,
    @Optional() private entityMediaRequest: EntityMediaRequestComponent,
    @Optional() private listFilter: ListFilterComponent,
    @Optional() private quickSearchWrapper: QuickSearchWrapperComponent,
    private dbService: IndexedDbService,
    private signalR: SignalRService
  ) {}

  ngOnChanges(): void {
    if (this.listFilter) {
      this.listFilter.setFilters(this.filterColumns);
    }
  }

  ngOnInit(): void {
    this.columnIds = this.columnDefinitions?.map((column) => column.id) ?? [];

    if (this.cardListWrapper && this.listName) {
      this.cardListWrapper.filterEmit.pipe(untilDestroyed(this)).subscribe(() => {
        this.openFilterDialog();
      });
      this.itemsUpdate.pipe(untilDestroyed(this)).subscribe((update: any) => {
        this.cardListWrapper.setItemCount(update.count);
        this.cardListWrapper.setCurrentItems(update.items);
      });
    }
    if (this.quickSearchWrapper && this.listName) {
      this.quickSearchWrapper.filterRemoved.pipe(untilDestroyed(this)).subscribe((filterResult: DialogFilterResult) => {
        this.removeFilter(filterResult);
      });
    }

    if (this.entityMediaRequest && this.listName) {
      this.entityMediaRequest.buttonClicked.pipe(untilDestroyed(this)).subscribe(() => this.triggerPreviewDialog());
      this.itemsUpdate.pipe(untilDestroyed(this)).subscribe((update: any) => {
        this.entityMediaRequest.setItemCount(update.count);
      });
    }

    if (this.listFilter && this.listName) {
      this.listFilter.filterRemoved.pipe(untilDestroyed(this)).subscribe((filterToRemove) => {
        this.filterColumns
          ?.filter((filter) => filter.column === filterToRemove.column)
          .forEach((filter) => (filter.stringFilter = ''));
        this.getItems();

        if (filterToRemove.contextKey) {
          this.formState.context[filterToRemove.contextKey] = '';
          this.formState.contextChanged.next(filterToRemove.contextKey);
        }
      });
      this.itemsUpdate.pipe(untilDestroyed(this)).subscribe((update: any) => {
        this.listFilter.setItemCount(update.count);
      });
    }

    if (this.quickSearchWrapper && this.listName) {
      this.quickSearchWrapper.quickSearchChange.pipe(untilDestroyed(this)).subscribe((result) => {
        this.onFilterChange(result);
      });
    }
    if (this.savedOptionsId) {
      this.state
        .selectDataListOptions(this.savedOptionsId)
        .pipe(take(1))
        .subscribe((options) => {
          if (options) {
            if (this.filterColumns) {
              options = { ...options, filterColumns: this.filterColumns };
            }
            if (this.additionalParams) {
              options = { ...options, additionalParams: this.additionalParams };
            }
            this.setOptions(options);
          }
        });
    }

    this.state
      .selectUser()
      .pipe(
        take(1),
        tap((user: User | null) => {
          this.userId = user?.userProfileId ?? '';
        })
      )
      .subscribe();

    // Every time the context changes we want to check for changes
    // As certain actions may show/hide based off of context
    this.formState?.contextChanged?.pipe(untilDestroyed(this)).subscribe(() => this.change.markForCheck());

    if (this.cardListWrapper?.currentItems?.length > 0) {
      this.itemCount = this.cardListWrapper?.itemCount;
      this.items = [...this.cardListWrapper.currentItems];

      this.change.markForCheck();
      this.change.detectChanges();
    } else if (this.quickSearchWrapper?.props['hideItemsUntilSearched']) {
      // Do not get list items
    } else {
      const contextFilterColumns = this.filterColumns?.filter((filter) => filter.contextKey) ?? [];

      if (contextFilterColumns.length > 0 && this.contextChanged) {
        this.handleInitialContext(contextFilterColumns);
        this.contextChanged.pipe(untilDestroyed(this)).subscribe((key) => this.handleContextKeyChanged(key));
      } else {
        this.getItems();
      }
    }

    if (this.detailDialogFormId) {
      this.state
        .selectForm(this.detailDialogFormId)
        .pipe(
          untilDestroyed(this),
          map((configs) => configs?.fields)
        )
        .subscribe((formDef) => {
          return (this.detailDialogFormDefinition = formDef);
        });
    }

    if (this.previewDialogFormId) {
      this.state
        .selectForm(this.previewDialogFormId)
        .pipe(
          untilDestroyed(this),
          map((configs) => configs?.fields)
        )
        .subscribe((formDef) => (this.previewDialogFormDefinition = formDef));
    }

    if (this.requestDialogFormId) {
      this.state
        .selectForm(this.requestDialogFormId)
        .pipe(
          untilDestroyed(this),
          map((configs) => configs?.fields)
        )
        .subscribe((formDef) => (this.requestDialogFormDefinition = formDef));
    }

    this.service
      .reloadEvent()
      .pipe(
        filter((id: string) => this.refreshId === id || this.listName === id),
        untilDestroyed(this)
      )
      .subscribe(() => this.getItems());

    this.setupRefresh();
  }

  hasSameValues(a: { [k: string]: any }, b: { [k: string]: any }) {
    return Object.values(a).every((v, i) => v === Object.values(b)[i]);
  }

  selectItem(item: any): void {
    if (this.selectMode === 'none' || item.isDisabled) {
      return;
    }
    if (this.selectMode === 'single') {
      this.handleSingleItemSelect(item);
    } else {
      this.handleMultipleItemSelect(item);
    }

    this.isAllSelected = this.getSelectedItemsOnPage().length === this.items.length;

    this.selectionChanged();
  }

  clearSelection(): void {
    this.selectedItems = [];
    this.selectionChanged();
  }

  selectAllItemsOnPage(): void {
    const currentlySelectedOnPage = this.getSelectedItemsOnPage();

    // If all items are selected on a page, select each item to deselect them
    if (currentlySelectedOnPage.length === this.items.length) {
      currentlySelectedOnPage.forEach((item) => this.selectItem(item));
      this.isAllSelected = false;
    }

    // If no items are selected on a page, select them all
    if (currentlySelectedOnPage.length === 0) {
      this.items.forEach((item) => this.selectItem(item));
      this.isAllSelected = true;
    }

    // If some items are selected on a page, select the ones that are not selected
    if (currentlySelectedOnPage.length > 0 && currentlySelectedOnPage.length < this.items.length) {
      this.items.forEach((item) => {
        if (!this.isItemChecked(item)) {
          this.selectItem(item);
        }
      });
      this.isAllSelected = true;
    }
  }

  getSelectedItemsOnPage(): any[] {
    return this.items.filter((item) => this.isItemChecked(item));
  }

  private handleSingleItemSelect(item: any): void {
    if (item === this.selectedItems?.[0]) {
      this.clearSelection();
    } else {
      this.clearSelection();
      this.selectedItems.push(item);
      this.selectionChanged();
    }
  }

  private handleMultipleItemSelect(item: any): void {
    if (this.idColumn) {
      if (!this.selectedItems) {
        this.selectedItems = [];
      }
      const index = this.selectedItems.findIndex((val: any) =>
        this.hasSameValues([this.idColumn], item[this.idColumn])
      );
      if (index === -1) {
        this.selectedItems.push(item);
      } else {
        this.selectedItems.splice(index, 1);
      }
    }
    this.selectionChanged();
  }

  selectionChanged() {
    if (this.selectMode === 'none') return;
    const result = this.selectMode === 'multi' ? this.selectedItems : this.selectedItems[0];
    this.change.detectChanges();
    if (this.isDirtyOnSelectChange) {
      this.onChange(result);
      this.onTouched();
    }
    this.itemSelectionChanged.emit();
  }

  isItemChecked(item: any): boolean {
    if (this.selectedItems && this.selectedItems.length > 0 && this.selectedItems[0] !== null) {
      return this.selectedItems?.filter((val: any) => val[this.idColumn] === item[this.idColumn]).length === 1;
    }
    return false;
  }

  getItems(filterColumns?: FilterColumn[]): void {
    this.setLoading(true);
    this.change.markForCheck();
    const options = this.getOptions();
    const resolvedFilterColumns = filterColumns || options.filterColumns || this.filterColumns;
    if (this.listName) {
      this.service
        .getList(this.listName, this.columnIds, { ...options, filterColumns: resolvedFilterColumns }, this.metaList)
        .pipe(untilDestroyed(this))
        .subscribe((result) => {
          this.itemCount = result.filteredRowCount;
          this.items = result.rows.map((row) => ({
            ...row,
            isEditable: row.createdBy ? row.createdBy === this.userId : true,
            isDisabled: this.isItemDisabled(row),
          }));
          this.filterColumns = resolvedFilterColumns;
          this.setLoading(false);
          this.itemsUpdate.emit({ items: this.items, count: this.itemCount });

          this.isAllSelected = this.getSelectedItemsOnPage().length === this.items.length;

          if (this.detailDialog && this.detailDialogDeferredItem) {
            if (this.detailDialogDeferredItem === 'first') {
              this.detailDialogCurrentItem = this.items[0];
            } else {
              this.detailDialogCurrentItem = this.items[this.items.length - 1];
            }
            this.detailDialog.loading = false;
            this.detailDialogDeferredItem = undefined;
            this.updateDetailDialogItem();
          }
          this.rowCountChanged.emit(this.itemCount);
          this.change.markForCheck();
        });
    }

    if (this.offlineStore) {
      if (!this.offlineStore.table) {
        console.error('You are missing the "table" property in your offlineStore config');
      }

      if (!this.offlineStore.route) {
        console.error('You are missing the "route" property in your offlineStore config');
      }

      this.dbService
        .table(this.offlineStore.table)
        .where('routeId')
        .startsWith(this.offlineStore.route)
        .toArray()
        .then((result: any) => {
          this.itemCount = result.length;
          this.items = result.map((row: any) => ({
            offlineId: row.id,
            ...utils.flattenObject(row.model),
            routeId: row.routeId,
            dateSaved: row.dateSaved,
            payload: row.model,
          }));
          this.items = this.items.sort((a: any, b: any) => {
            return (new Date(b.dateSaved.dateTime) as any) - (new Date(a.dateSaved.dateTime) as any);
          });
          this.setLoading(false);
          this.change.markForCheck();
        })
        .catch((error: any) => {
          console.error(error?.message ?? error);
          this.itemCount = 0;
          this.items = [];
          this.setLoading(false);
          this.change.markForCheck();
        });
    }

    if (this.queueConfig) {
      this.dbService
        .getAllAttachments(this.queueConfig.queueTableName)
        .pipe(take(1))
        .subscribe((items: unknown[]) => {
          this.itemCount = items.length;
          this.items = items;
          this.setLoading(false);
          this.change.markForCheck();
        });
    }

    if (!this.listName && !this.offlineStore && !this.queueConfig) {
      console.error('You are missing "listName","offlineStore" or "queueStore" in your props');
    }
  }

  isItemDisabled(item: any): boolean | undefined {
    if (this.disabledItems.length < 1 || this.selectMode === 'none') {
      return;
    }
    return this.disabledItems?.filter((val: any) => val[this.idColumn] === item[this.idColumn]).length === 1;
  }

  getOptions(): DataListOptions {
    return {
      id: this.savedOptionsId,
      sortColumn: this.sortColumn || this.columnIds[this.selectMode === 'none' ? 0 : 1],
      sortDirection: this.sortDirection || 'asc',
      quickSearch: this.quickSearch,
      pageIndex: this.pageIndex || 0,
      pageSize: this.pageSize || this.defaultPageSize,
      filterColumns: this.filterColumns,
      additionalParams: this.additionalParams,
    };
  }

  setOptions(options: DataListOptions) {
    if (options) {
      this.sortColumn = options.sortColumn;
      this.sortDirection = options.sortDirection;
      this.quickSearch = options.quickSearch;
      this.pageIndex = options.pageIndex;
      this.pageSize = options.pageSize;
      this.filterColumns = options.filterColumns;
      this.additionalParams = options.additionalParams;
    }
  }

  saveOptions() {
    this.state.saveOptions(this.getOptions());
  }

  getValue(item: any, column: any): any {
    if (column.computedValue) {
      const itemProp = column.computedValue.itemProp;
      let model;
      if (itemProp) {
        model = JSON.parse(item[itemProp])?.[0] ?? {};
      } else {
        model = item;
      }
      const result = this.formsHelper.parseTemplate(column.computedValue, model, '');
      return result;
    }
    if (column.expressionValue) {
      const expression = utils.evalStringFormExpression<string>(column.expressionValue);
      if (expression) return expression(this.formState.rootModel, item, this.formState.context, this.formsHelper);
    }
    return item[column.id];
  }

  onFilterChange(result: DialogFilterResult) {
    this.pageIndex = 0;
    this.filterColumns = result.filterColumns ?? this.filterColumns;
    this.quickSearch = result.quickSearch;
    this.getItems();
    this.selectionChanged();
    this.saveOptions();

    if (this.cardListWrapper) {
      this.cardListWrapper.setCurrentFilters(this.filterColumns);
    }
    if (this.quickSearchWrapper) {
      this.quickSearchWrapper.setCurrentFilters({
        ...result,
        filterColumns: result.filterColumns ?? this.filterColumns,
      });
    }
  }

  pageChange(event: PageEvent): void {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getItems();
    this.selectionChanged();
    this.saveOptions();

    if (this.cardListWrapper) {
      this.cardListWrapper.setCurrentPage(this.pageIndex);
      this.cardListWrapper.setCurrentPageSize(this.pageSize);
    }
  }

  itemClicked(item: any, column: any) {
    if (this.detailDialogFormDefinition) {
      if (
        !this.detailDialogTriggerIds ||
        this.detailDialogTriggerIds.length === 0 ||
        this.detailDialogTriggerIds.includes(column.id)
      ) {
        this.triggerDetailDialog(item);
      }
    }
  }

  isFieldClickable(item: any, column: any): boolean {
    return (
      !!this.detailDialogFormDefinition &&
      (!this.detailDialogTriggerIds ||
        this.detailDialogTriggerIds.length === 0 ||
        this.detailDialogTriggerIds.includes(column.id))
    );
  }

  triggerDetailDialog(model: any) {
    this.change.markForCheck();
    this.detailDialogCurrentItem = model;

    const data: DialogFormData = {
      model,
      fieldConfig: this.detailDialogFormDefinition,
      config: {
        enableDelete: false,
        enableEdit: false,
        titleTemplate: this.detailDialogTitleTemplate,
        isChildForm: true,
        hideTitle: !this.detailDialogEnableTitle,
      },
      disabled: false,
      options: this.options,
      context: { isEdit: false },
      editorMode: EditorMode.Select,
      hasNextItem: this.getDetailDialogHasNextItem(),
      hasPreviousItem: this.getDetailDialogHasPreviousItem(),
      isSelected: this.isItemChecked(model),
    };
    const dialogConfig: MatDialogConfig<DialogFormData> = { data };
    if (this.detailDialogMaxWidth) {
      dialogConfig.maxWidth = this.detailDialogMaxWidth;
    }
    if (this.detailDialogMinWidth) {
      dialogConfig.minWidth = this.detailDialogMinWidth;
    }
    if (this.detailDialogWidth) {
      dialogConfig.width = this.detailDialogWidth;
    }
    if (this.detailDialogHeight) {
      dialogConfig.height = this.detailDialogHeight;
    }
    if (this.detailDialogMaxHeight) {
      dialogConfig.maxHeight = this.detailDialogMaxHeight;
    }
    const dialogRef = this.dialog.open(DialogFormComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result: DialogFormResult) => {
      this.change.markForCheck();
    });

    this.detailDialog = dialogRef.componentInstance;
    this.detailDialog.previousItem.pipe(untilDestroyed(this)).subscribe(() => {
      this.doDetailDialogPreviousItem();
    });

    this.detailDialog.nextItem.pipe(untilDestroyed(this)).subscribe(() => {
      this.doDetailDialogNextItem();
    });

    this.detailDialog.selectionChanged.pipe(untilDestroyed(this)).subscribe(() => {
      this.selectItem(this.detailDialogCurrentItem);
    });
  }

  updateDetailDialogItem() {
    if (this.detailDialog) {
      this.detailDialog.isSelected = this.isItemChecked(this.detailDialogCurrentItem);
      this.detailDialog.model = this.detailDialogCurrentItem;
      this.detailDialog.hasPreviousItem = this.getDetailDialogHasPreviousItem();
      this.detailDialog.hasNextItem = this.getDetailDialogHasNextItem();
      this.detailDialog.change.markForCheck();
    }
  }

  doDetailDialogNextItem(): any {
    if (!this.getDetailDialogHasNextItem()) return null;
    if (this.getDetailDialogLastInPage()) {
      this.detailDialogDeferredItem = 'first';
      const pageIndex = (this.pageIndex || 0) + 1;
      this.pageChange({ pageIndex, pageSize: this.pageSize, length: this.itemCount });
      if (this.detailDialog) {
        this.detailDialog.loading = true;
      }
    } else {
      const index = this.items.findIndex((x) => this.hasSameValues(x, this.detailDialogCurrentItem));
      this.detailDialogCurrentItem = this.items[index + 1];
      this.updateDetailDialogItem();
    }
  }

  doDetailDialogPreviousItem(): any {
    if (!this.getDetailDialogHasPreviousItem()) return null;
    if (this.getDetailDialogFirstInPage()) {
      this.detailDialogDeferredItem = 'last';
      const pageIndex = (this.pageIndex || 0) - 1;
      this.pageChange({ pageIndex, pageSize: this.pageSize, length: this.itemCount });
      if (this.detailDialog) {
        this.detailDialog.loading = true;
      }
    } else {
      const index = this.items.findIndex((x) => this.hasSameValues(x, this.detailDialogCurrentItem));
      this.detailDialogCurrentItem = this.items[index - 1];
      this.updateDetailDialogItem();
    }
  }

  getDetailDialogFirstInPage() {
    return this.items.findIndex((x) => this.hasSameValues(x, this.detailDialogCurrentItem)) === 0;
  }

  getDetailDialogLastInPage() {
    return (
      this.items.findIndex((x) => this.hasSameValues(x, this.detailDialogCurrentItem)) ===
      (this.pageSize || this.defaultPageSize) - 1
    );
  }

  getDetailDialogHasNextItem(): boolean {
    return this.getDetailDialogItemFullIndex() < this.itemCount - 1;
  }

  getDetailDialogHasPreviousItem(): boolean {
    return this.getDetailDialogItemFullIndex() > 0;
  }

  getDetailDialogItemFullIndex(): number {
    const index = this.items.findIndex((x) => {
      return this.hasSameValues(x, this.detailDialogCurrentItem);
    });
    return (this.pageSize || this.defaultPageSize) * (this.pageIndex || 0) + index;
  }

  triggerPreviewDialog(): void {
    this.change.markForCheck();

    const data: DialogFormData = {
      model: { items: this.selectedItems },
      fieldConfig: this.previewDialogFormDefinition,
      config: {
        enableDelete: false,
        enableEdit: false,
        titleTemplate: this.previewDialogTitle,
        isChildForm: true,
      },
      disabled: false,
      options: this.options,
      context: { isEdit: false },
      editorMode: EditorMode.View,
    };
    const dialogConfig: MatDialogConfig<DialogFormData> = { data };

    if (this.previewDialogMaxWidth) {
      dialogConfig.maxWidth = this.previewDialogMaxWidth;
    }

    if (this.previewDialogMinWidth) {
      dialogConfig.minWidth = this.previewDialogMinWidth;
    }

    if (this.previewDialogWidth) {
      dialogConfig.width = this.previewDialogWidth;
    }

    const dialogRef = this.dialog.open(DialogFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(() => this.change.markForCheck());

    this.previewDialog = dialogRef.componentInstance;
    this.previewDialog.selectionChanged.pipe(untilDestroyed(this)).subscribe((item: any) => {
      const itemToSelect = this.selectedItems.filter((filterItem) => filterItem[this.idColumn] === item)[0];
      if (itemToSelect && this.previewDialog) {
        this.selectItem(itemToSelect);
        this.previewDialog.model = { items: this.selectedItems };
        this.previewDialog.change.markForCheck();
      }
    });

    this.previewDialog.dialogOpen.pipe(untilDestroyed(this)).subscribe(() => {
      this.triggerRequestDialog();
    });
  }

  triggerRequestDialog(): void {
    this.change.markForCheck();

    const data: DialogFormData = {
      model: { items: this.selectedItems },
      fieldConfig: this.requestDialogFormDefinition,
      config: {
        enableDelete: false,
        enableEdit: false,
        titleTemplate: this.requestDialogTitle,
        isChildForm: true,
      },
      disabled: false,
      options: this.options,
      context: { isEdit: false },
      editorMode: EditorMode.Form,
      formActionKey: this.requestActionKey,
      formRouteId: this.requestActionRouteId,
    };
    const dialogConfig: MatDialogConfig<DialogFormData> = { data };

    if (this.requestDialogMaxWidth) {
      dialogConfig.maxWidth = this.requestDialogMaxWidth;
    }

    if (this.requestDialogMinWidth) {
      dialogConfig.minWidth = this.requestDialogMinWidth;
    }

    if (this.requestDialogWidth) {
      dialogConfig.width = this.requestDialogWidth;
    }

    const dialogRef = this.dialog.open(DialogFormComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result: DialogFormResult) => {
      if (this.previewDialog && result.action === DialogFormAction.Submit) {
        this.previewDialog.dialogRef.close();

        this.formControl?.reset();

        this.service.reloadList('Submissions');

        this.change.markForCheck();
      }
    });

    const formComponent = dialogRef.componentInstance;
    formComponent.selectionChanged.pipe(untilDestroyed(this)).subscribe((item: any) => {
      this.selectItem(item);

      if (this.previewDialog) {
        this.previewDialog.model = { ...this.previewDialog.model, ...{ items: this.selectedItems } };
        this.previewDialog.options?.formState?.runChangeDetection();
      }

      formComponent.model = { ...formComponent.model, ...{ items: this.selectedItems } };
      formComponent.change.markForCheck();
    });
  }

  writeValue(value: any): void {
    if (value) {
      if (this.selectMode === 'multi') {
        this.selectedItems = value;
        this.isAllSelected = this.getSelectedItemsOnPage().length === this.items.length;
      }
      if (this.selectMode === 'single') {
        this.selectedItems = [value];
      }
    } else {
      this.selectedItems = [];
    }

    this.change.markForCheck();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  ngOnDestroy(): void {
    if (this.interval) {
      clearInterval(this.interval);
      this.interval = null;
    }
    if (this.refreshSignalR) {
      this.signalR.leaveGroup(this.refreshSignalR.hub, this.refreshSignalR.group);
    }
  }

  private openFilterDialog(): void {
    const config = {
      filterText: this.filterText,
      cancelText: this.cancelText,
      clearText: this.clearText,
      title: this.filterTitle,
      columnDefinitions: this.columnDefinitions,
      lastFilters: this.filterColumns,
      isDisabled: false,
      width: '700px',
    };

    const ref = this.listFilterService.openDialog(config, this.options ?? {});

    ref
      ?.afterClosed()
      .pipe(take(1))
      .subscribe((result: DialogFilterResult) => {
        if (result.action === DialogFilterAction.Filter || result.action === DialogFilterAction.Clear) {
          result.filterColumns = this.mergeFilters(result.filterColumns ?? []);
          this.onFilterChange(result);
        }
      });
  }

  private mergeFilters(filterResult: FilterColumn[]) {
    const filterNames = filterResult.map((filter) => filter.column);
    let existingFilters =
      this.filterColumns?.filter((filter) => !filter.userCreated && !filterNames.includes(filter.column)) ?? [];
    existingFilters = existingFilters.concat(filterResult);
    return existingFilters;
  }

  removeFilter(filter: DialogFilterResult) {
    this.onFilterChange(filter);
  }

  private setupRefresh(): void {
    if (this.refreshInterval > 0) {
      this.interval = setInterval(() => {
        this.getItems();
      }, this.refreshInterval);
    }
    if (this.refreshSignalR) {
      const event = this.signalR.getEvent(
        this.refreshSignalR.hub,
        this.refreshSignalR.event,
        this.refreshSignalR.group
      );
      if (event) {
        event.pipe(untilDestroyed(this)).subscribe(() => {
          this.getItems();
        });
      }
    }
  }

  private handleInitialContext(contextFilterColumns: FilterColumn[]): void {
    const initialContextColumns = contextFilterColumns.filter((col) => col.isInitialContext && col.contextKey);

    if (initialContextColumns?.length > 0) {
      // Set the stringFilter for each column with the contexts contextKey value
      initialContextColumns.forEach((col) => {
        const filter = this.context[col.contextKey as string];
        col.stringFilter = filter ?? '';
      });

      // Then retrieve the items with the stringFilters set for all our context columns
      this.getItems(initialContextColumns);
    }
  }

  private handleContextKeyChanged(key: string): void {
    if (this.ignoredContextKeys.indexOf(key) !== -1) {
      return;
    }

    if (this.isSingleContextFilter) {
      const otherContexts = this.filterColumns?.filter((col) => col.contextKey !== key);
      otherContexts?.forEach((context) => {
        context.stringFilter = '';
        if (context.contextKey && this.formState && this.formState.context) {
          this.formState.context[context.contextKey] = '';
        }
      });
    }

    this.handleMyOperationsContext(key);

    const contextCol = this.filterColumns?.find((col) => col.contextKey === key);
    if (contextCol) {
      const filter = this.context[key];
      if (filter) {
        contextCol.stringFilter = filter;
        this.getItems();
      } else {
        this.change.detectChanges();
      }
    }
  }

  /**
   * Conditional logic for MyOperations list
   * Handles setting filterColumns around how an operation is linked to a current user
   */
  private handleMyOperationsContext(key: string): void {
    if (this.listName === 'MyOperations' && key === 'linkCase') {
      const currentNonLinkFilters = this.filterColumns?.filter(
        (col) => col.column !== 'createdByMe' && col.column !== 'assignedToMe' && col.column !== 'linkedToMe'
      );

      this.filterColumns = currentNonLinkFilters;

      if (this.context['linkCase']) {
        this.filterColumns?.push({
          column: this.context['linkCase'],
          constantFilter: true,
          stringFilter: '1',
          displayValue: this.getMyOperationsColumnDisplayValue(this.context['linkCase']),
          userCreated: true,
          isExactMatchOverride: true,
        } as FilterColumn);
      }

      if (this.listFilter) {
        this.listFilter.setFilters(this.filterColumns);
      }

      this.getItems();
    }
  }

  private getMyOperationsColumnDisplayValue(columnName: string): string {
    switch (columnName) {
      case 'createdByMe':
        return 'Created by me';
      case 'assignedToMe':
        return 'Assigned to me';
      case 'linkedToMe':
        return 'Linked to me';
      default:
        return '';
    }
  }

  private setLoading(isLoading: boolean) {
    if (this.enableLoadingIndicator) this.isLoading = isLoading;
  }
}
