import { Component, OnInit, ChangeDetectionStrategy, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogListData } from '../../models';

@Component({
  selector: 'ni-dialog-list',
  templateUrl: './dialog-list.component.html',
  styleUrls: ['./dialog-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class DialogListComponent implements OnInit {
  dialogData?: DialogListData;

  constructor(
    private dialogRef: MatDialogRef<DialogListComponent>,
    @Inject(MAT_DIALOG_DATA) private data: DialogListData
  ) {}

  ngOnInit(): void {
    this.dialogData = this.data;
  }

  close(): void {
    this.dialogRef.close();
  }
}
