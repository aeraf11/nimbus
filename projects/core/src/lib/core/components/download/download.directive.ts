import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { HttpClient } from '@angular/common/http';
import { Directive, Input, HostListener, ElementRef } from '@angular/core';

@UntilDestroy()
@Directive({
  selector: '[phDownload]',
})
export class DownloadDirective {
  @Input() url?: string;
  @Input() name = 'download.json';
  @Input() responseType = 'text';
  @Input() mimeType = 'application/json';

  constructor(private ref: ElementRef, private httpClient: HttpClient) {
    ref.nativeElement.href = 'javascript:void(0)';
  }

  @HostListener('click', ['$event.isTrusted'])
  onClick(user: boolean): void {
    if (user) {
      if (this.url) {
        this.httpClient
          .get(this.url, { responseType: <any>this.responseType })
          .pipe(untilDestroyed(this))
          .subscribe((data) => {
            const blob = new Blob([<any>data], { type: this.mimeType });
            const downloadURL = URL.createObjectURL(blob);
            this.ref.nativeElement.href = downloadURL;
            this.ref.nativeElement.download = this.name;
            this.ref.nativeElement.click();
          });
      }
    }
  }
}
