import { ChangeDetectionStrategy, Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { MediaService } from '../../services';
import { DataType } from '../../models';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { Router } from '@angular/router';
import { OnChange } from 'property-watch-decorator';
import { UntypedFormGroup } from '@angular/forms';
import { DialogFormComponent } from '../dialog-form/dialog-form.component';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'ni-unidentified-subject-detail',
  templateUrl: './unidentified-subject-detail.component.html',
  styleUrls: ['./unidentified-subject-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnidentifiedSubjectDetailComponent implements OnInit {
  @OnChange(function (this: UnidentifiedSubjectDetailComponent, value: UnidentifiedSubject) {
    this.isLoading = true;
    if (value) {
      if (value.mediaJson && this.unidentifiedSubject) {
        this.unidentifiedSubjectMedia = this.unidentifiedSubject.mediaJson
          ? JSON.parse(this.unidentifiedSubject.mediaJson)
          : [];
        if (this.unidentifiedSubjectMedia && this.unidentifiedSubjectMedia.length > 0) {
          this.setImageUrls(this.unidentifiedSubjectMedia);
        }
        setTimeout(() => {
          this.isLoading = false;
          this.cd.markForCheck();
        }, 0);
      }
      if (this.unidentifiedSubjectMedia && this.unidentifiedSubjectMedia.length > 0) {
        this.setImageUrls(this.unidentifiedSubjectMedia);
        this.currentMedia = this.getDefaultMedia(this.unidentifiedSubjectMedia);
      }
    }
  })
  @Input()
  unidentifiedSubject?: UnidentifiedSubject;

  currentMediaIndex = 0;

  formGroup = new UntypedFormGroup({});

  unidentifiedSubjectMedia?: EntityMedia[];

  changeDetection() {
    this.cd.detectChanges();
  }

  fieldConfigs: FormlyFieldConfig[] = [
    {
      type: 'repeat',
      props: {
        enableCarousel: true,
        carouselControlSize: '20px',
        carouselControlIndent: '5px',
        contentPosition: 'center center',
        carouselDirection: 'horizontal',
        carouselMaxHeight: '550px',
        carouselMaxWidth: '100%',
        carouselControlDirection: 'horizontal',
        carouselItemCount: 1,
        carouselItemWidth: '100%',
        carouselItemHeight: '550px',
        layoutType: 'flex',
        carouselGroupControls: true,
        showPips: true,
        carouselIndex: 0,
      },
      fieldArray: {
        fieldGroup: [
          {
            key: 'blobId',
            type: 'imageview',
            props: {
              text: 'Unidentified Subject Photo',
              tabindex: 0,
              height: '550px',
              width: 'fit-content',
              maintainAspectRatio: true,
            },
            expressions: {
              hide: (field: FormlyFieldConfig) => field.model?.mediaType !== 1,
            },
          },
          {
            key: 'entityMediaId',
            type: 'video',
            expressions: {
              hide: (field: FormlyFieldConfig) => field.model?.mediaType !== 2,
            },
            props: {},
          },
        ],
      },
    },
  ];

  mediaType = MediaType;
  dataType = DataType;
  currentMedia?: EntityMedia;
  breakpoint$ = this.media.getBreakpoint();
  isLoading = false;
  dialogRef: MatDialogRef<DialogFormComponent> | undefined;

  ngOnInit(): void {
    this.isLoading = true;
    if (this.unidentifiedSubject && this.unidentifiedSubject.mediaJson) {
      this.unidentifiedSubjectMedia = this.unidentifiedSubject.mediaJson
        ? JSON.parse(this.unidentifiedSubject.mediaJson)
        : [];
      if (this.unidentifiedSubjectMedia && this.unidentifiedSubjectMedia.length > 0) {
        this.setImageUrls(this.unidentifiedSubjectMedia);
        this.currentMedia = this.getDefaultMedia(this.unidentifiedSubjectMedia);
      }
    }
  }

  constructor(private media: MediaService, private router: Router, private cd: ChangeDetectorRef) {}

  isEntityMediaLike(valueToTest: any): valueToTest is EntityMedia {
    return (
      valueToTest &&
      'reference' in valueToTest &&
      'entityMediaId' in valueToTest &&
      'blobId' in valueToTest &&
      'thumbnailBlobId' in valueToTest &&
      'webSizeBlobId' in valueToTest &&
      'progress' in valueToTest &&
      'isLocked' in valueToTest &&
      'isDefault' in valueToTest &&
      'isReference' in valueToTest &&
      'description' in valueToTest &&
      'mediaType' in valueToTest &&
      'height' in valueToTest &&
      'width' in valueToTest &&
      'thumbnailUrl' in valueToTest &&
      'imageUrl' in valueToTest &&
      'tags' in valueToTest
    );
  }

  isEntityMediaArrayLike(array: any[] | undefined): array is EntityMedia[] {
    if (array && array.length > 0) {
      return this.isEntityMediaLike(array[0]);
    } else {
      return false;
    }
  }

  getDefaultMedia(value: EntityMedia[]): EntityMedia | undefined {
    const defaultMedia = value.find((m: EntityMedia) => m.isDefault);
    if (defaultMedia) {
      return defaultMedia;
    } else {
      return value[0];
    }
  }

  setImageAndThumbnailUrls(media: EntityMedia): void {
    media.imageUrl = this.getImageUrl(media.blobId);
    media.thumbnailUrl = this.getImageUrl(media.thumbnailBlobId);
  }

  setImageUrls(media: EntityMedia[]): void {
    if (media && media.length > 0) {
      media.forEach((m) => {
        this.setImageAndThumbnailUrls(m);
      });
    }
  }

  getImageUrl(blobId: string | undefined): string | undefined {
    if (blobId) {
      return '/api/v2/media/photo?id=' + encodeURIComponent(blobId);
    } else return undefined;
  }

  setCurrentMedia(media: EntityMedia): void {
    this.currentMedia = media;
    if (this.unidentifiedSubject && this.unidentifiedSubjectMedia) {
      this.currentMediaIndex = this.unidentifiedSubjectMedia.indexOf(media);
    }
  }

  identify() {
    setTimeout(() => {
      this.router.navigateByUrl(`ident/add?unidentifiedSubjectId=${this.unidentifiedSubject?.unidentifiedSubjectId}`);
    }, 100);
  }
}

export interface UnidentifiedSubject {
  unidentifiedSubjectId: string;
  crimeNumber: string;
  unitId: string;
  offenceDate: '05-06-2023 01:00:00';
  crimeTypeId: string;
  premisesNumber: string;
  premisesName: string;
  premisesAddress: string;
  reference: string;
  photosJson: string;
  videosJson: string;
  mediaJson: string;
}

export interface EntityMedia {
  reference: string;
  entityMediaId: string;
  blobId: string;
  thumbnailBlobId: string;
  webSizeBlobId: string;
  progress: number;
  isLocked: boolean;
  isDefault: boolean;
  isReference: boolean;
  description: string;
  mediaType: MediaType;
  height: number;
  width: number;
  thumbnailUrl?: string;
  imageUrl?: string;
  tags: Tag[];
  mediaTypeId: number;
}

export interface Tag {
  displayValue: string;
  cssClass: string;
  hierarchicalDisplayValue: string;
}

export enum MediaType {
  Image = 1,
  Video = 2,
  Audio = 3,
  Sketch = 4,
}
