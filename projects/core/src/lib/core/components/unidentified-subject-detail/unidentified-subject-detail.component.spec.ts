import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnidentifiedSubjectDetailComponent } from './unidentified-subject-detail.component';

describe('UnidentifiedSubjectDetailComponent', () => {
  let component: UnidentifiedSubjectDetailComponent;
  let fixture: ComponentFixture<UnidentifiedSubjectDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UnidentifiedSubjectDetailComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(UnidentifiedSubjectDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
