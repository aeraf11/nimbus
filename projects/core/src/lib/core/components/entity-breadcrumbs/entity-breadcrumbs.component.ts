import { ChangeDetectionStrategy, Component, Input, OnChanges } from '@angular/core';
import { EntityBreadcrumb } from '../../models';

@Component({
  selector: 'ni-entity-breadcrumbs',
  templateUrl: './entity-breadcrumbs.component.html',
  styleUrls: ['./entity-breadcrumbs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntityBreadcrumbsComponent implements OnChanges {
  @Input() breadcrumbs: EntityBreadcrumb[] = [];

  ngOnChanges(): void {
    this.addIcon();
  }

  private addIcon(): void {
    this.breadcrumbs = this.breadcrumbs.map((breadcrumb) => ({
      ...breadcrumb,
      ...{ icon: this.getIconForEntity(breadcrumb.EntityTypeName) },
    }));
  }

  private getIconForEntity(name: string): string {
    switch (name) {
      case 'Operation':
        return 'fas fa-cubes';
      case 'Submission':
        return 'fas fa-cube';
      case 'Exhibit':
        return 'fas fa-tag';
      case 'Person':
        return 'far fa-address-card';
      case 'Location':
        return 'ni ni-bullseye';
      case 'Note':
        return 'fab fa-google-wallet';
      case 'Attachment':
        return 'fas fa-paperclip';
      case 'CrimeScene':
        return 'fas fa-map-marker-exclamation';
      case 'Examination':
        return 'fas fa-microscope';
      default:
        return 'fas fa-circle';
    }
  }
}
