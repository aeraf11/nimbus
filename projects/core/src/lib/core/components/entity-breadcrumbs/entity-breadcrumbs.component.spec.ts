import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntityBreadcrumbsComponent } from './entity-breadcrumbs.component';

describe('EntityBreadcrumbsComponent', () => {
  let component: EntityBreadcrumbsComponent;
  let fixture: ComponentFixture<EntityBreadcrumbsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EntityBreadcrumbsComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntityBreadcrumbsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
