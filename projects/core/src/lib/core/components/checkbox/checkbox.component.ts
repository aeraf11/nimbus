import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  ViewChild,
  ChangeDetectorRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatCheckbox } from '@angular/material/checkbox';

@Component({
  selector: 'ni-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: CheckboxComponent,
      multi: true,
    },
  ],
})
export class CheckboxComponent implements AfterViewInit, ControlValueAccessor {
  @Input() id = '';
  @Input() tabIndex: number | undefined;
  @Input() indeterminate = true;
  @Input() color = 'accent';
  @Input() labelPosition: 'before' | 'after' = 'before';
  @Input() label = '';
  @Input() labelClass = '';
  @Input() isRequired = false;
  @Input() hideRequiredMarker = true;

  constructor(private change: ChangeDetectorRef) {}

  private onChange!: (value: boolean) => void;

  inputElement: ElementRef<HTMLInputElement> | undefined;
  @ViewChild(MatCheckbox, { static: true }) checkbox!: MatCheckbox;

  isChecked: boolean | undefined;
  isDisabled = false;

  focus(): void {
    this.checkbox.focus();
  }

  ngAfterViewInit(): void {
    this.inputElement = this.checkbox._inputElement;
  }

  valueChanged(value: boolean) {
    this.onChange(value);
  }

  writeValue(value: boolean) {
    if (this.checkbox) {
      this.isChecked = value;
      this.change.markForCheck();
    }
  }

  registerOnChange(fn: (value: boolean) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(_fn: () => void): void {
    return;
  }

  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
