import { Component, Input } from '@angular/core';

@Component({
  selector: 'ni-case-summary',
  templateUrl: './case-summary.component.html',
  styleUrls: ['./case-summary.component.scss'],
})
export class CaseSummaryComponent {
  @Input() operationId: string | undefined;
  @Input() operationName: string | undefined;
  @Input() operationReference: string | undefined;
  @Input() crimeType: string | undefined;
  @Input() creationDate: string | undefined;

  onEnter(url: string) {
    window.location.pathname = url; // change this to use router once we have ng route for details
  }
}
