import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockIndexedDbService } from '../../services/indexeddb.service.spec';
import { FileManagerComponent } from './file-manager.component';
import { IndexedDbService } from '../../services/indexeddb.service';

describe('FileManagerComponent', () => {
  let component: FileManagerComponent;
  let fixture: ComponentFixture<FileManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FileManagerComponent],
      providers: [{ provide: IndexedDbService, useClass: MockIndexedDbService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FileManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
