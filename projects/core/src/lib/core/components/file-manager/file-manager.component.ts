import { IndexedDbService, LocalFile } from '@nimbus/core/src/lib/core';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, Input, OnInit, Optional } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';

export interface IndexedFile extends LocalFile {
  checked: boolean;
}

@Component({
  selector: 'ni-file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FileManagerComponent implements OnInit {
  constructor(
    private dbService: IndexedDbService,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
    private change: ChangeDetectorRef
  ) {
    if (data) {
      this.itemTableName = data.itemTableName;
      this.dialogView = data.dialogView;
    }
  }

  @Input() itemTableName = 'offlineUploads';
  @Input() dialogView = false;
  @Input() noResultsText = 'No files found';

  displayedColumns: string[] = ['select', 'name', 'size'];
  indexedData: IndexedFile[] = [];
  selectedList: string[] = [];
  dataSource = new MatTableDataSource<IndexedFile>(this.indexedData);
  selected = false;
  allSelected = false;
  someSelected = false;

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.dbService
      .table<LocalFile>(this.itemTableName)
      .toCollection()
      .sortBy('file.name')
      .then((files) => {
        this.dataSource = new MatTableDataSource<IndexedFile>(files.map((file) => ({ ...file, checked: false })));
        this.change.markForCheck();
        this.someSelected = false;
        this.allSelected = false;
      });
  }

  deleteFiles() {
    this.selectedList.forEach((index) => {
      this.dbService.table(`${this.itemTableName}`).delete(index);
      this.dbService.table(`${this.itemTableName}Chunks`).delete(index);
      this.dbService
        .table(`${this.itemTableName}Chunks`)
        .where('fileId')
        .anyOf(index)
        .delete()
        .then(() => {
          this.selectedList = [];
          this.loadData();
        });
    });
  }

  selectedIndex(event: MatCheckboxChange, index: IndexedFile, indexId: string) {
    event.checked
      ? ((index.checked = true), this.selectedList.push(indexId))
      : ((index.checked = false),
        this.selectedList.splice(this.selectedList.indexOf(indexId), 1),
        (this.allSelected = false));

    this.selectedList.length === this.dataSource.data.length ? (this.allSelected = true) : (this.allSelected = false);

    this.someComplete();
  }

  selectAll(completed: boolean) {
    this.someSelected = false;
    this.allSelected = completed;
    this.dataSource.data.forEach((file: IndexedFile) => {
      this.allSelected
        ? ((file.checked = true), this.selectedList.push(file.id))
        : ((file.checked = false), (this.selectedList = []), (this.selected = false));
    });
    this.someComplete();
  }

  someComplete() {
    this.selectedList.length < this.dataSource.data.length ? (this.someSelected = true) : (this.someSelected = false);
  }
}
