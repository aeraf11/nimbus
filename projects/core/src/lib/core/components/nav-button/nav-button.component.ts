import { Component, Input } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ni-nav-button',
  templateUrl: './nav-button.component.html',
  styleUrls: ['./nav-button.component.scss'],
})
export class NavButtonComponent {
  @Input() cssClass: string | undefined;
  @Input() text: string | undefined;
  @Input() color: ThemePalette;
  @Input() disabled = false;
  @Input() title = '';
  @Input() ariaLabel = '';
  @Input() route = '';
  @Input() param = '';

  constructor(private router: Router) {}

  navigate() {
    const url = this.route + this.param;
    this.router.navigateByUrl(url);
  }
}
