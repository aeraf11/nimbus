import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'ni-divider',
  templateUrl: './divider.component.html',
  styleUrls: ['./divider.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DividerComponent {
  @Input() width: string | undefined;
  @Input() style: string | undefined;
  @Input() marginTop: string | undefined;
  @Input() marginRight: string | undefined;
  @Input() marginBottom: string | undefined;
  @Input() marginLeft: string | undefined;
}
