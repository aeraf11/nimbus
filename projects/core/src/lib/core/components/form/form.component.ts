import { ChangeDetectorRef, Component, EventEmitter, HostListener, Input, Output, ViewChild } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FormlyFieldConfig, FormlyForm, FormlyFormOptions } from '@ngx-formly/core';
import { FormDefinition, FormsHelperService, StateService } from '@nimbus/core/src/lib/core';
import { OnChange } from 'property-watch-decorator';
import { Subject } from 'rxjs';

@UntilDestroy()
@Component({
  selector: 'ni-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent {
  @OnChange<FormDefinition | null>(function (this: FormComponent, value) {
    if (value) {
      this.form = new UntypedFormGroup({});
      if (value.options)
        this.options = { ...value.options, formState: { ...this.options.formState, ...value.options.formState } };
      this.fields = value.fields;
    }
  })
  @Input()
  definition: FormDefinition | null | undefined = null;

  @OnChange<any>(function (this: FormComponent, value) {
    if (value) {
      this.options = { ...this.options, formState: { ...this.options?.formState, rootModel: value } };
    }
    this.rootModelChanged?.next(null);
  })
  @Input()
  model: any = {};

  @OnChange<any>(function (this: FormComponent, value) {
    if (value) {
      this.options.formState = this.options.formState || {};
      this.options.formState.context = { ...this.options.formState?.context, ...value };
    }
  })
  @Input()
  context: any = {};

  @Input()
  errorMessage: string | null = null;

  @Input()
  disableDebug = false;

  @Input()
  forceShowDebug = false;

  @ViewChild('formly') formly!: FormlyForm;
  options: FormlyFormOptions = {};
  fields: FormlyFieldConfig[] = [];

  @Output() formAction: EventEmitter<{
    model: any;
    action?: string;
    routeId?: string;
    onSuccess?: () => void;
    showErrorSnack?: boolean;
  }> = new EventEmitter();

  @Output() modelChangedEvent: EventEmitter<any> = new EventEmitter();

  @Output() contextChangedEvent: EventEmitter<any> = new EventEmitter();

  form: UntypedFormGroup = new UntypedFormGroup({});
  rootModelChanged = new Subject<any>();
  contextChanged = new Subject<string>();
  debug = false;

  constructor(helper: FormsHelperService, state: StateService, private change: ChangeDetectorRef) {
    this.options = {
      ...this.options,
      formState: {
        ...this.options?.formState,
        helper,
        rootModelChanged: this.rootModelChanged,
        contextChanged: this.contextChanged,
        context: this.context,
        executeAction: (
          action: string,
          model: unknown,
          routeId: string,
          onSuccess: () => void,
          showErrorSnack: boolean
        ) => this.executeAction(action, model, routeId, onSuccess, showErrorSnack),
        isValid: () => {
          return this.form.valid;
        },
        isInvalid: () => {
          return this.form.invalid;
        },
        isPristine: () => {
          return this.form.pristine;
        },
        isDirty: () => {
          return this.form.dirty;
        },
      },
    };

    state
      .selectDebugOptions()
      .pipe(untilDestroyed(this))
      .subscribe((debugOptions) => (this.debug = debugOptions?.debugForms ?? false));

    state
      .selectFormStateSettings()
      .pipe(untilDestroyed(this))
      .subscribe((settings) => (this.options.formState.settings = { ...settings }));

    this.contextChanged.pipe(untilDestroyed(this)).subscribe((context) => this.contextChangedEvent.emit(context));
  }

  executeAction(action?: string, model?: unknown, routeId?: string, onSuccess?: () => void, showErrorSnack?: boolean) {
    this.formAction.emit({ model: model || this.model, action, routeId, onSuccess, showErrorSnack });
  }

  modelChange(model: any) {
    this.rootModelChanged.next(null);
    this.modelChangedEvent.emit(model);
  }

  @HostListener('window:beforeunload', ['$event'])
  onPageRefresh($event: any) {
    if (this.form.dirty && !this.debug) {
      $event.returnValue = confirm('Unsaved changes will be lost, are you sure?');
    }
  }

  reset() {
    this.form.reset('', { emitEvent: false });
    this.change.detectChanges();
  }

  clean() {
    this.form.markAsUntouched();
    this.form.markAsPristine();
    this.change.detectChanges();
  }

  getDirtyFields(): string[] {
    const changedProperties: string[] = [];

    Object.keys(this.form.controls).forEach((name) => {
      const currentControl = this.form.controls[name];

      if (currentControl.dirty) {
        changedProperties.push(name);
      }
    });

    return changedProperties;
  }
}
