import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { OnChange } from 'property-watch-decorator';
import { ProfileSize } from '../../models/profile-size';

@Component({
  selector: 'ni-profile-picture',
  templateUrl: './profile-picture.component.html',
  styleUrls: ['./profile-picture.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfilePictureComponent {
  @OnChange<string>(function (this: ProfilePictureComponent, value) {
    this.profileImageUrl = '/api/v2/media/photo?id=' + encodeURIComponent(value);
  })
  @Input()
  blobId: string | undefined;

  @Input() text: string | undefined;
  @Input() initials: string | undefined;
  @Input() defaultIfEmpty: string | undefined;
  @Input() size: ProfileSize = 'small';
  profileImageUrl = '';
}
