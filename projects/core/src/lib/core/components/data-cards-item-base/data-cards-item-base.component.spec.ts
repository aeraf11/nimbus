import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { ListFilterService, ListService, StateService } from '@nimbus/core/src/lib/core';
import { MockListService } from '../../../core/services/list.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { DataCardsItemBaseComponent } from './data-cards-item-base.component';
import { DataCardsComponent } from '../data-cards/data-cards.component';
import { MockListFilterService } from '../../services/list-filter-service/list-filter.service.spec';

describe('DataCardsItemBaseComponent', () => {
  let component: DataCardsItemBaseComponent;
  let fixture: ComponentFixture<DataCardsItemBaseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DataCardsItemBaseComponent],
      imports: [MatDialogModule],
      providers: [
        DataCardsComponent,
        { provide: StateService, useClass: MockStateService },
        { provide: ListService, useClass: MockListService },
        { provide: ListFilterService, useClass: MockListFilterService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataCardsItemBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
