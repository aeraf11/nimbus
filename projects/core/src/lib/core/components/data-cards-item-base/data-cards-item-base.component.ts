import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Subject } from 'rxjs';
import { DataCardsComponent } from '../data-cards/data-cards.component';

@UntilDestroy()
@Component({
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataCardsItemBaseComponent {
  private onChange!: (value: any) => void;
  private onTouched!: () => void;
  item: any;
  itemUpdate: Subject<any> = new Subject<any>();
  selectMode: 'none' | 'single' | 'multi';

  constructor(public parent: DataCardsComponent, change: ChangeDetectorRef) {
    parent.itemSelectionChanged.pipe(untilDestroyed(this)).subscribe(() => {
      change.markForCheck();
    });
    parent.itemsUpdate.pipe(untilDestroyed(this)).subscribe(() => {
      change.markForCheck();
    });
    this.selectMode = parent.selectMode;
  }

  itemClicked(item: any) {
    this.parent.selectItem(item);
  }

  writeValue(value: any): void {
    if (value) {
      this.item = value;
      this.itemUpdate.next(value);
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  isItemChecked(item: any): boolean {
    return this.parent.isItemChecked(item);
  }
}
