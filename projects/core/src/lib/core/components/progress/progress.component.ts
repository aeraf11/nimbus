import { Component, OnInit, Input, ElementRef, ChangeDetectionStrategy } from '@angular/core';
import { FormlyGroup } from '@ngx-formly/core/lib/templates/formly.group';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'ni-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgressComponent implements OnInit {
  defaultOptions = {
    props: {
      progressValue: 0,
      progressMax: 100,
      showValue: true,
    },
  };
  _progressValue?: number;
  _progressMax?: number;

  ngOnInit(): void {
    if (this.props && this.props.progressColour && this.element) {
      this.isCustomColour = true;
      this.element.nativeElement.style.setProperty('--progress-color', this.props['progressColour']);
    }
  }

  @Input() label?: string;

  @Input() set progressValue(value: number) {
    if (value) {
      this._progressValue = value;
    }
  }
  get progressValue(): number {
    if (this._progressValue) {
      return this._progressValue;
    } else if (this.formControl) {
      return this.formControl?.value?.table1?.results?.[0].value ?? this.props['progressValue'];
    } else {
      return this.defaultOptions.props.progressValue;
    }
  }

  @Input() set progressMax(value: number) {
    if (value) {
      this._progressMax = value;
    }
  }
  get progressMax(): number {
    if (this._progressMax) {
      return this._progressMax;
    } else if (this.formControl) {
      return this.formControl?.value?.table2?.results?.[0].total ?? this.props['progressMax'];
    } else {
      return this.defaultOptions.props.progressMax;
    }
  }

  @Input() isCustomColour: boolean = false;
  @Input() showValue: boolean = false;
  @Input() formControl?: FormControl;
  @Input() props?: any;

  constructor(private element: ElementRef) {}
}
