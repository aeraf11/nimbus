import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ChangeDetectionStrategy, Component, EventEmitter, OnInit } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';
import { CardListView } from '../../models';
import { OptionsService } from '../../services';

@UntilDestroy()
@Component({
  selector: 'ni-card-list-wrapper',
  templateUrl: './card-list-wrapper.component.html',
  styleUrls: ['./card-list-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardListWrapperComponent extends FieldWrapper implements OnInit {
  public defaults = {
    countIcon: 'filter',
  };

  views: CardListView[] = [];
  filterEmit = new EventEmitter<void>();
  viewEmit = new EventEmitter<CardListView>();
  itemCount = 0;
  selectedItemCount = 0;
  hasFilters = false;

  // Syncing Data Between child form elements
  currentItems: any[] = [];
  currentFilters: any;
  currentPage?: number;
  currentPageSize?: number;
  currentView?: CardListView;

  constructor(options: OptionsService) {
    super();
    this.viewEmit.subscribe((view: CardListView) => {
      return (this.currentView = view);
    });
  }

  ngOnInit(): void {
    this.formControl.valueChanges.pipe(untilDestroyed(this)).subscribe((value) => {
      this.selectedItemCount = value[Object.keys(value)[0]]?.length || 0;
    });
  }

  setItemCount(count: number): void {
    this.itemCount = count;
  }

  filterClicked(): void {
    this.filterEmit.emit();
  }

  setView(): void {
    if (this.currentView && this.views?.length > 0) {
      const index = this.views.indexOf(this.currentView);
      const i = index === this.views.length - 1 ? 0 : index + 1;
      this.viewEmit.emit(this.views[i]);
    }
  }

  setCurrentItems(items: any[]): void {
    this.currentItems = items;
  }

  setCurrentFilters(filters: any): void {
    this.hasFilters = filters?.length > 0;
    this.currentFilters = filters;
  }

  setCurrentPage(page: number): void {
    this.currentPage = page;
  }

  setCurrentPageSize(size: number): void {
    this.currentPageSize = size;
  }
}
