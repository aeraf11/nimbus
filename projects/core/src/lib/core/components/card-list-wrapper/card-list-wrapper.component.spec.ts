import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CardListWrapperComponent } from './card-list-wrapper.component';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '../../formly/test/formly-test.component';

describe('CardListWrapperComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          wrappers: [
            {
              name: 'cardlist',
              component: CardListWrapperComponent,
            },
          ],
        },
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'cardlist',
        wrappers: ['cardlist'],
        fieldGroup: [
          {
            template: 'test',
          },
        ],
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
