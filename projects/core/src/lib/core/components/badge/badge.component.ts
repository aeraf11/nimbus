import { Component, Input } from '@angular/core';

@Component({
  selector: 'ni-badge',
  templateUrl: './badge.component.html',
  styleUrls: ['./badge.component.scss'],
})
export class BadgeComponent {
  @Input() cssClass: string | undefined;
  @Input() text: string | undefined;
}
