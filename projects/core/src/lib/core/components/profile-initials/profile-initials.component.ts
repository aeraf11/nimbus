import { Component, HostBinding, Input } from '@angular/core';
import { ProfileSize } from '../../models/profile-size';

@Component({
  selector: 'ni-profile-initials',
  templateUrl: './profile-initials.component.html',
  styleUrls: ['./profile-initials.component.scss'],
})
export class ProfileInitialsComponent {
  @Input() text: string | undefined;
  @Input() size: ProfileSize = 'small';

  @HostBinding('class.large') get hostClass() {
    return this.size === 'large';
  }
}
