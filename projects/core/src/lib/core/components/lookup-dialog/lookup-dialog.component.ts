import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { deepClone } from 'deep.clone';
import { Lookup } from '../../models/lookup';
import { LookupDialogData } from '../../models/lookup-dialog-data';
import { LookupNode } from '../../models/lookup-node';
import { LookupService } from '../../services/lookup.service';

@UntilDestroy()
@Component({
  selector: 'ni-lookup-dialog',
  templateUrl: './lookup-dialog.component.html',
  styleUrls: ['./lookup-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LookupDialogComponent {
  lookups: LookupNode[] | null = null;
  lookupsFlat: Lookup[] | null = null;
  selectedIds: string[] = [];
  title!: string;
  lookupKey!: string;
  value!: string;
  leafSelection = true;
  addDefaultOption = true;
  selectedLookup!: Lookup | undefined;
  lookupFilters: string[];

  constructor(
    public dialogRef: MatDialogRef<LookupDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: LookupDialogData,
    private service: LookupService,
    private change: ChangeDetectorRef
  ) {
    this.title = data.title;
    this.lookupKey = data.key;
    this.leafSelection = data.leafSelection;
    this.addDefaultOption = data.addDefaultOption;
    this.lookupFilters = data.lookupFilters;
    deepClone({ value: data.value }).then((clone) => {
      this.value = clone.value;
      this.setSelectedLookup();
      this.setHierarchy();
    });
  }

  onItemSelected(event: { id: string; level: number }) {
    this.value = event.id;
    this.setSelectedLookup();
    this.change.markForCheck();
  }

  onCancel() {
    this.dialogRef.close();
  }

  onOk() {
    this.dialogRef.close({ selectedId: this.value, displayValue: this.selectedLookup?.displayValue });
  }

  setHierarchy() {
    if (this.lookupKey) {
      this.service
        .getLookupsHierachy(this.lookupKey, this.lookupFilters)
        .pipe(untilDestroyed(this))
        .subscribe((lookups) => {
          this.lookups = lookups;
          this.change.markForCheck();
        });

      this.service
        .getLookupsFlat(this.lookupKey, this.leafSelection)
        .pipe(untilDestroyed(this))
        .subscribe((lookups) => {
          this.lookupsFlat = lookups;
          this.setSelectedLookup();
          this.change.markForCheck();
        });
    }
    if (this.value) {
      this.service
        .getSelectedIds(this.value)
        .pipe(untilDestroyed(this))
        .subscribe((ids) => {
          this.selectedIds = ids;
          this.change.markForCheck();
        });
    }
  }

  setSelectedLookup() {
    if (this.value && this.lookupsFlat) {
      this.selectedLookup = this.lookupsFlat.find((lookup) => lookup.lookupId === this.value);
    }
  }
}
