import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Person } from '../../models/person';
import { FormlyFieldConfig, FormlyFieldProps } from '@ngx-formly/core';
import { ProfileSize } from '../../models';

@Component({
  selector: 'ni-person-view',
  templateUrl: './person-view.component.html',
  styleUrls: ['./person-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersonViewComponent implements OnInit {
  @Input() person?: Person;
  @Input() imageSize: ProfileSize = 'medium';
  @Input() fields?: FormlyFieldConfig<FormlyFieldProps & { [additionalProperties: string]: any }>[];
  @Input() photoProp = '';
  @Input() initialsProp = '';
  @Input() isEmpty!: boolean;
  @Input() isEmptyText?: string;
  @Input() noImageSize!: '124px' | '75px';

  constructor() {}

  ngOnInit(): void {}
}
