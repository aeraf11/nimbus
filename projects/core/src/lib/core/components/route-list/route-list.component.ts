import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { BaseTypeComponent } from '../../../apto/components/base-type/base-type.component';

@UntilDestroy()
@Component({
  selector: 'ni-route-list',
  templateUrl: './route-list.component.html',
  styleUrls: ['./route-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouteListComponent extends BaseTypeComponent {
  @Input() routes = [];
  @Input() formgroup: any;
  @Input() fields?: any;
  @Input() label?: string = '';
  @Input() marginOnItems?: boolean = false;
  @Input() fieldArray?: any = {
    type: 'jumbolink',
  };
}
