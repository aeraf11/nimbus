import { CdkStepper } from '@angular/cdk/stepper';
import { AfterViewInit, Component, EventEmitter, Input, Output, QueryList } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { PageStepperStepComponent } from '../page-stepper-step/page-stepper-step.component';

@UntilDestroy()
@Component({
  selector: 'ni-page-stepper',
  templateUrl: './page-stepper.component.html',
  styleUrls: ['./page-stepper.component.scss'],
  providers: [{ provide: CdkStepper, useExisting: PageStepperComponent }],
})
export class PageStepperComponent extends CdkStepper implements AfterViewInit {
  @Input() valueChanges?: Observable<any>;
  @Input() stepDivider?: { enabled: boolean; height: string; margin: string };
  @Input() useStepButtonLabels?: boolean = false;
  @Input() hidePreviousNextButtons?: boolean = false;
  @Input() progressBar?: {
    enabled: boolean;
    width: string;
    height: string;
    subLabel: string;
    conjunction: string;
    trackColour: string;
    progressColour: string;
  };

  @Input() buttonConfig?: {
    name: string;
    type: string;
    materialType: string;
    stepIndex: number;
    colour: string;
    label: string;
    ariaLabel: string;
    icon: string;
    size: string;
    alwaysEnabled: boolean;
    visible: boolean;
    isStepperControl: boolean;
    moveToStep: number;
  }[];

  @Output() buttonClicked = new EventEmitter();

  override readonly steps: QueryList<PageStepperStepComponent> = new QueryList<PageStepperStepComponent>();

  override ngAfterViewInit(): void {
    super.ngAfterViewInit();
  }

  setSelectedStepByIndex(index: number): void {
    if (this.selectedIndex !== index && this.isStepSelectable(index)) {
      this.selectedIndex = index;
    }
  }

  isStepSelectable(i: number): boolean {
    return !this.steps.some((step: PageStepperStepComponent, idx: number) => {
      return idx < i && !step?.control?.valid;
    });
  }

  isStepDirty(i: number): boolean {
    return !this.steps.some((step: PageStepperStepComponent, idx: number) => {
      return idx < i && !step?.control?.dirty;
    });
  }

  onButtonPress(buttonId: string, isStepperControl: boolean, stepIndex: number): void {
    if (isStepperControl) {
      this.setSelectedStepByIndex(stepIndex);
    } else {
      this.buttonClicked.emit(buttonId);
    }
  }

  isButtonDisabled(alwaysEnabled: boolean, isStepValid: any): boolean {
    if (alwaysEnabled) return isStepValid;
    return alwaysEnabled;
  }

  getAllStepsValidity(): boolean | undefined {
    return this.steps.get(this.selectedIndex - 1)?.control?.invalid;
  }
}
