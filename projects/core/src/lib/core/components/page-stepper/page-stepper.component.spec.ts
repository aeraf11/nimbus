import { CdkStepper, CdkStepperModule } from '@angular/cdk/stepper';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PageStepperComponent } from './page-stepper.component';

describe('PageStepperComponent', () => {
  let component: PageStepperComponent;
  let fixture: ComponentFixture<PageStepperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PageStepperComponent],
      imports: [CdkStepperModule],
      providers: [CdkStepper],
    }).compileComponents();

    fixture = TestBed.createComponent(PageStepperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
