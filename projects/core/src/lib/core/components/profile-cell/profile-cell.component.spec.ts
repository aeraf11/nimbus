import { ComponentFixture, TestBed } from '@angular/core/testing';
import { InitialsPipe } from './../../pipes/initials.pipe';
import { ProfileCellComponent } from './profile-cell.component';

describe('ProfileCellComponent', () => {
  let component: ProfileCellComponent;
  let fixture: ComponentFixture<ProfileCellComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProfileCellComponent, InitialsPipe],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
