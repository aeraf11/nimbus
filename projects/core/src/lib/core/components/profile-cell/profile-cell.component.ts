import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'ni-profile-cell',
  templateUrl: './profile-cell.component.html',
  styleUrls: ['./profile-cell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileCellComponent {
  @Input() name: string | undefined;
  @Input() blobId: string | undefined;

  get value(): { name: string; blobId: string } | string | undefined {
    if (this.name && this.blobId) return { name: this.name, blobId: this.blobId };
    else return this.name;
  }

  @Input()
  set value(value: { name: string; blobId: string } | string | undefined) {
    if (typeof value === 'object' && value !== null) {
      this.name = value?.name;
      this.blobId = value?.blobId;
    } else this.name = value;
  }
}
