import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { ListFilterService, StateService } from '@nimbus/core/src/lib/core';
import { ListService } from '../../../core/services/list.service';
import { MockListService } from '../../../core/services/list.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { CrimeSceneComponent } from './crime-scene.component';
import { DataCardsComponent } from '../data-cards/data-cards.component';
import { MockListFilterService } from '../../services/list-filter-service/list-filter.service.spec';

describe('CrimeSceneComponent', () => {
  let component: CrimeSceneComponent;
  let fixture: ComponentFixture<CrimeSceneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CrimeSceneComponent],
      imports: [MatDialogModule],
      providers: [
        DataCardsComponent,
        { provide: StateService, useClass: MockStateService },
        { provide: ListService, useClass: MockListService },
        { provide: ListFilterService, useClass: MockListFilterService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrimeSceneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
