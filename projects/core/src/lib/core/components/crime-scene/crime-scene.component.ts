import { Component, ChangeDetectionStrategy, forwardRef, ChangeDetectorRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { DataCardsItemBaseComponent } from '../data-cards-item-base/data-cards-item-base.component';
import { DataCardsComponent } from '../data-cards/data-cards.component';

@Component({
  selector: 'ni-crime-scene',
  templateUrl: './crime-scene.component.html',
  styleUrls: ['./crime-scene.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CrimeSceneComponent),
      multi: true,
    },
  ],
})
export class CrimeSceneComponent extends DataCardsItemBaseComponent implements ControlValueAccessor {
  constructor(public override parent: DataCardsComponent, private change: ChangeDetectorRef) {
    super(parent, change);
  }
}
