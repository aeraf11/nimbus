import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  NgZone,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { CircularChartLegend, CircularGaugePointer, CircularGaugeRange } from '../../models/circular-gauge-models';
import { CircularGaugeComponent } from '@syncfusion/ej2-angular-circulargauge';
import { Observable, debounceTime } from 'rxjs';
import { utils } from '../../utils';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'ni-circular-gauge',
  templateUrl: './circular-gauge.component.html',
  styleUrls: ['./circular-gauge.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class NiCircularGaugeComponent implements AfterViewInit {
  @Input() width: string | undefined;
  @Input() height: string | undefined;
  @Input() flexPosition: string | undefined;
  @Input() data: { pointers: CircularGaugePointer[]; ranges: CircularGaugeRange[] } | null = null;
  @Input() total: number | null = null;
  @Input() title: string | null = null;
  @Input() subTitle: string | null = null;
  @Input() chartLegend: CircularChartLegend | undefined;
  @Input() titleProportion = 0.2;
  @Input() titleSize: string | undefined;
  @Input() subTitleProportion = 0.08;
  @Input() subTitleSize: string | undefined;

  @ViewChild('outer')
  outer?: ElementRef;

  @ViewChild('chart')
  chart?: CircularGaugeComponent;

  calculating = true;
  calculatingHeight = true;
  centerY = '50%';
  baseRadius = 0;
  titlesTop = 0;
  calculatedTitleSize = '';
  calculatedSubTitleSize = '';
  calculatedLegendTitleSize = '';
  calculatedLegendSubTitleSize = '';
  legendTop = 0;

  DEFAULT_LEGEND_TITLE_PROPORTION = 0.08;
  DEFAULT_LEGEND_SUB_TITLE_PROPORTION = 0.04;
  DEFAULT_LEGEND_PADDING = '10%';

  private resize$?: Observable<ResizeObserverEntry[] | undefined>;
  private loopCheck = false;

  constructor(private change: ChangeDetectorRef, private zone: NgZone) {}

  ngAfterViewInit(): void {
    if (this.outer) {
      this.resize$ = utils.resizeObservable(this.outer?.nativeElement).pipe(untilDestroyed(this));
      this.resize$
        .pipe(untilDestroyed(this), debounceTime(500))
        .subscribe(() => this.zone.run(() => this.calculateGraphSize()));
    }
  }

  calcRangeRadii(ranges: CircularGaugeRange[]): CircularGaugeRange[] {
    let radiusProportion = 1;
    ranges.forEach((range) => {
      range.radius = `${this.baseRadius * radiusProportion}px`;
      radiusProportion = radiusProportion - 0.1;
    });
    return ranges;
  }

  calcPointerRadii(pointers: CircularGaugePointer[]): CircularGaugePointer[] {
    let radiusProportion = 1;
    pointers.forEach((pointer) => {
      pointer.radius = `${this.baseRadius * radiusProportion}px`;
      radiusProportion = radiusProportion - 0.1;
    });
    return pointers;
  }

  private calculateGraphSize(): void {
    if (this.loopCheck) {
      this.loopCheck = false;
      return;
    }
    this.calculating = true;
    this.calculatingHeight = true;
    this.change.markForCheck();
    this.change.detectChanges();

    const outerWidth = this.outer?.nativeElement.offsetWidth;
    let outerHeight = this.outer?.nativeElement.offsetHeight;
    if (outerHeight === 0) outerHeight = outerWidth;
    const legendHeight = utils.getWidthFromString(this.chartLegend?.height ?? '100%', outerHeight);
    const widthBasedDiameter = outerWidth;
    const heightBasedDiameter = Math.ceil(outerHeight - legendHeight);
    const diameter = widthBasedDiameter > heightBasedDiameter ? heightBasedDiameter : widthBasedDiameter;
    let surplus = (outerHeight - diameter - legendHeight) / 2;
    if (surplus < 0) surplus = 0;

    this.baseRadius = diameter / 2;
    this.centerY = `${this.baseRadius}px`;
    this.height = `${diameter}px`;
    this.titlesTop = this.baseRadius;
    this.calculatedTitleSize = this.getProportionedTitleSize(diameter, this.titleProportion, this.titleSize);
    this.calculatedSubTitleSize = this.getProportionedTitleSize(diameter, this.subTitleProportion, this.subTitleSize);
    this.calculatedLegendTitleSize = this.getProportionedTitleSize(
      diameter,
      this.chartLegend?.titleProportion || this.DEFAULT_LEGEND_TITLE_PROPORTION,
      this.chartLegend?.titleSize
    );
    this.calculatedLegendSubTitleSize = this.getProportionedTitleSize(
      diameter,
      this.chartLegend?.subTitleProportion || this.DEFAULT_LEGEND_SUB_TITLE_PROPORTION,
      this.chartLegend?.subTitleSize
    );

    setTimeout(() => this.calculateHeight(), 100);

    this.calculating = false;
    this.loopCheck = true;
    setTimeout(() => (this.loopCheck = false), 500);
    this.change.markForCheck();
  }

  private calculateHeight() {
    const svg = this.chart?.svgObject;
    if (svg) {
      const chartHeight = (<SVGGElement>svg.childNodes[2]).getBBox().height;
      const padding = utils.getWidthFromString(this.chartLegend?.padding || this.DEFAULT_LEGEND_PADDING, chartHeight);
      this.legendTop = chartHeight + padding;
    }
    this.calculatingHeight = false;
    this.change.markForCheck();
  }

  private getProportionedTitleSize(diameter: number, proportion: number, override: string | undefined): string {
    if (override) return override;
    else return `${diameter * proportion}px`;
  }

  get hasData(): boolean {
    return this.data ? this.data.pointers.length > 0 : false;
  }

  get margin(): unknown {
    return {
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
    };
  }

  get lineStyle(): unknown {
    return {
      color: 'transparent',
      width: 0,
    };
  }

  get majorTicks(): unknown {
    return {
      interval: 0,
      height: 0,
    };
  }

  get minorTicks(): unknown {
    return {
      interval: 0,
      height: 0,
    };
  }
}
