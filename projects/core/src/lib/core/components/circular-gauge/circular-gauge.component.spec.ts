import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NiCircularGaugeComponent } from './circular-gauge.component';

describe('NiCircularGaugeComponent', () => {
  let component: NiCircularGaugeComponent;
  let fixture: ComponentFixture<NiCircularGaugeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [NiCircularGaugeComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(NiCircularGaugeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
