import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnidentifiedSubjectComponent } from './unidentified-subject.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SiteConfig } from '../../models/site-config';
import { StateService } from '../../services';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { MatDialogModule } from '@angular/material/dialog';
import { DataCardsComponent } from '../data-cards/data-cards.component';

describe('UnidentifiedSubjectComponent', () => {
  let component: UnidentifiedSubjectComponent;
  let fixture: ComponentFixture<UnidentifiedSubjectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UnidentifiedSubjectComponent],
      imports: [HttpClientTestingModule, MatDialogModule],
      providers: [
        DataCardsComponent,
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
        { provide: StateService, useClass: MockStateService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(UnidentifiedSubjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
