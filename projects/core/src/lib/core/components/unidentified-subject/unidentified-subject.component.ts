import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, UntypedFormGroup } from '@angular/forms';
import { DataType } from '../../models/data-type';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { DataCardsItemBaseComponent } from '../data-cards-item-base/data-cards-item-base.component';
import { DataCardsComponent } from '../data-cards/data-cards.component';
import { EntityMedia } from '../unidentified-subject-detail/unidentified-subject-detail.component';

@Component({
  selector: 'ni-unidentified-subject',
  templateUrl: './unidentified-subject.component.html',
  styleUrls: ['./unidentified-subject.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UnidentifiedSubjectComponent),
      multi: true,
    },
  ],
})
export class UnidentifiedSubjectComponent extends DataCardsItemBaseComponent implements ControlValueAccessor {
  @Input() viewType: 'grid' | 'row' = 'grid';

  photoEndpoint = '/api/v2/media/photo?id=';
  defaultPhoto?: string;
  identifyButtonText = 'IDENTIFY';
  notFoundText = 'Not Found';
  DataType = DataType;
  unidentifiedSubjectMedia: EntityMedia[] = [];

  fieldConfigs: FormlyFieldConfig[] = [
    {
      type: 'repeat',
      props: {
        enableCarousel: true,
        carouselControlSize: '20px',
        carouselControlIndent: '5px',
        contentPosition: 'center center',
        carouselDirection: 'horizontal',
        carouselMaxHeight: '250px',
        carouselControlDirection: 'horizontal',
        carouselItemCount: 1,
        carouselItemidth: '100%',
      },
      fieldArray: {
        fieldGroup: [
          {
            key: 'blobId',
            type: 'video',
            props: {
              text: 'Unidentified Subject Photo',
              tabindex: 0,
            },
          },
        ],
      },
    },
  ];

  formGroup = new UntypedFormGroup({});

  constructor(
    public override parent: DataCardsComponent,
    private change: ChangeDetectorRef,
    private liveAnnouncer: LiveAnnouncer
  ) {
    super(parent, change);
  }

  override writeValue(value: any): void {
    if (value) {
      this.unidentifiedSubjectMedia = value.mediaJson ? JSON.parse(value.mediaJson) : [];
      this.item = value;
      this.defaultPhoto = this.getDefaultPhoto();
      this.itemUpdate.next(value);
      this.change.markForCheck();
    }
  }

  getDefaultPhoto(): string | undefined {
    let defaultPhoto = this.unidentifiedSubjectMedia.find((p: any) => p.isDefault && p.mediaType === 1);
    if (!defaultPhoto) {
      defaultPhoto = this.unidentifiedSubjectMedia[0];
    }
    if (this.unidentifiedSubjectMedia && this.unidentifiedSubjectMedia.length > 0 && defaultPhoto) {
      return this.photoEndpoint + defaultPhoto.thumbnailBlobId;
    }
    return undefined;
  }

  cardAction(item: any, column: any): void {
    this.parent.itemClicked(item, { id: 'unidentifiedSubjectId', unidentifiedSubject: item });
  }

  // A bit of a hacky way when the user focuses on the card to read out the counts. Spans need a role
  // otherwise they will be ignored by screen reader, but something like role="button" is misleading by
  // what else it will read out. So this was the next best option.
  announce(item: any): void {
    const announcement = `${item.photoCount} ${item.photoCount === 1 ? 'photo' : 'photos'}, ${item.videoCount} ${
      item.videoCount === 1 ? 'video' : 'videos'
    }, ${item.audioCount} 'audio' ${item.audioCount === 1 ? 'file' : 'files'}`;

    this.liveAnnouncer.announce(announcement);
  }
}
