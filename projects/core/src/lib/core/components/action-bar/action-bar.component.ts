import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ActionBarItem, ActionLayout, ActionStyle, ActionType, FormDefinition } from '../../models';
import { DialogFormComponent, DialogFormConfig, DialogFormData, StateService } from '@nimbus/core/src/lib/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'ni-action-bar',
  templateUrl: './action-bar.component.html',
  styleUrls: ['./action-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActionBarComponent {
  ActionLayout = ActionLayout;
  @Input() layout: ActionLayout = ActionLayout.Row;

  ActionStyle = ActionStyle;
  @Input() style: ActionStyle = ActionStyle.Button;

  @Input() items: ActionBarItem[] = [];
  @Input() buttonColour: string | undefined;
  @Input() gap = '0px';
  @Input() disabled = false;
  @Input() dialogWidth: string | undefined;
  @Input() dialogFormConfig: DialogFormConfig = { enableDelete: false, enableEdit: false, isChildForm: false };

  constructor(private router: Router, private dialog: MatDialog, private state: StateService) {}

  itemClicked(item: ActionBarItem): void {
    switch (item.ActionType) {
      case ActionType.Route:
        if (item.Url) {
          this.router.navigateByUrl(item.Url);
        } else {
          console.error(`Action bar item is of type Route, but has no url specified on template options.`, item);
        }
        break;
      case ActionType.ModalForm:
        if (item.FormId) {
          this.state
            .selectForm(item.FormId)
            .pipe(untilDestroyed(this))
            .subscribe((form) => {
              this.openDialog(form, this.dialogFormConfig, {}, item.ItemContext);
            });
        } else {
          console.error(`Action bar item is of type ModalForm, but has no formId specified on template options.`, item);
        }
        break;
    }
  }

  private openDialog(
    form: FormDefinition | undefined,
    config: DialogFormConfig,
    model: unknown,
    context: unknown
  ): void {
    const data: DialogFormData = {
      model,
      fieldConfig: form?.fields,
      config: { ...config, isChildForm: false, formId: form?.id },
      disabled: this.disabled,
      context,
    };

    const dialogConfig: MatDialogConfig<DialogFormData> = { data };
    if (this.dialogWidth) {
      dialogConfig.width = this.dialogWidth;
      dialogConfig.minWidth = '';
      dialogConfig.maxWidth = '';
    }

    const dialogRef = this.dialog.open(DialogFormComponent, dialogConfig);

    dialogRef.afterClosed().subscribe();
  }
}
