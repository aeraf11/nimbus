import { Directive, Input } from '@angular/core';
import { MatIcon } from '@angular/material/icon';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'mat-icon[icon]',
})
export class IconDirective {
  @Input() set icon(iconStr: string) {
    if (iconStr && iconStr.length > 0) {
      const parts = iconStr.split(' ');
      if (parts.length === 1) {
        this.matIcon._elementRef.nativeElement.innerText = parts[0];
      } else {
        const [fontSet, fontIcon] = parts;
        this.matIcon.fontSet = fontSet;
        this.matIcon.fontIcon = fontIcon;
        this.matIcon._elementRef.nativeElement.classList.add('scale');
      }
    }
  }

  @Input() set size(size: string) {
    if (size && size.length > 0) {
      this.matIcon._elementRef.nativeElement.style.fontSize = size;
      this.matIcon._elementRef.nativeElement.style.height = size;
      this.matIcon._elementRef.nativeElement.style.width = size;
    }
  }

  @Input() set scale(scale: boolean) {
    if (!scale) {
      this.matIcon._elementRef.nativeElement.classList.remove('scale');
    }
  }

  constructor(private readonly matIcon: MatIcon) {}
}
