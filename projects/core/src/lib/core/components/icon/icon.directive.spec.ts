import { TestBed } from '@angular/core/testing';
import { MatIcon, MatIconModule } from '@angular/material/icon';
import { IconDirective } from './icon.directive';

describe('IconDirective', () => {
  let directive: IconDirective;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [IconDirective],
      imports: [MatIconModule],
      providers: [IconDirective, { provide: MatIcon, useValue: jasmine.createSpy('MatIcon') }],
    }).compileComponents();

    directive = TestBed.inject(IconDirective);
  });

  it('should create an instance', () => {
    // Assert
    expect(directive).toBeTruthy();
  });
});
