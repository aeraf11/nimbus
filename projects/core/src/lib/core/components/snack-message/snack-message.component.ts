import { Component, ChangeDetectionStrategy, Inject, HostBinding } from '@angular/core';
import { MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { SnackMessageData, SnackMessageType } from '../../models';

@Component({
  selector: 'ni-snack-message',
  templateUrl: './snack-message.component.html',
  styleUrls: ['./snack-message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SnackMessageComponent {
  @HostBinding('class.snack-message') get snack() {
    return true;
  }

  @HostBinding('class.error') get error() {
    return this.data.type === SnackMessageType.Error;
  }

  @HostBinding('class.info') get info() {
    return this.data.type === SnackMessageType.Info;
  }

  @HostBinding('class.warn') get warn() {
    return this.data.type === SnackMessageType.Warn;
  }

  constructor(
    @Inject(MAT_SNACK_BAR_DATA) public data: SnackMessageData,
    public snackBarRef: MatSnackBarRef<SnackMessageComponent>
  ) {}
}
