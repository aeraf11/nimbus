import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarRef, MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';
import { SnackMessageType } from './../../models/snack-message-type';
import { SnackMessageComponent } from './snack-message.component';

describe('SnackMessageComponent', () => {
  let component: SnackMessageComponent;
  let fixture: ComponentFixture<SnackMessageComponent>;

  const dummyData = {
    type: SnackMessageType.Info,
    message: 'test',
    action: 'test',
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SnackMessageComponent],
      imports: [MatIconModule],
      providers: [
        { provide: MAT_SNACK_BAR_DATA, useValue: dummyData },
        { provide: MatSnackBarRef, useFactory: () => jasmine.createSpyObj('MatDialogRef', ['dismiss']) },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SnackMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
