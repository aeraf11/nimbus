import { AfterViewInit, ChangeDetectionStrategy, Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, UntypedFormControl } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { v4 as uuid } from 'uuid';

@UntilDestroy()
@Component({
  selector: 'ni-binary-size',
  templateUrl: './binary-size.component.html',
  styleUrls: ['./binary-size.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => BinarySizeComponent),
      multi: true,
    },
  ],
})
export class BinarySizeComponent implements ControlValueAccessor, AfterViewInit {
  onChange!: (value: any) => void;
  private onTouched!: () => void;
  @Input() id?: any;
  @Input() appearance?: any;
  @Input() value: any;
  @Input() required? = false;
  @Input() readonly? = false;
  @Input() placeholder? = '';
  @Input() tabindex?: number;
  @Input() errorStateMatcher = new ErrorStateMatcher();
  inputControl = new UntypedFormControl();
  buttonToggleControl = new UntypedFormControl();
  uniqueName = '';

  DEFAULT_UNIT = 'MB'; // MB is Default Unit, taken from Core behaviour

  toggleOptions: { value: string }[] = [
    {
      value: 'MB',
    },
    {
      value: 'GB',
    },
    {
      value: 'TB',
    },
  ];

  ngAfterViewInit(): void {
    this.uniqueName = this.getUniqueName();
    this.inputControl.valueChanges.pipe(untilDestroyed(this)).subscribe(() => {
      this.setValue();
    });
  }

  setValue() {
    let value = null;
    // Ensure we have both values, then update
    if (this.inputControl.value != null && this.buttonToggleControl.value != null) {
      value = {
        FloatValue: this.getFloatValue(this.buttonToggleControl.value, this.inputControl.value),
        EnteredValue: this.inputControl.value.toString(),
        Unit: this.buttonToggleControl.value,
      };
    }
    this.onChange(value);
  }

  onButtonToggleChange() {
    this.onTouched();
    this.setValue();
  }

  blur() {
    this.onTouched();
  }

  getAriaInputLabel(): string {
    return 'Binary Size Value';
  }

  getAriaButtonToggleLabel(): string {
    return 'Binary Size Unit';
  }

  getFloatValue(unit: string, value: number): number {
    let returnValue = 0;
    switch (unit) {
      case 'MB':
        returnValue = value * 1024;
        break;
      case 'GB':
        returnValue = value * 1048576;
        break;
      case 'TB':
        returnValue = value * 1073741824;
        break;
    }
    return returnValue;
  }

  writeValue(value: any): void {
    if (value == null) {
      this.buttonToggleControl.setValue(this.DEFAULT_UNIT);
    } else {
      this.setControlValues(value);
    }
  }

  setControlValues(value: any): void {
    this.buttonToggleControl.setValue(value.Unit);
    this.inputControl.setValue(value.EnteredValue);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  getUniqueName(): string {
    /*A throw away name is needed for inputs on Chrome. It's quite tricky to
      remove both autofills with a consistent method since Chrome can choose to ignore autofill off...
      https://stackoverflow.com/questions/12374442/chrome-ignores-autocomplete-off#:~:text=Chrome%20respects%20autocomplete%3Doff%20only,id%3D468153%20for%20more%20details.

      By giving a unique name to each input each time, Chrome seems to 'forget' you have previously entered any text.
      This way also still allows the configurable template option rather than the readonly method suggested
      elsewhere which would not...
    */
    return uuid();
  }
}
