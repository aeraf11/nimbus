import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BinarySizeComponent } from './binary-size.component';

describe('BinarySizeComponent', () => {
  let component: BinarySizeComponent;
  let fixture: ComponentFixture<BinarySizeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BinarySizeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BinarySizeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
