import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { DynamicRoutePageBanner } from './../../models/dynamic-route';
@Component({
  selector: 'ni-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageComponent {
  @Input() title = '';
  @Input() backButtonUrl = '';
  @Input() pageBanner: DynamicRoutePageBanner | undefined = undefined;
  @Input() hidePageTitle = false;
}
