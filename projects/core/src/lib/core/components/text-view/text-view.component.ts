import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'ni-text-view',
  templateUrl: './text-view.component.html',
  styleUrls: ['./text-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextViewComponent {
  @Input() formControl?: UntypedFormControl;
  @Input() text?: string;
  @Input() classNames?: any;
  @Input() singleLine?: boolean;
  @Input() alwaysShowLabel?: boolean;
  @Input() label?: string;
  @Input() icon?: string;
  @Input() labelClass?: string;
  @Input() inlineText?: string;
  @Input() dataType?: any;

  constructor() {}
}
