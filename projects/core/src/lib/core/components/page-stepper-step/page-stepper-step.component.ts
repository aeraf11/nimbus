import { CdkStep } from '@angular/cdk/stepper';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Component({
  selector: 'ni-page-stepper-step',
  templateUrl: './page-stepper-step.component.html',
  styleUrls: ['./page-stepper-step.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: CdkStep, useExisting: PageStepperStepComponent }],
})
export class PageStepperStepComponent extends CdkStep {
  @Input() heading?: string;
  @Input() icon?: string;
  @Input() subLabel?: string;
  @Input() buttonLabel?: string;
  @Input() buttonIcon?: string;
  @Input() buttonColour?: string;
  @Input() control?: AbstractControl | undefined;
}
