import { CdkStep, CdkStepper, CdkStepperModule } from '@angular/cdk/stepper';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PageStepperStepComponent } from './page-stepper-step.component';

describe('PageStepperStepComponent', () => {
  let component: PageStepperStepComponent;
  let fixture: ComponentFixture<PageStepperStepComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PageStepperStepComponent],
      imports: [CdkStepperModule],
      providers: [CdkStepper, CdkStep],
    }).compileComponents();

    fixture = TestBed.createComponent(PageStepperStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
