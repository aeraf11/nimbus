import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Router } from '@angular/router';
import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  ChangeDetectorRef,
  ElementRef,
  ViewEncapsulation,
  AfterViewInit,
  NgZone,
  Input,
} from '@angular/core';
import {
  AccumulationChart,
  AccumulationChartComponent,
  IAccAnimationCompleteEventArgs,
  IPointEventArgs,
  Points,
} from '@syncfusion/ej2-angular-charts';
import { Rect } from '@syncfusion/ej2-svg-base';
import { ChartDataAccumulation, ChartDataItem } from '@nimbus/core/src/lib/core';
import { debounceTime } from 'rxjs/operators';
import { utils } from '@nimbus/core/src/lib/core';
import { Observable } from 'rxjs';
@UntilDestroy()
@Component({
  selector: 'ni-chart-pie',
  templateUrl: './chart-pie.component.html',
  styleUrls: ['./chart-pie.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ChartPieComponent implements AfterViewInit {
  @Input() flexPosition: string | undefined;
  @Input() width: string | undefined;
  @Input() height: string | undefined;
  @Input() legend: any;
  @Input() tooltip: any;
  @Input() contextProperty: any;
  @Input() expandGrid: any;
  @Input() enableAnimation: boolean | undefined;
  @Input() startAngle: number | undefined;
  @Input() endAngle: number | undefined;
  @Input() innerRadius: string | undefined;
  @Input() dataLabel: any;
  @Input() centerTitles: boolean | undefined;
  @Input() chartLegendPadding: any;
  @Input() titleProportion: any;
  @Input() titleSize: any;
  @Input() subTitleProportion: any;
  @Input() subTitleSize: any;

  @Input() formControl?: any;
  @Input() formState?: any;

  @Input() model?: ChartDataAccumulation | null;
  @Input() data?: ChartDataItem[] | null;
  @Input() hasData?: boolean | null;
  @Input() title?: string | null;
  @Input() subTitle?: string | null;

  @ViewChild('pie')
  public pie?: AccumulationChartComponent | AccumulationChart;

  @ViewChild('titles')
  public titles?: ElementRef;

  @ViewChild('outer')
  public outer?: ElementRef;

  @ViewChild('noDataWrapper')
  public noDataWrapper?: ElementRef;

  resize$?: Observable<ResizeObserverEntry[] | undefined>;

  drawing = false;
  calculating = true;
  pieCenter = { x: '50%', y: '50%' };
  radius = '50%';
  _titleSize = '';
  _subTitleSize = '';
  _height = '';

  private chartMargin = 2;

  constructor(public change: ChangeDetectorRef, private router: Router, private zone: NgZone) {}

  ngAfterViewInit(): void {
    if (this.outer) {
      this.resize$ = utils.resizeObservable(this.outer?.nativeElement).pipe(untilDestroyed(this));
      this.resize$.pipe(untilDestroyed(this), debounceTime(500)).subscribe(() =>
        this.zone.run(() => {
          this.calculateGraphSize();
        })
      );
    }
  }

  onAnimationComplete(event: IAccAnimationCompleteEventArgs) {
    // This needs to be done here, not seriesRender to avoid inconsistent state from timing issues
    if (event.series?.accumulationBound && event.series?.innerRadius) {
      this.positionTitles(event.series.accumulationBound, event.series.innerRadius);
    }
    this.drawing = false;
  }

  onPointClick(event: IPointEventArgs) {
    const item = this.getPointData(event.point);
    if (item && item.url) {
      this.router.navigateByUrl(item.url);
    }

    if (this.contextProperty) {
      this.formState.context[this.contextProperty] = item.contextName ?? item.name ?? '';
      this.formState.contextChanged.next(this.contextProperty);
    }

    if (this.expandGrid) {
      this.formState.context['expandGrid'] = this.expandGrid;
      this.formState.contextChanged.next('expandGrid');
    }
  }

  positionTitles(bounds: Rect, radPercent: string) {
    if (this.titles && radPercent !== '0%') {
      const radDecimal: number = +radPercent.replace('%', '') / 100;
      const innerDiameter = bounds.width * radDecimal;
      const sideLength = Math.sqrt(Math.pow(innerDiameter, 2) / 2);
      const sidePx = `${sideLength}px`;
      const top = bounds.y + bounds.height / 2 - sideLength / 2;
      const left = bounds.x + bounds.width / 2 - sideLength / 2;

      this.titles.nativeElement.style.width = sidePx;
      this.titles.nativeElement.style.height = sidePx;
      this.titles.nativeElement.style.top = `${top}px`;
      this.titles.nativeElement.style.left = `${left}px`;
      this.titles.nativeElement.style.display = 'flex';
    }
  }

  getChartRect(id: string): DOMRect | null {
    if (!id) return null;
    const item = <SVGGElement>(<unknown>document.getElementById(`acc_chart_${id}_1_SeriesCollection`));
    if (item) return item.getBBox();
    return null;
  }

  getPointData(point: Points) {
    if (!point || !point.x) return null;
    return this.formControl?.value?.table1?.results?.find((item: any) => item.name === point.x) ?? null;
  }

  calcLegendX(legendWidth: string, totalWidth: number): number {
    const width = utils.getWidthFromString(legendWidth, totalWidth);
    return totalWidth - width;
  }

  private calculateGraphSize(): void {
    if (this.drawing) return;
    this.calculating = true;
    this.change.markForCheck();
    this.change.detectChanges();

    const noDataHeight = this.noDataWrapper ? this.noDataWrapper.nativeElement.offsetHeight : 0;
    const outerWidth = this.outer?.nativeElement.offsetWidth;
    const outerHeight = this.outer?.nativeElement.offsetHeight - noDataHeight;
    const legendWidth = this.legend.visible ? utils.getWidthFromString(this.legend.width, outerWidth) : 0;
    const padding = this.legend.visible ? utils.getWidthFromString(this.chartLegendPadding, outerWidth) : 0;
    const widthBasedDiameter = Math.ceil(outerWidth - legendWidth - padding);
    let heightBasedDiameter = outerHeight;
    if (heightBasedDiameter === 0) heightBasedDiameter = widthBasedDiameter;
    const diameter = widthBasedDiameter > heightBasedDiameter ? heightBasedDiameter : widthBasedDiameter;
    let surplus = (outerWidth - diameter - legendWidth - padding) / 2;
    if (surplus < 0) surplus = 0;

    //Chart margin added to make the radius ever so slightly smaller so it's not cut off
    //inside the wrapper div since it needs a position and we can't modify rectangle directly
    this.radius = `${diameter / 2 - this.chartMargin}px`;
    this._height = `${diameter}px`;
    this.pieCenter.x = `${diameter / 2 + surplus - 10}`;
    this.pieCenter.y = `${diameter / 2 - 10}px`;
    this.legend.location.x = diameter + padding + surplus;
    this._titleSize = this.getProportionedTitleSize(diameter, this.titleProportion, this.titleSize);
    this._subTitleSize = this.getProportionedTitleSize(diameter, this.subTitleProportion, this.subTitleSize);

    this.calculating = false;
    this.drawing = true;
    this.change.markForCheck();
  }

  getProportionedTitleSize(diameter: number, proportion: number, override: string): string {
    if (override) return override;
    else return `${diameter * proportion}px`;
  }
}
