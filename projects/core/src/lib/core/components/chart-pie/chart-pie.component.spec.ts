import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartPieComponent } from './chart-pie.component';
import { StateService } from '../../services/state.service';
import { ChartDataService } from '../../services/chart-data.service';
import { MockChartDataService } from '../../services/chart-data.service.spec';
import { MockStateService } from '../../services/mock-state.service.spec';

describe('ChartPieComponent', () => {
  let component: ChartPieComponent;
  let fixture: ComponentFixture<ChartPieComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ChartPieComponent],
      providers: [
        { provide: StateService, useClass: MockStateService },
        { provide: ChartDataService, useClass: MockChartDataService },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ChartPieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
