import { Component, OnInit, Input } from '@angular/core';
import { Person } from '../../models/person';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { ProfileSize } from '../../models';

@Component({
  selector: 'ni-person-list-view',
  templateUrl: './person-list-view.component.html',
  styleUrls: ['./person-list-view.component.scss'],
})
export class PersonListViewComponent implements OnInit {
  @Input() people?: Person[];
  @Input() fields?: FormlyFieldConfig[];
  @Input() imageSize: ProfileSize = 'medium';
  @Input() photoProp = '';
  @Input() initialsProp = '';
  @Input() noDataString = 'No data';
  @Input() personWidth? = '100%';

  constructor() {}

  ngOnInit(): void {}
}
