import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchResultCaseViewComponent } from './search-result-case-view.component';

describe('SearchResultCaseViewComponent', () => {
  let component: SearchResultCaseViewComponent;
  let fixture: ComponentFixture<SearchResultCaseViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SearchResultCaseViewComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(SearchResultCaseViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('operationUrl', () => {
    it('should return the correct url when an operationId is provided', () => {
      component.operationId = 'guid';
      expect(component.operationUrl).toEqual('/Operations/View/guid');
    });

    it('should return null when no operationId is provided', () => {
      expect(component.operationUrl).toBeNull();
    });
  });

  describe('target', () => {
    it('should return "_blank" when openResultsInNewTab is true', () => {
      component.openResultsInNewTab = true;
      expect(component.target).toEqual('_blank');
    });

    it('should return "_self" when openResultsInNewTab is false', () => {
      component.openResultsInNewTab = false;
      expect(component.target).toEqual('_self');
    });

    it('should return "_self" when openResultsInNewTab is not set', () => {
      expect(component.target).toEqual('_self');
    });
  });
});
