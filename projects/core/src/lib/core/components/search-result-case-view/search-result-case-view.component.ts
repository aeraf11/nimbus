import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'ni-search-result-case-view',
  templateUrl: './search-result-case-view.component.html',
  styleUrls: ['./search-result-case-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultCaseViewComponent {
  @Input() openResultsInNewTab = false;
  @Input() operationId!: string;
  @Input() operationName!: string;
  @Input() operationAlias!: string;

  get operationUrl(): string | null {
    return this.operationId ? `/Operations/View/${this.operationId}` : null;
  }

  get target(): string {
    return this.openResultsInNewTab ? '_blank' : '_self';
  }
}
