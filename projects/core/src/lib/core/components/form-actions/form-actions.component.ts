import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { FormActionsItem } from '../../models';

@Component({
  selector: 'ni-form-actions',
  templateUrl: './form-actions.component.html',
  styleUrls: ['./form-actions.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormActionsComponent {
  @Input() gap = '16px';
  @Input() items: FormActionsItem[] = [];
  @Output() clicked = new EventEmitter<FormActionsItem>();

  processClick(item: FormActionsItem) {
    this.clicked.emit(item);
  }
}
