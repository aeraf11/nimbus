import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import {
  DialogFormAction,
  DialogFormComponent,
  DialogFormResult,
  EditorMode,
  GroupConfig,
  TableTypeColumnDefinition,
} from '@nimbus/core/src/lib/core';
import { TableEditorComponent } from './table-editor.component';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Subject, bufferCount } from 'rxjs';

@Component({
  selector: 'ni-entity-table',
})
class StubEntityTableComponent {
  @Input() columnDefinitions: TableTypeColumnDefinition[] = [];
  @Input() items: unknown[] = [];
  @Input() context?: { entityId?: string; entityTypeId?: number; entityDescription?: string };
  @Input() required = false;
  @Input() disabled = false;
  @Input() placeholder!: string;
  @Input() ariaTableName?: string;
  @Input() ariaRowLabel?: string;
  @Input() groupBy?: GroupConfig | null;
  @Input() rootModel?: unknown;
  @Input() localModel?: unknown;
  @Output() rowClick = new EventEmitter<{ [key: string]: unknown }>();
}

describe('TableEditorComponent', () => {
  let afterClosedSubject: Subject<DialogFormResult>;
  let spyDialogRef: jasmine.SpyObj<MatDialogRef<unknown>>;
  let spyMatDialog: jasmine.SpyObj<MatDialog>;
  let spyOnChange: jasmine.Spy;
  let spyOnTouched: jasmine.Spy;

  let component: TableEditorComponent;
  let fixture: ComponentFixture<TableEditorComponent>;

  beforeEach(() => {
    afterClosedSubject = new Subject<DialogFormResult>();
    spyDialogRef = jasmine.createSpyObj<MatDialogRef<unknown>>('MatDialogRef', { afterClosed: afterClosedSubject });
    spyMatDialog = jasmine.createSpyObj<MatDialog>('MatDialog', { open: spyDialogRef });

    spyOnChange = jasmine.createSpy('onChange');
    spyOnTouched = jasmine.createSpy('onTouched');
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatDialogModule],
      providers: [{ provide: MatDialog, useValue: spyMatDialog }],
      declarations: [TableEditorComponent, StubEntityTableComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableEditorComponent);
    component = fixture.componentInstance;
    component.registerOnChange(spyOnChange);
    component.registerOnTouched(spyOnTouched);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should set ariaTableName if ariaTableName is undefined', () => {
      // Arrange
      component.ariaTableName = undefined as string | undefined;
      // Act
      component.ngOnInit();
      // Assert
      expect(component.ariaTableName).toEqual('Table of data');
    });

    it('should set ariaTableName using title if ariaTableName is undefined', () => {
      // Arrange
      component.ariaTableName = undefined as string | undefined;
      component.title = 'things';
      // Act
      component.ngOnInit();
      // Assert
      expect(component.ariaTableName).toEqual('Table of things');
    });
  });

  describe('columnDefinitions', () => {
    let columnDefinitions: TableTypeColumnDefinition[];

    beforeEach(() => {
      // Arrange
      columnDefinitions = [
        {
          key: 'key1',
          columnName: 'col1',
          tab: 'tab1',
          dataType: 'text',
          isEditorHeader: true,
        },
        {
          key: 'key2',
          columnName: 'col2',
          tab: 'tab1',
          dataType: 'date',
          isEditorHeader: false,
        },
      ];
    });

    it('should set column definitions', () => {
      // Act
      component.columnDefinitions = columnDefinitions;
      // Assert
      expect(component.columnDefinitions).toEqual(columnDefinitions);
    });

    it('should set editorTitleField', () => {
      // Act
      component.columnDefinitions = columnDefinitions;
      // Assert
      expect(component.editorTitleField).toEqual('key1');
    });
  });

  describe('openAddDialog', () => {
    it('should call MatDialog.open', () => {
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalled();
    });

    it('should call MatDialog.open with default values in model', () => {
      // Arrange
      const item = { key: 'value' };
      component.defaultValues = item;
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({ model: item }),
      });
    });

    it('should call MatDialog.open with dialogFormConfig', () => {
      // Arrange
      const dialogFormConfig = [{ key: 'value' }];
      component.dialogFormConfig = dialogFormConfig;
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({ fieldConfig: dialogFormConfig }),
      });
    });

    it('should call MatDialog.open with enableDelete flag in config', () => {
      // Arrange
      component.enableDelete = true;
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({ config: jasmine.objectContaining({ enableDelete: true }) }),
      });
    });

    it('should call MatDialog.open with enableEdit flag in config', () => {
      // Arrange
      component.enableEdit = true;
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({ config: jasmine.objectContaining({ enableEdit: true }) }),
      });
    });

    it('should call MatDialog.open with enableHide flag in config', () => {
      // Arrange
      component.enableHide = true;
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({ config: jasmine.objectContaining({ enableHide: true }) }),
      });
    });

    it('should call MatDialog.open with addText in config', () => {
      // Arrange
      component.addText = 'add';
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({ config: jasmine.objectContaining({ addText: 'add' }) }),
      });
    });

    it('should call MatDialog.open with deleteText in config', () => {
      // Arrange
      component.deleteText = 'delete';
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({ config: jasmine.objectContaining({ deleteText: 'delete' }) }),
      });
    });

    it('should call MatDialog.open with cancelText in config', () => {
      // Arrange
      component.cancelText = 'cancel';
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({ config: jasmine.objectContaining({ cancelText: 'cancel' }) }),
      });
    });

    it('should call MatDialog.open with saveText in config', () => {
      // Arrange
      component.saveText = 'save';
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({ config: jasmine.objectContaining({ saveText: 'save' }) }),
      });
    });

    it('should call MatDialog.open with hideText in config', () => {
      // Arrange
      component.hideText = 'hide';
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({ config: jasmine.objectContaining({ hideText: 'hide' }) }),
      });
    });

    it('should call MatDialog.open with titleTemplate in config', () => {
      // Arrange
      const titleTemplate = 'template';
      component.title = titleTemplate;
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({
          config: jasmine.objectContaining({ titleTemplate }),
        }),
      });
    });

    it('should call MatDialog.open with isChildForm set to true in config', () => {
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({
          config: jasmine.objectContaining({ isChildForm: true }),
        }),
      });
    });

    it('should call MatDialog.open with editorMode set to Add', () => {
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({
          editorMode: EditorMode.Add,
        }),
      });
    });

    it('should call MatDialog.open with disabled disabled flag', () => {
      // Arrange
      component.disabled = true;
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({
          disabled: true,
        }),
      });
    });

    it('should call MatDialog.open with dialogFormOptions', () => {
      // Arrange
      const dialogFormOptions = { formState: 'test' };
      component.dialogFormOptions = dialogFormOptions;
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({
          options: dialogFormOptions,
        }),
      });
    });

    it('should call MatDialog.open with dialogWidth', () => {
      // Arrange
      const dialogWidth = '50rem';
      component.dialogWidth = dialogWidth;
      // Act
      component.openAddDialog();
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.anything(),
        width: dialogWidth,
        maxWidth: '',
      });
    });

    it('should add item from _items to value array when afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'value' };
      const result = { action: DialogFormAction.Add, model: { _items: item } };
      // Act
      component.openAddDialog();
      afterClosedSubject.next(result);
      // Assert
      expect(component.value).toEqual([item]);
    });

    it('should add items from _items to value array when afterClosed observable emits', () => {
      // Arrange
      const items = [{ key: 'val1' }, { key: 'val2' }];
      const result = { action: DialogFormAction.Add, model: { _items: items } };
      // Act
      component.openAddDialog();
      afterClosedSubject.next(result);
      // Assert
      expect(component.value).toEqual(items);
    });

    it('should add item to value array when afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'val1' };
      const result = { action: DialogFormAction.Add, model: item };
      // Act
      component.openAddDialog();
      afterClosedSubject.next(result);
      // Assert
      expect(component.value).toEqual([item]);
    });

    it('should add item to value array with new id when afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'val1' };
      const result = { action: DialogFormAction.Add, model: item };
      // Act
      component.openAddDialog();
      afterClosedSubject.next(result);
      // Assert
      console.log(component.value);
      const id = (component.value[0] as { _id: string })._id;
      expect(id).toMatch(/[0-9A-Fa-f]{8}-?([0-9A-Fa-f]{4}-?){3}[0-9A-Fa-f]{12}/);
    });

    it('should emit item from addQueueItem output when afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'val1' };
      const dialogResult = { action: DialogFormAction.Add, model: item };
      let result = null as typeof item | null;
      component.addQueueItem.subscribe((value) => {
        result = value as { key: string };
      });
      // Act
      component.openAddDialog();
      afterClosedSubject.next(dialogResult);
      // Assert
      expect(result).toEqual(item);
    });

    it('should add uploads to value array as individual items when afterClosed observable emits', () => {
      // Arrange
      const model = { upload: [{ key: 'val1' }, { key: 'val2' }] };
      const result = { action: DialogFormAction.Add, model: model };
      // Act
      component.openAddDialog();
      afterClosedSubject.next(result);
      // Assert
      expect(component.value).toEqual([
        jasmine.objectContaining({ upload: [{ key: 'val1' }] }),
        jasmine.objectContaining({ upload: [{ key: 'val2' }] }),
      ]);
    });

    it('should add uploads to value array with description when afterClosed observable emits', () => {
      // Arrange
      const model = { upload: [{ key: 'val1' }, { key: 'val2' }], description: 'test' };
      const result = { action: DialogFormAction.Add, model: model };
      // Act
      component.openAddDialog();
      afterClosedSubject.next(result);
      // Assert
      expect(component.value).toEqual([
        jasmine.objectContaining({ upload: [{ key: 'val1' }], description: 'test' }),
        jasmine.objectContaining({ upload: [{ key: 'val2' }], description: 'test' }),
      ]);
    });

    it('should emit uploads from addQueueItem output when afterClosed observable emits', () => {
      // Arrange
      const model = { upload: [{ key: 'val1' }, { key: 'val2' }] };
      const dialogResult = { action: DialogFormAction.Add, model: model };
      let result = [] as (typeof model)[];
      // Group the next two emitted values into an array
      component.addQueueItem.pipe(bufferCount(2)).subscribe((values) => {
        result = values as (typeof model)[];
      });
      // Act
      component.openAddDialog();
      afterClosedSubject.next(dialogResult);
      // Assert
      expect(result).toEqual([
        jasmine.objectContaining({ upload: [{ key: 'val1' }] }),
        jasmine.objectContaining({ upload: [{ key: 'val2' }] }),
      ]);
    });

    it('should call onChange with the new items when afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'val1' };
      const result = { action: DialogFormAction.Add, model: item };
      // Act
      component.openAddDialog();
      afterClosedSubject.next(result);
      // Assert
      expect(spyOnChange).toHaveBeenCalledWith([item]);
    });

    it('should call onTouched if not disabled when afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'val1' };
      const result = { action: DialogFormAction.Add, model: item };
      // Act
      component.openAddDialog();
      afterClosedSubject.next(result);
      // Assert
      expect(spyOnTouched).toHaveBeenCalled();
    });

    it('should not call onTouched if disabled when afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'val1' };
      const result = { action: DialogFormAction.Add, model: item };
      component.disabled = true;
      // Act
      component.openAddDialog();
      afterClosedSubject.next(result);
      // Assert
      expect(spyOnTouched).not.toHaveBeenCalled();
    });
  });

  describe('openEditDialog', () => {
    it('should not call MatDialog.open if enableEdit is false', () => {
      // Arrange
      component.enableEdit = false;
      // Act
      component.openEditDialog({});
      // Assert
      expect(spyMatDialog.open).not.toHaveBeenCalled();
    });

    it('should call MatDialog.open if enableEdit is true', () => {
      // Arrange
      component.enableEdit = true;
      // Act
      component.openEditDialog({});
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalled();
    });

    it('should call MatDialog.open with the provided model', () => {
      // Arrange
      component.enableEdit = true;
      const item = { key: 'value' };
      // Act
      component.openEditDialog(item);
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({ model: item }),
      });
    });

    it('should call MatDialog.open with enableDelete flag in config if canDelete is set in model', () => {
      // Arrange
      const item = { key: 'value', canDelete: true };
      // Act
      component.openEditDialog(item);
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({ config: jasmine.objectContaining({ enableDelete: true }) }),
      });
    });

    it('should call MatDialog.open with titleTemplate in config set to model value if editorTitleField is set', () => {
      // Arrange
      const item = { key: 'value' };
      component.editorTitleField = 'key';
      // Act
      component.openEditDialog(item);
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({
          config: jasmine.objectContaining({ titleTemplate: 'value' }),
        }),
      });
    });

    it('should call MatDialog.open with editorMode set to Edit', () => {
      // Arrange
      const item = { key: 'value' };
      // Act
      component.openEditDialog(item);
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({
          editorMode: EditorMode.Edit,
        }),
      });
    });

    it('should call MatDialog.open with editorMode set to model editorMode', () => {
      // Arrange
      const item = { key: 'value', editorMode: EditorMode.View };
      // Act
      component.openEditDialog(item);
      // Assert
      expect(spyMatDialog.open).toHaveBeenCalledWith(DialogFormComponent, {
        data: jasmine.objectContaining({
          editorMode: EditorMode.View,
        }),
      });
    });

    it('should remove the item from the value array when action is Delete and afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'value' };
      component.value = [item];
      const dialogResult = { action: DialogFormAction.Delete, model: item };
      // Act
      component.openEditDialog(item);
      afterClosedSubject.next(dialogResult);
      // Assert
      expect(component.value).toEqual([]);
    });

    it('should emit item from deleteQueueItem output when action is Delete and afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'value' };
      component.value = [item];
      const dialogResult = { action: DialogFormAction.Delete, model: item };
      let result = null as typeof item | null;
      component.deleteQueueItem.subscribe((value) => {
        result = value as { key: string };
      });
      // Act
      component.openEditDialog(item);
      afterClosedSubject.next(dialogResult);
      // Assert
      expect(result).toEqual(item);
    });

    it('should call onChange with the value array when the action is Delete and afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'value' };
      component.value = [item];
      const dialogResult = { action: DialogFormAction.Delete, model: item };
      // Act
      component.openEditDialog(item);
      afterClosedSubject.next(dialogResult);
      // Assert
      expect(spyOnChange).toHaveBeenCalledWith([]);
    });

    it('should update item properties when action is Edit and afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'value' };
      const editedItem = { key: 'edit' };
      component.value = [item];
      const dialogResult = { action: DialogFormAction.Edit, model: editedItem };
      // Act
      component.openEditDialog(item);
      afterClosedSubject.next(dialogResult);
      // Assert
      expect(component.value).toEqual([editedItem]);
    });

    it('should call onChange with the value array when the action is Edit and afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'value' };
      const editedItem = { key: 'edit' };
      component.value = [item];
      const dialogResult = { action: DialogFormAction.Edit, model: editedItem };
      // Act
      component.openEditDialog(item);
      afterClosedSubject.next(dialogResult);
      // Assert
      expect(spyOnChange).toHaveBeenCalledWith([editedItem]);
    });

    it('should set item._isVisuallyHidden to true when action is Hide and afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'value' };
      component.value = [item];
      const dialogResult = { action: DialogFormAction.Hide, model: item };
      // Act
      component.openEditDialog(item);
      afterClosedSubject.next(dialogResult);
      // Assert
      expect(component.value).toEqual([{ ...item, _isVisuallyHidden: true }]);
    });

    it('should call onChange with the value array when the action is Hide and afterClosed observable emits', () => {
      // Arrange
      const item = { key: 'value' };
      component.value = [item];
      const dialogResult = { action: DialogFormAction.Hide, model: item };
      // Act
      component.openEditDialog(item);
      afterClosedSubject.next(dialogResult);
      // Assert
      expect(spyOnChange).toHaveBeenCalledWith([{ ...item, _isVisuallyHidden: true }]);
    });
  });
});
