import { GroupConfig } from './../entity-table/entity-table.component';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import {
  DialogFormAction,
  DialogFormComponent,
  DialogFormData,
  DialogFormResult,
  EditorMode,
  TableTypeColumnDefinition,
} from '@nimbus/core/src/lib/core';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { OnChange } from 'property-watch-decorator';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import deepClone from 'deep.clone';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'ni-table-editor',
  templateUrl: './table-editor.component.html',
  styleUrls: ['./table-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TableEditorComponent),
      multi: true,
    },
  ],
})
export class TableEditorComponent implements ControlValueAccessor, OnInit {
  /* EntityTableComponent bindings */
  @OnChange<TableTypeColumnDefinition[] | undefined>(function (this: TableEditorComponent, value) {
    if (value && value.length > 0) {
      this.editorTitleField = value.find((col) => col.isEditorHeader)?.key;
      this.changeDetector.markForCheck();
    }
  })
  @Input()
  columnDefinitions: TableTypeColumnDefinition[] = [];

  @Input() context?: { entityId?: string; entityTypeId?: number; entityDescription?: string };
  @Input() rootModel!: unknown;
  @Input() localModel?: unknown;
  @Input() required = false;
  @Input() disabled = false;
  @Input() ariaTableName?: string;
  @Input() ariaRowLabel = 'Open dialog';
  @Input() placeholder!: string;
  @Input() groupBy?: GroupConfig | null;

  /* TableEditorComponent bindings */
  @Input() dialogFormConfig?: FormlyFieldConfig[] | null;
  @Input() dialogFormOptions?: FormlyFormOptions;
  @Input() title?: string;
  @Input() addText = 'Add';
  @Input() cancelText = 'Cancel';
  @Input() deleteText = 'Delete';
  @Input() hideText = 'Hide';
  @Input() saveText = 'Save';
  @Input() enableAdd = true;
  @Input() enableDelete = true;
  @Input() enableEdit = true;
  @Input() enableHide = false;
  @Input() defaultValues: { [key: string]: unknown } = {};
  @Input() itemLimit?: number;
  @Input() dialogWidth?: string;
  @Output() addQueueItem = new EventEmitter<{ [key: string]: unknown }>();
  @Output() deleteQueueItem = new EventEmitter<{ [key: string]: unknown }>();

  onChange!: (value: unknown) => void;
  onTouched!: () => void;

  value: unknown[] = [];
  editorTitleField?: string;

  get addButtonIsDisabled() {
    return Array.isArray(this.value) && this.itemLimit != null && this.value.length >= this.itemLimit;
  }

  constructor(private dialog: MatDialog, private changeDetector: ChangeDetectorRef) {}

  ngOnInit(): void {
    if (!this.ariaTableName) {
      this.ariaTableName = `Table of ${this.title ?? 'data'}`;
    }
  }

  openEditDialog(item: { [key: string]: unknown }) {
    if (this.enableEdit) {
      this.openDialog(item);
    }
  }

  openAddDialog() {
    this.openDialog();
  }

  private openDialog(model?: { [key: string]: unknown }): void {
    this.changeDetector.markForCheck();

    const data: DialogFormData = {
      model: { ...this.defaultValues, ...model },
      fieldConfig: this.dialogFormConfig,
      config: {
        enableDelete: (model?.canDelete as boolean) ?? this.enableDelete,
        enableEdit: this.enableEdit,
        enableHide: this.enableHide,
        addText: this.addText,
        deleteText: this.deleteText,
        cancelText: this.cancelText,
        saveText: this.saveText,
        hideText: this.hideText,
        titleTemplate: model && this.editorTitleField ? (model[this.editorTitleField] as string) : this.title,
        isChildForm: true,
      },
      editorMode:
        model == null ? EditorMode.Add : model.editorMode ? (model.editorMode as EditorMode) : EditorMode.Edit,
      disabled: this.disabled,
      options: this.dialogFormOptions,
    };

    const dialogConfig: MatDialogConfig<DialogFormData> = { data };

    if (this.dialogWidth) {
      dialogConfig.width = this.dialogWidth;
      dialogConfig.maxWidth = '';
    }

    const dialogRef = this.dialog.open(DialogFormComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result: DialogFormResult) => {
      this.onDialogClosed(result, model);

      if (!this.disabled) {
        this.onTouched();
      }

      this.changeDetector.markForCheck();
    });
  }

  private onDialogClosed(result: DialogFormResult, model?: { [key: string]: unknown }) {
    switch (result.action) {
      case DialogFormAction.Add:
        this.onAddItem(result);
        break;
      case DialogFormAction.Cancel:
        break;
      case DialogFormAction.Delete:
        this.onDeleteItem(model);
        break;
      case DialogFormAction.Edit:
        this.onEditItem(result, model);
        break;
      case DialogFormAction.Hide:
        this.onHideItem(model);
        break;
    }
  }

  private onAddItem(result: DialogFormResult) {
    // Handle output from datatable where item(s) are not root level
    if (result.model._items) {
      if (Array.isArray(result.model._items)) {
        this.value = [...this.value, ...result.model._items];
      } else {
        this.value = [...this.value, result.model._items];
      }
    } else if (result.model?.upload?.length > 1) {
      const values: unknown[] = [];
      result.model.upload.forEach((upload: unknown) => {
        const uploadItem: { [key: string]: unknown } = {
          upload: [upload],
        };
        if (result.model.description) {
          uploadItem['description'] = result.model.description;
        }
        this.addClientId(uploadItem);
        this.addQueueItem.emit(uploadItem);
        values.push(uploadItem);
      });
      this.value = [...this.value, ...values];
    } else {
      // Track items with client id, enables model syncing in repeater for example)
      this.value = [...this.value, this.addClientId(result.model)];
      this.addQueueItem.emit(result.model);
    }
    this.onChange(this.value);
  }

  private onDeleteItem(model?: { [key: string]: unknown }) {
    this.value = this.value.filter((item) => item !== model);
    this.onChange(this.value);
    this.deleteQueueItem.emit(model);
  }

  private onEditItem(result: DialogFormResult, model?: { [key: string]: unknown }) {
    for (const property in result.model) {
      if (model) {
        model[property] = result.model[property];
      }
    }
    this.value = [...this.value];
    this.onChange(this.value);
  }

  private onHideItem(model?: { [key: string]: unknown }) {
    const itemToHide = this.value.filter((item) => item === model)[0];
    if (itemToHide) {
      (itemToHide as { _isVisuallyHidden: boolean })._isVisuallyHidden = true;
    }
    this.value = [...this.value];
    this.onChange(this.value);
  }

  writeValue(items: unknown[]): void {
    deepClone<{ items: unknown[] }>({ items }).then((clone) => {
      if (clone.items) {
        clone.items.forEach((item) => this.addClientId(item));
        this.value = clone.items;
        this.changeDetector.markForCheck();
      }
    });
  }

  registerOnChange(fn: (value: unknown) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  addClientId(item: unknown): unknown {
    if (typeof item === 'object' && item != null && !item.hasOwnProperty('_id')) {
      (item as { _id: string })._id = uuid();
    }
    return item;
  }
}
