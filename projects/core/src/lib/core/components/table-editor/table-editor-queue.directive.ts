import { take } from 'rxjs/operators';
import { Directive, OnInit, Input } from '@angular/core';
import { TableEditorComponent } from '../table-editor/table-editor.component';
import { QueueConfig } from '../../models';
import { FormsHelperService, IndexedDbService } from '../../services';
import { utils } from '../../utils';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Directive({
  selector: '[niTableEditorQueue]',
})
export class TableEditorQueueDirective implements OnInit {
  @Input('niTableEditorQueue') queueConfig!: QueueConfig;
  @Input() context?: { entityId?: string; entityTypeId?: string; entityDescription?: string };
  @Input() rootModel!: unknown;
  @Input() localModel?: unknown;

  constructor(
    private tableEditor: TableEditorComponent,
    private formsHelper: FormsHelperService,
    private dbService: IndexedDbService
  ) {}

  ngOnInit(): void {
    if (!this.queueConfig) {
      return;
    }

    this.tableEditor.addQueueItem.pipe(untilDestroyed(this)).subscribe((item) => {
      if (item.hasOwnProperty('_id')) {
        this.addQueueItem(item);
      }
    });

    this.tableEditor.deleteQueueItem.pipe(untilDestroyed(this)).subscribe((item) => {
      if (item.hasOwnProperty('_id')) {
        this.deleteQueueItem(item as { _id: string });
      }
    });

    // Load queued items
    const entityId = this.queueConfig.entityId
      ? this.formsHelper.parseTemplate(this.queueConfig.entityId, this.rootModel, '')
      : this.context?.entityId;

    this.dbService
      .getEntityAttachments(this.queueConfig.queueTableName, entityId)
      .pipe(take(1))
      .subscribe((items: unknown[]) => {
        if (items.length > 0) {
          this.tableEditor.writeValue(items);
        }
      });
  }

  private addQueueItem(item: { [key: string]: unknown }) {
    const { entityId, entityTypeId, description, editUrl, parentEntityId } = this.getEntityProps();
    if (entityId && entityTypeId) {
      this.dbService.table(this.queueConfig.queueTableName).add({
        id: item._id, //set queue id to match inner item id, as the inner item is passed later for delete
        item,
        entityId,
        entityTypeId,
        description,
        status: 'Queued',
        editUrl,
        parentEntityId,
      });
    }
  }

  private deleteQueueItem(item: { _id: string }) {
    // item._id is the queue item id
    this.dbService.table(this.queueConfig.queueTableName).delete(item._id);
    if (this.queueConfig.itemIdExpression) {
      //this gets us the file id based on the model for example.
      const idExpr = utils.evalStringFormExpression<string>(this.queueConfig.itemIdExpression);
      if (idExpr) {
        const itemId = idExpr(this.rootModel, item, this.context, this.formsHelper);
        // delete from item table for example files
        this.dbService.table(this.queueConfig.itemTableName).delete(itemId);
      }
    }
  }

  private getEntityProps(): {
    entityId: string;
    entityTypeId: string;
    description: string;
    editUrl: string;
    parentEntityId: string;
  } {
    const model = this.queueConfig.useLocalModel ? this.localModel : this.rootModel;

    const entityId = this.queueConfig.entityId
      ? this.formsHelper.parseTemplate(this.queueConfig.entityId, model, '')
      : this.context?.entityId;

    const entityTypeId = this.queueConfig.entityTypeId
      ? this.formsHelper.parseTemplate(this.queueConfig.entityTypeId, model, '')
      : this.context?.entityTypeId;

    const description = this.queueConfig.description
      ? this.formsHelper.parseTemplate(this.queueConfig.description, model, '')
      : this.context?.entityDescription;

    const editUrl = this.queueConfig.editUrl
      ? this.formsHelper.parseTemplate(this.queueConfig.editUrl, this.rootModel, '')
      : '';

    // Always use rootModel with parentEntity as the first ${tempId} on the model will be the parents id
    const parentEntityId = (this.rootModel as { tempId: string })?.tempId ?? '';
    return { entityId, entityTypeId, description, editUrl, parentEntityId };
  }
}
