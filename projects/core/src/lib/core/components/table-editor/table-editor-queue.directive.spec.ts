import { Table } from 'dexie';
import { FormsHelperService, IndexedDbService } from '../../services';
import { TableEditorQueueDirective } from './table-editor-queue.directive';
import { TableEditorComponent } from './table-editor.component';
import { Subject } from 'rxjs';

describe('TableEditorQueueDirective', () => {
  let addItemSubject: Subject<{ [key: string]: unknown }>;
  let deleteItemSubject: Subject<{ [key: string]: unknown }>;
  let spyTableEditorComponent: jasmine.SpyObj<TableEditorComponent>;

  let spyFormsHelperService: jasmine.SpyObj<FormsHelperService>;

  let getEntityAttachmentsSubject: Subject<unknown[]>;
  let spyTable: jasmine.SpyObj<Table>;
  let spyIndexedDbService: jasmine.SpyObj<IndexedDbService>;

  let directive: TableEditorQueueDirective;

  beforeEach(() => {
    addItemSubject = new Subject();
    deleteItemSubject = new Subject();
    spyTableEditorComponent = jasmine.createSpyObj('TableEditorComponent', ['writeValue'], {
      addQueueItem: addItemSubject.asObservable(),
      deleteQueueItem: deleteItemSubject.asObservable(),
    });

    spyFormsHelperService = jasmine.createSpyObj('FormsHelperService', ['parseTemplate']);

    getEntityAttachmentsSubject = new Subject();
    spyTable = jasmine.createSpyObj('Table', ['add', 'delete']);
    spyIndexedDbService = jasmine.createSpyObj('IndexedDbService', {
      getEntityAttachments: getEntityAttachmentsSubject.asObservable(),
      table: spyTable,
    });
  });

  beforeEach(() => {
    directive = new TableEditorQueueDirective(spyTableEditorComponent, spyFormsHelperService, spyIndexedDbService);
    directive.queueConfig = {
      queueTableName: 'queueTableName',
      itemTableName: 'itemTableName',
    };
    directive.rootModel = {};
  });

  it('should create an instance', () => {
    // Assert
    expect(directive).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should subscribe to addQueueItem', () => {
      // Arrange
      spyOn(addItemSubject, 'subscribe');
      // Act
      directive.ngOnInit();
      // Assert
      expect(addItemSubject.subscribe).toHaveBeenCalled();
    });

    it('should subscribe to deleteQueueItem', () => {
      // Arrange
      spyOn(deleteItemSubject, 'subscribe');
      // Act
      directive.ngOnInit();
      // Assert
      expect(deleteItemSubject.subscribe).toHaveBeenCalled();
    });

    it('should call FormsHelperService.parseTemplate if queueConfig.entityId is set', () => {
      // Arrange
      directive.queueConfig.entityId = 'entityId';
      // Act
      directive.ngOnInit();
      // Assert
      expect(spyFormsHelperService.parseTemplate).toHaveBeenCalled();
    });

    it('should not call FormsHelperService.parseTemplate if queueConfig.entityId is not set', () => {
      // Act
      directive.ngOnInit();
      // Assert
      expect(spyFormsHelperService.parseTemplate).not.toHaveBeenCalled();
    });

    it('should call IndexedDbService.getEntityAttachments', () => {
      // Arrange
      directive.context = { entityId: 'entityId' };
      // Act
      directive.ngOnInit();
      // Assert
      expect(spyIndexedDbService.getEntityAttachments).toHaveBeenCalled();
    });

    it('should call TableEditorComponent.writeValue if items are returned from IndexedDbService.getEntityAttachments', () => {
      // Arrange
      directive.context = { entityId: 'entityId' };
      // Act
      directive.ngOnInit();
      getEntityAttachmentsSubject.next([{}]);
      // Assert
      expect(spyTableEditorComponent.writeValue).toHaveBeenCalled();
    });

    it('should not call TableEditorComponent.writeValue if no items are returned from IndexedDbService.getEntityAttachments', () => {
      // Arrange
      directive.context = { entityId: 'entityId' };
      // Act
      directive.ngOnInit();
      getEntityAttachmentsSubject.next([]);
      // Assert
      expect(spyTableEditorComponent.writeValue).not.toHaveBeenCalled();
    });

    it('should only call TableEditorComponent.writeValue once if items are returned from IndexedDbService.getEntityAttachments more than once', () => {
      // Arrange
      directive.context = { entityId: 'entityId' };
      // Act
      directive.ngOnInit();
      getEntityAttachmentsSubject.next([{}]);
      getEntityAttachmentsSubject.next([{}]);
      // Assert
      expect(spyTableEditorComponent.writeValue).toHaveBeenCalledTimes(1);
    });
  });

  describe('addQueueItem', () => {
    it('should call FormsHelperService.parseTemplate if queueConfig.entityId is set', () => {
      // Arrange
      const entityId = 'entityId';
      directive.queueConfig.entityId = entityId;
      // Act
      directive.ngOnInit();
      addItemSubject.next({ _id: 'id' });
      // Assert
      expect(spyFormsHelperService.parseTemplate).toHaveBeenCalledWith(entityId, directive.rootModel, '');
    });

    it('should not call FormsHelperService.parseTemplate if queueConfig.entityId is not set', () => {
      // Arrange
      const entityId = 'entityId';
      // Act
      directive.ngOnInit();
      addItemSubject.next({ _id: 'id' });
      // Assert
      expect(spyFormsHelperService.parseTemplate).not.toHaveBeenCalledWith(entityId, directive.rootModel, '');
    });

    it('should access context.entityId if queueConfig.entityId is not set', () => {
      // Arrange
      const context = jasmine.createSpyObj('context', [], { entityId: 'entityId' });
      const spyGetEntityId = Object.getOwnPropertyDescriptor(context, 'entityId')?.get as jasmine.Spy;
      directive.context = context;
      // Act
      directive.ngOnInit();
      addItemSubject.next({ _id: 'id' });
      // Assert
      expect(spyGetEntityId).toHaveBeenCalled();
    });

    it('should call FormsHelperService.parseTemplate if queueConfig.entityTypeId is set', () => {
      // Arrange
      const entityTypeId = 'entityTypeId';
      directive.queueConfig.entityTypeId = entityTypeId;
      // Act
      directive.ngOnInit();
      addItemSubject.next({ _id: 'id' });
      // Assert
      expect(spyFormsHelperService.parseTemplate).toHaveBeenCalledWith(entityTypeId, directive.rootModel, '');
    });

    it('should not call FormsHelperService.parseTemplate if queueConfig.entityTypeId is not set', () => {
      // Arrange
      const entityTypeId = 'entityTypeId';
      // Act
      directive.ngOnInit();
      addItemSubject.next({ _id: 'id' });
      // Assert
      expect(spyFormsHelperService.parseTemplate).not.toHaveBeenCalledWith(entityTypeId, directive.rootModel, '');
    });

    it('should access context.entityTypeId if queueConfig.entityTypeId is not set', () => {
      // Arrange
      const context = jasmine.createSpyObj('context', [], { entityTypeId: 'entityTypeId' });
      const spyGetEntityTypeId = Object.getOwnPropertyDescriptor(context, 'entityTypeId')?.get as jasmine.Spy;
      directive.context = context;
      // Act
      directive.ngOnInit();
      addItemSubject.next({ _id: 'id' });
      // Assert
      expect(spyGetEntityTypeId).toHaveBeenCalled();
    });

    it('should call FormsHelperService.parseTemplate if queueConfig.description is set', () => {
      // Arrange
      const description = 'description';
      directive.queueConfig.description = description;
      // Act
      directive.ngOnInit();
      addItemSubject.next({ _id: 'id' });
      // Assert
      expect(spyFormsHelperService.parseTemplate).toHaveBeenCalledWith(description, directive.rootModel, '');
    });

    it('should not call FormsHelperService.parseTemplate if queueConfig.description is not set', () => {
      // Arrange
      const description = 'description';
      // Act
      directive.ngOnInit();
      addItemSubject.next({ _id: 'id' });
      // Assert
      expect(spyFormsHelperService.parseTemplate).not.toHaveBeenCalledWith(description, directive.rootModel, '');
    });

    it('should access context.entityDescription if queueConfig.description is not set', () => {
      // Arrange
      const context = jasmine.createSpyObj('context', [], { entityDescription: 'entityDescription' });
      const spyGetEntityDescription = Object.getOwnPropertyDescriptor(context, 'entityDescription')?.get as jasmine.Spy;
      directive.context = context;
      // Act
      directive.ngOnInit();
      addItemSubject.next({ _id: 'id' });
      // Assert
      expect(spyGetEntityDescription).toHaveBeenCalled();
    });

    it('should call FormsHelperService.parseTemplate if queueConfig.editUrl is set', () => {
      // Arrange
      const editUrl = true;
      directive.queueConfig.editUrl = editUrl;
      // Act
      directive.ngOnInit();
      addItemSubject.next({ _id: 'id' });
      // Assert
      expect(spyFormsHelperService.parseTemplate).toHaveBeenCalledWith(editUrl, directive.rootModel, '');
    });

    it('should not call FormsHelperService.parseTemplate if queueConfig.editUrl is not set', () => {
      // Arrange
      const editUrl = true;
      // Act
      directive.ngOnInit();
      addItemSubject.next({ _id: 'id' });
      // Assert
      expect(spyFormsHelperService.parseTemplate).not.toHaveBeenCalledWith(editUrl, directive.rootModel, '');
    });

    it('should add the item to the queue table', () => {
      // Arrange
      const item = { _id: 'id' };
      directive.context = {
        entityId: 'entityId',
        entityTypeId: 'entityTypeId',
        entityDescription: 'entityDescription',
      };
      directive.rootModel = { tempId: 'tempId' };
      // Act
      directive.ngOnInit();
      addItemSubject.next(item);
      // Assert
      expect(spyTable.add).toHaveBeenCalledWith({
        id: item._id,
        item,
        editUrl: '',
        entityId: 'entityId',
        entityTypeId: 'entityTypeId',
        description: 'entityDescription',
        status: 'Queued',
        parentEntityId: 'tempId',
      });
    });

    it('should not add the item to the queue table if it has no _id property', () => {
      // Arrange
      const item = {};
      directive.context = {
        entityId: 'entityId',
        entityTypeId: 'entityTypeId',
        entityDescription: 'entityDescription',
      };
      directive.rootModel = { tempId: 'tempId' };
      // Act
      directive.ngOnInit();
      addItemSubject.next(item);
      // Assert
      expect(spyTable.add).not.toHaveBeenCalled();
    });
  });

  describe('deleteQueueItem', () => {
    it('should delete the item from the queue table', () => {
      // Arrange
      const item = { _id: 'id' };
      // Act
      directive.ngOnInit();
      deleteItemSubject.next(item);
      // Assert
      expect(spyTable.delete).toHaveBeenCalledWith(item._id);
    });

    it('should delete an additional item from the queue table if queueConfig.itemIdExpression is set', () => {
      // Arrange
      const item = { _id: 'id', itemId: 'itemId' };
      const itemIdExpression = 'model.itemId';
      directive.queueConfig.itemIdExpression = itemIdExpression;
      // Act
      directive.ngOnInit();
      deleteItemSubject.next(item);
      // Assert
      expect(spyTable.delete).toHaveBeenCalledWith('itemId');
    });

    it('should not delete the item from the queue table if it has no _id property', () => {
      // Arrange
      const item = {};
      // Act
      directive.ngOnInit();
      deleteItemSubject.next(item);
      // Assert
      expect(spyTable.delete).not.toHaveBeenCalled();
    });
  });
});
