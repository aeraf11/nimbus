import { deepClone } from 'deep.clone';
import { UntypedFormGroup } from '@angular/forms';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { Input, ChangeDetectorRef } from '@angular/core';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Person } from '../../models/person';
import { ProfileSize } from '@nimbus/core/src/lib/core';
import { OnChange } from 'property-watch-decorator';

@Component({
  selector: 'ni-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersonComponent {
  @OnChange<FormlyFieldConfig[]>(function (this: PersonComponent, value) {
    deepClone(value).then((fieldConfig) => {
      this.fieldConfig = fieldConfig;
      this.changes.detectChanges();
    });
  })
  @Input()
  fields?: FormlyFieldConfig[];

  @Input() person?: Person;
  @Input() imageSize: ProfileSize = 'medium';
  @Input() photoProp = '';
  @Input() initialsProp = '';

  form: UntypedFormGroup = new UntypedFormGroup({});
  fieldConfig?: FormlyFieldConfig[];

  constructor(private changes: ChangeDetectorRef) {}

  getValue(key: string) {
    if (this.person) {
      return this.person[key as keyof Person];
    } else {
      return '';
    }
  }
}
