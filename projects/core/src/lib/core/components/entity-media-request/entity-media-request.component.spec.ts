import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntityMediaRequestComponent } from './entity-media-request.component';

describe('EntityMediaRequestComponent', () => {
  let component: EntityMediaRequestComponent;
  let fixture: ComponentFixture<EntityMediaRequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EntityMediaRequestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EntityMediaRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
