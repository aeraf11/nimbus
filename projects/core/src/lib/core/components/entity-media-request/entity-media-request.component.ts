import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'ni-entity-media-request',
  templateUrl: './entity-media-request.component.html',
  styleUrls: ['./entity-media-request.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntityMediaRequestComponent {
  @Input() actionLabel!: string;
  @Input() selectedItems!: any[];
  @Input() itemCount!: number;
  @Output() buttonClicked = new EventEmitter<any>();

  constructor(private changeDetector: ChangeDetectorRef) {}

  setItemCount(itemCount: number): void {
    this.itemCount = itemCount;
    this.changeDetector.markForCheck();
  }

  onActionClick() {
    this.buttonClicked.emit(this.selectedItems);
  }
}
