import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Output,
  TrackByFunction,
} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  DialogTreeAction,
  DialogTreeData,
  DialogTreeResult,
  Lookup,
  LookupFlatNode,
  LookupNode,
  MenuItem,
} from '../../models';

@UntilDestroy()
@Component({
  selector: 'ni-dialog-tree',
  templateUrl: './dialog-tree.component.html',
  styleUrls: ['./dialog-tree.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogTreeComponent {
  @Output() itemAdded = new EventEmitter<Lookup>();

  /** Map from flat node to nested node. This helps us finding the nested node to be modified */
  flatNodeMap = new Map<LookupFlatNode, LookupNode>();

  /** Map from nested node to flattened node. This helps us to keep the same object for selection */
  nestedNodeMap = new Map<LookupNode, LookupFlatNode>();

  /** A selected parent node to be inserted */
  selectedParent: LookupFlatNode | null = null;

  /** The new item's name */
  newItemName = '';

  treeControl: FlatTreeControl<LookupFlatNode>;
  treeFlattener: MatTreeFlattener<LookupNode, LookupFlatNode>;
  dataSource: MatTreeFlatDataSource<LookupNode, LookupFlatNode>;
  items!: LookupNode[];
  private iniitialised = false;
  private pendingExpandId: string | null = null;

  DialogTreeAction = DialogTreeAction;

  testy?: MenuItem;

  /** The selection for checklist */
  checklistSelection: SelectionModel<LookupFlatNode> = new SelectionModel<LookupFlatNode>(true /* multiple */);

  constructor(
    public dialogRef: MatDialogRef<DialogTreeComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogTreeData,
    private change: ChangeDetectorRef
  ) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel, this.isExpandable, this.getChildren);
    this.treeControl = new FlatTreeControl<LookupFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
    const iniitialSelectedIds = data.selectedItems.map((item) => item.lookupId);

    if (data.items) {
      data.items.pipe(untilDestroyed(this)).subscribe((items) => {
        // This is not happy with changes in the observable yielding new object references, must clear down and rebuild.
        // trackby does not work properly. Only really an issue if addCustom is enabled on large trees.
        this.nestedNodeMap.clear();
        this.flatNodeMap.clear();
        this.dataSource.data = items || [];
        const currentSelectedIds = this.checklistSelection.selected.map((lookup) => lookup.item.lookupId);
        const selectedIds = this.iniitialised ? currentSelectedIds : iniitialSelectedIds;

        const nodes: LookupFlatNode[] = [];
        this.flatNodeMap.forEach((value, key) => {
          if (selectedIds.includes(key.item.lookupId)) {
            nodes.push(key);
          }
          if (key.item.lookupId === this.pendingExpandId) {
            this.expandBranch(key);
          }
        });
        this.checklistSelection = new SelectionModel<LookupFlatNode>(true, nodes);
        change.markForCheck();
        this.iniitialised = true;
      });
    }
  }

  expandBranch(node: LookupFlatNode) {
    this.treeControl.expand(node);
    const parent = this.getParentNode(node);
    if (parent) this.expandBranch(parent);
  }

  getLevel = (node: LookupFlatNode) => node.level;

  isExpandable = (node: LookupFlatNode) => node.expandable;

  getChildren = (node: LookupNode): LookupNode[] => node.children || [];

  hasChild = (_: number, _nodeData: LookupFlatNode) => _nodeData.expandable;

  hasNoContent = (_: number, _nodeData: LookupFlatNode) => !_nodeData.item.lookupId;

  /**
   * Transformer to convert nested node to flat node. Record the nodes in maps for later use.
   */
  transformer = (node: LookupNode, level: number) => {
    const existingNode = this.nestedNodeMap.get(node);
    const flatNode = existingNode && existingNode.item === node.item ? existingNode : new LookupFlatNode();
    flatNode.item = node.item;
    flatNode.level = level;
    flatNode.expandable = !!node.children?.length;
    this.flatNodeMap.set(flatNode, node);
    this.nestedNodeMap.set(node, flatNode);
    return flatNode;
  };

  /** Whether all the descendants of the node are selected. */
  descendantsAllSelected(node: LookupFlatNode): boolean {
    if (!this.treeControl.dataNodes) return false;
    this.treeControl.options;
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected =
      descendants.length > 0 &&
      descendants.every((child) => {
        return this.checklistSelection.isSelected(child);
      });
    return descAllSelected;
  }

  /** Whether part of the descendants are selected */
  descendantsPartiallySelected(node: LookupFlatNode): boolean {
    if (!this.treeControl.dataNodes) return false;
    const descendants = this.treeControl.getDescendants(node);
    const result = descendants.some((child) => this.checklistSelection.isSelected(child));
    return result && !this.descendantsAllSelected(node);
  }

  /** Toggle the to-do item selection. Select/deselect all the descendants node */
  itemSelectionToggle(node: LookupFlatNode): void {
    this.checklistSelection.toggle(node);
    const descendants = this.treeControl.getDescendants(node);
    this.checklistSelection.isSelected(node)
      ? this.checklistSelection.select(...descendants)
      : this.checklistSelection.deselect(...descendants);

    // Force update for the parent
    descendants.forEach((child) => this.checklistSelection.isSelected(child));
    this.checkAllParentsSelection(node);
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  leafItemSelectionToggle(node: LookupFlatNode): void {
    this.checklistSelection.toggle(node);
    this.checkAllParentsSelection(node);
  }

  /* Checks all the parents when a leaf node is selected/unselected */
  checkAllParentsSelection(node: LookupFlatNode): void {
    let parent: LookupFlatNode | null = this.getParentNode(node);
    while (parent) {
      this.checkRootNodeSelection(parent);
      parent = this.getParentNode(parent);
    }
  }

  /** Check root node checked state and change it accordingly */
  checkRootNodeSelection(node: LookupFlatNode): void {
    const nodeSelected = this.checklistSelection.isSelected(node);
    const descendants = this.treeControl.getDescendants(node);
    const descAllSelected =
      descendants.length > 0 &&
      descendants.every((child) => {
        return this.checklistSelection.isSelected(child);
      });
    if (nodeSelected && !descAllSelected) {
      this.checklistSelection.deselect(node);
    } else if (!nodeSelected && descAllSelected) {
      this.checklistSelection.select(node);
    }
  }

  /* Get the parent node of a node */
  getParentNode(node: LookupFlatNode): LookupFlatNode | null {
    const currentLevel = this.getLevel(node);

    if (currentLevel < 1) {
      return null;
    }

    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;

    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];

      if (this.getLevel(currentNode) < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

  /** Select the category so we can insert the new item. */
  addNewItem(node: LookupFlatNode) {
    const parentNode = this.flatNodeMap.get(node);
    const item = {
      displayValue: '',
      hierarchicalDisplayValue: '',
      lookupId: '',
      lookupKey: '',
      parentLookupId: parentNode?.item.lookupId,
      isNew: true,
      level: 0,
    };
    if (parentNode) {
      parentNode.children = [...(parentNode?.children || []), { item }];
    }
    this.treeControl.expand(node);
    this.dataSource.data = this.dataSource.data;
    this.change.markForCheck();
  }

  /** Save the node to database */
  saveNode(node: LookupFlatNode, itemValue: string) {
    const nestedNode = this.flatNodeMap.get(node);
    if (nestedNode) {
      const parentNode = this.getParentNode(node);
      nestedNode.item = {
        lookupId: itemValue,
        lookupKey: '',
        displayValue: itemValue,
        hierarchicalDisplayValue: `${parentNode?.item.hierarchicalDisplayValue} > ${itemValue}`,
        parentLookupId: parentNode?.item.lookupId,
        isNew: true,
        level: 0,
      };
      this.pendingExpandId = parentNode?.item.lookupId || null;
      this.itemAdded.emit(nestedNode.item);
      this.change.markForCheck();
    }
  }

  onClose(action: DialogTreeAction): void {
    const result: DialogTreeResult = { action, items: this.checklistSelection.selected.map((lookup) => lookup.item) };
    this.dialogRef.close(result);
  }

  trackBy: TrackByFunction<LookupFlatNode> = (index, node) => node.item.lookupId;
}
