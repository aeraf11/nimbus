import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogModule } from '@angular/material/dialog';
import { MatTreeModule } from '@angular/material/tree';
import { DialogTreeComponent } from './dialog-tree.component';

describe('DialogTreeComponent', () => {
  let component: DialogTreeComponent;
  let fixture: ComponentFixture<DialogTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DialogTreeComponent],
      imports: [MatTreeModule, MatDialogModule],
      providers: [
        { provide: MatDialogRef, useFactory: () => jasmine.createSpyObj('MatDialogRef', ['close', 'afterClosed']) },
        { provide: MAT_DIALOG_DATA, useValue: { selectedItems: [] } },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
