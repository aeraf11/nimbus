import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  forwardRef,
  HostBinding,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatFormFieldAppearance } from '@angular/material/form-field';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  DataSourceService,
  FilterColumn,
  FormsHelperService,
  IndexedDbService,
  ListService,
  MetaDataItem,
  MetaDataService,
  StateService,
  utils,
} from '@nimbus/core/src/lib/core';
import deepClone from 'deep.clone';
import { from, fromEvent, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, share, startWith, switchMap, tap } from 'rxjs/operators';

interface ModelSyncJsonProperty {
  name: string;
  items?: {
    key: string;
    identifier: { property: string; value: string[] };
    addToRootModel?: boolean;
  }[];
  returnAll?: { key?: string; addToRootModel?: boolean };
  returnMode: 'single' | 'multiple';
  singleReturn?: 'object' | 'property';
  childName?: string;
  isMetaData?: boolean;
  convertToCamelCase?: boolean;
  innerMetaDataPropName?: string;
}

@UntilDestroy()
@Component({
  selector: 'ni-type-ahead',
  templateUrl: './type-ahead.component.html',
  styleUrls: ['./type-ahead.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TypeAheadComponent),
      multi: true,
    },
  ],
})
export class TypeAheadComponent implements ControlValueAccessor, AfterViewInit, OnChanges {
  items$!: Observable<{ id: string; displayName: string }[]>;
  displayName = new UntypedFormControl();
  selectedValue: { id: string; displayName: string } | null = null;
  loadingMatches = false;

  @Input() id = '';
  @Input() readonly: boolean | undefined = false;
  @Input() required = false;
  @Input() placeholder = '';
  @Input() tabindex: number | undefined;
  @Input() appearance: MatFormFieldAppearance = 'outline';
  @Input() listName!: string;
  @Input() valueProp!: string;
  @Input() labelProp!: string;
  @Input() labelTemplate!: string;
  @Input() selectColumns!: string[];
  @Input() sortColumn!: string;
  @Input() dataSource!: string;
  @Input() dataSourceParams!: any;
  @Input() dataSourceSearchParam!: string;
  @Input() mustMatch = false;
  @Input() noMatchText = '';
  @Input() filterColumns: Array<FilterColumn> = [];
  @Input() searchEmpty = true;
  @Input() version = 1;
  @Input() enableModelSync = false;
  @Input() modelSyncExclusions: string[] = [];
  @Input() modelSyncMetaDataMap?: { propName?: string; items: { key: string; label: string }[] } = undefined;
  @Input() modelSyncJsonProperties: ModelSyncJsonProperty[] = [];
  @Input() targetModel?: any;
  @Input() rootModel?: any;
  @Input() form?: UntypedFormGroup;
  @Input() autoFillProp?: string;
  @Input() autoFillModelSkipProps?: string[] = [];
  @Input() autoFillModelOverrides?: any[] = [];
  @Input() autoSyncRouteId?: string;
  @Input() offlineConfig?: { table: string; type: string };
  @Input() ariaLabel = '';
  @Input() defaultLabel = '';
  @Input() defaultInitiallySelected = false;
  @ViewChild('input') textInput!: ElementRef<HTMLInputElement>;
  @HostBinding('class.type-ahead') hostClass = true;

  defaultOption: any;

  private onChange!: (value: any) => void;
  private onTouched!: () => void;
  private matches: { id: string; displayName: string }[] = [];
  private route: any;

  constructor(
    private typeAheadService: ListService,
    private dataSourceService: DataSourceService,
    private formsHelper: FormsHelperService,
    private change: ChangeDetectorRef,
    private state: StateService,
    private renderer: Renderer2,
    private dbService: IndexedDbService,
    private metaDataService: MetaDataService
  ) {}

  ngAfterViewInit(): void {
    this.getData();

    this.state
      .selectCurrentRoute()
      .pipe(untilDestroyed(this))
      .subscribe((route) => (this.route = route));

    this.setDefaultOption();
    if (this.defaultInitiallySelected && this.defaultOption) {
      this.onChange(this.defaultOption);
      this.setValue(this.defaultOption);
    }
  }

  ngOnChanges(simpleChanges: SimpleChanges): void {
    if (simpleChanges.filterColumns) {
      this.getData();
    }
  }

  getData(): void {
    if (this.textInput) {
      const { listName, valueProp, labelProp, dataSource, dataSourceParams, dataSourceSearchParam } = this;

      const searchText$: Observable<string> = fromEvent(this.textInput.nativeElement, 'keyup').pipe(
        map((event) => (<HTMLInputElement>event.target).value),
        startWith(''),
        filter((search) => this.searchEmpty || !!search),
        debounceTime(200),
        distinctUntilChanged()
      );

      this.items$ = searchText$.pipe(
        tap(() => {
          this.loadingMatches = true;
          this.change.detectChanges();
        }),
        switchMap((search) => {
          if (this.offlineConfig) {
            return from(
              this.dbService.table(this.offlineConfig.table).where('type').equals(this.offlineConfig.type).toArray()
            );
          } else {
            if (listName) {
              const list = this.typeAheadService.getTypeAheadList(
                listName,
                search,
                this.sortColumn || labelProp,
                this.selectColumns || [valueProp, labelProp],
                this.filterColumns
              );
              // For version 1 back compatibility we need to map the object back to the original id/displayName structure.
              if (this.version === 1) {
                return list.pipe(
                  map((results) => {
                    return results.map((result) => {
                      return { displayName: result[labelProp || 'label'], id: result[valueProp || 'value'] };
                    });
                  })
                );
              }
              return list;
            } else {
              return this.dataSourceService.getData<{ id: string; displayName: string }>(dataSource, {
                ...dataSourceParams,
                [dataSourceSearchParam]: search,
              });
            }
          }
        }),
        share(),
        tap((matches) => {
          this.matches = matches;
          this.loadingMatches = false;
          this.change.detectChanges();
        })
      );
    }
  }

  writeValue(value: any): void {
    this.setValue(value);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.renderer.setProperty(this.textInput.nativeElement, 'disabled', isDisabled);
  }

  setValue(value: any) {
    if (value) {
      if (this.enableModelSync) {
        this.syncModel(value);
      }

      // If the skipped props include the autoFillProp typeahead we need to prevent
      // it executing in infinite loop by comparing stringified value.
      if ((this.route || this.autoSyncRouteId) && !this.isValueSelectedValue(value)) {
        this.autoFillPropertiesOnModel(value);
      }
    }

    this.displayName.setValue(value ? this.getLabel(value) : null);
    this.selectedValue = value;
    this.change.detectChanges();
  }

  onItemSelected(event: MatAutocompleteSelectedEvent): void {
    const value = event.option.value;
    if (value.displayName) {
      value.displayName = this.removeEscapeSequences(value.displayName);
    }

    this.onChange(value);
    this.setValue(value);
  }

  getLabel(item: any): string {
    if (this.version === 1) {
      return item.displayName;
    }
    return this.labelTemplate
      ? this.formsHelper.simpleTemplate(this.labelTemplate, item, '', true)
      : item[this.labelProp || 'label'];
  }

  onBlur(event: any) {
    // Don't run on selecting option
    if (event.relatedTarget?.tagName === 'MAT-OPTION') {
      return;
    }

    this.onTouched();

    const selectedLabel = this.textInput.nativeElement.value;

    if (selectedLabel) {
      // Don't run when default is selected
      if (selectedLabel === this.defaultLabel) {
        return;
      }

      let selectedValue: any = this.matches.find(
        (item) => this.getLabel(item).toLowerCase().trim() === selectedLabel.toLowerCase().trim()
      );

      // Note: must match should always be used where values are objects
      if (this.mustMatch) {
        if (!selectedValue) {
          this.clearSelection();
        }
        // If allowing non-matches, we are dealing with string based values (maybe lookups), but new items can only be strings
      } else {
        if (!selectedValue) {
          // For version 1 back compatibility we need to map the object back to the original id/displayName structure.
          if (this.version === 1) {
            selectedValue = { id: selectedLabel, displayName: selectedLabel };
          } else {
            // use new object with labelProp as the poperty name to maintain object shape between new/existing
            selectedValue = { [this.labelProp || 'label']: selectedLabel };
          }
        }
      }
      if (selectedValue) {
        this.onChange(selectedValue);
        this.displayName.setValue(this.getLabel(selectedValue));
        this.selectedValue = selectedValue;
      }
    } else {
      this.clearSelection();
    }
  }

  clearSelection() {
    this.onChange(null);
    this.displayName.setValue('');
    this.selectedValue = null;
  }

  private removeEscapeSequences(input: string): string {
    // \n replace newline
    // \r replace carriage return
    return input?.replace(/\r?\n/g, ' ') ?? '';
  }

  private syncModel(value: any): void {
    if (!this.targetModel) this.targetModel = {};
    const model: any = {};
    const metaDataProp = this.modelSyncMetaDataMap ? this.modelSyncMetaDataMap.propName ?? 'metaData' : '';
    const jsonPropNames = this.modelSyncJsonProperties.map((item) => item.name);

    for (const prop in value) {
      // Exclude properties that exist in modelSyncExclusions and the metaData property
      if (!this.modelSyncExclusions?.includes(prop) && prop !== metaDataProp) {
        this.targetModel[prop] = model[prop] = value[prop];
      }

      // Preferred way of parsing any JSON object/arrays (including meta data)
      if (jsonPropNames.includes(prop)) {
        const jsonProp = this.modelSyncJsonProperties.find((j) => j.name === prop);
        if (jsonProp && typeof value[prop] === 'string') {
          this.syncParsedJsonOntoModel(jsonProp, prop, value, model);
        }
      }

      // The deprecated old way of parsing metadata
      if (prop === metaDataProp) {
        this.syncMetaDataOntoModel(prop, value, model);
      }
    }

    // Finally, patch the model with the sync'd properties
    this.form?.patchValue(model);
  }

  private autoFillPropertiesOnModel(value: any): void {
    if (this.autoFillProp) {
      const props = { [this.autoFillProp]: value[this.autoFillProp] };
      deepClone(this.rootModel).then((clonemodel) => {
        this.state.mergeDynamicRouteModel(
          clonemodel,
          this.autoSyncRouteId || this.route.data.id,
          props,
          this.autoFillModelOverrides,
          this.autoFillModelSkipProps
        );
      });
    }
  }

  private isValueSelectedValue(value: any): boolean {
    if (!value) {
      return false;
    }

    return JSON.stringify(value) === JSON.stringify(this.selectedValue);
  }

  private syncParsedJsonOntoModel(jsonProp: ModelSyncJsonProperty, prop: string, value: any, model: any): void {
    if (this.handleSyncParsedJsonOntoModelErrors(jsonProp)) {
      return;
    }

    const parsedValue = JSON.parse(value[prop] ?? '');
    const rootProps: any[] = [];

    if (parsedValue) {
      // We provide a childName prop as MetaData usually comes in an Items array but other JSON could come in
      // child properties too so this is handled generically.
      // If the parsed json is an object, we place that in an array so we can filter it later.
      const parsedData = (jsonProp.childName ? parsedValue[jsonProp.childName] : parsedValue) ?? [];
      let filterableData = Array.isArray(parsedData) ? parsedData : [parsedData];

      // Add an isImport: true to each object, this is because we may want to tailor our view/edit
      // forms to show/hide certain things based on whether the item is imported or not
      filterableData = filterableData.map((obj) => ({ ...obj, isImported: true }));

      // If we want camelCase, then we can set it here as Phaneros Model generally uses camelCase
      if (jsonProp.convertToCamelCase) {
        filterableData = filterableData.map((obj) => utils.toCamel(obj));
      }

      // Handle meta data on any objects in the array
      filterableData = filterableData.map((value) => {
        const metaProperty =
          value[jsonProp.innerMetaDataPropName ?? jsonProp.convertToCamelCase ? 'metaData' : 'MetaData'];
        if (metaProperty) {
          const parsedMetaData = utils.parseMetaJson(metaProperty);
          return { ...value, ...this.metaDataService.formatMetaDataForModel(parsedMetaData) };
        }
        return value;
      });

      // For if we dont want to do any filtering and just want to place the whole parsed object on the model
      if (jsonProp.returnAll) {
        const config = jsonProp.returnAll;
        const key = config.key || prop;

        if (config.addToRootModel) {
          rootProps.push({ [key]: filterableData });
        } else {
          this.targetModel[key] = model[key] = filterableData;
        }
      }

      jsonProp.items?.forEach((mapping) => {
        // Firstly, we filter out the data
        const valueMatches = filterableData.filter((match) => {
          return mapping.identifier.value.includes(match[mapping.identifier.property]);
        });

        // Based on returnMode we either return all the matches or the first
        const modelValue = jsonProp.returnMode === 'single' ? valueMatches[0] ?? null : valueMatches;

        if (!Array.isArray(modelValue)) {
          const returnType = jsonProp.singleReturn ?? 'property';
          const formattedModelValue = jsonProp.isMetaData
            ? this.metaDataService.readValue(modelValue as MetaDataItem)
            : this.itemPropOrItem(modelValue, returnType, mapping.identifier.property);

          if (mapping.addToRootModel) {
            rootProps.push({ [mapping.key]: modelValue });
          } else {
            this.targetModel[mapping.key] = model[mapping.key] = formattedModelValue;
          }
        }

        if (Array.isArray(modelValue)) {
          if (mapping.addToRootModel) {
            rootProps.push({ [mapping.key]: modelValue });
          } else {
            this.targetModel[mapping.key] = model[mapping.key] = modelValue;
          }
        }
      });

      // Now if we have a bunch of props in rootProps array we place that on the rootModel
      let shouldUpdateRootModel = false;
      if (rootProps.length > 0) {
        deepClone(this.rootModel).then((rootClone) => {
          rootProps.forEach((prop) => {
            const key = Object.keys(prop)?.[0];
            const value = prop?.[key];
            const resolved = utils.resolveObjectPath(key, rootClone);

            // If we dont check that the rootModel has actually been changed we get into an infinite loop
            if (JSON.stringify(resolved) !== JSON.stringify(value)) {
              shouldUpdateRootModel = true;
            }

            if (key && value) {
              if (key.includes('.')) {
                try {
                  utils.setDeepPropertyByPath(key, rootClone, value);
                } catch {
                  console.error(
                    `The path "${key}" did not resolve in the model. Make sure you have the properties you expect setup in your config. \n` +
                      `As a result, the value was not placed on the model`
                  );
                  shouldUpdateRootModel = false;
                }
              } else {
                rootClone[key] = value;
              }
            }
          });

          if (shouldUpdateRootModel) {
            // Update the root model properly but only if the value for that key is different than what it was
            this.state.updateDynamicRouteModel(rootClone);
          }
        });
      }
    }
  }

  private handleSyncParsedJsonOntoModelErrors(jsonProp: ModelSyncJsonProperty): boolean {
    let errors = false;
    const itemKeys = jsonProp.items?.map((item) => item.key);
    const duplicateKeys = itemKeys?.filter((key, index) => itemKeys.indexOf(key) !== index) ?? [];

    if (duplicateKeys.length > 0) {
      errors = true;
      console.error('You have duplicate keys in your items array. Values have not been added to the model.');
    }

    return errors;
  }

  private itemPropOrItem(item: any, type: string, prop?: string): any {
    if (type === 'property' && prop) {
      return item?.[prop] ?? null;
    }
    return item;
  }

  // Deprecated in favour of syncParsedJsonOntoModel()
  private syncMetaDataOntoModel(prop: string, value: any, model: any): void {
    console.warn('Use modelSyncJsonProperties over modelSyncMetaDataMap');
    const metaData: MetaDataItem[] = typeof value[prop] === 'string' ? this.getMetadataItems(value[prop]) : value[prop];
    if (metaData?.length > 0) {
      this.modelSyncMetaDataMap?.items.forEach((mapping) => {
        const item = metaData.find((mdi) => mdi.Label === mapping.label);
        if (item) {
          this.targetModel[mapping.key] = model[mapping.key] = this.metaDataService.readValue(item);
        }
      });
    }
  }

  private setDefaultOption(): void {
    if (this.defaultLabel) {
      if (this.version === 1) {
        this.defaultOption = {
          displayName: this.defaultLabel,
          value: '',
        };
      }
      if (this.version === 2) {
        this.defaultOption = {
          [this.labelProp]: this.defaultLabel,
          value: '',
        };
      }
      this.change.markForCheck();
    }
  }

  private getMetadataItems(metadata: string): MetaDataItem[] {
    if (!metadata) {
      return [];
    }

    const parsed = JSON.parse(metadata);
    return parsed.Items ? parsed.Items : parsed;
  }
}
