import { FormsHelperService, ListService, MetaDataService } from '@nimbus/core/src/lib/core';
import { MockMetaDataService } from '../../services/meta-data.service.spec';
/* eslint-disable @typescript-eslint/no-explicit-any */
import { ComponentFixture, discardPeriodicTasks, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { StateService } from '@nimbus/core/src/lib/core';
import { of } from 'rxjs';
import { MockListService } from '../../services/list.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { MetaDataItem } from '../../models/meta-data-item';
import { DataSourceService } from '../../services/data-source.service';
import { MockDataSourceService } from '../../services/data-source.service.spec';
import { TypeAheadComponent } from './type-ahead.component';

describe('TypeAheadComponent', () => {
  let component: TypeAheadComponent;
  let fixture: ComponentFixture<TypeAheadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TypeAheadComponent],
      imports: [MatAutocompleteModule, MatFormFieldModule, MatInputModule, NoopAnimationsModule, ReactiveFormsModule],
      providers: [
        { provide: StateService, useClass: MockStateService },
        { provide: ListService, useClass: MockListService },
        { provide: DataSourceService, useClass: MockDataSourceService },
        { provide: MetaDataService, useClass: MockMetaDataService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeAheadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  beforeEach(() => {
    const anyOnChange = () => {
      return '';
    };
    const anyOnTouched = () => {
      return '';
    };

    component.registerOnChange(anyOnChange);
    component.registerOnTouched(anyOnTouched);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngAfterViewInit()', () => {
    describe('autoFillProp', () => {
      let state: StateService;

      beforeEach(() => (state = TestBed.inject(StateService)));

      it('calls state.selectCurrentRoute and sets the route variable', () => {
        // Arrange
        const someCurrentRoute = {
          data: {
            id: 'Current.Route.Id',
          },
        };

        spyOn(state, 'selectCurrentRoute').and.callFake(() => of(someCurrentRoute));

        // Act
        component.ngAfterViewInit();

        // Assert
        expect(state.selectCurrentRoute).toHaveBeenCalled();
        expect((component as any).route).toEqual(someCurrentRoute);
      });
    });
  });

  describe('setValue()', () => {
    it('sets displayName null if value is falsy', () => {
      // Arrange
      spyOn(component.displayName, 'setValue').and.callFake(() => null);

      // Act
      component.setValue(null);

      // Assert
      expect(component.displayName.setValue).toHaveBeenCalledWith(null);
    });

    it('calls getLabel with value and sets displayName value to that', () => {
      // Arrange
      const someLabel = 'someLabel';

      spyOn(component, 'getLabel').and.callFake(() => someLabel);
      spyOn(component.displayName, 'setValue').and.callFake(() => null);

      // Act
      component.setValue({});

      //Assert
      expect(component.getLabel).toHaveBeenCalled();
      expect(component.displayName.setValue).toHaveBeenCalledWith(someLabel);
    });

    describe('model sync', () => {
      beforeEach(() => {
        component.form = new FormGroup({});
        component.targetModel = {};
      });

      it('does not patch form value or set targetModel if enableModelSync is false', () => {
        // Arrange
        spyOn<any>(component.form, 'patchValue').and.callFake(() => null);

        const someTargetModel = { one: 'one' };

        component.enableModelSync = false;
        component.targetModel = someTargetModel;

        // Act
        component.setValue({});

        // Assert
        expect(component.form?.patchValue).not.toHaveBeenCalled();
        expect(component.targetModel).toEqual(someTargetModel);
      });

      it('patches properties on value onto the forms value and the targetModel variable', () => {
        // Arrange
        spyOn<any>(component.form, 'patchValue').and.callFake(() => null);

        component.enableModelSync = true;

        const someValue = {
          one: 'one',
          two: 'two',
          three: 'three',
        };

        // Act
        component.setValue(someValue);

        // Assert
        expect(component.form?.patchValue).toHaveBeenCalledWith(someValue);
        expect(component.targetModel).toEqual(someValue);
      });

      it('does not patch form value or set properties on targetModel that exists in modelSyncExlusions', () => {
        // Arrange
        spyOn<any>(component.form, 'patchValue').and.callFake(() => null);

        const someValueBefore = {
          one: 'one',
          two: 'two',
          three: 'three',
        };

        const someValueAfter = {
          three: 'three',
        };

        const someExclusions = ['one', 'two'];

        component.enableModelSync = true;
        component.modelSyncExclusions = someExclusions;

        // Act
        component.setValue(someValueBefore);

        // Assert
        expect(component.form?.patchValue).toHaveBeenCalledWith(someValueAfter);
        expect(component.targetModel).toEqual(someValueAfter);
      });

      describe('sync parsed json (including meta data)', () => {
        beforeEach(() => (component.enableModelSync = true));

        describe('returnMode is "single"', () => {
          it('returns null if json is empty array', () => {
            // Arrange
            component.modelSyncJsonProperties = [
              {
                name: 'emptyArray',
                items: [
                  {
                    key: 'something',
                    identifier: {
                      property: 'anything',
                      value: ['1'],
                    },
                  },
                ],
                returnMode: 'single',
              },
            ];

            // Act
            component.setValue({ emptyArray: JSON.stringify([]) });

            // Assert
            expect(component.targetModel.something).toEqual(null);
          });

          it('returns null if json is empty object', () => {
            // Arrange
            component.modelSyncJsonProperties = [
              {
                name: 'emptyArray',
                items: [
                  {
                    key: 'something',
                    identifier: {
                      property: 'anything',
                      value: ['1'],
                    },
                  },
                ],
                returnMode: 'single',
              },
            ];

            // Act
            component.setValue({ emptyArray: JSON.stringify({}) });

            // Assert
            expect(component.targetModel.something).toEqual(null);
          });

          it('returns a single property by default', () => {
            // Arrange
            const somePersonTypeId = '12345';
            component.modelSyncJsonProperties = [
              {
                name: 'testPeopleJson',
                items: [
                  {
                    key: 'id',
                    identifier: {
                      property: 'personTypeId',
                      value: [somePersonTypeId],
                    },
                  },
                ],
                returnMode: 'single',
              },
            ];

            const someValue = {
              forename: 'Tester',
              surname: 'Testington',
              personTypeId: somePersonTypeId,
            };

            // Act
            component.setValue({ testPeopleJson: JSON.stringify(someValue) });

            // Assert
            expect(component.targetModel.id).toEqual(somePersonTypeId);
          });

          it('returns a single object when singleReturn is set to "object"', () => {
            // Arrange
            component.modelSyncJsonProperties = [
              {
                name: 'testPeopleJson',
                items: [
                  {
                    key: 'people',
                    identifier: {
                      property: 'personTypeId',
                      value: ['12345'],
                    },
                  },
                ],
                returnMode: 'single',
                singleReturn: 'object',
              },
            ];

            const someValue = {
              forename: 'Tester',
              surname: 'Testington',
              personTypeId: '12345',
              isImported: true,
            };

            // Act
            component.setValue({ testPeopleJson: JSON.stringify(someValue) });

            // Assert
            expect(component.targetModel.people).toEqual(someValue);
          });

          it('returns the first object even if many exist on the array', () => {
            // Arrange
            component.modelSyncJsonProperties = [
              {
                name: 'testPeopleJson',
                items: [
                  {
                    key: 'people',
                    identifier: {
                      property: 'personTypeId',
                      value: ['12345'],
                    },
                  },
                ],
                returnMode: 'single',
                singleReturn: 'object',
              },
            ];

            const personOne = {
              forename: 'Tester',
              surname: 'Testington',
              personTypeId: '12345',
              isImported: true,
            };
            const personTwo = {
              forename: 'Wont',
              surname: 'Appear',
              personTypeId: '12345',
              isImported: true,
            };
            const someValue = [personOne, personTwo];

            // Act
            component.setValue({ testPeopleJson: JSON.stringify(someValue) });

            // Assert
            expect(component.targetModel.people).toEqual(personOne);
          });

          it('can parse child properties when "childName" is set', () => {
            // Arrange
            const somePersonTypeId = '12345';
            const someChildName = 'Items';
            component.modelSyncJsonProperties = [
              {
                name: 'testPeopleJson',
                items: [
                  {
                    key: 'id',
                    identifier: {
                      property: 'personTypeId',
                      value: [somePersonTypeId],
                    },
                  },
                ],
                childName: someChildName,
                returnMode: 'single',
              },
            ];

            const someValue: any = {};
            someValue[someChildName] = [
              {
                forename: 'Tester',
                surname: 'Testington',
                personTypeId: somePersonTypeId,
              },
            ];

            // Act
            component.setValue({ testPeopleJson: JSON.stringify(someValue) });

            // Assert
            expect(component.targetModel.id).toEqual(somePersonTypeId);
          });

          it('can parse meta data items', () => {
            // Arrange
            const someMetaDataValue = 'some meta data value';
            component.modelSyncJsonProperties = [
              {
                name: 'metaJson',
                items: [
                  {
                    key: 'dateOfBirth',
                    identifier: {
                      property: 'Label',
                      value: ['Date of birth'],
                    },
                  },
                ],
                childName: 'Items',
                returnMode: 'single',
                isMetaData: true,
              },
            ];

            const someValue: any = {
              Items: [
                {
                  Label: 'Date of birth',
                  Value: someMetaDataValue,
                },
              ] as MetaDataItem[],
            };

            // Act
            component.setValue({ metaJson: JSON.stringify(someValue) });

            // Assert
            expect(component.targetModel.dateOfBirth).toEqual(someMetaDataValue);
          });

          it('can add value to the rootModel via the updateDynamicRouteModel state function', fakeAsync(() => {
            // Arrange
            const someModelValue = 'anywhere';
            component.rootModel = {};
            component.modelSyncJsonProperties = [
              {
                name: 'ontoRoot',
                items: [
                  {
                    key: 'something',
                    identifier: {
                      property: 'anything',
                      value: [someModelValue],
                    },
                    addToRootModel: true,
                  },
                ],
                returnMode: 'single',
              },
            ];

            const someValue = {
              anything: 'anywhere',
              isImported: true,
            };

            const state = TestBed.inject(StateService);
            spyOn(state, 'updateDynamicRouteModel').and.callFake(() => null);

            // Act
            component.setValue({ ontoRoot: JSON.stringify(someValue) });
            tick();

            // Assert
            expect(state.updateDynamicRouteModel).toHaveBeenCalledWith({ something: someValue });
            discardPeriodicTasks();
          }));
        });

        describe('returnMode is "multiple', () => {
          it('returns empty array if json is empty array', () => {
            // Arrange
            component.modelSyncJsonProperties = [
              {
                name: 'emptyArray',
                items: [
                  {
                    key: 'something',
                    identifier: {
                      property: 'anything',
                      value: ['1'],
                    },
                  },
                ],
                returnMode: 'multiple',
              },
            ];

            // Act
            component.setValue({ emptyArray: JSON.stringify([]) });

            // Assert
            expect(component.targetModel.something).toEqual([]);
          });

          it('returns empty array if json is empty object', () => {
            // Arrange
            component.modelSyncJsonProperties = [
              {
                name: 'emptyArray',
                items: [
                  {
                    key: 'something',
                    identifier: {
                      property: 'anything',
                      value: ['1'],
                    },
                  },
                ],
                returnMode: 'multiple',
              },
            ];

            // Act
            component.setValue({ emptyArray: JSON.stringify({}) });

            // Assert
            expect(component.targetModel.something).toEqual([]);
          });

          it('returns the entire array and puts that on model', () => {
            // Arrange
            component.modelSyncJsonProperties = [
              {
                name: 'testPeopleJson',
                items: [
                  {
                    key: 'people',
                    identifier: {
                      property: 'personTypeId',
                      value: ['12345'],
                    },
                  },
                ],
                returnMode: 'multiple',
              },
            ];

            const personOne = {
              forename: 'Tester',
              surname: 'Testington',
              personTypeId: '12345',
              isImported: true,
            };
            const personTwo = {
              forename: 'Wont',
              surname: 'Appear',
              personTypeId: '12345',
              isImported: true,
            };
            const someValue = [personOne, personTwo];

            // Act
            component.setValue({ testPeopleJson: JSON.stringify(someValue) });

            // Assert
            expect(component.targetModel.people).toEqual(someValue);
          });

          it('can add values to the rootModel via the updateDynamicRouteModel state function', fakeAsync(() => {
            // Arrange
            component.rootModel = {};
            component.modelSyncJsonProperties = [
              {
                name: 'testPeopleJson',
                items: [
                  {
                    key: 'people',
                    identifier: {
                      property: 'personTypeId',
                      value: ['12345'],
                    },
                    addToRootModel: true,
                  },
                ],
                returnMode: 'multiple',
              },
            ];

            const personOne = {
              forename: 'Tester',
              surname: 'Testington',
              personTypeId: '12345',
              isImported: true,
            };
            const personTwo = {
              forename: 'Wont',
              surname: 'Appear',
              personTypeId: '12345',
              isImported: true,
            };
            const someValue = [personOne, personTwo];

            const state = TestBed.inject(StateService);
            spyOn(state, 'updateDynamicRouteModel').and.callFake(() => null);

            // Act
            component.setValue({ testPeopleJson: JSON.stringify(someValue) });
            tick();

            // Assert
            expect(state.updateDynamicRouteModel).toHaveBeenCalledWith({ people: someValue });
            discardPeriodicTasks();
          }));
        });
      });

      describe('sync meta data', () => {
        beforeEach(() => (component.enableModelSync = true));

        it('patches meta data if the property name is metaData by default', () => {
          // Arrange
          component.modelSyncMetaDataMap = {
            items: [{ key: 'one', label: 'One' }],
          };

          const someValue = {
            metaData: [{ Label: 'One', Value: 'One Value', ItemType: 1 }] as MetaDataItem[],
          };

          // Act
          component.setValue(someValue);

          // Assert
          expect(component.targetModel).toEqual({ one: 'One Value' });
        });

        it('can have a specified propertyName to set meta data', () => {
          // Arrange
          const someMetaDataPropName = 'someMetaDataPropName';
          component.modelSyncMetaDataMap = {
            propName: someMetaDataPropName,
            items: [{ key: 'one', label: 'One' }],
          };

          const someValue = {
            someMetaDataPropName: [{ Label: 'One', Value: 'One Value', ItemType: 1 }] as MetaDataItem[],
          };

          // Act
          component.setValue(someValue);

          // Assert
          expect(component.targetModel).toEqual({ one: 'One Value' });
        });

        it('can parse a string and grabs the "Items" property as thats how data is returned from SQL query', () => {
          // Arrange
          component.modelSyncMetaDataMap = {
            items: [{ key: 'one', label: 'One' }],
          };

          const someValue = {
            metaData: `{"Items": [{"Label": "One", "Value": "One Value", "ItemType": 1}]}`,
          };

          // Act
          component.setValue(someValue);

          // Assert
          expect(component.targetModel).toEqual({ one: 'One Value' });
        });
      });
    });

    describe('autofill properties', () => {
      const anyRootModel = {};
      let state: StateService;

      beforeEach(() => {
        state = TestBed.inject(StateService);
        component.rootModel = anyRootModel;
      });

      it('does not call mergeDynamicRouteModel when route is falsy', () => {
        // Arrange
        spyOn(state, 'mergeDynamicRouteModel').and.callFake(() => null);

        const someValue = {
          one: 'one',
        };

        component.autoFillProp = 'one';
        (component as any).route = null;

        // Act
        component.setValue(someValue);

        // Assert
        expect(state.mergeDynamicRouteModel).not.toHaveBeenCalled();
      });

      it('does not call mergeDynamicRouteModel when autoFillProp is falsy', () => {
        // Arrange
        spyOn(state, 'mergeDynamicRouteModel').and.callFake(() => null);

        const someValue = {
          one: 'one',
        };

        component.autoFillProp = '';
        (component as any).route = {
          data: {
            id: 'Route.Id',
          },
        };

        // Act
        component.setValue(someValue);

        // Assert
        expect(state.mergeDynamicRouteModel).not.toHaveBeenCalled();
      });

      it('does not call mergeDynamicRouteModel when the value is the same as the selectedValue', () => {
        // Arrange
        spyOn(state, 'mergeDynamicRouteModel').and.callFake(() => null);

        const someValue = {
          id: 'id',
          displayName: 'displayName',
        };

        component.autoFillProp = 'displayName';
        (component as any).route = {
          data: {
            id: 'Route.Id',
          },
        };

        component.selectedValue = someValue;

        // Act
        component.setValue(someValue);

        // Assert
        expect(state.mergeDynamicRouteModel).not.toHaveBeenCalled();
      });

      it('calls mergeDynamicRouteModel with autoFillProps and the appropriate skipProps properties missing', fakeAsync(() => {
        // Arrange
        spyOn(state, 'mergeDynamicRouteModel').and.callFake(() => null);

        const someRouteId = 'Route.Id';
        const anyAutofillModelOverrides = [''];
        const anyAutofillModelSkipProps = ['two', 'three'];

        const someValueBefore = {
          one: 'one',
          two: 'two',
          three: 'three',
        };

        const someValueAfter = {
          // two and three in skippedProps
          one: 'one',
        };

        component.autoFillProp = 'one';
        component.autoFillModelOverrides = anyAutofillModelOverrides;
        component.autoFillModelSkipProps = anyAutofillModelSkipProps;
        (component as any).route = {
          data: {
            id: someRouteId,
          },
        };

        // Act
        component.setValue(someValueBefore);
        tick(1000);

        // Assert
        expect(state.mergeDynamicRouteModel).toHaveBeenCalledWith(
          anyRootModel,
          someRouteId,
          someValueAfter,
          anyAutofillModelOverrides,
          anyAutofillModelSkipProps
        );
        flush();
      }));
    });
  });

  describe('onItemSelected()', () => {
    it('calls onChange and setValue with the value from the event', () => {
      // Arrange
      spyOn(component, 'setValue').and.callFake(() => null);
      spyOn<any>(component, 'onChange').and.callFake(() => null);

      const someValue = {
        id: 'id',
        displayName: 'displayName',
      };

      const someEvent = {
        option: {
          value: someValue,
        },
      } as MatAutocompleteSelectedEvent;

      // Act
      component.onItemSelected(someEvent);

      // Assert
      expect((component as any).onChange).toHaveBeenCalledWith(someValue);
      expect(component.setValue).toHaveBeenCalledOnceWith(someValue);
    });

    it('replaces escape sequences with a space if value has displayName property on it', () => {
      // Arrange
      spyOn(component, 'setValue').and.callFake(() => null);

      const someDisplayNameWithSequences = 'one\ntwo';
      const someDisplayNameWithoutSequences = 'one two';
      const someValueBefore = {
        id: 'id',
        displayName: someDisplayNameWithSequences,
      };
      const someValueAfter = {
        id: 'id',
        displayName: someDisplayNameWithoutSequences,
      };

      const someEvent = {
        option: {
          value: someValueBefore,
        },
      } as MatAutocompleteSelectedEvent;

      // Act
      component.onItemSelected(someEvent);

      // Assert
      expect(component.setValue).toHaveBeenCalledOnceWith(someValueAfter);
    });
  });

  describe('getLabel()', () => {
    it('returns the arguments displayName property if typeahead version is 1', () => {
      // Arrange
      component.version = 1;
      const someDisplayName = 'hello';
      const someItem = {
        id: 'id',
        displayName: someDisplayName,
      };

      // Act
      const result = component.getLabel(someItem);

      // Assert
      expect(result).toEqual(someDisplayName);
    });

    it('returns a parsed template if a labelTemplate exists after calling simpleTemplate with empty fallback and tidy set to true', () => {
      // Arrange
      const formsHelper = TestBed.inject(FormsHelperService);
      spyOn(formsHelper, 'simpleTemplate').and.callThrough();

      const someValue = 'someValue';
      const someFallback = '';
      const someTidy = true;
      const someLabelTemplate = '${test}';
      const someItem = {
        test: someValue,
      };

      component.version = 2;
      component.labelTemplate = someLabelTemplate;

      // Act
      const result = component.getLabel(someItem);

      // Assert
      expect(formsHelper.simpleTemplate).toHaveBeenCalledOnceWith(someLabelTemplate, someItem, someFallback, someTidy);
      expect(result).toEqual(someValue);
    });

    it('returns labelProp off the item if no labelTemplate is set on component', () => {
      // Arrange
      const formsHelper = TestBed.inject(FormsHelperService);
      spyOn(formsHelper, 'simpleTemplate').and.stub();

      const someValue = 'someValue';
      const someLabelProp = 'someLabelProp';
      const someItem = {
        someLabelProp: someValue,
      };

      component.version = 2;
      component.labelProp = someLabelProp;

      // Act
      const result = component.getLabel(someItem);

      // Assert
      expect(formsHelper.simpleTemplate).not.toHaveBeenCalled();
      expect(result).toEqual(someValue);
    });

    it('falls back to "label" if no labelProp is supplied', () => {
      // Arrange
      const someValue = 'someValue';
      const someItem = {
        label: someValue,
      };

      component.version = 2;

      // Act
      const result = component.getLabel(someItem);

      // Assert
      expect(component.labelProp).toBeUndefined();
      expect(result).toEqual(someValue);
    });

    it('returns undefined if falling back to "label" and no label property exists on the item', () => {
      // Arrange
      const someItem = {};

      component.version = 2;

      // Act
      const result = component.getLabel(someItem);

      // Assert
      expect(result).toBeUndefined();
    });
  });

  describe('onBlur()', () => {
    it('does not call onTouched if an option is selected', () => {
      // Arrange
      spyOn<any>(component, 'onTouched').and.callFake(() => null);

      const someEvent = {
        relatedTarget: {
          tagName: 'MAT-OPTION',
        },
      };

      // Act
      component.onBlur(someEvent);

      // Assert
      expect((component as any).onTouched).not.toHaveBeenCalled();
    });

    it('calls onTouched if blurring without selecting an option', () => {
      // Arrange
      spyOn<any>(component, 'onTouched').and.callFake(() => null);

      // Act
      component.onBlur({});

      // Assert
      expect((component as any).onTouched).toHaveBeenCalled();
    });

    describe('input has no value', () => {
      it('calls clearSelection', () => {
        // Arrange
        spyOn(component, 'clearSelection').and.callFake(() => null);
        component.textInput.nativeElement.value = '';

        // Act
        component.onBlur({});

        // Assert
        expect(component.clearSelection).toHaveBeenCalled();
      });
    });

    describe('input has value', () => {
      it('if a match exists, call onChange, set displayName value and set selectedValue', () => {
        // Arrange
        spyOn<any>(component, 'onChange').and.callFake(() => null);
        spyOn(component.displayName, 'setValue').and.callFake(() => null);

        const someDisplayName = 'one';
        const someMatch = { id: 'id', displayName: someDisplayName };

        component.textInput.nativeElement.value = 'one';
        (component as any).matches = [someMatch];

        // Act
        component.onBlur({});

        // Assert
        expect((component as any).onChange).toHaveBeenCalledWith(someMatch);
        expect(component.displayName.setValue).toHaveBeenCalledWith(someDisplayName);
        expect(component.selectedValue).toEqual(someMatch);
      });

      describe('mustMatch is true', () => {
        beforeEach(() => (component.mustMatch = true));

        it('calls clearSelection if there are no matches', () => {
          // Arrange
          spyOn(component, 'clearSelection').and.callFake(() => null);

          component.textInput.nativeElement.value = 'displayName';
          (component as any).matches = [];

          // Act
          component.onBlur({});

          // Assert
          expect(component.clearSelection).toHaveBeenCalled();
        });

        it('does not call clearSelection if there are matches', () => {
          // Arrange
          spyOn(component, 'clearSelection').and.callFake(() => null);

          component.textInput.nativeElement.value = 'displayName';
          (component as any).matches = [{ id: 'id', displayName: 'displayName' }];

          // Act
          component.onBlur({});

          // Assert
          expect(component.clearSelection).not.toHaveBeenCalled();
        });
      });

      describe('mustMatch is false', () => {
        it('sets id and displayName to input value if version 1', () => {
          // Arrange
          const someInputValue = 'someInputValue';

          component.textInput.nativeElement.value = someInputValue;
          component.version = 1;

          // Act
          component.onBlur({});

          // Assert
          expect(component.selectedValue).toEqual({ id: someInputValue, displayName: someInputValue });
        });

        it('sets "labelProp" to input value if version 2 and "labelProp" exists', () => {
          // Arrange
          const someInputValue = 'someInputValue';
          const someLabelProp = 'someLabelProp';

          component.textInput.nativeElement.value = someInputValue;
          component.labelProp = someLabelProp;
          component.version = 2;

          // Act
          component.onBlur({});

          // Assert
          expect(component.selectedValue as any).toEqual({ [someLabelProp]: someInputValue });
        });

        it('sets "label" ot input value if version 2 and "labelProp" does not exist', () => {
          // Arrange
          const someInputValue = 'someInputValue';

          component.textInput.nativeElement.value = someInputValue;
          component.version = 2;

          // Act
          component.onBlur({});

          // Assert
          expect(component.selectedValue as any).toEqual({ label: someInputValue });
        });

        it('call onChange, set displayName value and set selectedValue with our fallback', () => {
          // Arrange
          spyOn<any>(component, 'onChange').and.callFake(() => null);
          spyOn(component.displayName, 'setValue').and.callFake(() => null);

          const someInputValue = 'someInputValue';
          const someValue = {
            id: someInputValue,
            displayName: someInputValue,
          };

          component.textInput.nativeElement.value = someInputValue;
          component.version = 1;

          // Act
          component.onBlur({});

          // Assert
          expect((component as any).onChange).toHaveBeenCalledWith(someValue);
          expect(component.displayName.setValue).toHaveBeenCalledWith(someInputValue);
          expect(component.selectedValue).toEqual(someValue);
        });
      });
    });
  });

  describe('clearSelection()', () => {
    it('calls onChange with null', () => {
      // Arrange
      spyOn<any>(component, 'onChange').and.callFake(() => null);

      // Act
      component.clearSelection();

      // Assert
      expect((component as any).onChange).toHaveBeenCalledOnceWith(null);
    });

    it('calls displayName setValue with empty string', () => {
      // Arrange
      spyOn(component.displayName, 'setValue').and.callFake(() => null);

      // Act
      component.clearSelection();

      // Assert
      expect(component.displayName.setValue).toHaveBeenCalledWith('');
    });

    it('sets components selectedValue to null', () => {
      // Arrange
      const someValue = {
        id: 'someId',
        displayName: 'someDisplayName',
      };
      component.selectedValue = someValue;
      expect(component.selectedValue).toEqual(someValue);

      // Act
      component.clearSelection();

      // Assert
      expect(component.selectedValue).toBeNull();
    });
  });
});
