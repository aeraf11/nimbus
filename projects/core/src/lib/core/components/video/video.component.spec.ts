import { ComponentFixture, TestBed } from '@angular/core/testing';
import { VideoComponent } from './video.component';

describe('VideoComponent', () => {
  let component: VideoComponent;
  let fixture: ComponentFixture<VideoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [VideoComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(VideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return the original URL if it does not contain /{id}', () => {
    component.url = 'http://test.com/media/av/123.mp4';
    expect(component.getSourceUrl('abc123')).toEqual('http://test.com/media/av/123.mp4');
  });

  it('should replace /{id} with videoId if both are defined', () => {
    component.url = 'http://test.com/media/av/{id}';
    expect(component.getSourceUrl('abc123')).toEqual('http://test.com/media/av/abc123');
  });

  it('should set the sourceUrl property with formatted URL', () => {
    spyOn(component, 'getSourceUrl').and.returnValue('http://test.com/media/av/abc123');
    component.setFormattedUrl('abc123');
    expect(component.sourceUrl).toEqual('http://test.com/media/av/abc123');
  });

  it('should log an error message if the formatted URL is not available', () => {
    spyOn(component, 'getSourceUrl').and.returnValue(undefined);
    spyOn(console, 'warn');
    component.setFormattedUrl('abc123');
    expect(console.warn).toHaveBeenCalledWith('There was a problem with video id', 'abc123');
  });
});
