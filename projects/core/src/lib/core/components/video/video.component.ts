import { Component, ChangeDetectionStrategy, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { OnChange } from 'property-watch-decorator';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'ni-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => VideoComponent),
      multi: true,
    },
  ],
})
export class VideoComponent implements ControlValueAccessor {
  @Input() autoHideControls = true;
  @Input() autoHideControlsTime = 5;
  @Input() showPlayOverlay = true;
  @Input() hideControlPanel = false;
  @Input() showPlayPause = true;
  @Input() showCurrentTime = true;
  @Input() showRemainingTime = false;
  @Input() showTotalTime = false;
  @Input() showScrubBar = true;
  @Input() showScrubCurrentTime = true;
  @Input() showScrubSlider = true;
  @Input() showScrubBufferTime = true;
  @Input() timeFormat = 'hh:mm:ss';
  @Input() showMute = true;
  @Input() showVolume = true;
  @Input() showFullscreen = true;
  @Input() url = 'api/v2/media/av/{id}';
  @Input() width = '500px';
  @Input() height = '500px';

  @OnChange<string>(function (this: VideoComponent, value) {
    this.setFormattedUrl(value);
  })
  @Input()
  videoId = '';

  htmlVideoId: string;
  sourceUrl: string | undefined;

  constructor() {
    this.htmlVideoId = uuidv4();
  }

  setFormattedUrl(videoId: string): void {
    const formattedUrl = this.getSourceUrl(videoId);
    if (formattedUrl) {
      this.sourceUrl = formattedUrl;
    } else {
      console.warn('There was a problem with video id', videoId);
    }
  }

  getSourceUrl(videoId: string): string | undefined {
    const pattern = /\{id\}/;
    if (!videoId) {
      return;
    }
    if (this.url.match(pattern)) {
      return this.url.replace(pattern, `${videoId}`);
    }
    return this.url;
  }

  writeValue(value: any): void {
    if (value) {
      this.videoId = value;
      this.setFormattedUrl(value);
    }
  }

  registerOnChange(fn: any): void {
    return;
  }

  registerOnTouched(fn: any): void {
    return;
  }
}
