import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconTextInlineComponent } from './icon-text-inline.component';

describe('IconTextInlineComponent', () => {
  let component: IconTextInlineComponent;
  let fixture: ComponentFixture<IconTextInlineComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IconTextInlineComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(IconTextInlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
