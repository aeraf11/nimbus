import { Component, Input } from '@angular/core';

@Component({
  selector: 'ni-icon-text-inline',
  templateUrl: './icon-text-inline.component.html',
  styleUrls: ['./icon-text-inline.component.scss'],
})
export class IconTextInlineComponent {
  @Input() icon = '';
  @Input() colorClass = '';
  @Input() iconSize = '2rem';
  @Input() textSize = '2rem';
  @Input() text = '';
}
