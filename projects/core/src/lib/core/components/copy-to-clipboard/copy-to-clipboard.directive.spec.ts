import { Directive } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { CopyToClipboardDirective } from './copy-to-clipboard.directive';

@Directive({
  selector: '[niCopyToClipboard]',
})
export class MockCopyToClipboardDirective {}

describe('CopyToClipboardDirective', () => {
  it('should create an instance', () => {
    TestBed.configureTestingModule({
      imports: [MatSnackBarModule],
    });

    const snackBar = TestBed.inject(MatSnackBar);
    const directive = new CopyToClipboardDirective(snackBar);
    expect(directive).toBeTruthy();
  });
});
