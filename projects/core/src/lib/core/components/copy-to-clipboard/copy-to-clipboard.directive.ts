import { Directive, HostListener, Input } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackMessageType } from '../../models';
import { SnackMessageComponent } from '../snack-message/snack-message.component';

@Directive({
  selector: '[niCopyToClipboard]',
})
export class CopyToClipboardDirective {
  @Input() text = '';
  @Input() copyToClipboardSuccess = 'Copied to clipboard';
  @Input() copyToClipboardError = 'Could not copy to clipboard';

  constructor(private snackbar: MatSnackBar) {}

  @HostListener('click', ['$event']) copyToClipboard(event: Event): void {
    event.preventDefault();
    if (this.text) {
      navigator.clipboard.writeText(this.text).then(
        () => {
          this.snackbar.openFromComponent(SnackMessageComponent, {
            duration: 3000,
            data: { type: SnackMessageType.Info, message: this.copyToClipboardSuccess },
          });
        },
        (err) => {
          this.snackbar.openFromComponent(SnackMessageComponent, {
            duration: 3000,
            data: { type: SnackMessageType.Error, message: `${this.copyToClipboardError}. ${err}` },
          });
        }
      );
    }
  }
}
