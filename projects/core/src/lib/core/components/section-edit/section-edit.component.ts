﻿import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ni-section-edit',
  templateUrl: './section-edit.component.html',
  styleUrls: ['./section-edit.component.scss'],
})
export class SectionEditComponent {
  @Input() isEditMode = false;
  @Input() sectionEditCancelIcon = 'fas fa-xmark';
  @Input() sectionEditCancelText = 'Cancel';
  @Input() sectionEditEditIcon = 'far fa-pencil';
  @Input() sectionEditEditText = 'Edit Settings';
  @Input() sectionEditMarginBottom = '1rem';
  @Input() sectionEditMarginTop = '-1.5rem';
  @Input() sectionEditSaveIcon = 'fas fa-check';
  @Input() sectionEditSaveText = 'Save';
  @Output() cancel = new EventEmitter();
  @Output() edit = new EventEmitter();
  @Output() save = new EventEmitter();

  onSave() {
    this.save.emit();
  }

  onEdit() {
    this.edit.emit();
  }

  onCancel() {
    this.cancel.emit();
  }
}
