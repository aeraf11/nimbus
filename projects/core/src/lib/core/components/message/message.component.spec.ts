import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { ListFilterService, StateService } from '@nimbus/core/src/lib/core';
import { ListService } from '../../../core/services/list.service';
import { MockListService } from '../../../core/services/list.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { BlobService } from './../../../core/services/blob.service';
import { MockBlobService } from './../../../core/services/blob.service.spec';
import { MessageComponent } from './message.component';
import { DataCardsComponent } from '../data-cards/data-cards.component';

describe('MessageComponent', () => {
  let component: MessageComponent;
  let fixture: ComponentFixture<MessageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MessageComponent],
      imports: [MatDialogModule],
      providers: [
        DataCardsComponent,
        { provide: StateService, useClass: MockStateService },
        { provide: ListService, useClass: MockListService },
        { provide: ListFilterService, useClass: ListFilterService },
        { provide: BlobService, useClass: MockBlobService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
