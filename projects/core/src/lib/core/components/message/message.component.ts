import { BlobService } from '@nimbus/core/src/lib/core';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, forwardRef, HostBinding } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { DataCardsItemBaseComponent } from '../data-cards-item-base/data-cards-item-base.component';
import { DataCardsComponent } from '../data-cards/data-cards.component';

@UntilDestroy()
@Component({
  selector: 'ni-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MessageComponent),
      multi: true,
    },
  ],
})
export class MessageComponent extends DataCardsItemBaseComponent implements ControlValueAccessor {
  @HostBinding('class.message') hostClass = true;

  itemUpdated: Observable<any> = this.itemUpdate.asObservable();

  imageThumbnail = '';

  private readonly blobIdAttribute = 'data-blobid';

  constructor(public override parent: DataCardsComponent, change: ChangeDetectorRef, private blobService: BlobService) {
    super(parent, change);
    this.itemUpdated.pipe(untilDestroyed(this)).subscribe(() => this.updateAttachmentItem());
  }

  updateAttachmentItem() {
    const message = this.item.messageHtml;
    if (this.isAnchor(message)) {
      const div = document.createElement('div');
      div.innerHTML = message;
      const link = <HTMLAnchorElement>div.firstChild;
      if (message.indexOf('/Global/DownloadAttachment') > -1) {
        const url = new URL(link.href);

        this.item.blobId = url.searchParams.get('id');
        this.item.fileName = url.searchParams.get('filename');
      } else if (link.href.indexOf('#') > -1) {
        this.item.messageHtml = 'Action: ' + link.innerText;
      }
    } else if (message && message.indexOf('/Global/StreamPhoto') > -1) {
      this.imageThumbnail = this.getBlobIdFromString(message);
    }
  }

  isAnchor(message: string) {
    return /^\<a.*\>.*\<\/a\>/i.test(message);
  }

  private getBlobIdFromString(message: string): string {
    if (message.indexOf(this.blobIdAttribute) !== -1) {
      // Required as the created element will try and call /Global/StreamPhoto resulting in console error/extra unecessary call
      message = message.replace('src=', 'data-src=');
      const el = document.createElement('div');
      el.innerHTML = message;

      const image = el.getElementsByTagName('img')[0];
      if (image) {
        return image.getAttribute(this.blobIdAttribute) ?? '';
      }
    }
    return '';
  }
}
