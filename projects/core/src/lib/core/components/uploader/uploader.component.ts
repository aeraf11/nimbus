import { LiveAnnouncer } from '@angular/cdk/a11y';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnDestroy,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MatButton } from '@angular/material/button';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  FileInfo,
  RemovingEventArgs,
  SelectedEventArgs,
  SuccessEventArgs,
  UploaderComponent as SyncFusionUploaderComponent,
  UploadingEventArgs,
} from '@syncfusion/ej2-angular-inputs';
import { createSpinner, showSpinner } from '@syncfusion/ej2-popups';
import { OnChange } from 'property-watch-decorator';
import { v4 as uuidv4 } from 'uuid';
import { DialogConfig, DialogType } from '../../models';
import { StorageType } from '../../models/storage-type';
import { DialogService, FileService, FileSyncService } from '../../services';
import { MetaDataAttachment } from './../../models/meta-data-attachment';

interface AttachmentAssignment {
  file: FileInfo;
  attachmentId: string;
}

export type FileDetails = {
  info: FileInfo;
  progress: number;
  chunkIds?: string[];
  combining?: boolean | 'failed';
  error?: string;
  paused?: boolean;
  fileId?: string;
  attachmentId?: string;
  thumbnailId?: string;
  websizeId?: string;
  height?: number;
  width?: number;
};

@UntilDestroy()
@Component({
  selector: 'ni-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UploaderComponent),
      multi: true,
    },
  ],
})
export class UploaderComponent implements ControlValueAccessor, OnDestroy {
  UPLOAD_ERROR = 'Upload failed.';

  @Input() formControl = new UntypedFormControl();
  @Input() asyncSettings?: any;
  @Input() chunkSize!: number;
  @Input() multiple = false;
  @Input() enableModelSync = false;
  @Input() modelSyncExclusions: any[] = [];
  @Input() targetModel: any;
  @Input() autoUpload = false;
  @Input() minFileSize?: number;
  @Input() maxFileSize?: number;
  @Input() allowedExtensions?: string;
  @Input() sequentialUpload = false;
  @Input() form?: UntypedFormGroup;
  @Input() enableDelete = true;
  @Input() enableDownload = false;
  @Input() storageType: StorageType | undefined;
  @Input() showFileIdAfterUpload = false;
  @Input() scrollOnFileSelect = true;
  @Input() includeThumbnail = false;
  @Input() createWebImage = false;
  @Input() readonly = false;
  @Input() ariaLabel?: string;

  @OnChange<AttachmentAssignment>(function (this: UploaderComponent, value) {
    if (value && value.file && value.attachmentId) {
      const item = this.filesDetails?.find((file) => file.info.id === value.file.id);
      if (item) {
        item.attachmentId = value.attachmentId;
        this.change.markForCheck();
      }
    }
  })
  @Input()
  attachmentAssignment?: AttachmentAssignment;

  @Input() metaDataAttachments: MetaDataAttachment[] = [];

  @Output() fileUploadSuccess = new EventEmitter<FileDetails>();
  @Output() fileRemove = new EventEmitter<string>();

  @ViewChild('uploader') public uploader!: SyncFusionUploaderComponent;
  @ViewChild('dropArea') public dropArea!: ElementRef;
  @ViewChild('browse') public browseButton!: MatButton;
  @ViewChildren('filelist', { read: ElementRef }) renderedUsers?: QueryList<ElementRef>;
  public parentElement!: HTMLElement;
  public progressbarContainer!: HTMLElement;
  public filesDetails: FileDetails[] = [];
  public filesList: HTMLElement[] = [];
  private timeOutId!: any;
  value: any;

  private onChange!: (value: any) => void;
  private onTouched!: () => void;

  constructor(
    private fileService: FileService,
    private fileSyncService: FileSyncService,
    private element: ElementRef,
    private change: ChangeDetectorRef,
    private dialogService: DialogService,
    private liveAnnouncer: LiveAnnouncer
  ) {}

  onUploaderCreated() {
    if (this.formControl.value && this.formControl.value.length > 0) {
      const files: FileDetails[] = this.formControl.value.map((file: any) => {
        return {
          info: {
            name: file.name,
            size: file.size,
          },
          thumbnailId: file.thumbnailId,
          websizeId: file.websizeId,
          fileId: file.fileId,
          height: file.height,
          width: file.width,
          progress: 100,
        };
      });
      this.filesDetails = this.filesDetails.concat(files);
      this.change.detectChanges();
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-empty-function
  writeValue(): void {}

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  canRetry(file: FileDetails): boolean {
    return (
      file.error && this.asyncSettings.chunkSize && file.info.fileSource === 'paste' && file.combining !== 'failed'
    );
  }

  onBrowseClick() {
    const wrapper = <HTMLDivElement>this.element.nativeElement.getElementsByClassName('e-file-select-wrap')[0];
    const button = <HTMLButtonElement>wrapper.getElementsByClassName('e-btn')[0];
    if (wrapper && this.browseButton) {
      button.click();
    }
  }

  public onFileSelect(args: SelectedEventArgs): void {
    this.formControl.setErrors({ uploading: true });
    this.change.markForCheck();
    const details = args.filesData.map((info) => {
      return { info, progress: 0 };
    });
    if (!this.multiple) {
      this.filesDetails = [];
    }
    this.filesDetails = this.filesDetails.concat(details);

    if (this.scrollOnFileSelect) {
      this.timeOutId = setTimeout(() => {
        const userToScrollOn: ElementRef<any>[] = this.renderedUsers?.toArray() || [];
        userToScrollOn[this.filesDetails.length - 1].nativeElement.scrollIntoView({
          behavior: 'smooth',
        });
      }, 50);
    }

    this.uploader.upload(args.filesData, true);
    this.uploader.clearAll();
    args.cancel = true;
  }

  fileChanged(files: any): void {
    const result = this.multiple ? files : files[0];
    if (this.onChange) this.onChange(result);
    this.change.markForCheck();
    if (this.onTouched) this.onTouched();
  }

  onFileUpload(args: any): void {
    const progressValue: number = Math.round((args.e.loaded / args.e.total) * 100);
    const detail = this.getFileDetail(args.file.id);
    if (detail) detail.progress = progressValue;
  }

  onFileRemove(args: RemovingEventArgs): void {
    args.postRawFile = false;
  }

  onUploadFailed(args: any): void {
    const detail = this.getFileDetail(args.file.id);
    if (detail) detail.error = this.UPLOAD_ERROR;
    this.formControl.setErrors({ uploading: false });
    this.change.markForCheck();
  }

  removeFiles(file: FileDetails): void {
    this.dialogService
      .openDialog(<DialogConfig>{
        title: 'Phaneros',
        bodyText: 'Are you sure you want to remove this file?',
        type: DialogType.OKCancel,
      })
      .pipe(untilDestroyed(this))
      .subscribe((data: boolean) => {
        if (data) {
          this.uploader.clearAll();
          if (file.progress === 100) {
            this.uploader.remove(file.info);
            this.liveAnnouncer.announce('Successfully removed');
          } else if (file.chunkIds) {
            this.uploader.remove(file.info);
            this.liveAnnouncer.announce('Successfully removed');
          }
          this.filesDetails = this.filesDetails.filter((fileDetail) => fileDetail !== file);
          if (this.multiple) {
            const value = this.formControl.value?.filter((item: any) => file.fileId !== item.fileId);
            this.fileChanged(value);
          } else {
            this.fileChanged([]);
          }
          this.fileRemove.emit(file.attachmentId);
        }
      });
  }

  generateSpinner(targetElement: HTMLElement): void {
    createSpinner({ target: targetElement, width: '25px' });
    showSpinner(targetElement);
  }

  onUploadSuccess(args: SuccessEventArgs) {
    if (this.formControl?.hasError('uploading')) {
      this.formControl.setErrors({ uploading: null });
    }
    this.change.markForCheck();

    const detail = this.getFileDetail(args.file?.id || '');
    if (detail) {
      detail.error = undefined;
    }
  }

  retry(file: FileDetails) {
    this.uploader.retry(file.info, false, true);
  }

  pause(file: FileDetails) {
    this.uploader.pause(file.info, true);
    file.paused = true;
    this.formControl.setErrors({ uploading: false });
    this.change.markForCheck();
  }

  resume(file: FileDetails) {
    this.uploader.resume(file.info, true);
    file.paused = false;
  }

  onUploading(args: any) {
    const uploadingArgs: UploadingEventArgs = <UploadingEventArgs>args;
    const chunkTotal = Math.ceil(uploadingArgs.fileData.size / this.chunkSize);
    const detail = this.getFileDetail(uploadingArgs.fileData.id || '');
    if (!args.customFormData) args.customFormData = [];
    args.customFormData.push({ isChunk: !!this.asyncSettings.chunkSize && chunkTotal > 1 });
    args.customFormData.push({ storageType: this.storageType });
    args.customFormData.push({ includeThumbnail: this.includeThumbnail });
    args.customFormData.push({ createWebImage: this.createWebImage });

    if (detail) {
      if (uploadingArgs.currentChunkIndex === 0) {
        detail.chunkIds = [];
      }

      // Add handler to extract the file upload id and handle form value on successful upload
      uploadingArgs.currentRequest?.addEventListener('load', (evt) => {
        if (uploadingArgs.currentRequest?.status === 200) {
          const data = JSON.parse(uploadingArgs.currentRequest.response);

          // Chunked upload - first chunk comes through here
          if (uploadingArgs.currentChunkIndex === 0 && chunkTotal > 1) {
            detail.chunkIds?.push(data.fileId);
            // Non-chunked upload
          } else {
            detail.progress = 100;
            const newItems: any = [];

            newItems.push({
              fileId: data.fileId,
              name: uploadingArgs.fileData.name,
              type: uploadingArgs.fileData.type,
              size: uploadingArgs.fileData.size,
              clientId: uploadingArgs.fileData.id,
              thumbnailId: data.thumbnailStorageId,
              websizeId: data.websizeBlobId,
              height: data.height,
              width: data.width,
            });

            if (this.filesDetails) {
              const fileToUpdate: FileDetails = this.filesDetails.filter(
                (f) => f.info.id === newItems[0].clientId
              )?.[0];
              if (fileToUpdate) {
                fileToUpdate.fileId = data.fileId;
                fileToUpdate.thumbnailId = data.thumbnailStorageId;
              }
            }
            if (data.successMessage) {
              detail.info.status = data.successMessage;
            }

            if (this.multiple) {
              this.fileChanged([...(this.formControl.value || []), ...newItems]);
            } else {
              this.fileChanged(newItems);
              this.syncModel();
            }
            this.fileUploadSuccess.emit(detail);
          }
        }
      });

      // Check whether the file is uploading from paste.
      if (args.fileData.fileSource === 'paste') {
        const newName: string =
          this.getUniqueID(args.fileData.name.substring(0, args.fileData.name.lastIndexOf('.'))) + '.png';
        args.customFormData.push({ fileName: newName });
      }
    }
  }

  onChunkUploading(args: any) {
    const uploadingArgs: UploadingEventArgs = <UploadingEventArgs>args;
    const chunkTotal = Math.ceil(uploadingArgs.fileData.size / this.chunkSize);
    const lastChunk = uploadingArgs.currentChunkIndex === chunkTotal - 1;
    const detail = this.getFileDetail(uploadingArgs.fileData.id || '');

    if (!args.customFormData) args.customFormData = [];
    args.customFormData.push({ isChunk: !!this.asyncSettings.chunkSize });
    args.customFormData.push({ includeThumbnail: this.includeThumbnail });
    args.customFormData.push({ createWebImage: this.createWebImage });

    if (detail) {
      // Divided by 2 because the "uploading" bar only goes to 50 and "processing" bar beings at 51
      // to simulate 1 progres bar
      detail.progress = (((uploadingArgs.currentChunkIndex || 0) / chunkTotal) * 100) / 2;
      this.change.detectChanges();

      // Add handler to extract the chunk upload id and handle form value on successful upload
      uploadingArgs.currentRequest?.addEventListener('load', (evt) => {
        if (uploadingArgs.currentRequest?.status === 200) {
          const data = JSON.parse(uploadingArgs.currentRequest.response);
          detail.chunkIds?.push(data.fileId);
        }
        if (lastChunk) {
          detail.combining = true;
          detail.progress = 50; // 50 is the latest point of the "uploading" bar as the "processing" bar begins at 51
          this.change.detectChanges();

          const onError = (message: string): void => {
            detail.combining = 'failed';
            detail.error = this.UPLOAD_ERROR;
            console.error(`${this.UPLOAD_ERROR} - ${message}`);
          };

          const setProgress = (progress: number): void => {
            detail.progress = progress / 2 + 50; // Starts at 50 to make it look like "uploading" and "processing" is 1 bar
            this.change.detectChanges();
          };

          const onComplete = (model: any, fileData: FileInfo): void => {
            detail.combining = false;
            detail.progress = 100;

            const newItems = [];

            newItems.push({
              fileId: model.blobId,
              name: fileData.name,
              type: fileData.type,
              size: fileData.size,
              clientId: fileData.id,
              height: model.height,
              width: model.width,
              thumbnailId: model.thumbnailStorageId,
              websizeId: model.websizeBlobId,
            });

            if (this.multiple) {
              this.fileChanged([...(this.formControl.value ?? []), ...newItems]);
            } else {
              this.fileChanged(newItems);
              this.syncModel();
            }

            this.change.detectChanges();
          };

          this.fileService
            .recombineQueue(uploadingArgs.fileData.name, detail.chunkIds ?? [], this.storageType)
            .subscribe((result) => {
              if (result.success) {
                const recombinationId = result.fileRecombinationId;

                this.fileSyncService.addRecombinations({
                  id: recombinationId,
                  model: null,
                  attachmentId: uploadingArgs.fileData.id ?? '', // clientId
                  fileId: '', // we will get this off the model in onComplete
                  setError: (message) => onError(message),
                  setProgress: (progress) => setProgress(progress),
                  onComplete: (model, attachment, fileId) => onComplete(model, uploadingArgs.fileData),
                });
              }
            });
        }
      });
    } else {
      args.cancel = true;
    }
  }

  onRemoving(args: any) {
    const removingArgs: RemovingEventArgs = <RemovingEventArgs>args;
    const file = removingArgs.filesData[0]; // async remove only ever single

    //Push the fileId recevied from the upload response into the formdata
    if (this.value) {
      const upload = this.multiple
        ? this.formControl?.value.find((u: any) => u.name === file.name && u.type === file.type && u.size === file.size)
        : this.formControl?.value;
      if (upload?.fileId) {
        removingArgs.customFormData.push({ fileId: upload.fileId });
      }
    }
  }

  getUniqueID(name: string): string {
    return uuidv4();
  }

  getFileDetail(id: string): FileDetails | undefined {
    return this.filesDetails.find((detail) => detail.info.id === id);
  }

  syncModel(): void {
    const value: any = this.formControl.value;
    if (value) {
      const model: any = {};
      if (this.enableModelSync) {
        for (const prop in value) {
          if (!this.modelSyncExclusions?.includes(prop)) {
            this.targetModel[prop] = model[prop] = value[prop];
          }
        }
        this.form?.patchValue(model);
      }
    }
  }

  removeMetaData(attachmentId: string): void {
    this.fileRemove.emit(attachmentId);
  }

  downloadAttachment(attachmentId: string | undefined): void {
    if (attachmentId) {
      this.fileService.downloadAttachmentById(attachmentId);
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.timeOutId);
  }
}
