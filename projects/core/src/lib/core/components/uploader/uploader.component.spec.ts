import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { SiteConfig } from '../../models/site-config';
import { StateService } from '../../services/state.service';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { UploaderComponent } from './uploader.component';

describe('UploaderComponent', () => {
  let component: UploaderComponent;
  let fixture: ComponentFixture<UploaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UploaderComponent],
      imports: [HttpClientTestingModule, MatDialogModule],
      providers: [
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
        { provide: StateService, useClass: MockStateService },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA], // Required for ejs-loader
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UploaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
