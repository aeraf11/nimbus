import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntityTableComponent } from './entity-table.component';
import { FormsHelperService } from '../../services';
import { DataType, TableTypeColumnDefinition } from '../../models';
import { ChangeDetectorRef } from '@angular/core';

describe('EntityTableComponent', () => {
  let spyFormsHelperService: jasmine.SpyObj<FormsHelperService>;

  let component: EntityTableComponent;
  let fixture: ComponentFixture<EntityTableComponent>;

  beforeEach(() => {
    spyFormsHelperService = jasmine.createSpyObj('FormsHelperService', ['parseTemplate']);
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EntityTableComponent],
      providers: [{ provide: FormsHelperService, useValue: spyFormsHelperService }],
    }).compileComponents();

    fixture = TestBed.createComponent(EntityTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    // Assert
    expect(component).toBeTruthy();
  });

  describe('columnDefinitions', () => {
    let columnDefinitions: TableTypeColumnDefinition[];

    beforeEach(() => {
      // Arrange
      columnDefinitions = [
        {
          key: 'key1',
          columnName: 'col1',
          tab: 'tab1',
          dataType: 'text',
        },
        {
          key: 'key2',
          columnName: 'col2',
          tab: 'tab1',
          dataType: 'date',
        },
        {
          key: 'key3',
          columnName: 'col3',
          tab: 'tab2',
          dataType: 'dateTime',
        },
        {
          key: 'key4',
          columnName: 'col4',
          tab: 'tab2',
          dataType: 'multiChoice',
        },
      ];
    });

    it('should set columnDefinitions', () => {
      // Act
      component.columnDefinitions = columnDefinitions;
      // Assert
      expect(component.columnDefinitions).toEqual(columnDefinitions);
    });

    it('should set columnKeys', () => {
      // Act
      component.columnDefinitions = columnDefinitions;
      // Assert
      expect(component.columnKeys).toEqual(['key1', 'key2', 'key3', 'key4']);
    });

    it('should set tabColumns', () => {
      //Act
      component.columnDefinitions = columnDefinitions;
      // Assert
      expect(component.tabColumns).toEqual(
        new Map([
          ['tab1', [columnDefinitions[0], columnDefinitions[1]]],
          ['tab2', [columnDefinitions[2], columnDefinitions[3]]],
        ])
      );
    });

    it('should set tabColumnKeys', () => {
      //Act
      component.columnDefinitions = columnDefinitions;
      // Assert
      expect(component.tabColumnKeys).toEqual(
        new Map([
          ['tab1', [columnDefinitions[0].key, columnDefinitions[1].key]],
          ['tab2', [columnDefinitions[2].key, columnDefinitions[3].key]],
        ])
      );
    });

    it('should set tableTabIds', () => {
      // Act
      component.columnDefinitions = columnDefinitions;
      // Assert
      expect(component.tableTabIds).toEqual(new Set(['tab1', 'tab2']));
    });

    it('should set columnNames', () => {
      // Act
      component.columnDefinitions = columnDefinitions;
      // Assert
      expect(component.columnNames).toEqual(
        new Map([
          ['key1', 'col1'],
          ['key2', 'col2'],
          ['key3', 'col3'],
          ['key4', 'col4'],
        ])
      );
    });

    it('should set columnTypes', () => {
      // Act
      component.columnDefinitions = columnDefinitions;
      // Assert
      expect(component.columnTypes).toEqual(
        new Map([
          ['key1', DataType.text],
          ['key2', DataType.date],
          ['key3', DataType.dateTime],
          ['key4', DataType.multiChoice],
        ])
      );
    });

    it('should call ChangeDetectorRef.markForCheck', () => {
      // Arrange
      const changeDetectorRef = fixture.debugElement.injector.get(ChangeDetectorRef);
      const spyMarkForCheck = spyOn(changeDetectorRef.constructor.prototype, 'markForCheck');
      // Act
      component.columnDefinitions = columnDefinitions;
      // Assert
      expect(spyMarkForCheck).toHaveBeenCalled();
    });
  });

  describe('items', () => {
    let items: unknown[];

    beforeEach(() => {
      // Arrange
      items = [
        {
          key: 'key1',
          name: 'item1',
        },
        {
          key: 'key2',
          parentKey: 'key1',
          name: 'item2',
        },
        {
          key: 'key3',
          parentKey: 'key4',
          name: 'item3',
        },
      ];
    });

    it('should set items', () => {
      // Act
      component.items = items;
      // Assert
      expect(component.items).toEqual(items);
    });

    it('should remove items without a parent when groupBy is set', () => {
      // Act
      component.groupBy = { parentKeyPath: 'parentKey', selfKeyPath: 'key' };
      component.items = items;
      // Assert
      expect(component.items).toEqual([items[0], items[1]]);
    });
  });

  describe('getValue', () => {
    let item: { [key: string]: unknown };
    let column: TableTypeColumnDefinition;

    beforeEach(() => {
      // Arrange
      item = {
        key: 'itemKey',
        name: 'itemName',
      };
      column = {
        key: 'name',
        columnName: 'Name',
      };
    });

    it('should return item[column.key] if computedValue and expressionValue are not set', () => {
      // Act
      const result = component.getValue(item, column);
      // Assert
      expect(result).toEqual('itemName');
    });

    it('should call formsHelper.parseTemplate with computedValue if it exists', () => {
      // Arrange
      column.computedValue = 'computedValue';
      // Act
      component.getValue(item, column);
      // Assert
      expect(spyFormsHelperService.parseTemplate).toHaveBeenCalledWith('computedValue', item, '');
    });

    it('should return expressionValue if it contains a static value', () => {
      // Arrange
      column.expressionValue = '"test"';
      // Act
      const result = component.getValue(item, column);
      // Assert
      expect(result).toEqual('test');
    });

    it('should return the correct item value if expressionValue references model', () => {
      // Arrange
      column.expressionValue = 'model.key';
      // Act
      const result = component.getValue(item, column);
      // Assert
      expect(result).toEqual('itemKey');
    });
  });
});
