import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Input,
  Output,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import {
  DataType,
  convertToDataType,
  FormsHelperService,
  utils,
  TableTypeColumnDefinition,
} from '@nimbus/core/src/lib/core';

export interface GroupConfig {
  parentKeyPath: string;
  selfKeyPath: string;
}

@Component({
  selector: 'ni-entity-table',
  templateUrl: './entity-table.component.html',
  styleUrls: ['./entity-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => EntityTableComponent),
      multi: true,
    },
  ],
})
export class EntityTableComponent {
  @Input()
  set columnDefinitions(value: TableTypeColumnDefinition[]) {
    this.setColumnDefinitions(value);
  }

  get columnDefinitions(): TableTypeColumnDefinition[] {
    return this._columnDefinitions;
  }

  @Input() set items(value: unknown[]) {
    this.setItems(value);
  }

  get items(): unknown[] {
    return this._items;
  }

  @Input() context?: { entityId?: string; entityTypeId?: number; entityDescription?: string };
  @Input() required = false;
  @Input() disabled = false;
  @Input() placeholder!: string;
  @Input() ariaTableName?: string;
  @Input() ariaRowLabel?: string;
  @Input() groupBy?: GroupConfig | null;
  @Input() rootModel?: unknown;
  @Input() localModel?: unknown;

  @Output() rowClick = new EventEmitter<{ [key: string]: unknown }>();

  get columnKeys(): string[] {
    return this._columnDefinitions.map((field) => '' + field.key);
  }

  columnNames = new Map<string, string>();
  columnTypes = new Map<string, DataType>();
  tableTabIds = new Set<string>();
  tabColumns = new Map<string, TableTypeColumnDefinition[]>();
  tabColumnKeys = new Map<string, string[]>();
  allRowsAreHidden = false;

  private _columnDefinitions: TableTypeColumnDefinition[] = [];
  private _items: unknown[] = [];

  constructor(private change: ChangeDetectorRef, private formsHelper: FormsHelperService) {}

  getValue(item: { [key: string]: unknown }, column: TableTypeColumnDefinition): unknown {
    try {
      if (column.computedValue) {
        return this.formsHelper.parseTemplate(column.computedValue, item, '');
      }
      if (column.expressionValue) {
        const expression = utils.evalStringFormExpression<string>(column.expressionValue);
        if (expression) {
          return expression(this.rootModel, item, this.context, this.formsHelper);
        }
      }
    } catch (ex) {}
    return item[column.key];
  }

  private setItems(items: unknown[]) {
    if (Array.isArray(items)) {
      if (this.groupBy != null) {
        this._items = this.removeOrphanedItems(items, this.groupBy);
      } else {
        this._items = items;
      }
      this.allRowsAreHidden = items.every(
        (item) =>
          item != null && typeof item === 'object' && '_isVisuallyHidden' in item && item._isVisuallyHidden === true
      );
    } else {
      this._items = [];
      this.allRowsAreHidden = false;
    }
  }

  private setColumnDefinitions(columnDefinitions: TableTypeColumnDefinition[]) {
    if (Array.isArray(columnDefinitions) && columnDefinitions.length) {
      this._columnDefinitions = columnDefinitions;

      this.columnNames.clear();
      this.columnTypes.clear();

      this.tableTabIds.clear();
      this.tabColumns.clear();
      this.tabColumnKeys.clear();

      columnDefinitions.forEach((col) => {
        this.columnNames.set(col.key, col.columnName || col.key);
        this.columnTypes.set(col.key, col.dataType ? convertToDataType(col.dataType) : DataType.text);

        const tabId = col.tab || 'Default';

        this.tableTabIds.add(tabId);
        this.tabColumns.has(tabId) ? this.tabColumns.get(tabId)?.push(col) : this.tabColumns.set(tabId, [col]);
        this.tabColumnKeys.has(tabId)
          ? this.tabColumnKeys.get(tabId)?.push(col.key)
          : this.tabColumnKeys.set(tabId, [col.key]);
      });

      this.change.markForCheck();
    }
  }

  private removeOrphanedItems(items: unknown[], groupBy: GroupConfig): unknown[] {
    const itemKeys = items.map((item) => ({
      item,
      parent: utils.resolveObjectPath(groupBy.parentKeyPath, item),
      self: utils.resolveObjectPath(groupBy.selfKeyPath, item),
    }));

    const allKeys = new Set(itemKeys.map((itemKey) => itemKey.self));

    // Return only items that are a root parent (no parent key) or have a parent
    return itemKeys
      .filter((itemKey) => {
        return !itemKey.parent || allKeys.has(itemKey.parent);
      })
      .map((itemKey) => itemKey.item);
  }
}
