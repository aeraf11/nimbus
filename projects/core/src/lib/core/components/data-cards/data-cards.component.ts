import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  HostBinding,
  Input,
  Optional,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, UntypedFormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { FormlyFieldConfig, FormlyForm } from '@ngx-formly/core';
import deepClone from 'deep.clone';
import { OnChange } from 'property-watch-decorator';
import {
  StateService,
  IndexedDbService,
  SignalRService,
  ListService,
  FormsHelperService,
  ListFilterService,
  CardListWrapperComponent,
  QuickSearchWrapperComponent,
  ItemsUi,
  EntityMediaRequestComponent,
  ListFilterComponent,
} from '@nimbus/core/src/lib/core';
import { DataListBaseComponent } from '../data-list-base/data-list-base.component';

@Component({
  selector: 'ni-data-cards',
  templateUrl: './data-cards.component.html',
  styleUrls: ['./data-cards.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DataCardsComponent),
      multi: true,
    },
  ],
})
export class DataCardsComponent extends DataListBaseComponent implements ControlValueAccessor {
  @ViewChild('formly') formly?: FormlyForm;
  @HostBinding('class.collapse') @Input() collapse = false;
  @HostBinding('class.full-height') @Input() fullHeight = false;
  quickSearchComponent: QuickSearchWrapperComponent | undefined;

  constructor(
    service: ListService,
    change: ChangeDetectorRef,
    state: StateService,
    private changes: ChangeDetectorRef,
    formsHelper: FormsHelperService,
    @Optional() cardListWrapper: CardListWrapperComponent,
    listFilterService: ListFilterService,
    dialog: MatDialog,
    @Optional() entityMediaRequest: EntityMediaRequestComponent,
    @Optional() listFilter: ListFilterComponent,
    @Optional() quickSearchWrapper: QuickSearchWrapperComponent,
    indexedDbService: IndexedDbService,
    signalR: SignalRService
  ) {
    super(
      service,
      change,
      state,
      formsHelper,
      cardListWrapper,
      listFilterService,
      dialog,
      entityMediaRequest,
      listFilter,
      quickSearchWrapper,
      indexedDbService,
      signalR
    );
    this.quickSearchComponent = quickSearchWrapper;
  }

  @OnChange<FormlyFieldConfig>(function (this: DataCardsComponent, value) {
    if (value) {
      deepClone(value).then((fieldConfig) => {
        if (this.fieldConfigs[0]) {
          this.fieldConfigs[0].fieldArray = fieldConfig;
          this.fieldConfigs = [...this.fieldConfigs];
        }
        this.changes.markForCheck();
      });
    }
  })
  @Input()
  field?: FormlyFieldConfig;

  @OnChange<ItemsUi>(function (this: DataCardsComponent, value) {
    if (value) {
      if (this.fieldConfigs[0]) {
        this.fieldConfigs[0].props = { itemsUi: value };

        this.fieldConfigs = [...this.fieldConfigs];
      }

      this.changes.markForCheck();
    }
  })
  @Input()
  itemsUi?: ItemsUi;

  formGroup = new UntypedFormGroup({});

  fieldConfigs: FormlyFieldConfig[] = [
    {
      key: 'items',
      type: 'repeat',
      props: {},
      fieldArray: undefined,
      className: 'block',
    },
  ];

  get showList(): boolean {
    return (this.quickSearchComponent?.props?.hideItemsUntilSearched && this.quickSearchComponent?.hasSearched) ?? true;
  }

  getModel() {
    const results = {
      items: this.items,
    };
    return results;
  }
}
