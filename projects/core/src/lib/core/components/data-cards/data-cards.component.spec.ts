import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { ListFilterService, StateService } from '@nimbus/core/src/lib/core';
import { ListService } from '../../../core/services/list.service';
import { MockListService } from '../../../core/services/list.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { LoadingComponent } from './../../../core/components/loading/loading.component';
import { DataCardsComponent } from './data-cards.component';
import { MockListFilterService } from '../../services/list-filter-service/list-filter.service.spec';

describe('DataCardsComponent', () => {
  let component: DataCardsComponent;
  let fixture: ComponentFixture<DataCardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DataCardsComponent, LoadingComponent],
      imports: [MatDialogModule],
      providers: [
        {
          provide: ListService,
          useClass: MockListService,
        },
        {
          provide: StateService,
          useClass: MockStateService,
        },
        {
          provide: ListFilterService,
          useClass: MockListFilterService,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataCardsComponent);
    component = fixture.componentInstance;
    component.listName = 'Test';

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
