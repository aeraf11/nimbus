import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RichFieldsViewComponent } from './rich-fields-view.component';

describe('RichFieldsViewComponent', () => {
  let component: RichFieldsViewComponent;
  let fixture: ComponentFixture<RichFieldsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RichFieldsViewComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(RichFieldsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
