import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'ni-rich-fields-view',
  templateUrl: './rich-fields-view.component.html',
  styleUrls: ['./rich-fields-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RichFieldsViewComponent {
  constructor() {}

  @Input() isEmpty?: boolean;
  @Input() fieldGroup?: any;
  @Input() formControl?: any;
  getValue(key: any): string {
    return this.formControl.value[key];
  }
}
