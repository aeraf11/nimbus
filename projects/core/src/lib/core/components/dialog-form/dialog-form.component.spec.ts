import { A11yModule } from '@angular/cdk/a11y';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StateService } from '@nimbus/core/src/lib/core';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { EditorMode } from '@nimbus/core/src/lib/core';
import { DialogFormAction } from './../../../core/models/dialog-form-action';
import { DialogFormComponent } from './dialog-form.component';

describe('DialogFormComponent', () => {
  let component: DialogFormComponent;
  let fixture: ComponentFixture<DialogFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DialogFormComponent],
      imports: [ReactiveFormsModule, A11yModule],
      providers: [
        { provide: MatDialogRef, useFactory: () => jasmine.createSpyObj('MatDialogRef', ['close', 'afterClosed']) },
        { provide: MAT_DIALOG_DATA, useValue: {} },
        {
          provide: StateService,
          useClass: MockStateService,
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  // TODO: constructor coverage

  describe('ngOnInit()', () => {
    describe('title', () => {
      it('sets title to "" if config.hideTitle is true', () => {
        // Arrange
        component.config.hideTitle = true;

        // Act
        component.ngOnInit();

        // Assert
        expect(component.title).toEqual('');
      });

      it('sets title to parsed template if config.titleTemplate is set', () => {
        // Arrange
        const someValue = 'someValue';

        component.config.titleTemplate = '${test}';
        component.model = {
          test: someValue,
        };

        // Act
        component.ngOnInit();

        // Assert
        expect(component.title).toEqual(someValue);
      });

      it('sets title to "Add" if no titleTemplate and EditorMode is Add', () => {
        // Arrange
        component.editorMode = EditorMode.Add;

        // Act
        component.ngOnInit();

        // Assert
        expect(component.title).toEqual('Add');
      });

      it('sets title to "Edit" if no titleTemplate and EditorMode is Edit', () => {
        // Arrange
        component.editorMode = EditorMode.Edit;

        // Act
        component.ngOnInit();

        // Assert
        expect(component.title).toEqual('Edit');
      });

      it('sets title to "View" if no titleTemplate by default', () => {
        // Arrange
        component.editorMode = EditorMode.None;

        // Act
        component.ngOnInit();

        // Assert
        expect(component.title).toEqual('View');
      });
    });
  });

  describe('onClose()', () => {
    let stateService: StateService;

    beforeEach(() => (stateService = TestBed.inject(StateService)));

    it('clears and closes form if config.isChildForm is true, regardless of action', () => {
      // Arrange
      spyOn(stateService, 'submitFormClear').and.callFake(() => null);

      const someAction = DialogFormAction.Submit;
      const someModel = { test: 'one' };

      component.config.isChildForm = true;
      component.model = someModel;

      // Act
      component.onClose(someAction);

      // Assert
      expect(stateService.submitFormClear).toHaveBeenCalled();
      expect(component.dialogRef.close).toHaveBeenCalledWith({ action: someAction, model: someModel });
    });

    it('clears and closes form if action is cancel, regardless of config.isChildForm', () => {
      // Arrange
      spyOn(stateService, 'submitFormClear').and.callFake(() => null);

      const someAction = DialogFormAction.Cancel;
      const someModel = { test: 'one' };

      component.config.isChildForm = false;
      component.model = someModel;

      // Act
      component.onClose(someAction);

      // Assert
      expect(stateService.submitFormClear).toHaveBeenCalled();
      expect(component.dialogRef.close).toHaveBeenCalledWith({ action: someAction, model: someModel });
    });

    it('calls submitForm with model, action, formId and context if not a child form and action is not cancel', () => {
      // Arrange
      spyOn(stateService, 'submitFormClear').and.callFake(() => null);
      spyOn(stateService, 'submitForm').and.callFake(() => null);

      const someAction = DialogFormAction.Submit;
      const someModel = { test: 'one' };
      const someFormId = 'someFormId';
      const someContext = { context: 'one' };

      component.config.isChildForm = false;
      component.model = someModel;
      component.config.formId = someFormId;
      component.options = {
        formState: {
          context: someContext,
        },
      };

      // Act
      component.onClose(someAction);

      // Assert
      expect(stateService.submitFormClear).not.toHaveBeenCalled();
      expect(component.dialogRef.close).not.toHaveBeenCalled();
      expect(stateService.submitForm).toHaveBeenCalledWith(someModel, someAction, someFormId, someContext);
    });

    it('submitForm formId falls back to empty string if no formId exists on the config', () => {
      // Arrange
      spyOn(stateService, 'submitFormClear').and.callFake(() => null);
      spyOn(stateService, 'submitForm').and.callFake(() => null);

      const someAction = DialogFormAction.Submit;
      const someModel = { test: 'one' };
      const someContext = { context: 'one' };

      component.config.isChildForm = false;
      component.model = someModel;
      component.options = {
        formState: {
          context: someContext,
        },
      };

      // Act
      component.onClose(someAction);

      // Assert
      expect(stateService.submitFormClear).not.toHaveBeenCalled();
      expect(component.dialogRef.close).not.toHaveBeenCalled();
      expect(stateService.submitForm).toHaveBeenCalledWith(someModel, someAction, '', someContext);
    });
  });

  describe('scrollToBottom()', () => {
    it('sets content nativeElement scrollTop to the nativeElements scrollHeight', () => {
      // Arrange
      component.content.nativeElement.scrollTop = 500;
      const padding = 10;

      // Act
      component.scrollToBottom();

      // Assert
      expect(component.content.nativeElement.scrollTop + padding).toEqual(component.content.nativeElement.scrollHeight);
    });
  });

  describe('scrollToTop()', () => {
    it('sets content nativeElement scrollTop to 0', () => {
      // Arrange
      component.content.nativeElement.scrollTop = 500;

      // Act
      component.scrollToTop();

      // Assert
      expect(component.content.nativeElement.scrollTop).toEqual(0);
    });
  });

  describe('onSelectionChanged()', () => {
    it('flips the isSelected boolean', () => {
      // Arrange
      component.isSelected = true;

      // Act
      component.onSelectionChanged();

      // Assert
      expect(component.isSelected).toEqual(false);
    });

    it('emits selectionChanged with the passed argument', () => {
      // Arrange
      spyOn(component.selectionChanged, 'emit').and.callFake(() => null);

      const someItem = { test: 'one' };

      // Act
      component.onSelectionChanged(someItem);

      // Assert
      expect(component.selectionChanged.emit).toHaveBeenCalledWith(someItem);
    });
  });

  describe('onDialogOpen()', () => {
    it('emits dialogOpen', () => {
      // Arrange
      spyOn(component.dialogOpen, 'emit').and.callFake(() => null);

      // Act
      component.onDialogOpen();

      // Assert
      expect(component.dialogOpen.emit).toHaveBeenCalled();
    });
  });

  describe('onSubmit()', () => {
    let stateService: StateService;

    beforeEach(() => (stateService = TestBed.inject(StateService)));

    it('does not call postForm if there is no formActionKey', () => {
      // Arrange
      spyOn(stateService, 'postForm').and.callFake(() => null);

      component.formActionKey = undefined;
      component.formRouteId = 'someFormRouteId';

      // Act
      component.onSubmit();

      // Assert
      expect(stateService.postForm).not.toHaveBeenCalled();
    });

    it('does not call postForm if there is no formRouteId', () => {
      // Arrange
      spyOn(stateService, 'postForm').and.callFake(() => null);

      component.formActionKey = 'someFormActionKey';
      component.formRouteId = undefined;

      // Act
      component.onSubmit();

      // Assert
      expect(stateService.postForm).not.toHaveBeenCalled();
    });

    it('calls postForm if both formActionKey and formRouteId exist', () => {
      // Arrange
      spyOn(stateService, 'postForm').and.callFake(() => null);

      component.formActionKey = 'someFormActionKey';
      component.formRouteId = 'someRouteId';

      // Act
      component.onSubmit();

      // Assert
      expect(stateService.postForm).toHaveBeenCalled();
    });
  });

  describe('executeAction()', () => {
    const someRootExecuteAction = (action: string, model: unknown) => null;

    it('sets savedModel to model if the action is "saveModel"', fakeAsync(() => {
      // Arrange
      const someAction = 'saveModel';
      const someModel = { test: 'one' };

      component.model = someModel;
      component.savedModel = null;

      // Act
      component.executeAction(someRootExecuteAction, someAction, someModel);
      tick(1000);

      // Assert
      expect(component.savedModel).toEqual(someModel);
    }));

    it('sets model to savedModel if action is "resetModel"', fakeAsync(() => {
      // Arrange
      const someAction = 'resetModel';
      const someSavedModel = {};

      component.model = null;
      component.savedModel = someSavedModel;

      // Act
      component.executeAction(someRootExecuteAction, someAction, {});
      tick(1000);

      // Assert
      expect(component.model).toEqual(someSavedModel);
    }));

    // SpyOn function as an argument?
    xit('calls the rootExecuteAction function if the action is not "saveModel" or "resetModel"', () => {
      // Arrange
      const executeSpy = jasmine.createSpy('someRootExecuteAction').and.callThrough();
      const someAction = 'anything else';

      // Act
      component.executeAction(someRootExecuteAction, someAction, {});

      // Assert
      expect(executeSpy).toHaveBeenCalled();
    });
  });
});
