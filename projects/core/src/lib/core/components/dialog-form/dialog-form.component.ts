import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { DialogFormAction, FormsHelperService, StateService } from '@nimbus/core/src/lib/core';
import { deepClone } from 'deep.clone';
import { Subject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { DialogFormData, EditorMode, DialogFormConfig } from '@nimbus/core/src/lib/core';

@UntilDestroy()
@Component({
  selector: 'ni-dialog-form',
  templateUrl: './dialog-form.component.html',
  styleUrls: ['./dialog-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DialogFormComponent implements OnInit {
  @ViewChild('content') content!: ElementRef;

  @Input() hasNextItem?: boolean;
  @Input() hasPreviousItem?: boolean;
  @Input() model: any = {};
  @Input() loading = false;
  @Input() isSelected = false;
  @Input() formActionKey?: string;
  @Input() formRouteId?: string;

  @Output() nextItem = new EventEmitter();
  @Output() previousItem = new EventEmitter();
  @Output() selectionChanged = new EventEmitter<any | void>();
  @Output() dialogOpen = new EventEmitter<void>();

  title = '';
  form: UntypedFormGroup = new UntypedFormGroup({});
  options: FormlyFormOptions | undefined = {};
  editorMode?: EditorMode;
  DialogFormAction = DialogFormAction;
  EditorMode = EditorMode;
  fieldConfig!: FormlyFieldConfig[];
  errorMessage!: string | null | undefined;
  savedModel: unknown = {};
  dialogFormChanged = new Subject<void>();
  debug = false;
  buttonType = 'raised';
  config: DialogFormConfig = {
    titleTemplate: '',
    enableDelete: true,
    enableEdit: true,
    saveText: 'Save',
    deleteText: 'Delete',
    cancelText: 'Cancel',
    hideText: 'Hide',
    addText: 'Add',
    isChildForm: true,
  };

  constructor(
    public dialogRef: MatDialogRef<DialogFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogFormData,
    public change: ChangeDetectorRef,
    private state: StateService,
    private helper: FormsHelperService
  ) {
    this.options = {
      ...this.options,
      formState: { ...this.options?.formState, helper },
    };

    if (data.config) this.config = { ...this.config, ...data.config };

    this.editorMode = data.editorMode ?? (data.model ? EditorMode.Edit : EditorMode.Add);

    if (data.buttonType) {
      this.buttonType = data.buttonType;
    }

    if (data.options) {
      this.options = {
        ...this.options,
        formState: {
          ...this.options?.formState,
          executeAction: (action: string, model: unknown) =>
            this.executeAction(data.options?.formState.executeAction, action, model),
          rootModel: data.options.formState.rootModel,
          context: data.options.formState.context,
          contextChanged: data.options.formState.contextChanged,
          isAdd: this.editorMode === EditorMode.Add,
          isEdit: this.editorMode === EditorMode.Edit,
          isSelect: this.editorMode === EditorMode.Select,
          isSelected: () => this.isSelected,
          selectionChanged: (item?: any) => this.onSelectionChanged(item),
          dialogFormChanged: () => this.dialogFormChanged,
          runChangeDetection: () => this.runChangeDetection(),
        },
      };
      change.markForCheck();
    }

    this.hasNextItem = !!data.hasNextItem;
    this.hasPreviousItem = !!data.hasPreviousItem;
    this.isSelected = !!data.isSelected;
    this.formActionKey = data.formActionKey;
    this.formRouteId = data.formRouteId;

    if (data.model) {
      deepClone<any>(data.model).then((clone) => {
        this.model = clone;
        change.markForCheck();
      });
      deepClone<any>(data.model).then((clone) => {
        this.savedModel = clone;
      });
    }

    if (data.fieldConfig) {
      deepClone<any>(data.fieldConfig).then((clone) => {
        this.fieldConfig = clone;
        change.markForCheck();
      });
    }

    if (data.disabled) this.form.disable();
    else this.form.enable();

    this.state
      .selectFormSubmissionResult()
      .pipe(
        untilDestroyed(this),
        filter((result) => result !== null)
      )
      .subscribe((result) => {
        if (result?.success) {
          this.state.submitFormClear();
          this.dialogRef.close();
        } else {
          this.errorMessage = result?.errorMessage;
          change.markForCheck();
          this.scrollToTop();
        }
      });

    state
      .selectDebugOptions()
      .pipe(untilDestroyed(this))
      .subscribe((debugOptions) => (this.debug = debugOptions?.debugForms ?? false));
  }

  ngOnInit(): void {
    this.title = this.getTitle();
  }

  onClose(action: DialogFormAction): void {
    if (this.config.isChildForm || action === DialogFormAction.Cancel) {
      this.state.submitFormClear();
      this.dialogRef.close({ action, model: this.model });
    } else {
      this.state.submitForm(this.model, action, this.config.formId || '', this.options?.formState.context);
    }
  }

  scrollToBottom(): void {
    this.content.nativeElement.scrollTop = this.content.nativeElement.scrollHeight;
  }

  scrollToTop(): void {
    this.content.nativeElement.scrollTop = 0;
  }

  onSelectionChanged(item?: unknown): void {
    this.isSelected = !this.isSelected;
    this.selectionChanged.emit(item);
  }

  onDialogOpen(): void {
    this.dialogOpen.emit();
  }

  onSubmit(): void {
    if (this.formActionKey && this.formRouteId) {
      this.state.postForm(
        {
          model: this.model,
          action: this.formActionKey,
          context: this.options?.formState?.context ?? {},
        },
        this.formRouteId,
        () => this.dialogRef.close({ action: DialogFormAction.Submit, model: this.model })
      );
    }
  }

  executeAction(rootExecuteAction: (action: string, model: unknown) => void, action: string, model: unknown) {
    if (action === 'saveModel') {
      deepClone<any>(this.model).then((clone) => {
        this.savedModel = clone;
      });
    } else if (action === 'resetModel') {
      deepClone<any>(this.savedModel).then((clone) => {
        this.model = clone;
      });
    } else if (rootExecuteAction) {
      rootExecuteAction(action, model);
    }
  }

  private runChangeDetection(): void {
    this.dialogFormChanged.next();
    this.change.markForCheck();
    this.change.detectChanges();
  }

  private getTitle(): string {
    if (this.config.hideTitle) {
      return '';
    }

    if (this.config.title) {
      return this.config.title;
    }

    if (this.config.titleTemplate) {
      return this.helper.parseTemplate(this.config.titleTemplate, this.model, '');
    }

    if (this.editorMode === EditorMode.Add) {
      return 'Add';
    }

    if (this.editorMode === EditorMode.Edit) {
      return 'Edit';
    }

    return 'View';
  }
}
