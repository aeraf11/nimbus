export enum DataType {
  text = 0,
  date = 1,
  dateTime = 2,
  multiChoice = 3,
  lookup = 4,
  lookupHierarchical = 5,
  selectOption = 6,
  profile = 7,
  status = 8,
  icon = 9,
  messages = 10,
  blobView = 11,
  entityReference = 12,
  entityMedia = 13,
  iconText = 14,
  dateRelative = 15,
  lookupWithFallback = 16,
  lookupHierarchicalWithFallback = 17,
  dateTimeNoTz = 18,
  dateTimeWithSeconds = 19,
  dateTimeWithSecondsNoTz = 20,
}

export const isDataTypeMatch = (type: DataType, value: string | number | DataType | undefined): boolean => {
  return type === value || (<any>DataType)['' + value] === type;
};

export const convertToDataType = (value: unknown): DataType => {
  if (typeof value === 'number') {
    return value;
  } else {
    return (<any>DataType)['' + value];
  }
};
