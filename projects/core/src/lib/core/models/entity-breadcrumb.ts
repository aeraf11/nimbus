// Pascal case because that is what is saved in the database
export interface EntityBreadcrumb {
  EntityType: number;
  EntityId: string;
  EntityIdField: string;
  EntityTypeName: string;
  Description: string;
  Url: string;
  CssClass: string;
  NavKey: string;
  IsVisible: boolean;
  icon?: string;
}
