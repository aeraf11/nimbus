export class FormActionsItem {
  label: string | undefined;
  ariaLabel: string | undefined;
  color: string | undefined;
  disabled: boolean | undefined;
}
