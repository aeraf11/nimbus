export interface User {
  userProfileId: string;
  fullName: string;
  groupName: string;
  email: string;
  contactNumber: string;
  branch: string;
  permissions: string[];
  dateFormat: string;
  timeFormat: string;
  thumbnailBlobId: string;
  initials: string;
  timeZoneName?: string;
  timeZoneOffset?: string;
  rank?: string;
  firstName?: string;
  surname?: string;
  location?: string;
  exhibitCheckInLocationId?: string;
  exhibitCheckOutLocationId?: string;
  forceAgencyLookupId: string;
  timeZoneId: string;
}
