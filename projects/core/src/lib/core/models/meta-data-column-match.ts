export interface MetaDataColumnMatch {
  uuid: string;
  columnName: string;
  entityTypeLabelId: string;
}
