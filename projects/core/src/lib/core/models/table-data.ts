export interface TableData {
  rows: Array<any>;
  rowCount: number;
  filteredRowCount: number;
}
