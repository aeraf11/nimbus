export interface QueuedAttachment {
  id: string;
  fileId: string;
  entityId: string;
  entityTypeId: number;
  description: string;
  item?: any;
}
