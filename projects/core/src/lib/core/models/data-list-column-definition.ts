import { DataType } from '@nimbus/core/src/lib/core';

export interface DataListColumnDefinition {
  id: string;
  value: string;
  dataType?: DataType;
  isHidden: boolean;
  isSortable: boolean;
  minWidth: string;
  maxWidth: string;
  tab?: string;
  tabs?: string;
}
