export enum EditorMode {
  Add,
  Edit,
  Select,
  View,
  Form,
  None,
}
