﻿export interface Translations {
  [contextKey: string]: {
    [token: string]: string;
  };
}
