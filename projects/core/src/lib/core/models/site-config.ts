export class SiteConfig {
  apiUrl: string;
  initUrl: string;

  constructor(apiUrl: string, initUrl: string) {
    this.apiUrl = apiUrl;
    this.initUrl = initUrl;
  }
}
