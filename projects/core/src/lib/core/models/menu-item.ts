export interface MenuItem {
  items?: MenuItem[] | null;
  name: string;
  route?: string;
  routeId?: string | null;
  navigateFromRootOrigin?: boolean | undefined;
  menuId?: string | null;
  icon?: string;
  permissionKeys?: string[];
  scrollId?: string;
  queryParams?: any;
}
