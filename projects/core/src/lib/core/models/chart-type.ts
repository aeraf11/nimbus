export enum ChartType {
  Pie,
  Donut,
  Funnel,
  Pyramid,
  Bar,
  Line,
  CircularGauge,
}
