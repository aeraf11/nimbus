export interface FileRecombinationProgressChanged {
  fileId: string;
  recombinationId: string;
  progress: number;
  failed: boolean;
  errorMessage: string;
}
