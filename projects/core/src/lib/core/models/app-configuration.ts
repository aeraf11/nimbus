import { TimeZone } from '@nimbus/material';
import { DebugOptions, FileUploadConfig, ScheduledJob, ServiceWorkerConfig, Theme, ApplicationInsightsConfig } from '.';
import { IndexedDbConfig } from './../services/indexeddb.service';
import { PermissionLogic } from './permission-logic';

export interface AppConfiguration {
  oidc?: OidcConfiguration;
  homeConfig?: HomeItemConfig[];
  menuConfig?: MenuItemConfig[];
  staticRoutes?: RouteConfig[];
  timeZones?: TimeZone[];
  themeConfig?: Theme[];
  fileUploadConfig?: FileUploadConfig;
  debugOptions?: DebugOptions;
  serviceWorkerConfig?: ServiceWorkerConfig;
  syncfusionLicenseKey?: string;
  indexedDbConfig?: IndexedDbConfig;
  workOnlinePromptTimeout?: number;
  enableFormTranslations?: boolean;
  siteId?: string;
  scheduledJobs?: ScheduledJob[];
  offlineCachedRoutes?: string[];
  applicationInsightsConfig?: ApplicationInsightsConfig;
  defaultEntityMetaData?: number[];
  metaDataCacheRefreshInterval?: number;
  signalRHubs?: SignalRHub[];
  formStateSettings?: { [key: string]: any };
}

// NOTE duplicated over in projects\core\src\lib\auth\reducers\auth.reducer.ts ngrx hack
export class OidcConfiguration {
  stsServer?: string;
  clientId?: string;
  scope?: string;
  responseType?: string;
  silentRenew?: boolean;
  renewTimeBeforeTokenExpiresInSeconds?: number;
  logLevel?: number;
}

// NOTE duplicated over in projects\core\src\lib\apto\models ngrx hack
export interface MenuItemConfig {
  icon: string;
  items?: MenuItemConfig[] | null;
  name: string;
  routeId: string | null;
  queryParams: any;
}

// NOTE duplicated over in projects\core\src\lib\apto\models ngrx hack
export interface HomeItemConfig {
  name: string;
  icon: string;
  routeId: string;
  separator?: boolean;
  standalone?: boolean;
}

// NOTE duplicated over in projects\core\src\lib\apto\reducers\form.reducer.ts ngrx hack
export interface RouteConfig {
  permissionKeys?: string[];
  permissionLogic?: PermissionLogic;
  route: string;
  routeId: string;
  navigateFromRootOrigin?: boolean | undefined;
  forceAgencyPermissions: string[];
  showOnline: boolean;
  showOffline: boolean;
  acceptanceGuardConfig?: any;
}

// NOTE duplicated over in projects\core\src\lib\apto\models ngrx hack
export interface SignalRHub {
  name: string;
  url: string;
  events: string[];
}
