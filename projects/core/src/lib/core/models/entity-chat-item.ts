export interface EntityChatItem {
  messageHtml: string;
  entityTypeId: number;
  entityId: string;
  files: any;
}
