export interface CircularGaugeRange {
  start: number;
  end: number;
  color: string;
  radius: string;
}

export interface CircularGaugePointer {
  value: number;
  type: string;
  pointerWidth: string;
  radius: string;
  color: string;
  text: string;
}

export interface CircularChartLegend {
  visible: boolean;
  width: string;
  height: string;
  titleProportion: number;
  titleSize: string | undefined;
  subTitleProportion: number;
  subTitleSize: string | undefined;
  padding: string;
}
