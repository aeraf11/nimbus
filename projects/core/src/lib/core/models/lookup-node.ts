import { Lookup } from './lookup';

export interface LookupNode {
  children?: LookupNode[];
  item: Lookup;
}
