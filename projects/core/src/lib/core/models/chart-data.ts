import { ChartDataAccumulation } from './chart-data-accumlation';
import { ChartDataSimple } from './chart-data-simple';

export type ChartData = ChartDataAccumulation | ChartDataSimple | undefined;
