export interface FileUploadConfig {
  allowedExtensions: string;
  asyncSettings: {
    saveUrl: string;
    removeUrl: string;
    chunkSize: number;
    retryCount: number;
    retryAfterDelay: number;
  };
  autoUpload: boolean;
  locale: string;
  maxFileSize: number;
  minFileSize: number;
  multiple: boolean;
  sequentialUpload: boolean;
  showFileList: boolean;
  template: string;
}
