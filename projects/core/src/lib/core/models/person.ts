export interface Person {
  fullName: string;
  initials?: string;
  collarNo?: string;
  contactNo?: string;
  email?: string;
  photo?: string;
  photoId?: string;
  jobDescription?: string;
}
