export interface TableTypeColumnDefinition {
  key: string;
  columnName: string;
  isEditorHeader?: boolean;
  dataType?: string;
  tab?: string;
  computedValue?: string;
  expressionValue?: string;
}
