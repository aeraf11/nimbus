export interface Theme {
  label: string;
  id: string;
  href: string;
  isDark: boolean;
}
