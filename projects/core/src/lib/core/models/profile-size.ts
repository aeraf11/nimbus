export type ProfileSize = 'xsmall' | 'small' | 'medium' | 'large' | 'xlarge';
