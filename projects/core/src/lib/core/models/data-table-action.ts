export interface DataTableAction {
  ariaLabel?: string;
  disabled?: string;
  disabledExpression?: string;
  disabledTitle?: string;
  disabledTitleExpression?: string;
  download?: {
    attachmentId?: string;
    logAction?: { entityId: string; entityTypeId: number; action: string };
    blobId?: string;
    fileName?: string;
  };
  fallback?: string;
  formAction?: string;
  key: string;
  name: string;
  params?: any;
  permissions?: string[];
  showErrorSnack?: boolean;
  successExpression?: string;
  url?: string;
  visibility?: string;
  visibilityExpression?: string;
}
