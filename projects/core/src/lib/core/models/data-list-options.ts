import { FilterColumn } from './filter-column';

export interface DataListOptions {
  id?: string;
  sortColumn: string;
  sortDirection: string;
  quickSearch: string;
  pageIndex: number;
  pageSize: number;
  filterColumns?: FilterColumn[];
  additionalParams?: { [key: string]: string };
}
