import { CustomField } from './custom-field';

export interface LookupDetail {
  lookupKey: string;
  lookupId: string;
  displayValue: string;
  entityType?: number;
  score?: number;
  cssClass?: string;
  iconClassNames?: string;
  fontCssClass?: string;
  isDefault?: boolean;
  parentLookupId?: string;
  hierarchicalDisplayValue: string;
  customFields?: CustomField[];
  isNew?: boolean;
  shortCode?: string;
}
