export interface Lookup {
  displayValue: string;
  hierarchicalDisplayValue: string;
  lookupId: string;
  lookupKey: string;
  level: number;
  entityType?: number;
  cssClass?: string;
  parentLookupId?: string;
  isNew?: boolean;
  isLeaf?: boolean;
  shortCode?: string;
  prefix?: string;
  isTopLevel?: boolean;
}
