export interface CardOptions {
  id: string;
  isCardCollapsed: boolean;
}
