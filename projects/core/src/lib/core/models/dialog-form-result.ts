import { DialogFormAction } from '@nimbus/core/src/lib/core';
export interface DialogFormResult {
  model: any;
  action: DialogFormAction;
}
