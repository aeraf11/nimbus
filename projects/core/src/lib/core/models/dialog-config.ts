import { DialogType } from './dialog-type';

export interface DialogConfig {
  title: string;
  bodyText: string;
  type: DialogType;
}
