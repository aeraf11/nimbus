export interface FileChunk {
  blob: Blob;
  id: number;
  fileId: string;
  uploadId: string;
}
