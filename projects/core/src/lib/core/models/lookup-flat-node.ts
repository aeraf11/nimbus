import { Lookup } from './lookup';

export class LookupFlatNode {
  item!: Lookup;
  level!: number;
  expandable!: boolean;
}
