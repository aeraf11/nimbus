import { RouteConfig } from './app-configuration';

export interface DynamicRoutePageBanner {
  bannerImageUrl?: string; // Deprecated
  bannerLogoUrl?: string; // Deprecated
  bannerLogoAlt?: string; // Deprecated
  backgroundSize?: string; // Deprecated
  backgroundPosition?: string; // Deprecated
  height?: string; // Deprecated
  marginBottom?: string; // Deprecated

  background?: {
    image: string;
    darkImage?: string;
    position?: string;
    size?: string;
    height?: string;
    marginBottom?: string;
  };
  logo?: {
    image: string;
    darkImage?: string;
    altText?: string;
    width?: string;
    height?: string;
  };
}

export interface DynamicRoute extends RouteConfig {
  formId: string;
  pageTitle: string;
  cssClass?: string;
  modelId?: string;
  readId?: string;
  writeId?: string;
  header?: string;
  pageBanner?: any;
  hidePageTitle?: boolean;
  showOnline: boolean;
  showOffline: boolean;
  guardConfig?: AcceptanceGuardConfig;
}
export interface AcceptanceGuardConfig {
  storageKey: string | null;
  returnUrl: string;
  frequency: 'always' | 'session';
}
