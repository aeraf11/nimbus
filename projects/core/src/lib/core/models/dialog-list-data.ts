export interface DialogListData {
  items: any[];
  count: number;
  icon: string;
  listName: string;
}
