import { ChartDataItem } from './chart-data-item';

export interface ChartDataSimple {
  data: ChartDataItem[];
}
