import { FormlyFormOptions } from '@ngx-formly/core';
import { DialogFilterConfig } from './dialog-filter-config';

export interface DialogFilterData {
  config: DialogFilterConfig;
  disabled: boolean;
  options?: FormlyFormOptions;
}
