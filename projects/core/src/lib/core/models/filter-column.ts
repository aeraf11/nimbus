export interface FilterColumn {
  column: string;
  columnAlias: string;
  stringFilter: string;
  constantFilter: boolean;
  contextKey?: string;
  entityTypeId?: number;
  displayValue?: string;
  rawFilter?: any;
  userCreated?: boolean;
  isJsonFilter?: boolean;
  isInitialContext?: boolean;
  isExactMatchOverride?: boolean;
  defaultValue?: any;
}
