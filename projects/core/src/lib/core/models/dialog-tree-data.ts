import { Observable } from 'rxjs';
import { Lookup } from './lookup';
import { LookupNode } from './lookup-node';

export interface DialogTreeData {
  items: Observable<LookupNode[] | null>;
  selectedItems: Lookup[];
  addCustom: boolean;
  placeholder: string;
  newItemLabel: string;
  disabled: boolean;
  okText: string;
  cancelText: string;
  leafSelection: boolean;
}
