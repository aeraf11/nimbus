import { EntitySource } from './entity-source';

export interface DocumentUploadModel {
  source: EntitySource;
  id: string;
  fileData: string;
  reason: string;
  checkIn: boolean;
}
