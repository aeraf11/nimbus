export interface ChartDataItem {
  name: string;
  value: number;
  colour?: string;
  url?: string;
}
