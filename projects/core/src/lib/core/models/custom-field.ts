export interface CustomField {
  name: string;
  value: string;
}
