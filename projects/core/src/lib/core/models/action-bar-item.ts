import { ActionType } from './action-type';

export class ActionBarItem {
  Label: string | undefined;
  Url: string | undefined;
  AriaLabel: string | undefined;
  ActionType: ActionType = ActionType.Route;
  FormId: string | undefined;
  ItemContext: unknown | undefined;
}
