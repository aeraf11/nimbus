export interface DataSourceSimple {
  table1: {
    results: { name: string; value: number; cssClass: string; url: string }[];
  };
  results: { name: string; value: number; cssClass: string; url: string }[];
}
