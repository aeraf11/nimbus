import { MetaDataItem } from './meta-data-item';

export interface MetaDataCollection {
  Items?: MetaDataItem[];
}
