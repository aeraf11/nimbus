export interface ApplicationInsightsConfig {
  instrumentationKey: string;
  customerName: string;
  applicationName: string;
  loggingLevelConsole: number;
  enableDebugExceptions: boolean;
  enableAutoRouteTracking: boolean;
  version: string;
}
