import { LookupNode } from './lookup-node';

export type LookupHierarchy = { [key: string]: Array<LookupNode> } | null;
