export interface HomeItem {
  name: string;
  icon: string;
  route: string;
  separator?: boolean;
  standalone?: boolean;
}
