export interface ButtonListButton {
  text: string;
  value: string;
  isHidden?: boolean;
}
