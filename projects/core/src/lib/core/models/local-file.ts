export interface LocalFile {
  id: string;
  file: { name: string; size: number; type: string };
  status: string;
  progressCurrent?: number;
  progressMax?: number;
}
