// Note MetaData comes in blobs from the db and does not conform to the convention of camel case :'(  .
export interface MetaDataItem {
  Id: number;
  _GroupName: string | null;
  Sequence: number;
  Label: string;
  Placeholder: string | null;
  Value: string | null;
  ValueData?: string | null;
  ItemType: MetaDataItemType;
  LookupTable?: string | null;
  LookupUrl?: string | null;
  LookupRefreshKey?: string | null;
  Choices?: MetaDataChoice[];
  IsRequiredOnStart?: boolean;
  IsRequiredOnComplete?: boolean;
  RelatedUrl?: MetaDataRelatedUrl;
  Instructions?: string | null;
  PreventFutureDates?: boolean;
  PreventPastDates?: boolean;
  AddEntityCalendarItem?: boolean;
  ForceUpdateOnSaveEntityCalendarItem?: boolean;
  EntityCalendarItemPriorityId?: number | string | null;
  EntityCalendarItemTypeId?: number | string | null;
  EntityCalendarItemRequiresCompletion?: boolean;
  EntityCalendarItemId?: number | string | null;
  IsVisibleOnOverview?: boolean;
  IsVisibleInPhanerosAdd?: boolean;
  IsVisibleInPhanerosView?: boolean;
  IsDefaultToNow?: boolean;
  HasDefaultBeenSet?: boolean;
  Conditional?: MetaDataCondition[];
  IsMandatory?: boolean;
  CannotBeRemoved?: false;
  ValidationExpression?: unknown;
  Mask?: string | null;
  WriteOnceReadMany?: boolean;
  CreateNoteEntry?: boolean;
  NoIndex?: boolean;
  EntityTypeId?: number | null;
  CssClass?: string | null;
  RelatesToAllowedEntityTypes?: unknown[];
  ItemTypeVersion?: number;
  DisplayGroupName?: string | null;
  SourceEntityTypeId?: number | null;
  SourceEntityId?: string | null;
  AssetSettings?: unknown;
  AddressSettings?: unknown;
  PersonSettings?: unknown;
  UserSettings?: unknown;
  RichTextSettings?: unknown;
  PickAndUpdateSettings?: unknown;
  MovementSettings?: unknown;
  AutoCollapse?: boolean;
  OldLabel?: string | null;
  ShowSeconds?: boolean;
}

export interface MetaDataChoice {
  Label?: string | null;
  Value?: string | null;
  ValueData?: null;
}

interface MetaDataRelatedUrl {
  Text: string | null;
  Url: string | null;
  Action: string | null;
}

export enum MetaDataItemType {
  Text,
  Lookup,
  Choice,
  MultiChoice,
  ConfigHidden,
  TextArea,
  ChoiceSimple,
  Date,
  DateTime,
  ReadOnly,
  File,
  ImageChoice,
  TextPanel,
  YesNo,
  Checkbox,
  DateRange,
  RelatesTo,
  Signature,
  RichText,
  DisplayGroup,
  Password,
  Asset,
  TextWithBarcode,
  MediaUpload,
  AttachmentUpload,
  BinarySize,
  SubmissionSelection,
  Address,
  Assets,
  User,
  Person,
  EntityMediaUpload,
  PickAndUpdate,
  EntitySketch,
  Movement,
}

export interface MetaDataCondition {
  PropertyName: string;
  PropertyValue: string;
  LogicType: MetaDataConditionLogic;
  PropertyOnModel: boolean;
}

export const enum MetaDataConditionLogic {
  Equal,
  NotEqual,
  LessThan,
  GreaterThan,
}

export interface UngroupedMetaData {
  buildGroups: boolean;
  items: MetaDataItem[];
}

export const DefaultUngroupedMetaData = {
  buildGroups: false,
  items: [],
};
