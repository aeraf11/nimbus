import { DialogTreeAction } from './dialog-tree-action';
import { Lookup } from './lookup';

export interface DialogTreeResult {
  action: DialogTreeAction;
  items: Lookup[];
}
