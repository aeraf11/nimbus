import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';

export interface FormDefinition {
  fields: FormlyFieldConfig[];
  options: FormlyFormOptions;
  id: string;
}
