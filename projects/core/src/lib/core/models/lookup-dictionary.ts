import { Lookup } from './lookup';

export type LookupDictionary = { [key: string]: Array<Lookup> } | null;
