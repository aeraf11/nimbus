export interface ChartDataAccumulation {
  data: { name: string; value: number; colour?: string; url?: string }[];
  total: number;
}
