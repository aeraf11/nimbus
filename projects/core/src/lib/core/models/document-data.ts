import { EntitySource } from './entity-source';

export interface DocumentData {
  source: EntitySource;
  id: string;
  fileName: string;
  fileType: string;
  fileData: string;
}
