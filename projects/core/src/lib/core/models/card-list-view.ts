export interface CardListView {
  label: string;
  type: string;
  icon: string;
}
