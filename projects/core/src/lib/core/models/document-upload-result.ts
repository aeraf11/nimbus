import { EntitySource } from './entity-source';

export interface DocumentUploadResult {
  source: EntitySource;
  id: string;
  fileName: string;
}
