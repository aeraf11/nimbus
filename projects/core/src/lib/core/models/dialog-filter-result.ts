import { DialogFilterAction, FilterColumn } from '@nimbus/core/src/lib/core';

export interface DialogFilterResult {
  action: DialogFilterAction;
  quickSearch: string;
  filterColumns?: FilterColumn[] | undefined;
}
