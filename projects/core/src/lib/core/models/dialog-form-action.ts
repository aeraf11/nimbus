export enum DialogFormAction {
  Add,
  Edit,
  Delete,
  Cancel,
  Submit,
  Hide,
}
