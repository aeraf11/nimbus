import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';
import { EditorMode } from '../models';
import { DialogFormConfig } from './dialog-form-config';

export interface DialogFormData {
  fieldConfig: FormlyFieldConfig[] | undefined | null;
  model: any;
  context?: any;
  config: DialogFormConfig;
  options?: FormlyFormOptions;
  disabled: boolean;
  editorMode?: EditorMode;
  hasNextItem?: boolean;
  hasPreviousItem?: boolean;
  isSelected?: boolean;
  formActionKey?: string;
  formRouteId?: string;
  buttonType?: 'raised' | 'stroked';
  formClass?: string;
}
