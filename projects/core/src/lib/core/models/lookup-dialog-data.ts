export interface LookupDialogData {
  title: string;
  key: string;
  value: string;
  leafSelection: boolean;
  addDefaultOption: boolean;
  lookupFilters: string[];
}
