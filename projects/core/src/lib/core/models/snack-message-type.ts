export enum SnackMessageType {
  Error,
  Info,
  Warn,
}
