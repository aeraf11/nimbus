export interface DialogFormConfig {
  titleTemplate?: string;
  saveText?: string;
  cancelText?: string;
  deleteText?: string;
  addText?: string;
  enableDelete: boolean;
  enableEdit: boolean;
  isChildForm: boolean;
  formId?: string;
  titleIcon?: string;
  hideTitle?: boolean;
  enableHide?: boolean;
  hideText?: string;
  title?: string;
  addIcon?: string;
  cancelIcon?: string;
}
