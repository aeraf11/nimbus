export interface ServiceWorkerConfig {
  showUpdateMessage: boolean;
  updateMessage: string;
  updateMessageTitle: string;
  updateCheckInterval: number;
}
