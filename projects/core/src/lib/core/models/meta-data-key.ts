export interface MetaDataKey {
  uuid: string;
  entityTypeId: number;
  label: string;
  entityType: string;
  entityTypeLabelId: string;
}
