import { Time } from '@nimbus/material';

export interface ScheduledJob {
  id: string;
  isEnabled: boolean;
  defaultTime: Time;
  userTime?: Time;
  interval: number;
  options: unknown;
}
