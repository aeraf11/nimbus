export interface MetaDataAttachment {
  AttachmentId: string;
  LinkedAttachmentId: string;
  Description: string;
}

export interface MetaDataAttachmentResponse {
  attachmentId: string;
  linkedAttachmentId: string;
  description: string;
}
