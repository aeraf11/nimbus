export interface QueueConfig {
  queueTableName: string;
  itemTableName: string;
  entityId?: string;
  entityTypeId?: string;
  description?: string;
  itemIdExpression?: string;
  useLocalModel?: boolean;
  editUrl?: boolean;
  parentEntityId?: string;
}
