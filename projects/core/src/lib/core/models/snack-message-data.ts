import { SnackMessageType } from './snack-message-type';
export interface SnackMessageData {
  type: SnackMessageType;
  message: string;
  action: string;
}
