export interface DataSourceAccumulation {
  table1: {
    results: { name: string; value: number; cssClass: string; url: string }[];
  };
  table2: {
    results: { total: number }[];
  };
}
