import { FilterColumn } from '@nimbus/core/src/lib/core';
export interface DialogFilterConfig {
  title: string;
  cancelText: string;
  filterText: string;
  clearText: string;
  columnDefinitions: any;
  lastFilters: FilterColumn[] | undefined;
  isDisabled?: boolean;
  width?: string;
}
