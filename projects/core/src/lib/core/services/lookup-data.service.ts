import { SiteConfig } from '../models';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { LookupDetail } from '../models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LookupDataService {
  private lookupUrl: string;

  constructor(private httpClient: HttpClient, private siteConfig: SiteConfig) {
    this.lookupUrl = `${siteConfig.apiUrl}/fulllookup`;
  }

  loadLookups(): Observable<LookupDetail[]> {
    return this.httpClient.get<LookupDetail[]>(this.lookupUrl);
  }
}
