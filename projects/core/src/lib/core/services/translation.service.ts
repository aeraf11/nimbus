﻿import { Injectable } from '@angular/core';
import { StateService, Translations } from '@nimbus/core/src/lib/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class TranslationService {
  translationState: Translations = {};
  translationError = 'ERROR: Incorrect context';

  constructor(private stateService: StateService) {
    this.stateService
      .selectAllTranslations()
      .pipe(untilDestroyed(this))
      .subscribe((translations: Translations): void => {
        this.translationState = translations;
      });

    globalThis.$getTranslation = (contextKey: string, token: string): string => this.getTranslation(contextKey, token);
  }

  getTranslation(contextKey: string, token: string): string {
    const context = this.translationState[contextKey || '_root'];

    if (!context) {
      console.warn(this.translationError);
      return this.translationError;
    }

    return context[token] || token;
  }
}
