import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { ApplicationRef, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StorageType, SiteConfig } from '../models';
import { FileRecombinationProgressChanged } from '../models/file-recombination-progress-change';
import { v4 as uuidv4 } from 'uuid';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class FileService {
  private chunkUrl: string;
  private recombineUrl: string;
  private recombineQueueUrl: string;
  private recombineProgressUrl: string;
  private fileDownloadUrl: string;
  private fileName: string = uuidv4();

  constructor(private httpClient: HttpClient, private siteConfig: SiteConfig, private appRef: ApplicationRef) {
    this.chunkUrl = `${siteConfig.apiUrl}/file`;
    this.recombineUrl = `${siteConfig.apiUrl}/file/chunks`;
    this.recombineQueueUrl = `${siteConfig.apiUrl}/file/recombine`;
    this.recombineProgressUrl = `${siteConfig.apiUrl}/file/progress`;
    this.fileDownloadUrl = `${siteConfig.apiUrl}/file/download`;
  }

  async downloadAttachmentById(
    ids: string | string[],
    logAction?: { entityId: string; entityTypeId: number; action: string }
  ): Promise<void> {
    const content = {
      ids: Array.isArray(ids) ? ids.join(',') : ids,
      downloadLogEntityId: logAction?.entityId,
      downloadLogEntityTypeId: logAction?.entityTypeId,
      logAction: logAction?.action,
    };
    this.httpClient
      .post(this.fileDownloadUrl, content, { responseType: 'blob', observe: 'response' })
      .pipe(untilDestroyed(this))
      .subscribe((response) => {
        this.handleDownloadResponse(response);
      });
  }

  handleDownloadResponse(response: HttpResponse<Blob>): void {
    const contentDispositionHeader = response.headers.get('Content-Disposition');
    if (contentDispositionHeader) {
      this.fileName = this.getFilenameFromContentDisposition(contentDispositionHeader);
    }
    if (response.body) {
      this.triggerFileDownload(response.body, this.fileName);
    }
  }

  getFilenameFromContentDisposition(contentDispositionHeader: string): string {
    const matches = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/.exec(contentDispositionHeader);
    if (matches != null && matches[1]) {
      return matches[1].replace(/['"]/g, '');
    }
    return this.fileName;
  }

  triggerFileDownload(data: Blob, fileName: string): void {
    const blob = new Blob([data], { type: data.type });
    const anchor = document.createElement('a');

    anchor.href = window.URL.createObjectURL(blob);
    anchor.download = fileName;
    document.body.appendChild(anchor);
    anchor.click();

    // Clean up
    document.body.removeChild(anchor);
    window.URL.revokeObjectURL(anchor.href);
  }

  uploadBlob(
    blob: Blob,
    fileName: string,
    isChunk = false,
    storageType = StorageType.Attachment
  ): Observable<{ success: boolean; fileId: string; errorMessage: string }> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': <any>undefined,
      }),
    };
    const formData = new FormData();
    formData.append('UploadFiles', blob, fileName);
    formData.append('isChunk', isChunk + '');
    formData.append('storageType', storageType + '');
    return this.httpClient.post<{ success: boolean; fileId: string; errorMessage: string }>(this.chunkUrl, formData);
  }

  recombine(
    fileName: string,
    chunkIds: string[],
    storageType = StorageType.Attachment
  ): Observable<{ fileId: string; success: boolean; errorMessage: string }> {
    return this.httpClient.post<{ fileId: string; success: boolean; errorMessage: string }>(
      this.recombineUrl,
      {
        fileName,
        chunkIds,
        storageType,
      },
      { headers: new HttpHeaders({ timeout: `${20000}` }) }
    );
  }

  recombineQueue(
    fileName: string,
    chunkIds: string[],
    storageType = StorageType.Attachment
  ): Observable<{ fileRecombinationId: string; success: boolean }> {
    return this.httpClient.post<{ fileRecombinationId: string; success: boolean }>(this.recombineQueueUrl, {
      fileName,
      chunkIds,
      storageType,
    });
  }

  recombineProgress(fileRecombinationId: string): Observable<FileRecombinationProgressChanged> {
    return this.httpClient.get<FileRecombinationProgressChanged>(
      `${this.recombineProgressUrl}?fileRecombinationId=${fileRecombinationId}`
    );
  }
}
