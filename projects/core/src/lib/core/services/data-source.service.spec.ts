import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { DataSourceResult, SiteConfig } from '../models';
import { DataSourceService } from './data-source.service';

export class MockDataSourceService {
  getData(dataSource: string, params: object): Observable<any[]> {
    return of([]);
  }

  transformDataSource(dsData: DataSourceResult): any[] {
    return [];
  }
}

describe('DataSourceService', () => {
  let service: DataSourceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') }],
    });
    service = TestBed.inject(DataSourceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
