import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TimeZone } from '@nimbus/material';
import { Observable } from 'rxjs';
import { SiteConfig } from '../models';
import { AppConfiguration } from '../models/app-configuration';

@Injectable({
  providedIn: 'root',
})
export class ConfigurationService {
  private initUrl: string;
  private tzUrl: string;

  constructor(private httpClient: HttpClient, private siteConfig: SiteConfig) {
    this.initUrl = siteConfig.initUrl;
    this.tzUrl = `${siteConfig.apiUrl}/timezone`;
  }

  getAppConfig(): Observable<AppConfiguration> {
    return this.httpClient.get<AppConfiguration>(this.initUrl);
  }

  getTimeZones(): Observable<TimeZone[]> {
    return this.httpClient.get<TimeZone[]>(this.tzUrl);
  }
}
