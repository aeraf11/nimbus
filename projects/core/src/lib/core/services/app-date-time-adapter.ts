/**
 * @license
 * Copyright Google LLC All Rights Reserved.
 *
 * Use of this source code is governed by an MIT-style license that can be
 * found in the LICENSE file at https://angular.io/license
 */

import { Platform } from '@angular/cdk/platform';
import { Inject, Injectable, isDevMode, Optional } from '@angular/core';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { DateTime, DateTimeAdapter, DateTimeRange, Time, TimeZone } from '@nimbus/material';
import {
  addDays,
  addMonths,
  addYears,
  differenceInCalendarDays,
  differenceInDays,
  differenceInMilliseconds,
  differenceInMinutes,
  differenceInMonths,
  differenceInWeeks,
  differenceInYears,
  endOfDay,
  endOfMonth,
  endOfYear,
  format,
  formatISO,
  getDaysInMonth,
  getHours,
  getMinutes,
  getSeconds,
  getYear,
  isValid,
  parse,
  set,
  startOfDay,
  startOfMonth,
  startOfYear,
} from 'date-fns';
import getDate from 'date-fns/getDate';
import getDay from 'date-fns/getDay';
import getMonth from 'date-fns/getMonth';
import parseISO from 'date-fns/parseISO';
import { skip, take } from 'rxjs/operators';
import { StateService } from '../services/state.service';

// TODO(mmalerba): Remove when we no longer support safari 9.
/** Whether the browser supports the Intl API. */
let SUPPORTS_INTL_API: boolean;

// We need a try/catch around the reference to `Intl`, because accessing it in some cases can
// cause IE to throw. These cases are tied to particular versions of Windows and can happen if
// the consumer is providing a polyfilled `Map`. See:
// https://github.com/Microsoft/ChakraCore/issues/3189
// https://github.com/angular/components/issues/15687
try {
  SUPPORTS_INTL_API = typeof Intl != 'undefined';
} catch {
  SUPPORTS_INTL_API = false;
}

/** The default month names to use if Intl API is not available. */
const DEFAULT_MONTH_NAMES = {
  long: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
  short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  narrow: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
};

/** The default date names to use if Intl API is not available. */
const DEFAULT_DATE_NAMES = range(31, (i) => String(i + 1));

/** The default day of the week names to use if Intl API is not available. */
const DEFAULT_DAY_OF_WEEK_NAMES = {
  long: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
  short: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
  narrow: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
};

/**
 * Matches strings that have the form of a valid RFC 3339 string
 * (https://tools.ietf.org/html/rfc3339). Note that the string may not actually be a valid date
 * because the regex will match strings an with out of bounds month, date, etc.
 */
const ISO_8601_REGEX = /^\d{4}-\d{2}-\d{2}(?:T\d{2}:\d{2}:\d{2}(?:\.\d+)?(?:Z|(?:(?:\+|-)\d{2}:\d{2}))?)?$/;

type FormatRelativeToken = 'other' | 'lastWeek' | 'yesterday' | 'today' | 'tomorrow' | 'nextWeek';

const DEFAULT_RELATIVE_DATE_FORMATS = new Map<FormatRelativeToken, string>([
  ['other', ''],
  ['lastWeek', 'Last week'],
  ['yesterday', 'Yesterday'],
  ['today', 'Today'],
  ['tomorrow', 'Tomorrow'],
  ['nextWeek', 'Next Week'],
]);

const MIN_DATETIME_LENGTH = 10;

/** Creates an array and fills it with values. */
function range<T>(length: number, valueFunction: (index: number) => T): T[] {
  const valuesArray = Array(length);
  for (let i = 0; i < length; i++) {
    valuesArray[i] = valueFunction(i);
  }
  return valuesArray;
}

/** Adapts the native JS Date for use with cdk-based components that work with dates. */
@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class AppDateTimeAdapter extends DateTimeAdapter<DateTime> {
  /** Whether to clamp the date between 1 and 9999 to avoid IE and Edge errors. */
  private readonly _clampDate: boolean;

  /**
   * Whether to use `timeZone: 'utc'` with `Intl.DateTimeFormat` when formatting dates.
   * Without this `Intl.DateTimeFormat` sometimes chooses the wrong timeZone, which can throw off
   * the result. (e.g. in the en-US locale `new Date(1800, 7, 14).toLocaleDateString()`
   * will produce `'8/13/1800'`.
   *
   * TODO(mmalerba): drop this variable. It's not being used in the code right now. We're now
   * getting the string representation of a Date object from its utc representation. We're keeping
   * it here for sometime, just for precaution, in case we decide to revert some of these changes
   * though.
   */
  useUtcForDisplay = true;

  private dateFormat = 'dd MMM yyyy';
  private timeFormat = 'HH:mm';
  private timeFormatWithSeconds = 'HH:mm:ss';
  public timeZones: TimeZone[] = [];
  private timeZoneDictionary: { [key: string]: string } = {};
  public timeZoneId: string | null = null;

  constructor(@Optional() @Inject(MAT_DATE_LOCALE) matDateLocale: string, platform: Platform, state: StateService) {
    super();
    super.setLocale(matDateLocale);

    // IE does its own time zone correction, so we disable this on IE.
    this.useUtcForDisplay = !platform.TRIDENT;
    this._clampDate = platform.TRIDENT || platform.EDGE;

    state
      .selectDateTimeFormats()
      .pipe()
      .subscribe(({ dateFormat, timeFormat, timeZoneId }) => {
        this.dateFormat = dateFormat || this.dateFormat;
        this.timeFormat = timeFormat || this.timeFormat;
        this.timeZoneId = timeZoneId || this.timeZoneId;
      });

    state
      .selectTimeZones()
      .pipe(skip(1), take(1))
      .subscribe((timeZones) => {
        this.timeZones = timeZones || [];
        this.timeZoneDictionary = {};
        this.timeZones.forEach((tz) => {
          this.timeZoneDictionary[tz.value] = tz.label;
        });
      });
  }

  getYear(dateTime: DateTime): number {
    if (!dateTime.dateTime) return 0;
    return getYear(parseISO(dateTime.dateTime));
  }

  getMonth(dateTime: DateTime): number {
    if (!dateTime.dateTime) return 0;
    return getMonth(parseISO(dateTime.dateTime));
  }

  getDate(dateTime: DateTime): number {
    if (!dateTime.dateTime) return 0;
    return getDate(parseISO(dateTime.dateTime));
  }

  getDayOfWeek(dateTime: DateTime): number {
    if (!dateTime.dateTime) return 0;
    return getDay(parseISO(dateTime.dateTime));
  }

  getMonthNames(style: 'long' | 'short' | 'narrow'): string[] {
    if (SUPPORTS_INTL_API) {
      const dtf = new Intl.DateTimeFormat(this.locale, { month: style, timeZone: 'utc' });
      return range(12, (i) => this._stripDirectionalityCharacters(this._format(dtf, new Date(2017, i, 1))));
    }
    return DEFAULT_MONTH_NAMES[style];
  }

  getDateNames(): string[] {
    if (SUPPORTS_INTL_API) {
      const dtf = new Intl.DateTimeFormat(this.locale, { day: 'numeric', timeZone: 'utc' });
      return range(31, (i) => this._stripDirectionalityCharacters(this._format(dtf, new Date(2017, 0, i + 1))));
    }
    return DEFAULT_DATE_NAMES;
  }

  getDayOfWeekNames(style: 'long' | 'short' | 'narrow'): string[] {
    if (SUPPORTS_INTL_API) {
      const dtf = new Intl.DateTimeFormat(this.locale, { weekday: style, timeZone: 'utc' });
      return range(7, (i) => this._stripDirectionalityCharacters(this._format(dtf, new Date(2017, 0, i + 1))));
    }
    return DEFAULT_DAY_OF_WEEK_NAMES[style];
  }

  getYearName(dateTime: DateTime): string {
    return String(this.getYear(dateTime));
  }

  getFirstDayOfWeek(): number {
    // We can't tell using native JS Date what the first day of the week is, we default to Sunday.
    return 0;
  }

  getNumDaysInMonth(dateTime: DateTime): number {
    if (!dateTime.dateTime) return 0;
    return getDaysInMonth(parseISO(dateTime.dateTime));
  }

  getDateFormat(): string {
    return this.dateFormat;
  }

  getTimeFormat(): string {
    return this.timeFormat;
  }

  getDateRange(daterange: DateTimeRange) {
    return {
      start: this.formatRangeDate(daterange?.start),
      end: this.formatRangeDate(daterange?.end, true),
    };
  }

  formatRangeDate(dateTime: DateTime | null, isEndDate = false) {
    if (!dateTime) return null;
    const displayDate = this.format(dateTime);
    let date = this.parse(displayDate);
    if (isEndDate && date) date = this.addCalendarDays(date, 1);
    return { date: date?.dateTime, displayDate: displayDate };
  }

  clone(dateTime: DateTime): DateTime {
    return { ...dateTime };
  }

  createDate(year: number, month: number, date: number): DateTime {
    if (isDevMode()) {
      // Check for invalid month and date (except upper bound on date which we have to check after
      // creating the Date).
      if (month < 0 || month > 11) {
        throw Error(`Invalid month index "${month}". Month index has to be between 0 and 11.`);
      }

      if (date < 1) {
        throw Error(`Invalid date "${date}". Date has to be greater than 0.`);
      }
    }

    const result = this._createDateWithOverflow(year, month, date);
    // Check that the date wasn't above the upper bound for the month, causing the month to overflow
    if (result.getMonth() != month && isDevMode()) {
      throw Error(`Invalid date "${date}" for month with index "${month}".`);
    }

    return { dateTime: formatISO(result), timeZoneId: null };
  }

  today(): DateTime {
    return { dateTime: formatISO(new Date()), timeZoneId: null };
  }

  parse(value: any, dummy: any = null, showSeconds = false): DateTime | null {
    if ((value + '').length < MIN_DATETIME_LENGTH) return null; // Need to not try to parse partially entered short strings.
    let parsed;
    try {
      const format = showSeconds ? `${this.dateFormat} ${this.timeFormat}:ss` : `${this.dateFormat} ${this.timeFormat}`;
      parsed = parse(value, format, new Date());
      if (parsed.toString() === 'Invalid Date') throw 'Invalid DateTime';
    } catch {
      try {
        parsed = parseISO(value);
        if (parsed.toString() === 'Invalid Date') throw 'Invalid DateTime';
      } catch {
        parsed = parse(value, this.dateFormat, new Date());
      }
    }
    if (!isValid(parsed)) return null;
    return { dateTime: formatISO(parsed), timeZoneId: null };
  }

  parseWithTime(value: any, showSeconds = false) {
    return this.parse(value, null, showSeconds);
  }

  format(dateTime: DateTime): string {
    if (!dateTime.dateTime) return '';
    return format(parseISO(dateTime.dateTime), this.dateFormat);
  }

  formatDateTime(dateTime: DateTime, includeTime: boolean, includeTimeZone?: boolean, includeSeconds = false): string {
    if (!dateTime.dateTime) return '';
    let dt;

    if (includeTime) {
      const timeFormat = includeSeconds ? this.timeFormatWithSeconds : this.timeFormat;
      dt = format(parseISO(dateTime.dateTime), `${this.dateFormat} ${timeFormat}`);
    } else {
      dt = this.format(dateTime);
    }
    if (includeTime) {
      //Note: The matching of timezone in the timezone dictionary is case sensitive.
      const tz = dateTime.timeZoneId ? this.timeZoneDictionary[dateTime.timeZoneId.toLowerCase()] : null;
      return tz && includeTimeZone ? `${dt} ${tz}` : `${dt}`;
    }
    return dt;
  }

  addCalendarYears(dateTime: DateTime, years: number): DateTime {
    if (!dateTime.dateTime) return { dateTime: null, timeZoneId: null };
    return { dateTime: formatISO(addYears(parseISO(dateTime.dateTime), years)), timeZoneId: null };
  }

  addCalendarMonths(dateTime: DateTime, months: number): DateTime {
    if (!dateTime.dateTime) return { dateTime: null, timeZoneId: null };
    return { dateTime: formatISO(addMonths(parseISO(dateTime.dateTime), months)), timeZoneId: null };
  }

  addCalendarDays(dateTime: DateTime, days: number): DateTime {
    if (!dateTime.dateTime) return { dateTime: null, timeZoneId: null };
    return { dateTime: formatISO(addDays(parseISO(dateTime.dateTime), days)), timeZoneId: null };
  }

  toIso8601(dateTime: DateTime): string {
    return dateTime.dateTime || '';
  }

  /**
   * Returns the given value if given a valid Date or null. Deserializes valid ISO 8601 strings
   * (https://www.ietf.org/rfc/rfc3339.txt) into valid Dates and empty string into null. Returns an
   * invalid date for all other values.
   */
  override deserialize(value: any): DateTime | null {
    if (value?.dateTime === null) return super.deserialize(null);
    return super.deserialize(value);
  }

  isDateInstance(obj: any) {
    return obj && 'dateTime' in obj && 'timeZoneId' in obj;
  }

  isValid(dateTime: DateTime) {
    if (!dateTime.dateTime || dateTime.dateTime.length < MIN_DATETIME_LENGTH) return false;
    try {
      parseISO(dateTime.dateTime);
      return true;
    } catch {
      return false;
    }
  }

  invalid(): DateTime {
    return { dateTime: null, timeZoneId: null };
  }

  getTime(dateTime: DateTime): Time {
    if (!dateTime.dateTime) return { hours: 0, minutes: 0, seconds: 0 };
    const parsed = parseISO(dateTime.dateTime);
    return { hours: getHours(parsed), minutes: getMinutes(parsed), seconds: getSeconds(parsed) };
  }

  setTime(dateTime: DateTime, time: Time): DateTime {
    if (!dateTime?.dateTime) return { dateTime: null, timeZoneId: null };
    const adjusted = set(parseISO(dateTime.dateTime), time);
    return { ...dateTime, dateTime: formatISO(adjusted) };
  }

  getTimeZoneId(dateTime: DateTime): string | null {
    return dateTime.timeZoneId;
  }

  setTimeZoneId(dateTime: DateTime, timeZoneId: string): DateTime {
    return { ...dateTime, timeZoneId };
  }

  getTimeZones(): TimeZone[] {
    return this.timeZones;
  }

  sameDateTime(dateTime1: DateTime | null, dateTime2: DateTime | null): boolean {
    if (!dateTime1 || !dateTime2) return false;
    return dateTime1.dateTime === dateTime2.dateTime;
  }

  /**
   * Custom Ranges
   */
  getTodayInRange(): DateTimeRange {
    const start = startOfDay(new Date());
    const end = endOfDay(new Date());
    return this.buildDateRange(formatISO(start), formatISO(end));
  }

  getYesterdayInRange(): DateTimeRange {
    const date = addDays(new Date(), -1);
    const start = startOfDay(date);
    const end = endOfDay(date);
    return this.buildDateRange(formatISO(start), formatISO(end));
  }

  getLastNDaysInRange(days: number): DateTimeRange {
    const date = new Date();
    const start = startOfDay(addDays(date, -days));
    return this.buildDateRange(formatISO(start), formatISO(endOfDay(date)));
  }

  getThisMonthInRange(): DateTimeRange {
    const date = new Date();
    const start = startOfMonth(date);
    return this.buildDateRange(formatISO(start), formatISO(endOfMonth(date)));
  }

  getLastMonthInRange(): DateTimeRange {
    const date = addMonths(new Date(), -1);
    const start = startOfMonth(date);
    const end = endOfMonth(date);
    return this.buildDateRange(formatISO(start), formatISO(end));
  }

  getLastNMonthsInRange(months: number): DateTimeRange {
    const date = new Date();
    const start = addMonths(date, -months);
    return this.buildDateRange(formatISO(start), formatISO(endOfDay(date)));
  }

  getThisYearInRange(): DateTimeRange {
    const date = new Date();
    const start = formatISO(startOfYear(date));
    const end = formatISO(endOfYear(date)); // do to month
    return this.buildDateRange(start, end);
  }

  getLastYearInRange(): DateTimeRange {
    const date = addYears(new Date(), -1);
    const start = formatISO(startOfYear(date));
    const end = formatISO(endOfYear(date));
    return this.buildDateRange(start, end);
  }

  formatRelative(dateTime: DateTime): string {
    if (!dateTime.dateTime) return '';
    return this.formatRelativeString(dateTime.dateTime);
  }

  formatRelativeString(dateTimeString: string): string {
    if (!dateTimeString) return '';
    const now = new Date();
    const test = parseISO(dateTimeString);
    const diff = differenceInCalendarDays(test, now);
    const token = this.getRelativeToken(diff);
    const format = this.getRelativeFormat(token);

    return format || this.formatDateTime({ dateTime: dateTimeString, timeZoneId: null }, false, true);
  }

  // Mimics dateFns's behaviour but using our own formats with no times
  private getRelativeToken(diff: number): FormatRelativeToken {
    let token: FormatRelativeToken;
    if (diff < -6) {
      token = 'other';
    } else if (diff < -1) {
      token = 'lastWeek';
    } else if (diff < 0) {
      token = 'yesterday';
    } else if (diff < 1) {
      token = 'today';
    } else if (diff < 2) {
      token = 'tomorrow';
    } else if (diff < 7) {
      token = 'nextWeek';
    } else {
      token = 'other';
    }
    return token;
  }

  // Initially setup for general 'en' culture, going forward we could run the formats through our translation service when we do that or set up culture files.
  private getRelativeFormat(token: FormatRelativeToken) {
    return DEFAULT_RELATIVE_DATE_FORMATS.get(token);
  }

  private buildDateRange(start: string, end: string): DateTimeRange {
    return {
      start: { dateTime: start, timeZoneId: this.timeZoneId },
      end: { dateTime: end, timeZoneId: this.timeZoneId },
    };
  }

  /** Creates a date but allows the month and date to overflow. */
  private _createDateWithOverflow(year: number, month: number, date: number) {
    // Passing the year to the constructor causes year numbers <100 to be converted to 19xx.
    // To work around this we use `setFullYear` and `setHours` instead.
    const d = new Date();
    d.setFullYear(year, month, date);
    d.setHours(d.getHours(), d.getMinutes(), d.getSeconds());
    return d;
  }

  /**
   * Pads a number to make it two digits.
   * @param n The number to pad.
   * @returns The padded number.
   */
  private _2digit(n: number) {
    return ('00' + n).slice(-2);
  }

  /**
   * Strip out unicode LTR and RTL characters. Edge and IE insert these into formatted dates while
   * other browsers do not. We remove them to make output consistent and because they interfere with
   * date parsing.
   * @param str The string to strip direction characters from.
   * @returns The stripped string.
   */
  private _stripDirectionalityCharacters(str: string) {
    return str.replace(/[\u200e\u200f]/g, '');
  }

  /**
   * When converting Date object to string, javascript built-in functions may return wrong
   * results because it applies its internal DST rules. The DST rules around the world change
   * very frequently, and the current valid rule is not always valid in previous years though.
   * We work around this problem building a new Date object which has its internal UTC
   * representation with the local date and time.
   * @param dtf Intl.DateTimeFormat object, containg the desired string format. It must have
   *    timeZone set to 'utc' to work fine.
   * @param date Date from which we want to get the string representation according to dtf
   * @returns A Date object with its UTC representation based on the passed in date info
   */
  private _format(dtf: Intl.DateTimeFormat, date: Date) {
    // Passing the year to the constructor causes year numbers <100 to be converted to 19xx.
    // To work around this we use `setUTCFullYear` and `setUTCHours` instead.
    const d = new Date();
    d.setUTCFullYear(date.getFullYear(), date.getMonth(), date.getDate());
    d.setUTCHours(date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());
    return dtf.format(d);
  }

  getIsoStartOfToday(): string {
    return formatISO(startOfDay(new Date()));
  }

  getOverdueFilter(): string {
    return `{"end":"${this.getIsoStartOfToday()}"}`;
  }

  getUpcomingFilter(): string {
    return `{"start":"${this.getIsoStartOfToday()}"}`;
  }

  /**
   * Custom Comparisons
   */
  compareYears(start: DateTime, end: DateTime): number {
    if (!start?.dateTime || !end?.dateTime) {
      return 0;
    }

    return differenceInYears(parseISO(start.dateTime), parseISO(end.dateTime));
  }

  compareMonths(start: DateTime, end: DateTime): number {
    if (!start?.dateTime || !end?.dateTime) {
      return 0;
    }

    return differenceInMonths(parseISO(start.dateTime), parseISO(end.dateTime));
  }

  compareWeeks(start: DateTime, end: DateTime): number {
    if (!start?.dateTime || !end?.dateTime) {
      return 0;
    }

    return differenceInWeeks(parseISO(start.dateTime), parseISO(end.dateTime));
  }

  compareDays(start: DateTime, end: DateTime): number {
    if (!start?.dateTime || !end?.dateTime) {
      return 0;
    }

    return differenceInDays(parseISO(start.dateTime), parseISO(end.dateTime));
  }

  compareMinutes(start: DateTime, end: DateTime): number {
    if (!start?.dateTime || !end?.dateTime) {
      return 0;
    }

    return differenceInMinutes(parseISO(start.dateTime), parseISO(end.dateTime));
  }

  compareMilliseconds(start: DateTime, end: DateTime): number {
    if (!start?.dateTime || !end?.dateTime) {
      return 0;
    }

    return differenceInMilliseconds(parseISO(start.dateTime), parseISO(end.dateTime));
  }

  convertFromCoreDateTime(coreDate: { DateTime: string | null; TimeZoneId: string } | null): DateTime | null {
    if (!coreDate) return null;
    if (!coreDate.DateTime) return { dateTime: null, timeZoneId: coreDate.TimeZoneId };
    const dateTimeObj = parseISO(coreDate.DateTime);
    const dateTime = formatISO(dateTimeObj);
    const result = { dateTime, timeZoneId: coreDate.TimeZoneId };
    return result;
  }

  convertToCoreDateTime(dt: DateTime | null): { DateTime: string | null; TimeZoneId: string | null } | null {
    if (!dt) return null;
    if (!dt.dateTime) return { DateTime: null, TimeZoneId: dt.timeZoneId };
    const dateTimeObj = parseISO(dt.dateTime);
    const DateTime = format(dateTimeObj, 'yyyy-MM-dd HH:mm:ss');
    return { DateTime, TimeZoneId: dt.timeZoneId };
  }

  convertFromCoreDateRange(
    coreDateRange: { From: string | null; To: string | null; TimeZoneId: string } | null
  ): DateTimeRange | null {
    if (!coreDateRange) return null;
    const start = this.convertFromCoreDateTime({ DateTime: coreDateRange.From, TimeZoneId: coreDateRange.TimeZoneId });
    const end = this.convertFromCoreDateTime({ DateTime: coreDateRange.To, TimeZoneId: coreDateRange.TimeZoneId });
    return { start, end };
  }

  convertToCoreDateRange(
    range: DateTimeRange
  ): { From: string | null; To: string | null; TimeZoneId: string | null } | null {
    if (!range) return null;
    const from = this.convertToCoreDateTime(range.start);
    const to = this.convertToCoreDateTime(range.end);
    return { From: from?.DateTime || null, To: to?.DateTime || null, TimeZoneId: to?.TimeZoneId || null };
  }

  getStartOfDay(dt: DateTime | null): DateTime | null {
    if (!dt) return null;
    if (!dt.dateTime) return dt;
    const dateTimeObj = parseISO(dt.dateTime);
    const startOfDayObj = startOfDay(dateTimeObj);
    return { dateTime: formatISO(startOfDayObj), timeZoneId: dt.timeZoneId };
  }

  getEndOfDay(dt: DateTime | null): DateTime | null {
    if (!dt) return null;
    if (!dt.dateTime) return dt;
    const dateTimeObj = parseISO(dt.dateTime);
    const endOfDayObj = endOfDay(dateTimeObj);
    return { dateTime: formatISO(endOfDayObj), timeZoneId: dt.timeZoneId };
  }
}
