import { Injectable, OnDestroy } from '@angular/core';
import { Dexie, liveQuery } from 'dexie';
import { from, Observable, of } from 'rxjs';
import { QueuedAttachment } from '../models';

export interface IndexedDbConfig {
  version: number;
  stores: { name: string; columns: string }[];
}

@Injectable({
  providedIn: 'root',
})
export class IndexedDbService extends Dexie implements OnDestroy {
  constructor() {
    super('LocalDb');
  }

  init(config: IndexedDbConfig): void {
    if (config) {
      this.version(config.version).stores(this.mapStores(config.stores));
      this.open();
    }
  }

  ngOnDestroy(): void {
    if (!this.hasBeenClosed) {
      this.close();
    }
  }

  private mapStores(configStores: { name: string; columns: string }[]): { [key: string]: string } {
    const stores: { [key: string]: string } = {};
    configStores.forEach((store: { name: string; columns: string }) => (stores[store.name] = store.columns));
    return stores;
  }

  getEntityAttachments(queueTableName: string, entityId: string): Observable<unknown[]> {
    if (!entityId) {
      return of([]);
    }

    return from(liveQuery(() => this._getEntityAttachments(queueTableName, entityId)));
  }

  private async _getEntityAttachments(queueTable: string, entityId: string): Promise<unknown[]> {
    const queuedItems = await this.table<QueuedAttachment>(queueTable).where('entityId').equals(entityId).toArray();
    return queuedItems.map((queuedItem) => queuedItem.item);
  }

  getAllAttachments(queueTableName: string): Observable<QueuedAttachment[]> {
    return from(liveQuery(() => this._getAllAttachments(queueTableName)));
  }

  private async _getAllAttachments(table: string): Promise<QueuedAttachment[]> {
    return await this.table<QueuedAttachment>(table).toArray();
  }
}
