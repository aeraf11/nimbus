import { Injectable } from '@angular/core';
import { ApplicationInsights } from '@microsoft/applicationinsights-web';
import { ApplicationInsightsConfig } from '../models';

@Injectable({
  providedIn: 'root',
})
export class AppInsightsService {
  appInsights: ApplicationInsights | undefined;
  appInsightsConfig: ApplicationInsightsConfig | undefined;
  customProperties: { [key: string]: any } | undefined;
  public get isTracking() {
    return this.appInsights?.config && !this.appInsights?.config.disableTelemetry;
  }

  setTrackingStatus(isOnline: boolean) {
    if (this.appInsights) {
      this.appInsights.config.disableTelemetry = !isOnline;
    }
  }

  onInit(config: ApplicationInsightsConfig | undefined) {
    this.appInsightsConfig = config;
    if (this.appInsightsConfig?.instrumentationKey) {
      this.appInsights = new ApplicationInsights({
        config: {
          instrumentationKey: this.appInsightsConfig?.instrumentationKey,
          enableDebugExceptions: this.appInsightsConfig?.enableDebugExceptions,
          loggingLevelConsole: this.appInsightsConfig?.loggingLevelConsole,
          enableAutoRouteTracking: this.appInsightsConfig?.enableAutoRouteTracking, // option to log all route changes
        },
      });
      this.customProperties = {
        customerName: this.appInsightsConfig?.customerName,
        applicationName: this.appInsightsConfig?.applicationName,
        version: this.appInsightsConfig?.version,
      };
      this.appInsights.loadAppInsights();
    }
  }

  logPageView(name?: string, url?: string) {
    if (this.isTracking) {
      this.appInsights?.trackPageView({
        name: name,
        uri: url,
        properties: this.customProperties,
      });
    }
  }

  logEvent(name: string, properties?: { [key: string]: any }) {
    if (this.isTracking) {
      properties = { ...properties, ...this.customProperties };
      this.appInsights?.trackEvent({ name: name }, properties);
    }
  }

  logMetric(name: string, average: number, properties?: { [key: string]: any }) {
    if (this.isTracking) {
      properties = { ...properties, ...this.customProperties };
      this.appInsights?.trackMetric({ name: name, average: average }, properties);
    }
  }

  logException(exception: Error, severityLevel?: number) {
    if (this.isTracking) {
      this.appInsights?.trackException({ exception: exception, severityLevel: severityLevel }, this.customProperties);
    }
  }

  logTrace(message: string, properties?: { [key: string]: any }) {
    properties = { ...properties, ...this.customProperties };
    this.appInsights?.trackTrace({ message: message }, properties);
  }
}
