import { StateService } from '@nimbus/core/src/lib/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { FormDefinition, SiteConfig } from '@nimbus/core/src/lib/core';
import { Observable, of } from 'rxjs';
import { FormDataService } from './form-data.service';
import { MockStateService } from '@nimbus/core/src/lib/core';

export class MockFormsDataService {
  loadForms(): Observable<FormDefinition[]> {
    return of();
  }

  bustCache(): Observable<any> {
    return of();
  }

  getFormlyComponentTemplate(formlyType: string, key: string, label: string, props?: any): any {
    return {};
  }
}

describe('FormsDataService', () => {
  let service: FormDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: StateService,
          useClass: MockStateService,
        },
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
      ],
    });
    service = TestBed.inject(FormDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
