import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SiteConfig, FormDefinition, StateService } from '@nimbus/core/src/lib/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class FormDataService {
  private formUrl: string;
  private siteId?: string;

  constructor(private httpClient: HttpClient, private siteConfig: SiteConfig, private stateService: StateService) {
    this.formUrl = `${siteConfig.apiUrl}/form`;

    this.stateService
      .selectSiteId()
      .pipe(untilDestroyed(this))
      .subscribe((id) => {
        this.siteId = id;
      });
  }

  loadForms(): Observable<FormDefinition[]> {
    return this.httpClient.get<FormDefinition[]>(`${this.formUrl}?siteId=${this.siteId}`);
  }

  bustCache(): Observable<any> {
    const url = this.formUrl + '?action=BustCache';
    return this.httpClient.post<any>(url, null);
  }

  getFormlyComponentTemplate(formlyType: string, key: string, label: string, props?: any): any {
    const type = formlyType;
    let componentJson = { type: type, key: key, props: { label: label } };
    if (props) {
      switch (formlyType) {
        case 'input': {
          const json = {
            outputDisplayValue: true,
          };
          props = { ...json, ...props, label: label };
          break;
        }
        case 'select2': {
          const json = {
            addDefaultOption: true,
            overrideFormReset: true,
            version: 2,
            resultCount: 1000,
            filterColumns: [],
            multiple: true,
          };
          props = { ...json, ...props, label: label };
          break;
        }
        case 'multichoice': {
          const json = {
            addDefaultOption: true,
            isFilterSearchMode: true,
            filterColumns: [],
            leafSelection: false,
            removable: true,
            enableTree: 'auto',
          };
          props = { ...json, ...props, label: label };
          break;
        }
        default: {
          break;
        }
      }
      componentJson = { ...componentJson, props: { ...props } };
    }
    return componentJson;
  }
}
