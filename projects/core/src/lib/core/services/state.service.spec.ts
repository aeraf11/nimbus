﻿import { StateService } from '@nimbus/core/src/lib/core';
import { TestBed } from '@angular/core/testing';

describe('StateService', () => {
  let service: StateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
