import { SiteConfig } from '@nimbus/core/src/lib/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { FileSyncService } from './file-sync.service';
import { SignalRService } from './signal-r.service';
import { MockSignalRService } from './signal-r.service.spec';

describe('FileSyncService', () => {
  let service: FileSyncService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
        { provide: SignalRService, useClass: MockSignalRService },
      ],
    });
    service = TestBed.inject(FileSyncService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
