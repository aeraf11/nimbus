import { LookupHierarchy } from '../models/lookup-hierarchy';
import { LookupDetail } from '../models/lookup-detail';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Dictionary } from '@ngrx/entity';
import { filter, map } from 'rxjs/operators';
import { Lookup, LookupDictionary, LookupNode } from '../models';
import { StateService } from './state.service';

@Injectable({
  providedIn: 'root',
})
export class LookupService {
  private hierarchy$: Observable<LookupHierarchy>;
  private flat$: Observable<LookupDictionary>;
  private details$: Observable<Dictionary<LookupDetail>>;
  private details?: Dictionary<LookupDetail>;

  constructor(private state: StateService) {
    this.details$ = state.selectLookupDetailEntities().pipe(filter((details) => !!details));
    this.hierarchy$ = state.selectLookupsHierarchy().pipe(filter((hierarchy) => !!hierarchy));
    this.flat$ = state.selectLookupsFlat().pipe(filter((flat) => !!flat));
    this.details$.subscribe((details) => (this.details = details));
  }

  getLookupsHierachy(key: string, filters: string[] = []): Observable<LookupNode[] | null> {
    if (filters && filters.length > 0) {
      return this.state.selectLookupsHierarchyFiltered(filters);
    } else {
      return this.hierarchy$.pipe(map((hierarchy) => this._getLookupsHierachy(hierarchy, key)));
    }
  }

  private _getLookupsHierachy(hierarchy: LookupHierarchy, key: string): LookupNode[] | null {
    if (!hierarchy?.[key]) {
      console.error(`There was an issue with the lookup with the key "${key}", it could be one of the following reasons:

      1. The lookup does not exist, check the key: "${key}"
      2. There is no "top level" item in this lookup, i.e. all items have a parentLookupId`);
      return null;
    }
    return hierarchy[key];
  }

  getLookupsFlat(key: string, leafOnly: boolean, filters: string[] = []): Observable<Lookup[] | null> {
    if (filters && filters.length > 0) {
      return this.state.selectLookupsFlatFiltered(filters);
    }
    return this.flat$.pipe(map((flat) => this._getLookupsFlat(flat, key, leafOnly)));
  }

  private _getLookupsFlat(flat: LookupDictionary, key: string, leafOnly: boolean): Lookup[] | null {
    if (!flat || !flat[key]) return null;
    if (leafOnly) return flat[key].filter((lookup) => lookup.isLeaf);
    return flat[key];
  }

  getLookupsForList(filters: string[] = []): Observable<LookupNode[] | null> {
    return this.state.selectLookupsForList(filters);
  }

  getSelectedIds(id: string): Observable<string[]> {
    return this.details$.pipe(map((details) => this._getSelectedIds(details, id)));
  }

  private _getSelectedIds(details: Dictionary<LookupDetail>, id: string): string[] {
    if (!id) return [];
    return this._getlevelIds(details, id).reverse();
  }

  getHierarchicalIdentifiers(id: string): Observable<{ entityId: string; entityType: number }[]> {
    return this.details$.pipe(map((details) => this._getHierarchicalIdentifiers(details, id)));
  }

  private _getHierarchicalIdentifiers(
    details: Dictionary<LookupDetail>,
    id: string
  ): { entityId: string; entityType: number }[] {
    if (!id) return [];
    return this._getlevelIdentifiers(details, id);
  }

  getDetail(id: string): Observable<LookupDetail | null> {
    return this.details$.pipe(map((details) => this._getDetail(details, id)));
  }

  getDetailSync(id: string): LookupDetail | null {
    if (!this.details) return null;
    return this._getDetail(this.details, id);
  }

  private _getDetail(details: Dictionary<LookupDetail>, id: string): LookupDetail | null {
    if (!details || !id) return null;
    return details[id] || null;
  }

  getDetails(ids: string[]): Observable<LookupDetail[] | null> {
    return this.details$.pipe(map((details) => this._getDetails(details, ids)));
  }

  private _getDetails(details: Dictionary<LookupDetail>, ids: string[]): LookupDetail[] | null {
    if (!details || !ids || ids.length === 0) return null;
    const list: LookupDetail[] = [];
    ids.forEach((id) => {
      const detail = details[id];
      if (detail) list.push(detail);
    });
    return list;
  }

  getDisplayValue(id: string): Observable<string | null> {
    return this.details$.pipe(map((details) => this._getDisplayValue(details, id)));
  }

  private _getDisplayValue(details: Dictionary<LookupDetail>, id: string): string | null {
    if (!details || !id) return null;
    return details[id]?.displayValue || null;
  }

  private _getlevelIds(details: Dictionary<LookupDetail> | null, id: string, ids: string[] = []): string[] {
    if (!details) return [];
    const lookup = details[id];
    if (lookup) {
      ids.push(id);
      if (lookup.parentLookupId) {
        this._getlevelIds(details, lookup.parentLookupId, ids);
      }
    }
    return ids;
  }

  private _getlevelIdentifiers(
    details: Dictionary<LookupDetail> | null,
    id: string,
    identifiers: { entityId: string; entityType: number }[] = []
  ): { entityId: string; entityType: number }[] {
    if (!details) return [];
    const lookup = details[id];
    if (lookup) {
      identifiers.push({ entityId: lookup.lookupId, entityType: lookup.entityType || 52 });
      if (lookup.parentLookupId) {
        this._getlevelIdentifiers(details, lookup.parentLookupId, identifiers);
      }
    }
    return identifiers;
  }
}
