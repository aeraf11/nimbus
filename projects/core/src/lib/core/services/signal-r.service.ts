import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { filter, map, Observable, Subject } from 'rxjs';
import { SignalRHub } from '../models/app-configuration';
import { StateService } from './state.service';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class SignalRService {
  initialised = false;
  accessToken?: string;
  userProfileId?: string;
  hubConfig?: SignalRHub[];
  hubs = new Map<
    string,
    { connection: HubConnection; events: Map<string, Observable<any>>; groups: Map<string, number> }
  >();

  constructor(state: StateService) {
    state
      .selectUser()
      .pipe(untilDestroyed(this))
      .subscribe((user) => {
        if (user?.userProfileId) {
          this.setUserProfileId(user.userProfileId);
        }
      });

    state
      .selectSignalRConfig()
      .pipe(untilDestroyed(this))
      .subscribe((config) => {
        if (config) {
          this.setConfig(config);
        }
      });
  }

  setAccessToken(accessToken: string) {
    this.accessToken = accessToken;
    this.init();
  }

  setUserProfileId(userProfileId: string) {
    this.userProfileId = userProfileId;
    this.init();
  }

  setConfig(hubs: SignalRHub[]) {
    this.hubConfig = hubs;
    this.init();
  }

  init() {
    if (!this.initialised && this.userProfileId && this.accessToken && this.hubConfig) {
      this.hubConfig.forEach((hub) => {
        const connection = new HubConnectionBuilder()
          .withAutomaticReconnect()
          .withUrl(hub.url, { accessTokenFactory: () => this.accessToken || '' })
          .build();
        const events = new Map<string, Observable<any>>();
        hub.events.forEach((event) => {
          const subject = new Subject<any>();
          connection.on(event, (payload: any) => {
            subject.next(payload);
          });
          events.set(event, subject);
        });
        connection.start().then(
          () => {
            connection.send('JoinGroup', this.userProfileId);
            const groups = new Map<string, number>();
            this.hubs?.set(hub.name, { connection, events, groups });
          },
          (error) => {
            console.error(error);
          }
        );
      });
      this.initialised = true;
    }
  }

  getEvent<T>(hubName: string, eventName: string, group?: string): Observable<T> | undefined {
    if (this.hubs) {
      const hub = this.hubs.get(hubName);
      if (hub) {
        const payload$ = hub.events.get(eventName);
        if (group) {
          this.incrementGroup(group, hub.groups);
          hub.connection.send('JoinGroup', group);
          return payload$?.pipe(
            filter((payload) => this.groupMatch(payload, group)),
            map((payload) => payload.event)
          );
        }
        return payload$?.pipe(map((payload) => payload.event));
      }
    }
    return undefined;
  }

  groupMatch(payload: any, group: string) {
    const groups = [...(payload.userProfileIds || []), ...(payload.topics || [])];
    return groups.includes(group);
  }

  leaveGroup(hubName: string, group: string) {
    const hub = this.hubs.get(hubName);
    if (hub) {
      const currentCount = hub.groups.get(group) || 0;
      if (currentCount > 1) {
        hub.groups.set(group, currentCount - 1);
      } else {
        hub.groups.delete(group);
        hub.connection.send('LeaveGroup', group);
      }
    }
  }

  incrementGroup(group: string, groups: Map<string, number>) {
    const currentCount = groups.get(group) || 0;
    groups.set(group, currentCount + 1);
  }
}

export class SignalRHubs {
  public static readonly notifyUser = 'notifyuser';
}

export class SignalREvents {
  public static readonly onEntityConversationChanged = 'OnEntityConversationChanged';
  public static readonly onFileRecombinationProgressChanged = 'OnFileRecombinationProgressChanged';
}
