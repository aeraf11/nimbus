import { Observable, of } from 'rxjs';

export class MockDialogService {
  openDialog(): Observable<any> {
    return of('');
  }
}
