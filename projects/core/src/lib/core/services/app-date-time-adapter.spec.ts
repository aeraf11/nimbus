import { Platform } from '@angular/cdk/platform';
import { TestBed } from '@angular/core/testing';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import { isNullOrUndefined } from '@microsoft/applicationinsights-core-js';
import { provideMockStore } from '@ngrx/store/testing';
import { DateTimeAdapter } from '@nimbus/material';
import { AppDateTimeAdapter } from './app-date-time-adapter';
import { StateService } from './state.service';
import { MockStateService } from '@nimbus/core/src/lib/core';

export class MockDateTimeAdapter {}

describe('AppDateTimeAdapter', () => {
  let service: AppDateTimeAdapter;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        AppDateTimeAdapter,
        provideMockStore({ initialState: {} }),
        {
          provide: StateService,
          useClass: MockStateService,
        },
        {
          provide: DateTimeAdapter,
          useClass: AppDateTimeAdapter,
          deps: [MAT_DATE_LOCALE, Platform, MockStateService],
        },
        {
          provide: DateAdapter,
          useClass: AppDateTimeAdapter,
          deps: [MAT_DATE_LOCALE, Platform, MockStateService],
        },
      ],
    });
    service = TestBed.inject(AppDateTimeAdapter);

    // store.overrideSelector(authReducers.selectAuthUserState, {
    //   user: {
    //     userProfileId: '',
    //     fullName: '',
    //     groupName: '',
    //     email: '',
    //     contactNumber: '',
    //     branch: '',
    //     permissions: [],
    //     dateFormat: '',
    //     timeFormat: '',
    //     thumbnailBlobId: '',
    //     initials: '',
    //   },
    // });
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should convert from null', () => {
    expect(service.convertFromCoreDateTime(null)).toEqual(null);
  });

  it('should convert from null core date', () => {
    expect(
      service.convertFromCoreDateTime({ DateTime: null, TimeZoneId: '18864771-cccc-41fd-aaae-3b1cfb7315c5' })
    ).toEqual({
      dateTime: null,
      timeZoneId: '18864771-cccc-41fd-aaae-3b1cfb7315c5',
    });
  });

  it('should convert from non-null core date', () => {
    expect(
      service.convertFromCoreDateTime({
        DateTime: '2023-12-25 12:00:00',
        TimeZoneId: '18864771-cccc-41fd-aaae-3b1cfb7315c5',
      })
    ).toEqual({
      dateTime: '2023-12-25T12:00:00Z',
      timeZoneId: '18864771-cccc-41fd-aaae-3b1cfb7315c5',
    });
  });

  it('should convert to null core date', () => {
    expect(
      service.convertToCoreDateTime({ dateTime: null, timeZoneId: '18864771-cccc-41fd-aaae-3b1cfb7315c5' })
    ).toEqual({
      DateTime: null,
      TimeZoneId: '18864771-cccc-41fd-aaae-3b1cfb7315c5',
    });
  });

  it('should convert to non-null core date', () => {
    expect(
      service.convertToCoreDateTime({
        dateTime: '2023-12-25T12:00:00Z',
        timeZoneId: '18864771-cccc-41fd-aaae-3b1cfb7315c5',
      })
    ).toEqual({
      DateTime: '2023-12-25 12:00:00',
      TimeZoneId: '18864771-cccc-41fd-aaae-3b1cfb7315c5',
    });
  });
});
