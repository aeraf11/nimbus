import { TestBed } from '@angular/core/testing';
import { LookupService } from './lookup.service';
import { StateService } from './state.service';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { Observable, of } from 'rxjs';
import { LookupDetail } from '../models';

describe('LookupService', () => {
  let service: LookupService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: StateService, useClass: MockStateService }],
    });
    service = TestBed.inject(LookupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

//create a mock lookup service class
export class MockLookupService {
  getHierarchicalIdentifiers(): string[] {
    return [];
  }

  getDetail(id: string): Observable<LookupDetail | null> {
    return of(null);
  }
}
