import { Dictionary } from '@ngrx/entity';
import { TimeZone } from '@nimbus/material';
import { Observable, of } from 'rxjs';
import {
  ApplicationInsightsConfig,
  CardOptions,
  DataListOptions,
  DebugOptions,
  DialogFormAction,
  DynamicRoute,
  FileUploadConfig,
  FormDefinition,
  Lookup,
  LookupDetail,
  LookupDictionary,
  LookupHierarchy,
  LookupNode,
  MetaDataColumnMatch,
  MetaDataItem,
  ScheduledJob,
  SignalRHub,
  Theme,
  Translations,
  User,
} from '../models';
import { StateService } from './state.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class MockStateService extends StateService {
  selectAppInsightsConfig(): Observable<ApplicationInsightsConfig | undefined> {
    return of();
  }

  selectDataListOptions(savedOptionsId: string): Observable<DataListOptions> {
    return of();
  }

  selectLookupsFlatFiltered(filters: string[]): Observable<Lookup[]> {
    return of();
  }

  selectScheduledJob(id: string): Observable<ScheduledJob | undefined> {
    return of();
  }

  selectScheduledJobs(): Observable<ScheduledJob[]> {
    return of();
  }

  selectEnableFormTranslations(): Observable<boolean | undefined> {
    return of();
  }

  saveOptions(options: DataListOptions): void {
    throw new Error('Method not implemented.');
  }

  saveCardOptions(options: CardOptions): void {
    throw new Error('Method not implemented.');
  }

  saveBookmark(routeId: string, bookmark: any): void {
    throw new Error('Method not implemented.');
  }

  setLoading(loading: boolean): void {
    throw new Error('Method not implemented.');
  }

  loadCurrentDynamicRouteModel(routeId: string, context: any): void {
    throw new Error('Method not implemented.');
  }

  loadCurrentDynamicRouteModelOffline(routeId: string, context: any): void {
    throw new Error('Method not implemented.');
  }

  postFormOffline(model: any, context: any, routeId: string): void {
    throw new Error('Method not implemented.');
  }

  addLookup(lookup: LookupDetail): void {
    throw new Error('Method not implemented.');
  }

  setDashboardLayout(dashboardLayout: string): void {
    throw new Error('Method not implemented.');
  }

  setTheme(themeId: string): void {
    throw new Error('Method not implemented.');
  }

  upgradeConfig(): void {
    throw new Error('Method not implemented.');
  }

  selectForm(): Observable<FormDefinition | undefined> {
    return of();
  }

  selectThemes(): Observable<Theme[] | undefined> {
    return of(undefined);
  }

  selectDateTimeFormats(): Observable<{ dateFormat: string | undefined; timeFormat: string | undefined }> {
    return of({
      dateFormat: 'yyyy-MM-dddd',
      timeFormat: 'HH:mm:ss',
    });
  }

  selectTimeZones(): Observable<TimeZone[] | null> {
    return of([
      {
        label: 'TestTimeZone',
        value: '',
      },
    ]);
  }

  selectMenuState(): Observable<'open' | 'closed'> {
    return of('open');
  }

  selectLookupDetailEntities(): Observable<Dictionary<LookupDetail>> {
    return of();
  }

  selectDashboardLayout(): Observable<'grid' | 'column'> {
    return of('grid');
  }

  selectLookupsHierarchy(): Observable<LookupHierarchy> {
    return of();
  }

  selectLookupsFlat(): Observable<LookupDictionary> {
    return of();
  }

  selectUser(): Observable<User | null> {
    return of(null);
  }

  selectFormSubmissionResult(): Observable<{ success: boolean; errorMessage: string | null } | null> {
    return of(null);
  }

  selectDebugOptions(): Observable<DebugOptions | undefined> {
    return of(undefined);
  }

  selectFileUploadConfig(): Observable<FileUploadConfig | undefined> {
    return of(undefined);
  }

  selectFormStateSettings(): Observable<{ [key: string]: any } | undefined> {
    return of(undefined);
  }

  selectCurrentDynamicForm(): Observable<FormDefinition | undefined> {
    return of(undefined);
  }

  selectCurrentRoute(): Observable<any> {
    return of(undefined);
  }

  selectDynamicRouteCurrentModel(): Observable<any> {
    return of({});
  }

  selectDynamicRouteCurrentError(): Observable<string | null> {
    return of(null);
  }

  selectCurrentBreakpoint(): Observable<'sm' | 'md' | 'lg'> {
    return of('lg');
  }

  selectCurrentDynamicRoute(): Observable<DynamicRoute | undefined> {
    return of(undefined);
  }

  clearCurrentDynamicRouteModel(): void {
    // void
  }

  selectLookupsForList(filters: string[]): Observable<LookupNode[]> {
    return of();
  }

  selectRoutesWithPermission(routes: any[]): any {
    return of();
  }

  selectSiteId() {
    return of('');
  }

  selectConnectionOnline(): Observable<boolean> {
    return of();
  }

  selectWorkOnline(): Observable<boolean> {
    return of();
  }

  selectSelectedThemeIsDark(): Observable<boolean | undefined> {
    return of(false);
  }

  selectUpgradeConfigMessage(): Observable<string> {
    return of('');
  }

  selectDefaultMetaDataByEntityTypeId(id: number): Observable<MetaDataItem[]> {
    return of([]);
  }

  selectMetaDataKeyColumns(listColumns: string[], entityType: string): Observable<MetaDataColumnMatch[]> {
    return of();
  }

  selectSignalRConfig(): Observable<SignalRHub[] | undefined> {
    return of();
  }

  selectAllTranslations(): Observable<Translations> {
    return of();
  }

  mergeDynamicRouteModel(
    model: any,
    routeId: string,
    props: {
      [x: string]: any;
    },
    autoFillModelOverrides: any[] | undefined,
    autoFillModelSkipProps: any[] | undefined
  ): void {
    // void
  }

  updateDynamicRouteModel(model: unknown): void {
    // void
  }

  submitFormClear(): void {
    // void
  }

  submitForm(model: any, formAction: DialogFormAction, formId: string, context: any) {
    // void
  }

  postForm(model: any, routeId: string, onSuccess?: () => void, showErrorSnack?: boolean) {
    // void
  }

  selectLookupsHierarchyFiltered(filters: string[]): Observable<LookupNode[]> {
    return of([]);
  }
}
