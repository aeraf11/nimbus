import { Observable, of } from 'rxjs';
import { EntityChatItem } from '../models';

export class MockChatService {
  addMessage(entityChatItem: EntityChatItem): Observable<boolean> {
    return of(true);
  }
}
