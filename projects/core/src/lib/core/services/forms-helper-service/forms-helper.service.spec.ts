import { TestBed } from '@angular/core/testing';
import { StateService } from '@nimbus/core/src/lib/core';
import { FormsHelperService } from './forms-helper.service';
import { MockStateService } from '@nimbus/core/src/lib/core';

describe('FormsHelperService', () => {
  let service: FormsHelperService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: StateService, useClass: MockStateService }],
    });
    service = TestBed.inject(FormsHelperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
