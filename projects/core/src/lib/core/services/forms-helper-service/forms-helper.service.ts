import { Injectable } from '@angular/core';
import { AppDateTimeAdapter, LookupService, utils } from '@nimbus/core/src/lib/core';
import { DateTime } from '@nimbus/material';

@Injectable({
  providedIn: 'root',
})
export class FormsHelperService {
  // private dateTime: AppDateTimeAdapter === DON'T REMOVE. It seems not to be used, but it is used by forms consuming this service.
  constructor(private lookup: LookupService, private dateTime: AppDateTimeAdapter) {}

  joinField(source: any[], newFieldName: string, fields: string[], seperator = ' '): any[] {
    return source.map((item) => {
      const newItem = { ...item };
      const fieldValues = fields.map((field) => item[field]);
      newItem[newFieldName] = fieldValues.join(seperator);
      return newItem;
    });
  }

  formatField(source: any[], newFieldName: string, template: string, exclProp?: string, exclVal?: string): any[] {
    const formatted = source.map((item) => {
      return { ...item, [newFieldName]: this.parseTemplate(template, item, '') };
    });

    if (exclProp && exclVal) {
      return formatted.filter((item) => item[exclProp] !== exclVal);
    } else {
      return formatted;
    }
  }

  parseTemplate(object: any, map: any, fallback: string): any {
    if (!object) return '';

    if (typeof object !== 'object') {
      let props = this.simpleTemplate(object, map, fallback);
      props = this.parseTemplateLookups(props, map, fallback);
      props = this.parseExpression(props, map);
      props = this.tidy(props);
      return props;
    }

    if (Array.isArray(object)) {
      const result: any[] = [];
      object.forEach((item) => {
        result.push(this.parseTemplate(item, map, fallback));
      });
      return result;
    }

    if (typeof object === 'object') {
      const result: any = {};
      for (const key in object) {
        if (object.hasOwnProperty(key)) {
          result[key] = this.parseTemplate(object[key], map, fallback);
        }
      }
      return result;
    }
  }

  parseTemplateBool(template: string, obj: any, fallback: string): boolean {
    return this.parseTemplate(template, obj, fallback) === 'true';
  }

  simpleTemplate(template: string, map: any, fallback: string, tidy = false): string {
    if (typeof template !== 'string') return template;
    const result = template.replace(/\$\{[^}]+\}/g, (match) => this.resolveTemplateParam(match, fallback, map));
    if (tidy) return this.tidy(result);
    return result;
  }

  tidy(text: string): string {
    if (!text) {
      return '';
    }

    if (typeof text !== 'string') {
      return text;
    }

    return text.replace(/\?\{[^a-zA-Z0-9\{\}\!\"\£\$\€\%\^\*\-\+\\\/]+\}/g, '').replace(/\?\{([^}]*)\}/g, '$1');
  }

  clearTemplate(template: string): string {
    if (typeof template !== 'string') return template;
    const result = template.replace(/\$\{[^}]+\}/g, '');
    return this.tidy(result);
  }

  parseTemplateLookups(template: string, map: any, fallback: string): string {
    if (typeof template !== 'string') return template;
    const keyMatchMap = new Map<string, string>();
    const keys: string[] = [];
    template.match(/[\#|\*]\{[^}]+\}/g)?.map((match) => {
      const key = this.resolveTemplateParam(match, fallback, map);
      keys.push(key);
      keyMatchMap.set(key, match);
    });

    if (keys.length > 0) {
      const lookups = keys.map((key) => this.lookup.getDetailSync(key));
      let result = template.slice(); // slice() copies the string
      lookups.forEach((lookup) => {
        const match = keyMatchMap.get(lookup?.lookupId ?? '') ?? '';
        result = result.replace(
          match,
          match.startsWith('#') ? lookup?.displayValue ?? fallback ?? '' : lookup?.shortCode ?? fallback ?? ''
        );
      });
      return result;
    }
    return template;
  }

  parseExpression(expression: string, map: unknown): string {
    if (typeof expression !== 'string') return expression;
    const result = expression.replace(/\=\{([^}]+)\}/g, (match, p) => this.parseStringExpression(p, map));
    return result;
  }

  parseStringExpression(expression: string, map: unknown): string {
    if (typeof expression !== 'string') return expression;
    const exp = utils.evalStringExpression(expression, ['formState']);
    if (exp) {
      return exp(map) + '';
    } else return expression;
  }

  resolveTemplateParam(name: string, fallback: string, map: any) {
    const result = name
      .slice(2, -1)
      .trim()
      .split('.')
      .reduce(
        (obj, key) =>
          !!obj && obj[key] !== undefined && obj[key] !== null
            ? obj[key]
            : fallback || fallback === ''
            ? fallback
            : name,
        map
      );
    return result;
  }

  getFileExtension(filename: string, uppercase = false): string | null {
    if (!filename) return null;
    const index = filename.lastIndexOf('.');
    if (index === -1) return null;
    const ext = filename.substring(index + 1);
    if (uppercase) return ext.toUpperCase();
    return ext;
  }

  log(...args: unknown[]) {
    console.info('Logged values:', args);
  }

  isWithinMinutes(date: string, range: number): boolean {
    if (!date || !range) {
      return false;
    }

    const inputDate = this.parseDateString(date);
    const today = this.dateTime.today();

    if (inputDate) {
      return this.dateTime.compareMinutes(today, inputDate) <= range;
    }

    return false;
  }

  isWithinDays(date: string, range: number): boolean {
    if (!date || !range) {
      return false;
    }

    const inputDate = this.parseDateString(date);
    const today = this.dateTime.today();

    if (inputDate) {
      return this.dateTime.compareDays(today, inputDate) < range;
    }

    return false;
  }

  isWithinWeeks(date: string, range: number): boolean {
    if (!date || !range) {
      return false;
    }

    const inputDate = this.parseDateString(date);
    const today = this.dateTime.today();

    if (inputDate) {
      return this.dateTime.compareWeeks(today, inputDate) < range;
    }

    return false;
  }

  isWithinMonths(date: string, range: number): boolean {
    if (!date || !range) {
      return false;
    }

    const inputDate = this.parseDateString(date);
    const today = this.dateTime.today();

    if (inputDate) {
      return this.dateTime.compareMonths(today, inputDate) < range;
    }

    return false;
  }

  getYearsSince(date: string): number {
    if (date) {
      const inputDate = this.parseDateString(date);
      if (inputDate) {
        const today = this.dateTime.today();
        return this.dateTime.compareYears(today, inputDate);
      }
    }
    return 0;
  }

  private parseDateString(date: string): DateTime | null {
    try {
      return this.dateTime.parse(date);
    } catch (ex) {
      throw new Error(`Could not parse date from ${date}`);
    }
  }
}
