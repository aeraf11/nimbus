import { SiteConfig } from '../models';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { DataSourceResult } from '../models';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DataSourceService {
  private dataSourceUrl: string;

  constructor(private httpClient: HttpClient, private siteConfig: SiteConfig) {
    this.dataSourceUrl = `${siteConfig.apiUrl}/datasources`;
  }

  getData<T>(dataSource: string, params: object): Observable<T[]> {
    return this.getDataSource(dataSource, params).pipe(map((dsData) => this.transformDataSource(dsData)));
  }

  private getDataSource(dataSource: string, params: object): Observable<DataSourceResult> {
    const paramsArray: string[] = [];
    if (params) {
      Object.entries(params).forEach(([key, value]) => {
        paramsArray.push(`${key}=${value}`);
      });
    }
    let url = `${this.dataSourceUrl}/${dataSource}`;
    if (paramsArray.length > 0) {
      url = `${url}?${paramsArray.join('&')}`;
    }
    return this.httpClient.get<DataSourceResult>(url);
  }

  transformDataSource(dsData: DataSourceResult): any[] {
    return dsData?.results ?? [];
  }
}
