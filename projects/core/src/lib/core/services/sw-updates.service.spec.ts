import { ServiceWorkerModule, SwUpdate } from '@angular/service-worker';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { SiteConfig } from '../models';
import { SwUpdatesService } from './sw-updates.service';
import { MatDialogModule } from '@angular/material/dialog';

describe('SwUpdatesService', () => {
  let service: SwUpdatesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, MatDialogModule],
      providers: [
        {
          provide: SwUpdate,
          useClass: ServiceWorkerModule,
        },
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
      ],
    });
    service = TestBed.inject(SwUpdatesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
