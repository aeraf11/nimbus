import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { SiteConfig } from '../models';
import { LookupDataService } from './lookup-data.service';

describe('LookupDataService', () => {
  let service: LookupDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') }],
    });
    service = TestBed.inject(LookupDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
