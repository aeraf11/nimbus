import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { SiteConfig } from '../models';
import { ConfigurationService } from './configuration.service';

describe('ConfigurationService', () => {
  let service: ConfigurationService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') }],
    });

    service = TestBed.inject(ConfigurationService);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    const dummyConfig = {
      oidc: {},
      menuConfig: [],
      staticRoutes: [],
      timeZones: [],
      themeConfig: [],
    };

    service.getAppConfig().subscribe((config) => {
      expect(config).toBeTruthy();
    });

    const req = httpTestingController.expectOne('someInitUrl');
    expect(req.request.method).toBe('GET');
    req.flush(dummyConfig);
  });
});
