import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { ChartData, ChartType, DataSourceAccumulation, SiteConfig } from '../models';
import { ChartDataService } from './chart-data.service';

export class MockChartDataService {
  getChartData(
    type: ChartType,
    data?: DataSourceAccumulation,
    dataSource?: string,
    params?: object
  ): Observable<ChartData> {
    return of();
  }

  getParams(keys: string[], formState: any): any {
    return {};
  }

  useThemeForLabels(props: any): boolean {
    return true;
  }
}

describe('ChartDataService', () => {
  let service: ChartDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') }],
    });
    service = TestBed.inject(ChartDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
