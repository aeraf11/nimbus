import { MetaDataColumnMatch } from '@nimbus/core/src/lib/core';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable, of, Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { DataListOptions } from '../models/data-list-options';
import { FilterColumn } from '../models/filter-column';
import { Lookup } from '../models/lookup';
import { LookupNode } from '../models/lookup-node';
import { SiteConfig } from '../models/site-config';
import { TableData } from '../models/table-data';
import { MetaDataService } from './meta-data.service';
import { StateService } from './state.service';
import { deepClone } from 'deep.clone';
const SAVED_OPTIONS_KEY = 'savedOptions';

@Injectable({
  providedIn: 'root',
})
export class ListService {
  private apiUrl: string;
  private reloadListEvent = new Subject<string>();

  constructor(
    private http: HttpClient,
    private siteConfig: SiteConfig,
    private state: StateService,
    private metaDataService: MetaDataService
  ) {
    this.apiUrl = `${siteConfig.apiUrl}/list`;
  }

  getTypeAheadList(
    listName: string,
    search: string,
    sortColumn: string,
    selectColumns: string[],
    filterColumns: Array<FilterColumn>,
    resultLimit = 10
  ): Observable<any[]> {
    listName = this.checkListName(listName);
    return this.state
      .selectMetaDataKeyColumns(selectColumns, this.metaDataService.getEntityTypeIdFromListName(listName))
      .pipe(
        switchMap((metaColumns) => {
          const selectColsText = selectColumns.concat(metaColumns.map(({ uuid }) => uuid)).join('&columns=');
          let url = `${this.apiUrl}?listName=${listName}&sortColumn=${sortColumn}&direction=asc&pageIndex=0&pageSize=${resultLimit}&columns=${selectColsText}`;
          if (!filterColumns || filterColumns.length === 0) {
            url += `&filter=${search}`;
            return this.http.get<TableData>(url).pipe(
              map((data) => {
                return this.reformMetaDataCols(data.rows, metaColumns, selectColumns);
              })
            );
          } else {
            filterColumns.forEach((f) => {
              if (!f.constantFilter) {
                f.stringFilter = search;
              }
            });
            return this.http.post<TableData>(url, { filters: filterColumns }).pipe(
              map((data) => {
                return this.reformMetaDataCols(data.rows, metaColumns, selectColumns);
              })
            );
          }
        })
      );
  }

  reformMetaDataCols(rows: any, metaData: MetaDataColumnMatch[], selectColumns: string[]) {
    if (metaData) {
      rows = rows.map((row: any) => this.metaDataColConvert(row, metaData, selectColumns));
    }
    return rows;
  }

  metaDataColConvert(row: any, metaData: MetaDataColumnMatch[], selectColumns: string[]) {
    for (const prop in row) {
      if (!selectColumns.some((col) => col === prop)) {
        if (!row.hasOwnProperty(prop)) continue;
        const mdItem = metaData.find((md) => md.uuid === prop);

        if (mdItem) {
          row = { ...row, [mdItem.columnName]: row[prop] };
          delete row[mdItem.uuid];
        }
      }
    }
    return row;
  }

  getList(
    listName: string,
    selectColumns: string[],
    options: DataListOptions,
    metaList?: { targetCount: number; metaFilters: { filterColumns: FilterColumn[] }[] }
  ): Observable<ListResult> {
    listName = this.checkListName(listName);
    return this.state
      .selectMetaDataKeyColumns(selectColumns, this.metaDataService.getEntityTypeIdFromListName(listName))
      .pipe(
        switchMap((metaColumns) => {
          const selectColsText = selectColumns.concat(metaColumns.map(({ uuid }) => uuid)).join('&columns=');
          let sort = options.sortColumn;
          const metaSort = metaColumns.find((meta) => options.sortColumn === meta.columnName);
          if (metaSort) {
            sort = metaSort.uuid;
          }

          let url = `${this.apiUrl}?listName=${listName}&sortColumn=${sort}&direction=${options.sortDirection}&pageIndex=${options.pageIndex}&pageSize=${options.pageSize}&columns=${selectColsText}`;

          if (options.quickSearch) {
            url += `&quickSearch=${options.quickSearch}`;
          }
          if (metaList) {
            const metaResults: Observable<ListResult>[] = [];
            metaList.metaFilters.forEach((metaFilter) => {
              metaResults.push(this.http.post<TableData>(url, { filters: metaFilter.filterColumns }));
            });
            return forkJoin(metaResults).pipe(
              map((resultsArray: ListResult[]) => {
                const result = this.combineMetaResults(resultsArray, metaList.targetCount);
                return { ...result, rows: this.reformMetaDataCols(result.rows, metaColumns, selectColumns) };
              })
            );
          } else {
            const convertedFilters = this.convertFilterMetaNamesToGuids(options.filterColumns, metaColumns);
            return this.http
              .post<TableData>(url, { filters: convertedFilters, additionalParams: options.additionalParams })
              .pipe(
                map((resultsArray: TableData) => {
                  const rows = this.reformMetaDataCols(resultsArray.rows, metaColumns, selectColumns);
                  return { ...resultsArray, rows: rows };
                })
              );
          }
        })
      );
  }

  convertFilterMetaNamesToGuids(filters?: FilterColumn[], metaColumns?: MetaDataColumnMatch[]) {
    if (filters && metaColumns) {
      const clonedFilters = JSON.parse(JSON.stringify(filters));
      return clonedFilters?.map((filter: FilterColumn) => {
        return this.getFilterMatch(filter, metaColumns);
      });
    }
  }

  getFilterMatch(filter: FilterColumn, metaColumns: MetaDataColumnMatch[]) {
    const match = metaColumns.find((meta) => meta.columnName === filter.column);
    if (match) {
      filter.column = match.uuid;
    }
    return filter;
  }

  checkListName(listName: string): string {
    if (listName === 'Persons') {
      listName = 'PersonLink';
    }
    return listName;
  }

  combineMetaResults(results: ListResult[], targetCount: number): ListResult {
    let combined: ListResult = { filteredRowCount: 0, rowCount: 0, rows: [] };
    results.forEach((result) => {
      let filteredRowCount = combined.filteredRowCount + result.filteredRowCount;
      filteredRowCount = filteredRowCount > targetCount ? targetCount : filteredRowCount;
      const rows = [...combined.rows, ...result.rows.slice(0, targetCount - combined.filteredRowCount)];
      combined = { filteredRowCount, rowCount: filteredRowCount, rows };
    });

    return combined;
  }

  saveOptions(options: DataListOptions) {
    if (!options.id) return;
    const savedOptions = JSON.parse(localStorage.getItem(SAVED_OPTIONS_KEY) || '{}');
    savedOptions[options.id] = {
      id: options.id,
      sortColumn: options.sortColumn,
      sortDirection: options.sortDirection,
      pageSize: options.pageSize,
    };

    localStorage.setItem(SAVED_OPTIONS_KEY, JSON.stringify(savedOptions));
  }

  loadOptions(): { [key: string]: DataListOptions } {
    return <{ [key: string]: DataListOptions }>JSON.parse(localStorage.getItem(SAVED_OPTIONS_KEY) || '{}');
  }

  getHierarchy(lookupDetails: Lookup[]): Observable<LookupNode[]> {
    const topLevel = lookupDetails.filter((ld) => ld.isTopLevel);
    let result: LookupNode[] = [];

    for (const {
      lookupId,
      displayValue,
      hierarchicalDisplayValue,
      cssClass,
      isNew,
      parentLookupId,
      lookupKey,
      entityType,
    } of topLevel) {
      if (!result) result = [];
      const item = {
        lookupId,
        parentLookupId,
        displayValue,
        hierarchicalDisplayValue: hierarchicalDisplayValue ?? displayValue,
        cssClass,
        isNew,
        isLeaf: true,
        level: 0,
        lookupKey,
        entityType,
      };
      result.push({
        item,
        children: this.getHierachyChildren(lookupDetails, parentLookupId || '', item, 1),
      });
    }
    return of(result);
  }

  getHierachyChildren(lookupDetails: Lookup[], lookupId: string, parent: Lookup, level: number): LookupNode[] {
    const children = lookupDetails.filter((ld) => ld.parentLookupId === lookupId && !ld.isTopLevel);
    if (children.length > 0) parent.isLeaf = false;
    return children.map(
      ({ lookupId, displayValue, hierarchicalDisplayValue, cssClass, isNew, lookupKey, entityType }) => {
        const item = {
          lookupId,
          displayValue,
          hierarchicalDisplayValue: hierarchicalDisplayValue ?? displayValue,
          cssClass,
          isNew,
          isLeaf: true,
          level,
          lookupKey,
          entityType,
        };
        return {
          item,
          children: this.getHierachyChildren(lookupDetails, lookupId, item, level + 1),
        };
      }
    );
  }

  reloadList(id: string): void {
    this.reloadListEvent.next(id);
  }

  reloadEvent(): Observable<string> {
    return this.reloadListEvent.asObservable();
  }
}

export interface ListResult {
  filteredRowCount: number;
  rowCount: number;
  rows: ListResultRow[];
}

export interface ListResultRow {
  createdBy?: string;
}
