import { TestBed } from '@angular/core/testing';

import { DocumentService } from './document.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SiteConfig } from '../models';

describe('DocumentService', () => {
  let service: DocumentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') }],
    });
    service = TestBed.inject(DocumentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
