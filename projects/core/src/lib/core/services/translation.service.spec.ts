﻿import { TestBed } from '@angular/core/testing';
import { TranslationService } from './translation.service';
import { StateService } from './state.service';
import { of } from 'rxjs';
import { MockStateService, Translations } from '@nimbus/core/src/lib/core';

describe('TranslationService', () => {
  let translationService: TranslationService;
  const mockStateService = new MockStateService();
  const mockTranslationState: Translations = {
    _root: {
      blank: 'blank',
    },
    operation: {
      name: 'test',
    },
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: StateService, useValue: mockStateService }],
    });
  });

  it('should create', () => {
    translationService = TestBed.inject(TranslationService);

    expect(translationService).toBeTruthy();
  });

  describe('constructor', () => {
    it('should save translations to translationState', () => {
      spyOn(mockStateService, 'selectAllTranslations').and.returnValue(of(mockTranslationState));

      translationService = TestBed.inject(TranslationService);

      expect(translationService.translationState).toEqual(mockTranslationState);
    });

    it('should assign getTranslation() to $getTranslation', () => {
      TestBed.inject(TranslationService);

      expect($getTranslation).toBeDefined();
    });
  });

  describe('getTranslation()', () => {
    it("returns 'test' when it is passed contextKey of 'operation' and token of 'name'", () => {
      translationService = TestBed.inject(TranslationService);
      translationService.translationState = mockTranslationState;

      const result = translationService.getTranslation('operation', 'name');

      expect(result).toEqual('test');
    });

    it('returns warning when it is an incorrect contextKey', () => {
      translationService = TestBed.inject(TranslationService);
      translationService.translationState = mockTranslationState;

      const result = translationService.getTranslation('test', '');

      expect(result).toEqual('ERROR: Incorrect context');
    });
  });
});
