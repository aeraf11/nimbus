import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { NEVER, Observable, of } from 'rxjs';
import { MetaDataItem } from '../models/meta-data-item';
import { SiteConfig } from '../models/site-config';
import { MetaDataService } from './meta-data.service';
import { StateService } from './state.service';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { utils } from '../utils';
import { LookupService } from './lookup.service';
import { MockLookupService } from './lookup.service.spec';

export class MockMetaDataService {
  readValue(item: MetaDataItem): string | object | null {
    return item.Value;
  }

  getDefaultMetaDataForEntities(entityIds: number[]): Observable<Record<number, string>> {
    const record: Record<number, string> = {
      1: '',
    };
    return of(record);
  }

  getMetaDataForEntity(entityId: string, entityType: number): Observable<string | undefined> {
    return of('');
  }

  parseMetaJson(metaJson: string | undefined): MetaDataItem[] {
    return [];
  }
}

describe('MetaDataService', () => {
  let service: MetaDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: SiteConfig, useValue: new SiteConfig('/api/v2', 'someInitUrl') },
        { provide: StateService, useClass: MockStateService },
        { provide: LookupService, useClass: MockLookupService },
      ],
    });
    service = TestBed.inject(MetaDataService);
  });

  it('should be created (MetaDataService)', () => {
    expect(service).toBeTruthy();
  });

  describe('readValue()', () => {
    it('returns meta datas Value if no type is specified on the Item', () => {
      // Arrange
      const someValue = 'value';
      const someMetaData = {
        Label: 'label',
        Value: someValue,
      } as MetaDataItem;

      // Act
      const result = service.readValue(someMetaData);

      // Assert
      expect(result).toEqual(someValue);
    });

    it('returns camelCase DateTime object if the ItemType is Date or DateTime', () => {
      // Arrange
      const someValueBefore = '{ "DateTime": "dt", "TimeZoneId": "tz" }';
      const someValueAfter = { dateTime: 'dt', timeZoneId: 'tz' };

      const someDateMetaData = {
        Label: 'label',
        Value: someValueBefore,
        ItemType: 7, // Date
      } as MetaDataItem;

      const someDateTimeMetaData = {
        Label: 'label',
        Value: someValueBefore,
        ItemType: 8, // Date
      } as MetaDataItem;

      // Act
      const resultDate = service.readValue(someDateMetaData);
      const resultDateTime = service.readValue(someDateTimeMetaData);

      // Assert
      expect(resultDate).toEqual(someValueAfter);
      expect(resultDateTime).toEqual(someValueAfter);
    });

    it('returns items Value if Date or DateTime if the value cannot be JSON parsed', () => {
      // Arrange
      const someValue = '';
      const someMetaData = {
        Label: 'label',
        Value: someValue,
        ItemType: 7,
      } as MetaDataItem;

      // Act
      const result = service.readValue(someMetaData);

      // Assert
      expect(result).toEqual(someValue);
    });
  });

  describe('getDefaultMetaDataForEntities()', () => {
    it('calls the endpoint with list of ids', () => {
      // Arrange
      const http = TestBed.inject(HttpClient);
      const someElementIds = [1, 2];
      spyOn(http, 'post').and.callFake(() => NEVER);

      // Act
      service.getDefaultMetaDataForEntities(someElementIds);

      // Assert
      expect(http.post).toHaveBeenCalledWith('/api/v2/metadata/defaults', someElementIds);
    });
  });

  describe('getMetaDataForEntity()', () => {
    it('calls the endpoint with entity id and entity type', () => {
      // Arrange
      const http = TestBed.inject(HttpClient);
      const someEntityId = 'id';
      const someEntityType = 2;
      spyOn(http, 'post').and.callFake(() => NEVER);

      // Act
      service.getMetaDataForEntity(someEntityId, someEntityType);

      // Assert
      expect(http.post).toHaveBeenCalledWith('/api/v2/metadata/entity', {
        entityId: someEntityId,
        entityType: someEntityType,
      });
    });
  });

  describe('parseMetaJson()', () => {
    it('returns empty array if metaJson argument is falsy', () => {
      // Arrange
      const someArgument = '';

      // Act
      const result = utils.parseMetaJson(someArgument);

      // Assert
      expect(result).toEqual([]);
    });

    it('returns the Items property as meta JSON often comes in a nested Items array', () => {
      // Arrange
      const someMetaCollection = { Items: [{} as MetaDataItem] };
      const someArgument = JSON.stringify(someMetaCollection);

      // Act
      const result = utils.parseMetaJson(someArgument);

      // Assert
      expect(result.length).toEqual(1);
    });

    it('returns the parsed object if there is no Items property', () => {
      // Arrange
      const someArgument = JSON.stringify([{} as MetaDataItem, {} as MetaDataItem]);

      // Act
      const result = utils.parseMetaJson(someArgument);

      // Assert
      expect(result.length).toEqual(2);
    });

    it('throws an error if the argument cannot be parsed', () => {
      // Arrange
      const someInvalidJson = '{test": true';

      // Act & Assert
      expect(function () {
        utils.parseMetaJson(someInvalidJson);
      }).toThrowError('Could not parse MetaData JSON object');
    });
  });
});
