import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { SiteConfig } from '../models';
import { BlobService } from './blob.service';

export class MockBlobService {
  getBlob(url: string): Observable<Blob> {
    return of();
  }

  downloadBlob(blob: Blob, name: string): void {
    // void
  }
}

describe('BlobService', () => {
  let service: BlobService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') }],
    });
    service = TestBed.inject(BlobService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
