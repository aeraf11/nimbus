import { DialogService } from './dialog.service';
import { ApplicationRef, Injectable, OnDestroy } from '@angular/core';
import { SwUpdate, VersionReadyEvent } from '@angular/service-worker';
import { concat, interval, Subject } from 'rxjs';
import { filter, first, map, take, takeUntil, tap } from 'rxjs/operators';
import { ServiceWorkerConfig } from '../models/service-worker-config';
import { DialogConfig, DialogType } from '../models';

@Injectable({
  providedIn: 'root',
})
export class SwUpdatesService implements OnDestroy {
  private onDestroyed$ = new Subject<void>();
  private updateCheckIntervalFallback = 1000 * 60 * 60 * 6; // 6 hours
  private swConfig: ServiceWorkerConfig | undefined;
  private isUpdateApplying = false;

  constructor(private swu: SwUpdate, private appRef: ApplicationRef, private dialog: DialogService) {}

  start(swConfig: ServiceWorkerConfig | undefined): void {
    this.swConfig = swConfig;

    if (this.swu.isEnabled) {
      this.checkForUpdatesOnInterval();
      this.onUpdate();
    }
  }

  ngOnDestroy(): void {
    this.onDestroyed$.next();
    this.onDestroyed$.complete();
  }

  private checkForUpdatesOnInterval(): void {
    const isAppStable$ = this.appRef.isStable.pipe(first((v) => v));
    const updateCheckInterval = this.swConfig?.updateCheckInterval ?? this.updateCheckIntervalFallback;

    concat(isAppStable$, interval(updateCheckInterval))
      .pipe(
        tap(() => console.info('[Service Worker] Checking for update...')),
        takeUntil(this.onDestroyed$)
      )
      .subscribe(() => this.swu.checkForUpdate());
  }

  private onUpdate(): void {
    this.swu.versionUpdates
      .pipe(
        filter((evt): evt is VersionReadyEvent => evt.type === 'VERSION_READY'),
        map((evt) => ({
          type: 'UPDATE_AVAILABLE',
          current: evt.currentVersion,
          available: evt.latestVersion,
        })),
        tap((evt) => console.info('[Service Worker] Update available!', evt)),
        takeUntil(this.onDestroyed$)
      )
      .subscribe(() => {
        if (!this.isUpdateApplying) {
          this.applyUpdate();
        }
      });
  }

  private applyUpdate(): void {
    if (this.swConfig?.showUpdateMessage) {
      this.isUpdateApplying = true;

      this.dialog
        .openDialog(<DialogConfig>{
          title: this.swConfig?.updateMessageTitle,
          bodyText: this.swConfig?.updateMessage,
          type: DialogType.OKCancel,
        })
        .pipe(take(1))
        .subscribe((data: boolean) => {
          if (data) {
            this.swu.activateUpdate().then(() => {
              this.isUpdateApplying = false;
              document.location.reload();
            });
          } else {
            this.isUpdateApplying = false;
          }
        });
    }
  }
}
