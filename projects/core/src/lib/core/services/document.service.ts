import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SiteConfig } from './../models/site-config';
import { EntitySource } from '../models/entity-source';
import { Observable } from 'rxjs';
import { DocumentUploadResult } from '../models/document-upload-result';
import { DocumentData } from '../models/document-data';
import { DocumentUploadModel } from '../models/document-upload-data';

@Injectable({
  providedIn: 'root',
})
export class DocumentService {
  private apiUrl: string;

  constructor(private http: HttpClient, siteConfig: SiteConfig) {
    this.apiUrl = siteConfig.apiUrl + /document/;
  }

  public getDocument(entitySource: EntitySource, id: string): Observable<DocumentData> {
    if (this.documentIdValid(id)) {
      return this.http.get<DocumentData>(`${this.apiUrl}?source=${entitySource}&id=${encodeURIComponent(id)}`);
    } else {
      return new Observable<DocumentData>((observer) => {
        observer.next({ source: entitySource, id: id, fileName: '', fileType: '', fileData: '' });
        observer.complete();
      });
    }
  }

  public saveDocument(
    entitySource: EntitySource,
    id: string,
    fileData: string,
    reasonText = '',
    checkIn = true
  ): Observable<DocumentUploadResult> {
    if (this.fileDataValid(fileData)) {
      const model: DocumentUploadModel = {
        source: entitySource,
        id: id,
        fileData: fileData,
        reason: reasonText,
        checkIn: checkIn,
      };
      return this.http.post<DocumentUploadResult>(this.apiUrl, model);
    } else {
      return new Observable<DocumentUploadResult>((observer) => {
        observer.next({ source: entitySource, id: id, fileName: '' });
        observer.complete();
      });
    }
  }

  private documentIdValid(id: string): boolean {
    return id !== undefined && id !== '';
  }

  private fileDataValid(fileData: string): boolean {
    return fileData !== undefined && fileData !== '';
  }
}
