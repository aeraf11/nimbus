import { TestBed } from '@angular/core/testing';
import { IndexedDbService } from './indexeddb.service';

export class MockIndexedDbService {
  init(): void {
    // do nothing
  }

  table(tableName: string): any {
    return this;
  }

  where(where: any): any {
    return this;
  }

  anyOf(anyOf: any): any {
    return this;
  }

  equals(equals: any): any {
    return this;
  }

  delete(): Promise<any> {
    return new Promise(() => []);
  }

  toArray(): Promise<any> {
    return new Promise(() => []);
  }

  toCollection(toCollection: any): any {
    return this;
  }

  sortBy(): Promise<any> {
    return new Promise(() => []);
  }
}

describe('IndexedDbService', () => {
  let service: IndexedDbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IndexedDbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
