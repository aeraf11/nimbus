import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SiteConfig } from '../models';

@Injectable({
  providedIn: 'root',
})
export class BlobService {
  private apiUrl: string;

  constructor(private http: HttpClient, private siteConfig: SiteConfig) {
    this.apiUrl = siteConfig.apiUrl;
  }

  getBlob(url: string): Observable<Blob> {
    return this.http.get(`${this.apiUrl}${url}`, { responseType: 'blob' });
  }

  downloadBlob(blob: Blob, name: string): void {
    const blobUrl = URL.createObjectURL(blob);
    const link = document.createElement('a');

    link.href = blobUrl;
    link.download = name;

    document.body.appendChild(link);

    link.dispatchEvent(
      new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
        view: window,
      })
    );

    document.body.removeChild(link);
  }
}
