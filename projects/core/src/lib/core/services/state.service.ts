import { Injectable } from '@angular/core';
import { Dictionary } from '@ngrx/entity';
import { TimeZone } from '@nimbus/material';
import { Observable } from 'rxjs';
import {
  ApplicationInsightsConfig,
  CardOptions,
  DataListOptions,
  DebugOptions,
  DialogFormAction,
  DynamicRoute,
  FileUploadConfig,
  FormDefinition,
  Lookup,
  LookupDetail,
  LookupDictionary,
  LookupHierarchy,
  LookupNode,
  MetaDataColumnMatch,
  MetaDataItem,
  ScheduledJob,
  Theme,
  Translations,
  User,
} from '../models';
import { SignalRHub } from './../models/app-configuration';

@Injectable({
  providedIn: 'root',
})
export abstract class StateService {
  abstract selectAppInsightsConfig(): Observable<ApplicationInsightsConfig | undefined>;
  abstract selectConnectionOnline(): Observable<boolean>;
  abstract selectCurrentBreakpoint(): Observable<'sm' | 'md' | 'lg'>;
  abstract selectCurrentDynamicForm(): Observable<FormDefinition | undefined>;
  abstract selectCurrentDynamicRoute(): Observable<DynamicRoute | undefined>;
  abstract selectCurrentRoute(): Observable<any>;
  abstract selectDashboardLayout(): Observable<string>;
  abstract selectDataListOptions(savedOptionsId: string): Observable<DataListOptions>;
  abstract selectDateTimeFormats(): Observable<{ dateFormat?: string; timeFormat?: string; timeZoneId?: string }>;
  abstract selectDebugOptions(): Observable<DebugOptions | undefined>;
  abstract selectDefaultMetaDataByEntityTypeId(id: number): Observable<MetaDataItem[]>;
  abstract selectDynamicRouteCurrentError(): Observable<string | null>;
  abstract selectDynamicRouteCurrentModel(): Observable<any>;
  abstract selectFileUploadConfig(): Observable<FileUploadConfig | undefined>;
  abstract selectForm(formId: string): Observable<FormDefinition | undefined>;
  abstract selectFormStateSettings(): Observable<{ [key: string]: any } | undefined>;
  abstract selectFormSubmissionResult(): Observable<{ success: boolean; errorMessage: string | null } | null>;
  abstract selectLookupDetailEntities(): Observable<Dictionary<LookupDetail>>;
  abstract selectLookupsFlat(): Observable<LookupDictionary>;
  abstract selectLookupsFlatFiltered(filters: string[]): Observable<Lookup[]>;
  abstract selectLookupsForList(filters: string[]): Observable<LookupNode[]>;
  abstract selectLookupsHierarchy(): Observable<LookupHierarchy>;
  abstract selectLookupsHierarchyFiltered(filters: string[]): Observable<LookupNode[]>;
  abstract selectMenuState(): Observable<'open' | 'closed'>;
  abstract selectMetaDataKeyColumns(listColumns: string[], entityType: string): Observable<MetaDataColumnMatch[]>;
  abstract selectRoutesWithPermission(routes: any[]): any;
  abstract selectScheduledJob(id: string): Observable<ScheduledJob | undefined>;
  abstract selectScheduledJobs(): Observable<ScheduledJob[]>;
  abstract selectSelectedThemeIsDark(): Observable<boolean | undefined>;
  abstract selectSignalRConfig(): Observable<SignalRHub[] | undefined>;
  abstract selectSiteId(): Observable<string | undefined>;
  abstract selectThemes(): Observable<Theme[] | undefined>;
  abstract selectTimeZones(): Observable<TimeZone[] | null>;
  abstract selectUpgradeConfigMessage(): Observable<string | undefined>;
  abstract selectUser(): Observable<User | null>;
  abstract selectWorkOnline(): Observable<boolean>;
  abstract selectAllTranslations(): Observable<Translations>;
  abstract selectEnableFormTranslations(): Observable<boolean | undefined>;

  abstract mergeDynamicRouteModel(
    model: any,
    routeId: string,
    props: {
      [x: string]: any;
    },
    autoFillModelOverrides: any[] | undefined,
    autoFillModelSkipProps: any[] | undefined
  ): void;

  abstract updateDynamicRouteModel(model: unknown): void;

  abstract saveOptions(options: DataListOptions): void;
  abstract saveCardOptions(options: CardOptions): void;
  abstract saveBookmark(routeId: string, bookmark: any): void;
  abstract setLoading(loading: boolean): void;
  abstract loadCurrentDynamicRouteModel(routeId: string, context: any): void;
  abstract loadCurrentDynamicRouteModelOffline(routeId: string, context: any): void;
  abstract postForm(model: any, routeId: string, onSuccess?: () => void, showErrorSnack?: boolean): void;

  abstract postFormOffline(model: any, context: any, routeId: string): void;
  abstract clearCurrentDynamicRouteModel(): void;
  abstract addLookup(lookup: LookupDetail): void;
  abstract submitFormClear(): void;
  abstract submitForm(model: any, formAction: DialogFormAction, formId: string, context: any): void;
  abstract setDashboardLayout(dashboardLayout: string): void;
  abstract setTheme(themeId: string): void;
  abstract upgradeConfig(): void;
}
