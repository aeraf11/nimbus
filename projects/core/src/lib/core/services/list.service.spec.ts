import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { DataListOptions, FilterColumn, Lookup, LookupNode, SiteConfig } from '../models';
import { ListResult, ListService } from './list.service';
import { StateService } from './state.service';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { MetaDataService } from './meta-data.service';
import { MockMetaDataService } from './meta-data.service.spec';
import { LookupService } from './lookup.service';
import { MockLookupService } from './lookup.service.spec';

export class MockListService {
  getTypeAheadList(
    listName: string,
    search: string,
    sortColumn: string,
    selectColumns: string[],
    filterColumn: Array<FilterColumn>,
    resultLimit = 10
  ): Observable<any[]> {
    return of([]);
  }

  getList(
    listName: string,
    selectColumns: string[],
    options: DataListOptions,
    metaList?: { targetCount: number; metaFilters: { filterColumns: FilterColumn[] }[] }
  ): Observable<ListResult> {
    return of({ filteredRowCount: 0, rowCount: 0, rows: [] });
  }

  combineMetaResults(results: ListResult[], targetCount: number): ListResult {
    return { filteredRowCount: 0, rowCount: 0, rows: [] };
  }

  saveOptions(options: DataListOptions) {
    // void
  }

  loadOptions(): { [key: string]: DataListOptions } {
    return <{ [key: string]: DataListOptions }>JSON.parse('{}');
  }

  getHierarchy(lookupDetails: Lookup[]): Observable<LookupNode[]> {
    return of([]);
  }

  getHierachyChildren(lookupDetails: Lookup[], lookupId: string, parent: Lookup, level: number): LookupNode[] {
    return [];
  }

  reloadList(listName: string): void {
    // void
  }

  reloadEvent(): Observable<string> {
    return of('reload event');
  }
}

describe('ListService', () => {
  let service: ListService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
        { provide: StateService, useClass: MockStateService },
        { provide: MetaDataService, useClass: MockMetaDataService },
        { provide: LookupService, useClass: MockLookupService },
      ],
    });
    service = TestBed.inject(ListService);
  });

  it('should be created (ListService)', () => {
    expect(service).toBeTruthy();
  });
});
