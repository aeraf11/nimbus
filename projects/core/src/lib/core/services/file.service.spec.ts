import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { SiteConfig } from '../models';
import { FileService } from './file.service';

export class MockFileService {
  combineChunks(
    fileName: string,
    chunkIds: string[]
  ): Observable<{ fileId: string; success: boolean; errorMessage: string }> {
    return of({
      fileId: 'id',
      success: true,
      errorMessage: '',
    });
  }
}

describe('FileService', () => {
  let service: FileService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') }],
    });
    service = TestBed.inject(FileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
