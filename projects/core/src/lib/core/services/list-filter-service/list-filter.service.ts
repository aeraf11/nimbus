import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { DialogFilterComponent, DialogFilterConfig, DialogFilterData } from '@nimbus/core/src/lib/core';

@Injectable({
  providedIn: 'root',
})
export class ListFilterService {
  constructor(private dialog: MatDialog) {}

  openDialog(config: DialogFilterConfig, options?: any): MatDialogRef<any> | undefined {
    const cols = config.columnDefinitions;
    const isDisabled = config.isDisabled;

    if (cols && !isDisabled) {
      const filterableCols = cols.filter((col: any) => col.filterParams || col.filterType);

      const dialogConfig: MatDialogConfig<DialogFilterData> = {
        data: {
          config: {
            filterText: config.filterText ?? '',
            cancelText: config.cancelText ?? '',
            clearText: config.clearText ?? '',
            title: config.title ?? '',
            columnDefinitions: filterableCols,
            lastFilters: config.lastFilters,
          },
          disabled: isDisabled ?? false,
          options: options,
        },
        width: config.width ?? '',
        minWidth: '',
        maxWidth: '',
      };

      return this.dialog.open(DialogFilterComponent, dialogConfig);
    }

    return undefined;
  }
}
