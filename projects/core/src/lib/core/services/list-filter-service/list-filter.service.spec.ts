import { TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { ListFilterService } from './list-filter.service';

export class MockListFilterService {
  openDialog(): MatDialogRef<any> | undefined {
    return undefined;
  }
}

describe('ListFilterService', () => {
  let service: ListFilterService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule],
    });
    service = TestBed.inject(ListFilterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
