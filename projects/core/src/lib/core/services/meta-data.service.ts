import { filter } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { combineLatest, map, Observable, of, switchMap, take } from 'rxjs';
import { MetaDataKey } from '../models/meta-data-key';
import { utils } from '../utils';
import { MetaDataAttachmentResponse } from './../models/meta-data-attachment';
import { MetaDataItem, MetaDataItemType } from './../models/meta-data-item';
import { SiteConfig } from './../models/site-config';
import { StateService } from './state.service';
import { LookupService } from './lookup.service';

@Injectable({
  providedIn: 'root',
})
export class MetaDataService {
  private readonly defaultsUrl = `${this.siteConfig.apiUrl}/metadata/defaults`;
  private readonly entityUrl = `${this.siteConfig.apiUrl}/metadata/entity`;
  private readonly attachmentUrl = `${this.siteConfig.apiUrl}/metadata/attachment`;
  private readonly conditionalUrl = `${this.siteConfig.apiUrl}/metadata/conditional`;
  private readonly keysUrl = `${this.siteConfig.apiUrl}/metadata/keys`;
  private listNameEntityTypeIdMap = new Map<string, string>([
    ['Actions', '61'],
    ['Attachments', '65'],
    ['Courses', '59'],
    ['CourseTemplates', '60'],
    ['CrimeScenes', '78'],
    ['DataSource', '103'],
    ['EntityAlert', '102'],
    ['EntityCalendarItem', '82'],
    ['EntityMedia', '25'],
    ['Examination', '85'],
    ['Exhibits', '3'],
    ['MetaObject', '51'],
    ['Notes', '39'],
    ['Operations', '1'],
    ['MyOperations', '1'],
    ['Persons', '23'],
    ['PersonGrouped', '23'],
    ['PersonLink', '23'],
    ['Submissions', '2'],
    ['ThirdPartyProvider', '50'],
    ['Users', '13'],
    ['ExhibitTypeId', '8'],
  ]);

  constructor(
    private http: HttpClient,
    private siteConfig: SiteConfig,
    private state: StateService,
    private lookup: LookupService
  ) {}

  readValue(item: MetaDataItem): string | object | null {
    if (!item || item.Value === null || item.Value === undefined) {
      return null;
    }

    switch (item.ItemType) {
      case 7: // Date
      case 8: // DateTime
        try {
          const dateTimeObj = JSON.parse(item.Value);
          return dateTimeObj ? utils.toCamel(dateTimeObj) : item.Value;
        } catch {
          return item.Value;
        }
      default:
        return item.Value;
    }
  }

  loadMetaDataCacheKeys(): Observable<MetaDataKey[]> {
    return this.http.get<MetaDataKey[]>(this.keysUrl).pipe(
      map((keys) => {
        return keys.map((key) => ({ ...key, entityTypeLabelId: key.entityTypeId + key.label }));
      })
    );
  }

  getDefaultMetaDataForEntities(entityIds: number[]): Observable<{ [key: number]: string }> {
    return this.http.post<{ [key: number]: string }>(this.defaultsUrl, entityIds);
  }

  getMetaDataForEntity(entityId: string, entityType: number): Observable<string | undefined> {
    if (!entityId) {
      return of('');
    }
    return this.http.post<string | undefined>(this.entityUrl, { entityId, entityType });
  }

  createMetaDataAttachment(
    name: string,
    size: number,
    blobId: string
  ): Observable<MetaDataAttachmentResponse | undefined> {
    if (!name || !size) {
      return of(undefined);
    }

    const formData = new FormData();
    formData.append('fileName', name);
    formData.append('fileSize', size.toString());
    formData.append('blobId', blobId);
    return this.http.post<MetaDataAttachmentResponse>(this.attachmentUrl, formData);
  }

  getMetaData(entityType: number, entityId?: string, isEdit = true): Observable<MetaDataItem[]> {
    return combineLatest([
      this.state.selectDefaultMetaDataByEntityTypeId(entityType),
      this.getMetaDataForEntity(entityId ?? '', entityType),
    ]).pipe(
      map(([defaultMeta, entityJson]) => {
        const entityMeta = utils.parseMetaJson(entityJson);
        const meta = entityMeta && entityMeta.length > 0 ? entityMeta : defaultMeta;
        return this.filterMetaData(meta, isEdit);
      }),
      take(1)
    );
  }

  getEntityTypeIdFromListName(listName: string): string {
    return this.listNameEntityTypeIdMap.get(listName) || '';
  }

  // This makes it so we can change a meta data item into something that the Phaneros model
  // understands. e.g. a MetaDataItem like { "Label": "Hello", "Value": "Goodbye" } will become
  // "hello": "goodbye". As this uses our readValue function, dates will be properly parsed.
  formatMetaDataForModel(metaData: MetaDataItem[]): object {
    const initialValue = {};

    if (!metaData || metaData.length < 1) {
      return initialValue;
    }

    return metaData?.reduce((obj, item) => {
      const formattedLabel = utils.stringToCamel(item['Label']);
      const value = this.readValue(item) ?? item.ValueData;
      return {
        ...obj,
        [formattedLabel]: value,
      };
    }, initialValue);
  }

  mergeMetaData(baseItems: MetaDataItem[], newItems: MetaDataItem[], isEdit: boolean): MetaDataItem[] {
    const labels = new Set(baseItems.map((item) => item.Label));

    const filteredBase = this.filterMetaData(baseItems, isEdit);

    const filteredNew = this.filterMetaData(newItems, isEdit).filter((item) => !labels.has(item.Label));

    return [...filteredBase, ...filteredNew];
  }

  filterMetaData(metaData: MetaDataItem[], isEdit: boolean): MetaDataItem[] {
    return metaData.filter(
      (item) =>
        (item.IsVisibleInPhanerosAdd && isEdit) ||
        (item.IsVisibleInPhanerosView && !isEdit) ||
        item.ItemType === MetaDataItemType.DisplayGroup
    );
  }

  getConditionalMetadata(
    conditionalValue: string,
    entityMetaDataItems: MetaDataItem[] = [],
    isEdit = true
  ): Observable<MetaDataItem[]> {
    if (!conditionalValue) {
      return of(entityMetaDataItems);
    }
    return this.lookup.getHierarchicalIdentifiers(conditionalValue).pipe(
      take(1),
      switchMap((identifiers) => {
        return this.http.post<MetaDataItem[]>(this.conditionalUrl, identifiers).pipe(
          map((conditionalMetaDataItems) => {
            return this.mergeMetaData(entityMetaDataItems, conditionalMetaDataItems, isEdit);
          })
        );
      })
    );
  }
}
