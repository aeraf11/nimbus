import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ColourService {
  colours: string[] = [];
  private lookup = new Map<string, string>();
  private readonly classNames = [
    'c-neg-1',
    'c-neg-2',
    'c-neg-3',
    'c-neg-4',
    'c-neg-5',
    'c-neu-1',
    'c-neu-2',
    'c-neu-3',
    'c-neu-4',
    'c-neu-5',
    'c-neu-6',
    'c-neu-7',
    'c-pos-1',
    'c-pos-2',
    'c-pos-3',
    'c-pos-4',
    'c-pos-5',
  ];

  constructor() {
    this.colours = this.classNames.map((className: string) => this.getClassBackgoundColour(className) ?? '');
  }

  getClassBackgoundColour(cssClass: string): string | undefined {
    if (!cssClass) return undefined;
    if (this.lookup.has(cssClass)) return this.lookup.get(cssClass);

    const testElement = document.createElement('div');
    document.body.append(testElement);
    testElement.classList.add(cssClass);
    const result = getComputedStyle(testElement).backgroundColor;
    document.body.removeChild(testElement);
    this.lookup.set(cssClass, result);

    return result;
  }
}
