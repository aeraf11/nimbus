import { TestBed } from '@angular/core/testing';
import { StateService } from '@nimbus/core/src/lib/core';
import { Observable } from 'rxjs';
import { SignalRHub } from '../models/app-configuration';
import { SignalRService } from './signal-r.service';
import { MockStateService } from '@nimbus/core/src/lib/core';

export class MockSignalRService {
  setAccessToken(accessToken: string) {
    // void
  }

  setUserProfileId(userProfileId: string) {
    // void
  }

  setConfig(hubs: SignalRHub[]) {
    // void
  }

  init() {
    // void
  }

  getEvent<T>(hubName: string, eventName: string, group?: string): Observable<T> | undefined {
    return undefined;
  }

  groupMatch(payload: any, group: string) {
    // void
  }

  leaveGroup(hubName: string, group: string) {
    // void
  }

  incrementGroup(group: string, groups: Map<string, number>) {
    // void
  }
}

describe('SignalRService', () => {
  let service: SignalRService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: StateService, useClass: MockStateService }],
    });
    service = TestBed.inject(SignalRService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
