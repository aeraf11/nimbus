import { SiteConfig } from '../models';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { EntityChatItem } from '../models';

@Injectable({
  providedIn: 'root',
})
export class ChatService {
  private apiUrl: string;

  constructor(private http: HttpClient, private siteConfig: SiteConfig) {
    this.apiUrl = `${siteConfig.apiUrl}/chats`;
  }

  addMessage(entityChatItem: EntityChatItem): Observable<boolean> {
    return this.http.post<boolean>(this.apiUrl, entityChatItem);
  }
}
