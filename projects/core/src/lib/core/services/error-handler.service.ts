import { ErrorHandler, Injectable } from '@angular/core';
import { AppInsightsService } from '..';
@Injectable({
  providedIn: 'root',
})
export class ErrorHandlerService extends ErrorHandler {
  constructor(private appInsightsService: AppInsightsService) {
    super();
  }

  override handleError(error: any): void {
    if (this.appInsightsService.isTracking) {
      this.appInsightsService.logException(error);
    } else {
      super.handleError(error);
    }
  }
}
