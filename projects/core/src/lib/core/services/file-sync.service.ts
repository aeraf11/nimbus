import { Injectable } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { FileChunk } from '../models/file-chunk';
import { FileRecombinationProgressChanged } from '../models/file-recombination-progress-change';
import { StorageType } from '../models/storage-type';
import { FileService } from './file.service';
import { IndexedDbService } from './indexeddb.service';
import { SignalREvents, SignalRHubs, SignalRService } from './signal-r.service';

export interface FileRecombination {
  id: string;
  model: any;
  attachmentId: string;
  fileId: string;
  setStatus?: (attachmentId: string, status: string) => void;
  onComplete?: (model: any, attachmnetId: string, fileId: string) => void;
  setProgress?: (progress: number) => void;
  setError?: (message: string) => void;
}
@Injectable({
  providedIn: 'root',
})
export class FileSyncService {
  private recombinations: FileRecombination[] = [];
  private checking = false;
  private enablePolling = false;

  constructor(private db: IndexedDbService, private fileService: FileService, private signalR: SignalRService) {
    window.setInterval(() => this.checkProgress(), 5000);
  }

  async processAttachment(
    attachmentId: string,
    file: any,
    chunkTableName: string,
    setStatus: (attachmentId: string, status: string) => void,
    storageType = StorageType.Attachment
  ): Promise<{ success: boolean; fileId?: string; errorMessage?: string; fileRecombinationId?: string }> {
    const chunkIds = await (
      await this.db.table<FileChunk>(chunkTableName).where('fileId').equals(file.fileId).toArray()
    ).map((chunk) => chunk.id);

    const count = chunkIds.length;

    for (let i = 0; i < count; i++) {
      const chunkId = chunkIds[i];
      const uploadResult = await this.processChunk(chunkId, file.name, count > 1, chunkTableName, storageType);
      if (count === 1 || !uploadResult.success) {
        return uploadResult;
      } else {
        await this.db
          .table<FileChunk>(chunkTableName)
          .where('id')
          .equals(chunkId)
          .modify({ uploadId: uploadResult.fileId });
        if (setStatus) {
          await setStatus(attachmentId, `Uploading: ${Math.round((i / count) * 100)}%`);
        }
      }
    }

    if (setStatus) {
      await setStatus(attachmentId, `Processing upload: 0%`);
    }
    return await this.combineChunks(file.fileId, file.name, chunkTableName, storageType);
  }

  async processChunk(
    chunkId: number,
    fileName: string,
    isChunk: boolean,
    chunkTableName: string,
    storageType = StorageType.Attachment
  ): Promise<{ success: boolean; fileId: string; errorMessage: string }> {
    const chunk = await this.db.table<FileChunk>(chunkTableName).where('id').equals(chunkId).first();
    return await firstValueFrom(this.fileService.uploadBlob(chunk?.blob || new Blob(), fileName, isChunk, storageType));
  }

  async combineChunks(
    fileId: string,
    fileName: string,
    chunkTableName: string,
    storageType: StorageType
  ): Promise<{ fileId?: string; success: boolean; errorMessage?: string; fileRecombinationId: string }> {
    const uploadIds = await (
      await this.db.table<FileChunk>(chunkTableName).where('fileId').equals(fileId).toArray()
    ).map((chunk) => chunk.uploadId);

    return await firstValueFrom(this.fileService.recombineQueue(fileName, uploadIds, storageType));
  }

  addRecombinations(recombination: FileRecombination) {
    this.recombinations.push(recombination);

    const event = this.signalR.getEvent<FileRecombinationProgressChanged>(
      SignalRHubs.notifyUser,
      SignalREvents.onFileRecombinationProgressChanged,
      recombination.id
    );
    if (event) {
      const sub = event.subscribe((progress) => {
        if (progress.progress === 100) {
          this.signalR.leaveGroup(SignalRHubs.notifyUser, recombination.id);
          sub.unsubscribe();
        }
        this.processProgress(recombination, progress);
      });
    } else {
      this.enablePolling = true;
    }
  }

  checkProgress() {
    if (this.enablePolling && this.recombinations && this.recombinations.length > 0 && !this.checking) {
      this.checking = true;
      this.recombinations.forEach(async (recombination) => {
        const progress = await firstValueFrom(this.fileService.recombineProgress(recombination.id));
        await this.processProgress(recombination, progress);
      });
      this.checking = false;
    }
  }

  async processProgress(recombination: FileRecombination, progress: FileRecombinationProgressChanged) {
    if (!progress.failed) {
      if (progress.progress === 100) {
        if (recombination.onComplete) {
          await recombination.onComplete(
            { ...recombination.model, blobId: progress.fileId },
            recombination.attachmentId,
            recombination.fileId
          );
        }
        this.recombinations = this.recombinations.filter((r) => r !== recombination);
      } else {
        if (recombination.setStatus) {
          await recombination.setStatus(recombination.attachmentId, `Processing upload: ${progress.progress}%`);
        }
        if (recombination.setProgress) {
          recombination.setProgress(progress.progress);
        }
      }
    } else {
      if (recombination.setStatus) {
        await recombination.setStatus(recombination.attachmentId, `Error: ${progress.errorMessage}`);
      }
      if (recombination.setError) {
        recombination.setError(progress.errorMessage);
      }
    }
  }
}
