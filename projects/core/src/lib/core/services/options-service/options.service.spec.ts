import { Options } from './options.service';

export class MockOptionsService {
  saveOptions(options: Options) {
    // void
  }

  loadOptions(id: string): Options | null {
    return { id: 'test', isCardCollapsed: false };
  }
}
