import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class OptionsService {
  saveOptions(options: Options) {
    localStorage.setItem(options.id, JSON.stringify(options));
  }

  loadOptions(id: string): Options {
    return JSON.parse(localStorage.getItem(id) || '{}');
  }
}

export interface Options {
  id: string;
  [additionalProperties: string]: any;
}
