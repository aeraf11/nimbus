import { Injectable } from '@angular/core';
import { Theme } from '../models/theme';
import { skip, take } from 'rxjs/operators';
import { StyleManager } from './style-manager';
import { StateService } from '../services/state.service';

@Injectable({
  providedIn: 'root',
})
export class ThemeService {
  private themes: Theme[] = [];
  private currentThemeId = 'light-theme';
  private THEME_KEY = 'theme';

  constructor(private state: StateService, private styleManager: StyleManager) {
    state
      .selectThemes()
      .pipe(skip(1), take(1))
      .subscribe((themes) => {
        this.themes = themes || [];
      });
  }

  setTheme(id: string) {
    const theme = this.themes.find((t) => t.id === id);
    if (theme) {
      if (theme.href) this.styleManager.setStyle(this.THEME_KEY, theme.href);
      else this.styleManager.removeStyle(this.THEME_KEY);
    }
  }
}
