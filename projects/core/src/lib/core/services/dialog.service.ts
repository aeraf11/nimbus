import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { map } from 'rxjs/operators';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { DialogConfig } from '../models';
import { DialogComponent } from '../components/dialog/dialog.component';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class DialogService {
  constructor(public dialog: MatDialog) {}

  openDialog(config: DialogConfig): Observable<any> {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.hasBackdrop = true;
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = config;

    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);
    return dialogRef.afterClosed().pipe(
      untilDestroyed(this),
      map((data) => data)
    );
  }
}
