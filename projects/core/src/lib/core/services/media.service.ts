import { switchMap } from 'rxjs/operators';
import { distinctUntilChanged } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { map, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MediaService {
  constructor(public mediaObserver: MediaObserver) {}

  getBreakpoint(): Observable<'sm' | 'md' | 'lg'> {
    return this.mediaObserver.asObservable().pipe(
      distinctUntilChanged(),
      map((media) => {
        if (media.find((m) => m.mqAlias === 'gt-md')) return 'lg';
        if (media.find((m) => m.mqAlias === 'md')) return 'md';
        if (media.find((m) => m.mqAlias === 'lt-md')) return 'sm';
        throw new Error('Unknown breakpoint');
      })
    );
  }

  getBreakpointValue(values: string): Observable<string> {
    if (!values) return of('');
    const [sm] = values.split(',');
    let [, md, lg] = values.split(',');
    if (!md) md = sm;
    if (!lg) lg = md;

    return this.mediaObserver.asObservable().pipe(
      map((media) => {
        if (media.find((m) => m.mqAlias === 'gt-md')) return lg;
        if (media.find((m) => m.mqAlias === 'md')) return md;
        else return sm;
      })
    );
  }

  getBreakpointValueString(values: string, breakpoint: string, fallback = false): string {
    if (!values) {
      return '';
    }

    // eslint-disable-next-line prefer-const
    let [sm, md, lg] = values?.split(',') ?? ['', '', ''];
    if (fallback) {
      if (!md) md = sm;
      if (!lg) lg = md;
    }

    switch (breakpoint) {
      case 'sm':
        return sm;
      case 'md':
        return md;
      case 'lg':
      default:
        return lg;
    }
  }

  getBreakpointValueObservable(observableValues: Observable<string>) {
    return observableValues.pipe(switchMap((values) => this.getBreakpointValue(values)));
  }
}
