import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  ChartData,
  ChartDataAccumulation,
  ChartDataSimple,
  ChartType,
  DataSourceAccumulation,
  DataSourceSimple,
  SiteConfig,
} from '../models';
import { ColourService } from './colour.service';

@Injectable({
  providedIn: 'root',
})
export class ChartDataService {
  private dataSourceUrl: string;

  constructor(private httpClient: HttpClient, private colour: ColourService, private siteConfig: SiteConfig) {
    this.dataSourceUrl = `${siteConfig.apiUrl}/datasources`;
  }

  getChartData(
    type: ChartType,
    data?: DataSourceAccumulation,
    dataSource?: string,
    params?: object
  ): Observable<ChartData> {
    if (data) {
      return of(this.transformDataSource(type, data));
    } else if (dataSource && params) {
      return this.getDataSource(dataSource, params).pipe(map((dsData) => this.transformDataSource(type, dsData)));
    } else {
      return of(undefined);
    }
  }

  getParams(keys: string[], formState: any): any {
    const params: any = {};
    if (keys?.length > 0) {
      keys.forEach((key) => {
        params[key] = formState.context[key] || formState.rootModel[key];
      });
    }
    return params;
  }

  useThemeForLabels(xAxis: any, yAxis: any): boolean {
    return xAxis.labelStyle?.color && yAxis?.labelStyle?.color;
  }

  private getDataSource(dataSource: string, params: object): Observable<DataSourceAccumulation | DataSourceSimple> {
    const paramsArray: string[] = [];
    if (params) {
      Object.entries(params).forEach(([key, value]) => {
        paramsArray.push(`${key}=${value}`);
      });
    }
    let url = `${this.dataSourceUrl}/${dataSource}`;
    if (paramsArray.length > 0) {
      url = `${url}?${paramsArray.join('&')}`;
    }
    return this.httpClient.get<DataSourceAccumulation | DataSourceSimple>(url);
  }

  // As we add other shape data for other charts can '|' the types here in the param and use the switch
  private transformDataSource(type: ChartType, data: DataSourceAccumulation | DataSourceSimple): ChartData {
    switch (type) {
      case ChartType.Donut:
      case ChartType.Funnel:
      case ChartType.Pie:
      case ChartType.Pyramid:
      case ChartType.CircularGauge:
        return this.transformDataSourceAccumulative(data as DataSourceAccumulation);
      case ChartType.Bar:
      case ChartType.Line:
        return this.transformDataSourceNumeric(data as DataSourceSimple);
      default:
        return undefined;
    }
  }

  private transformDataSourceNumeric(data: DataSourceSimple): ChartDataSimple {
    return {
      data: (data.table1?.results || data.results).map((item: any) => {
        return {
          name: item.name,
          value: item.value,
          colour: this.colour.getClassBackgoundColour(item.cssClass),
        };
      }),
    };
  }

  private transformDataSourceAccumulative(data: DataSourceAccumulation): ChartDataAccumulation {
    return {
      data: (data.table1?.results).map((item: any) => {
        return {
          name: item.name,
          value: item.value,
          colour: this.colour.getClassBackgoundColour(item.cssClass),
          url: item.url,
        };
      }),
      total: data.table2?.results[0].total,
    };
  }
}
