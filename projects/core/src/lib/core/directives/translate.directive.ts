import { Directive, ElementRef, Input, OnInit, Optional } from '@angular/core';
import { TranslateContextDirective } from '.';

/**
 * Tranlsate the text content of an element, with an optional context.
 *
 * @example
 * <p niTranslate>My string</p>
 *
 * @example
 * <p niTranslate context="MyContext">My string</p>
 */
@Directive({
  selector: '[niTranslate]',
})
export class TranslateDirective implements OnInit {
  @Input() context?: string;

  constructor(private elementRef: ElementRef, @Optional() private areaContext?: TranslateContextDirective) {}

  ngOnInit(): void {
    const context = this.getContext();
    this.elementRef.nativeElement.textContent =
      context === undefined
        ? $translate`${this.elementRef.nativeElement.textContent}`
        : $translate`${context}::${this.elementRef.nativeElement.textContent}`;
  }

  private getContext(): string | undefined {
    if (this.context && this.context.length > 0) {
      return this.context;
    } else if (this.areaContext) {
      return this.areaContext.niTranslateContext;
    } else {
      return undefined;
    }
  }
}
