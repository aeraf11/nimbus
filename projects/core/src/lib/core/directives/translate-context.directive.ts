import { Directive, Input } from '@angular/core';

/**
 * Provide a context for a group of translate directives or pipes further up the DOM tree.
 *
 * @example
 * <div niTranslateContext="MyContext">
 *   <p niTranslate>My string</p>
 *   <p niTranslate>My other string</p>
 * </div>
 *
 * @example with a context override
 * <div niTranslateContext="MyContext">
 *   <p niTranslate context="MyOtherContext">My string</p>
 *   <p niTranslate context="MyOtherContext">My other string</p>
 * </div>
 *
 * @example with a translate pipe
 * <div niTranslateContext="MyContext">
 *   <p>{{ 'My string' | translate }}</p>
 *   <p>{{ 'My other string' | translate }}</p>
 * </div>
 *
 * @example with a translate pipe and a context override
 * <div niTranslateContext="MyContext">
 *   <p>{{ 'My string' | translate: 'MyOtherContext' }}</p>
 *   <p>{{ 'My other string' | translate: 'MyOtherContext' }}</p>
 * </div>
 */
@Directive({
  selector: '[niTranslateContext]',
})
export class TranslateContextDirective {
  @Input() niTranslateContext!: string;
}
