import { ElementRef } from '@angular/core';
import { TranslateDirective, TranslateContextDirective } from '.';

describe('TranslateDirective', () => {
  let translateRef: any;

  let spyNativeElement: jasmine.SpyObj<HTMLElement>;
  let spyElementRef: jasmine.SpyObj<ElementRef>;
  let spyTranslateContextDirective: jasmine.SpyObj<TranslateContextDirective>;
  let directive: TranslateDirective;

  // We're going to replace the global $translate tag function with a spy
  // so we need to store a reference and restore it when we're done
  beforeAll(() => {
    translateRef = globalThis.$translate;
  });

  afterAll(() => {
    globalThis.$translate = translateRef;
  });

  describe('without TranslateContextDirective', () => {
    beforeEach(() => {
      globalThis.$translate = jasmine.createSpy('translate').and.returnValue('Translation');
      spyNativeElement = jasmine.createSpyObj('HTMLElement', [], { textContent: 'My string' });
      spyElementRef = jasmine.createSpyObj('ElementRef', [], { nativeElement: spyNativeElement });
      directive = new TranslateDirective(spyElementRef);
    });

    it('should create an instance', () => {
      expect(directive).toBeTruthy();
    });

    describe('ngOnInit', () => {
      it('should translate the text content of the element', () => {
        directive.ngOnInit();
        const spyTextContentSetter = Object.getOwnPropertyDescriptor(spyNativeElement, 'textContent')?.set;
        expect(spyTextContentSetter).toHaveBeenCalledWith('Translation');
      });

      it('should call $translate', () => {
        directive.ngOnInit();
        expect(globalThis.$translate).toHaveBeenCalledWith(<TemplateStringsArray>(<unknown>['', '']), 'My string');
      });

      it('should call $translate with context from Input', () => {
        directive.context = 'MyContext';
        directive.ngOnInit();
        expect(globalThis.$translate).toHaveBeenCalledWith(
          <TemplateStringsArray>(<unknown>['', '::', '']),
          'MyContext',
          'My string'
        );
      });

      it('should ignore context if it is an empty string', () => {
        directive.context = '';
        directive.ngOnInit();
        expect(globalThis.$translate).toHaveBeenCalledWith(<TemplateStringsArray>(<unknown>['', '']), 'My string');
      });
    });
  });

  describe('with TranslateContextDirective', () => {
    beforeEach(() => {
      globalThis.$translate = jasmine.createSpy('translate').and.returnValue('Translation');
      spyNativeElement = jasmine.createSpyObj('HTMLElement', [], { textContent: 'My string' });
      spyElementRef = jasmine.createSpyObj('ElementRef', [], { nativeElement: spyNativeElement });
      spyTranslateContextDirective = jasmine.createSpyObj('TranslateContextDirective', [], {
        niTranslateContext: 'MyContext',
      });
      directive = new TranslateDirective(spyElementRef, spyTranslateContextDirective);
    });

    it('should create an instance', () => {
      expect(directive).toBeTruthy();
    });

    describe('ngOnInit', () => {
      it('should translate the text content of the element', () => {
        directive.ngOnInit();
        expect(globalThis.$translate).toHaveBeenCalledWith(
          <TemplateStringsArray>(<unknown>['', '::', '']),
          'MyContext',
          'My string'
        );
      });
    });
  });
});
