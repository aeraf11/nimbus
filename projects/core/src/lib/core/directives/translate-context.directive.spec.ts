import { TranslateContextDirective } from './translate-context.directive';

describe('TranslateContextDirective', () => {
  let directive: TranslateContextDirective;

  beforeEach(() => {
    directive = new TranslateContextDirective();
    directive.niTranslateContext = 'MyContext';
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  describe('niTranslateContext', () => {
    it('should be defined', () => {
      expect(directive.niTranslateContext).toBeDefined();
    });
  });
});
