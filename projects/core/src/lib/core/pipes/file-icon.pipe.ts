import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fileIcon',
})
export class FileIconPipe implements PipeTransform {
  transform(value: string): string {
    const parts = value.split('.');
    const ext = parts[parts.length - 1];

    switch (ext) {
      case 'doc':
      case 'docx':
        return 'fas fa-file-word';
      case 'ppt':
      case 'pptx':
        return 'fas fa-file-powerpoint';
      case 'xls':
      case 'xlsx':
        return 'fas fa-file-excel';
      case 'pdf':
        return 'fas fa-file-pdf';
      case 'zip':
      case '7z':
        return 'fas fa-file-archive';
      case 'bmp':
      case 'png':
      case 'jpg':
      case 'gif':
        return 'fas fa-file-image';
      case 'json':
        return 'fas fa-file-code';
      case 'mp4':
        return 'fas fa-file-video';
      default:
        return 'fas fa-file';
    }
  }
}
