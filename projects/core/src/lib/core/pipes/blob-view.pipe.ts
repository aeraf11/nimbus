import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'blobView',
})
export class BlobViewPipe implements PipeTransform {
  private url = '/api/v2/media/photo?id=';

  transform(value: string, url?: string): string {
    if (!value) {
      return '';
    }

    if (url) {
      this.url = url;
    }

    return `${this.url}${value}`;
  }
}
