import { LookupService } from '../services';
import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'lookupList',
})
export class LookupListPipe implements PipeTransform {
  constructor(private service: LookupService) {}

  transform(ids: string[], hierarchical = false): Observable<string> {
    const lookups = this.service.getDetails(ids);
    return lookups.pipe(
      map((details) => {
        if (!details || details.length === 0) return '';
        const names = details.map((detail) => (hierarchical ? detail.hierarchicalDisplayValue : detail.displayValue));
        return names.join(', ');
      })
    );
  }
}
