import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toDisplayValue',
})
export class ToDisplayValuePipe implements PipeTransform {
  transform(value: { displayValue: string } | Array<{ displayValue: string }> | null): string {
    if (!value) return '';
    if (Array.isArray(value)) return value.map((x) => x.displayValue).join(', ');
    return value.displayValue;
  }
}
