import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'liveAnnouncer',
})
export class LiveAnnouncerPipe implements PipeTransform {
  constructor(private liveAnnouncer: LiveAnnouncer) {}

  transform(value: unknown) {
    if (value) {
      this.liveAnnouncer.announce(value.toString());
    }
    return value;
  }
}
