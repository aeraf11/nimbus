import { TestBed, waitForAsync } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { StateService } from '../services';
import { AppDateTimeAdapter } from '../services/app-date-time-adapter';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { ToDatetimePipe } from './to-datetime.pipe';

describe('ToDatetimePipe', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ToDatetimePipe],
      providers: [
        ToDatetimePipe,
        AppDateTimeAdapter,
        provideMockStore({ initialState: {} }),
        { provide: StateService, useClass: MockStateService },
      ],
    });
  }));

  it('should create an instance', () => {
    const pipe = TestBed.inject(ToDatetimePipe);
    expect(pipe).toBeTruthy();
  });
});
