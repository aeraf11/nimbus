import { AppDateTimeAdapter } from '../services/app-date-time-adapter';
import { Pipe, PipeTransform } from '@angular/core';
import { DateTime } from '@nimbus/material';

@Pipe({
  name: 'toDate',
})
export class ToDatePipe implements PipeTransform {
  constructor(private adapter: AppDateTimeAdapter) {}

  transform(value: string | DateTime): string | null {
    if (value) {
      let dateTime: DateTime | null;
      if (typeof value === 'string') {
        dateTime = this.adapter.parse(value);
      } else dateTime = value;
      if (dateTime) return this.adapter.formatDateTime(dateTime, false);
    }

    return null;
  }
}
