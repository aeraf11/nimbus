import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'entityMedia',
})
export class EntityMediaPipe implements PipeTransform {
  transform(id: string, width?: string, height?: string, maintainAspect?: boolean): string {
    if (!id) {
      return '';
    }
    let url = '/api/v2/media/entity?id=' + id;
    if (width) url = url + '&width=' + width;
    if (height) url = url + '&height=' + height;
    if (maintainAspect === false) url = url + '&maintainAspect=false';

    return url;
  }
}
