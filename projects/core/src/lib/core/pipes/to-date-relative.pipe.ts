import { Pipe, PipeTransform } from '@angular/core';
import { DateTime } from '@nimbus/material';
import { AppDateTimeAdapter } from '../services';

@Pipe({
  name: 'toDateRelative',
})
export class ToDateRelativePipe implements PipeTransform {
  constructor(private adapter: AppDateTimeAdapter) {}

  transform(value: string | DateTime): string | null {
    if (value) {
      if (typeof value === 'string') {
        return this.adapter.formatRelativeString(value);
      } else {
        return this.adapter.formatRelative(value);
      }
    }
    return null;
  }
}
