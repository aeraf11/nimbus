import { LookupService } from '../services';
import { Pipe, PipeTransform, ElementRef } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({
  name: 'lookup',
})
export class LookupPipe implements PipeTransform {
  constructor(private service: LookupService, private elRef: ElementRef) {}

  transform(id: string, hierarchical = false, isIdFallback = false): Observable<string> {
    const lookup = this.service.getDetail(id);
    return lookup.pipe(
      map((detail) => {
        if (!detail) return isIdFallback ? id : '';
        if (hierarchical) return detail.hierarchicalDisplayValue;
        if (this.elRef.nativeElement.parentNode.className === 'shortString') {
          return detail.displayValue.substring(0, 10) + '...';
        } else {
          return detail.displayValue;
        }
      })
    );
  }
}
