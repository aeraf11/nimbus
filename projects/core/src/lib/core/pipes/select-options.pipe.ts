import { Pipe, PipeTransform } from '@angular/core';
import { Observable, of as observableOf } from 'rxjs';
import { map } from 'rxjs/operators';

@Pipe({ name: 'phSelectOptions' })
export class SelectOptionsPipe implements PipeTransform {
  transform(options: any, props?: any) {
    if (!(options instanceof Observable)) {
      options = observableOf(options);
    }

    return (options as Observable<any>).pipe(map((value) => this.toOptions(value, props || {})));
  }

  private toOptions(options: any, props: any) {
    const gOptions: any[] = [],
      groups: { [key: string]: any[] } = {};

    props['_flatOptions'] = true;
    options.map((option: any, index: number) => {
      if (!this.getGroupProp(option, props)) {
        gOptions.push(this.toOption(option, props, index));
      } else {
        props['_flatOptions'] = false;
        if (!groups[this.getGroupProp(option, props)]) {
          groups[this.getGroupProp(option, props)] = [];
          gOptions.push({
            label: this.getGroupProp(option, props),
            group: groups[this.getGroupProp(option, props)],
          });
        }
        groups[this.getGroupProp(option, props)].push(this.toOption(option, props, index));
      }
    });

    if (props['addDefaultOption'] === undefined || props['addDefaultOption']) {
      return [{ label: '---choose---', value: null }, ...gOptions];
    } else {
      return gOptions;
    }
  }

  private toOption(item: any, to: any, index: number) {
    const label = this.getLabelProp(item, to);
    return {
      label,
      value: this.getValueProp(item, to, index, label),
      disabled: this.getDisabledProp(item, to) || false,
    };
  }

  private getLabelProp(item: any, props: any): string {
    if (typeof props.labelProp === 'function') {
      return props.labelProp(item);
    }

    if (this.shouldUseLegacyOption(item, props)) {
      console.warn(
        `NgxFormly: legacy select option '{key, value}' is deprecated since v5.5, use '{value, label}' instead.`
      );
      return item.value;
    }

    return item[props.labelProp || 'label'];
  }

  private getValueProp(
    item: any,
    props: any,
    index: number,
    label: string
  ): string | { id: string; displayValue: string } {
    let value = null;

    if (typeof props.valueProp === 'function') {
      value = props.valueProp(item);
    } else if (this.shouldUseLegacyOption(item, props)) {
      value = item.key;
    } else if (props.valueProp === '<index>') {
      value = index + '';
    } else {
      value = item[props.valueProp] ?? item[props.valuePropFallback] ?? item['value'];
    }

    if (props.outputObject === undefined || props.outputObject) {
      return { id: value, displayValue: label };
    } else {
      return value;
    }
  }

  private getDisabledProp(item: any, props: any): string {
    if (typeof props.disabledProp === 'function') {
      return props.disabledProp(item);
    }
    return item[props.disabledProp || 'disabled'];
  }

  private getGroupProp(item: any, props: any): string {
    if (typeof props.groupProp === 'function') {
      return props.groupProp(item);
    }

    return item[props.groupProp || 'group'];
  }

  private shouldUseLegacyOption(item: any, props: any) {
    return (
      !props.valueProp &&
      !props.labelProp &&
      item != null &&
      typeof item === 'object' &&
      'key' in item &&
      'value' in item
    );
  }
}
