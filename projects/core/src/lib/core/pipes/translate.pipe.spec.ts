import { TranslateContextDirective } from '../directives';
import { TranslatePipe } from '.';

describe('TranslatePipe', () => {
  let translateRef: any;

  let spyTranslateContextDirective: jasmine.SpyObj<TranslateContextDirective>;
  let pipe: TranslatePipe;

  // We're going to replace the global $translate tag function with a spy
  // so we need to store a reference and restore it when we're done
  beforeAll(() => {
    translateRef = globalThis.$translate;
  });

  afterAll(() => {
    globalThis.$translate = translateRef;
  });

  describe('without TranslateContextDirective', () => {
    beforeEach(() => {
      globalThis.$translate = jasmine.createSpy('translate').and.returnValue('Translation');

      pipe = new TranslatePipe();
    });

    it('should create an instance', () => {
      expect(pipe).toBeTruthy();
    });

    describe('transform', () => {
      it('should return the translation', () => {
        expect(pipe.transform('My string')).toBe('Translation');
      });

      it('should call $translate without context', () => {
        pipe.transform('My string');
        expect(globalThis.$translate).toHaveBeenCalledWith(<TemplateStringsArray>(<unknown>['', '']), 'My string');
      });

      it('should call $translate with context as string', () => {
        pipe.transform('My string', 'MyContext');
        expect(globalThis.$translate).toHaveBeenCalledWith(
          <TemplateStringsArray>(<unknown>['', '::', '']),
          'MyContext',
          'My string'
        );
      });

      it('should call $translate with context from args.context', () => {
        pipe.transform('My string', { context: 'MyContext' });
        expect(globalThis.$translate).toHaveBeenCalledWith(
          <TemplateStringsArray>(<unknown>['', '::', '']),
          'MyContext',
          'My string'
        );
      });
    });
  });

  describe('with TranslateContextDirective', () => {
    beforeEach(() => {
      globalThis.$translate = jasmine.createSpy('translate').and.returnValue('Translation');

      spyTranslateContextDirective = jasmine.createSpyObj('TranslateContextDirective', [], {
        niTranslateContext: 'MyContext',
      });

      pipe = new TranslatePipe(spyTranslateContextDirective);
    });

    it('should create an instance', () => {
      expect(pipe).toBeTruthy();
    });

    describe('transform', () => {
      it('should call $translate with context from TranslateContextDirective', () => {
        pipe.transform('My string');
        expect(globalThis.$translate).toHaveBeenCalledWith(
          <TemplateStringsArray>(<unknown>['', '::', '']),
          'MyContext',
          'My string'
        );
      });
    });
  });
});
