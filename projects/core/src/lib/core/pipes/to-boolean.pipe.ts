import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toBoolean',
})
export class ToBooleanPipe implements PipeTransform {
  transform(value: boolean | null, defaultBool = false): boolean {
    return typeof value === 'boolean' ? value : defaultBool;
  }
}
