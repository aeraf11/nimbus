import { TestBed } from '@angular/core/testing';
import { StateService } from '../services';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { UserPermissionsPipe } from './user-permissions.pipe';

describe('UserPermissionsPipe', () => {
  let pipe: UserPermissionsPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UserPermissionsPipe],
      providers: [UserPermissionsPipe, { provide: StateService, useClass: MockStateService }],
    });

    pipe = TestBed.inject(UserPermissionsPipe);
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
});
