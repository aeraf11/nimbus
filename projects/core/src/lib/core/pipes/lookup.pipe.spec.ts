import { TestBed } from '@angular/core/testing';
import { StateService } from '../services';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { LookupService } from './../services/lookup.service';
import { LookupPipe } from './lookup.pipe';
import { ElementRef } from '@angular/core';

describe('LookupPipe', () => {
  let pipe: LookupPipe;
  const mockElementRef: any = {
    nativeElement: {
      stringText: 'test',
    },
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LookupPipe],
      providers: [
        LookupService,
        LookupPipe,
        { provide: StateService, useClass: MockStateService },
        { provide: ElementRef, useValue: mockElementRef },
      ],
    });

    pipe = TestBed.inject(LookupPipe);
  });

  it('create an instance', () => {
    // Assert
    expect(pipe).toBeTruthy();
  });
});
