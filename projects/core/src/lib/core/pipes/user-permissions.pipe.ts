import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../models';
import { StateService } from '../services';
import { take, tap } from 'rxjs/operators';

@Pipe({
  name: 'userHasPermissions',
})
export class UserPermissionsPipe implements PipeTransform {
  user: User | null = null;

  constructor(private state: StateService) {
    this.state
      .selectUser()
      .pipe(
        take(1),
        tap((user: User | null) => {
          this.user = user;
        })
      )
      .subscribe();
  }

  transform(permissions: string[] | null): boolean {
    if (!permissions || permissions?.length < 1) return true;
    return permissions.every((permission: string) => this.user?.permissions?.includes(permission));
  }
}
