import { getInitials } from '../utils/utils';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'initials',
})
export class InitialsPipe implements PipeTransform {
  transform(fullName: string | undefined): string | undefined {
    if (!fullName) return undefined;
    return getInitials(fullName);
  }
}
