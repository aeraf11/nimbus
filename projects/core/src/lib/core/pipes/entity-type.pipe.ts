import { Pipe, PipeTransform } from '@angular/core';
import { EntityType } from '../models/entity-type';

@Pipe({
  name: 'entityType',
})
export class EntityTypePipe implements PipeTransform {
  transform(value: number, fallback?: string): string {
    if (!value) {
      return '';
    }

    return EntityType[value] ?? fallback ?? '';
  }
}
