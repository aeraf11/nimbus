import { Optional, Pipe, PipeTransform } from '@angular/core';
import { TranslateContextDirective } from '../directives';

interface TranslatePipeArgs {
  context?: string;
}

/**
 * Translate a string, with an optional context.
 *
 * @example
 * <p>{{ 'My string' | translate }}</p>
 *
 * @example
 * <p>{{ 'My string' | translate: 'MyContext' }}</p>
 *
 * @example
 * <p>{{ 'My string' | translate: { context: 'MyContext' } }}</p>
 */
@Pipe({
  name: 'translate',
})
export class TranslatePipe implements PipeTransform {
  constructor(@Optional() private areaContext?: TranslateContextDirective) {}

  transform(value: string, args?: string | TranslatePipeArgs): string {
    if (typeof args === 'string') {
      return this.translateWithContext(args, value);
    }

    if (args && args.context) {
      return this.translateWithContext(args.context, value);
    }

    if (this.areaContext) {
      return this.translateWithContext(this.areaContext.niTranslateContext, value);
    }

    return $translate`${value}`;
  }

  private translateWithContext(context: string, value: string): string {
    return context.length > 0 ? $translate`${context}::${value}` : $translate`${value}`;
  }
}
