import { Pipe, PipeTransform } from '@angular/core';
import { DateTime } from '@nimbus/material';
import { AppDateTimeAdapter } from '../services/app-date-time-adapter';

@Pipe({
  name: 'toDatetime',
})
export class ToDatetimePipe implements PipeTransform {
  constructor(private adapter: AppDateTimeAdapter) {}

  transform(value: string | DateTime, includeTimeZone = true, includeSeconds = false): string | null {
    if (value) {
      let dateTime: DateTime | null;
      if (typeof value === 'string') {
        dateTime = this.adapter.parse(value);
      } else {
        dateTime = value;
      }
      if (dateTime) {
        return this.adapter.formatDateTime(dateTime, true, includeTimeZone, includeSeconds);
      }
    }

    return null;
  }
}
