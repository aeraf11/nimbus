import { untilDestroyed } from '@ngneat/until-destroy';
import { MemoizedSelector, select } from '@ngrx/store';
import { Observable, OperatorFunction } from 'rxjs';

export function selectUntilDestroyed<T, S = any>(
  selector: MemoizedSelector<S, T>,
  instance: any
): OperatorFunction<S, T> {
  return (store: Observable<S>) => store.pipe(select(selector), untilDestroyed(instance));
}
