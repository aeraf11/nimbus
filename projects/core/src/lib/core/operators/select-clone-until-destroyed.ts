import { untilDestroyed } from '@ngneat/until-destroy';
import { MemoizedSelector, select } from '@ngrx/store';
import deepClone from 'deep.clone';
import { Observable, OperatorFunction } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

export function selectCloneUntilDestroyed<T, S = any>(
  selector: MemoizedSelector<S, T>,
  instance: any
): OperatorFunction<S, T> {
  return (store: Observable<S>) =>
    store.pipe(
      select(selector),
      switchMap((obj) => deepClone({ obj })),
      map(({ obj }) => obj),
      untilDestroyed(instance)
    );
}
