import { map, switchMap } from 'rxjs/operators';
import { MemoizedSelector, select } from '@ngrx/store';
import { Observable, OperatorFunction } from 'rxjs';
import { deepClone } from 'deep.clone';

export function selectClone<T, S = any>(selector: MemoizedSelector<S, T>): OperatorFunction<S, T> {
  return (store: Observable<S>) =>
    store.pipe(
      select(selector),
      switchMap((obj) => deepClone({ obj })),
      map(({ obj }) => obj)
    );
}
