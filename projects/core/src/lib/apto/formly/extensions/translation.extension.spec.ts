﻿import { TestBed } from '@angular/core/testing';
import { TranslationExtension } from '@nimbus/core/src/lib/apto';
import { FormsHelperService, MockStateService, StateService, TranslationService } from '@nimbus/core/src/lib/core';
import { FormlyFieldConfig } from '@ngx-formly/core';

describe('TranslationExtension', () => {
  let oneTimeFormlyField: FormlyFieldConfig;
  let translationExtension: TranslationExtension;
  let translationService: TranslationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: FormsHelperService, useClass: FormsHelperService },
        { provide: StateService, useClass: MockStateService },
      ],
    });

    const stateService = TestBed.inject(StateService);
    const formsHelper = TestBed.inject(FormsHelperService);

    translationExtension = new TranslationExtension(stateService, formsHelper);

    translationService = TestBed.inject(TranslationService);
    translationService.translationState = {
      _root: {
        'Oliver is logged in as {0}': 'Luke is logged in as {0}',
      },
      operation: {
        name: 'test',
      },
    };
  });

  it('should be created', () => {
    expect(translationExtension).toBeTruthy();
  });

  describe('getTranslation()', () => {
    it('should return the correct translation when I pass it a string with expressions', () => {
      oneTimeFormlyField = {
        props: {
          $label: 'Oliver is logged in as ${user}',
        },
        options: {
          formState: {
            user: 'Tester 1',
          },
        },
      };

      const result: string = translationExtension.getTranslation(oneTimeFormlyField.props?.$label);

      expect(result).toEqual('Luke is logged in as ${user}');
    });
  });

  describe('prePopulate()', () => {
    it('should do nothing if translationEnabled === false', () => {
      oneTimeFormlyField = {
        props: {
          $label: 'Oliver is logged in as ${user}',
        },
        options: {
          formState: {
            user: 'Tester 1',
          },
        },
      };
      translationExtension.checkedTranslationEnabled = true;
      translationExtension.translationEnabled = false;

      translationExtension.prePopulate(oneTimeFormlyField);

      expect(oneTimeFormlyField.props?.label).toBeFalsy();
    });

    describe('if translationEnabled === true', () => {
      beforeEach(() => {
        translationExtension.checkedTranslationEnabled = true;
        translationExtension.translationEnabled = true;
      });

      it('should create a new property without the `$` prefix on the formlyField.props object when passed oneTimeFormlyField.props.$label', () => {
        oneTimeFormlyField = {
          props: {
            $label: 'Oliver is logged in as ${user}',
          },
          options: {
            formState: {
              user: 'Tester 1',
            },
          },
        };

        translationExtension.prePopulate(oneTimeFormlyField);

        expect(oneTimeFormlyField.props?.label).toEqual(
          `Luke is logged in as ${oneTimeFormlyField.options?.formState.user}`
        );
      });

      it('should create the properties "label", "_$$label.translation" on the FormlyField.props object when passed continuousFormlyField.props.$$label', () => {
        const continuousFormlyField: FormlyFieldConfig = {
          props: {
            $$label: 'Oliver is logged in as ${user}',
          },
          options: {
            formState: {
              user: 'Tester 2',
            },
          },
        };

        translationExtension.prePopulate(continuousFormlyField);

        expect(continuousFormlyField.props?.label).toEqual(
          `Luke is logged in as ${continuousFormlyField.options?.formState.user}`
        );
        expect(continuousFormlyField.props?._$$label?.translation).toEqual('Luke is logged in as ${user}');
      });

      describe('Performance Test', () => {
        it('should measure the execution time of prePopulate() when called 1000 times', () => {
          translationService.translationState = {
            _root: {
              'Oliver is logged in as {0} with {1} rights': 'Luke is signed in as {0} with {1} rights',
            },
            operation: {
              name: 'test',
            },
          };
          const continuousFormlyField: FormlyFieldConfig = {
            props: {
              $$label: 'Oliver is logged in as ${user} with ${adminRights} rights',
            },
            options: {
              formState: {
                user: 'Tester 2',
                adminRights: 'full admin',
              },
            },
          };
          const startTime = Date.now();

          for (let index = 0; index < 1000; index++) {
            translationExtension.prePopulate(continuousFormlyField);
          }

          const endTime = Date.now();
          const executionTime = endTime - startTime;

          console.log(`Execution time: ${executionTime} ms`);
          expect(continuousFormlyField.props?.label).toEqual(
            `Luke is signed in as ${continuousFormlyField.options?.formState.user} with ${continuousFormlyField.options?.formState.adminRights} rights`
          );
          expect(executionTime).toBeLessThan(1000);
        });
      });
    });
  });
});
