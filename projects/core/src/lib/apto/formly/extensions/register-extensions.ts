﻿import { FormsHelperService, StateService } from '@nimbus/core/src/lib/core';
import { TemplatedPropertiesExtension, TranslationExtension } from '@nimbus/core/src/lib/apto';

export function registerExtensions(formsHelper: FormsHelperService, stateService: StateService) {
  return {
    extensions: [
      {
        name: 'templatedproperties',
        extension: new TemplatedPropertiesExtension(formsHelper),
      },
      {
        name: '$',
        extension: new TranslationExtension(stateService, formsHelper),
      },
      {
        name: '$$',
        extension: new TranslationExtension(stateService, formsHelper),
      },
    ],
  };
}
