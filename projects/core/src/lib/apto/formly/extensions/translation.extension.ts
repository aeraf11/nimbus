﻿import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormsHelperService, StateService } from '@nimbus/core/src/lib/core';
import { take } from 'rxjs';
import { filter } from 'rxjs/operators';

export class TranslationExtension {
  constructor(private stateService: StateService, private formsHelper: FormsHelperService) {}

  oneTimeTranslationRegex = /^\$\w/;
  continuousTranslationRegex = /^\$\$/;
  formlyExpressionRegex = /[$?#*]{[\w{},\s.]+}/g;
  translationExpressionRegex = /{(\d)}/g;

  checkedTranslationEnabled = false;
  translationEnabled?: boolean;

  prePopulate(field: FormlyFieldConfig): void {
    if (!this.checkedTranslationEnabled) {
      this.checkTranslationEnabled();
    }

    if (!this.translationEnabled) {
      return;
    }

    for (const key in field.props) {
      if (this.oneTimeTranslationRegex.test(key)) {
        const newProperty: string = key.replace('$', '');

        if (field.props[newProperty]) {
          return;
        } else {
          const translation: string = this.getTranslation(field.props[key]);

          field.props[newProperty] = this.formsHelper.parseTemplate(translation, field?.options?.formState ?? {}, '');
        }
      } else if (this.continuousTranslationRegex.test(key)) {
        const newProperty: string = key.replace('$$', '');

        if (field.props[`_$$${newProperty}`]?.translation) {
          field.props[newProperty] = this.formsHelper.parseTemplate(
            field.props[`_$$${newProperty}`].translation,
            field?.options?.formState ?? {},
            ''
          );
        } else {
          const translation: string = this.getTranslation(field.props[key]);

          field.props[newProperty] = this.formsHelper.parseTemplate(translation, field?.options?.formState ?? {}, '');
          field.props[`_$$${newProperty}`] = {
            translation: translation,
          };
        }
      }
    }
  }

  getTranslation(stringToTranslate: string): string {
    const expressions: string[] = [];
    let replaceCount = 0;

    const token: string = stringToTranslate.replace(this.formlyExpressionRegex, (match: string): string => {
      const translationExpression = `{${replaceCount}}`;
      replaceCount++;
      expressions.push(match);

      return translationExpression;
    });

    return $getTranslation('', token).replace(
      this.translationExpressionRegex,
      (match: string, captureGroup1: string): string => {
        return expressions[parseInt(captureGroup1)];
      }
    );
  }

  checkTranslationEnabled(): void {
    this.checkedTranslationEnabled = true;
    this.stateService
      .selectEnableFormTranslations()
      .pipe(
        filter((value) => value !== undefined),
        take(1)
      )
      .subscribe((status) => {
        this.translationEnabled = status;
      });
  }
}
