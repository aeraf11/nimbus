/* eslint-disable @typescript-eslint/no-explicit-any */
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormsHelperService } from '@nimbus/core/src/lib/core';

export class TemplatedPropertiesExtension {
  constructor(private formsHelper: FormsHelperService) {}

  prePopulate(field: FormlyFieldConfig) {
    const templatedProperties = field.props?.['templatedProperties'];

    // Possibly add some way of throttling or exiting at this point if this gets too chatty.
    if (!templatedProperties) {
      return;
    }
    Object.keys(templatedProperties).forEach((key: string) => {
      const propertyKey = this.getPropertyKey(key);

      field.expressions = {
        ...(field.expressions || {}),
        [propertyKey]: (field: any) => {
          const formState = field?.options?.formState ?? {};
          return this.formsHelper.parseTemplate(templatedProperties[key], formState, '');
        },
      };
    });
  }

  getPropertyKey(key: string): string {
    if (key.startsWith('model.') || key.startsWith('templateOptions.') || key.startsWith('props.')) {
      return key;
    }
    return `props.${key}`;
  }
}
