import { Injectable } from '@angular/core';
import { StateService } from '@nimbus/core/src/lib/core';
import { UserSettingsModel } from '../models/user-settings';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private state: StateService) {}

  onThemeChange(themeId: string) {
    this.state.setTheme(themeId);
  }

  saveUserSettings(model: UserSettingsModel) {
    this.onThemeChange(model.themeId);
  }
}
