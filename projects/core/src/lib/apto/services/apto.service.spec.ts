import { TestBed } from '@angular/core/testing';

import { AptoService } from './apto.service';

describe('AptoService', () => {
  let service: AptoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AptoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
