import { Injectable } from '@angular/core';
import { FileSyncService, IndexedDbService } from '@nimbus/core/src/lib/core';
import PQueue from 'p-queue';
import { firstValueFrom } from 'rxjs';
import { QueuedAttachment } from './../../core/models/queued-attachment';
import { DynamicRoutesDataService } from './dynamic-routes-data.service';

@Injectable({
  providedIn: 'root',
})
export class QueueSyncService {
  private queueTableName = 'queuedItems';
  private fileTableName = 'offlineUploads';
  private chunkTableName = `${this.fileTableName}Chunks`;

  constructor(
    private db: IndexedDbService,
    private fileService: FileSyncService,
    private dynamicRoutes: DynamicRoutesDataService
  ) {}

  async processQueue(options: { concurrentUploads: number }) {
    const queue = new PQueue({ concurrency: +options.concurrentUploads });

    const attachments = await this.db.table<QueuedAttachment>(this.queueTableName).toArray();

    attachments.forEach(async (attachment) => {
      (async () => {
        await queue.add(() => this.executeAttachmentTask(attachment));
      })();
    });
  }

  async executeAttachmentTask(attachment: QueuedAttachment) {
    try {
      const file = attachment.item.upload[0];
      const uploadResult = await this.fileService.processAttachment(
        attachment.id,
        file,
        this.chunkTableName,
        (attachmentId, status) => this.setStatus(attachmentId, status)
      );
      if (uploadResult.success) {
        const model = this.getModel(uploadResult.fileId + '', attachment, file);
        if (uploadResult.fileId) {
          await this.processEntityLink(model, attachment.id, file.fileId);
        } else if (uploadResult.fileRecombinationId) {
          this.fileService.addRecombinations({
            id: uploadResult.fileRecombinationId,
            model,
            attachmentId: attachment.id,
            fileId: file.fileId,
            setStatus: (attachmentId, status) => this.setStatus(attachmentId, status),
            onComplete: (model, attachment, fileId) => this.processEntityLink(model, attachment, fileId),
          });
        }
      } else {
        await this.setStatus(attachment.id, `Error: ${uploadResult.errorMessage}`);
      }
    } catch (err) {
      await this.setStatus(attachment.id, `Error: ${err}`);
    }
  }

  async processEntityLink(model: EntityLinkModel, attachmentId: string, fileId: string) {
    const result = await firstValueFrom(this.dynamicRoutes.postForm('Attachments.Add', model));
    if ((<any>result).attachmentId) {
      await this.cleanUp(attachmentId, fileId);
    } else {
      await this.setStatus(attachmentId, `Error`);
    }
  }

  async setStatus(id: string, status: string) {
    return await this.db.table(this.queueTableName).where('id').equals(id).modify({ status });
  }

  async cleanUp(queueId: string, fileId: string) {
    await this.db.table(this.queueTableName).where('id').equals(queueId).delete();
    await this.db.table(this.fileTableName).where('id').equals(fileId).delete();
    await this.db.table(this.chunkTableName).where('fileId').equals(fileId).delete();
  }

  getModel(blobId: string, attachment: QueuedAttachment, file: any): EntityLinkModel {
    return {
      blobId,
      entityId: attachment.entityId,
      entityTypeId: attachment.entityTypeId,
      filename: file.name,
      size: file.size,
      description: attachment.item.description,
    };
  }
}

export interface EntityLinkModel {
  blobId: string;
  entityId: string;
  entityTypeId: number;
  filename: string;
  size: string;
  description: string;
}
