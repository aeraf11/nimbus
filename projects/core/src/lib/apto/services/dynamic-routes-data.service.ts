import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { DynamicRoute, SiteConfig, StateService } from '@nimbus/core/src/lib/core';
import { Observable } from 'rxjs';
import { DynamicRoutePostResult } from '../models';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class DynamicRoutesDataService {
  private routesUrl: string;
  private mainUrl: string;
  private siteId?: string;

  constructor(private httpClient: HttpClient, private siteConfig: SiteConfig, private stateService: StateService) {
    this.routesUrl = `${siteConfig.apiUrl}/dynamicroute/all?siteId=`;
    this.mainUrl = `${siteConfig.apiUrl}/dynamicroute`;

    this.stateService
      .selectSiteId()
      .pipe(untilDestroyed(this))
      .subscribe((id) => {
        this.siteId = id;
      });
  }

  loadRoutes(siteId?: string): Observable<DynamicRoute[]> {
    return this.httpClient.get<DynamicRoute[]>(this.routesUrl + siteId);
  }

  loadModel(name: string, context: any): Observable<any> {
    const paramsArray = [`name=${name}`, `siteId=${this.siteId}`];
    if (context) {
      Object.entries(context).forEach(([key, value]) => {
        paramsArray.push(`${key}=${value}`);
      });
    }
    const url = `${this.mainUrl}?${paramsArray.join('&')}`;

    return this.httpClient.get<any>(url);
  }

  postForm(name: string, model: any): Observable<DynamicRoutePostResult> {
    return this.httpClient.post<DynamicRoutePostResult>(`${this.mainUrl}?siteId=${this.siteId}&name=${name}`, model);
  }
}
