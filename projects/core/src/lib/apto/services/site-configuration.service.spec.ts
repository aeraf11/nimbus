import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { SiteConfig } from '@nimbus/core/src/lib/core';
import { StateService } from '../../core/services/state.service';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { SiteConfigurationService } from './site-configuration.service';

describe('SiteConfigurationService', () => {
  let service: SiteConfigurationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
        { provide: StateService, useClass: MockStateService },
      ],
    });
    service = TestBed.inject(SiteConfigurationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
