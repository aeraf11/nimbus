import { Injectable, OnDestroy } from '@angular/core';
import { AppDateTimeAdapter, ScheduledJob, StateService } from '@nimbus/core/src/lib/core';
import { Time } from '@nimbus/material';
import { take } from 'rxjs';
import { QueueSyncService } from './queue-sync.service';

const JOB_TIMES_KEY = 'jobTimes';

@Injectable({
  providedIn: 'root',
})
export class ScheduledJobService implements OnDestroy {
  intervals: Map<string, number> = new Map<string, number>();
  timeouts: Map<string, number> = new Map<string, number>();
  jobs: ScheduledJob[] = [];

  constructor(
    private state: StateService,
    private dateTime: AppDateTimeAdapter,
    private queueService: QueueSyncService
  ) {}

  init(): void {
    const config = JSON.parse(localStorage.getItem(JOB_TIMES_KEY) || '{}');

    this.state
      .selectScheduledJobs()
      .pipe(take(1))
      .subscribe((jobs) => {
        this.jobs = jobs.map((job) => {
          return { ...job, userTime: config[job.id] };
        });
        this.jobs.forEach((job) => this.queueJob(job));
      });
  }

  ngOnDestroy(): void {
    this.clearJobs();
  }

  private queueJob(job: ScheduledJob): void {
    if (job.isEnabled) {
      const delay = this.workoutTimeRemainingInMs(job.userTime || job.defaultTime);
      console.info(`Scheduling job: ${job.id} for ${delay / 1000} s`);
      const timeout = window.setTimeout(() => {
        this.executeJob(job);
      }, delay);
      this.timeouts.set(job.id, timeout);
    }
  }

  resetJob(jobId: string, time: Time) {
    const job = this.jobs.find((job) => job.id === jobId);
    if (job) {
      if (this.timeouts.has(jobId)) {
        clearTimeout(this.timeouts.get(jobId));
      }
      if (this.intervals.has(jobId)) {
        clearInterval(this.intervals.get(jobId));
      }
      job.userTime = time;
      this.queueJob(job);
    }
  }

  private workoutTimeRemainingInMs(time: Time): number {
    const date = this.dateTime.today();
    const dateWithTime = this.dateTime.setTime(date, time);
    let delay = this.dateTime.compareMilliseconds(dateWithTime, date);
    if (delay < 0) delay += 86400000; // already passed today, run tomorrow
    return delay;
  }

  private executeJob(job: ScheduledJob): void {
    switch (job.id) {
      case 'SyncQueuedAttachments':
        this.syncQueuedAttachments(job.options);
        if (job.interval && !this.intervals.has(job.id)) {
          const interval = window.setInterval(() => this.syncQueuedAttachments(job.options), job.interval);
          this.intervals.set(job.id, interval);
        }
        break;
      default:
        throw new Error(`The job with the id: ${job.id} was not found`);
    }
  }

  private syncQueuedAttachments(options: unknown): void {
    this.queueService.processQueue(<{ concurrentUploads: number }>options);
  }

  private clearJobs(): void {
    this.intervals.forEach((interval) => clearInterval(interval));
    this.timeouts.forEach((timeout) => clearTimeout(timeout));
  }
}
