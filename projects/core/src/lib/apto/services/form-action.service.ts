import { AttachmentSyncService } from './attachment-sync.service';
import { Injectable } from '@angular/core';
import { StateService } from '@nimbus/core/src/lib/core';
import { UserSettingsModel } from '../models/user-settings';
import { UserService } from './user.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class FormActionService {
  constructor(
    private state: StateService,
    private userService: UserService,
    private attachmentSyncService: AttachmentSyncService,
    private router: Router,
    private location: Location
  ) {}

  handleFormAction(
    model: any,
    action?: string,
    routeId?: string,
    onSuccess?: () => void,
    showErrorSnack?: boolean,
    context?: any
  ): void {
    if (action === 'saveUserSettings') {
      this.userService.saveUserSettings(<UserSettingsModel>model);
    } else if (action === 'acceptance') {
      sessionStorage.setItem(model.storageKey, 'accepted');
      this.router.navigate([model.returnUrl]);
    } else if (action === 'back') {
      this.location.historyGo(model.delta);
    } else {
      const isSavedOffline = (model as any)?.isSavedOffline;
      if ((isSavedOffline && action !== 'sync') || !context.isOnline) {
        this.state.postFormOffline(model, context, routeId ?? '');
      } else {
        if (action === 'sync') {
          this.state.setLoading(true); // Don't need to set false after as the postFormSuccess will set this false
          this.attachmentSyncService.processAttachmentsForOfflineEntity(
            model,
            (response: { model: unknown; cleanup?: { queueId: string; fileId: string }[] } | undefined) =>
              this.offlineAttachmentsComplete(response, model, action, routeId ?? '', showErrorSnack, onSuccess),
            { concurrentUploads: 2 }
          );
        } else {
          this.state.postForm({ ...model, action: action }, routeId ?? '', onSuccess, showErrorSnack);
        }
      }
    }
  }

  offlineAttachmentsComplete(
    response: { model: unknown; cleanups?: { queueId: string; fileId: string }[] } | undefined,
    model: any,
    action: string,
    routeId: string,
    showErrorSnack?: boolean,
    onSuccess?: () => void
  ) {
    const success = () => {
      if (onSuccess) onSuccess();
      response?.cleanups?.forEach((cleanup) => this.attachmentSyncService.cleanUp(cleanup.queueId, cleanup.fileId));
    };
    if (response) {
      const payloadModel = response.model ?? model;
      this.state.postForm({ ...payloadModel, action: action }, routeId, success, showErrorSnack);
    }
  }
}
