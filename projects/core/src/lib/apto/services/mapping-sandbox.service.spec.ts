import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { MappingSandboxService } from './mapping-sandbox.service';

export class MockMappingSandboxService {
  getMapping(): Observable<string> {
    return of('');
  }
}

describe('MappingSandboxService', () => {
  let service: MappingSandboxService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(MappingSandboxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
