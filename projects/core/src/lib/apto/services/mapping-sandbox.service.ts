import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MappingSandboxService {
  constructor(private http: HttpClient) {}

  getMapping(mappings: unknown, model: unknown): Observable<string> {
    return this.http.post<string>('/api/v2/mapping', { mappings, model });
  }
}
