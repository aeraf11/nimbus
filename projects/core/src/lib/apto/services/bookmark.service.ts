import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BookmarkService {
  bookmarksChanged$ = new Subject<void>();

  getBookmarksChanged(): Observable<void> {
    return this.bookmarksChanged$.asObservable();
  }

  saveBookmark(routeId: string, bookmark: any): void {
    if (this.isBookmarkSaved(routeId, bookmark)) {
      this.removeBookmark(routeId, bookmark);
    } else {
      this.addBookmark(routeId, bookmark);
    }
  }

  loadAllBookmarks(routeId: string): any[] {
    const bookmarks = localStorage.getItem(`${routeId}.Bookmarks`);

    if (bookmarks) {
      return JSON.parse(bookmarks);
    }

    return [];
  }

  isBookmarkSaved(routeId: string, bookmark: any): boolean {
    const bookmarks = this.loadAllBookmarks(routeId);
    return bookmarks.filter((b) => JSON.stringify(b) === JSON.stringify(bookmark))?.length > 0;
  }

  private removeBookmark(routeId: string, bookmark: any): void {
    const removed = this.loadAllBookmarks(routeId).filter((b) => JSON.stringify(b) !== JSON.stringify(bookmark));
    localStorage.setItem(`${routeId}.Bookmarks`, JSON.stringify(removed));
    this.bookmarksChanged$.next();
  }

  private addBookmark(routeId: string, bookmark: any): void {
    const toAdd = this.loadAllBookmarks(routeId);
    toAdd.push(bookmark);
    localStorage.setItem(`${routeId}.Bookmarks`, JSON.stringify(toAdd));
    this.bookmarksChanged$.next();
  }
}
