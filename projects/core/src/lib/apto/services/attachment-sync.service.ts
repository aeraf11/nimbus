import { Injectable } from '@angular/core';
import { FileSyncService, IndexedDbService, utils } from '@nimbus/core/src/lib/core';
import PQueue from 'p-queue';
import { StorageType } from '../../core/models';

interface AttachmentRoute {
  entityLocation: string;
  offlineUploaderKey: string;
}

@Injectable({
  providedIn: 'root',
})
export class AttachmentSyncService {
  private attachmentTableName = 'syncAttachmentQueue';
  private mediaTableName = 'syncMediaQueue';
  private fileTableName = 'offlineUploads';
  private chunkTableName = `${this.fileTableName}Chunks`;
  private fileMap = new Map<string, string>();

  constructor(private db: IndexedDbService, private fileService: FileSyncService) {}

  async processAttachmentsForOfflineEntity(
    model: any,
    callback: (response: { model: unknown; cleanups?: { queueId: string; fileId: string }[] } | undefined) => void,
    options?: { concurrentUploads: number }
  ) {
    if (!model || !model.tempId) {
      return;
    }

    const queue = new PQueue({ concurrency: options?.concurrentUploads ?? 2 });

    const cleanups: { queueId: string; fileId: string }[] = [];

    const attachments = await this.readFromIndexedDb(this.attachmentTableName, model);
    const medias = await this.readFromIndexedDb(this.mediaTableName, model);
    const fileCount = attachments.length + medias.length;

    if (fileCount > 0) {
      for (const attachment of attachments) {
        await queue.add(() => this.processAttachment(attachment, model, true, fileCount, cleanups, callback));
      }

      for (const media of medias) {
        await queue.add(() =>
          this.processAttachment(media, model, false, fileCount, cleanups, callback, StorageType.Media)
        );
      }
    } else {
      callback({ model });
    }
  }

  async cleanUp(queueId: string, fileId: string) {
    await this.db.table(this.attachmentTableName).where('id').equals(queueId).delete();
    await this.db.table(this.mediaTableName).where('id').equals(queueId).delete();
    await this.db.table(this.fileTableName).where('id').equals(fileId).delete();
    await this.db.table(this.chunkTableName).where('fileId').equals(fileId).delete();
  }

  private async readFromIndexedDb(tableName: string, model: any) {
    return await this.db
      .table(tableName)
      .where('parentEntityId')
      .equals(model.tempId)
      .or('entityId')
      .equals(model.tempId)
      .toArray();
  }

  private async processAttachment(
    attachment: any,
    model: any,
    isAttachment = true,
    fileCount: number,
    cleanups: { queueId: string; fileId: string }[],
    callback: (response: { model: unknown; cleanup?: { queueId: string; fileId: string }[] } | undefined) => void,
    storageType = StorageType.Attachment
  ) {
    const queueId = attachment.item._id;
    try {
      const file = attachment.item.upload[0];
      const uploadResult = await this.fileService.processAttachment(
        attachment.id,
        file,
        this.chunkTableName,
        (attachmentId, status) => this.setStatus(attachmentId, status, isAttachment),
        storageType
      );
      if (uploadResult.success) {
        if (uploadResult.fileId) {
          await this.uploadComplete(attachment, model, uploadResult.fileId, fileCount, cleanups, callback);
        } else if (uploadResult.fileRecombinationId) {
          this.fileService.addRecombinations({
            id: uploadResult.fileRecombinationId,
            model,
            attachmentId: attachment.id,
            fileId: file.fileId,
            setStatus: (attachmentId, status) => this.setStatus(attachmentId, status, isAttachment),
            onComplete: (model, attachmentId, fileId) => {
              this.uploadComplete(attachment, model, model.blobId, fileCount, cleanups, callback);
            },
          });
        }
      } else {
        await this.setStatus(attachment.id, `Error: ${uploadResult.errorMessage}`, isAttachment);
      }
    } catch (err) {
      await this.setStatus(attachment.id, `Error: ${err}`, isAttachment);
    }
  }

  private async uploadComplete(
    attachment: any,
    model: any,
    fileId: string,
    fileCount: number,
    cleanups: { queueId: string; fileId: string }[],
    callback: (response: { model: unknown; cleanup?: { queueId: string; fileId: string }[] } | undefined) => void
  ) {
    this.fileMap.set(attachment.item.upload[0].fileId, fileId);
    cleanups.push({ queueId: attachment.id, fileId: attachment.item.upload[0].fileId });

    if (this.fileMap.size === fileCount) {
      this.allUploadsComplete(model, cleanups, callback);
    }
  }

  private async allUploadsComplete(
    model: any,
    cleanups: { queueId: string; fileId: string }[],
    callback: (response: { model: unknown; cleanups?: { queueId: string; fileId: string }[] } | undefined) => void
  ) {
    if (this.fileMap.size > 0) {
      this.addServerBlobToModel(model);
    }
    callback({ model, cleanups });
  }

  private addServerBlobToModel(model: any): void {
    const entityType = model.offlineConfig?.entityType;

    switch (entityType) {
      case 'CrimeScene':
        this.addBlobsIntoCrimeSceneModel(model);
        break;
      default:
        return;
    }
  }

  // Entity speicific functions

  // CrimeScene
  private addBlobsIntoCrimeSceneModel(model: any): void {
    const attachmentConfig: AttachmentRoute[] = model.offlineConfig?.attachments;
    attachmentConfig?.forEach((config) => {
      const expr = utils.evalStringExpression<unknown>(config.entityLocation, ['model']);
      if (expr) {
        const entity = expr(model);
        if (entity) {
          if (Array.isArray(entity)) {
            entity.forEach((e) => this.mergeIntoModel(e, config.offlineUploaderKey));
          } else {
            this.mergeIntoModel(entity, config.offlineUploaderKey);
          }
        }
      }
    });
  }

  // Offline uploader will always have the same "upload" key regardless of entity so this is generic
  private mergeIntoModel(entity: any, offlineUploaderKey: string) {
    entity[offlineUploaderKey]?.forEach((offlineupload: any) => {
      const upload = offlineupload.upload[0];
      if (this.fileMap.get(upload.fileId)) {
        offlineupload.upload[0] = {
          ...offlineupload.upload[0],
          blobId: this.fileMap.get(upload.fileId),
        };
      }
      this.fileMap.delete(upload.fileId);
    });
  }

  async setStatus(id: string, status: string, isAttachment: boolean) {
    return await this.db
      .table(isAttachment ? this.attachmentTableName : this.mediaTableName)
      .where('id')
      .equals(id)
      .modify({ status });
  }
}
