import { StateService } from '@nimbus/core/src/lib/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { SiteConfig } from '@nimbus/core/src/lib/core';
import { QueueSyncService } from './queue-sync.service';
import { MockStateService } from '@nimbus/core/src/lib/core';

describe('QueueSyncService', () => {
  let service: QueueSyncService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: StateService,
          useClass: MockStateService,
        },
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
      ],
    });
    service = TestBed.inject(QueueSyncService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
