import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { SiteConfig, StateService } from '@nimbus/core/src/lib/core';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { AttachmentSyncService } from './attachment-sync.service';

describe('AttachmentSyncService', () => {
  let service: AttachmentSyncService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
        { provide: StateService, useClass: MockStateService },
      ],
    });
    service = TestBed.inject(AttachmentSyncService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
