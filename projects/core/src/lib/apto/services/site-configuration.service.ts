import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { SiteConfig, StateService } from '@nimbus/core/src/lib/core';
import { Observable } from 'rxjs';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class SiteConfigurationService {
  private apiUrl: string;
  private siteId?: string;

  constructor(private http: HttpClient, private siteConfig: SiteConfig, stateService: StateService) {
    this.apiUrl = `${siteConfig.apiUrl}/siteconfiguration/upgrade`;
    stateService
      .selectSiteId()
      .pipe(untilDestroyed(this))
      .subscribe((id) => {
        this.siteId = id;
      });
  }

  upgrade(): Observable<boolean> {
    return this.http.post<boolean>(this.apiUrl, { siteId: this.siteId });
  }
}
