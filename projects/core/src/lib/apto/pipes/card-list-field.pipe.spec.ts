import { TestBed } from '@angular/core/testing';
import { FormlyConfig } from '@ngx-formly/core';
import { CardListWrapperComponent } from '@nimbus/core/src/lib/core';
import { CardListFieldPipe } from './card-list-field.pipe';

describe('CardListFieldPipe', () => {
  let pipe: CardListFieldPipe;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CardListFieldPipe],
      providers: [FormlyConfig, CardListWrapperComponent, CardListFieldPipe],
    });

    pipe = TestBed.inject(CardListFieldPipe);
  });

  it('create an instance', () => {
    // Assert
    expect(pipe).toBeTruthy();
  });
});
