import { ComponentFactoryResolver, ComponentRef, Injector, Pipe, PipeTransform } from '@angular/core';
import { FieldType, FormlyConfig, FormlyFieldConfig } from '@ngx-formly/core';
import { CardListWrapperComponent } from '@nimbus/core/src/lib/core';
import { utils } from '@nimbus/core/src/lib/core';
import { TypeOption } from '@ngx-formly/core/lib/models';

@Pipe({
  name: 'cardListField',
})
export class CardListFieldPipe implements PipeTransform {
  constructor(
    private formlyConfig: FormlyConfig,
    private cardListWrapper: CardListWrapperComponent,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector
  ) {}

  transform(field: FormlyFieldConfig): FormlyFieldConfig {
    const type = this.formlyConfig.getType(field.type ?? '');

    if (!type) {
      throw new Error('Field requires a type');
    }

    // Get default options off the type (set in formly-config.ts)
    const defaultOptions = type.defaultOptions;
    utils.mergeDeep(field, defaultOptions);

    // Get default options off the component (set in the component-type.component.ts)
    const componentDefaultOptions = this.getDefaultOptionsFromComponent(field);
    utils.mergeDeep(field, componentDefaultOptions);

    // Get default options off any component that is being extended
    const extendComponentDefaultOptions = this.getDefaultOptionsFromExtendedComponent(type);
    utils.mergeDeep(field, extendComponentDefaultOptions);

    // Apply any options set in the card list wrapper
    this.getOptionsFromCardListWrapper(field);

    return field;
  }

  private getDefaultOptionsFromComponent(field: FormlyFieldConfig): any {
    const componentRef = this.resolveFieldTypeRef(field);

    if (!componentRef) {
      return {};
    }

    return componentRef?.instance?.defaultOptions ?? {};
  }

  private getDefaultOptionsFromExtendedComponent(type: TypeOption): any {
    const extendType = this.formlyConfig.getType(type.extends ?? '');

    if (!extendType || !extendType.component) {
      return {};
    }

    const extendComponent = this.componentFactoryResolver
      .resolveComponentFactory<any>(extendType.component)
      .create(this.injector);

    return extendComponent?.instance?.defaultOptions ?? {};
  }

  private getOptionsFromCardListWrapper(field: FormlyFieldConfig): void {
    if (this.cardListWrapper) {
      field.props = field.props ?? {};

      field.props = {
        ...field.props,
        ...{
          filterColumns: this.cardListWrapper.currentFilters ?? field.props['filterColumns'],
          pageSize: this.cardListWrapper.currentPageSize ?? field.props['pageSize'],
          pageIndex: this.cardListWrapper.currentPage ?? field.props['pageIndex'],
        },
      };
    }
  }

  private resolveFieldTypeRef(field: any): ComponentRef<FieldType> | null {
    const type: any = this.formlyConfig.getType(field.type ?? field.name ?? '');
    if (!type) {
      return null;
    }

    if (!type.component || type['_componentRef']) {
      return type['_componentRef'];
    }

    const { _resolver, _injector } = field.options;
    if (!_resolver || !_injector) {
      return null;
    }

    const componentRef = _resolver.resolveComponentFactory(type.component).create(_injector);
    Object.defineProperty(type, '_componentRef', { enumerable: false, writable: true, configurable: true });
    type['_componentRef'] = componentRef;
    componentRef.destroy();

    return type['_componentRef'];
  }
}
