import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FormlySandboxComponent } from './formly-sandbox.component';
import { PageComponent } from '@nimbus/core/src/lib/core';

describe('FormlySandboxComponent', () => {
  let component: FormlySandboxComponent;
  let fixture: ComponentFixture<FormlySandboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormlySandboxComponent, PageComponent],
      imports: [MatSnackBarModule, MatIconModule, MatCheckboxModule, ReactiveFormsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlySandboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
