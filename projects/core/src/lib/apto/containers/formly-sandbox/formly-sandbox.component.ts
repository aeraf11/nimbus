import { Component, ChangeDetectionStrategy } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';

@Component({
  templateUrl: './formly-sandbox.component.html',
  styleUrls: ['./formly-sandbox.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormlySandboxComponent {
  inputFields = new UntypedFormControl('');
  inputModel = new UntypedFormControl('');
  inputContext = new UntypedFormControl('');
  inputShowDebug = new UntypedFormControl(false);

  formlyModel: any;
  formlyFields: any;
  formlyContext: any;

  test(): void {
    let fields = this.parse(this.inputFields.value);

    if (!Array.isArray(fields)) {
      fields = [fields];
    }

    this.formatTextAreaControl(this.inputFields, fields);
    this.formlyFields = fields;
    this.formlyModel = this.inputModel.value ? this.parse(this.inputModel.value) : {};
    this.formlyContext = this.inputContext.value ? this.parse(this.inputContext.value) : {};
  }

  parse(str: string): any {
    try {
      return JSON.parse(str);
    } catch (ex) {
      console.error(ex);
      return {};
    }
  }

  modelChanged(model: any): void {
    this.formatTextAreaControl(this.inputModel, model);
  }

  contextChanged(context: any): void {
    this.formatTextAreaControl(this.inputContext, context);
  }

  private formatTextAreaControl(control: UntypedFormControl, value: any): void {
    control.setValue(JSON.stringify(value, null, 4));
  }
}
