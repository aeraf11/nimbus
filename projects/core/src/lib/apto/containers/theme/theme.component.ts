import { DialogService, SnackMessageComponent } from '@nimbus/core/src/lib/core';
import { Component, ChangeDetectionStrategy, ViewEncapsulation } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormDefinition } from '@nimbus/core/src/lib/core';
import { DialogConfig, DialogType, SnackMessageType } from '@nimbus/core/src/lib/core';
import { StateService } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ni-theme',
  templateUrl: './theme.component.html',
  styleUrls: ['./theme.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ThemeComponent {
  definition: FormDefinition = {
    id: 'Theme Gallery',
    fields: [
      {
        fieldGroup: [
          { template: '<h1 class="section-header">Formly Edit Components</h1>' },

          { template: '<h2>Action Bar</h2>' },
          {
            type: 'actionbar',
            wrappers: ['card'],
            props: { actionStyle: 'Button', actionLayout: 'Column' },
            fieldGroup: [
              {
                props: { label: 'Foo', url: '/home' },
              },
              {
                props: { label: 'Bar', url: '/home' },
              },
            ],
          },
          { template: '<hr><h2>Button List</h2>' },
          {
            key: 'buttonlist',
            type: 'buttonlist',
            props: {
              enableClear: true,
              buttons: [
                {
                  text: 'Button One',
                  value: 'button1value',
                },
                {
                  text: 'Button Two',
                  value: 'button2value',
                },
                {
                  text: 'Button Three',
                  value: 'button3value',
                },
              ],
            },
          },

          { template: '<hr><h2>Checkbox</h2>' },
          {
            key: 'checkbox',
            type: 'checkbox',
            props: {
              label: 'Checkbox',
              required: true,
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
              pattern: 'true',
            },
            validation: {
              messages: {
                pattern: 'This is a validation error',
              },
            },
          },

          { template: '<hr><h2>Data Table</h2>' },
          {
            type: 'datatable',
            props: {
              filterColumns: [],
              showPaging: true,
              showQuickSearch: true,
              selectMode: 'multi',
              listName: 'Operations',
              sortColumn: 'name',
              columnDefinitions: [
                {
                  id: 'operationName',
                  value: 'Operation Name',
                  filterType: 'input',
                },
                {
                  id: 'operationAlias',
                  value: 'Operation Alias',
                },
              ],
              idColumn: 'operationId',
              actions: [
                {
                  key: 'view',
                  name: 'View',
                  url: 'home',
                },
                {
                  key: 'edit',
                  name: 'Edit',
                  url: 'home',
                },
              ],
            },
          },

          { template: '<hr><h2>Data Table Responsive Tabs</h2>' },
          {
            key: 'dataTableTabs',
            type: 'datatable',
            props: {
              savedOptionsId: 'submissions',
              listName: 'Submissions',
              selectMode: 'none',
              sortColumn: 'reference',
              columnDefinitions: [
                {
                  id: 'reference',
                  value: 'Reference',
                  isSortable: true,
                  filterParams: {
                    formlyType: 'input',
                  },
                  tabs: 'Reference, Details',
                },
                {
                  id: 'operationName',
                  value: 'Operation',
                  isSortable: true,
                  filterParams: {
                    formlyType: 'input',
                  },
                  tabs: 'Details, Details',
                },
                {
                  id: 'creationDate',
                  value: 'Date Created',
                  dataType: 'date',
                  isSortable: true,
                  filterParams: {
                    formlyType: 'daterange',
                    props: {
                      label: 'Date Created',
                      showQuickLinks: true,
                    },
                  },
                  tabs: 'Details, Details',
                },
                {
                  id: 'submittingOfficer',
                  value: 'Submitting Officer',
                  dataType: 'profile',
                  isSortable: true,
                  filterParams: {
                    formlyType: 'multichoice',
                    searchValueColumn: 'fullName',
                    props: {
                      listName: 'Users',
                      valueProp: 'personId',
                      labelProp: 'fullName',
                      sortColumn: 'fullName',
                      selectColumns: ['personId', 'fullName'],
                    },
                  },
                  computedValue: {
                    name: '${submittingOfficer}',
                    blobId: '${submittingOfficerBlobId}',
                  },
                  tabs: 'Submitter, Details',
                },
                {
                  id: 'submittingOfficerBlobId',
                  isHidden: true,
                },
                {
                  id: 'statusName',
                  value: 'Status',
                  dataType: 'status',
                  isSortable: true,
                  minWidth: '200px',
                  filterParams: {
                    formlyType: 'multichoice',
                    props: {
                      lookupKey: 'SubmissionStatus',
                    },
                  },
                  computedValue: {
                    value: '${statusName}',
                    cssClass: '${cssClass}',
                  },
                  tabs: 'Status/Type, Status/Type',
                },
                {
                  id: 'canEdit',
                  isHidden: true,
                },
                {
                  id: 'submissionTypeLookupId',
                  value: 'Submission Type',
                  dataType: 'lookup',
                  isSortable: true,
                  isfilterable: true,
                  filterParams: {
                    filterAlias: 'submissionType',
                    formlyType: 'multichoice',
                    props: {
                      lookupKey: 'SubmissionType',
                    },
                  },
                  tabs: 'Status/Type, Status/Type',
                },
                {
                  id: 'unreadMessages',
                  dataType: 'messages',
                  tabs: 'Details, Details',
                },
              ],
              idColumn: 'submissionId',
              filterColumns: [],
              showPaging: true,
              showQuickSearch: true,
              filterTitle: 'Filter Submissions',
              actionsTabs: 'Reference, Details',
              actions: [
                {
                  key: 'view',
                  name: 'View',
                  url: 'submissions/*{submissionTypeLookupId}/view/${submissionId}',
                  fallback: '5c75f686-fc8d-4588-ab62-8d250a03011d',
                  params: {
                    backButtonUrl: '/submissions/list',
                  },
                },
                {
                  key: 'edit',
                  name: 'Edit',
                  url: 'submissions/*{submissionTypeLookupId}/edit?submissionId=${submissionId}',
                  fallback: '5c75f686-fc8d-4588-ab62-8d250a03011d',
                  visibility: '${canEdit}',
                },
              ],
            },
          },

          { template: '<hr><h2>Data Cards</h2>' },
          { template: '<h3>Crime Scenes</h3>' },
          {
            key: 'datacards',
            type: 'datacards',
            props: {
              selectMode: 'multiple',
              listName: 'CrimeScenes',
              sortColumn: 'reference',
              noResultsText: 'No crime scenes found',
              columnDefinitions: [
                {
                  id: 'reference',
                },
                {
                  id: 'crimeSceneId',
                },
                {
                  id: 'name',
                },
                {
                  id: 'crimeSceneTypeLookup',
                },
                {
                  id: 'thumbnail',
                },
                {
                  id: 'entityLocationId',
                },
              ],
              filterColumns: [
                {
                  column: 'operationId',
                  stringFilter: '4d604d03-d14c-4bb6-8c79-28b2ab791f6f',
                  constantFilter: true,
                },
              ],
              idColumn: 'crimeSceneId',
              itemTemplate: {
                type: 'crimescene',
              },
              itemsUi: {
                columnCounts: 3,
                gap: '1rem',
                cardWrapper: true,
              },
            },
          },
          {
            props: {
              title: 'Task Summary',
            },
          },
          { template: '<h3>Task Summary</h3>' },
          {
            key: 'datatableSingle',
            type: 'datacards',
            props: {
              listName: 'EntityCalendarItem',
              idColumn: 'entityCalendarItemId',
              sortColumn: 'requiredCompletionDate',
              sortDirection: 'asc',
              pageSize: 10,
              columnDefinitions: [
                {
                  id: 'entityCalendarItemId',
                },
                {
                  id: 'requiredCompletionDate',
                },
                {
                  id: 'description',
                },
                {
                  id: 'actionLinked',
                },
                {
                  id: 'overdue',
                },
              ],
              showPaging: false,
              itemTemplate: {
                wrappers: ['card'],
                templateOptions: {
                  cardEnableHoverClass: true,
                },
                type: 'tasksummary',
              },
              itemsUi: {
                columnCounts: [1],
                gap: '0.5rem',
              },
              templatedProperties: {
                metaList: {
                  targetCount: 10,
                  metaFilters: [
                    {
                      filterColumns: [
                        {
                          column: 'requiredCompletionDate',
                          constantFilter: true,
                          stringFilter: '={formState.helper.dateTime.getOverdueFilter()}',
                          isJsonFilter: true,
                        },
                        {
                          column: '',
                          constantFilter: true,
                          stringFilter: '${context.userProfileId}',
                          entityTypeId: 13,
                        },
                      ],
                    },
                    {
                      filterColumns: [
                        {
                          column: 'requiredCompletionDate',
                          constantFilter: true,
                          stringFilter: '={formState.helper.dateTime.getUpcomingFilter()}',
                          isJsonFilter: true,
                        },
                        {
                          column: '',
                          constantFilter: true,
                          stringFilter: '${context.userProfileId}',
                          entityTypeId: 13,
                        },
                      ],
                    },
                  ],
                },
              },
            },
          },
          { template: '<hr><h2>Date</h2>' },
          {
            key: 'date',
            type: 'date',
            props: {
              label: 'Date',
              required: true,
              placeholder: 'This is placeholder text',
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h2>Date Range</h2>' },
          {
            key: 'daterange',
            type: 'daterange',
            props: {
              label: 'Date Range',
              required: true,
              placeholder: 'This is placeholder text',
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h2>Date Time</h2>' },
          {
            key: 'datetime',
            type: 'datetime',
            props: {
              label: 'Date Time',
              required: true,
              showTime: true,
              showTimeZone: true,
              placeholder: 'This is placeholder text',
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h2>Document Editor</h2>' },
          {
            key: 'documentEditor',
            type: 'doceditor',
            props: {},
          },
          { template: '<hr><h2>Document Viewer</h2>' },
          {
            key: 'documentViewer',
            type: 'docviewer',
            props: {},
          },

          { template: '<hr><h2>Form Actions</h2>' },
          {
            type: 'formactions',
            fieldGroup: [
              {
                props: { label: 'Cancel', action: 'cancel' },
              },
              {
                props: { label: 'Save a Draft', color: 'primary', action: 'draft' },
              },
              {
                props: { label: 'Save & Submit', color: 'primary', action: 'save' },
                expressions: {
                  'props.disabled': 'formState?.isValid ? !formState?.isValid() : true',
                },
              },
            ],
          },

          { template: '<hr><h2>Input</h2>' },
          {
            key: 'input',
            type: 'input',
            props: {
              label: 'Input',
              required: true,
              placeholder: 'This is some placeholder text',
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h2>Layout Choice</h2>' },
          {
            key: 'layoutchoice',
            type: 'layoutchoice',
            props: {
              options: [
                {
                  id: 'column',
                  icon: 'fas fa-pause',
                },
                {
                  id: 'grid',
                  icon: 'fas fa-grid-2',
                },
              ],
            },
          },

          { template: '<hr><h2>Lookup</h2>' },
          {
            key: 'lookup',
            type: 'lookup',
            props: {
              label: 'Lookup',
              required: true,
              lookupKey: 'PersonTypeGroup',
              leafSelection: false,
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },
          { template: '<hr><h2>Messages</h2>' },
          {
            key: 'datatableSingle',
            type: 'messages',
            props: {
              maxHeight: '15rem',
              fullWidthContent: 1,
              allowAddMessage: true,
            },
            expressions: {
              'props.filterColumns': "[{column: '', stringFilter: model.selectedOperationId, entityTypeId: 2}]",
            },
          },
          { template: '<hr><h2>Multi-Checkbox</h2>' },
          {
            key: 'multicheckbox',
            type: 'multicheckbox',
            props: {
              label: 'Multi Choice',
              required: true,
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
            expressions: {
              'props.options': 'formState.selectOptionsData.colours',
            },
          },

          { template: '<hr><h2>Multi Choice</h2>' },
          {
            key: 'multichoice',
            type: 'multichoice',
            props: {
              label: 'Multi Choice',
              required: true,
              lookupKey: 'CMOCrimeCategoryOffense',
              addCustom: true,
              enableTree: 'auto',
              leafSelection: false,
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h2>Radio</h2>' },
          {
            key: 'radio',
            type: 'radio',
            props: {
              label: 'Radio',
              required: true,
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
              valueProp: 'id',
              labelProp: 'name',
            },
            expressions: {
              'props.options': 'formState.selectOptionsData.teams',
            },
          },

          { template: '<hr><h2>Read Only</h2>' },
          {
            key: 'readonly',
            type: 'readonly',
            props: {
              label: 'Read Only',
              placeholder: 'This is some placeholder text',
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h2>Repeat Items</h2>' },
          {
            key: 'repeatItems',
            type: 'repeat',
            fieldArray: {
              fieldGroup: [{ key: 'name', type: 'textview' }],
            },
          },

          { template: '<hr><h2>Repeat Carousel</h2>' },
          {
            key: 'repeatItemsCarousel',
            type: 'repeat',
            props: {
              enableCarousel: true,
            },
            fieldArray: {
              fieldGroup: [
                {
                  key: 'chartdonut',
                  type: 'chartdonut',
                  props: {
                    label: 'Exhibits by Type',
                    title: '${total}',
                    subTitle: 'Exhibits',
                    width: '400px',
                    contextProperty: 'filterString',
                    legend: {
                      width: '0',
                      visible: false,
                    },
                  },
                },
              ],
            },
          },

          { template: '<hr><h2>Select</h2>' },
          {
            key: 'select',
            type: 'select',
            props: {
              label: 'Select',
              required: true,
              valueProp: 'id',
              labelProp: 'name',
              placeholder: 'This is some placeholder text',
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
            expressions: {
              'props.options': 'formState.selectOptionsData.teams',
            },
          },

          { template: '<hr><h2>Select Dynamic</h2>' },
          {
            key: 'select-submission',
            type: 'select2',
            props: {
              label: 'Submission Picker',
              listName: 'Submissions',
              resultCount: 1000,
              placeholder: 'This is some placeholder text',
              labelTemplate: '${submissionId}: ref: ${reference}',
              selectColumns: ['submissionId', 'reference'],
            },
          },

          { template: '<hr><h2>Slide Toggle</h2>' },
          {
            key: 'toggle',
            type: 'toggle',
            props: {
              label: 'Slide Toggle',
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h2>Stepper</h2>' },
          {
            type: 'stepper',
            fieldGroup: [
              {
                props: { label: 'Personal data' },
                fieldGroup: [
                  {
                    key: 'firstname',
                    type: 'input',
                    props: {
                      label: 'First name',
                      required: true,
                      description:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                    },
                  },
                  {
                    key: 'age',
                    type: 'input',
                    props: {
                      type: 'number',
                      label: 'Age',
                      required: true,
                      description:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                    },
                  },
                ],
              },
              {
                props: { label: 'Destination' },
                fieldGroup: [
                  {
                    key: 'country',
                    type: 'input',
                    props: {
                      label: 'Country',
                      required: true,
                      description:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                    },
                  },
                ],
              },
              {
                props: { label: 'Day of the trip' },
                fieldGroup: [
                  {
                    key: 'day',
                    type: 'date',
                    props: {
                      label: 'Day of the trip',
                      required: true,
                      description:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                    },
                  },
                ],
              },
            ],
          },

          { template: '<hr><h2>Table</h2>' },
          {
            key: 'table',
            type: 'table',
            props: {
              columnDefinitions: [
                { key: 'exhibitReference', columnName: 'Exhibit Reference', tab: 'First' },
                { key: 'sealNumber', columnName: 'Seal Number', tab: 'First' },
                { key: 'exhibitDescription', columnName: 'Description', isEditorHeader: true, tab: 'Second' },
                { key: 'seizedDate', columnName: 'Seized Date', dataType: 'date', tab: 'Second' },
              ],
              formId: 'Exhibits.Add',
              enableAdd: true,
              enableDelete: false,
              enableEdit: true,
              saveText: 'Custom Save',
              cancelText: 'Custom Cancel',
              deleteText: 'Custom Delete',
              addText: 'Custom Add',
              disabled: false,
              itemLimit: 2,
              required: true,
              dialogWidth: '50vw',
              label: 'Exhibits',
              placeholder: 'This is placeholder text',
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
            validators: {
              validation: ['arrayRequired'],
            },
          },

          { template: '<hr><h2>Text Area</h2>' },
          {
            key: 'textarea',
            type: 'textarea',
            props: {
              label: 'Text Area',
              required: true,
              cols: 1,
              rows: 4,
              placeholder: 'This is some placeholder text',
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h2>Text Area (Read Only)</h2>' },
          {
            key: 'textareareadonly',
            type: 'textareareadonly',
            props: {
              label: 'Text Area Read Only',
              required: false,
              cols: 1,
              rows: 4,
              placeholder: 'This is some placeholder text',
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h2>Text Panel (Read Only)</h2>' },
          {
            key: 'textpanel',
            type: 'textpanel',
            props: {
              label: 'Text Panel Read Only',
              color: 'alert-success',
              placeholder: 'This is some placeholder text',
              ariaLabel: 'Alert panel',
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h2>Time</h2>' },
          {
            key: 'time',
            type: 'time',
            props: {
              label: 'Time',
              required: true,
              showSeconds: true,
              defaultTime: { hours: 12, minutes: 30, seconds: 30 },
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h2>Type Ahead</h2>' },
          {
            key: 'typeahead',
            type: 'typeahead',
            props: {
              label: 'Type Ahead',
              required: true,
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
              listName: 'Operations',
              valueProp: 'operationId',
              labelProp: 'name',
            },
          },

          { template: '<hr><h2>Uploader</h2>' },
          {
            key: 'uploader',
            type: 'uploader',
            props: {
              label: 'Uploader',
              required: true,
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h2>Uploader Offline</h2>' },
          {
            key: 'uploaderoffline',
            type: 'uploaderoffline',
            props: {
              label: 'Uploader Offline',
              required: true,
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
              itemTableName: 'devGallery',
            },
          },

          { template: '<hr><h2>Yes No</h2>' },
          {
            key: 'yesno',
            type: 'yesno',
            props: {
              label: 'Yes No',
              required: true,
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h1 class="section-header">Formly View Components</h1>' },

          { template: '<h2>Date List View</h2>' },
          {
            key: 'datelistview',
            type: 'datelistview',
            wrappers: ['card'],
            props: { label: 'Key Dates' },
            fieldGroup: [
              {
                key: 'bailDate',
                type: 'textview',
                props: { label: 'BAIL', icon: 'fas fa-door-open', dataType: 'date' },
              },
              {
                key: 'courtDate',
                type: 'textview',
                props: { label: 'COURT', icon: 'fas fa-gavel', dataType: 'date' },
              },
              {
                key: 'cpsDate',
                type: 'textview',
                props: { label: 'CPS', icon: 'fas fa-balance-scale', dataType: 'date' },
              },
            ],
          },

          { template: '<hr><h2>Divider View</h2>' },
          {
            key: 'divider',
            type: 'divider',
            wrappers: ['card'],
            props: { label: 'Divider' },
          },

          { template: '<hr><h2>Entity Add View</h2>' },
          {
            type: 'entityadd',
            props: {
              entities: [
                {
                  icon: 'fas fa-male',
                  subIcon: 'fal fa-plus',
                  text: 'Nominal',
                  formId: 'Investigator.Cases.Nominal.Add',
                },
                {
                  icon: 'fas fa-map-marker-alt',
                  subIcon: 'fal fa-plus',
                  text: 'Scene',
                  formId: 'Investigator.Cases.Scene.Add',
                },
              ],
            },
          },

          { template: '<hr><h2>Entity Summary View</h2>' },
          {
            key: 'entitysummary',
            type: 'entitysummary',
            props: {
              entitySummaryItems: [
                {
                  icon: {
                    name: 'fal fa-folder',
                  },
                  label: {
                    text: 'Case',
                  },
                  text: {
                    property: 'operationName',
                  },
                },
                {
                  isDivider: true,
                },
                {
                  icon: {
                    name: 'fal fa-hashtag',
                  },
                  label: {
                    text: 'Reference',
                  },
                  text: {
                    property: 'operationAlias',
                  },
                },
              ],
            },
          },

          { template: '<hr><h2>Field View Type</h2>' },
          {
            key: 'fieldview',
            wrappers: ['card'],
            type: 'fieldview',
            props: {
              label: 'Submission',
              layout: 'Column',
            },
            fieldGroup: [
              {
                key: 'operationName',
                type: 'textview',
                props: { label: 'Operation:', classNames: 'heading mb-12 fs-19' },
              },
              {
                key: 'referenceName',
                type: 'textview',
                props: { label: 'Reference', classNames: 'heading mb-12 fs-19' },
              },
              {
                key: 'statusName',
                type: 'textview',
                props: { label: 'Status:', classNames: 'heading fs-19' },
              },
            ],
          },

          { template: '<hr><h2>Identification</h2>' },
          {
            key: 'identification',
            type: 'datacards',
            props: {
              selectMode: 'single',
              listName: 'MyIdents',
              sortColumn: 'reference',
              noResultsText: 'No idents found',
              title: 'Identifications',
              icon: '',
              columnDefinitions: [
                {
                  id: 'reference',
                },
                {
                  id: 'nameOfIndividual',
                },
                {
                  id: 'defaultPhotoThumbnailBlobId',
                },
                {
                  id: 'defaultEntityMediaBlobId',
                },
                {
                  id: 'hasAttachments',
                },
                {
                  id: 'actionRequired',
                },
              ],
              idColumn: 'identId',
              itemTemplate: {
                type: 'identification',
              },
              itemsUi: {
                columnCounts: 3,
                gap: '1rem',
              },
              label: '',
            },
          },

          { template: '<hr><h2>Inline Calendar</h2>' },
          {
            key: 'inlinecalendar',
            type: 'inlinecalendar',
            props: {
              width: '400px',
              showTitle: true,
              subtitleFontSize: '12px',
              subtitleLineHeight: '14px',
              titleFontSize: '32px',
              titleLineHeight: '36px',
            },
          },

          { template: '<hr><h2>Jumbo Link</h2>' },
          {
            key: 'jumbolink',
            type: 'jumbolink',
          },

          { template: '<hr><h2>Person View</h2>' },
          {
            key: 'person',
            type: 'personview',
            wrappers: ['card'],
            props: { label: 'Person View', imageSize: 'xlarge', noData: 'No person data' },
            fieldGroup: [
              { key: 'fullName', type: 'textview', props: { classNames: 'heading' } },
              { key: 'jobDescription', type: 'textview' },
              { key: 'collarNo', type: 'textview' },
              { key: 'email', type: 'textview' },
              { key: 'contactNo', type: 'textview' },
            ],
          },

          { template: '<hr><h2>Person List View</h2>' },
          {
            key: 'personlist',
            type: 'personlistview',
            wrappers: ['card'],
            props: { label: 'People', imageSize: 'medium', photoProp: 'photoId', initialsProp: 'initials' },
            fieldGroup: [
              { key: 'fullName', type: 'textview', props: { classNames: 'heading' } },
              { key: 'personType', type: 'textview' },
            ],
          },

          { template: '<hr><h2>Rich Text Editor</h2>' },
          {
            key: 'richTextEditor',
            type: 'richtext',
            props: {
              label: 'Rich Text',
              required: true,
            },
          },
          { template: '<hr><h2>Rich Text Viewer</h2>' },
          {
            key: 'richTextViewer',
            type: 'richtextviewer',
            props: {
              label: 'Rich Text Viewer (Readonly)',
            },
          },
          { template: '<hr><h2>Rich Text Fields View</h2>' },
          {
            type: 'richfieldsview',
            wrappers: ['card'],
            key: 'overview',
            props: {
              label: 'Overview',
              noData: 'No overview data found',
            },
            fieldGroup: [
              {
                key: 'circumstances',
              },
              {
                key: 'keywords',
                props: { label: 'Notes' },
              },
            ],
          },

          { template: '<hr><h2>Route List</h2>' },
          {
            key: 'routelist',
            type: 'routelist',
            props: {
              marginOnItems: false,
              fieldArray: {
                type: 'jumbolink',
              },
            },
          },

          { template: '<hr><h2>Table View</h2>' },
          {
            key: 'tableview',
            type: 'tableview',
            props: {
              columnDefinitions: [
                { key: 'exhibitReference', columnName: 'Reference' },
                { key: 'icon', columnName: 'Cat.', dataType: 'icon' },
                { key: 'exhibitType', columnName: 'Type' },
                { key: 'exhibitDescription', columnName: 'Description' },
                { key: 'attributedTo', columnName: 'Attributed', dataType: 'profile' },
                { key: 'priority', columnName: 'Priority', dataType: 'status' },
                { key: 'pin', columnName: 'Pin / Password' },
              ],
              description:
                'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
            },
          },

          { template: '<hr><h1 class="section-header">Formly Wrappers</h1>' },

          { template: '<hr><h2>Wrapper - Card</h2>' },
          {
            key: 'card',
            wrappers: ['card'],
            fieldGroup: [
              {
                key: 'name',
                type: 'input',
                props: {
                  type: 'text',
                  label: 'Name',
                },
              },
              {
                key: 'age',
                type: 'input',
                props: {
                  type: 'text',
                  label: 'Age',
                },
              },
            ],
          },
          { template: '<hr><h2>Wrapper - Grid</h2>' },
          {
            key: 'grid',
            wrappers: ['grid'],
            fieldGroup: [
              {
                key: 'left',
                type: 'input',
                props: {
                  type: 'text',
                  label: 'Left',
                  gridArea: 'left',
                },
              },
              {
                key: 'right',
                type: 'input',
                props: {
                  type: 'text',
                  label: 'Right',
                  gridArea: 'right',
                },
              },
            ],
          },
          { template: '<hr><h2>Wrapper - Panel</h2>' },
          {
            key: 'address',
            wrappers: ['panel'],
            props: { label: 'Address' },
            fieldGroup: [
              {
                key: 'town',
                type: 'input',
                props: {
                  required: true,
                  type: 'text',
                  label: 'Town',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          { template: '<hr><h2>Wrapper - Section Edit</h2>' },
          {
            wrappers: ['sectionedit'],
            props: {
              sectionEditMarginTop: '0',
              sectionEditMarginBottom: '0',
            },
            fieldGroup: [
              {
                fieldGroup: [
                  {
                    wrappers: ['card'],
                    fieldGroup: [
                      {
                        props: {
                          cardAlt: true,
                          singleLine: true,
                          label: 'Theme: ',
                          inlineText: true,
                          alwaysShowLabel: true,
                          dataType: 'lookup',
                        },
                        key: 'themeId',
                        type: 'textview',
                      },
                    ],
                  },
                ],
              },
              {
                fieldGroup: [
                  {
                    fieldGroup: [
                      {
                        wrappers: [],
                        props: {
                          label: 'Theme',
                          lookupKey: 'Themes',
                          enableFieldLabel: true,
                          className: 'user-settings alt-form-field',
                        },
                        key: 'themeId',
                        type: 'lookup',
                      },
                    ],
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
    options: {
      formState: {
        selectOptionsData: {
          teams: [
            { id: '1', name: 'Bayern Munich', sportId: '1' },
            { id: '2', name: 'Real Madrid', sportId: '1' },
            { id: '3', name: 'Cleveland', sportId: '2' },
            { id: '4', name: 'Miami', sportId: '2' },
          ],
          players: [
            { id: '1', name: 'Bayern Munich (Player 1)', teamId: '1' },
            { id: '2', name: 'Bayern Munich (Player 2)', teamId: '1' },
            { id: '3', name: 'Real Madrid (Player 1)', teamId: '2' },
            { id: '4', name: 'Real Madrid (Player 2)', teamId: '2' },
            { id: '5', name: 'Cleveland (Player 1)', teamId: '3' },
            { id: '6', name: 'Cleveland (Player 2)', teamId: '3' },
            { id: '7', name: 'Miami (Player 1)', teamId: '4' },
            { id: '8', name: 'Miami (Player 2)', teamId: '4' },
          ],
          colours: [
            { label: 'Red', value: 'red' },
            { label: 'Blue', value: 'blue' },
            { label: 'Green', value: 'green' },
          ],
        },
      },
    },
  };

  model = {
    richTextViewer:
      '<p><span style="font-size: 14pt;"><span style="color: rgb(255, 0, 0); text-decoration: inherit;">Some markup </span></span><span style="font-size: 24pt;"><span style="font-family: Verdana, Geneva, sans-serif;"><strong><em><span style="text-decoration: underline;">Something else</span></em></strong></span></span><strong>​</strong></p>',
    selectedOperationId: '4d604d03-d14c-4bb6-8c79-28b2ab791f6f',
    readonly: 'foobar',
    textpanel: 'foobar',
    textareareadonly:
      'READONLY AutoSize - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras feugiat vehicula eros, sed dictum purus luctus sit amet. Etiam mollis hendrerit semper. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis convallis lorem et ante laoreet efficitur. In quam quam, dignissim in ipsum quis, varius condimentum diam. In hac habitasse platea dictumst. Vestibulum nisi nibh, vulputate ac dapibus sit amet, dignissim vel sapien. Donec nisl justo, finibus vel tincidunt at, imperdiet at lacus. Nam ante eros, accumsan at dapibus id, placerat in enim. Maecenas non euismod magna. Vestibulum nisi sapien, hendrerit vel rutrum sit amet, cursus in ex. Aliquam iaculis lacus urna. In dictum diam ac lacus elementum, non maximus sem efficitur. Ut feugiat vulputate purus sit amet hendrerit.',
    exhibits2: [
      {
        exhibitReference: '123',
        sealNumber: '456',
        exhibitDescription: 'Exhibit 1',
      },

      {
        exhibitReference: '789',
        sealNumber: '1011',
        exhibitDescription: 'Exhibit 2',
      },
    ],
    lookup: '24c20155-1410-4594-b81d-efb8d504886b',
    table: [
      {
        exhibitReference: 'stuff',
        sealNumberKnown: true,
        exhibitDescription: 'q',
        seizedDate: {
          dateTime: '2021-05-27T13:40:38+01:00',
          timeZoneId: null,
        },
        seizedBy: 'dwdw',
        evidenceType: null,
        exhibitType: null,
        healthHazard: false,
        dnaPreservation: false,
        examinationType: null,
        examinations: null,
        examinationSupportingInfo: 'ssss',
        exhibitLocation: 'Branch',
        returnDestination: 'Branch',
        sealNumber: '1234',
        yourBranch: true,
        returnDestinationYourBranch: true,
      },
    ],
    tableview: [
      {
        exhibitReference: '001',
        exhibitType: 'Laptop (non-Mac)',
        exhibitDescription: 'laptop unknown make',
        seizedFrom: null,
        attributedTo: 'Joe Bloggs',
        recoveredBy: 'Jim Kent',
        priority: {
          value: 'High',
          cssClass: 'c-neg-3',
        },
        pin: null,
        icon: 'fas fa-arrow-up',
      },
      {
        exhibitReference: '002',
        exhibitType: 'Laptop (non-Mac)',
        exhibitDescription: 'laptop unknown make',
        seizedFrom: null,
        attributedTo: 'Joe Bloggs',
        recoveredBy: 'Jim Kent',
        priority: null,
        pin: '12345',
        icon: 'far fa-home',
      },
    ],
    multichoice: [
      '2bbb5139-62ae-4484-9cb3-a7de39479585',
      '34144e78-7545-450b-90e6-01a2bc4e1c2e',
      'ac6d4246-2062-43fa-bd4a-0ad4fa32368c',
    ],
    overview: {
      circumstances:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
      keywords:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
    },
    fieldview: { operationName: 'Test operation', statusName: 'Investigation', referenceName: '88088808-02' },
    person: {
      fullName: 'Forename Surname',
      initials: 'FS',
      jobDescription: 'Job Description',
      collarNo: '12345',
      contactNo: '08777777777',
      email: 'forename.surname@person.com',
      photoId: null,
    },
    personlist: [
      {
        fullName: 'Person One the quick brown fox jumps over the lazy dog',
        initials: 'PO',
        jobDescription: 'Job Description',
        personType: 'Analyst',
        photoId: null,
      },
      {
        fullName: 'Person Two',
        initials: 'PT',
        jobDescription: 'Job Description',
        personType: 'Analyst',
        photoId: null,
      },
      {
        fullName: 'Person Three',
        initials: 'PT',
        jobDescription: 'Job Description',
        personType: 'Analyst',
        photoId: null,
      },
      {
        fullName: 'Person Four',
        initials: 'PF',
        jobDescription: 'Job Description',
        personType: 'Analyst',
        photoId: null,
      },
    ],
    datelistview: {
      bailDate: '12/4/2021',
      courtDate: '12/8/2021',
      cpsDate: '12/12/2021',
    },
    repeatItems: [
      { name: 'Item One' },
      { name: 'Item Two' },
      { name: 'Item Three' },
      { name: 'Item Four' },
      { name: 'Item Five' },
      { name: 'Item Six' },
      { name: 'Item Seven' },
    ],
    repeatItemsCarousel: [
      {
        chartdonut: {
          table1: {
            results: [
              { name: 'Computer Device', value: 1, cssClass: 'c-neu-4', url: '/form-gallery#1' },
              { name: 'Drugs', value: 2, cssClass: 'c-pos-4', url: '/form-gallery#2' },
              { name: 'Fibres', value: 3, cssClass: 'c-neg-5', url: '/form-gallery#3' },
              { name: 'Firearms', value: 1, cssClass: 'c-neu-5', url: '/form-gallery#4' },
              { name: 'Footwear', value: 2, cssClass: 'c-pos-5', url: '/form-gallery#5' },
              { name: 'Mobile Device', value: 6, cssClass: 'c-neu-6', url: '/form-gallery#6' },
            ],
          },
          table2: {
            results: [{ total: 15 }],
          },
        },
      },
      {
        chartdonut: {
          table1: {
            results: [
              { name: 'Computer Device', value: 4, cssClass: 'c-neu-4', url: '/form-gallery#1' },
              { name: 'Drugs', value: 1, cssClass: 'c-pos-4', url: '/form-gallery#2' },
              { name: 'Fibres', value: 4, cssClass: 'c-neg-5', url: '/form-gallery#3' },
              { name: 'Firearms', value: 2, cssClass: 'c-neu-5', url: '/form-gallery#4' },
            ],
          },
          table2: {
            results: [{ total: 11 }],
          },
        },
      },
      {
        chartdonut: {
          table1: {
            results: [
              { name: 'Computer Device', value: 4, cssClass: 'c-neu-4', url: '/form-gallery#1' },
              { name: 'Drugs', value: 6, cssClass: 'c-pos-4', url: '/form-gallery#2' },
            ],
          },
          table2: {
            results: [{ total: 10 }],
          },
        },
      },
    ],
    inlinecalendar: [
      {
        entityCalendarItemId: '01',
        requiredCompletionDate: '2022-03-05',
        description: 'This is item one',
        requiresCompletion: true,
        actualCompletionDate: '',
      },
      {
        entityCalendarItemId: '01',
        requiredCompletionDate: '2022-03-08',
        description: 'This is item one',
        requiresCompletion: false,
        actualCompletionDate: '',
      },
      {
        entityCalendarItemId: '02',
        requiredCompletionDate: '2022-03-30',
        description: 'This is item two',
        requiresCompletion: true,
        actualCompletionDate: '2022-03-31',
      },
      {
        entityCalendarItemId: '03',
        requiredCompletionDate: '2022-04-26',
        description: 'This is item three',
      },
    ],
    jumbolink: {
      text: 'Jumbo Link',
      icon: 'chat',
      url: '#',
    },
    routelist: [
      {
        text: 'Operations',
        icon: 'description',
        routeId: 'Operations.List',
      },
      {
        text: 'Submissions',
        icon: 'filter_none_filled',
        routeId: 'Submissions.List',
      },
      {
        text: 'Sandbox',
        icon: 'fas fa-wrench',
        url: '/formly-sandbox',
      },
    ],
    entitysummary: {
      operationId: '12345',
      operationName: 'Operation Gallery',
      operationAlmatCardImageSmall: '12345',
    },
  };

  context = {};

  SnackMessageType = SnackMessageType;
  DialogType = DialogType;

  loading = false;

  niIcons = [
    'ni ni-printer',
    'ni ni-bottle',
    'ni ni-internet-explorer',
    'ni ni-library',
    'ni ni-incognito',
    'ni ni-memory-stick',
    'ni ni-card-reader',
    'ni ni-firefox',
    'ni ni-digg',
    'ni ni-google-wallet',
    'ni ni-visa-debit-card',
    'ni ni-steering-wheel',
    'ni ni-dollar-bill',
    'ni ni-motorbike',
    'ni ni-id',
    'ni ni-calculator',
    'ni ni-bitcoin',
    'ni ni-arrow-up',
    'ni ni-warning-sign',
    'ni ni-tablets',
    'ni ni-phone',
    'ni ni-m16',
    'ni ni-fire',
    'ni ni-pinterest',
    'ni ni-eye',
    'ni ni-steam',
    'ni ni-lightning',
    'ni ni-auction-hammer',
    'ni ni-discover-credit-card',
    'ni ni-amazon',
    'ni ni-question-block',
    'ni ni-bullets',
    'ni ni-dna',
    'ni ni-hand-bag',
    'ni ni-handgun',
    'ni ni-pill-box',
    'ni ni-processor-chip',
    'ni ni-anchor',
    'ni ni-youtube',
    'ni ni-key',
    'ni ni-wordpress',
    'ni ni-reddit',
    'ni ni-bow-and-arrow',
    'ni ni-wheelchair',
    'ni ni-sim-card',
    'ni ni-vimeo',
    'ni ni-car',
    'ni ni-stripe-card',
    'ni ni-google-chrome',
    'ni ni-mastercard',
    'ni ni-scissors',
    'ni ni-spoon',
    'ni ni-star',
    'ni ni-house',
    'ni ni-splash',
    'ni ni-tablet',
    'ni ni-hashtag',
    'ni ni-paypal-card',
    'ni ni-windows',
    'ni ni-knife',
    'ni ni-chip',
    'ni ni-paypal',
    'ni ni-heartbeat',
    'ni ni-tools',
    'ni ni-dribbble',
    'ni ni-credit-card',
    'ni ni-instagram',
    'ni ni-road',
    'ni ni-screwdriver',
    'ni ni-truck',
    'ni ni-wrench',
    'ni ni-foursquare',
    'ni ni-sandisk',
    'ni ni-handshake',
    'ni ni-thunder',
    'ni ni-target',
    'ni ni-bullseye',
    'ni ni-hard-drive-2',
    'ni ni-sword',
    'ni ni-pocket-knife',
    'ni ni-pill',
    'ni ni-pointer',
    'ni ni-office',
    'ni ni-skype',
    'ni ni-ambulance',
    'ni ni-hammer',
    'ni ni-footsteps',
    'ni ni-bluetooth',
    'ni ni-floppy',
    'ni ni-laptop',
    'ni ni-injection',
    'ni ni-bomb',
    'ni ni-flat-screwdriver',
    'ni ni-apple',
    'ni ni-tyre-tracks',
    'ni ni-paw-print',
    'ni ni-monitor',
    'ni ni-brain',
    'ni ni-boxes',
    'ni ni-cube',
    'ni ni-flame',
    'ni ni-weed',
    'ni ni-person',
    'ni ni-git',
    'ni ni-binoculars',
    'ni ni-opera',
    'ni ni-american-express-credit-card',
    'ni ni-bone',
    'ni ni-uzi',
    'ni ni-android',
    'ni ni-bike',
    'ni ni-magnifying-glass',
    'ni ni-question-mark',
    'ni ni-email',
    'ni ni-heart',
    'ni ni-sunglasses',
    'ni ni-dropbox',
    'ni ni-small-hammer',
    'ni ni-flask',
    'ni ni-padlock',
    'ni ni-fingerprint',
    'ni ni-twitter',
    'ni ni-bicycle',
    'ni ni-linux',
    'ni ni-water-drop',
    'ni ni-jet-plane',
    'ni ni-hard-drive',
    'ni ni-stethoscope',
    'ni ni-safari',
    'ni ni-needle',
    'ni ni-scales',
  ];

  constructor(private snackbar: MatSnackBar, private dialogService: DialogService, private state: StateService) {}

  openDialog(title: string, body: string, type: DialogType) {
    this.dialogService.openDialog(<DialogConfig>{
      title: title,
      bodyText: body,
      type,
    });
  }

  openSnack(type: SnackMessageType, message: string) {
    this.snackbar.openFromComponent(SnackMessageComponent, {
      duration: 5000,
      data: { type, message },
    });
  }

  showLoading() {
    this.state.setLoading(true);
    setTimeout(() => this.state.setLoading(false), 5000);
  }
}
