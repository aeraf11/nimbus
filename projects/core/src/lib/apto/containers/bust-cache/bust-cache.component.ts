import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormDataService } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ni-bust-cache',
  templateUrl: './bust-cache.component.html',
  styleUrls: ['./bust-cache.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BustCacheComponent implements OnInit {
  constructor(private service: FormDataService) {}

  ngOnInit(): void {
    this.service.bustCache().subscribe(() => {
      console.warn('cache busted');
      navigator.serviceWorker.getRegistrations().then(function (registrations) {
        for (const registration of registrations) {
          registration.unregister();
        }
      });
      window.history.back();
      setTimeout(() => window.location.reload(), 500);
    });
  }
}
