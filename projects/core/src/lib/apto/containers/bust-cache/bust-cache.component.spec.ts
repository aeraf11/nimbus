import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BustCacheComponent } from './bust-cache.component';
import { FormDataService } from '@nimbus/core/src/lib/core';
import { MockFormsDataService } from '../../../core/services/form-data-service/form-data.service.spec';

describe('BustCacheComponent', () => {
  let component: BustCacheComponent;
  let fixture: ComponentFixture<BustCacheComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BustCacheComponent],
      providers: [{ provide: FormDataService, useClass: MockFormsDataService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BustCacheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
