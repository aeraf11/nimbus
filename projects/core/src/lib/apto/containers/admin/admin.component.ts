import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FormDefinition } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ni-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdminComponent {
  definition: FormDefinition = {
    id: 'Admin',
    fields: [
      {
        fieldGroup: [
          {
            wrappers: ['card'],
            fieldGroup: [
              {
                type: 'uploader',
                props: {
                  label: 'Home Image Upload',
                  description:
                    'Upload items here and copy the file ids into the "pageBanner" section in the Home route in your config',
                  allowedExtensions: '.jpg,.png,.bmp,.gif',
                  formFieldMarginTop: '0',
                  showFileIdAfterUpload: true,
                  asyncSettings: {
                    saveUrl: 'api/v2/media/photo',
                    removeUrl: 'api/v2/media/photo/delete',
                  },
                },
              },
            ],
          },
        ],
      },
    ],
    options: {},
  };
}
