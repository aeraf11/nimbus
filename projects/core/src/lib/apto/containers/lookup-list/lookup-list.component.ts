import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { LookupNode, LookupService } from '@nimbus/core/src/lib/core';
import { fromEvent, Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, startWith } from 'rxjs/operators';

@Component({
  selector: 'ni-lookup-list',
  templateUrl: './lookup-list.component.html',
  styleUrls: ['./lookup-list.component.scss'],
})
export class LookupListComponent implements AfterViewInit {
  lookups: LookupNode[] | null = [];
  lookupType: string[] = [];
  @ViewChild('input') textInput!: ElementRef<HTMLInputElement>;
  searchEmpty = true;
  constructor(private service: LookupService) {
    this.service.getLookupsForList([]).subscribe((matches) => {
      if (matches) {
        matches.forEach((lookup) => {
          if (!this.lookupType.includes(lookup.item.lookupKey)) {
            this.lookupType.push(lookup.item.lookupKey);
          }
        });
        this.lookupType = this.lookupType.sort();
      }
    });
  }

  ngAfterViewInit(): void {
    const searchText$: Observable<string> = fromEvent(this.textInput.nativeElement, 'keyup').pipe(
      map((event) => (<HTMLInputElement>event.target).value),
      startWith(''),
      filter((search) => this.searchEmpty || !!search),
      debounceTime(200),
      distinctUntilChanged()
    );

    searchText$.subscribe((val) => {
      this.service.getLookupsForList(val ? [val] : []).subscribe((matches) => {
        this.lookups = matches;
      });
    });
  }

  onItemSelected(value: string) {
    this.service.getLookupsHierachy(value).subscribe((matches) => {
      this.lookups = matches;
    });
  }
}
