import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { StateService } from '@nimbus/core/src/lib/core';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { PageComponent } from '@nimbus/core/src/lib/core';
import { LookupListComponent } from './lookup-list.component';

describe('LookupListComponent', () => {
  let component: LookupListComponent;
  let fixture: ComponentFixture<LookupListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LookupListComponent, PageComponent],
      imports: [MatCardModule, MatFormFieldModule, MatSelectModule, MatInputModule, NoopAnimationsModule],
      providers: [{ provide: StateService, useClass: MockStateService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LookupListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
