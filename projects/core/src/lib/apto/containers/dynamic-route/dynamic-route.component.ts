import { Component, HostBinding, Inject, OnDestroy, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  AppInsightsService,
  CanComponentDeactivate,
  DynamicRoute,
  FormComponent,
  FormDefinition,
  FormsHelperService,
  StateService,
} from '@nimbus/core/src/lib/core';
import deepClone from 'deep.clone';
import { combineLatest, Observable, of, take } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { ENABLE_DYNAMIC_ROUTE_PAGE_COMPONENT } from '../../apto.module';
import { FormActionService } from './../../services/form-action.service';

@UntilDestroy()
@Component({
  selector: 'ni-dynamic-route',
  templateUrl: './dynamic-route.component.html',
  styleUrls: ['./dynamic-route.component.scss'],
})
export class DynamicRouteComponent implements OnDestroy, CanComponentDeactivate {
  dynamicRoute?: DynamicRoute;
  definition$: Observable<FormDefinition | undefined>;
  model$: Observable<any>;
  error$: Observable<string | null>;

  context: any = {};
  formSubmitted = false;
  @ViewChild('form') formComponent!: FormComponent;
  @HostBinding('class') hostclass = 'content-fit-item-block';

  constructor(
    private state: StateService,
    route: ActivatedRoute,
    private formHelper: FormsHelperService,
    private formActionService: FormActionService,
    private matDialog: MatDialog,
    private router: Router,
    private appInsights: AppInsightsService,
    @Inject(ENABLE_DYNAMIC_ROUTE_PAGE_COMPONENT) public enableDynamicRoutePageComponent: boolean
  ) {
    this.definition$ = state.selectCurrentDynamicForm();
    this.model$ = state.selectDynamicRouteCurrentModel();
    this.error$ = state.selectDynamicRouteCurrentError();

    const userProfileId$ = state.selectUser().pipe(map((user) => user?.userProfileId));
    const isOnline$ = combineLatest([state.selectConnectionOnline(), state.selectWorkOnline()]).pipe(
      map(([isConnectionOnline, isWorkOnline]) => {
        return !!(isConnectionOnline && isWorkOnline);
      })
    );

    // Reload model when these change
    combineLatest([state.selectCurrentDynamicRoute(), userProfileId$, isOnline$])
      .pipe(distinctUntilChanged(), take(1))
      .subscribe(([dynamicRoute, userProfileId, isOnline]: any[]) => {
        this.dynamicRoute = dynamicRoute;
        this.context = {
          userProfileId,
          isOnline,
        };

        route.snapshot.paramMap.keys.forEach((key) => (this.context[key] = route.snapshot.paramMap.get(key)));
        route.snapshot.queryParamMap.keys.forEach((key) => (this.context[key] = route.snapshot.queryParamMap.get(key)));

        if (dynamicRoute?.routeId && dynamicRoute?.readId && dynamicRoute?.modelId) {
          if (isOnline) {
            this.state.loadCurrentDynamicRouteModel(dynamicRoute?.routeId, this.context);
          } else {
            this.state.loadCurrentDynamicRouteModelOffline(dynamicRoute?.routeId, this.context);
          }
        } else {
          this.model$ = of({});
        }
      });

    // Don't reload model when these change
    state
      .selectCurrentBreakpoint()
      .pipe(untilDestroyed(this))
      .subscribe((breakpoint) => {
        this.context = { ...this.context, breakpoint };
        this.triggerFormContextChanged('breakpoint');
      });

    isOnline$.pipe(untilDestroyed(this)).subscribe((isOnline) => {
      this.context = { ...this.context, isOnline };
      this.triggerFormContextChanged('isOnline');
      this.appInsights.setTrackingStatus(isOnline);
    });

    if (this.context.backButtonUrl) {
      this.model$.pipe(untilDestroyed(this)).subscribe((model) => {
        if (model) {
          this.context.backButtonUrl = this.formHelper.parseTemplate(this.context.backButtonUrl, model, '');
        }
      });
    }
  }

  getHeader(template: string | undefined, model: any) {
    if (template && model) {
      return this.formHelper.parseTemplate(template, model, '');
    }
    return this.formHelper.clearTemplate(template || '');
  }

  handleFormAction(
    event: {
      model: any;
      action?: string;
      routeId?: string;
      onSuccess?: () => void;
      showErrorSnack?: boolean;
    },
    context?: any
  ) {
    this.formSubmitted = true;
    deepClone(event.model).then((modelClone) =>
      this.formActionService.handleFormAction(
        modelClone,
        event.action,
        event.routeId || this.dynamicRoute?.routeId,
        event.onSuccess,
        event.showErrorSnack,
        context
      )
    );
  }

  canDeactivate() {
    if (this.formComponent.form.dirty && !this.formSubmitted) {
      return confirm('Unsaved changes will be lost, are you sure?');
    }
    if (this.matDialog.openDialogs.length > 0) {
      return false;
    }
    return true;
  }

  ngOnDestroy() {
    this.state.clearCurrentDynamicRouteModel();
  }

  private triggerFormContextChanged(propertyName: string) {
    if (this.formComponent) {
      this.formComponent.options?.formState?.contextChanged?.next(propertyName ?? '');
    }
  }
}
