import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { PageComponent, SiteConfig, StateService } from '@nimbus/core/src/lib/core';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { ENABLE_DYNAMIC_ROUTE_PAGE_COMPONENT } from '../../apto.module';
import { DynamicRouteComponent } from './dynamic-route.component';

describe('DynamicRouteComponent', () => {
  let component: DynamicRouteComponent;
  let fixture: ComponentFixture<DynamicRouteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DynamicRouteComponent, PageComponent],
      imports: [RouterTestingModule, HttpClientTestingModule, MatDialogModule],
      providers: [
        { provide: StateService, useClass: MockStateService },
        { provide: ENABLE_DYNAMIC_ROUTE_PAGE_COMPONENT, useValue: true },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {},
          },
        },
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicRouteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
