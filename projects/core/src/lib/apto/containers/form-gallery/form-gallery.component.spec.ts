import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatMenuModule } from '@angular/material/menu';
import { RouterTestingModule } from '@angular/router/testing';
import { BlobService } from '@nimbus/core/src/lib/core';
import { MockBlobService } from './../../../core/services/blob.service.spec';
import { FormGalleryComponent } from './form-gallery.component';
import { PageComponent } from '@nimbus/core/src/lib/core';

describe('FormGalleryComponent', () => {
  let component: FormGalleryComponent;
  let fixture: ComponentFixture<FormGalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormGalleryComponent, PageComponent],
      imports: [RouterTestingModule, HttpClientTestingModule, MatMenuModule],
      providers: [{ provide: BlobService, useClass: MockBlobService }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormGalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
