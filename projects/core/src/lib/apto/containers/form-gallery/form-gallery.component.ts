import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BlobService, FormDefinition, MenuItem } from '@nimbus/core/src/lib/core';
import { take } from 'rxjs/operators';
import {
  ActionBarDoc,
  ButtonListDoc,
  BinarySizeDoc,
  CardListWrapperDoc,
  CardWrapperDoc,
  ChartBarDoc,
  ChartLineDoc,
  ChartPieDoc,
  CheckboxDoc,
  ChipDoc,
  CircularGaugeDoc,
  ContextDoc,
  DataCardsDoc,
  DataDownloadablesDoc,
  DataTableDoc,
  DateDoc,
  DateListDoc,
  DateRangeDoc,
  DateTimeDoc,
  DateTimeViewDoc,
  DisplayGroupDoc,
  DividerDoc,
  EntityAddDoc,
  EntitySummaryDoc,
  FieldViewDoc,
  FileManagerDoc,
  FormActionsDoc,
  FormFieldWrapperDoc,
  GridWrapperDoc,
  IconTextInlineDoc,
  ImageChoiceDoc,
  InlineCalendarDoc,
  InputDoc,
  JobTimeDoc,
  JumboLinkDoc,
  LatLongDoc,
  LayoutChoiceDoc,
  ListFilterWrapperDoc,
  LookupDoc,
  MediaUploadDoc,
  MetaDataListDoc,
  MultiCheckboxDoc,
  MultiChoiceDoc,
  NavButtonDoc,
  PanelWrapperTypeDoc,
  PersonDoc,
  PersonListDoc,
  ProgressDoc,
  RadioDoc,
  ReadonlyDoc,
  RelatesToDoc,
  RepeatDoc,
  ReportButtonDoc,
  RichFieldsDoc,
  RichTextDoc,
  RouteListDoc,
  ScrollWrapperDoc,
  SectionEditWrapperDoc,
  SelectDoc,
  SignatureDoc,
  SlideToggleDoc,
  StepperDoc,
  TableDoc,
  TextAreaDoc,
  TextAreaReadonlyDoc,
  TextBarcodeDoc,
  TextDoc,
  TextPanelDoc,
  TimeDoc,
  TypeAheadDoc,
  UploaderDoc,
  UploaderOfflineDoc,
  VideoDoc,
  YesNoDoc,
} from '../../components';
import { DocEditorDoc } from '@nimbus/core/src/lib/apto';
import { deepClone } from 'deep.clone';

@Component({
  selector: 'ni-form-gallery',
  templateUrl: './form-gallery.component.html',
  styleUrls: ['./form-gallery.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class FormGalleryComponent implements OnInit {
  definition: FormDefinition = {
    id: 'Gallery',
    fields: [
      {
        fieldGroup: [
          {
            template: '<h2 class="menu-group">Edit Components</h2>',
            props: {
              menuGroup: {
                name: 'Edit Components',
                items: [
                  { name: 'Binary Size', scrollId: 'binarysize' },
                  { name: 'Checkbox', scrollId: 'checkbox' },
                  { name: 'Chip', scrollId: 'chip' },
                  { name: 'Context', scrollId: 'context' },
                  {
                    name: 'Data Lists',
                    items: [
                      { name: 'Data Table - Actions', scrollId: 'dt-actions' },
                      { name: 'Data Table - No Select', scrollId: 'dt-none' },
                      { name: 'Data Table - Single Select', scrollId: 'dt-single' },
                      { name: 'Data Table - Multiple Select', scrollId: 'dt-multiple' },
                      { name: 'Data Table - Responsive Tabs', scrollId: 'dt-tabs' },
                      { name: 'Data Cards - Crime Scenes', scrollId: 'data-cards' },
                      { name: 'Data Cards - Messages', scrollId: 'data-cards-messages' },
                      { name: 'Data Downloadables', scrollId: 'data-downloadables' },
                    ],
                  },
                  {
                    name: 'Date',
                    items: [
                      { name: 'Date', scrollId: 'date' },
                      { name: 'Date Required', scrollId: 'date-required' },
                      { name: 'Date Range', scrollId: 'date-range' },
                      { name: 'Date Quick Links', scrollId: 'date-quick' },
                    ],
                  },
                  { name: 'Date Time', scrollId: 'date-time' },
                  { name: 'Date Time View', scrollId: 'date-time-view' },
                  { name: 'Document Editor', scrollId: 'doc-editor' },
                  { name: 'Document Viewer', scrollId: 'doc-viewer' },
                  { name: 'Icon Text Inline', scrollId: 'icon-text-inline' },
                  { name: 'Identification', scrollId: 'identification' },
                  { name: 'Image Choice', scrollId: 'image-choice' },
                  { name: 'Input', scrollId: 'input' },
                  { name: 'Job Time', scrollId: 'jobtime' },
                  { name: 'Layout Choice', scrollId: 'layout-choice' },
                  {
                    name: 'Lookup',
                    items: [
                      { name: 'No Leaf', scrollId: 'lookup-no-leaf' },
                      { name: 'Leaf', scrollId: 'lookup-leaf' },
                      { name: 'Submission Queue', scrollId: 'lookup-submission-queue' },
                      { name: 'Filtered', scrollId: 'lookup-filtered' },
                      { name: 'Top Level', scrollId: 'lookup-top-level' },
                      { name: 'Child Items', scrollId: 'lookup-child' },
                    ],
                  },
                  { name: 'Media Upload', scrollId: 'media-upload' },
                  { name: 'Meta Data Panel (Operation)', scrollId: 'meta-data-panel' },
                  { name: 'Multi-Checkbox', scrollId: 'multi-checkbox' },
                  { name: 'Multi Choice', scrollId: 'multi-choice' },
                  { name: 'Radio', scrollId: 'radio' },
                  { name: 'Read Only', scrollId: 'read-only' },
                  { name: 'Relates To', scrollId: 'relates-to' },
                  { name: 'Rich Text', scrollId: 'rich-text' },
                  { name: 'Rich Text Viewer (read only)', scrollId: 'rich-text-viewer' },
                  { name: 'Select', scrollId: 'select' },
                  { name: 'Select - Grouped', scrollId: 'select-grouped' },
                  { name: 'Select - Disabled', scrollId: 'select-disabled' },
                  { name: 'Select (Index)', scrollId: 'select-index' },
                  { name: 'Select - Crime Scene Exhibit', scrollId: 'select-list-source' },
                  { name: 'Signature', scrollId: 'signature' },
                  { name: 'Slide Toggle', scrollId: 'slide-toggle' },
                  { name: 'Table', scrollId: 'table' },
                  { name: 'Text Area', scrollId: 'text-area' },
                  { name: 'Text Area (Read Only)', scrollId: 'text-area-read-only' },
                  { name: 'Text Panel', scrollId: 'text-panel' },
                  { name: 'Text With Barcode', scrollId: 'text-barcode' },
                  { name: 'Time', scrollId: 'time' },
                  {
                    name: 'Type Ahead',
                    items: [
                      { name: 'Operation Type Ahead', scrollId: 'type-ahead-operation' },
                      { name: 'Submission Type Ahead - Autofill', scrollId: 'type-ahead-submission' },
                      { name: 'Type Ahead (Constant Filter)', scrollId: 'type-ahead-constant-filter' },
                      { name: 'Type Ahead Exhibit', scrollId: 'type-ahead-exhibit' },
                      { name: 'Type Ahead Users', scrollId: 'type-ahead-users' },
                      { name: 'Type Ahead (old configration)', scrollId: 'type-ahead-original-config' },
                      { name: 'Type Ahead LDAP Person Search', scrollId: 'type-ahead-person-ldap' },
                      { name: 'Type Ahead Person Search', scrollId: 'type-ahead-person-list' },
                      { name: 'Type Ahead My Team', scrollId: 'type-ahead-my-team' },
                      { name: 'Type Ahead All Actions', scrollId: 'type-ahead-all-actions' },
                      { name: 'Type Ahead My Actions', scrollId: 'type-ahead-my-actions' },
                      { name: 'Type Ahead EntityCalendarItems', scrollId: 'type-ahead-task' },
                      { name: 'Type Ahead Notifications', scrollId: 'type-ahead-notification' },
                    ],
                  },
                  { name: 'Unidentified Subject', scrollId: 'unidentified-subject' },
                  { name: 'Uploader', scrollId: 'uploader' },
                  { name: 'Uploader Offline', scrollId: 'uploader-offline' },
                  { name: 'Video', scrollId: 'video' },
                  { name: 'Yes No', scrollId: 'yesno' },
                ],
              },
            },
          },

          {
            props: {
              title: 'Binary Size',
              doc: BinarySizeDoc,
              id: 'binarysize',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'binarysize',
                type: 'binarysize',
                props: {
                  label: 'Binary Size',
                  required: true,
                  version: 1,
                  placeholder: '',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Checkbox',
              doc: CheckboxDoc,
              id: 'checkbox',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'checkbox',
                type: 'checkbox',
                props: {
                  label: 'Checkbox',
                  required: true,
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                  pattern: 'true',
                },
                validation: {
                  messages: {
                    pattern: 'This is a validation error',
                  },
                },
              },
            ],
          },

          {
            props: {
              title: 'Chip',
              doc: ChipDoc,
              id: 'chip',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'chip',
                type: 'chip',
                props: {
                  label: 'Chip',
                  enableRemove: true,
                  colour: 'primary',
                  valueProp: 'text',
                  removeButtonAriaLabel: 'Remove Chip',
                  listAriaLabel: 'List Items',
                },
              },
            ],
          },

          {
            props: {
              title: 'Context',
              doc: ContextDoc,
              id: 'context',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'context',
                type: 'context',
                props: {
                  expressions: { contextComponentExpression: '`This is an evaluated expression: ${model.readonly}`' },
                },
              },
            ],
          },

          {
            props: {
              title: 'Date',
            },
            wrappers: ['formgallerygroup'],
            fieldGroup: [
              {
                wrappers: ['formgallery'],
                props: {
                  doc: DateDoc,
                  grouped: true,
                  id: 'date',
                },
                fieldGroup: [
                  {
                    key: 'date',
                    type: 'date',
                    props: {
                      label: 'Date',
                      required: false,
                      placeholder: 'This is placeholder text',
                      description:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Date Required',
                  doc: DateDoc,
                  grouped: true,
                  id: 'date-required',
                },
                fieldGroup: [
                  {
                    key: 'dateRequired',
                    type: 'date',
                    props: {
                      label: 'Date Required',
                      required: true,
                      placeholder: 'This is placeholder text',
                      description:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Date Range',
                  doc: DateRangeDoc,
                  grouped: true,
                  id: 'date-range',
                },
                fieldGroup: [
                  {
                    key: 'daterange',
                    type: 'daterange',
                    props: {
                      label: 'Date Range',
                      required: true,
                      placeholder: 'This is placeholder text',
                      description:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Date Quick Links',
                  doc: DateRangeDoc,
                  grouped: true,
                  id: 'date-quick',
                },
                fieldGroup: [
                  {
                    key: 'daterange',
                    type: 'daterange',
                    props: {
                      showQuickLinks: true,
                      label: 'Date Range',
                      required: true,

                      placeholder: 'This is placeholder text',
                      description:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                    },
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Date Time',
              doc: DateTimeDoc,
              id: 'date-time',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'datetime',
                type: 'datetime',
                props: {
                  label: 'Date Time',
                  required: true,
                  showTime: true,
                  showTimeZone: true,
                  placeholder: 'This is placeholder text',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Date Time View',
              doc: DateTimeViewDoc,
              id: 'date-time-view',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'datetimeview',
                type: 'datetimeview',
                props: {
                  label: 'Date Time View',
                  includeTimeZone: true,
                  includeSeconds: false,
                },
              },
            ],
          },

          {
            props: {
              title: 'Data Lists',
            },
            wrappers: ['formgallerygroup'],
            fieldGroup: [
              {
                props: {
                  doc: DataTableDoc,
                  grouped: true,
                  id: 'dt-actions',
                  title: 'Data Table - Actions',
                },
                wrappers: ['formgallery'],
                fieldGroup: [
                  {
                    key: 'submissionActions',
                    type: 'submissionsdatatable',
                    props: {
                      actions: [
                        {
                          key: 'view',
                          name: 'View',
                          url: 'submissions/*{submissionTypeLookupId}/view/${submissionId}',
                          fallback: '5c75f686-fc8d-4588-ab62-8d250a03011d',
                          params: {
                            backButtonUrl: '/submissions/list',
                          },
                        },
                        {
                          key: 'edit',
                          name: 'Edit',
                          url: 'submissions/*{submissionTypeLookupId}/add?submissionId=${submissionId}',
                          fallback: '5c75f686-fc8d-4588-ab62-8d250a03011d',
                          visibility: '${canEdit}',
                        },
                      ],
                    },
                  },
                ],
              },
              {
                props: {
                  doc: DataTableDoc,
                  grouped: true,
                  id: 'dt-none',
                  title: 'Data Table - No Select',
                },
                wrappers: ['formgallery'],
                fieldGroup: [
                  {
                    key: 'submission',
                    type: 'datatable',
                    props: {
                      listName: 'Submissions',
                      selectMode: 'none',
                      sortColumn: 'reference',
                      columnDefinitions: [
                        {
                          id: 'reference',
                          value: 'Reference',
                          isSortable: true,
                          filterParams: {
                            formlyType: 'input',
                          },
                        },
                        {
                          id: 'operationName',
                          value: 'Operation',
                          isSortable: true,
                          filterParams: {
                            formlyType: 'input',
                          },
                        },
                        {
                          id: 'creationDate',
                          value: 'Date Created',
                          dataType: 1,
                          isSortable: true,
                          filterParams: {
                            formlyType: 'daterange',
                          },
                        },
                        {
                          id: 'submittingOfficer',
                          value: 'Submitting Officer',
                          dataType: 7,
                          isSortable: true,
                          filterParams: {
                            formlyType: 'multichoice',
                            searchValueColumn: 'fullName',
                            props: {
                              listName: 'PersonGrouped',
                              valueProp: 'personId',
                              labelProp: 'fullName',
                              sortColumn: 'fullName',
                              selectColumns: ['personId', 'fullName'],
                            },
                          },
                        },
                        {
                          id: 'statusName',
                          value: 'Status',
                          dataType: 8,
                          isSortable: true,
                          filterParams: {
                            formlyType: 'multichoice',
                            props: {
                              lookupKey: 'SubmissionStatus',
                            },
                          },
                          computedValue: {
                            value: '${statusName}',
                            cssClass: '${cssClass}',
                          },
                        },
                        {
                          id: 'submissionTypeLookupId',
                          value: 'Submission Type',
                          dataType: 4,
                          isSortable: true,
                          isfilterable: true,
                          filterParams: {
                            filterAlias: 'submissionType',
                            formlyType: 'multichoice',
                            props: {
                              lookupKey: 'SubmissionType',
                            },
                          },
                        },
                      ],
                      idColumn: 'submissionId',
                      filterColumns: [],
                      showPaging: true,
                      showQuickSearch: true,
                      filterTitle: 'Filter Submissions',
                    },
                  },
                ],
              },
              {
                props: {
                  doc: DataTableDoc,
                  grouped: true,
                  id: 'dt-single',
                  title: 'Data Table - Single Select',
                },
                wrappers: ['formgallery'],
                fieldGroup: [
                  {
                    key: 'datatableSingle',
                    type: 'datatable',
                    props: {
                      listName: 'Operations',
                      sortColumn: 'name',
                      columnDefinitions: [
                        {
                          id: 'operationName',
                          value: 'Operation Name',
                        },
                        {
                          id: 'operationAlias',
                          value: 'Operation Alias',
                        },
                      ],
                      selectMode: 'single',
                      idColumn: 'operationId',
                      ariaLabelField: 'operationName',
                    },
                  },
                ],
              },

              {
                props: {
                  doc: DataTableDoc,
                  grouped: true,
                  id: 'dt-multiple',
                  title: 'Data Table - Multiple Select',
                },
                wrappers: ['formgallery'],
                fieldGroup: [
                  {
                    key: 'datatableMulti',
                    type: 'datatable',
                    props: {
                      selectMode: 'multi',
                      listName: 'Operations',
                      sortColumn: 'name',
                      columnDefinitions: [
                        {
                          id: 'operationName',
                          value: 'Operation Name',
                        },
                        {
                          id: 'operationAlias',
                          value: 'Operation Alias',
                        },
                      ],
                      idColumn: 'operationId',
                      ariaLabelField: 'operationName',
                    },
                  },
                ],
              },

              {
                props: {
                  doc: DataTableDoc,
                  grouped: true,
                  id: 'dt-tabs',
                  title: 'Data Table - Responsive Tabs',
                },
                wrappers: ['formgallery'],
                fieldGroup: [
                  {
                    key: 'dataTableTabs',
                    type: 'datatable',
                    props: {
                      savedOptionsId: 'submissions',
                      listName: 'Submissions',
                      selectMode: 'none',
                      sortColumn: 'reference',
                      columnDefinitions: [
                        {
                          id: 'reference',
                          value: 'Reference',
                          isSortable: true,
                          filterParams: {
                            formlyType: 'input',
                          },
                          tabs: 'Reference, Details',
                        },
                        {
                          id: 'operationName',
                          value: 'Operation',
                          isSortable: true,
                          filterParams: {
                            formlyType: 'input',
                          },
                          tabs: 'Details, Details',
                        },
                        {
                          id: 'creationDate',
                          value: 'Date Created',
                          dataType: 'date',
                          isSortable: true,
                          filterParams: {
                            formlyType: 'daterange',
                            props: {
                              label: 'Date Created',
                              showQuickLinks: true,
                            },
                          },
                          tabs: 'Details, Details',
                        },
                        {
                          id: 'submittingOfficer',
                          value: 'Submitting Officer',
                          dataType: 'profile',
                          isSortable: true,
                          filterParams: {
                            formlyType: 'multichoice',
                            searchValueColumn: 'fullName',
                            props: {
                              listName: 'Users',
                              valueProp: 'personId',
                              labelProp: 'fullName',
                              sortColumn: 'fullName',
                              selectColumns: ['personId', 'fullName'],
                            },
                          },
                          computedValue: {
                            name: '${submittingOfficer}',
                            blobId: '${submittingOfficerBlobId}',
                          },
                          tabs: 'Submitter, Details',
                        },
                        {
                          id: 'submittingOfficerBlobId',
                          isHidden: true,
                        },
                        {
                          id: 'statusName',
                          value: 'Status',
                          dataType: 'status',
                          isSortable: true,
                          minWidth: '200px',
                          filterParams: {
                            formlyType: 'multichoice',
                            props: {
                              lookupKey: 'SubmissionStatus',
                            },
                          },
                          computedValue: {
                            value: '${statusName}',
                            cssClass: '${cssClass}',
                          },
                          tabs: 'Status/Type, Status/Type',
                        },
                        {
                          id: 'canEdit',
                          isHidden: true,
                        },
                        {
                          id: 'submissionTypeLookupId',
                          value: 'Submission Type',
                          dataType: 'lookup',
                          isSortable: true,
                          isfilterable: true,
                          filterParams: {
                            filterAlias: 'submissionType',
                            formlyType: 'multichoice',
                            props: {
                              lookupKey: 'SubmissionType',
                            },
                          },
                          tabs: 'Status/Type, Status/Type',
                        },
                        {
                          id: 'unreadMessages',
                          dataType: 'messages',
                          tabs: 'Details, Details',
                        },
                      ],
                      idColumn: 'submissionId',
                      filterColumns: [],
                      showPaging: true,
                      showQuickSearch: true,
                      filterTitle: 'Filter Submissions',
                      actionsTabs: 'Reference, Details',
                      actions: [
                        {
                          key: 'view',
                          name: 'View',
                          url: 'submissions/*{submissionTypeLookupId}/view/${submissionId}',
                          fallback: '5c75f686-fc8d-4588-ab62-8d250a03011d',
                          params: {
                            backButtonUrl: '/submissions/list',
                          },
                        },
                        {
                          key: 'edit',
                          name: 'Edit',
                          url: 'submissions/*{submissionTypeLookupId}/edit?submissionId=${submissionId}',
                          fallback: '5c75f686-fc8d-4588-ab62-8d250a03011d',
                          visibility: '${canEdit}',
                        },
                      ],
                    },
                  },
                ],
              },

              {
                props: {
                  doc: DataCardsDoc,
                  grouped: true,
                  id: 'data-cards',
                  title: 'Data Cards Crime Scenes',
                },
                wrappers: ['formgallery'],
                fieldGroup: [
                  {
                    key: 'datacards',
                    type: 'datacards',
                    props: {
                      selectMode: 'multiple',
                      listName: 'CrimeScenes',
                      sortColumn: 'reference',
                      noResultsText: 'No crime scenes found',
                      title: 'Crime Scenes',
                      icon: '',
                      columnDefinitions: [
                        {
                          id: 'reference',
                        },
                        {
                          id: 'crimeSceneId',
                        },
                        {
                          id: 'name',
                        },
                        {
                          id: 'crimeSceneTypeLookup',
                        },
                        {
                          id: 'thumbnail',
                        },
                        {
                          id: 'entityLocationId',
                        },
                      ],
                      filterColumns: [
                        {
                          column: 'operationId',
                          stringFilter: 'BF772502-95BF-49DD-9BA5-A43A82835077',
                          constantFilter: true,
                        },
                      ],
                      idColumn: 'crimeSceneId',
                      itemTemplate: {
                        type: 'crimescene',
                      },
                      itemsUi: {
                        columnCounts: 3,
                        gap: '1rem',
                      },
                    },
                  },
                ],
              },
              {
                props: {
                  doc: DataCardsDoc,
                  grouped: true,
                  id: 'data-cards-messages',
                  title: 'Data Cards Messages',
                },
                wrappers: ['formgallery'],
                fieldGroup: [
                  {
                    key: 'datatableSingle',
                    type: 'messages',
                    props: {
                      maxHeight: '15rem',
                      fullWidthContent: 1,
                      allowAddMessage: true,
                    },
                    expressions: {
                      'props.filterColumns': "[{column: '', stringFilter: model.selectedOperationId, entityTypeId: 2}]",
                    },
                  },
                ],
              },
              {
                props: {
                  doc: DataCardsDoc,
                  grouped: true,
                  id: 'data-cards-summary',
                  title: 'Data Cards Summary',
                },
                wrappers: ['formgallery'],
                fieldGroup: [
                  {
                    key: 'datatableSingle',
                    type: 'datacards',
                    props: {
                      listName: 'EntityCalendarItem',
                      idColumn: 'entityCalendarItemId',
                      sortColumn: 'requiredCompletionDate',
                      sortDirection: 'asc',
                      pageSize: 10,
                      columnDefinitions: [
                        {
                          id: 'entityCalendarItemId',
                        },
                        {
                          id: 'requiredCompletionDate',
                        },
                        {
                          id: 'description',
                        },
                        {
                          id: 'actionLinked',
                        },
                        {
                          id: 'overdue',
                        },
                      ],
                      showPaging: false,
                      itemTemplate: {
                        wrappers: ['card'],
                        templateOptions: {
                          cardEnableHoverClass: true,
                        },
                        type: 'tasksummary',
                      },
                      itemsUi: {
                        columnCounts: [1],
                        gap: '0.5rem',
                      },
                      templatedProperties: {
                        metaList: {
                          targetCount: 10,
                          metaFilters: [
                            {
                              filterColumns: [
                                {
                                  column: 'requiredCompletionDate',
                                  constantFilter: true,
                                  stringFilter: '={formState.helper.dateTime.getOverdueFilter()}',
                                  isJsonFilter: true,
                                },
                                {
                                  column: '',
                                  constantFilter: true,
                                  stringFilter: '${context.userProfileId}',
                                  entityTypeId: 13,
                                },
                              ],
                            },
                            {
                              filterColumns: [
                                {
                                  column: 'requiredCompletionDate',
                                  constantFilter: true,
                                  stringFilter: '={formState.helper.dateTime.getUpcomingFilter()}',
                                  isJsonFilter: true,
                                },
                                {
                                  column: '',
                                  constantFilter: true,
                                  stringFilter: '${context.userProfileId}',
                                  entityTypeId: 13,
                                },
                              ],
                            },
                          ],
                        },
                      },
                    },
                  },
                ],
              },
            ],
          },

          {
            props: {
              doc: DataDownloadablesDoc,
              grouped: true,
              id: 'data-downloadables',
              title: 'Data Downloadables',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'datadownloadables',
                type: 'datadownloadables',
                props: {
                  selectMode: 'multi',
                  listName: 'Attachments',
                  sortColumn: 'creationDate',
                  idColumn: 'attachmentId',
                  sortDirection: 'desc',
                  maxHeight: '20rem',
                  pageSize: -1,
                  columnDefinitions: [
                    {
                      id: 'attachmentId',
                    },
                    {
                      id: 'fileName',
                    },
                    {
                      id: 'blobId',
                    },
                    {
                      id: 'creationDate',
                    },
                    {
                      id: 'updatedDate',
                    },
                    {
                      id: 'description',
                    },
                    {
                      id: 'createdByFullName',
                    },
                    {
                      id: 'createdByInitials',
                    },
                    {
                      id: 'createdByBlobId',
                    },
                    {
                      id: 'entityReference',
                    },
                  ],
                  ariaLabelField: 'fileName',
                },
              },
            ],
          },

          {
            props: {
              title: 'Document Editor',
              doc: DocEditorDoc,
              id: 'doc-editor',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'docEditor',
                type: 'doceditor',
                props: {},
              },
            ],
          },

          {
            props: {
              title: 'Document Viewer',
              doc: DocEditorDoc,
              id: 'doc-viewer',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'docViewer',
                type: 'docviewer',
                props: {},
              },
            ],
          },

          {
            props: {
              title: 'Icon Text Inline',
              doc: IconTextInlineDoc,
              id: 'icon-text-inline',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'icontextinline',
                type: 'icontextinline',
                props: {
                  text: 'Warning',
                  iconSize: '2rem',
                  textSize: '2rem',
                  colorClass: 'icon-text-warning',
                  icon: 'warning',
                },
              },
            ],
          },

          {
            props: {
              title: 'Identification',
              id: 'identification',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'identification',
                type: 'datacards',
                props: {
                  selectMode: 'single',
                  listName: 'MyIdents',
                  sortColumn: 'reference',
                  noResultsText: 'No idents found',
                  title: 'Identifications',
                  icon: '',
                  columnDefinitions: [
                    {
                      id: 'reference',
                    },
                    {
                      id: 'nameOfIndividual',
                    },
                    {
                      id: 'defaultPhotoThumbnailBlobId',
                    },
                    {
                      id: 'defaultEntityMediaBlobId',
                    },
                    {
                      id: 'hasAttachments',
                    },
                    {
                      id: 'actionRequired',
                    },
                  ],
                  idColumn: 'identId',
                  itemTemplate: {
                    type: 'identification',
                  },
                  itemsUi: {
                    columnCounts: 3,
                    gap: '1rem',
                  },
                  label: '',
                },
              },
            ],
          },

          {
            props: {
              title: 'Image Choice',
              doc: ImageChoiceDoc,
              id: 'image-choice',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'imagechoice',
                type: 'imagechoice',
                props: {
                  label: 'Image Choice',
                  required: true,
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Input (AKA String, Text)',
              doc: InputDoc,
              id: 'input',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'input',
                type: 'input',
                props: {
                  label: 'Input',
                  required: true,
                  placeholder: 'This is some placeholder text',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Job Time',
              doc: JobTimeDoc,
              id: 'jobtime',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'jobtime',
                type: 'jobtime',
                props: {
                  jobId: 'SyncQueuedAttachments',
                  ariaLabel: 'Time Test Label',
                },
              },
            ],
          },

          {
            props: {
              title: 'Layout Choice',
              doc: LayoutChoiceDoc,
              id: 'layout-choice',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'layoutchoice',
                type: 'layoutchoice',
                props: {
                  options: [
                    {
                      id: 'column',
                      icon: 'fas fa-pause',
                    },
                    {
                      id: 'grid',
                      icon: 'fas fa-grid-2',
                    },
                  ],
                },
              },
            ],
          },

          {
            wrappers: ['formgallerygroup'],
            props: {
              title: 'Lookup',
            },
            fieldGroup: [
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Lookup No Leaf - Person Lookup ',
                  grouped: true,
                  doc: LookupDoc,
                  id: 'lookup-no-leaf',
                },
                fieldGroup: [
                  {
                    key: 'lookupNoLeaf',
                    type: 'lookup',
                    props: {
                      label: 'Lookup (Leaf selection off)',
                      required: true,
                      lookupKey: 'PersonTypeGroup',
                      leafSelection: false,
                      description:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                      ariaLabel: 'Lookup person input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Lookup Leaf',
                  grouped: true,
                  doc: LookupDoc,
                  id: 'lookup-leaf',
                },
                fieldGroup: [
                  {
                    key: 'lookupLeaf',
                    type: 'lookup',
                    props: {
                      label: 'Lookup (Leaf selection on)',
                      required: true,
                      lookupKey: 'CMOCrimeCategoryOffense',
                      leafSelection: true,
                      description:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                      ariaLabel: 'Lookup input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Lookup Submission Queue',
                  grouped: true,
                  doc: LookupDoc,
                  id: 'lookup-submission-queue',
                },
                fieldGroup: [
                  {
                    key: 'lookupsubmissionqueue',
                    type: 'lookup',
                    props: {
                      label: 'Lookup (Submission Queue)',
                      required: true,
                      lookupKey: 'SubmissionQueue',
                      leafSelection: false,
                      description:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                      ariaLabel: 'Lookup submission queue input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Lookup Examination Types - Filtered',
                  grouped: true,
                  doc: LookupDoc,
                  id: 'lookup-filtered',
                },
                fieldGroup: [
                  {
                    key: 'examinationtypefiltered',
                    type: 'lookup',
                    props: {
                      lookupKey: 'ExaminationType',
                      label: 'Examination Type',
                      lookupFilters: ['93DC86D4-580D-4FA8-A4B4-414C0E75C8B9', '7DDF043E-1A6D-4045-8A5A-B0AEB5E16C05'],
                      description: 'Filtered to Fingerprint & Footprint by GUID (e.g. Parent with children).',
                      ariaLabel: 'Lookup exhibit types filtered input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Lookup Top Level',
                  grouped: true,
                  doc: LookupDoc,
                  id: 'lookup-top-level',
                },
                fieldGroup: [
                  {
                    key: 'lookuptoplevel',
                    type: 'lookup',
                    props: {
                      lookupKey: 'ExaminationType',
                      label: 'Top Level Only',
                      topLevelOnly: true,
                      description:
                        'ExaminationTypes have children but these are just the top level items within that lookup',
                      ariaLabel: 'Lookup top level only input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Lookup Child Items',
                  grouped: true,
                  doc: LookupDoc,
                  id: 'lookup-child',
                },
                fieldGroup: [
                  {
                    key: 'lookupgrandchild',
                    type: 'lookup',
                    props: {
                      lookupKey: 'ExaminationType',
                      label: 'Look up children',
                      parentLookupId: '9bd9feaa-016c-4836-b93c-682019d6e251',
                      ariaLabel: 'Lookup child items input field',
                      leafSelection: true,
                      addDefaultOption: true,
                      minDialogWidth: '15%',
                      maxDialogWidth: '1000px',
                      enableFieldLabel: false,
                    },
                    wrappers: ['formfield'],
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Media Upload',
              doc: MediaUploadDoc,
              id: 'media-upload',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'media',
                type: 'uploader',
                props: {
                  label: 'Media Upload',
                  required: true,
                  storageType: 1,
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Meta Data Panel',
              doc: MetaDataListDoc,
              id: 'meta-data-panel',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'metadataPanel',
                wrappers: ['card'],
                props: {
                  cardLabel: 'Operation Meta Data',
                  cardContentOverflowVisible: true,
                  gridArea: 'metadata',
                },
                fieldGroup: [
                  {
                    key: 'metadata',
                    type: 'metadatalist',
                    props: {
                      readonly: true,
                      entityType: 1,
                    },
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Multi-Checkbox',
              doc: MultiCheckboxDoc,
              id: 'multi-checkbox',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'multicheckbox',
                type: 'multicheckbox',
                props: {
                  label: 'Multi-checkbox',
                  required: true,
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
                expressions: {
                  'props.options': 'formState.selectOptionsData.colours',
                },
              },
            ],
          },

          {
            props: {
              title: 'Multi Choice',
              doc: MultiChoiceDoc,
              id: 'multi-choice',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'multichoice',
                type: 'multichoice',
                props: {
                  label: 'Multi Choice',
                  required: true,
                  lookupKey: 'PersonTypeGroup',
                  addCustom: true,
                  enableTree: 'auto',
                  leafSelection: false,
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                  ariaLabel: 'Multi choice input field',
                },
              },
            ],
          },
          {
            props: {
              title: 'Nav Button',
              doc: NavButtonDoc,
              id: 'nav-button',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'navbutton',
                type: 'navbutton',
                props: {
                  label: 'Nav Button',
                  ariaLabel: 'Nav button input field',
                  text: 'Navigate',
                  disabled: false,
                  route: '/home',
                  param: 'test',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },
          {
            props: {
              title: 'Radio',
              doc: RadioDoc,
              id: 'radio',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'radio',
                type: 'radio',
                props: {
                  label: 'Radio',
                  required: true,
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                  valueProp: 'id',
                  labelProp: 'name',
                },
                expressions: {
                  'props.options': 'formState.selectOptionsData.teams',
                },
              },
            ],
          },

          {
            props: {
              title: 'Read Only',
              doc: ReadonlyDoc,
              id: 'read-only',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'readonly',
                type: 'readonly',
                props: {
                  label: 'Read Only',
                  placeholder: 'This is some placeholder text',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Relates To',
              doc: RelatesToDoc,
              id: 'relates-to',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'relatesto',
                type: 'relatesto',
                props: {
                  label: 'Relates To',
                  required: true,
                },
              },
            ],
          },

          {
            props: {
              title: 'Rich Text',
              doc: RichTextDoc,
              id: 'rich-text',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'richTextEditor',
                type: 'richtext',
                props: {
                  label: 'Rich Text Editor (SyncFusion)',
                  required: true,
                },
              },
            ],
          },

          {
            props: {
              title: 'Rich Text Viewer',
              doc: RichTextDoc,
              id: 'rich-text-viewer',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'richTextViewer',
                type: 'richtextviewer',
                props: {
                  label: 'Rich Text Viewer (Readonly)',
                },
              },
            ],
          },

          {
            props: {
              title: 'Select (AKA Enum)',
              doc: SelectDoc,
              id: 'select',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'select',
                type: 'select',
                props: {
                  label: 'Select',
                  required: true,
                  version: 1,
                  placeholder: 'This is some placeholder text',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
                expressions: {
                  'props.options': 'formState.selectOptionsData.teamsLabelValue',
                },
              },
            ],
          },

          {
            props: {
              title: 'Select - Grouped',
              doc: SelectDoc,
              id: 'select-grouped',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'selectGrouped',
                type: 'select',
                props: {
                  label: 'Select With Grouped Options',
                  required: true,
                  groupProp: 'sportId',
                  placeholder: 'This is some placeholder text',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
                expressions: {
                  'props.options': 'formState.selectOptionsData.teamsLabelValue',
                },
              },
            ],
          },

          {
            props: {
              title: 'Select - Disabled Options',
              doc: SelectDoc,
              id: 'select-disabled',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'selectWithDisabledOptions',
                type: 'select',
                props: {
                  label: 'Select With Disabled Options',
                  required: true,
                  disabledProp: 'isDisabled',
                  placeholder: 'This is some placeholder text',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
                expressions: {
                  'props.options': 'formState.selectOptionsData.teamsLabelValue',
                },
              },
            ],
          },

          {
            props: {
              title: 'Select - Multiple',
              doc: SelectDoc,
              id: 'select-multiple',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'selectWithDisabledOptions',
                type: 'select',
                props: {
                  label: 'Select With Disabled Options',
                  required: true,
                  multiple: true,
                  addDefaultOption: false,
                  selectAllOption: 'Select All',
                  placeholder: 'This is some placeholder text',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
                expressions: {
                  'props.options': 'formState.selectOptionsData.teamsLabelValue',
                },
              },
            ],
          },

          {
            props: {
              title: 'Select (AKA Enum with Index)',
              doc: SelectDoc,
              id: 'select-index',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'selectWithIndex',
                type: 'select',
                props: {
                  label: 'Select With Indexed Options',
                  required: true,
                  valueProp: '<index>',
                  labelProp: 'name',
                  placeholder: 'This is some placeholder text',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
                expressions: {
                  'props.options': 'formState.selectOptionsData.teams',
                },
              },
            ],
          },

          {
            props: {
              title: 'Select exhibits by crime scene (from a list-source)',
              doc: SelectDoc,
              id: 'select-list-source',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'select-crime-scene',
                type: 'select2',
                props: {
                  label: 'Crime Scene Picker',
                  placeholder: 'Choose a crime scene',
                  listName: 'CrimeScenes',
                  sortColumn: 'reference',
                  description: '',
                  labelTemplate: '${crimeSceneTypeLookup}: ${name} - ref: ${reference}',
                  filterColumns: [],
                  selectColumns: ['crimeSceneId', 'name', 'crimeSceneTypeLookup', 'reference'],
                  contextMap: { crimeSceneId: '$.crimeSceneId' },
                },
                expressions: {
                  'props.filterColumns':
                    "[{column: 'operationId', stringFilter: model.selectedOperationId, constantFilter: true}]",
                },
              },
              {
                key: 'select-exhibit',
                type: 'select2',
                props: {
                  label: 'Crime Scene Exhibits',
                  placeholder: 'Choose an exhibit',
                  listName: 'Exhibits',
                  sortColumn: 'reference',
                  description: 'Exhibit list for crime scene',
                  labelTemplate: '${exhibitType}  - ref: ${reference}',
                  filterColumns: [
                    {
                      column: 'crimeSceneId',
                      constantFilter: true,
                      contextKey: 'crimeSceneId',
                    },
                  ],
                  selectColumns: ['exhibitId', 'reference'],
                  enableModelSync: true,
                  modelSyncExclusions: [],
                },
              },
            ],
          },
          {
            props: {
              title: 'Signature',
              doc: SignatureDoc,
              id: 'signature',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'signature',
                type: 'signature',
                props: {
                  label: 'Signature',
                  required: true,
                },
              },
            ],
          },

          {
            props: {
              title: 'Slide Toggle',
              doc: SlideToggleDoc,
              id: 'slide-toggle',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'toggle',
                type: 'toggle',
                props: {
                  label: 'Slide Toggle',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Table',
              doc: TableDoc,
              id: 'table',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'table',
                type: 'table',
                props: {
                  columnDefinitions: [
                    { key: 'reference', columnName: 'Exhibit Reference', tab: 'First' },
                    { key: 'sealNumber', columnName: 'Seal Number', tab: 'First' },
                    { key: 'exhibitDescription', columnName: 'Description', isEditorHeader: true, tab: 'Second' },
                    { key: 'seizedDate', columnName: 'Seized Date', dataType: 'date', tab: 'Second' },
                  ],
                  formId: 'Exhibits.Add',
                  enableAdd: true,
                  enableDelete: true,
                  enableEdit: true,
                  saveText: 'Custom Save',
                  cancelText: 'Custom Cancel',
                  deleteText: 'Custom Delete',
                  addText: 'Custom Add',
                  disabled: false,
                  itemLimit: 2,
                  required: false,
                  dialogWidth: '50vw',
                  label: 'Exhibits',
                  placeholder: 'This is placeholder text',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
                validators: {
                  validation: ['arrayRequired'],
                },
              },
            ],
          },

          {
            props: {
              title: 'Text Area',
              doc: TextAreaDoc,
              id: 'text-area',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'textarea',
                type: 'textarea',
                props: {
                  label: 'Text Area',
                  required: true,
                  cols: 1,
                  rows: 4,
                  placeholder: 'This is some placeholder text',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Text Area (Read Only)',
              doc: TextAreaReadonlyDoc,
              id: 'text-area-read-only',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'textareareadonly',
                type: 'textareareadonly',
                props: {
                  label: 'Text Area Read Only',
                  required: false,
                  cols: 1,
                  rows: 4,
                  placeholder: 'This is some placeholder text',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Text Panel',
              doc: TextPanelDoc,
              id: 'text-panel',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'textpanel',
                type: 'textpanel',
                props: {
                  label: 'Text Panel',
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Text With Barcode',
              doc: TextBarcodeDoc,
              id: 'text-barcode',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'textwithbarcode',
                type: 'textwithbarcode',
                props: {
                  label: 'Text With Barcode',
                  required: true,
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Time',
              doc: TimeDoc,
              id: 'time',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'time',
                type: 'time',
                props: {
                  label: 'Time',
                  required: true,
                  showSeconds: true,
                  defaultTime: { hours: 12, minutes: 30, seconds: 30 },
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            wrappers: ['formgallerygroup'],
            props: {
              title: 'Type Ahead',
            },
            fieldGroup: [
              {
                wrappers: ['formgallery'],
                props: {
                  grouped: true,
                  doc: TypeAheadDoc,
                  id: 'type-ahead-operation',
                },
                fieldGroup: [
                  {
                    key: 'typeahead',
                    type: 'typeahead',
                    props: {
                      label: 'Type Ahead',
                      required: true,
                      description:
                        'Operation type ahead.  One or more filters columns to avoid ambiguous search results ',
                      listName: 'Operations',
                      valueProp: 'operationId',
                      labelProp: 'name',
                      mustMatch: 'true',
                      noMatchText: 'No Operation Found',
                      filterColumns: [{ column: 'operationName', stringFilter: '', constantFilter: false }],
                      ariaLabel: 'Type ahead input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Type Ahead Must Match',
                  grouped: true,
                  doc: TypeAheadDoc,
                  id: 'type-ahead-submission',
                },
                fieldGroup: [
                  {
                    key: 'submissiontypeahead',
                    type: 'submissiontypeahead',
                    props: {
                      label: 'Submission Type Ahead',
                      required: true,
                      description: 'Submissions typeahead',
                      ariaLabel: 'Submissions typeahead input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Type Ahead Must Match',
                  grouped: true,
                  doc: TypeAheadDoc,
                  id: 'type-ahead-constant-filter',
                },
                fieldGroup: [
                  {
                    key: 'typeahead',
                    type: 'typeahead',
                    props: {
                      label: 'operations typeahead with constant filter',
                      required: true,
                      description:
                        'Type-ahead with constant filter - e.g. search with operations created by Carl Barron',
                      listName: 'Operations',
                      valueProp: 'operationId',
                      labelProp: 'operationName',
                      filterColumns: [
                        { column: 'operationName', stringFilter: '', constantFilter: false },
                        { column: 'createdByFullname', stringFilter: 'Carl Barron', constantFilter: true },
                      ],
                      ariaLabel: 'Operations typeahead with constant filter input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Exhibits typeahead',
                  grouped: true,
                  doc: TypeAheadDoc,
                  id: 'type-ahead-exhibit',
                },
                fieldGroup: [
                  {
                    key: 'typeahead',
                    type: 'typeahead',
                    props: {
                      label: 'Exhibits typeahead for Test Operation',
                      required: true,
                      description: 'Search exhibits from Test Operation by reference',
                      listName: 'Exhibits',
                      valueProp: 'exhibitId',
                      labelProp: 'reference',
                      filterColumns: [],
                      ariaLabel: 'Search exhibits from Test Operation by reference input field',
                    },
                    expressions: {
                      'props.filterColumns':
                        "[{column: 'operationId', stringFilter: model.selectedOperationId, constantFilter: true}, { column: 'reference', stringFilter: '', constantFilter: false }]",
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Type Ahead Users',
                  grouped: true,
                  doc: TypeAheadDoc,
                  id: 'type-ahead-users',
                },
                fieldGroup: [
                  {
                    key: 'typeaheadusers',
                    type: 'typeahead2',
                    props: {
                      label: 'Users Type Ahead',
                      required: true,
                      description: 'Users Type Ahead.  Must match',
                      listName: 'Users',
                      valueProp: 'personId',
                      labelProp: 'fullName',
                      mustMatch: true,
                      filterColumns: [{ column: 'fullName', stringFilter: '', constantFilter: false }],
                      ariaLabel: 'Users type ahead input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Old Configuration',
                  grouped: true,
                  doc: TypeAheadDoc,
                  id: 'type-ahead-original-config',
                },
                fieldGroup: [
                  {
                    key: 'typeaheadmustmatch',
                    type: 'typeahead',
                    props: {
                      label: 'Backward compatible with old configuration',
                      required: true,
                      description: 'With original configuration for compatibility.',
                      listName: 'Operations',
                      valueProp: 'operationId',
                      labelProp: 'name',
                      mustMatch: 'true',
                      noMatchText: 'No Operation Found',
                      ariaLabel: 'With original configuration input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Type Ahead LDAP Person Search',
                  grouped: true,
                  doc: TypeAheadDoc,
                  id: 'type-ahead-person-ldap',
                },
                fieldGroup: [
                  {
                    key: 'personsearchtypeahead',
                    type: 'typeahead2',
                    props: {
                      label: 'Person Search Type Ahead',
                      required: true,
                      description: 'Person search typeahead',
                      labelTemplate: '${fullName}?{ (${emailAddress})}',
                      dataSource: 'PersonSearch',
                      params: { mode: 3 },
                      searchParam: 'SearchString',
                      searchEmpty: false,
                      ariaLabel: 'Person search typeahead input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Persons typeahead against existing users',
                  doc: TypeAheadDoc,
                  grouped: true,
                  id: 'type-ahead-person-list',
                },
                fieldGroup: [
                  {
                    key: 'personsimpletypeahead',
                    type: 'typeahead2',
                    props: {
                      label: 'Persons Type Ahead',
                      description: 'Search for exising witnesses against Test Operation.',
                      listName: 'Persons',
                      labelTemplate: '${fullName}?{ (${personTypeDisplay}) ${ue1a4579ecbcc49238eeb006378d36d42}}',
                      labelProp: 'fullName',
                      mustMatch: true,
                      filterColumns: [],
                      selectColumns: [
                        'personId',
                        'fullName',
                        'personTypeDisplay',
                        'ue1a4579ecbcc49238eeb006378d36d42',
                        'canEdit',
                        'reference',
                        'forename',
                      ],
                      ariaLabel: 'Persons Type Ahead input field',
                    },
                    expressions: {
                      'props.filterColumns':
                        "[{column: '', stringFilter: model.selectedOperationId, constantFilter: true, entityTypeId: 1}, { column: 'fullName', stringFilter: '', constantFilter: false },{ column: 'personTypeId', stringFilter: '695973FF-8CA6-4F63-AE1F-2E4F93161829', constantFilter: true }]",
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Persons in "My Team" (Shadow)',
                  doc: TypeAheadDoc,
                  grouped: true,
                  id: 'type-ahead-my-team',
                },
                fieldGroup: [
                  {
                    key: 'All Staff typeahead',
                    type: 'typeahead2',
                    props: {
                      label: 'All Staff Type Ahead',
                      description:
                        'Search all staff - displays text if they are my team or not. (c/w filtered typeahead below)',
                      listName: 'Users',
                      labelTemplate: '${fullName} (In My Team - ${myTeam})',
                      labelProp: 'fullName',
                      mustMatch: true,
                      filterColumns: [{ column: 'fullName', stringFilter: '', constantFilter: false }],
                      selectColumns: [
                        'personId',
                        'fullName',
                        'JobDescription',
                        'myTeam',
                        'photo',
                        'emailAddress',
                        'phoneNumber',
                        'reference',
                        'initials',
                      ],
                      ariaLabel: 'Search all staff type ahead input field',
                    },
                  },
                  {
                    key: 'my team typeahead',
                    type: 'typeahead2',
                    props: {
                      label: 'My Team Type Ahead',
                      description: 'Search in your team - filtered to My Team only',
                      listName: 'Users',
                      labelTemplate: '${fullName}',
                      labelProp: 'fullName',
                      mustMatch: true,
                      filterColumns: [
                        { column: 'myTeam', stringFilter: true, constantFilter: true },
                        { column: 'fullName', stringFilter: '', constantFilter: false },
                      ],
                      selectColumns: ['personId', 'fullName', 'JobDescription', 'myTeam'],
                      ariaLabel: 'Search in your team type ahead input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Type Ahead - All Actions',
                  grouped: true,
                  doc: TypeAheadDoc,
                  id: 'type-ahead-all-actions',
                },
                fieldGroup: [
                  {
                    key: 'typeahead',
                    type: 'typeahead',
                    props: {
                      label: 'Actions',
                      required: true,
                      description: 'All actions you have permissions to see',
                      listName: 'Actions',
                      valueProp: 'actionId',
                      labelProp: 'reference',
                      mustMatch: true,
                      ariaLabel: 'Type Ahead All Actions input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Type Ahead - My Actions',
                  grouped: true,
                  doc: TypeAheadDoc,
                  id: 'type-ahead-my-actions',
                },
                fieldGroup: [
                  {
                    key: 'typeahead',
                    type: 'typeahead',
                    props: {
                      label: 'Actions display',
                      required: true,
                      description: 'Only my actions visible',
                      listName: 'Actions',
                      valueProp: 'actionId',
                      labelProp: 'reference',
                      mustMatch: true,
                      filterColumns: [
                        {
                          column: 'myActions',
                          stringFilter: true,
                          constantFilter: true,
                        },
                      ],
                      ariaLabel: 'Type Ahead - My Actions input field',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'EntityCalendarItem Type Ahead',
                  doc: TypeAheadDoc,
                  grouped: true,
                  id: 'type-ahead-task',
                },
                fieldGroup: [
                  {
                    key: 'tasktypeahead',
                    type: 'typeahead',
                    props: {
                      label: 'Tasks Type Ahead',
                      description: 'Search for EntityCalendarItems or "Task"s as they will be known as in Shadow',
                      listName: 'EntityCalendarItem',
                      valueProp: 'entityCalendarItemId',
                      labelProp: 'description',
                      mustMatch: true,
                      selectColumns: [
                        'description',
                        'requiredCompletionDate',
                        'actualCompletionDate',
                        'requiresCompletion',
                      ],
                      ariaLabel: 'Entity Calendar Item Type Ahead input field',
                    },
                  },
                ],
              },

              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Notification Type Ahead',
                  doc: TypeAheadDoc,
                  grouped: true,
                  id: 'type-ahead-notification',
                },
                fieldGroup: [
                  {
                    key: 'notificationtypeahead',
                    type: 'typeahead',
                    props: {
                      label: 'Notifications Type Ahead',
                      description: 'Search for EntityAlerts or "Notifications"s as they will be known as in Shadow',
                      listName: 'EntityAlert',
                      valueProp: 'entityAlertId',
                      labelProp: 'details',
                      mustMatch: true,
                      templatedProperties: {
                        filterColumns: [
                          {
                            column: '',
                            stringFilter: '${context.submissionId}',
                            entityTypeId: 2,
                            constantFilter: true,
                          },
                        ],
                      },

                      ariaLabel: 'Notifications Type Ahead input field',
                    },
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Unidentified Subject',
              id: 'unidentified-subject',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'unidentified-subject',
                type: 'datacards',
                props: {
                  selectMode: 'single',
                  listName: 'UnidentifiedSubject',
                  sortColumn: 'reference',
                  noResultsText: 'No unidentified subjects found',
                  title: 'Unidentified Subjects',
                  icon: '',
                  columnDefinitions: [
                    {
                      id: 'unitId',
                    },
                    {
                      id: 'crimeTypeId',
                    },
                    {
                      id: 'crimeNumber',
                    },
                    {
                      id: 'creationDate',
                    },
                    {
                      id: 'premisesName',
                    },
                    {
                      id: 'premisesAddress',
                    },
                    {
                      id: 'photoCount',
                    },
                    {
                      id: 'audioCount',
                    },
                    {
                      id: 'videoCount',
                    },
                    {
                      id: 'mediaJson',
                    },
                   
                    {
                      id: 'reference',
                    },
                  ],
                  idColumn: 'identId',
                  itemTemplate: {
                    type: 'unidentifiedsubject',
                  },
                  itemsUi: {
                    columnCounts: 3,
                    gap: '1rem',
                  },
                  label: '',
                },
              },
            ],
          },

          {
            props: {
              title: 'Uploader',
              doc: UploaderDoc,
              id: 'uploader',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'uploader',
                type: 'uploader',
                props: {
                  label: 'Uploader',
                  required: true,
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Uploader Offline',
              doc: UploaderOfflineDoc,
              id: 'uploader-offline',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'uploaderoffline',
                type: 'uploaderoffline',
                props: {
                  label: 'Uploader Offline',
                  required: true,
                  enableFileManager: true,
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                  itemTableName: 'devGallery',
                },
              },
            ],
          },

          {
            props: {
              title: 'Video',
              doc: VideoDoc,
              id: 'video',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'video',
                type: 'video',
                defaultValue: 'BD1CA7F9-3446-4100-A45F-73F7F4C5ECF1',
              },
            ],
          },

          {
            props: {
              title: 'Yes No',
              doc: YesNoDoc,
              id: 'yesno',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'yesno',
                type: 'yesno',
                props: {
                  label: 'Yes No',
                  required: true,
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            template: '<h2 class="menu-group">View Components</h2>',
            props: {
              menuGroup: {
                name: 'View Components',
                items: [
                  {
                    name: 'Chart-Bar',
                    items: [
                      { name: 'Chart-Bar (Model Data)', scrollId: 'bar-data' },
                      { name: 'Chart-Bar (DataSource)', scrollId: 'bar-datasource' },
                    ],
                  },
                  {
                    name: 'Chart-Pie',
                    items: [
                      { name: 'Chart-Pie (Model Data)', scrollId: 'pie-data' },
                      { name: 'Chart-Pie (DataSource)', scrollId: 'pie-datasource' },
                    ],
                  },
                  {
                    name: 'Chart-Line',
                    scrollId: 'line-data',
                  },
                  { name: 'Circular Gauge', scrollId: 'circular-gauge' },
                  { name: 'Date List', scrollId: 'date-list-view' },
                  { name: 'Display Group', scrollId: 'display-group' },
                  { name: 'Divider', scrollId: 'divider' },
                  { name: 'Entity Add', scrollId: 'entity-add' },
                  { name: 'Entity Summary', scrollId: 'entity-summary' },
                  { name: 'Field', scrollId: 'field-view' },
                  { name: 'Inline Calendar', scrollId: 'inline-calendar' },
                  { name: 'Person', scrollId: 'person-view' },
                  { name: 'Person List', scrollId: 'person-list' },
                  { name: 'Progress', scrollId: 'progress' },
                  {
                    name: 'Repeat',
                    items: [
                      { name: 'Repeat', scrollId: 'repeat' },
                      { name: 'Repeat Carousel', scrollId: 'repeat-carousel' },
                    ],
                  },
                  { name: 'Report Button', scrollId: 'report-button' },
                  { name: 'Rich Fields', scrollId: 'rich-fields' },
                  { name: 'Route List', scrollId: 'route-list' },
                  { name: 'Table', scrollId: 'table-view' },
                  { name: 'Text', scrollId: 'text-view' },
                ],
              },
            },
          },

          {
            wrappers: ['formgallerygroup'],
            props: {
              title: 'Chart - Bar',
            },
            fieldGroup: [
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Bar From Model Data',
                  doc: ChartBarDoc,
                  id: 'bar-data',
                  grouped: true,
                },
                fieldGroup: [
                  {
                    key: 'chartbar',
                    type: 'chartbar',
                    wrappers: ['card'],
                    props: {
                      label: 'Exhibits by Location',
                      width: '100%',
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Bar From Data Source',
                  doc: ChartBarDoc,
                  id: 'bar-datasource',
                  grouped: true,
                },
                fieldGroup: [
                  {
                    key: 'chartbardatasource',
                    type: 'chartbar',
                    wrappers: ['card'],
                    props: {
                      label: 'Exhibits by Location',
                      width: '100%',
                      direction: 'vertical',
                      dataSource: 'PhanerosSubmissionExhibitsByLocation',
                      params: ['submissionId'],
                    },
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Chart - Pie',
            },
            wrappers: ['formgallerygroup'],
            fieldGroup: [
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Pie From Model Data',
                  doc: ChartPieDoc,
                  grouped: true,
                  id: 'pie-data',
                },
                fieldGroup: [
                  {
                    key: 'chartdonut',
                    type: 'chartdonut',
                    wrappers: ['card'],
                    props: {
                      label: 'Exhibits by Type',
                      title: '${total}',
                      subTitle: 'Exhibits',
                      width: '400px',
                      legend: {
                        width: '36%',
                        height: '100%',
                      },
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Pie From DataSource',
                  doc: ChartPieDoc,
                  grouped: true,
                  id: 'pie-datasource',
                },
                fieldGroup: [
                  {
                    key: 'chartdonutdatasourceexhibits',
                    type: 'chartdonut',
                    wrappers: ['card'],
                    props: {
                      label: 'Exhibits by Status',
                      title: '${total}',
                      subTitle: 'Exhibits',
                      dataSource: 'PhanerosSubmissionExhibitsByStatus',
                      params: ['submissionId'],
                      width: '400px',
                      legend: {
                        width: '36%',
                      },
                    },
                  },
                ],
              },
            ],
          },

          {
            wrappers: ['formgallery'],
            props: {
              title: 'Line From Model Data',
              doc: ChartLineDoc,
              id: 'line-data',
            },
            fieldGroup: [
              {
                key: 'chartline',
                type: 'chartline',
                wrappers: ['card'],
                props: {
                  label: 'Unread Messages',
                  title: 'Last 10 days',
                  maxWidth: '400px',
                  height: '225px',
                  isAreaGradient: true,
                  areaColour: '#158ac0',
                  yAxis: {
                    labelFormat: 'n0',
                    minimum: 0,
                  },
                },
              },
            ],
          },

          {
            wrappers: ['formgallery'],
            props: {
              title: 'Circular Gauge',
              doc: CircularGaugeDoc,
              id: 'circular-gauge',
            },
            fieldGroup: [
              {
                key: 'circulargauge',
                type: 'circulargauge',
                wrappers: ['card'],
                props: {
                  width: '300px',
                  height: '350px',
                  title: 'Actions',
                },
              },
            ],
          },

          {
            props: {
              title: 'Date List View',
              doc: DateListDoc,
              id: 'date-list-view',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'datelistview',
                type: 'datelistview',
                wrappers: ['card'],
                props: { label: 'Key Dates' },
                fieldGroup: [
                  {
                    key: 'bailDate',
                    type: 'textview',
                    props: { label: 'BAIL', icon: 'fas fa-door-open', dataType: 'date' },
                  },
                  {
                    key: 'courtDate',
                    type: 'textview',
                    props: { label: 'COURT', icon: 'fas fa-gavel', dataType: 'date' },
                  },
                  {
                    key: 'cpsDate',
                    type: 'textview',
                    props: { label: 'CPS', icon: 'fas fa-balance-scale', dataType: 'date' },
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Display Group',
              doc: DisplayGroupDoc,
              id: 'display-group',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'displaygroup',
                type: 'displaygroup',
                props: {
                  label: 'Display Group',
                  required: true,
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Divider',
              doc: DividerDoc,
              id: 'divider',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'divider',
                type: 'divider',
              },
            ],
          },

          {
            props: {
              title: 'Entity Add',
              doc: EntityAddDoc,
              id: 'entity-add',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                type: 'entityadd',
                props: {
                  entities: [
                    {
                      icon: 'fas fa-male',
                      subIcon: 'fal fa-plus',
                      text: 'Nominal',
                      formId: 'Investigator.Cases.Nominal.Add',
                    },
                    {
                      icon: 'fas fa-map-marker-alt',
                      subIcon: 'fal fa-plus',
                      text: 'Scene',
                      formId: 'Investigator.Cases.Scene.Add',
                    },
                  ],
                },
              },
            ],
          },

          {
            props: {
              title: 'Entity Summary',
              doc: EntitySummaryDoc,
              id: 'entity-summary',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'entitysummary',
                type: 'entitysummary',
                props: {
                  entitySummaryItems: [
                    {
                      icon: {
                        name: 'fal fa-folder',
                      },
                      label: {
                        text: 'Case',
                      },
                      text: {
                        property: 'operationName',
                      },
                    },
                    {
                      isDivider: true,
                    },
                    {
                      icon: {
                        name: 'fal fa-hashtag',
                      },
                      label: {
                        text: 'Reference',
                      },
                      text: {
                        property: 'operationAlias',
                      },
                    },
                  ],
                },
              },
            ],
          },

          {
            props: {
              title: 'Field View',
              doc: FieldViewDoc,
              id: 'field-view',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'fieldview',
                wrappers: ['card'],
                type: 'fieldview',
                props: {
                  label: 'Submission',
                  layout: 'Column',
                },
                fieldGroup: [
                  {
                    key: 'operationName',
                    type: 'textview',
                    props: { label: 'Operation:', classNames: 'heading' },
                  },
                  {
                    key: 'referenceName',
                    type: 'textview',
                    props: { label: 'Reference', classNames: 'heading' },
                  },
                  {
                    key: 'statusName',
                    type: 'textview',
                    props: { label: 'Status:', classNames: 'heading' },
                  },
                ],
              },
            ],
          },
          {
            props: {
              title: 'File Manager',
              doc: FileManagerDoc,
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'filemanager',
                type: 'filemanager',
                props: {
                  itemTableName: 'devGallery',
                },
              },
            ],
          },
          {
            props: {
              title: 'Inline Calendar',
              doc: InlineCalendarDoc,
              id: 'inline-calendar',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'inlinecalendar',
                type: 'inlinecalendar',
                props: {
                  width: '400px',
                  showTitle: true,
                  subtitleFontSize: '12px',
                  subtitleLineHeight: '14px',
                  subtitleColour: 'grey',
                  titleFontSize: '32px',
                  titleLineHeight: '36px',
                  titleColour: '#0ce872',
                },
              },
            ],
          },

          {
            props: {
              title: 'Person View',
              doc: PersonDoc,
              id: 'person-view',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'person',
                type: 'personview',
                wrappers: ['card'],
                props: { label: 'Person View', imageSize: 'xlarge', noData: 'No person data' },
                fieldGroup: [
                  { key: 'fullName', type: 'textview', props: { classNames: 'heading' } },
                  { key: 'jobDescription', type: 'textview' },
                  { key: 'collarNo', type: 'textview' },
                  { key: 'email', type: 'textview' },
                  { key: 'contactNo', type: 'textview' },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Person List View',
              doc: PersonListDoc,
              id: 'person-list',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'personlist',
                type: 'personlistview',
                wrappers: ['card'],
                props: {
                  label: 'People',
                  imageSize: 'medium',
                  photoProp: 'photoId',
                  initialsProp: 'initials',
                },
                fieldGroup: [
                  { key: 'fullName', type: 'textview', props: { classNames: 'heading' } },
                  { key: 'personType', type: 'textview' },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Progress',
              doc: ProgressDoc,
              id: 'progress',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'progress',
                type: 'progress',
                props: {
                  label: 'Unread',
                },
              },
            ],
          },

          {
            props: {
              title: 'Repeat',
            },
            wrappers: ['formgallerygroup'],
            fieldGroup: [
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Repeat Items',
                  doc: RepeatDoc,
                  grouped: true,
                  id: 'repeat',
                },
                fieldGroup: [
                  {
                    key: 'repeatItems',
                    type: 'repeat',
                    fieldArray: {
                      fieldGroup: [{ key: 'name', type: 'textview' }],
                    },
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Repeat Carousel',
                  doc: RepeatDoc,
                  grouped: true,
                  id: 'repeat-carousel',
                },
                fieldGroup: [
                  {
                    key: 'repeatItemsCarousel',
                    type: 'repeat',
                    props: {
                      enableCarousel: true,
                    },
                    fieldArray: {
                      fieldGroup: [
                        {
                          key: 'chartdonut',
                          type: 'chartdonut',
                          props: {
                            label: 'Exhibits by Type',
                            title: '${total}',
                            subTitle: 'Exhibits',
                            width: '450px',
                            contextProperty: 'filterString',
                            legend: {
                              width: '0',
                              visible: false,
                            },
                          },
                        },
                      ],
                    },
                  },
                ],
              },
            ],
          },

          {
            wrappers: ['formgallerygroup'],
            fieldGroup: [
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Report Button',
                  doc: ReportButtonDoc,
                  id: 'report-button',
                },
                fieldGroup: [
                  {
                    key: 'reportButton',
                    type: 'reportbutton',
                    props: {
                      label: 'MG21',
                      name: 'MG21',
                      format: 'pdf',
                      formId: 'Submissions.Other.View',
                    },
                    expressions: {
                      'props.params': '{ submissionId: formState?.context?.submissionId }',
                    },
                  },
                  {
                    template:
                      "<h2>This won't function in form gallery, but you can see the appearance and config options here.</h2>",
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Rich Fields View',
              doc: RichFieldsDoc,
              id: 'rich-fields',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                type: 'richfieldsview',
                wrappers: ['card'],
                key: 'overview',
                props: {
                  label: 'Overview',
                  noData: 'No overview data found',
                },
                fieldGroup: [
                  {
                    key: 'circumstances',
                  },
                  {
                    key: 'keywords',
                    props: { label: 'Notes' },
                  },
                  {
                    key: 'overviewlookup',
                    type: 'textview',
                    props: {
                      label: 'Lookup',
                      dataType: 4,
                    },
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Route List',
              doc: RouteListDoc,
              id: 'route-list',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'routelist',
                type: 'routelist',
                props: {
                  marginOnItems: false,
                  fieldArray: {
                    type: 'jumbolink',
                  },
                },
              },
            ],
          },

          {
            props: {
              title: 'Table View',
              doc: TableDoc,
              id: 'table-view',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'tableview',
                type: 'tableview',
                props: {
                  columnDefinitions: [
                    { key: 'exhibitReference', columnName: 'Reference' },
                    { key: 'icon', columnName: 'Cat.', dataType: 'icon' },
                    { key: 'exhibitType', columnName: 'Type' },
                    { key: 'exhibitDescription', columnName: 'Description' },
                    { key: 'attributedTo', columnName: 'Attributed', dataType: 'profile' },
                    { key: 'priority', columnName: 'Priority', dataType: 'status' },
                    { key: 'pin', columnName: 'Pin / Password' },
                  ],
                  description:
                    'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                },
              },
            ],
          },

          {
            props: {
              title: 'Text View',
              doc: TextDoc,
              id: 'text-view',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'textview',
                type: 'textview',
              },
            ],
          },

          {
            template: '<h2 class="menu-group">Wrapper Components</h2>',
            props: {
              menuGroup: {
                name: 'Wrapper Components',
                items: [
                  { name: 'Card', scrollId: 'wrapper-card' },
                  { name: 'Card List', scrollId: 'wrapper-card-list' },
                  { name: 'Form Field', scrollId: 'wrapper-form-field' },
                  { name: 'Grid', scrollId: 'wrapper-grid' },
                  { name: 'Panel', scrollId: 'wrapper-panel' },
                  { name: 'LatLong', scrollId: 'wrapper-latlong' },
                  { name: 'Scroll', scrollId: 'wrapper-scroll' },
                  { name: 'Section Edit', scrollId: 'wrapper-section-edit' },
                  { name: 'List Filter', scrollId: 'wrapper-list-filter' },
                ],
              },
            },
          },

          {
            props: {
              title: 'Card',
            },
            wrappers: ['formgallerygroup'],
            fieldGroup: [
              {
                wrappers: ['formgallery'],
                props: {
                  doc: CardWrapperDoc,
                  grouped: true,
                  id: 'wrapper-card',
                },
                fieldGroup: [
                  {
                    key: 'card',
                    wrappers: ['card'],
                    props: {
                      label: 'Card Title',
                    },
                    fieldGroup: [
                      {
                        key: 'name',
                        type: 'input',
                        props: {
                          type: 'text',
                          label: 'Name',
                          ariaLabel: 'Name input field',
                        },
                      },
                      {
                        key: 'age',
                        type: 'input',
                        props: {
                          type: 'text',
                          label: 'Age',
                          ariaLabel: 'Age input field',
                        },
                      },
                    ],
                  },
                ],
              },
              {
                wrappers: ['formgallery'],
                props: {
                  title: 'Card Hover',
                  doc: CardWrapperDoc,
                  grouped: true,
                  id: 'wrapper-card-hover',
                },
                fieldGroup: [
                  {
                    key: 'card',
                    wrappers: ['card'],
                    props: {
                      label: 'Card Title',
                      enableHoverClass: true,
                    },
                    fieldGroup: [
                      {
                        key: 'name',
                        type: 'input',
                        props: {
                          type: 'text',
                          label: 'Name',
                          ariaLabel: 'Name input field',
                        },
                      },
                      {
                        key: 'age',
                        type: 'input',
                        props: {
                          type: 'text',
                          label: 'Age',
                          ariaLabel: 'Age input field',
                        },
                      },
                    ],
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Card List',
              doc: CardListWrapperDoc,
              id: 'wrapper-card-list',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'card-list',
                wrappers: ['cardlist'],
                props: {
                  cardListLabel: 'Card List Title',
                },
                fieldGroup: [
                  {
                    key: 'name',
                    type: 'input',
                    props: {
                      type: 'text',
                      label: 'Name',
                      ariaLabel: 'Name input field',
                    },
                  },
                  {
                    key: 'age',
                    type: 'input',
                    props: {
                      type: 'text',
                      label: 'Age',
                      ariaLabel: 'Age input field',
                    },
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Form Field',
              doc: FormFieldWrapperDoc,
              id: 'wrapper-form-field',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'formfieldinput',
                type: 'input',
                props: {
                  label: 'Form field',
                  ariaLabel: 'Form field input',
                },
              },
            ],
          },

          {
            props: {
              title: 'Grid',
              doc: GridWrapperDoc,
              id: 'wrapper-grid',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'grid',
                wrappers: ['grid'],
                fieldGroup: [
                  {
                    key: 'left',
                    type: 'input',
                    props: {
                      type: 'text',
                      label: 'Left',
                      gridArea: 'left',
                      ariaLabel: 'Left input field',
                    },
                  },
                  {
                    key: 'right',
                    type: 'input',
                    props: {
                      type: 'text',
                      label: 'Right',
                      gridArea: 'right',
                      ariaLabel: 'Right input field',
                    },
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Panel',
              doc: PanelWrapperTypeDoc,
              id: 'wrapper-panel',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'address',
                wrappers: ['panel'],
                props: { panelLabel: 'Address' },
                fieldGroup: [
                  {
                    key: 'town',
                    type: 'input',
                    props: {
                      required: true,
                      type: 'text',
                      label: 'Town',
                      description:
                        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                      ariaLabel: 'Town input field',
                    },
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Lat/Long',
              doc: LatLongDoc,
              id: 'wrapper-latlong',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'latlong',
                wrappers: ['latlong'],
                fieldGroup: [
                  {
                    key: 'latitude',
                    type: 'input',
                    props: {
                      label: 'Latitude',
                      ariaLabel: 'Latitude input field',
                    },
                  },
                  {
                    key: 'longitude',
                    type: 'input',
                    props: {
                      label: 'Longitude',
                      ariaLabel: 'Longitude input field',
                    },
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Scroll',
              doc: ScrollWrapperDoc,
              id: 'wrapper-scroll',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'scroll',
                wrappers: ['card', 'scroll'],
                props: {
                  scrollHeight: '300px',
                  scrollWidth: '200px',
                },
                fieldGroup: [
                  {
                    template: '<div>I am 1000px high and 5000px wide</div>',
                    className: 'scroll-wrapper-child',
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Section Edit',
              doc: SectionEditWrapperDoc,
              id: 'wrapper-section-edit',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                wrappers: ['sectionedit'],
                props: {
                  sectionEditMarginTop: '0',
                  sectionEditMarginBottom: '0',
                },
                fieldGroup: [
                  {
                    fieldGroup: [
                      {
                        wrappers: ['card'],
                        fieldGroup: [
                          {
                            props: {
                              cardAlt: true,
                              singleLine: true,
                              label: 'Theme: ',
                              inlineText: true,
                              alwaysShowLabel: true,
                              dataType: 'lookup',
                            },
                            key: 'themeId',
                            type: 'textview',
                          },
                        ],
                      },
                    ],
                  },
                  {
                    fieldGroup: [
                      {
                        fieldGroup: [
                          {
                            wrappers: [],
                            props: {
                              label: 'Theme',
                              lookupKey: 'Themes',
                              enableFieldLabel: true,
                              className: 'user-settings alt-form-field',
                            },
                            key: 'themeId',
                            type: 'lookup',
                          },
                        ],
                      },
                    ],
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'List Filter',
              doc: ListFilterWrapperDoc,
              id: 'wrapper-list-filter',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                wrappers: ['listfilter', 'scroll'],
                props: {
                  scrollMaxHeight: '300px',
                  listFilterEnableSearch: true,
                  listFilterSearchText: 'Find Cases',
                  listFilterSearchIcon: 'fas fa-folders',
                  listFilterSearchSubTitle: 'Cases',
                  listFilterSearchDialogWidth: '100%',
                  listFilterSearchDialogMaxWidth: '700px',
                  listFilterSearchResultOptions: {
                    listName: 'Operations',
                    idColumn: 'operationId',
                    columnDefinitions: [{ id: 'operationId' }, { id: 'operationName' }, { id: 'operationAlias' }],
                    sortColumn: 'operationName',
                    sortDirection: 'asc',
                    pageSize: -1,
                    itemTemplate: {
                      wrappers: ['card'],
                      props: {
                        enableHoverClass: true,
                      },
                      type: 'searchresultcase',
                    },
                  },
                },
                fieldGroup: [
                  {
                    key: 'listfilterwrapper',
                    type: 'datacards',
                    props: {
                      listName: 'Operations',
                      idColumn: 'operationId',
                      sortColumn: 'operationId',
                      sortDirection: 'desc',
                      pageSize: 20,
                      columnDefinitions: [
                        {
                          id: 'operationId',
                          value: 'Id',
                        },
                      ],
                      showPaging: false,
                      itemTemplate: {
                        template: '<div>A list item</div>',
                      },
                      itemsUi: {
                        columnCounts: [1],
                        gap: '1rem',
                      },
                      templatedProperties: {
                        filterColumns: [
                          {
                            column: 'operationStatusName',
                            constantFilter: true,
                            stringFilter: 'Active',
                          },
                        ],
                      },
                    },
                  },
                ],
              },
            ],
          },
          {
            template: '<h2 class="menu-group">Misc Components</h2>',
            props: {
              menuGroup: {
                name: 'Misc Components',
                items: [
                  { name: 'Action Bar', scrollId: 'action-bar' },
                  { name: 'Button List', scrollId: 'button-list' },
                  { name: 'Form Actions', scrollId: 'form-actions' },
                  { name: 'Jumbo Link', scrollId: 'jumbo-link' },
                  { name: 'Stepper', scrollId: 'stepper' },
                ],
              },
            },
          },

          {
            props: {
              title: 'Action Bar',
              doc: ActionBarDoc,
              id: 'action-bar',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                type: 'actionbar',
                wrappers: ['card'],
                props: { actionStyle: 'Button', actionLayout: 'Column' },
                fieldGroup: [
                  {
                    props: { label: 'Foo', url: '/home' },
                  },
                  {
                    props: { label: 'Bar', url: '/home' },
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Button List',
              doc: ButtonListDoc,
              id: 'button-list',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'buttonlist',
                type: 'buttonlist',
                props: {
                  enableClear: true,
                  buttons: [
                    {
                      text: 'Button One',
                      value: 'button1value',
                    },
                    {
                      text: 'Button Two',
                      value: 'button2value',
                    },
                    {
                      text: 'Button Three',
                      value: 'button3value',
                    },
                  ],
                },
              },
            ],
          },

          {
            props: {
              title: 'Form Actions',
              doc: FormActionsDoc,
              id: 'form-actions',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                type: 'formactions',
                fieldGroup: [
                  {
                    props: { label: 'Cancel', action: 'cancel' },
                  },
                  {
                    props: { label: 'Save a Draft', color: 'primary', action: 'draft' },
                  },
                  {
                    props: { label: 'Save & Submit', color: 'primary', action: 'save' },
                    expressions: {
                      'props.disabled': 'formState?.isValid ? !formState?.isValid() : true',
                    },
                  },
                ],
              },
            ],
          },

          {
            props: {
              title: 'Jumbo Link',
              doc: JumboLinkDoc,
              id: 'jumbo-link',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                key: 'jumbolink',
                type: 'jumbolink',
                props: {
                  fill: false,
                  enableBookmark: false,
                  bookmarkRouteId: 'Phaneros.Home',
                  defaultIfEmpty: '',
                },
              },
            ],
          },

          {
            props: {
              title: 'Stepper',
              doc: StepperDoc,
              id: 'stepper',
            },
            wrappers: ['formgallery'],
            fieldGroup: [
              {
                type: 'stepper',
                fieldGroup: [
                  {
                    props: { label: 'Personal data' },
                    fieldGroup: [
                      {
                        key: 'firstname',
                        type: 'input',
                        props: {
                          label: 'First name',
                          required: true,
                          description:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                          ariaLabel: 'First name input field',
                        },
                      },
                      {
                        key: 'age',
                        type: 'input',
                        props: {
                          type: 'number',
                          label: 'Age',
                          required: true,
                          description:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                          ariaLabel: 'Age input field',
                        },
                      },
                    ],
                  },
                  {
                    props: { label: 'Destination' },
                    fieldGroup: [
                      {
                        key: 'country',
                        type: 'input',
                        props: {
                          label: 'Country',
                          required: true,
                          description:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                          ariaLabel: 'Country input field',
                        },
                      },
                    ],
                  },
                  {
                    props: { label: 'Day of the trip' },
                    fieldGroup: [
                      {
                        key: 'day',
                        type: 'date',
                        props: {
                          label: 'Day of the trip',
                          required: true,
                          description:
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
                          ariaLabel: 'Day of the trip input field',
                        },
                      },
                    ],
                  },
                ],
              },
            ],
          },
          {
            template: '<h2 class="menu-group">Schema downloads</h2>',
            props: {
              menuGroup: {
                name: 'Schema Downloads',
                items: [
                  { name: 'Download as JSON', route: 'json' },
                  { name: 'Download as typescript', route: 'ts' },
                ],
              },
            },
          },
        ],
      },
    ],
    options: {
      formState: {
        selectOptionsData: {
          teams: [
            { id: '1', name: 'Bayern Munich', sportId: '1' },
            { id: '2', name: 'Real Madrid', sportId: '1' },
            { id: '3', name: 'Cleveland', sportId: '2' },
            { id: '4', name: 'Miami', sportId: '2' },
          ],
          teamsLabelValue: [
            { value: '1', label: 'Bayern Munich', sportId: '1', isDisabled: false },
            { value: '2', label: 'Real Madrid', sportId: '1', isDisabled: true },
            { value: '3', label: 'Cleveland', sportId: '2', isDisabled: false },
            { value: '4', label: 'Miami', sportId: '2', isDisabled: true },
          ],
          players: [
            { id: '1', name: 'Bayern Munich (Player 1)', teamId: '1' },
            { id: '2', name: 'Bayern Munich (Player 2)', teamId: '1' },
            { id: '3', name: 'Real Madrid (Player 1)', teamId: '2' },
            { id: '4', name: 'Real Madrid (Player 2)', teamId: '2' },
            { id: '5', name: 'Cleveland (Player 1)', teamId: '3' },
            { id: '6', name: 'Cleveland (Player 2)', teamId: '3' },
            { id: '7', name: 'Miami (Player 1)', teamId: '4' },
            { id: '8', name: 'Miami (Player 2)', teamId: '4' },
          ],
          colours: [
            { label: 'Red', value: 'red' },
            { label: 'Blue', value: 'blue' },
            { label: 'Green', value: 'green' },
          ],
        },
      },
    },
  };

  model = {
    imagecheck: `2022\/08\/25\/3ef46233adef4e2491c8eb0dc568ac1e.png`,
    richTextEditor: '<b>Some rich text</b> with mark up',
    richTextViewer:
      '<p><span style="font-size: 14pt;"><span style="color: rgb(255, 0, 0); text-decoration: inherit;">Some markup </span></span><span style="font-size: 24pt;"><span style="font-family: Verdana, Geneva, sans-serif;"><strong><em><span style="text-decoration: underline;">Something else</span></em></strong></span></span><strong>​</strong></p>',
    examinationTypeLookupId: '8da9efb1-439f-49ca-8072-6d6320ab79c0',
    selectedOperationId: 'BF772502-95BF-49DD-9BA5-A43A82835077',
    chartbar: {
      results: [
        { name: 'House', value: 2, cssClass: 'c-neu-4' },
        { name: 'Car', value: 5, cssClass: 'c-pos-4' },
        { name: 'Park', value: 2, cssClass: 'c-neg-3' },
        { name: 'School', value: 4, cssClass: 'c-pos-2' },
      ],
    },
    chartdonut: {
      table1: {
        results: [
          { name: 'Computer Device', value: 1, cssClass: 'c-neu-4', url: '/form-gallery#1' },
          { name: 'Drugs', value: 2, cssClass: 'c-pos-4', url: '/form-gallery#2' },
          { name: 'Fibres', value: 3, cssClass: 'c-neg-5', url: '/form-gallery#3' },
          { name: 'Firearms', value: 1, cssClass: 'c-neu-5', url: '/form-gallery#4' },
          { name: 'Footwear', value: 2, cssClass: 'c-pos-5', url: '/form-gallery#5' },
          { name: 'Mobile Device', value: 6, cssClass: 'c-neu-6', url: '/form-gallery#6' },
        ],
      },
      table2: {
        results: [{ total: 15 }],
      },
    },
    circulargauge: {
      table1: {
        results: [
          { name: 'Allocated', value: 8 },
          { name: 'In Progress', value: 15 },
          { name: 'Done', value: 7 },
        ],
      },
      table2: {
        results: [{ total: 30 }],
      },
    },
    chartline: {
      table1: {
        results: [
          { name: '01', value: 1 },
          { name: '02', value: 5 },
          { name: '03', value: 2 },
          { name: '04', value: 4 },
          { name: '05', value: 5 },
          { name: '06', value: 9 },
          { name: '07', value: 7 },
          { name: '08', value: 4 },
          { name: '09', value: 5 },
          { name: '10', value: 10 },
        ],
      },
    },
    readonly: 'foobar',
    textpanel: 'foobar',
    textareareadonly:
      'READONLY AutoSize - Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras feugiat vehicula eros, sed dictum purus luctus sit amet. Etiam mollis hendrerit semper. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis convallis lorem et ante laoreet efficitur. In quam quam, dignissim in ipsum quis, varius condimentum diam. In hac habitasse platea dictumst. Vestibulum nisi nibh, vulputate ac dapibus sit amet, dignissim vel sapien. Donec nisl justo, finibus vel tincidunt at, imperdiet at lacus. Nam ante eros, accumsan at dapibus id, placerat in enim. Maecenas non euismod magna. Vestibulum nisi sapien, hendrerit vel rutrum sit amet, cursus in ex. Aliquam iaculis lacus urna. In dictum diam ac lacus elementum, non maximus sem efficitur. Ut feugiat vulputate purus sit amet hendrerit.',
    exhibits2: [
      {
        exhibitReference: '123',
        sealNumber: '456',
        exhibitDescription: 'Exhibit 1',
      },

      {
        exhibitReference: '789',
        sealNumber: '1011',
        exhibitDescription: 'Exhibit 2',
      },
    ],
    lookupNoLeaf: 'ce12caaa-e957-4e7d-8db0-95fd414c47f0',
    lookupLeaf: 'f181a443-1900-4e2a-ac14-4d3077103440',
    table: [
      {
        reference: 'stuff',
        sealNumberKnown: true,
        exhibitDescription: 'q',
        seizedDate: {
          dateTime: '2021-05-27T13:40:38+01:00',
          timeZoneId: null,
        },
        seizedBy: 'dwdw',
        evidenceType: null,
        exhibitType: null,
        healthHazard: false,
        dnaPreservation: false,
        examinationType: null,
        examinations: null,
        examinationSupportingInfo: 'ssss',
        exhibitLocation: 'Branch',
        returnDestination: 'Branch',
        sealNumber: '1234',
        yourBranch: true,
        returnDestinationYourBranch: true,
      },
    ],

    tableview: [
      {
        exhibitReference: '001',
        exhibitType: 'Laptop (non-Mac)',
        exhibitDescription: 'laptop unknown make',
        seizedFrom: null,
        attributedTo: 'Joe Bloggs',
        recoveredBy: 'Jim Kent',
        priority: {
          value: 'High',
          cssClass: 'c-neg-3',
        },
        pin: null,
        icon: 'fas fa-arrow-up',
      },
      {
        exhibitReference: '002',
        exhibitType: 'Laptop (non-Mac)',
        exhibitDescription: 'laptop unknown make',
        seizedFrom: null,
        attributedTo: 'Joe Bloggs',
        recoveredBy: 'Jim Kent',
        priority: null,
        pin: '12345',
        icon: 'far fa-home',
      },
    ],
    multichoice: [
      '2bbb5139-62ae-4484-9cb3-a7de39479585',
      '34144e78-7545-450b-90e6-01a2bc4e1c2e',
      'ac6d4246-2062-43fa-bd4a-0ad4fa32368c',
    ],
    overview: {
      circumstances:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
      keywords:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque feugiat cursus magna vel sodales. In quis lacus eget erat luctus rutrum non sed lorem. Donec pulvinar ante at mauris molestie, vitae semper urna lobortis.',
      overviewlookup: '79428ef7-65eb-44ff-8c84-86066a7d4379',
    },
    fieldview: { operationName: 'Test operation', statusName: 'Investigation', referenceName: '88088808-02' },
    person: {
      fullName: 'Forename Surname',
      initials: 'FS',
      jobDescription: 'Job Description',
      collarNo: '12345',
      contactNo: '08777777777',
      email: 'forename.surname@person.com',
      photoId: null,
    },
    personlist: [
      {
        fullName: 'Person One the quick brown fox jumps over the lazy dog',
        initials: 'PO',
        jobDescription: 'Job Description',
        personType: 'Analyst',
        photoId: null,
      },
      {
        fullName: 'Person Two',
        initials: 'PT',
        jobDescription: 'Job Description',
        personType: 'Analyst',
        photoId: null,
      },
      {
        fullName: 'Person Three',
        initials: 'PT',
        jobDescription: 'Job Description',
        personType: 'Analyst',
        photoId: null,
      },
      {
        fullName: 'Person Four',
        initials: 'PF',
        jobDescription: 'Job Description',
        personType: 'Analyst',
        photoId: null,
      },
    ],
    datelistview: {
      bailDate: { dateTime: '2022-03-01T00:00:00Z', timeZoneId: null },
      courtDate: { dateTime: '2022-03-02T00:00:00Z', timeZoneId: null },
      cpsDate: { dateTime: '2022-03-03T00:00:00Z', timeZoneId: null },
    },
    textview: 'This is some text',
    chip: { text: 'Chip Text' },
    progress: {
      table2: {
        results: [
          {
            total: 40,
          },
        ],
      },
    },
    repeatItems: [
      { name: 'Item One' },
      { name: 'Item Two' },
      { name: 'Item Three' },
      { name: 'Item Four' },
      { name: 'Item Five' },
      { name: 'Item Six' },
      { name: 'Item Seven' },
    ],
    repeatItemsCarousel: [
      {
        chartdonut: {
          table1: {
            results: [
              { name: 'Computer Device', value: 1, cssClass: 'c-neu-4', url: '/form-gallery#1' },
              { name: 'Drugs', value: 2, cssClass: 'c-pos-4', url: '/form-gallery#2' },
              { name: 'Fibres', value: 3, cssClass: 'c-neg-5', url: '/form-gallery#3' },
              { name: 'Firearms', value: 1, cssClass: 'c-neu-5', url: '/form-gallery#4' },
              { name: 'Footwear', value: 2, cssClass: 'c-pos-5', url: '/form-gallery#5' },
              { name: 'Mobile Device', value: 6, cssClass: 'c-neu-6', url: '/form-gallery#6' },
            ],
          },
          table2: {
            results: [{ total: 15 }],
          },
        },
      },
      {
        chartdonut: {
          table1: {
            results: [
              { name: 'Computer Device', value: 4, cssClass: 'c-neu-4', url: '/form-gallery#1' },
              { name: 'Drugs', value: 1, cssClass: 'c-pos-4', url: '/form-gallery#2' },
              { name: 'Fibres', value: 4, cssClass: 'c-neg-5', url: '/form-gallery#3' },
              { name: 'Firearms', value: 2, cssClass: 'c-neu-5', url: '/form-gallery#4' },
            ],
          },
          table2: {
            results: [{ total: 11 }],
          },
        },
      },
      {
        chartdonut: {
          table1: {
            results: [
              { name: 'Computer Device', value: 4, cssClass: 'c-neu-4', url: '/form-gallery#1' },
              { name: 'Drugs', value: 6, cssClass: 'c-pos-4', url: '/form-gallery#2' },
            ],
          },
          table2: {
            results: [{ total: 10 }],
          },
        },
      },
    ],
    inlinecalendar: [
      {
        entityCalendarItemId: '01',
        requiredCompletionDate: '2022-03-05',
        description: 'This is item one',
        requiresCompletion: true,
        actualCompletionDate: '',
      },
      {
        entityCalendarItemId: '01',
        requiredCompletionDate: '2022-03-08',
        description: 'This is item one',
        requiresCompletion: false,
        actualCompletionDate: '',
      },
      {
        entityCalendarItemId: '02',
        requiredCompletionDate: '2022-03-30',
        description: 'This is item two',
        requiresCompletion: true,
        actualCompletionDate: '2022-03-31',
      },
      {
        entityCalendarItemId: '03',
        requiredCompletionDate: '2022-04-26',
        description: 'This is item three',
      },
    ],
    jumbolink: {
      text: 'Jumbo Link',
      icon: 'chat',
      url: '#',
    },
    routelist: [
      {
        text: 'Operations',
        icon: 'description',
        routeId: 'Operations.List',
      },
      {
        text: 'Submissions',
        icon: 'filter_none_filled',
        routeId: 'Submissions.List',
      },
      {
        text: 'Sandbox',
        icon: 'fas fa-wrench',
        url: '/formly-sandbox',
      },
    ],
    buttonlist: '',
    uploaderoffline: [
      {
        fileId: '2eb58c17-3b7b-4cdd-a306-e11dbe4d44db',
        name: '',
        type: '',
        size: 0,
      },
    ],
    datetimeview: {
      dateTime: '2022-03-01T00:00:00Z',
      timeZoneId: '79DFDF27-99C9-4BD6-AAA0-3D62A4AD3821',
    },
    entitysummary: {
      operationId: '12345',
      operationName: 'Operation Gallery',
      operationAlmatCardImageSmall: '12345',
    },
  };

  homeItem: MenuItem = {
    name: 'Home',
    icon: 'home',
  };

  menuItems: MenuItem[] = [];

  context = { submissionId: '1D5BB4A1-F151-495C-B6E9-86AD5AA4BC09' };

  homeUrl = '/home';
  savedModel: unknown = {};

  constructor(private router: Router, private blobService: BlobService, activatedRoute: ActivatedRoute) {
    if (activatedRoute.snapshot.queryParamMap.has('home')) {
      this.homeUrl = activatedRoute.snapshot.queryParamMap.get('home') || this.homeUrl;
    }
  }

  ngOnInit() {
    const items = this.definition?.fields?.[0]?.fieldGroup ?? [];
    this.menuItems = items.map((item: any) => item.props?.menuGroup).filter((item: any) => item);

    if (this.model) {
      deepClone<any>(this.model).then((clone) => {
        this.savedModel = clone;
      });
    }
  }

  menuAction(element: MenuItem): void {
    if (element.scrollId) {
      document.getElementById(element.scrollId)?.scrollIntoView();
    } else if (element.route) {
      this.downloadFile(element.route);
    }
  }

  navigateToHome(): void {
    this.router.navigateByUrl(this.homeUrl);
  }

  downloadFile(type: string): void {
    if (type) {
      this.blobService
        .getBlob(`/schema/${type}`)
        .pipe(take(1))
        .subscribe((blob: Blob) => this.blobService.downloadBlob(blob, `mapping.${type}`));
    }
  }

  executeAction(action?: string) {
    if (action === 'saveModel') {
      deepClone<any>(this.model).then((clone) => {
        this.savedModel = clone;
      });
    } else if (action === 'resetModel') {
      deepClone<any>(this.savedModel).then((clone) => {
        this.model = clone;
      });
    }
  }
}
