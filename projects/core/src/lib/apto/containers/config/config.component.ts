import { Observable } from 'rxjs';
import { SiteConfig, StateService } from '@nimbus/core/src/lib/core';
import { Component } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'ni-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss'],
})
export class ConfigComponent {
  exportUrl?: string;
  options = {
    allowedExtensions: '.json',
    asyncSettings: {
      saveUrl: '',
      removeUrl: 'api/v2/file/delete',
      chunkSize: 104857600,
      retryCount: 5,
      retryAfterDelay: 3000,
    },
    autoUpload: true,
    locale: '',
    maxFileSize: 9007199254740991,
    minFileSize: 0,
    multiple: true,
    sequentialUpload: false,
    showFileList: true,
    template: '',
  };

  upgradeMessage$: Observable<string | undefined>;

  constructor(siteConfig: SiteConfig, private stateService: StateService) {
    this.upgradeMessage$ = stateService.selectUpgradeConfigMessage();
    this.stateService
      .selectSiteId()
      .pipe(untilDestroyed(this))
      .subscribe((id) => {
        this.exportUrl = `${siteConfig.apiUrl}/siteconfiguration/export?siteId=${id}`;
      });

    this.options.asyncSettings.saveUrl = `${siteConfig.apiUrl}/siteconfiguration/import`;
  }

  upgrade() {
    this.stateService.upgradeConfig();
  }
}
