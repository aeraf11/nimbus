import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import {
  DownloadDirective,
  FileService,
  PageComponent,
  StateService,
  UploaderComponent,
} from '@nimbus/core/src/lib/core';
import { SiteConfig } from '@nimbus/core/src/lib/core';
import { MockFileService } from '../../../core/services/file.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { ConfigComponent } from './config.component';

describe('ConfigComponent', () => {
  let component: ConfigComponent;
  let fixture: ComponentFixture<ConfigComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConfigComponent, PageComponent, UploaderComponent, DownloadDirective],
      imports: [MatCardModule, MatDialogModule, HttpClientTestingModule],
      providers: [
        { provide: StateService, useClass: MockStateService },
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
        { provide: FileService, useClass: MockFileService },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
