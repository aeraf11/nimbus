import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MappingSandboxService } from './../../services/mapping-sandbox.service';
import { MockMappingSandboxService } from './../../services/mapping-sandbox.service.spec';
import { MappingSandboxComponent } from './mapping-sandbox.component';

describe('MappingSandboxComponent', () => {
  let component: MappingSandboxComponent;
  let fixture: ComponentFixture<MappingSandboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MappingSandboxComponent],
      providers: [
        {
          provide: MappingSandboxService,
          useClass: MockMappingSandboxService,
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MappingSandboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
