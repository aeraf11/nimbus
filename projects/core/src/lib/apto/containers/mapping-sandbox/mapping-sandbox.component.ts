import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { take } from 'rxjs';
import { MappingSandboxService } from '../../services';

@Component({
  templateUrl: './mapping-sandbox.component.html',
  styleUrls: ['./mapping-sandbox.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MappingSandboxComponent {
  inputMappingConfig = new UntypedFormControl('');
  inputModel = new UntypedFormControl('');
  output: unknown;

  constructor(private service: MappingSandboxService, private change: ChangeDetectorRef) {}

  getMapping(): void {
    if (this.isFormControlReady(this.inputMappingConfig) && this.isFormControlReady(this.inputModel)) {
      this.service
        .getMapping(this.inputMappingConfig.value, this.inputModel.value)
        .pipe(take(1))
        .subscribe((response) => {
          if (response) {
            this.output = JSON.parse(response);
            this.change.markForCheck();
          }
        });
    }
  }

  private isFormControlReady(control: UntypedFormControl): boolean {
    return control.valid && control.value;
  }
}
