export const CardListWrapperDoc = `
<h3>Template Options</h3>
<ul>
  <li>cardListLabel: string</li>
  <li>cardListLabelIcon: string</li>
  <li>cardListCountIcon: string</li>
  <li>cardListIsFilterable: boolean</li>
  <li>ariaLabel: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  cardListCountIcon: 'filter'
}</pre>
`;
