import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-messages-type',
  templateUrl: './messages-type.component.html',
  styleUrls: ['./messages-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessagesTypeComponent extends BaseViewTypeComponent {
  override defaultOptions = {
    props: {
      listName: 'EntityConversationItems',
      sortColumn: 'lastUpdatedDate',
      noResultsText: 'No messages found',
      title: 'Messages',
      icon: 'chat',
      columnDefinitions: [
        {
          id: 'entityConversationItemId',
        },
        {
          id: 'lastViewed',
        },
        {
          id: 'messageHtml',
        },
        {
          id: 'photoId',
        },
        {
          id: 'isRead',
        },
      ],
      filterColumns: [],
      showPaging: false,
      pageSize: -1,
      idColumn: 'entityConversationItemId',
      itemTemplate: {
        type: 'message',
      },
      itemsUi: {
        columnCounts: 1,
        gap: '0',
      },
      fullWidthContent: 0,
      cardWrapper: true,
      allowAddMessage: true,
    },
  };

  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
