import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import {
  ChatService,
  FormlyTestComponent,
  ListFilterService,
  ListService,
  OptionsService,
  StateService,
  buildFormlyTestModule,
} from '@nimbus/core/src/lib/core';
import { MessagesTypeComponent } from './messages-type.component';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { MockOptionsService } from '../../../core/services/options-service/options.service.spec';
import { MockListService } from '../../../core/services/list.service.spec';
import { MockChatService } from '../../../core/services/chats.service.spec';
import { MockListFilterService } from '../../../core/services/list-filter-service/list-filter.service.spec';

describe('MessagesTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'messages',
              component: MessagesTypeComponent,
            },
          ],
        },
        imports: [MatDialogModule],
        providers: [
          {
            provide: StateService,
            useClass: MockStateService,
          },
          {
            provide: OptionsService,
            useClass: MockOptionsService,
          },
          {
            provide: ListService,
            useClass: MockListService,
          },
          {
            provide: ChatService,
            useClass: MockChatService,
          },
          {
            provide: ListFilterService,
            useClass: MockListFilterService,
          },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'messages',
        type: 'messages',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
