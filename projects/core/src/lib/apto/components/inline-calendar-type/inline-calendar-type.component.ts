import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaseTypeComponent } from './../base-type/base-type.component';

@Component({
  selector: 'ni-inline-calendar-type',
  templateUrl: './inline-calendar-type.component.html',
  styleUrls: ['./inline-calendar-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class InlineCalendarTypeComponent extends BaseTypeComponent implements OnInit {
  activeMonth: any;
  today: any;
  wrapperMaxWidth = '100%';
  calculating = true;
  activeMonthInit = false;

  override defaultOptions = {
    props: {
      todayFormat: 'dd MMMM yyyy',
      activeFormat: 'MMMM yyyy',
      isDynamicHeight: false,
    },
  };

  constructor(public override change: ChangeDetectorRef) {
    super(change);
  }

  override ngOnInit(): void {
    if (!this.props['isDynamicHeight']) {
      this.calculating = false;
      this.change.markForCheck();
    } else {
      this.calculating = false;
      this.change.markForCheck();
    }
  }
}
