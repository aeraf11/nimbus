import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AppDateTimeAdapter, StateService } from '@nimbus/core/src/lib/core';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { InlineCalendarTypeComponent } from './inline-calendar-type.component';
import { DateAdapter, MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { DateTimeAdapter, MatDatepickerModule } from '@nimbus/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Platform } from '@angular/cdk/platform';
describe('InlineCalendarTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        imports: [MatDatepickerModule, MatNativeDateModule, NoopAnimationsModule],

        config: {
          types: [
            {
              name: 'inlinecalendar',
              component: InlineCalendarTypeComponent,
            },
          ],
        },
        providers: [
          {
            provide: StateService,
            useClass: MockStateService,
          },
          {
            provide: DateTimeAdapter,
            useClass: AppDateTimeAdapter,
            deps: [MAT_DATE_LOCALE, Platform, StateService],
          },
          {
            provide: DateAdapter,
            useClass: AppDateTimeAdapter,
            deps: [MAT_DATE_LOCALE, Platform, StateService],
          },
          {
            provide: AppDateTimeAdapter,
            useClass: AppDateTimeAdapter,
            deps: [MAT_DATE_LOCALE, Platform, StateService],
          },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'inlinecalendar',
        type: 'inlinecalendar',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
