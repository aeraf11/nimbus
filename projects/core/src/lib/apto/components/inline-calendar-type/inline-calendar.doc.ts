export const InlineCalendarDoc = `
<h3>Props</h3>
<ul>
  <li>todayFormat: string (in date format e.g. dd/MMMM/yyyy)</li>
  <li>activeFormat: string (in date format e.g. dd/MMMM/yyyy)</li>
  <li>titleFontSize: string</li>
  <li>titleLineHeight: string</li>
  <li>subtitleFontSize: stirng</li>
  <li>subtitleLineHeight: boolean</li>
  <li>isDynamicHeight: boolean (for use when you want the calendar to be sized by its height, not width)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    props: {
      todayFormat: 'dd MMMM yyyy',
      activeFormat: 'MMMM yyyy',
      isDynamicHeight: false,
    },
  }</pre>
`;
