export const RouteListDoc = `
<h3>Props</h3>
<ul>
  <li>fieldArray: object</li>
  <li>bookmarkRouteId: string</li>
  <li>marginOnItems: boolean</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    noData: 'No routes',
  },
}</pre>
`;
