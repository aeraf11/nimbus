import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { StateService } from '@nimbus/core/src/lib/core';
import { BookmarkService } from './../../services/bookmark.service';
import { BaseTypeComponent } from './../base-type/base-type.component';

@UntilDestroy()
@Component({
  selector: 'ni-route-list-type',
  templateUrl: './route-list-type.component.html',
  styleUrls: ['./route-list-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouteListTypeComponent extends BaseTypeComponent implements OnInit {
  routes$: any;

  fg = new UntypedFormGroup({});

  fields = [
    {
      key: 'routes',
      type: 'repeat',
      props: {
        layoutType: 'flex',
      },
      fieldArray: undefined,
    },
  ];

  constructor(change: ChangeDetectorRef, private state: StateService, private bookmark: BookmarkService) {
    super(change);
  }

  override ngOnInit(): void {
    super.ngOnInit();

    this.loadModel();
    this.bookmark.bookmarksChanged$.pipe(untilDestroyed(this)).subscribe(() => this.loadModel());

    this.fields[0].fieldArray = this.props?.fieldArray ?? undefined;
    this.fields[0].props = {
      ...this.fields[0].props,
      ...this.props,
    };
  }

  private loadModel(): void {
    if (this.props['bookmarkRouteId']) {
      this.routes$ = this.state.selectRoutesWithPermission(
        this.bookmark.loadAllBookmarks(this.props['bookmarkRouteId']) ?? []
      );
    } else {
      this.routes$ = this.state.selectRoutesWithPermission(this.formControl.value ?? []);
    }
    this.change.markForCheck();
  }
}
