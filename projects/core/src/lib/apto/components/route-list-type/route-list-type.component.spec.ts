import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StateService } from '@nimbus/core/src/lib/core';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { RouteListTypeComponent } from './route-list-type.component';

describe('RouteListTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'routelist',
              component: RouteListTypeComponent,
            },
          ],
        },
        providers: [{ provide: StateService, useClass: MockStateService }],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'routelist',
        type: 'routelist',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
