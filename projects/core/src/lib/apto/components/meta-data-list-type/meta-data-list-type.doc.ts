export const MetaDataListDoc = `
<h3>Template Options</h3>
<ul>
  <li>containerType: 'card' | 'panel' | 'none'</li>
  <li>entityId: string</li>
  <li>entityType: number</li>
  <li>readonly: boolean</li>
  <li>ungroupedTitle: string</li>
</ul>
<h3>Default Options</h3>
<pre>{}</pre>
`;
