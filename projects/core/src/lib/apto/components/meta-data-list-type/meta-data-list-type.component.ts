import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AbstractControl, FormArray, FormControl, ValidationErrors } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { DefaultUngroupedMetaData, MetaDataItem, MetaDataService, UngroupedMetaData } from '@nimbus/core/src/lib/core';
import { deepClone } from 'deep.clone';
import { BehaviorSubject, Subscription } from 'rxjs';
import { BaseTypeComponent } from './../base-type/base-type.component';

@UntilDestroy()
@Component({
  selector: 'ni-meta-data-list-type',
  templateUrl: './meta-data-list-type.component.html',
  styleUrls: ['./meta-data-list-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MetaDataListTypeComponent extends BaseTypeComponent implements OnInit {
  entityTypeId?: number;
  entityId?: string;
  conditionalValue?: string;

  formArray?: FormArray<FormControl<MetaDataItem>>;
  formArray$: BehaviorSubject<FormArray<FormControl<MetaDataItem>>> = new BehaviorSubject(
    new FormArray<FormControl<MetaDataItem>>([])
  );

  formArraySub?: Subscription;

  formModel?: unknown;
  formModel$?: BehaviorSubject<unknown> = new BehaviorSubject<unknown>({});

  ungrouped$: BehaviorSubject<UngroupedMetaData> = new BehaviorSubject<UngroupedMetaData>(DefaultUngroupedMetaData);
  defaultUngrouped = DefaultUngroupedMetaData;

  entityMetaDataItems: MetaDataItem[] = [];
  buildingEntityMetaData = false;
  conditionalMetaDataBuildPending = false;

  constructor(public override change: ChangeDetectorRef, private service: MetaDataService) {
    super(change);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.formControl.addValidators([(control) => this.localFormArrayRelayValidator(control)]);
    this.assignProps();

    if (this.formControl.value) {
      this.setMetaDataItems(this.formControl.value);
    } else {
      this.buildEntityMetaData();
      if (this.conditionalValue) {
        this.buildConditionalMetaData();
      }
    }

    this.options?.formState?.rootModelChanged?.pipe(untilDestroyed(this))?.subscribe(() => {
      deepClone(this.model).then((cloneModel) => this.formModel$?.next(cloneModel));
    });

    this.field.options?.fieldChanges?.pipe(untilDestroyed(this)).subscribe((changes) => {
      if (['props.entityType', 'props.entityId'].includes(changes.property)) {
        this.assignProps();
        this.buildEntityMetaData();
      } else if (['props.conditionalType', 'props.conditionalValue'].includes('' + changes.property)) {
        this.assignProps();
        this.buildConditionalMetaData();
      }
      if (changes.field.type === 'metadatalist' && changes.type === 'valueChanges' && Array.isArray(changes.value)) {
        this.ungrouped$.next({
          buildGroups: false,
          items: changes.value,
        });
      }
    });
  }

  assignProps() {
    this.entityTypeId = this.props['entityType'];
    this.entityId = this.props['entityId'];
    this.conditionalValue = this.props['conditionalValue'];
  }

  buildEntityMetaData() {
    if (this.entityTypeId) {
      this.buildingEntityMetaData = true;
      this.service.getMetaData(this.entityTypeId, this.entityId, !this.props['readonly']).subscribe((metaData) => {
        this.entityMetaDataItems = metaData;
        this.setMetaDataItems(metaData);

        this.buildingEntityMetaData = false;
        if (this.conditionalMetaDataBuildPending) {
          this.conditionalMetaDataBuildPending = false;
          this.buildConditionalMetaData();
        }
      });
      this.change.markForCheck();
    }
  }

  buildConditionalMetaData() {
    if (this.entityTypeId && this.conditionalValue) {
      if (this.buildingEntityMetaData) {
        this.conditionalMetaDataBuildPending = true;
        return;
      }
      this.service
        .getConditionalMetadata(this.conditionalValue, this.entityMetaDataItems, !this.props['readonly'])
        .subscribe((metaData) => {
          this.setMetaDataItems(metaData);
        });
      this.change.markForCheck();
    }
  }

  setMetaDataItems(metaData: MetaDataItem[]) {
    this.buildFormArray(metaData);
    this.ungrouped$.next({
      buildGroups: true,
      items: metaData,
    });
  }

  buildFormArray(metaData: MetaDataItem[]) {
    if (this.formArraySub) {
      this.formArraySub.unsubscribe();
    }
    const controls = <FormControl<MetaDataItem>[]>metaData.map((item) => new FormControl<MetaDataItem>(item)) || [];
    this.formArray = new FormArray<FormControl<MetaDataItem>>(controls);
    this.formArray$?.next(this.formArray);
    this.formArraySub = this.formArray.valueChanges.subscribe(() => this.formControl.setValue(this.formArray?.value));
    this.formControl.setValue(this.formArray?.value);
  }

  localFormArrayRelayValidator(control: AbstractControl<any, any>): ValidationErrors | null {
    if (!this.formArray) return null;
    return this.formArray.valid ? null : { invalidItem: 'One of the metadata items is invalid.' };
  }
}
