import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-rich-fields-view-type',
  templateUrl: './rich-fields-view-type.component.html',
  styleUrls: ['./rich-fields-view-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RichFieldsViewTypeComponent extends BaseViewTypeComponent {
  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
