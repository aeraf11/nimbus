export const RichFieldsDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>noData: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
</ul>
<h3>Default Options</h3>
<pre>{}</pre>
`;
