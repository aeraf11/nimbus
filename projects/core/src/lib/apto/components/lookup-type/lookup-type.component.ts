import { Component, ChangeDetectionStrategy } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-lookup-type',
  templateUrl: './lookup-type.component.html',
  styleUrls: ['./lookup-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LookupTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      leafSelection: true,
      addDefaultOption: true,
      minDialogWidth: '15%',
      maxDialogWidth: '1000px',
      enableFieldLabel: false,
    },
  };
}
