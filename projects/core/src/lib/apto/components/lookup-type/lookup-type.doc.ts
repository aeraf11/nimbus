export const LookupDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>lookupKey: string</li>
  <li>leafSelection: boolean</li>
  <li>description: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>addDefaultOption: boolean</li>
  <li>outputDisplayValue: boolean (out put displayValue in model rather than guid) </li>
  <li>lookupFilters : filter lookups at branch, id or regex (format: '/foo/'  c/w hierarchical display value)</li>
  <li>topLevelOnly: boolean</li>
  <li>parentLookupId: string</li>
  <li>minDialogWidth: string</li>
  <li>maxDialogWidth: string</li>
  <li>enableFieldLabel: boolean</li>
  <li>className: string</li>
  <li>ariaLabel: string</li>
  <li>choices: { fallback: boolean; items: MetaDataChoice[] } - some meta data lookups dont exist as lookup so fallback to meta data Choices</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    leafSelection: true,
    addDefaultOption: true,
    minDialogWidth: '15%',
    maxDialogWidth: '1000px',
    enableFieldLabel: false
  },
}</pre>
`;
