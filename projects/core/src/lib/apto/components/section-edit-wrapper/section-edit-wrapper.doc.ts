export const SectionEditWrapperDoc = `
<h3>Template Options</h3>
<ul>
  <li>alwaysShowLabel: boolean</li>
  <li>sectionEditMarginTop: string</li>
  <li>sectionEditMarginBottom: string</li>
  <li>sectionEditEditIcon: string</li>
  <li>sectionEditSaveIcon: string</li>
  <li>sectionEditCancelIcon: string</li>
  <li>sectionEditEditText: string</li>
  <li>sectionEditSaveText: string</li>
  <li>sectionEditCancelText: string</li>
  <li>sectionEditSaveAction: string (Specifies name of action to be called when saving)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  sectionEditMarginTop: '-1.5rem',
  sectionEditMarginBottom: '1rem',
  sectionEditEditIcon: 'edit',
  sectionEditSaveIcon: 'done',
  sectionEditCancelIcon: 'cancel',
  sectionEditEditText: 'Edit Settings',
  sectionEditSaveText: 'Save',
  sectionEditCancelText: 'Cancel',
}</pre>
`;
