import { Component } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
  selector: 'ni-section-edit-wrapper',
  templateUrl: './section-edit-wrapper.component.html',
  styleUrls: ['./section-edit-wrapper.component.scss'],
})
export class SectionEditWrapperComponent extends FieldWrapper {
  defaults = {
    sectionEditCancelIcon: 'fas fa-xmark',
    sectionEditCancelText: 'Cancel',
    sectionEditEditIcon: 'far fa-pencil',
    sectionEditEditText: 'Edit Settings',
    sectionEditMarginBottom: '1rem',
    sectionEditMarginTop: '-1.5rem',
    sectionEditSaveIcon: 'fas fa-check',
    sectionEditSaveText: 'Save',
  };

  isEditMode = false;

  constructor() {
    super();
  }

  onSave() {
    this.isEditMode = false;
    if (this.props['sectionEditSaveAction']) {
      this.formState.executeAction(this.props['sectionEditSaveAction'], this.model);
    }
    this.formState.executeAction('saveModel');
  }

  onEdit() {
    this.isEditMode = true;
  }

  onCancel() {
    this.isEditMode = false;
    this.formState.executeAction('resetModel');
  }
}
