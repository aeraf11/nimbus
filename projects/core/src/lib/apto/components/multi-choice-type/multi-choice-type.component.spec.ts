import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { StateService } from '@nimbus/core/src/lib/core';
import { ListService } from '../../../core/services/list.service';
import { MockListService } from '../../../core/services/list.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { MultiChoiceTypeComponent } from './multi-choice-type.component';

describe('MultiChoiceTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'multichoice',
              component: MultiChoiceTypeComponent,
            },
          ],
        },
        imports: [MatDialogModule, MatAutocompleteModule, MatChipsModule, MatFormFieldModule, MatInputModule],
        providers: [
          { provide: StateService, useClass: MockStateService },
          { provide: ListService, useClass: MockListService },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'multichoice',
        type: 'multichoice',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
