import { Component, ChangeDetectionStrategy } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-multi-choice-type',
  templateUrl: './multi-choice-type.component.html',
  styleUrls: ['./multi-choice-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MultiChoiceTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      appearance: 'outline',
      selectable: true,
      removable: true,
      addCustom: false,
      enableTree: 'auto',
      newItemLabel: 'Add new item...',
      leafSelection: true,
      outputDisplayValue: false,
    },
  };
}
