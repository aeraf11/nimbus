export const MultiChoiceDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>lookupKey: string</li>
  <li>addCustom: boolean</li>
  <li>enableTree: string</li>
  <li>leafSelection: boolean</li>
  <li>description: string</li>
  <li>placeholder: string/li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>appearance: string ("outline" | "legacy" | "fill" | "standard")</li>
  <li>selectable: boolean</li>
  <li>removable: boolean</li>
  <li>newItemLabel: string</li>
  <li>parentIdProp: string (hierarchical look up parent node)</li>
  <li>ariaLabel: string</li>
  <li>outputDisplayValue: boolean</li>
  <li>parentLookupId: string</li>
  <li>lookupFilters: string[]</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    appearance: 'outline',
    selectable: true,
    removable: true,
    addCustom: false,
    enableTree: 'auto',
    newItemLabel: 'Add new item...',
    leafSelection: true,
    outputDisplayValue: false
  },
}</pre>
`;
