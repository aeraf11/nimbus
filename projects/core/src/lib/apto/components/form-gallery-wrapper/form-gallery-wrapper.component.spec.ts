import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { FormGalleryWrapperComponent } from './form-gallery-wrapper.component';

describe('FormGalleryWrapperComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          wrappers: [
            {
              name: 'formgallerywrapper',
              component: FormGalleryWrapperComponent,
            },
          ],
        },
        imports: [MatSnackBarModule],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'formgallerywrapper',
        wrappers: ['formgallerywrapper'],
        fieldGroup: [
          {
            template: 'test',
          },
        ],
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
