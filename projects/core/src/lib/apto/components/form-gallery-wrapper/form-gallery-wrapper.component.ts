import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';
import { SnackMessageComponent, SnackMessageType } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ni-form-gallery-wrapper',
  templateUrl: './form-gallery-wrapper.component.html',
  styleUrls: ['./form-gallery-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormGalleryWrapperComponent extends FieldWrapper implements OnInit {
  active: 'component' | 'json' | 'doc' = 'component';
  codeObject: any;
  private readonly nonRequiredProps = ['description', 'focus', 'disabled', 'placeholder'];

  constructor(private snackbar: MatSnackBar) {
    super();
  }

  ngOnInit() {
    this.codeObject = this.getFieldGroup(this.field.fieldGroup)[0];
  }

  viewComponent(event: any) {
    event.preventDefault();
    this.active = 'component';
  }

  viewJSON(event: any) {
    event.preventDefault();
    this.active = 'json';
  }

  viewDoc(event: any) {
    event.preventDefault();
    this.active = 'doc';
  }

  copyToClipboard(event: any, text: any) {
    event.preventDefault();
    navigator.clipboard.writeText(text).then(
      () => {
        this.snackbar.openFromComponent(SnackMessageComponent, {
          duration: 3000,
          data: { type: SnackMessageType.Info, message: `Copied to clipboard.` },
        });
      },
      (err) => {
        this.snackbar.openFromComponent(SnackMessageComponent, {
          duration: 3000,
          data: { type: SnackMessageType.Error, message: `Could not copy JSON. ${err}` },
        });
      }
    );
  }

  getFieldGroup(obj: any) {
    if (obj && obj.length < 1) return [];

    const groups: any = [];

    obj.forEach((group: any) => {
      const fieldGroup: any = {
        key: group.key,
        type: group.type,
        props: this.cleanProps(group.props),
      };
      if (group && group.wrappers && group.wrappers.length > 0) {
        fieldGroup.wrappers = group.wrappers;
      }
      if (group.fieldGroup) {
        fieldGroup.fieldGroup = this.getFieldGroup(group.fieldGroup);
      }
      groups.push(this.cleanFieldGroup(fieldGroup));
    });

    return groups;
  }

  cleanFieldGroup(fieldGroup: any) {
    const fg = { ...fieldGroup };
    if (fieldGroup.type === 'formly-group') {
      delete fg.type;
    }
    return fg;
  }

  cleanProps(props: any) {
    const result = { ...props };
    delete result['_flatOptions'];
    return this.removeUnecessaryProps(result);
  }

  removeUnecessaryProps(props: any) {
    const result = { ...props };
    this.nonRequiredProps.forEach((prop: string) => delete result[prop]);
    return result;
  }
}
