import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'ni-input-type',
  templateUrl: './input-type.component.html',
  styleUrls: ['./input-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InputTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      appearance: 'outline',
      //For reasons unknown, autocomplete needs value flipped in html so true == disabled
      enableAutoComplete: false,
    },
  };

  uniqueName = '';

  override ngOnInit() {
    super.ngOnInit();
    this.uniqueName = this.getUniqueName();
  }

  getUniqueName(): string {
    /*A throw away name is needed for inputs on Chrome. It's quite tricky to
      remove both autofills with a consistent method since Chrome can choose to ignore autofill off...
      https://stackoverflow.com/questions/12374442/chrome-ignores-autocomplete-off#:~:text=Chrome%20respects%20autocomplete%3Doff%20only,id%3D468153%20for%20more%20details.

      By giving a unique name to each input each time, Chrome seems to 'forget' you have previously entered any text.
      This way also still allows the configurable template option rather than the readonly method suggested
      elsewhere which would not...
    */
    return uuid();
  }

  get type() {
    return this.props['type'] || 'text';
  }
}
