import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { InputTypeComponent } from './input-type.component';

describe('InputTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormlyTestComponent, InputTypeComponent],
      imports: [
        ReactiveFormsModule,
        FormlyModule.forRoot({
          types: [
            {
              name: 'input',
              component: InputTypeComponent,
            },
          ],
        }),
        MatFormFieldModule,
        MatInputModule,
        NoopAnimationsModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
  });

  it('should create', () => {
    const componentInstance = fixture.componentInstance;
    componentInstance.fields = [
      {
        key: 'input',
        type: 'input',
      },
    ];
    fixture.detectChanges();
    expect(componentInstance).toBeTruthy();
  });
});
