export const InputDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>placeholder: string</li>
  <li>description: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>appearance: string ("outline" | "legacy" | "fill" | "standard")</li>
  <li>enableAutoComplete: boolean</li>
  <li>ariaLabel: string</li>
  <li>patternValidationPattern: string (Regex pattern to use with the 'pattern' validator)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    appearance: 'outline',
    enableAutoComplete: false
  },
}</pre>
`;

export const ReadonlyDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>placeholder: string</li>
  <li>description: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>appearance: string ("outline" | "legacy" | "fill" | "standard")</li>
  <li>enableAutoComplete: boolean</li>
  <li>patternValidationPattern: string (Regex pattern to use with the 'pattern' validator)</li>
  </ul>
<h3>Default Options</h3>
<pre>{
  props: {
    appearance: 'outline',
    enableAutoComplete: false,
    readonly: true
  },
}</pre>
`;
