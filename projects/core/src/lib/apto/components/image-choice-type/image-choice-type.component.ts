import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-image-choice-type',
  templateUrl: './image-choice-type.component.html',
  styleUrls: ['./image-choice-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageChoiceTypeComponent extends BaseTypeComponent {
  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
