export const RichTextDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>readOnly: boolean</li>
  <li>tools: string[]</li>
  <li>height: string</li>
  <li>width: string</li>
  <li>enableResize: boolean</li>
  <li>showCharCount: boolean</li>
  <li>ariaLabel: string</li>
  <li>toolBarType: Expand or MulitLine</li>
</ul>
<h3>Default Options</h3>
<pre>{}</pre>
<h3>Tools</h3>
<ul>
  <li>Undo</li>
  <li>Redo</li>
  <li>|</li>
  <li>Bold</li>
  <li>Italic</li>
  <li>Underline</li>
  <li>StrikeThrough</li>
  <li>|</li>
  <li>FontName</li>
  <li>FontSize</li>
  <li>FontColor</li>
  <li>BackgroundColor</li>
  <li>|</li>
  <li>SubScript</li>
  <li>SuperScript</li>
  <li>|</li>
  <li>LowerCase</li>
  <li>UpperCase</li>
  <li>|</li>
  <li>Formats</li>
  <li>Alignments</li>
  <li>|</li>
  <li>OrderedList</li>
  <li>UnorderedList</li>
  <li>Indent</li>
  <li>Outdent</li>
  <li>|</li>
  <li>CreateLink</li>,
  <li>Image</li>
  <li>|</li>
  <li>ClearFormat</li>
  <li>Print</li>
  <li>SourceCode</li>
  <li>|</li>
  <li>FullScreen</li>
</ul>
`;
