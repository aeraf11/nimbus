import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-rich-text-type',
  templateUrl: './rich-text-type.component.html',
  styleUrls: ['./rich-text-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RichTextTypeComponent extends BaseTypeComponent {
  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
