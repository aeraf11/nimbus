import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogFormAction, StateService, utils } from '@nimbus/core/src/lib/core';
import deepClone from 'deep.clone';
import { take } from 'rxjs';
import { DialogFormData, DialogFormResult, EditorMode } from '@nimbus/core/src/lib/core';
import { BaseTypeComponent } from '../base-type/base-type.component';
import { DialogFormComponent } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ni-entity-add-type',
  templateUrl: './entity-add-type.component.html',
  styleUrls: ['./entity-add-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntityAddTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      entities: [],
    },
  };

  entities: any[] = [];

  private defaultDialogConfig: Partial<DialogFormData> = {
    model: {},
    config: {
      enableDelete: false,
      enableEdit: false,
      isChildForm: true,
    },
    editorMode: EditorMode.Add,
    disabled: false,
    buttonType: 'stroked',
    formClass: 'compact-form',
  };

  constructor(change: ChangeDetectorRef, private dialog: MatDialog, private state: StateService) {
    super(change);
  }

  override ngOnInit(): void {
    super.ngOnInit();

    this.entities = this.props['entities'] || [];
    this.field.options?.fieldChanges?.subscribe((changes) => {
      if (changes.type === 'expressionChanges') {
        this.entities = this.props['entities']?.map((entity: any) => {
          if (entity.text === changes.property) {
            entity.count = changes.value;
          }
          return entity;
        });
      }
    });
  }

  openDialog(entity: any): void {
    this.state
      .selectForm(entity.formId)
      .pipe(take(1))
      .subscribe((configs) => {
        if (configs) {
          deepClone(this.defaultDialogConfig).then((defaultConfig) => {
            const data = utils.mergeDeep(defaultConfig, entity.dialogConfig ?? {}, {
              fieldConfig: configs?.fields ?? [],
            });

            const dialogConfig: MatDialogConfig<DialogFormData> = { data, width: '100%', maxWidth: '800px' };
            const dialogRef = this.dialog.open(DialogFormComponent, dialogConfig);

            dialogRef.afterClosed().subscribe((result: DialogFormResult) => {
              switch (result.action) {
                case DialogFormAction.Add:
                  this.formControl.setValue([...(this.value || []), { ...result.model, entityType: entity.type }]);
              }
            });
          });
        } else {
          console.warn(`There is no form with the id: ${entity.formId}`);
        }
      });
  }
}
