import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-unidentified-subject-detail-type',
  templateUrl: './unidentified-subject-detail-type.component.html',
  styleUrls: ['./unidentified-subject-detail-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnidentifiedSubjectDetailTypeComponent extends BaseTypeComponent implements OnInit {
  constructor(change: ChangeDetectorRef) {
    super(change);
  }

  fieldConfigs = [];

  override ngOnInit(): void {
    super.ngOnInit();
  }
}
