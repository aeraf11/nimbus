import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnidentifiedSubjectDetailTypeComponent } from './unidentified-subject-detail-type.component';
import { FormlyTestComponent, LookupService, OptionsService, StateService, buildFormlyTestModule } from '../../../core';
import { MockOptionsService } from '../../../core/services/options-service/options.service.spec';
import { MockLookupService } from '../../../core/services/lookup.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { MatDialogModule } from '@angular/material/dialog';

describe('UnidentifiedSubjectDetailTypeComponent', () => {
  let component: FormlyTestComponent;
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'unidentifiedsubjectdetail',
              component: UnidentifiedSubjectDetailTypeComponent,
            },
          ],
        },
        imports: [MatDialogModule],
        providers: [
          { provide: OptionsService, useClass: MockOptionsService },
          { provide: LookupService, useClass: MockLookupService },
          { provide: StateService, useClass: MockStateService },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'unidentifiedsubjectdetail',
        type: 'unidentifiedsubjectdetail',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
