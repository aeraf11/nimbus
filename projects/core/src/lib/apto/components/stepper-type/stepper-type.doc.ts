export const StepperDoc = `
<h3>Props</h3>
<ul>
  <li>label: string</li>
  <li>isLinear: boolean</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    isLinear: false,
  },
}</pre>
`;
