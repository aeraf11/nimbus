import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewEncapsulation } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { BaseTypeComponent } from '../base-type/base-type.component';

@UntilDestroy()
@Component({
  selector: 'ni-stepper-type',
  templateUrl: './stepper-type.component.html',
  styleUrls: ['./stepper-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class StepperTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      isLinear: false,
    },
  };

  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
