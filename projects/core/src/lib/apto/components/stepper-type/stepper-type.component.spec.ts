import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatStepperModule } from '@angular/material/stepper';
import { FormlyModule } from '@ngx-formly/core';
import { StepperTypeComponent } from './stepper-type.component';
import { FormlyTestComponent } from '../../../core/formly/test/formly-test.component';

describe('StepperTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatStepperModule,
        FormlyModule.forRoot({
          types: [
            {
              name: 'stepper',
              component: StepperTypeComponent,
            },
          ],
        }),
      ],
      declarations: [FormlyTestComponent, StepperTypeComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    fixture.componentInstance.fields = [
      {
        key: 'stepper',
        type: 'stepper',
      },
    ];
  });

  it('should create', () => {
    // Arrange
    const componentInstance = fixture.componentInstance;

    // Act
    fixture.detectChanges();

    // Assert
    expect(componentInstance).toBeTruthy();
  });
});
