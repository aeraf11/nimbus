import { animate, state, style, transition, trigger } from '@angular/animations';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FieldWrapper, FormlyFieldConfig } from '@ngx-formly/core';
import { MediaService, StateService } from '@nimbus/core/src/lib/core';
import { wrapGrid } from 'animate-css-grid';
import { distinctUntilChanged, map, Observable, of, Subject } from 'rxjs';

@UntilDestroy()
@Component({
  selector: 'ni-grid-wrapper',
  templateUrl: './grid-wrapper.component.html',
  styleUrls: ['./grid-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('expandCollapse', [
      state(
        'expanded',
        style({
          height: '100%',
          width: '100%',
          top: '0',
          left: '0',
        })
      ),
      state(
        'collapsed',
        style({
          height: '{{height}}',
          width: '{{width}}',
          top: '{{top}}',
          left: '{{left}}',
        }),
        { params: { width: '*', height: '*', top: '*', left: '*' } }
      ),
      transition('expanded => collapsed', [animate('400ms')]),
      transition('collapsed => expanded', [animate('400ms')]),
      transition('void => *', animate(0)),
    ]),
  ],
})
export class GridWrapperComponent extends FieldWrapper implements OnInit, AfterViewInit {
  @ViewChildren('sections', { read: ElementRef }) sections?: QueryList<ElementRef>;
  public defaults = {
    gridAreas: 'top | left | right | bottom, top top | left right | bottom bottom',
    gridGap: '16px',
    expandable: false,
    gridExpandContextKeys: [],
  };

  animating = false;
  allAreas = '';
  allAreas$: Subject<string> = new Subject();
  areas$: Observable<string> = of('');
  rows$: Observable<number> = of(1);
  cols$: Observable<number> = of(1);
  isExpandable = false;
  gridExpandContextKeys: string[] = [];

  itemStateMap: Map<FormlyFieldConfig, 'collapsed' | 'expanded'> = new Map();
  itemDimensionsMap: Map<FormlyFieldConfig, ItemDimensions> = new Map();

  @ViewChild('grid') grid?: ElementRef;

  @Output() animatingChanged = new EventEmitter<boolean>();

  defaultEasing = 'easeInOut';
  defaultStagger = 0;
  defaultDuration = 300;
  gridWrapper:
    | {
        unwrapGrid: () => void;
        forceGridAnimation: () => void;
      }
    | undefined = undefined;

  constructor(
    private mediaService: MediaService,
    private change: ChangeDetectorRef,
    private stateService: StateService
  ) {
    super();
  }

  ngOnInit(): void {
    this.areas$ = this.mediaService.getBreakpointValueObservable(this.allAreas$).pipe(distinctUntilChanged());
    this.rows$ = this.areas$.pipe(map((areas) => this.getRowsByArea(areas)));
    this.cols$ = this.areas$.pipe(map((areas) => this.getColumnsByArea(areas)));

    this.allAreas = this.props['gridAreas'] ?? this.defaults.gridAreas;

    this.formState?.rootModelChanged?.pipe(untilDestroyed(this)).subscribe(() => {
      if (this.props['gridAreas'] && this.props['gridAreas'] !== this.allAreas) {
        this.allAreas$.next(this.props['gridAreas']);
        this.allAreas = this.props['gridAreas'];
        const elem = this.grid?.nativeElement;
        if (elem) {
          if (elem.classList.contains('animate')) {
            elem.classList.remove('animate');
          } else {
            elem.classList.add('animate');
          }
        }
      }
    });

    this.gridExpandContextKeys = this.props['gridExpandContextKeys'] ?? this.defaults.gridExpandContextKeys;
    if (this.gridExpandContextKeys.length > 0) {
      this.formState?.contextChanged?.pipe(distinctUntilChanged(), untilDestroyed(this)).subscribe((context: any) => {
        if (context === 'expandGrid') {
          this.gridExpandContextKeys.forEach((key) => {
            const item = this.field.fieldGroup?.find((group) => group.props?.['expandKey'] === key);
            const index = this.field.fieldGroup?.findIndex((group) => group.props?.['expandKey'] === key) ?? -1;

            if (item && index !== -1 && this.itemStateMap.get(item) === 'collapsed') {
              this.setExpandContext(index, item);
            }
          });
        }
      });
    }
  }

  ngAfterViewInit(): void {
    this.setWrapGrid();
    this.allAreas$.next(this.allAreas);

    this.isExpandable = this.props['childrenExpandable'];
    this.field.fieldGroup?.forEach((fieldGroup) => {
      this.itemStateMap.set(fieldGroup, 'collapsed');
    });
  }

  setWrapGrid(wrap = true): void {
    if (wrap) {
      if (this.grid && this.props['gridAnimationEnabled']) {
        this.gridWrapper = wrapGrid(this.grid.nativeElement, {
          duration: this.props['gridAnimationDuration'] || this.defaultDuration,
          easing: this.props['gridAnimationEasing'] || this.defaultEasing,
          stagger: this.props['gridAnimationStagger'] || this.defaultStagger,
          onStart: () => this.animatingChanged.emit(true),
          onEnd: () => this.animatingChanged.emit(false),
        });
      }
    } else if (this.gridWrapper) {
      this.gridWrapper.unwrapGrid();
    }
  }

  setExpandContext(i: number, item: FormlyFieldConfig): void {
    this.setWrapGrid(false);

    const state = this.itemStateMap.get(item);
    if (state === 'collapsed') {
      const element = this.sections?.toArray()[i].nativeElement;
      this.itemDimensionsMap.set(item, {
        dimensions: {
          height: element.offsetHeight + 'px',
          width: element.offsetWidth + 'px',
          left: element.offsetLeft + 'px',
          top: element.offsetTop + 'px',
        },
        gridArea: item.props?.['gridArea'],
      });
      const dimensions = this.itemDimensionsMap.get(item);
      element.style.height = dimensions?.dimensions.height;
      element.style.width = dimensions?.dimensions.width;
      element.style.left = dimensions?.dimensions.left;
      element.style.top = dimensions?.dimensions.top;
      element.style.gridArea = null;
      element.style.position = 'absolute';
      element.style.zIndex = '3';
    }

    setTimeout(() => {
      this.toggleState(item);

      const expandKey = item.props?.['expandKey'];
      if (expandKey) {
        this.stateService.updateDynamicRouteModel({
          ...(this.formState.rootModel || {}),
          expandedPanel: this.itemStateMap.get(item) === 'expanded' ? item.props?.['expandKey'] : null,
        });
      }

      this.formState.context['isGridExpanded'] = this.itemStateMap.get(item) === 'expanded';
      this.formState.contextChanged.next('isGridExpanded');

      this.change.markForCheck();
    }, 0);
  }

  toggleState(item: FormlyFieldConfig): void {
    this.animatingChanged.emit(true);
    if (this.itemStateMap.get(item) === 'expanded') {
      this.itemStateMap.set(item, 'collapsed');
    } else {
      this.itemStateMap.set(item, 'expanded');
    }
  }

  getColumnsByArea(area: string | null): number {
    let columnCount = 0;
    if (area) {
      const rows: string[] = area.split('|').map((r: string) => r.trim());
      rows.forEach((r: string) => {
        const length = r.split(' ').length;
        if (length > columnCount) {
          columnCount = length;
        }
      });
    }
    return columnCount;
  }

  getRowsByArea(area: string | null): number {
    let rowCount = 0;
    if (area) {
      const rows: string[] = area.split('|');
      rowCount = rows.length;
    }
    return rowCount;
  }

  onAnimationComplete(index: number, item: FormlyFieldConfig): void {
    const dimensions = this.itemDimensionsMap.get(item);
    if (dimensions) {
      this.animatingChanged.emit(false);

      if (this.itemStateMap.get(item) === 'collapsed') {
        const element = this.sections?.toArray()[index].nativeElement;

        setTimeout(() => {
          element.style.height = null;
          element.style.width = null;
          element.style.left = null;
          element.style.top = null;
          element.style.position = null;
          element.style.zIndex = null;
          element.style.gridArea = dimensions.gridArea;
          this.setWrapGrid(true);
          this.change.markForCheck();
        }, 0);
      }
    }
  }
}

export interface ItemDimensions {
  dimensions: {
    height: string;
    width: string;
    top: string;
    left: string;
  };
  gridArea: 'expanded' | 'collapsed';
}
