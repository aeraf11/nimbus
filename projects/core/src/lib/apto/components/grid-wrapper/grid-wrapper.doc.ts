export const GridWrapperDoc = `
<h3>Template Options</h3>
<ul>
  <li>gridAreas: string</li>
  <li>gridGap: string ("{number}px")
  <li>gridEvenRowHeights: boolean (whether to have even/equal height rows in the grid)
  <li>gridAnimationEnabled: boolean
  <li>gridAnimationDuration: number (how long to animate over in ms)
  <li>gridAnimationEasing: 'linear' | 'easeInOut' | 'easeIn' | 'easeOut' | 'circIn' | 'circOut' | 'circInOut' | 'backIn' | 'backOut' | 'backInOut' | 'anticipate' (easing mode for animation)
  <li>gridAnimationStagger: number (stagger items movement within the grid)
  <li>evenRowHeights: boolean (whether to have even/equal height rows in the grid)
  <li>gridGap: string ("{number}px")</li>
  <li>childrenExpandable: boolean</li>
  <li>gridExpandContextKeys: string[]</li>
</ul>
<h3>Default Options</h3>
<pre>{
  gridAreas: 'top | left | right | bottom, top top | left right | bottom bottom',
  gridGap: '16px',
  gridEvenRowHeights: false,
  gridAnimationEnabled: false,
  gridAnimationDuration: 300,
  gridAnimationEasing: 'easeInOut',
  gridAnimationStagger: 0,
  gridExpandContextKeys: [],
}</pre>
`;
