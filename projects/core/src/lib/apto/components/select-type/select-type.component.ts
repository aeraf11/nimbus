import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';
import { ListService } from '../../../core/services/list.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'ni-select-type',
  templateUrl: './select-type.component.html',
  styleUrls: ['./select-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class SelectTypeComponent extends BaseTypeComponent implements AfterViewInit {
  private valueProp = 'id';
  private labelProp = 'displayValue';
  loadingMatches = false;
  items: any;

  override defaultOptions = {
    props: {
      appearance: 'outline',
      options: [],
      version: 1,
      modelSyncExclusions: [],
    },
  };

  constructor(private service: ListService, change: ChangeDetectorRef) {
    super(change);
  }

  ngAfterViewInit(): void {
    this.setDefaultValue();
    if (this.props['listName']) {
      if (this.props['valueProp'] && this.props['labelProp']) {
        this.valueProp = this.props['valueProp'];
        this.labelProp = this.props['labelProp'];
      }
      if (this.props['filterColumns']?.some((f: any) => f.contextKey)) {
        this.formState.contextChanged.subscribe((key: any) => {
          const contextCol = this.props['filterColumns'].find((col: any) => col.contextKey === key);
          if (contextCol) {
            const filter = this.formState.context[key];
            if (filter) {
              contextCol.stringFilter = filter;
              this.getList();
            } else {
              this.formControl.reset();
              this.change.detectChanges();
            }
          }
        });
      } else {
        this.getList();
      }
    }
  }

  setDefaultValue(): void {
    let value = this.value;
    if (typeof value === 'object') {
      const valueProp = this.valueProp ? this.valueProp : 'id';
      value = value[valueProp] ? value : undefined;
    }
    this.value = value;
  }

  getList() {
    this.loadingMatches = true;
    let resultCount = 10;
    if (this.props['resultCount']) {
      resultCount = this.props['resultCount'];
    }
    this.service
      .getTypeAheadList(
        this.props['listName'],
        '',
        this.props['sortColumn'],
        this.props['selectColumns'],
        this.props['filterColumns'],
        resultCount
      )
      .pipe(untilDestroyed(this))
      .subscribe((lists) => {
        if (this.props['addDefaultOption'] === undefined || this.props['addDefaultOption']) {
          lists.unshift(null);
        }
        this.items = lists;
        this.loadingMatches = false;
        if (!this.props['overrideFormReset']) {
          this.formControl.reset();
        }

        this.change.markForCheck();
      });
  }
}
