export const SelectDoc = `
<h3>Template Options</h3>
<h4>Common</h4>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>labelProp: string</li>
  <li>valueProp: string</li>
  <li>placeholder: string</li>
  <li>defaultValue: string[]<li>
  <li>description: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>appearance: string ("outline" | "legacy" | "fill" | "standard")</li>

</ul>
<h4>Grouping Options</h4>
<ul>
  <li>groupProp: string ( Indicates the item property to group by. <i>Defaults to 'group'</i> )</li>
</ul>

<h4>Disabling Options</h4>
<ul>
<li>disabledProp: string ( The property that indicates an option is disabled. <i>Defaults to 'disabled'</i>)</li>
</ul>

<h4>Fixed Source</h4>
<ul>
  <li>options: []</li>
  <li>valueProp: string</li>
</ul>
<h4>List Endpoint Source</h4>
<ul>
  <li>labelTemplate: string</li>
  <li>listName: string</li>
  <li>filterColumns:
    <ul>
      <li>column: string</li>
      <li>stringFilter: string</li>
      <li>constantFilter: boolean</li>
    </ul>
  </li>
  <li>selectColumns: string[]
  </li>
  <li>modelSyncExclusions: string[]</li>
  <li>enableModelSync: boolean</li>

</ul>
<h4>Version 2 Redundant Properties<h4>
<ul>
  <li>valueProp: string - N.B. In V1 this is used for lists to transform the items to an id/displayName structure, not performed in V2 as whole object is used as value.</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    appearance: 'outline',
    options: [],
    modelSyncExclusions: [],
    compareWith(o1: any, o2: any) {
      return o1 === o2;
    },
    version: 1,
  },
}</pre>
`;
