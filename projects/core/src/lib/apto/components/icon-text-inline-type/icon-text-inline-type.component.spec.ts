import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconTextInlineTypeComponent } from './icon-text-inline-type.component';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';

describe('IconTextInlineTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormlyTestComponent, IconTextInlineTypeComponent],
      imports: [
        ReactiveFormsModule,
        FormlyModule.forRoot({
          types: [
            {
              name: 'icontextinline',
              component: IconTextInlineTypeComponent,
            },
          ],
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
  });

  it('should create', () => {
    const componentInstance = fixture.componentInstance;
    componentInstance.fields = [
      {
        key: 'icontextinline',
        type: 'icontextinline',
      },
    ];
    fixture.detectChanges();
    expect(componentInstance).toBeTruthy();
  });
});
