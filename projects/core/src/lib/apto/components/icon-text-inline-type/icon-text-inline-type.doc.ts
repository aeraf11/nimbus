export const IconTextInlineDoc = `
<h3>Template Options</h3>
<ul>
  <li>colorClass: 'warning' | 'success' | 'danger' | 'info'</li>
  <li>icon: string</li>
  <li>iconSize: string</li>
  <li>text: string</li>
  <li>textSize: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  colorClass: 'warning',
  icon: 'warning',
  iconSize: '2rem',
  text: 'Warning',
  textSize: '2rem',
}</pre>
`;
