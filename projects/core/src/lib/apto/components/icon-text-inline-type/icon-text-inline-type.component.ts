import { ChangeDetectionStrategy, ChangeDetectorRef, Component } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-icon-text-inline-type',
  templateUrl: './icon-text-inline-type.component.html',
  styleUrls: ['./icon-text-inline-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconTextInlineTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      colorClass: 'warning',
      text: 'Warning',
      iconSize: '2rem',
      textSize: '2rem',
      icon: 'warning',
    },
  };

  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
