import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ChartDataService } from '@nimbus/core/src/lib/core';
import { MockChartDataService } from '../../../core/services/chart-data.service.spec';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { ChartBarTypeComponent } from './chart-bar-type.component';

describe('ChartBarTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'chartbar',
              component: ChartBarTypeComponent,
            },
          ],
        },
        imports: [RouterTestingModule],
        providers: [
          {
            provide: ChartDataService,
            useClass: MockChartDataService,
          },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'chartbar',
        type: 'chartbar',
      },
    ];
  });

  it('should create', () => {
    // Assert
    expect(component).toBeTruthy();
  });
});
