import { Component, ChangeDetectionStrategy, ViewEncapsulation, OnInit, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';
import { ChartDataService } from '../../../core/services/chart-data.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable, tap, map } from 'rxjs';
import { ChartDataItem, ChartType } from '../../../core';

@UntilDestroy()
@Component({
  selector: 'ni-chart-bar-type',
  templateUrl: './chart-bar-type.component.html',
  styleUrls: ['./chart-bar-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ChartBarTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      flexPosition: 'center center',
      direction: 'horizontal',
      width: '100%',
      height: '100%',
      title: '',
      subTitle: '',
      enableAnimation: true,
      columnWidth: 0.75,
      columnSpacing: 0,
      chartArea: {
        border: {
          width: 0,
        },
        spacing: 0,
      },
      xAxis: {
        title: '',
        lineWidth: 0,
        tickWidth: 0,
        labelFormat: '',
        visible: true,
      },
      yAxis: {
        title: '',
        lineWidth: 0,
        tickWidth: 0,
        labelFormat: '',
        visible: false,
        minimum: 0,
      },
      marker: {
        dataLabel: {
          visible: true,
          position: 'Top',
          name: '${point.value}',
          font: {
            color: 'white',
            fontWeight: 'Bold',
            size: '14px',
          },
        },
      },
      tooltip: {
        enable: true,
      },
    },
  };

  data$?: Observable<ChartDataItem[] | undefined>;
  hasData$?: Observable<boolean>;

  constructor(changes: ChangeDetectorRef, private service: ChartDataService) {
    super(changes);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.handleData();

    if (this.formControl) {
      this.formControl.valueChanges
        .pipe(
          untilDestroyed(this),
          tap(() => this.handleData())
        )
        .subscribe();
    }
  }

  handleData(): void {
    const params = this.service.getParams(this.props['params'], this.formState);
    this.data$ = this.service
      .getChartData(ChartType.Bar, this.formControl.value, this.props['dataSource'], params)
      .pipe(map((data) => data?.data));
    this.hasData$ = this.data$.pipe(map((data) => (data && data.length > 0 ? true : false)));
  }
}
