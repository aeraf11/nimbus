export const ChartBarDoc = `
<h3>Available Data Sources</h3>
<ul>
  <li>PhanerosSubmissionExhibitsByType</li>
  <li>PhanerosSubmissionExhibitsByStatus</li>
  <li>PhanerosSubmissionExhibitsByLocation</li>
  <li>PhanerosSubmissionExhibitsByPriority</li>
</ul>
<h3>Template Options</h3>
<ul>
  <li>direction: string ("horizontal" | "vertical")</li>
  <li>width: string (as pixel or percent of parent container)</li>
  <li>height: string (as pixel or percent of parent container)</li>
  <li>title: string (templated, can use \${property} to access rootModel)</li>
  <li>subTitle: string (templated, can use \${property} to access rootModel)</li>
  <li>enableAnimation: boolean</li>
  <li>columnWidth: number (between 0 and 1)</li>
  <li>columnSpacing: number (between 0 and 1)</li>
  <li>chartArea: object - see <a href="https://ej2.syncfusion.com/angular/documentation/api/chart/chartArea/" target="_blank">SyncFusion docs</a></li>
  <li>xAxis: object - see <a href="https://ej2.syncfusion.com/angular/documentation/api/chart/axis/" target="_blank">SyncFusion docs</a></li>
  <li>yAxis: object - see <a href="https://ej2.syncfusion.com/angular/documentation/api/chart/axis/" target="_blank">SyncFusion docs</a></li>
  <li>marker: object - see <a href="https://ej2.syncfusion.com/angular/documentation/api/chart/markerSettings/" target="_blank">SyncFusion docs</a></li>
  <li>tooltip: object - see <a href="https://ej2.syncfusion.com/angular/documentation/api/chart/tooltipSettings/" target="_blank">SyncFusion docs</a></li>
  <li>flexPosition: string</li>
</ul>
<h3>Props</h3>
<pre>{
  props: {
    direction: 'horizontal',
    width: '100%',
    height: '100%',
    title: '',
    subTitle: '',
    enableAnimation: true,
    columnWidth: 0.8,
    columnSpacing: 0.2,
    animation: {
      enable: false,
    },
    chartArea: {
      border: {
        width: 0,
      },
    },
    xAxis: {
      title: '',
      lineWidth: 0,
      tickWidth: 0,
      labelFormat: '',
      visible: true,
    },
    yAxis: {
      title: '',
      lineWidth: 0,
      tickWidth: 0,
      labelFormat: '',
      visible: false,
      minimum: 0
    },
    marker: {
      dataLabel: {
        visible: true,
        position: 'Top',
        name: '\${point.value}',
        font: {
          color: 'white',
          fontWeight: 'Bold',
          size: '14px',
        },
      },
    },
    tooltip: {
      enable: true,
    },
  },
}</pre>
`;
