import { BaseTypeComponent } from './../base-type/base-type.component';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'ni-base-view-type',
  template: '',
})
export class BaseViewTypeComponent extends BaseTypeComponent implements OnInit {
  public isEmpty = false;

  constructor(public override change: ChangeDetectorRef) {
    super(change);
  }

  override ngOnInit(): void {
    this.isEmpty = this.checkEmpty(this.formControl.value);
    this.formControl.valueChanges.pipe(untilDestroyed(this)).subscribe((val: any) => {
      this.isEmpty = this.checkEmpty(val);
      this.change.detectChanges();
    });
    super.ngOnInit();
  }

  checkEmpty(value: any) {
    if (!value) return true;
    return Array.isArray(value) ? this.arrayIsEmpty(value) : this.jsonIsEmpty(value);
  }

  getValue(key: any): string {
    return this.formControl.value[key];
  }

  private arrayIsEmpty(array: any[]) {
    return array?.length === 0;
  }

  private jsonIsEmpty(obj: any) {
    return !Object.values(obj).some((value) => value);
  }
}
