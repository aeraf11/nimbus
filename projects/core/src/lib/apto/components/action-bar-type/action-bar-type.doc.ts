export const ActionBarDoc = `
<h3>Template Options</h3>
<ul>
  <li>actionStyle: ActionType ("Link" | "Button")</li>
  <li>actionLayout: ActionLayout ("Row" | "Column")</li>
  <li>gap: "{number}px"</li>
  <li>buttonColor: "primary" | "accent" | "warn" | undefined</li>
  <li>dialogWidth: "{number}px" | "{number}%"</li>
  <li>ariaLabel: string</li>
  </ul>
<h3>Default Options</h3>
<pre>{
  props: {
    actionStyle: 'Button',
    actionLayout: 'Row',
    gap: '16px',
    buttonColour: 'primary',
    dialogWidth: undefined,
  },
}</pre>
`;
