import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';
import { ActionBarItem, ActionLayout, ActionStyle, ActionType } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ni-action-bar-type',
  templateUrl: './action-bar-type.component.html',
  styleUrls: ['./action-bar-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActionBarTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      actionStyle: ActionStyle.Button,
      actionLayout: ActionLayout.Row,
      gap: '16px',
      buttonColour: 'primary',
      dialogWidth: undefined,
    },
  };

  get items(): ActionBarItem[] {
    return (
      this.field.fieldGroup?.map(
        (fg) =>
          ({
            Label: fg.props?.['label'],
            Url: fg.props?.['url'],
            AriaLabel: fg.props?.['ariaLabel'],
            ActionType: fg.props?.['actionType'] ?? ActionType.Route,
            FormId: fg.props?.['formId'],
            ItemContext: fg.options?.formState?.context,
          } as ActionBarItem)
      ) ?? []
    );
  }

  constructor(change: ChangeDetectorRef) {
    super(change);
  }

  get actionLayout(): ActionLayout {
    return ActionLayout[this.props['actionLayout']] as unknown as ActionLayout;
  }

  get actionStyle(): ActionStyle {
    return ActionStyle[this.props['actionStyle']] as unknown as ActionStyle;
  }
}
