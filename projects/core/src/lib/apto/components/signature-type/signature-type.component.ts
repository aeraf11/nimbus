import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-signature-type',
  templateUrl: './signature-type.component.html',
  styleUrls: ['./signature-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignatureTypeComponent extends BaseTypeComponent {
  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
