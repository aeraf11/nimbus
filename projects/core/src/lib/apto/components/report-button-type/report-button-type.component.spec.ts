import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { ReportButtonTypeComponent } from './report-button-type.component';

describe('ReportButtonTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'reportbutton',
              component: ReportButtonTypeComponent,
            },
          ],
        },
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;
    component.fields = [
      {
        key: 'reportbutton',
        type: 'reportbutton',
      },
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    //Assert
    expect(component).toBeTruthy();
  });
});
