export const ReportButtonDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string (button label)</li>
  <li>name: string (report name to run)</li>
  <li>format: string (file format)</li>
  <li>formId: string (id of the form that will process the attach report business action)</li>
  <li>params: object (e.g. { submissionId: '20d0bb36-5543-4329-b338-6f905a464dcc' })</li>
</ul>
<h3>Default Options</h3>
<pre>{
  label: 'Run Report'
}</pre>
`;
