import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-report-button-type',
  templateUrl: './report-button-type.component.html',
  styleUrls: ['./report-button-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ReportButtonTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      label: 'Run Report',
    },
  };

  constructor(change: ChangeDetectorRef) {
    super(change);
  }

  runReport() {
    this.formState.executeAction(
      'report',
      {
        name: this.props['name'],
        format: this.props['format'] || null,
        arguments: this.getArguments(),
        context: this.formState.context,
      },
      this.props['formId']
    );
  }

  getArguments(): { parameterName: string; value: any }[] {
    if (!this.props['params']) return [];
    return Object.keys(this.props['params']).map((key) => {
      return { parameterName: key, value: this.props['params'][key] };
    });
  }
}
