import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { OptionsService, StateService } from '@nimbus/core/src/lib/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, OnInit, ChangeDetectorRef, Optional } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';
import { GridWrapperComponent } from '../grid-wrapper/grid-wrapper.component';

@UntilDestroy()
@Component({
  selector: 'ni-card-wrapper',
  templateUrl: './card-wrapper.component.html',
  styleUrls: ['./card-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('expandCollapse', [
      state(
        'expand',
        style({
          height: '*',
          padding: '*',
        })
      ),
      state(
        'collapse',
        style({
          height: '0',
          padding: '0',
        })
      ),
      transition('expand => collapse', animate('200ms ease-in-out')),
      transition('collapse => expand', animate('200ms ease-in-out')),
    ]),
  ],
})
export class CardWrapperComponent extends FieldWrapper implements OnInit {
  isCardCollapsed = false;
  animationState = 'expand';

  constructor(
    private service: OptionsService,
    private state: StateService,
    @Optional() private gridWrapper: GridWrapperComponent,
    private change: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit(): void {
    this.loadInitialCollapseState();
    if (this.props['cardDettachCDDuringAnimation'] && this.gridWrapper) {
      this.gridWrapper.animatingChanged.pipe(untilDestroyed(this)).subscribe((animating) => {
        if (animating) {
          this.change.detach();
        } else {
          this.change.reattach();
          this.change.markForCheck();
        }
      });
    }
  }

  setIsCardCollapsed(): void {
    this.isCardCollapsed = !this.isCardCollapsed;
    this.animationState = this.isCardCollapsed ? 'collapse' : 'expand';

    const collapse = this.props['cardCollapse'] ?? this.props['collapse'];
    const collapseSaveId = this.props['cardCollapseSaveId'] ?? this.props['collapseSaveId'];

    if (collapse && collapseSaveId) {
      this.state.saveCardOptions({ id: collapseSaveId, isCardCollapsed: this.isCardCollapsed });
    }
  }

  private loadInitialCollapseState(): void {
    const collapse = this.props['cardCollapse'] ?? this.props['collapse'];
    const collapseSaveId = this.props['cardCollapseSaveId'] ?? this.props['collapseSaveId'];

    if (collapse && collapseSaveId) {
      const options = this.service.loadOptions(collapseSaveId);
      if (options) {
        this.isCardCollapsed = options.isCardCollapsed;
        this.animationState = this.isCardCollapsed ? 'collapse' : 'expand';
      }
    }
  }
}
