export const CardWrapperDoc = `
<h3>Template Options</h3>
<ul>
  <li>cardLabel: string</li>
  <li>cardLabelIcon: string</li>
  <li>cardPadding: string</li>
  <li>cardAlt: boolean (alternative style card)</li>
  <li>cardEnableHoverClass: boolean</li>
  <li>cardAutoHeight: boolean</li>
  <li>cardCollapse: boolean</li>
  <li>cardCollapseSaveId: string</li>
  <li>cardDettachCDDuringAnimation: boolean (Dettaches change detection while the parent container is animating to prevent odd changes occuring part way through an animation)</li>
  <li>cardContentOverflowVisible: boolean (Overflow is "hidden" by default to account for certain flexbox/grid requirements. However when nesting cards you may want content overflow to be visible)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  cardPadding: '1rem',
  cardAlt: false,
  cardEnableHoverClass: false,
  cardAutoHeight: false,
  cardDettachCDDuringAnimation: false
  cardCollapse: false
}</pre>
`;
