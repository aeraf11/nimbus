import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-image-view-type',
  templateUrl: './image-view-type.component.html',
  styleUrls: ['./image-view-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageViewTypeComponent extends BaseViewTypeComponent implements OnInit {
  override defaultOptions = {
    props: {},
  };

  imageUrl?: string;

  constructor(change: ChangeDetectorRef) {
    super(change);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    if (!this.props['watermark']) {
      this.imageUrl = this.value ? '/api/v2/media/photo?id=' + encodeURIComponent(this.value) : this.props['imageUrl'];
    }
  }
}
