export const ImageDoc = `
<h3>Props</h3>
<ul>
  <li>width: number</li>
  <li>height: number</li>
  <li>maintainAspect: boolean</li>
  <li>imageUrl: string (You can override blobId handeling with any imageUrl)</li>
  <li>margins: string (CSS margins e.g. '10px 20px')</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    maintainAspect: true,
  },
}</pre>
`;
