import { Platform } from '@angular/cdk/platform';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { AppDateTimeAdapter, StateService } from '@nimbus/core/src/lib/core';
import { DateTimeAdapter } from '@nimbus/material';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { TimeTypeComponent } from './time-type.component';
import { FormlyTestComponent } from '../../../core/formly/test/formly-test.component';

describe('TimeTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'time',
              component: TimeTypeComponent,
            },
          ],
        },
        providers: [
          {
            provide: StateService,
            useClass: MockStateService,
          },
          {
            provide: DateTimeAdapter,
            useClass: AppDateTimeAdapter,
            deps: [MAT_DATE_LOCALE, Platform, StateService],
          },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'time',
        type: 'time',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
