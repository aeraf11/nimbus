import { Component, ChangeDetectionStrategy } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-time-type',
  templateUrl: './time-type.component.html',
  styleUrls: ['./time-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TimeTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      showSpinners: true,
      stepHours: 1,
      stepMinutes: 1,
      stepSeconds: 1,
      showSeconds: false,
      disableMinutes: false,
      showMeridian: false,
      color: 'primary',
    },
  };
}
