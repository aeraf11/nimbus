export const TimeDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>showSeconds: boolean</li>
  <li>defaultTime: object ({hours: number, minutes: number, seconds: number})</li>
  <li>description: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>showSpinners: boolean</li>
  <li>stepHours: number</li>
  <li>stepMinutes: number</li>
  <li>stepSeconds: number</li>
  <li>disableMinutes: boolean</li>
  <li>showMeridian: boolean</li>
  <li>color: string ("primary" | "accent" | "warn")</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    showSpinners: true,
    stepHours: 1,
    stepMinutes: 1,
    stepSeconds: 1,
    showSeconds: false,
    disableMinutes: false,
    showMeridian: false,
    color: 'primary',
  },
}</pre>
`;
