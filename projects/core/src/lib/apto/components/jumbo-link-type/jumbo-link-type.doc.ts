export const JumboLinkDoc = `
<h3>Propss</h3>
<ul>
  <li>fill: boolean</li>
  <li>enableBookmark: boolean</li>
  <li>bookmarkRouteId: string</li>
  <li>defaultIfEmpty: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    fill: true,
    enableBookmark: true,
    bookmarkRouteId: 'Phaneros.Home',
    defaultIfEmpty: ''
  }
}</pre>
`;
