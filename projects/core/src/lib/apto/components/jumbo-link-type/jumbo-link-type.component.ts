import { ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BookmarkService } from './../../services/bookmark.service';
import { BaseTypeComponent } from './../base-type/base-type.component';

@UntilDestroy()
@Component({
  selector: 'ni-jumbo-link-type',
  templateUrl: './jumbo-link-type.component.html',
  styleUrls: ['./jumbo-link-type.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class JumboLinkTypeComponent extends BaseTypeComponent implements OnInit {
  isBookmarkSaved = false;
  override defaultOptions = {
    props: {
      bookmarkRouteId: 'Phaneros.Home',
      defaultIfEmpty: '',
    },
  };

  constructor(change: ChangeDetectorRef, private bookmark: BookmarkService, private router: Router) {
    super(change);
  }

  override ngOnInit(): void {
    super.ngOnInit();

    if (this.props?.enableBookmark && this.props?.bookmarkRouteId) {
      this.isBookmarkSaved = this.checkIfBookmarkSaved();
      this.bookmark
        .getBookmarksChanged()
        .pipe(untilDestroyed(this))
        .subscribe(() => {
          setTimeout(() => {
            this.isBookmarkSaved = this.checkIfBookmarkSaved();
            this.change.markForCheck();
          }, 0);
        });
    }
  }

  navigateToRoute(): void {
    if (this.formControl.value.url) {
      this.router.navigateByUrl(this.formControl.value.url);
    }
  }

  private checkIfBookmarkSaved(): boolean {
    return this.bookmark.isBookmarkSaved(this.props?.bookmarkRouteId, this.formControl.value);
  }
}
