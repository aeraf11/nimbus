export const RepeatDoc = `
<h3>Template Options</h3>
<ul>
  <li>noData: string</li>
  <li>carouselControlSize: string</li>
  <li>carouselContentPosition: string</li>
  <li>itemsUi: { columnCounts: number[], gap: string}</li>
  <li>rootModelClone: string (This is the path to an array within the rootModel, relative to the root. E.g. people.addPerson - note do not prefix with 'rootModel.'. Initial values, adds, updates and deletes will all be patched through to the repeater section, but it is one-way so shared fields should really be read-only in your repeater section.</li>
  <li>ariaLabelPrev: string</li>
  <li>ariaLabelNext: string</li>
  <li>carouselDirection: 'horizontal' | 'vertical'</li>
  <li>carouselMaxHeight: string (suffix with px/em/rem e.g. '500px')</li>
  <li>carouselGroupControls: 'previous' | 'next'</li>
  <li>carouselItemCount: number</li>
  <li>carouselItemWidth: string (css value, e.g.100%)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  noData: 'No items',
  carouselControlSize: '32px',
  carouselContentPosition: 'center center',
  marginOnItems: true,
  ariaLabelPrev: 'Previous Item',
  ariaLabelNext: 'Next Item',
  carouselDirection: 'horizontal',
  carouselControlDirection: 'horizontal',
  carouselItemCount: 1,
}</pre>
`;
