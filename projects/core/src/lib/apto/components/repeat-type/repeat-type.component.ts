import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, UntypedFormControl } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FieldArrayType } from '@ngx-formly/core';
import { ItemsUi, utils } from '@nimbus/core/src/lib/core';

export interface Syncable {
  _id: number;
}

@UntilDestroy()
@Component({
  selector: 'ni-repeat-type',
  templateUrl: './repeat-type.component.html',
  styleUrls: ['./repeat-type.component.scss'],
})
export class RepeatTypeComponent extends FieldArrayType implements OnInit {
  public defaults = {
    noData: 'No items',
    carouselControlSize: '32px',
    carouselContentPosition: 'center center',
    marginOnItems: true,
    ariaLabelPrev: 'Previous Item',
    ariaLabelNext: 'Next Item',
    carouselDirection: 'horizontal',
    carouselControlDirection: 'horizontal',
    carouselItemCount: 1,
  };

  rootModelExpr?: (rootModel: unknown) => unknown;

  sm = 1;
  md = 1;
  lg = 1;

  gap = '1rem';

  formControlMap: Map<number, AbstractControl> = new Map();

  @ViewChild('slides') private slides?: ElementRef;

  paddingTop = '0';
  paddingRight = '0';
  paddingBottom = '0';
  paddingLeft = '0';

  slideHeight = 100;
  slideHeightCss = '';

  constructor(private change: ChangeDetectorRef, private element: ElementRef) {
    super();
  }

  get contentPosition(): string {
    return this.props['carouselContentPosition'] ?? this.defaults.carouselContentPosition;
  }

  get controlSize(): string {
    return this.props['carouselControlSize'] ?? this.defaults.carouselControlSize;
  }

  get ariaLabelPrev(): string {
    return this.props['ariaLabelPrev'] ?? this.defaults.ariaLabelPrev;
  }

  get ariaLabelNext(): string {
    return this.props['ariaLabelNext'] ?? this.defaults.ariaLabelNext;
  }

  get carouselIndex(): number {
    return this.props['carouselIndex'] ?? 0;
  }

  set carouselIndex(index: number) {
    this.props['carouselIndex'] = index;
  }

  ngOnInit(): void {
    this.setControlPadding();
    this.field.options?.fieldChanges?.pipe(untilDestroyed(this)).subscribe(() => {
      this.setControlPadding();
      this.change.detectChanges();
    });

    this.element.nativeElement.style.setProperty('--control-indent', this.props['carouselControlIndent'] || '0');

    this.slideHeightCss = `${this.slideHeight / (this.props['carouselItemCount'] ?? this.defaults.carouselItemCount)}%`;

    const ui = <ItemsUi>this.props['itemsUi'];
    if (ui?.columnCounts) {
      if (Array.isArray(ui.columnCounts) && ui.columnCounts.length === 3) {
        [this.sm, this.md, this.lg] = ui.columnCounts;
      } else {
        this.sm = this.md = this.lg = ui.columnCounts as number;
      }
    }

    if (ui?.gap) {
      this.gap = ui.gap;
    }

    if (this.formState.dialogFormChanged) {
      this.formState.dialogFormChanged().subscribe(() => {
        setTimeout(() => this.change.markForCheck(), 0);
      });
    }

    if (this.props['rootModelClone']) {
      this.rootModelExpr = utils.evalStringExpression<unknown>(`rootModel.${this.props['rootModelClone']}`, [
        'rootModel',
      ]);

      // Note: no intial sync is required as the table component emits a change once the client ids have been patched to any initial model items
      const syncControl = this.getFormControlExpr(this.props['rootModelClone']);
      if (syncControl) {
        syncControl.valueChanges.pipe(untilDestroyed(this)).subscribe(() => this.syncRootModelClone());
      }
    }
  }

  getFormControlExpr(path: string): UntypedFormControl | null {
    let exprStr = 'root';
    const parts = path.split('.');
    const postFix = parts.reduce((acc, val) => {
      return acc + `?.controls['${val}']`;
    }, '');
    exprStr += postFix;
    const expr = utils.evalStringExpression<UntypedFormControl>(exprStr, ['root']);
    if (expr) {
      try {
        return expr(this.form.root);
      } catch (e) {
        console.warn(
          `Root model clone expression is invalid. Expression was ${exprStr} exception was ${e}. Root was:`,
          this.form.root
        );
        return null;
      }
    }
    return null;
  }

  syncRootModelClone() {
    if (this.rootModelExpr) {
      const newVal = this.rootModelExpr(this.formState.rootModel);
      if (Array.isArray(newVal)) {
        const items = newVal as Array<Syncable>;
        const modelIds = items.map((item) => item._id);
        const formControlIds = [...this.formControlMap.keys()];
        this.syncUpdates(items, formControlIds);
        this.syncAdds(items, formControlIds);
        this.syncDeletes(formControlIds, modelIds);
      }
    }
  }

  syncAdds(items: Array<Syncable>, formControlIds: number[]) {
    const addItemss = items.filter((item) => !formControlIds.includes(item._id));
    addItemss.forEach((item) => {
      this.add(1, item);
      const control = this.formControl.controls[this.formControl.controls.length - 1];
      this.formControlMap.set(item._id, control);
    });
  }

  syncDeletes(formControlIds: number[], modelIds: number[]) {
    const deleteIds = formControlIds.filter((id) => !modelIds.includes(id));
    deleteIds.forEach((id) => {
      const index = formControlIds.indexOf(id);
      this.remove(index);
      this.formControlMap.delete(id);
      formControlIds.splice(index, 1);
    });
  }

  syncUpdates(items: Array<Syncable>, formControlIds: number[]) {
    const updateItems = items.filter((item) => formControlIds.includes(item._id));
    updateItems.forEach((item) => {
      const control = this.formControlMap.get(item._id);
      if (control) {
        control.patchValue(item);
      }
    });
  }

  getColumns(columnCount: number): string {
    return `repeat(${columnCount}, 1fr)`;
  }

  prev(evt: Event): void {
    evt.stopPropagation();
    const slides = this.slides?.nativeElement;
    const slidesLength: number = this.getSlidesLength(this.field?.fieldGroup?.length);
    const direction = this.props['carouselDirection'] ?? this.defaults.carouselDirection;

    if (this.carouselIndex > 0) {
      this.carouselIndex--;
      if (direction === 'horizontal') {
        slides?.scrollBy({ left: -slides?.clientWidth, top: 0, behavior: 'smooth' });
      }
      if (direction === 'vertical') {
        slides?.scrollBy({ left: 0, top: -slides?.clientHeight, behavior: 'smooth' });
      }
    } else {
      this.carouselIndex = slidesLength - 1;
      if (direction === 'horizontal') {
        slides?.scrollTo({ left: slides.clientWidth * slidesLength - 1, top: 0, behavior: 'smooth' });
      }
      if (direction === 'vertical') {
        slides?.scrollTo({ left: 0, top: slides.clientHeight * slidesLength - 1, behavior: 'smooth' });
      }
    }
  }

  next(evt: Event): void {
    evt.stopPropagation();
    const slides = this.slides?.nativeElement;
    const slidesLength: number = this.getSlidesLength(this.field?.fieldGroup?.length);
    const direction = this.props['carouselDirection'] ?? this.defaults.carouselDirection;

    if (this.carouselIndex < slidesLength - 1) {
      this.carouselIndex++;
      if (direction === 'horizontal') {
        slides?.scrollBy({ left: slides?.clientWidth, top: 0, behavior: 'smooth' });
      }
      if (direction === 'vertical') {
        slides?.scrollBy({ left: 0, top: slides?.clientHeight, behavior: 'smooth' });
      }
    } else {
      this.carouselIndex = 0;
      slides?.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
    }
  }

  handlePipClick(index: number) {
    const slides = this.slides?.nativeElement;
    const direction = this.props['carouselDirection'] ?? this.defaults.carouselDirection;

    this.carouselIndex = index;
    if (direction === 'horizontal') {
      slides?.scrollTo({ left: slides.clientWidth * index, top: 0, behavior: 'smooth' });
    }
    if (direction === 'vertical') {
      slides?.scrollTo({ left: 0, top: slides.clientHeight * index, behavior: 'smooth' });
    }
  }

  private getSlidesLength(length?: number): number {
    if (length) {
      return length / (this.props['carouselItemCount'] ?? this.defaults.carouselItemCount);
    }
    return 0;
  }

  private setControlPadding(): void {
    const direction = this.props['carouselControlDirection'] ?? this.defaults.carouselControlDirection;
    const size = this.props['carouselControlSize'] ?? this.defaults.carouselControlSize;
    const group = this.props['carouselGroupControls'];

    if (direction === 'horizontal') {
      this.paddingLeft = group === 'next' ? '0' : size;
      this.paddingRight = group === 'previous' ? '0' : size;
      this.paddingTop = '0';
      this.paddingBottom = '0';
    }

    if (direction === 'vertical') {
      this.paddingLeft = '0';
      this.paddingRight = '0';
      this.paddingTop = group === 'next' ? '0' : size;
      this.paddingBottom = group === 'previous' ? '0' : size;
    }
  }
}
