import { ComponentFixture, TestBed } from '@angular/core/testing';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { TaskSummaryTypeComponent } from './task-summary-type.component';
import { FormlyTestComponent } from '../../../core/formly/test/formly-test.component';

describe('TaskSummaryTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'tasksummary',
              component: TaskSummaryTypeComponent,
            },
          ],
        },
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.model = {
      tasksummary: {},
    };

    component.fields = [
      {
        key: 'tasksummary',
        type: 'tasksummary',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
