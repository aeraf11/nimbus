import { Component } from '@angular/core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-task-summary-type',
  templateUrl: './task-summary-type.component.html',
  styleUrls: ['./task-summary-type.component.scss'],
})
export class TaskSummaryTypeComponent extends BaseViewTypeComponent {}
