export const DataCardsDoc = `
<h3>Props</h3>
<ul>
  <li>additionalParams: object (supports addidtional params a list endpoint may support such as 'PersonTypeGroupId' on 'MyOperations'</li>
  <li>columnDefinitions: string[]</li>
  <li>filterColumns: FilterColumn[] ({column: string, stringFilter: string, constantFilter: boolean})</li>
  <li>idColumn: string</li>
  <li>listName: string</li>
  <li>pageIndex: number</li>
  <li>pageSize: number</li>
  <li>selectedItems: any[]</li>
  <li>selectMode: "single" | "multi" | "none"</li>
  <li>sortColumn: string</li>
  <li>showPaging: boolean</li>
  <li>noResultsText: string</li>
  <li>label: string</li>
  <li>icon: string</li>
  <li>maxHeight: string</li>
  <li>fullWidthContent: boolean</li>
  <li>cardWrapper: boolean</li>
  <li>refreshInterval: number (in milliseconds: 1000 = 1 second)</li>
  <li>refreshSignalR: { hub: string; event: string; group: string }</li>
  <li>itemTemplate
    <ul>
      <li>type: "message" | "crimscene"</li>
    </ul
  </li>
  <li>itemsUi
    <ul>
      <li>columnCounts: number</li>
      <li>gap: string</li>
    </ul>
  </li>
  <li>detailDialogFormId: string</li>
  <li>detailDialogTitleTemplate: string (templated string for the dialog title e.g. '\${fileName}')</li>
  <li>detailDialogTriggerIds: string[] (ids of the columns to be used to trigger the dialog)</li>
  <li>detailDialogMaxWidth: string</li>
  <li>detailDialogMinWidth: string</li>
  <li>detailDialogWidth: string</li>
  <li>previewDialogFormId: string</li>
  <li>previewDialogTitle: string</li>
  <li>previewDialogMaxWidth: string</li>
  <li>previewDialogMinWidth: string</li>
  <li>previewDialogWidth: string</li>
  <li>requestDialogFormId: string</li>
  <li>requestDialogTitle: string</li>
  <li>requestDialogMaxWidth: string</li>
  <li>requestDialogMinWidth: string</li>
  <li>requestDialogWidth: string</li>
  <li>requestActionKey: string</li>
  <li>requestActionRouteId: string</li>
  <li>enableSelectAll: boolean</li>
  <li>collapse: boolean</li>
  <li>isSingleContextFilter: boolean</li>
  <li>isDirtyOnSelectChange: boolean</li>
  <li>refreshId: string (an id that we can use in removeOfflineEntity Output effect)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    noResultsText: "No results found"
  },
}</pre>
`;
