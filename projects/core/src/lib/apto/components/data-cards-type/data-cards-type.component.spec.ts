import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { By } from '@angular/platform-browser';
import { ListFilterService, ListService, StateService } from '@nimbus/core/src/lib/core';
import { MockListService } from '../../../core/services/list.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { DataCardsTypeComponent } from './data-cards-type.component';
import { MockListFilterService } from '../../../core/services/list-filter-service/list-filter.service.spec';
import { DataCardsComponent } from '@nimbus/core/src/lib/core';
import { DataListBaseComponent } from '../../../core/components/data-list-base/data-list-base.component';

describe('DataCardsTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;
  let dataCardsComponent: DataCardsTypeComponent & DataListBaseComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'datacards',
              component: DataCardsTypeComponent,
            },
          ],
        },
        imports: [MatDialogModule],
        providers: [
          { provide: StateService, useClass: MockStateService },
          { provide: ListService, useClass: MockListService },
          { provide: ListFilterService, useClass: MockListFilterService },
        ],
      })
    ).compileComponents();
  });

  const setup = (props?: any) => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    const defaultProps = {
      listName: 'Test',
      columnDefinitions: [
        {
          id: 'test',
          value: 'test',
        },
      ],
    };

    component.fields = [
      {
        key: 'datacards',
        type: 'datacards',
        props: {
          ...defaultProps,
          ...props,
        },
      },
    ];

    fixture.detectChanges();

    dataCardsComponent = fixture.debugElement.query(By.directive(DataCardsComponent))?.componentInstance;
  };

  it('should create', () => {
    // Arrange
    setup();

    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });

  it('should set isDirtyOnSelectChange to true if not specifically provided in props', () => {
    // Arrange
    setup();
    const dataCards = dataCardsComponent as any;

    // Act
    fixture.detectChanges();

    // Assert
    expect(dataCards.isDirtyOnSelectChange).toEqual(true);
  });

  it('should set isDirtyOnSelectChange to what is supplied in the props if any', () => {
    // Arrange
    setup({
      isDirtyOnSelectChange: false,
    });
    const dataCards = dataCardsComponent as any;

    // Act
    fixture.detectChanges();

    //
    expect(dataCards.isDirtyOnSelectChange).toEqual(false);
  });
});
