import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-data-cards-type',
  templateUrl: './data-cards-type.component.html',
  styleUrls: ['./data-cards-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataCardsTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      noResultsText: 'No results found',
    },
  };

  override ngOnInit(): void {
    super.ngOnInit();
    this.field.props.rowCount = 0;
  }

  onRowCountChanged(rowCount: number) {
    this.field.props.rowCount = rowCount;
  }
}
