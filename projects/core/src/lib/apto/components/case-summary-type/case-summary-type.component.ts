import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseViewTypeComponent } from './../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-case-summary-type',
  templateUrl: './case-summary-type.component.html',
  styleUrls: ['./case-summary-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CaseSummaryTypeComponent extends BaseViewTypeComponent {}
