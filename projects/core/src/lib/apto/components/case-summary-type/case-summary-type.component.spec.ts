import { ComponentFixture, TestBed } from '@angular/core/testing';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { CaseSummaryTypeComponent } from './case-summary-type.component';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';

describe('CaseSummaryTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'casesummary',
              component: CaseSummaryTypeComponent,
            },
          ],
        },
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.model = {
      casesummary: {},
    };

    component.fields = [
      {
        key: 'casesummary',
        type: 'casesummary',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
