import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BaseTypeComponent } from './../base-type/base-type.component';
import { v4 as uuidv4 } from 'uuid';

@Component({
  selector: 'ni-uuid-type',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UuidTypeComponent extends BaseTypeComponent implements OnInit {
  override ngOnInit(): void {
    if (!this.formControl.value) {
      this.formControl.setValue(uuidv4());
    }
  }
}
