import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { FieldWrapper, FormlyFieldConfig } from '@ngx-formly/core';
import { MediaService } from '@nimbus/core/src/lib/core';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'ni-flex-wrapper',
  templateUrl: './flex-wrapper.component.html',
  styleUrls: ['./flex-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FlexWrapperComponent extends FieldWrapper implements OnInit {
  defaults = {
    layouts: 'row',
    gaps: '16px',
    alignments: 'unset unset',
  };

  layout$: Observable<string> = of('');
  gap$: Observable<string> = of('');
  alignment$: Observable<string> = of('');

  constructor(private mediaService: MediaService) {
    super();
  }

  ngOnInit(): void {
    const layouts = this.props['flexLayouts'] || this.props['layouts'] || this.defaults.layouts;
    this.layout$ = this.mediaService.getBreakpointValue(layouts);

    const gaps = this.props['flexGaps'] ?? this.props['gaps'] ?? this.defaults.gaps;
    this.gap$ = this.mediaService.getBreakpointValue(gaps);

    const alignments = this.props['flexAlignments'] ?? this.props['alignments'] ?? this.defaults.alignments;
    this.alignment$ = this.mediaService.getBreakpointValue(alignments);

    this.field.fieldGroup?.forEach((field) => {
      if (field.props && field.props?.['flexes']) {
        field.props['flexes$'] = this.mediaService.getBreakpointValue(field.props['flexes']);
      }
    });
  }
}
