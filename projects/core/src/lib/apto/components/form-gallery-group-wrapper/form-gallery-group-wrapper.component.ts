import { Component, ChangeDetectionStrategy } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
  selector: 'ni-form-gallery-group-wrapper',
  templateUrl: './form-gallery-group-wrapper.component.html',
  styleUrls: ['./form-gallery-group-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormGalleryGroupWrapperComponent extends FieldWrapper {}
