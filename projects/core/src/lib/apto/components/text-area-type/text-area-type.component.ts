import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-text-area-type',
  templateUrl: './text-area-type.component.html',
  styleUrls: ['./text-area-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextAreaTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      appearance: 'outline',
      cols: 1,
      autosize: true,
      autosizeMinRows: 2,
    },
  };
}
