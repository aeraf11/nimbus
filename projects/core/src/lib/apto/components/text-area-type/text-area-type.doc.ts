export const TextAreaDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>cols: number</li>
  <li>rows: number</li>
  <li>placeholder: string</li>
  <li>description: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>appearance: string ("outline" | "legacy" | "fill" | "standard")</li>
  <li>autoSize: boolean</li>
  <li>autoSizeMinRows: number</li>
  <li>ariaLabel: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    appearance: 'outline',
    cols: 1,
    autosize: true,
    autosizeMinRows: 2,
  },
}</pre>
`;

export const TextAreaReadonlyDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>cols: number</li>
  <li>rows: number</li>
  <li>placeholder: string</li>
  <li>description: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>appearance: string ("outline" | "legacy" | "fill" | "standard")</li>
  <li>autoSize: boolean</li>
  <li>autoSizeMinRows: number</li>
  <li>ariaLabel: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    appearance: 'outline',
    cols: 1,
    autosize: true,
    autosizeMinRows: 2,
    readonly: true,
  },
}</pre>
`;
