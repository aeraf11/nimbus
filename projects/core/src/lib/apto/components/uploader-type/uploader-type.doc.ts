export const UploaderDoc = `
<h3>Props</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>description: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>allowedExtensions: string</li>
  <li>asyncSettings: object ({chunkSize: number, saveUrl: string, removeUrl: string, retryAfterDelay: number, retryCount: number})</li>
  <li>autoUpload: boolean</li>
  <li>locale: string</li>
  <li>maxFileSize: number</li>
  <li>minFileSize: number</li>
  <li>sequentialUpload: boolean</li>
  <li>showFileList: boolean</li>
  <li>template: string</li>
  <li>storageType: number (0 = Attachment , 1 =  Media)</li>
  <li>showFileIdAfterUpload: boolean</li>
  <li>enableDownload: boolean (only available if the attachmentId has been assigned)</li>
  <li>scrollOnFileSelect: boolean</li>
  <li>attachmentAssignment: {file: FileInfo, attachmentId: string} (for assigning attachmentId onto a file)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  showFileIdAfterUpload: false,
  enableDownload: false,
  scrollOnFileSelect: true,
}</pre>
`;
