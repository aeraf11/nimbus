import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ChangeDetectionStrategy, Component, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';
import { StateService } from '@nimbus/core/src/lib/core';

@UntilDestroy()
@Component({
  selector: 'ni-uploader-type',
  templateUrl: './uploader-type.component.html',
  styleUrls: ['./uploader-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UploaderTypeComponent extends BaseTypeComponent {
  constructor(change: ChangeDetectorRef, private state: StateService) {
    super(change);

    this.state
      .selectFileUploadConfig()
      .pipe(untilDestroyed(this))
      .subscribe((config) => {
        if (config) {
          this.defaultOptions = {
            props: {
              showFileIdAfterUpload: false,
              enableDownload: false,
              scrollOnFileSelect: true,
              ...config,
            },
          };
        }
      });
  }
}
