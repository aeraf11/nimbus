import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FileService, DialogService } from '@nimbus/core/src/lib/core';
import { StateService } from '@nimbus/core/src/lib/core';
import { MockFileService } from '../../../core/services/file.service.spec';
import { MockDialogService } from '../../../core/services/dialog.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { UploaderTypeComponent } from './uploader-type.component';
import { FormlyTestComponent } from '../../../core/formly/test/formly-test.component';

describe('UploaderTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'uploader',
              component: UploaderTypeComponent,
            },
          ],
        },
        providers: [
          {
            provide: StateService,
            useClass: MockStateService,
          },
          {
            provide: FileService,
            useClass: MockFileService,
          },
          {
            provide: DialogService,
            useClass: MockDialogService,
          },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'uploader',
        type: 'uploader',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
