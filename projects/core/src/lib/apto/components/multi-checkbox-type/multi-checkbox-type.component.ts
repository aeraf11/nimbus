import { Component, ChangeDetectionStrategy, ViewChildren, QueryList } from '@angular/core';
import { MatCheckbox } from '@angular/material/checkbox';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-multi-checkbox-type',
  templateUrl: './multi-checkbox-type.component.html',
  styleUrls: ['./multi-checkbox-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MultiCheckboxTypeComponent extends BaseTypeComponent {
  @ViewChildren(MatCheckbox) checkboxes!: QueryList<MatCheckbox>;

  override defaultOptions = {
    props: {
      hideFieldUnderline: true,
      floatLabel: 'always' as const,
      options: [],
      color: 'accent' as const, // workaround for https://github.com/angular/components/issues/18465
    },
  };

  onChange(value: any, checked: boolean) {
    if (this.props['type'] === 'array') {
      this.formControl.patchValue(
        checked
          ? [...(this.formControl.value || []), value]
          : [...(this.formControl.value || [])].filter((o) => o !== value)
      );
    } else {
      this.formControl.patchValue({ ...this.formControl.value, [value]: checked });
    }
    this.formControl.markAsTouched();
  }

  // TODO: find a solution to prevent scroll on focus
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  override onContainerClick() {}

  isChecked(option: any) {
    const value = this.formControl.value;

    return value && (this.props['type'] === 'array' ? value.indexOf(option.value) !== -1 : value[option.value]);
  }
}
