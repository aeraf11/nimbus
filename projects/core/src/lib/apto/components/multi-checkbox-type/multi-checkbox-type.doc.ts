export const MultiCheckboxDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>description: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>hideFieldUnderline: boolean</li>
  <li>floatLabel: string ("always" | "never" | "auto")</li>
  <li>color: string ("primary" | "accent" | "warn" | undefined)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    hideFieldUnderline: true,
    floatLabel: 'always' as const,
    options: [],
    color: 'accent' as const,
  },
}</pre>
`;
