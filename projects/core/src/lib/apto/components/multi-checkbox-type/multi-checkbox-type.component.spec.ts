import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormlyModule } from '@ngx-formly/core';
import { FormlySelectModule } from '@ngx-formly/core/select';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { MultiCheckboxTypeComponent } from './multi-checkbox-type.component';

describe('MultiCheckboxTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatCheckboxModule,
        FormlySelectModule,
        FormlyModule.forRoot({
          types: [{ name: 'multicheckbox', component: MultiCheckboxTypeComponent }],
        }),
      ],
      declarations: [MultiCheckboxTypeComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
  });

  it('should create', () => {
    const componentInstance = fixture.componentInstance;
    componentInstance.fields = [
      {
        key: 'multicheckbox',
        type: 'multicheckbox',
      },
    ];
    fixture.detectChanges();
    expect(componentInstance).toBeTruthy();
  });
});
