import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseViewTypeComponent } from './../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-search-result-team-member-view-type',
  templateUrl: './search-result-team-member-view-type.component.html',
  styleUrls: ['./search-result-team-member-view-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultTeamMemberViewTypeComponent extends BaseViewTypeComponent {}
