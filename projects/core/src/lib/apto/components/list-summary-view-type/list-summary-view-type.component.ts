import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-list-summary-view-type',
  templateUrl: './list-summary-view-type.component.html',
  styleUrls: ['./list-summary-view-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListSummaryViewTypeComponent extends BaseViewTypeComponent implements OnInit {
  constructor(change: ChangeDetectorRef) {
    super(change);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.field.props.rowCount = 0;
  }

  onRowCountChanged(rowCount: number) {
    this.field.props.rowCount = rowCount;
  }
}
