import { OnInit, ChangeDetectorRef } from '@angular/core';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { untilDestroyed } from '@ngneat/until-destroy';

import { MediaService } from '../../../core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';
@Component({
  selector: 'ni-person-list-view-type',
  templateUrl: './person-list-view-type.component.html',
  styleUrls: ['./person-list-view-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersonListViewTypeComponent extends BaseViewTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      noData: 'No people found',
      photoProp: 'photoId',
      initialsProp: 'initials',
      breakPoint: '1,2,2',
    },
  };

  personWidth = '100%';
  constructor(change: ChangeDetectorRef, private mediaService: MediaService) {
    super(change);
  }

  override ngOnInit() {
    const breakPointTrilogy = this.props['breakPoint'];
    if (breakPointTrilogy) {
      this.mediaService
        .getBreakpoint()
        .pipe(untilDestroyed(this))
        .subscribe((breakpoint) => {
          const value = 100 / +this.mediaService.getBreakpointValueString(breakPointTrilogy, breakpoint, false);
          this.personWidth = `${value}%`;
          this.change.markForCheck();
          this.change.detectChanges();
        });
    }
    super.ngOnInit();
  }
}
