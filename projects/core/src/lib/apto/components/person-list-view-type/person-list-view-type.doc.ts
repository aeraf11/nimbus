export const PersonListDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>imageSize: ProfileSize ("xsmall" | "small" | "medium" | "large" | "xlarge")</li>
  <li>noData: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>photoProp: string</li>
  <li>initialsProp: string</li>
  <li>breakPoint: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    noData: 'No people found',
    photoProp: 'photoId',
    initialsProp: 'initials',
    breakPoint: '1,2,2'
  },
}</pre>
`;
