export const JobTimeDoc = `
<h3>Template Options</h3>
<ul>
  <li>jobId: string</li>
  <li>showSpinners: boolean</li>
  <li>stepHours: number</li>
  <li>stepMinutes: number</li>
  <li>stepSeconds: number</li>
  <li>showSeconds: boolean</li>
  <li>disableMinutes: boolean</li>
  <li>showMeridian: boolean</li>
  <li>color: string</li>
  <li>defaultTime: datetime</li>
  <li>required: boolean</li>
</ul>
<h3>Default Options</h3>
<pre>{
  showSpinners: true,
  stepHours: 1,
  stepMinutes: 1
}</pre>
`;
