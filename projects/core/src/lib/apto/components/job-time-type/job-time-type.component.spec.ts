import { StateService } from '@nimbus/core/src/lib/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { JobTimeTypeComponent } from './job-time-type.component';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';

describe('JobTimeTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'jobtime',
              component: JobTimeTypeComponent,
            },
          ],
        },
        providers: [{ provide: StateService, useClass: MockStateService }],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);

    fixture.detectChanges();
  });

  it('should create', () => {
    const componentInstance = fixture.componentInstance;
    componentInstance.fields = [
      {
        key: 'jobtime',
        type: 'jobtime',
      },
    ];

    // Act
    fixture.detectChanges();

    //Assert
    expect(componentInstance).toBeTruthy();
  });
});
