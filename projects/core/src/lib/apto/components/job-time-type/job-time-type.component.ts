import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ScheduledJob, StateService } from '@nimbus/core/src/lib/core';
import { Time } from '@nimbus/material';
import { deepClone } from 'deep.clone';
import { map, Observable } from 'rxjs';
import { ScheduledJobService } from '../../services';
import { BaseTypeComponent } from '../base-type/base-type.component';

const JOB_TIMES_KEY = 'jobTimes';

@UntilDestroy()
@Component({
  selector: 'ni-job-time-type',
  templateUrl: './job-time-type.component.html',
  styleUrls: ['./job-time-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class JobTimeTypeComponent extends BaseTypeComponent implements OnInit {
  job$?: Observable<ScheduledJob | undefined>;
  userTime?: Time;
  time$?: Observable<Time | undefined>;

  constructor(private state: StateService, private jobService: ScheduledJobService, change: ChangeDetectorRef) {
    super(change);
  }

  override defaultOptions = {
    props: {
      showSpinners: true,
      stepHours: 1,
      stepMinutes: 1,
    },
  };

  override ngOnInit(): void {
    super.ngOnInit();

    const config = JSON.parse(localStorage.getItem(JOB_TIMES_KEY) || '{}');
    if (config && config[this.props['jobId']]) {
      this.userTime = config[this.props['jobId']];
    }

    this.job$ = this.state.selectScheduledJob(this.props['jobId']);
    this.time$ = this.job$.pipe(
      untilDestroyed(this),
      map((job) => this.userTime || job?.defaultTime)
    );

    this.time$.subscribe((time) => deepClone(time).then((clone) => this.formControl.setValue(clone)));

    this.formControl.valueChanges.pipe(untilDestroyed(this)).subscribe((time) => this.saveUserTime(time));
  }

  saveUserTime(time: Time) {
    const config = JSON.parse(localStorage.getItem(JOB_TIMES_KEY) || '{}');
    config[this.props['jobId']] = time;
    localStorage.setItem(JOB_TIMES_KEY, JSON.stringify(config));
    this.jobService.resetJob(this.props['jobId'], time);
  }
}
