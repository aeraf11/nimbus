import { FocusMonitor } from '@angular/cdk/a11y';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';
import { CheckboxComponent } from '../../../core/components/checkbox/checkbox.component';

@Component({
  selector: 'ni-checkbox-type',
  templateUrl: './checkbox-type.component.html',
  styleUrls: ['./checkbox-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CheckboxTypeComponent extends BaseTypeComponent implements AfterViewInit, OnDestroy {
  @ViewChild(CheckboxComponent, { static: true }) checkbox!: CheckboxComponent;

  override defaultOptions = {
    props: {
      hideFieldUnderline: true,
      indeterminate: true,
      floatLabel: 'always' as const,
      hideLabel: true,
      align: 'start', // start or end
      color: 'accent' as const, // workaround for https://github.com/angular/components/issues/18465
    },
  };

  constructor(private focusMonitor: FocusMonitor, change: ChangeDetectorRef) {
    super(change);
  }

  override onContainerClick(event: MouseEvent): void {
    this.checkbox.focus();
    super.onContainerClick(event);
  }

  ngAfterViewInit() {
    if (this.checkbox.inputElement) {
      this.focusMonitor.monitor(this.checkbox.inputElement, true).subscribe((focusOrigin) => {
        if (focusOrigin) {
          this.props['focus'] && this.props['focus'](this.field);
        } else {
          this.props['blur'] && this.props['blur'](this.field);
        }
      });
    }
  }

  override ngOnDestroy() {
    if (this.checkbox?.inputElement) {
      this.focusMonitor.stopMonitoring(this.checkbox.inputElement);
    }
    super.ngOnDestroy();
  }
}
