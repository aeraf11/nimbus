export const CheckboxDoc = `
<h3>Props Options</h3>
<ul>
  <li>label: string</li>
  <li>labelClass: string</li>
  <li>required: boolean</li>
  <li>description: string</li>
  <li>pattern: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>hideFieldUnderline: boolean</li>
  <li>indeterminate: boolean</li>
  <li>floatLabel: string ("always" | "never" | "auto")</li>
  <li>hideLabel: boolean</li>
  <li>align: string ("start" | "end")</li>
  <li>color: string ("primary" | "accent" | "warn" | undefined)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    hideFieldUnderline: true,
    indeterminate: true,
    floatLabel: 'always' as const,
    hideLabel: true,
    align: 'start',
    color: 'accent' as const, // workaround for https://github.com/angular/components/issues/18465
  },
}</pre>
`;
