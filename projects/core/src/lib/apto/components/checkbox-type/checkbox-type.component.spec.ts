import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { CheckboxTypeComponent } from './checkbox-type.component';
import { CheckboxComponent } from '../../../core/components/checkbox/checkbox.component';

describe('CheckboxTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormlyTestComponent, CheckboxTypeComponent, CheckboxComponent],
      imports: [
        ReactiveFormsModule,
        MatCheckboxModule,
        FormlyModule.forRoot({
          types: [
            {
              name: 'checkbox',
              component: CheckboxTypeComponent,
            },
          ],
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
  });

  it('should create', () => {
    const componentInstance = fixture.componentInstance;
    componentInstance.fields = [
      {
        key: 'checkbox',
        type: 'checkbox',
      },
    ];
    fixture.detectChanges();
    expect(componentInstance).toBeTruthy();
  });
});
