import { FormsHelperService, utils } from '@nimbus/core/src/lib/core';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';
import { merge } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'ni-context-type',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContextTypeComponent extends BaseTypeComponent implements OnInit {
  expressionMap = new Map<
    string,
    ((rootModel: unknown, model: unknown, context: unknown, helper: FormsHelperService) => unknown) | undefined
  >();

  constructor(change: ChangeDetectorRef, private formsHelper: FormsHelperService) {
    super(change);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    if (this.props['expressions']) {
      for (const prop in this.props['expressions']) {
        const expr = this.getExpresssion(this.props['expressions'][prop]);
        this.expressionMap.set(prop, expr);
      }
    }

    merge(this.formState.rootModelChanged, this.formState.contextChanged)
      .pipe(untilDestroyed(this))
      .subscribe(() => this.processExpressions());

    this.processExpressions();
  }

  processExpressions() {
    if (this.props['expressions']) {
      for (const prop in this.props['expressions']) {
        const expr = this.expressionMap.get(prop);
        if (expr) {
          const value = expr(this.formState.rootModel, this.model, this.formState.context, this.formsHelper);
          if (this.formState.context[prop] !== value) {
            this.formState.context[prop] = value;
            this.formState.contextChanged.next(prop);
          }
        }
      }
    }
  }

  getExpresssion(
    expressionStr: string
  ): ((rootModel: unknown, model: unknown, context: unknown, helper: FormsHelperService) => unknown) | undefined {
    const expression = utils.evalStringFormExpression<unknown>(expressionStr);
    return expression;
  }
}
