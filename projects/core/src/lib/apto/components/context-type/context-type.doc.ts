export const ContextDoc = `
<h3>Template Options</h3>
<ul>
  <li>expressions: object ( the object property names map to the same name in the context and the property values are evaluated as expressions with access to rootModel, mode, context and formsHelper )</li>
</ul>

<h3>Default Options</h3>
<pre>{
}</pre>
`;
