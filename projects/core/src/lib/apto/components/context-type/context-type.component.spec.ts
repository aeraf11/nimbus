import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ContextTypeComponent } from './context-type.component';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { StateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';

describe('ContextTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'context',
              component: ContextTypeComponent,
            },
          ],
        },
        providers: [{ provide: StateService, useClass: MockStateService }],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;
    component.fields = [
      {
        key: 'context',
        type: 'context',
      },
    ];
    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    //Assert
    expect(component).toBeTruthy();
  });
});
