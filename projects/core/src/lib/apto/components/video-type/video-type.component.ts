import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-video-type',
  templateUrl: './video-type.component.html',
  styleUrls: ['./video-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VideoTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      autoHideControls: false,
      autoHideControlsTime: 5,
      showPlayOverlay: true,
      hideControlPanel: false,
      showPlayPause: true,
      showCurrentTime: true,
      showRemainingTime: false,
      showTotalTime: false,
      showScrubBar: true,
      showScrubCurrentTime: true,
      showScrubSlider: true,
      showScrubBufferTime: true,
      timeFormat: 'hh:mm:ss',
      showMute: true,
      showVolume: true,
      showFullscreen: true,
      url: 'api/v2/media/av/{id}',
      height: '500px',
      width: '500px',
    },
  };

  override ngOnInit() {
    super.ngOnInit();
  }
}
