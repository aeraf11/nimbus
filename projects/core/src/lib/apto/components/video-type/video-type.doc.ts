export const VideoDoc = `
<h3>Template Options</h3>
<ul>
  <li>autoHideControls: boolean</li>
  <li>autoHideControlsTime: number</li>
  <li>showPlayOverlay: boolean</li>
  <li>hideControlPanel: boolean</li>
  <li>showPlayPause: boolean</li>
  <li>showCurrentTime: boolean</li>
  <li>showRemainingTime: boolean</li>
  <li>showTotalTime: boolean</li>
  <li>showScrubBar: boolean</li>
  <li>showScrubCurrentTime: boolean</li>
  <li>showScrubSlider: boolean</li>
  <li>showScrubBufferTime: boolean</li>
  <li>timeFormat: string</li>
  <li>showMute: boolean</li>
  <li>showVolume: boolean</li>
  <li>showFullscreen: boolean</li>
  <li>url: string</li>
  <li>videoId: string</li>
  <li>height: string<li>
  <li>width: string<li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    autoHideControls: false,
    autoHideControlsTime: 5,
    showPlayOverlay: true,
    hideControlPanel: false,
    showPlayPause: true,
    showCurrentTime: true,
    showRemainingTime: false,
    showTotalTime: false,
    showScrubBar: true,
    showScrubCurrentTime: true,
    showScrubSlider: true,
    showScrubBufferTime: true,
    timeFormat: 'hh:mm:ss',
    showMute: true,
    showVolume: true,
    showFullscreen: true,
    url: 'api/v2/media/av/{id}'
    height: '500px',
    width: '500px',
  },
}</pre>
`;
