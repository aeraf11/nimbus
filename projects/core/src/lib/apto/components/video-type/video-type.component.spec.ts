import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoTypeComponent } from './video-type.component';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '../../../core/formly/test/formly-test.component';

describe('VideoTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'video',
              component: VideoTypeComponent,
            },
          ],
        },
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'video',
        type: 'video',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
