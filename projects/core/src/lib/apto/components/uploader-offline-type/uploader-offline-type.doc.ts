export const UploaderOfflineDoc = `
<h3>Props</h3>
<ul>
  <li>itemTableName: string (the table name must match a store that was set in the application's appsettings.json)</li>
  <li>chunkSize: number</li>
  <li>multiple: boolean</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    itemTableName: 'offlineUploads',
    noResultsText: 'No files found'
  },
}</pre>
`;
