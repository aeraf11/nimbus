import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IndexedDbService, StateService } from '../../../core';
import { MockIndexedDbService } from '../../../core/services/indexeddb.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { UploaderOfflineTypeComponent } from './uploader-offline-type.component';
import { of } from 'rxjs';
import { MatDialogModule } from '@angular/material/dialog';
import { FormlyTestComponent } from '../../../core/formly/test/formly-test.component';

describe('UploaderOfflineTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'uploaderoffline',
              component: UploaderOfflineTypeComponent,
            },
          ],
        },
        imports: [MatDialogModule],
        providers: [
          { provide: IndexedDbService, useClass: MockIndexedDbService },
          { provide: StateService, useClass: MockStateService },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'uploaderoffline',
        type: 'uploaderoffline',
      },
    ];

    fixture.detectChanges();
  });

  beforeEach(() => {
    const state = TestBed.inject(StateService);
    spyOn(state, 'selectFileUploadConfig').and.returnValue(
      of({
        allowedExtensions: '.jpg',
        asyncSettings: {
          saveUrl: '/save',
          removeUrl: '/remove',
          chunkSize: 1024 * 1024,
          retryCount: 2,
          retryAfterDelay: 10000,
        },
        autoUpload: false,
        locale: 'en',
        maxFileSize: 100,
        minFileSize: 100,
        multiple: false,
        sequentialUpload: false,
        showFileList: true,
        showFileManager: true,
        template: '',
      })
    );
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
