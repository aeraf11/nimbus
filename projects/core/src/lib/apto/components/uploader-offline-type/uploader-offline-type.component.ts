import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { StateService } from '@nimbus/core/src/lib/core';
import { BaseTypeComponent } from './../base-type/base-type.component';

@UntilDestroy()
@Component({
  selector: 'ni-uploader-offline-type',
  templateUrl: './uploader-offline-type.component.html',
  styleUrls: ['./uploader-offline-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UploaderOfflineTypeComponent extends BaseTypeComponent implements OnInit {
  constructor(change: ChangeDetectorRef, private state: StateService) {
    super(change);

    this.state
      .selectFileUploadConfig()
      .pipe(untilDestroyed(this))
      .subscribe((config) => {
        if (config) {
          this.defaultOptions = {
            props: {
              ...config,
              itemTableName: 'offlineUploads',
              noResultsText: 'No files found',
            },
          };
        }
      });
  }

  override ngOnInit(): void {
    super.ngOnInit();
  }
}
