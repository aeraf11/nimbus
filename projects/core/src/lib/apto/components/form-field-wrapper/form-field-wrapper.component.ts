import { ChangeDetectionStrategy, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';
import { MediaService } from '@nimbus/core/src/lib/core';
import { of } from 'rxjs';

@Component({
  selector: 'ni-form-field-wrapper',
  templateUrl: './form-field-wrapper.component.html',
  styleUrls: ['./form-field-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class FormFieldWrapperComponent extends FieldWrapper implements OnInit {
  defaults = {
    formFieldLeftWidths: '100%,33.33%',
    formFieldMiddleWidths: '100%,50%',
    formFieldRightWidths: '100%,16.66%',
    formFieldMarginTop: '1.5rem',
  };

  leftFlex$ = of('');
  middleFlex$ = of('');
  rightFlex$ = of('');
  validPreTouch = false;

  constructor(private mediaService: MediaService) {
    super();
  }

  ngOnInit(): void {
    this.leftFlex$ = this.mediaService.getBreakpointValue(
      this.props['formFieldLeftWidths'] ?? this.defaults.formFieldLeftWidths
    );
    this.middleFlex$ = this.mediaService.getBreakpointValue(
      this.props['formFieldMiddleWidths'] ?? this.defaults.formFieldMiddleWidths
    );
    this.rightFlex$ = this.mediaService.getBreakpointValue(
      this.props['formFieldRightWidths'] ?? this.defaults.formFieldRightWidths
    );
    if (this.props['formFieldValidatePreTouch']) this.validPreTouch = true;
  }
}
