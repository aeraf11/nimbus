export const FormFieldWrapperDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>formFieldHideLabel: boolean</li>
  <li>formFieldHideRequiredMarker: boolean</li>
  <li>formFieldLeftWidths: string ("{number}%/em/px comma separated trinity")</li>
  <li>formFieldMiddleWidths: string ("{number}%/em/px comma separated trinity")</li>
  <li>formFieldRightWidths: string ("{number}%/em/px comma separated trinity")</li>
  <li>formFieldMarginTop: string</li>
  <li>formFieldValidatePreTouch: boolean</li>
  <li>yesValidationMessage: string (if using yes validator, override validation message)</li>
  <li>noValidationMessage: string (if using no validator, override validation message)</li>
  <li>patternValidationMessage: string (if using pattern validator, override validation message)</li>
  <li>formFieldSmallLabel: boolean (makes sure the label is not bold and the spacing between label and field is smaller)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  formFieldLeftWidths: '100%,33.33%',
  formFieldMiddleWidths: '100%,50%',
  formFieldRightWidths: '100%,16.66%',
  formFieldMarginTop: '1.5rem',
}</pre>
`;
