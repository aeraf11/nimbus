import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-text-panel-type',
  templateUrl: './text-panel-type.component.html',
  styleUrls: ['./text-panel-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextPanelTypeComponent extends BaseTypeComponent {
  @Input() textControl = new UntypedFormControl();
  override defaultOptions = {
    props: {
      label: 'Text Panel',
      appearance: 'outline',
      color: 'panel-alert-success' as const, // workaround for https://github.com/angular/components/issues/18465,
      ariaLabel: 'Alert panel',
    },
  };
}
