export const TextPanelDoc = `
<h3>Props</h3>
<ul>
  <li>label: string</li>
  <li>description: string</li>
  <li>appearance: string</li>
  <li>color: string</li>
  <li>ariaLabel: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    label: 'Text Panel',
    appearance: 'outline',
    color: 'panel-alert-success',
    ariaLabel: 'Alert panel',
  }
}</pre>
`;
