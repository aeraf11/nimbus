import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { TextPanelTypeComponent } from './text-panel-type.component';
import { FormlyTestComponent } from '../../../core/formly/test/formly-test.component';

describe('TextPanelTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormlyTestComponent, TextPanelTypeComponent],
      imports: [
        ReactiveFormsModule,
        FormlyModule.forRoot({
          types: [
            {
              name: 'textpanel',
              component: TextPanelTypeComponent,
            },
          ],
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
  });

  it('should create', () => {
    const componentInstance = fixture.componentInstance;
    componentInstance.fields = [
      {
        key: 'textpanel',
        type: 'textpanel',
      },
    ];
    fixture.detectChanges();
    expect(componentInstance).toBeTruthy();
  });
});
