import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';
import { FieldViewLayout } from '../../models';
import { OnChange } from 'property-watch-decorator';
import deepClone from 'deep.clone';
import { FormlyFieldConfig } from '@ngx-formly/core';

@Component({
  selector: 'ni-fields-view-type',
  templateUrl: './fields-view-type.component.html',
  styleUrls: ['./fields-view-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class FieldsViewTypeComponent extends BaseViewTypeComponent {
  override defaultOptions = {
    props: {
      layout: FieldViewLayout.Column,
      noData: 'No data found',
    },
  };

  fieldConfig?: FormlyFieldConfig[];

  @OnChange<FormlyFieldConfig>(function (this: FieldsViewTypeComponent, value) {
    deepClone(value.fieldGroup).then((fieldConfig) => {
      this.fieldConfig = fieldConfig;
      this.change.detectChanges();
    });
  })
  override field: any;

  constructor(change: ChangeDetectorRef) {
    super(change);
  }

  get layout() {
    return this.layoutMatch(this.props['layout'], FieldViewLayout.Row) ? 'row' : 'column';
  }

  layoutMatch(a: string, b: FieldViewLayout) {
    return (<any>FieldViewLayout)[a] === b;
  }
}
