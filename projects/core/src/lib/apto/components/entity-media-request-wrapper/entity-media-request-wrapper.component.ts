import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  OnInit,
  ViewEncapsulation,
  ChangeDetectorRef,
} from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FieldWrapper } from '@ngx-formly/core';

@UntilDestroy()
@Component({
  selector: 'ni-entity-media-request-wrapper',
  templateUrl: './entity-media-request-wrapper.component.html',
  styleUrls: ['./entity-media-request-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class EntityMediaRequestWrapperComponent extends FieldWrapper implements OnInit {
  public defaults = {
    entityMediaRequestActionLabel: 'Request Selected',
  };

  constructor(private cd: ChangeDetectorRef) {
    super();
  }

  itemCount = 0;
  selectedItems: any[] = [];
  actionEmit = new EventEmitter<void>();

  ngOnInit(): void {
    this.formControl.valueChanges.pipe(untilDestroyed(this)).subscribe((obj) => {
      this.selectedItems = [];
      const mediaList = obj[Object.keys(obj)[0]];
      if (mediaList) {
        this.selectedItems.push(...mediaList);
      }
      this.cd.detectChanges();
    });
  }

  onActionClick(selectedItems: any): void {
    this.actionEmit.emit(selectedItems);
  }

  setItemCount(count: number): void {
    this.itemCount = count;
  }
}
