export const ImageCheckDoc = `
<h3>Props:</h3>
<ul>
  <li>width: string</li>
  <li>height: string</li>
  <li>maintainAspect: boolean</li>
  <li>text: string (the images alt text)</li>
  <li>isRemoveOnly: boolean</li>
  <li>watermark: boolean</li>
  <li>margins: string (like CSS margins e.g "0 0 1rem 0")</li>
  <li>minWidth: string (suffix with px/em/rem)</li>
  <li>minHeight: string (suffix with px/em/rem)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  minHeight: '60px',
  minWidth: '100px',
}</pre>
`;
