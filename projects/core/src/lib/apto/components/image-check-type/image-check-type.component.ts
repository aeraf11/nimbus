import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-image-check-type',
  templateUrl: './image-check-type.component.html',
  styleUrls: ['./image-check-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageCheckTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      minWidth: '100px',
      minHeight: '60px',
    },
  };

  constructor(public override change: ChangeDetectorRef) {
    super(change);
  }

  onCheckChange() {
    if (this.formState.isSelect && this.formState.selectionChanged) {
      this.formState.selectionChanged();
    }
  }

  onRemove(): void {
    if (this.formState.selectionChanged) {
      this.formState.selectionChanged(this.value);
    }
  }
}
