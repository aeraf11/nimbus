export const DataDownloadablesDoc = `
<h3>Template Options</h3>
<ul>
  <li>additionalParams: object (supports addidtional params a list endpoint may support such as 'PersonTypeGroupId' on 'MyOperations'</li>
  <li>cancelText: string</li>
  <li>columnDefinitions: string[]</li>
  <li>dialogWidth: string</li>
  <li>filterColumns: FilterColumn[] ({column: string, stringFilter: string, constantFilter: boolean})</li>
  <li>filterDisabled: boolean</li>
  <li>filterText: string</li>
  <li>filterTitle: string</li>
  <li>idColumn: string</li>
  <li>listName: string</li>
  <li>pageIndex: number</li>
  <li>pageSize: number</li>
  <li>quickSearch: string</li>
  <li>savedOptionsId: string (set an id if you want the options to be saved)</li>
  <li>selectedItems: any[]</li>
  <li>selectMode: "single" | "multi" | "none"</li>
  <li>showPaging: boolean</li>
  <li>showQuickSearch: boolean</li>
  <li>sortColumn: string</li>
  <li>sortDirection: "asc" | "desc"</li>
  <li>detailDialogFormId: string</li>
  <li>detailDialogTitleTemplate: string (templated string for the dialog title e.g. '\${fileName}')</li>
  <li>detailDialogTriggerIds: string[] (ids of the columns to be used to trigger the dialog)</li>
  <li>detailDialogMaxWidth: string</li>
  <li>detailDialogMinWidth: string</li>
  <li>detailDialogWidth: string</li>
  <li>isDirtyOnSelectChange: boolean</li>
  <li>multiDownloadStagger: number (time between multiple downloads) (removed in 3.26.0)</li>
  <li>ariaLabelField: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  isDirtyOnSelectChange: false,
}</pre>
`;
