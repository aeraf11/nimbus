import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { SiteConfig, StateService } from '@nimbus/core/src/lib/core';
import { ListService } from '../../../core/services/list.service';
import { MockListService } from '../../../core/services/list.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { BlobService } from './../../../core/services/blob.service';
import { MockBlobService } from './../../../core/services/blob.service.spec';
import { DataDownloadablesTypeComponent } from './data-downloadables-type.component';

describe('DataDownloadablesTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'datadownloadables',
              component: DataDownloadablesTypeComponent,
            },
          ],
        },
        imports: [MatDialogModule, HttpClientTestingModule],
        providers: [
          { provide: StateService, useClass: MockStateService },
          { provide: ListService, useClass: MockListService },
          { provide: BlobService, useClass: MockBlobService },
          { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'datadownloadables',
        type: 'datadownloadables',
        props: {
          listName: 'Test',
          columnDefinitions: [{ id: 'test', value: 'test' }],
        },
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
