import { BaseViewTypeComponent } from './../base-view-type/base-view-type.component';
import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';

@Component({
  selector: 'ni-data-downloadables-type',
  templateUrl: './data-downloadables-type.component.html',
  styleUrls: ['./data-downloadables-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataDownloadablesTypeComponent extends BaseViewTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      isDirtyOnSelectChange: false,
    },
  };

  override ngOnInit(): void {
    super.ngOnInit();
    this.field.props.rowCount = 0;
  }

  onRowCountChanged(rowCount: number) {
    this.field.props.rowCount = rowCount;
  }
}
