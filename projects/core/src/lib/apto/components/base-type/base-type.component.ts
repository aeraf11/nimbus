import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { ChangeDetectorRef } from '@angular/core';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FieldType } from '@ngx-formly/material/form-field';
import { Subject } from 'rxjs';
import { FieldTypeConfig } from '@ngx-formly/core';
import { FormGroup } from '@angular/forms';

@UntilDestroy()
@Component({
  selector: 'ni-base-type',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BaseTypeComponent extends FieldType<FieldTypeConfig> implements OnInit {
  changes!: Subject<any>;

  constructor(public change: ChangeDetectorRef) {
    super();
  }

  ngOnInit() {
    this.subscribeToChanges();
    const contextMap = this.props['contextMap'];

    if (contextMap) {
      this.field.formControl?.valueChanges.subscribe((newVal) => {
        const value = Array.isArray(newVal) ? newVal[0] : newVal;
        for (const map in contextMap) {
          const expression = contextMap[map];
          let resolvedValue = value;
          if (value) {
            resolvedValue = this.resolveContextExpression(expression, { ...value });
          }
          this.formState.context[map] = resolvedValue;
          this.formState.contextChanged.next(map);
        }
      });
    }
  }

  subscribeToChanges() {
    this.changes = this.options?.formState?.rootModelChanged as Subject<any>;
    if (this.changes) {
      this.changes.pipe(untilDestroyed(this)).subscribe(() => this.change.markForCheck());
    }
    this.formControl.valueChanges.pipe(untilDestroyed(this)).subscribe(() => {
      this.change.markForCheck();
    });
  }

  resolveContextExpression(expression: string, newVal: any) {
    if (expression === '$') {
      return newVal;
    }
    return expression
      .substring(2)
      .split('.')
      .reduce((obj, key) => (obj && obj[key] ? obj[key] : null), newVal);
  }

  get formGroup(): FormGroup {
    return <FormGroup>this.form;
  }
}
