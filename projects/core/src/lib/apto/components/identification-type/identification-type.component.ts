import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-identification-type',
  templateUrl: './identification-type.component.html',
  styleUrls: ['./identification-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IdentificationTypeComponent extends BaseViewTypeComponent {
  override defaultOptions = {
    props: {
      viewType: 'grid',
    },
  };
}
