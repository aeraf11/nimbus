export const DateRangeDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>description: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>appearance: string ("outline" | "legacy" | "fill" | "standard")</li>
  <li>showQuickLinks: boolean</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    appearance: 'outline',
    showQuickLinks: false
  },
}</pre>
`;
