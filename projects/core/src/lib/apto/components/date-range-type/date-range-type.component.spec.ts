import { Platform } from '@angular/cdk/platform';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DateAdapter, MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { StateService } from '@nimbus/core/src/lib/core';
import { AppDateTimeAdapter } from '@nimbus/core/src/lib/core';
import { DateTimeAdapter, MatDatepickerModule } from '@nimbus/material';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { DateRangeTypeComponent } from './date-range-type.component';

describe('DateRangeTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'daterange',
              component: DateRangeTypeComponent,
            },
          ],
        },
        imports: [MatDatepickerModule, MatNativeDateModule],
        providers: [
          {
            provide: StateService,
            useClass: MockStateService,
          },
          {
            provide: DateTimeAdapter,
            useClass: AppDateTimeAdapter,
            deps: [MAT_DATE_LOCALE, Platform, StateService],
          },
          {
            provide: DateAdapter,
            useClass: AppDateTimeAdapter,
            deps: [MAT_DATE_LOCALE, Platform, StateService],
          },
          {
            provide: AppDateTimeAdapter,
            useClass: AppDateTimeAdapter,
            deps: [MAT_DATE_LOCALE, Platform, StateService],
          },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'daterange',
        type: 'daterange',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
