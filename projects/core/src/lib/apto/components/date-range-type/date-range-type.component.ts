import { Component, ChangeDetectionStrategy } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-date-range-type',
  templateUrl: './date-range-type.component.html',
  styleUrls: ['./date-range-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DateRangeTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      appearance: 'outline',
      showQuickLinks: false,
    },
  };
}
