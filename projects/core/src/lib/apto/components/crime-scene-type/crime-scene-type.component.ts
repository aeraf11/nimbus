import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-crime-scene-type',
  templateUrl: './crime-scene-type.component.html',
  styleUrls: ['./crime-scene-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CrimeSceneTypeComponent extends BaseViewTypeComponent {}
