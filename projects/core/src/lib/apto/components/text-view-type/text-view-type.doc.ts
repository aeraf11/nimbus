export const TextDoc = `
<h3>Props</h3>
<ul>
  <li>alwaysShowLabel: boolean</li>
  <li>dataType: object (see below)</li>
  <li>icon: string</li>
  <li>labelClass: string (e.g. heading makes it bold)</li>
  <li>noData: string</li>
</ul>
<h4>Data Type</h4>
<ul>
  <li>text</li>
  <li>date</li>
  <li>dateTime</li>
  <li>multiChoice</li>
  <li>lookup</li>
  <li>lookupHierarchical</li>
  <li>selectOption</li>
  <li>profile</li>
  <li>status</li>
  <li>icon</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    noData: 'No data found',
  },
}</pre>
`;
