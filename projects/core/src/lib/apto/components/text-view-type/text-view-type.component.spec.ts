import { ComponentFixture, TestBed } from '@angular/core/testing';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { TextViewTypeComponent } from './text-view-type.component';
import { FormlyTestComponent } from '../../../core/formly/test/formly-test.component';

describe('TextViewTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'textview',
              component: TextViewTypeComponent,
            },
          ],
        },
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'textview',
        type: 'textview',
      },
    ];
  });

  it('should create', () => {
    // Assert
    expect(component).toBeTruthy();
  });
});
