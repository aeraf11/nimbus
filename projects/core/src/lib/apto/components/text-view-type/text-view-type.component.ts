import { ChangeDetectorRef } from '@angular/core';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-text-view-type',
  templateUrl: './text-view-type.component.html',
  styleUrls: ['./text-view-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextViewTypeComponent extends BaseViewTypeComponent {
  override defaultOptions = {
    props: {
      noData: 'No data found',
    },
  };

  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
