export const BinarySizeDoc = `
<h3>Template Options</h3>
<ul>
  <li>appearance: string ("outline" | "legacy" | "fill" | "standard")</li>
  <li>required: boolean</li>
  <li>readonly: boolean</li>
  <li>placeholder: string</li>
  <li>tabIndex: number</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    appearance: 'outline'
  },
}</pre>
`;
