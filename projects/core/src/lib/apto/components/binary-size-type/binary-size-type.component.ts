import { ChangeDetectionStrategy, Component, ChangeDetectorRef, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-binary-size-type',
  templateUrl: './binary-size-type.component.html',
  styleUrls: ['./binary-size-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class BinarySizeTypeComponent extends BaseTypeComponent implements AfterViewInit {
  override defaultOptions = {
    props: {
      appearance: 'outline',
    },
  };

  constructor(change: ChangeDetectorRef) {
    super(change);
  }

  ngAfterViewInit(): void {
    // we always receive MetaItem ValueData in the string format, when readonly we need to parse it on the model.
    // config that consumes this stringifies on write. eg : `"ValueData": "#tostring(#valueof($.binarySize))"`
    if (this.props['readonly'] && typeof this.formControl?.value === 'string') {
      this.formControl.setValue(JSON.parse(this.formControl.value));
    }
  }
}
