import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { BinarySizeTypeComponent } from './binary-size-type.component';

describe('BinarySizeTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormlyTestComponent, BinarySizeTypeComponent],
      imports: [
        FormlyModule.forRoot({
          types: [
            {
              name: 'binarysize',
              component: BinarySizeTypeComponent,
            },
          ],
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
  });

  it('should create', () => {
    const componentInstance = fixture.componentInstance;
    componentInstance.fields = [
      {
        key: 'binarysize',
        type: 'binarysize',
      },
    ];
    fixture.detectChanges();
    expect(componentInstance).toBeTruthy();
  });
});
