import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-media-upload-type',
  templateUrl: './media-upload-type.component.html',
  styleUrls: ['./media-upload-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MediaUploadTypeComponent extends BaseTypeComponent {
  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
