import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import {
  BlobService,
  DataTableComponent,
  ListFilterService,
  SiteConfig,
  StateService,
} from '@nimbus/core/src/lib/core';
import { ListService } from '../../../core/services/list.service';
import { MockListService } from '../../../core/services/list.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { DataTableTypeComponent } from './data-table-type.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MockListFilterService } from '../../../core/services/list-filter-service/list-filter.service.spec';
import { MockBlobService } from '../../../core/services/blob.service.spec';

describe('DataTableTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;
  let dataTableComponent: DataTableComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'datatable',
              component: DataTableTypeComponent,
            },
          ],
        },
        imports: [RouterTestingModule, MatDialogModule, HttpClientTestingModule],
        providers: [
          { provide: StateService, useClass: MockStateService },
          { provide: ListService, useClass: MockListService },
          { provide: BlobService, useClass: MockBlobService },
          { provide: ListFilterService, useClass: MockListFilterService },
          { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
        ],
      })
    ).compileComponents();
  });

  const setup = (props?: any) => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    const defaultProps = {
      listName: 'Test',
      columnDefinitions: [{ id: 'test', value: 'test' }],
    };

    component.fields = [
      {
        key: 'datatable',
        type: 'datatable',
        props: {
          ...defaultProps,
          ...props,
        },
      },
    ];

    fixture.detectChanges();

    dataTableComponent = fixture.debugElement.query(By.directive(DataTableComponent))?.componentInstance;
  };

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'datatable',
        type: 'datatable',
        props: {
          listName: 'Test',
          columnDefinitions: [{ id: 'test', value: 'test' }],
        },
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Arrange
    setup();

    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });

  it('should set isDirtyOnSelectChange to true if not specifically provided in props', () => {
    // Arrange
    setup();
    const dataTable = dataTableComponent as any;

    // Act
    fixture.detectChanges();

    // Assert
    expect(dataTable.isDirtyOnSelectChange).toEqual(true);
  });

  it('should set isDirtyOnSelectChange to what is supplied in the props if any', () => {
    // Arrange
    setup({
      isDirtyOnSelectChange: false,
    });
    const dataTable = dataTableComponent as any;

    // Act
    fixture.detectChanges();

    //
    expect(dataTable.isDirtyOnSelectChange).toEqual(false);
  });
});
