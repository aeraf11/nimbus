import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-data-table-type',
  templateUrl: './data-table-type.component.html',
  styleUrls: ['./data-table-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataTableTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      filterText: 'Filter',
      filterTitle: 'Filter Results',
      cancelText: 'Cancel',
      clearText: 'Clear',
      enableLoadingIndicator: true,
      pageSize: 10,
    },
  };

  override ngOnInit(): void {
    super.ngOnInit();
    this.field.props.rowCount = 0;
  }

  onRowCountChanged(rowCount: number) {
    this.field.props.rowCount = rowCount;
  }
}
