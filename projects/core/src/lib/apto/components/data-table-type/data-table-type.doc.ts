export const DataTableDoc = `
<h3>Template Options</h3>
<ul>
  <li>additionalParams: object (supports addidtional params a list endpoint may support such as 'PersonTypeGroupId' on 'MyOperations'</li>
  <li>cancelText: string</li>
  <li>columnDefinitions: { id: string, value: string, dataType: DataType, isHidden: boolean, isSortable: boolean,  filterParams: { formlyType: 'input' | 'daterange' | 'lookup' | 'multichoice' | 'select2' }, minWidth: string, maxWidth: string }[]</li>
  <li>dialogWidth: string</li>
  <li>filterColumns: FilterColumn[] ({column: string, stringFilter: string, constantFilter: boolean})</li>
  <li>filterDisabled: boolean</li>
  <li>filterText: string</li>
  <li>filterTitle: string</li>
  <li>idColumn: string</li>
  <li>listName: string</li>
  <li>pageIndex: number</li>
  <li>pageSize: number</li>
  <li>quickSearch: string</li>
  <li>savedOptionsId: string (set an id if you want the options to be saved)</li>
  <li>selectedItems: any[]</li>
  <li>selectMode: "single" | "multi" | "none"</li>
  <li>showPaging: boolean</li>
  <li>showQuickSearch: boolean</li>
  <li>sortColumn: string</li>
  <li>sortDirection: "asc" | "desc"</li>
  <li>actions: {
    ariaLabel: string,
    disabled: string,
    disabledExpression: string,
    disabledTitle: string,
    disabledTitleExpression: string,
    download: {
      attachmentId: string;
      logAction: { entityId: string; entityTypeId: number; action: string };
      blobId: string,
      fileName: string
    },
    fallback: string (default replacement value when templating),
    formAction: string (executes a form post with the item model as the form model and this string as the action code),
    key: string,
    name: string,
    params: object (represents query string params),
    permissions: string[],
    showErrorSnack:boolean (whether to show a snackbar message with an error in response to a formAction),
    successExpression: string (expression exexcuted on a successful formAction),
    url: string (can use template notation e.g. \${submissionId}),
    visibility: boolean (can be templated),
    visibilityExpression: string (expresssion that resolves to boolean, has access to rootModel, model & context)
  }</li>
  <li>actionsMinWidth: string</li>
  <li>actionsMaxWidth: string</li>
  <li>refreshInterval: number (in milliseconds: 1000 = 1 second)</li>
  <li>refreshSignalR: { hub: string; event: string; group: string }</li>
  <li>detailDialogFormId: string</li>
  <li>detailDialogTitleTemplate: string (templated string for the dialog title e.g. '\${fileName}')</li>
  <li>detailDialogTriggerIds: string[] (ids of the columns to be used to trigger the dialog)</li>
  <li>detailDialogMaxWidth: string</li>
  <li>detailDialogMinWidth: string</li>
  <li>detailDialogWidth: string</li>
  <li>selectPosition: number</li>
  <li>previewDialogFormId: string</li>
  <li>previewDialogTitle: string</li>
  <li>previewDialogMaxWidth: string</li>
  <li>previewDialogMinWidth: string</li>
  <li>previewDialogWidth: string</li>
  <li>requestDialogFormId: string</li>
  <li>requestDialogTitle: string</li>
  <li>requestDialogMaxWidth: string</li>
  <li>requestDialogMinWidth: string</li>
  <li>requestDialogWidth: string</li>
  <li>requestActionKey: string</li>
  <li>requestActionRouteId: string</li>
  <li>enableSelectAll: boolean</li>
  <li>actionsTabs: string</li>
  <li>isDirtyOnSelectChange: boolean</li>
  <li>refreshId: string (an id that we can use in removeOfflineEntity Output effect)</li>
  <li>ariaTableName: string (an accessible name for the table)</li>
  <li>ariaLabelField: string (checkboxes are already prefixed with 'Checkbox' so will appear 'Checkbox yourLabel')</li>
  <li>ariaColumnDefinitions: string[] (this will override the ariaLabelField, it needs to match the columnDefinitions id you have set/want)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    filterText: 'Filter',
    filterTitle: 'Filter Results',
    cancelText: 'Cancel',
    clearText: 'Clear',
    enableLoadingIndicator: true,
    pageSize: 10,
  },
}</pre>
`;
