import { Component, ChangeDetectionStrategy } from '@angular/core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-message-type',
  templateUrl: './message-type.component.html',
  styleUrls: ['./message-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageTypeComponent extends BaseViewTypeComponent {}
