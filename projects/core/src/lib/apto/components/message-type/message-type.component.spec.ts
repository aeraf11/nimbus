import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { ListFilterService, StateService } from '@nimbus/core/src/lib/core';
import { ListService } from '../../../core/services/list.service';
import { MockListService } from '../../../core/services/list.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { BlobService } from './../../../core/services/blob.service';
import { MockBlobService } from './../../../core/services/blob.service.spec';
import { MessageTypeComponent } from './message-type.component';
import { DataCardsComponent } from '../../../core/components/data-cards/data-cards.component';
import { MockListFilterService } from '../../../core/services/list-filter-service/list-filter.service.spec';

describe('MessageTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'message',
              component: MessageTypeComponent,
            },
          ],
        },
        imports: [MatDialogModule],
        providers: [
          DataCardsComponent,
          { provide: StateService, useClass: MockStateService },
          { provide: ListService, useClass: MockListService },
          { provide: ListFilterService, useClass: MockListFilterService },
          { provide: BlobService, useClass: MockBlobService },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'message',
        type: 'message',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
