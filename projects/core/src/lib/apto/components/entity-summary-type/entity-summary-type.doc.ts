export const EntitySummaryDoc = `
<h3>Template Options</h3>
<ul>
  <li>entitySummaryItems: object ({ icon?: { name?: string; colour?: string; size?: string; }; label?: { text?: string; colour?: string; size?: string; }; text?: { text?: string; colour?: string; size?: string; }; isDivider?: boolean })</li>
  <li>entitySummaryUrl: string</li>
  <li>entitySummaryIsUrlRoute: boolean</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    entitySummaryItems: [],
    entitySummaryUrl: '',
    entitySummaryIsUrlRoute: false,
  }
}</pre>
`;
