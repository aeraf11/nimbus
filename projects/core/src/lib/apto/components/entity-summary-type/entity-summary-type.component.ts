import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BaseTypeComponent } from './../base-type/base-type.component';
import { FormsHelperService } from '@nimbus/core/src/lib/core';

interface EntitySummaryItem {
  icon?: {
    name?: string;
    colour?: string;
    size?: string;
  };
  label?: {
    text?: string;
    colour?: string;
    size?: string;
  };
  text?: {
    property?: string;
    text?: string;
    colour?: string;
    size?: string;
  };
  isDivider?: boolean;
}

@Component({
  selector: 'ni-entity-summary-type',
  templateUrl: './entity-summary-type.component.html',
  styleUrls: ['./entity-summary-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntitySummaryTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      entitySummaryItems: [],
      entitySummaryUrl: '',
      entitySummaryIsUrlRoute: false,
    },
  };

  entitySummaryItems: EntitySummaryItem[] = [];
  entitySummaryUrl = '';

  constructor(change: ChangeDetectorRef, private formsHelper: FormsHelperService) {
    super(change);
  }

  override ngOnInit(): void {
    super.ngOnInit();

    this.entitySummaryItems = this.props['entitySummaryItems'] ?? [];
    this.entitySummaryUrl = this.formsHelper
      .simpleTemplate(this.props['entitySummaryUrl'], this.formControl.value, '')
      .toString();
  }
}
