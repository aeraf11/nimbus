import { ChangeDetectorRef, Component } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-file-manager-type',
  templateUrl: './file-manager-type.component.html',
  styleUrls: ['./file-manager-type.component.scss'],
})
export class FileManagerTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      itemTableName: 'offlineUploads',
      dialogView: false,
      noResultsText: 'No files found',
    },
  };

  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
