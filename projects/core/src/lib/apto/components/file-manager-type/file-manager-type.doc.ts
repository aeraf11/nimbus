export const FileManagerDoc = `
<h3>Props</h3>
<ul>
  <li>itemTableName: string</li>
  <li>dialogView: boolean</li>
  <li>noResultsText: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    itemTableName: 'devGallery',
    dialogView: 'false',
    noResultsText: 'No files found'
}</pre>
`;
