import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';
import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'ni-search-result-case-view-type',
  templateUrl: './search-result-case-view-type.component.html',
  styleUrls: ['./search-result-case-view-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchResultCaseViewTypeComponent extends BaseViewTypeComponent implements OnInit {
  get openResultsInNewTab(): boolean {
    return this.props['openResultInNewTab'];
  }

  get operationId(): string {
    return this.formControl.value.operationId;
  }

  get operationName(): string {
    return this.formControl.value.operationName;
  }

  get operationAlias(): string {
    return this.formControl.value.operationAlias;
  }
}
