import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { BaseTypeComponent } from './../base-type/base-type.component';
import { Component, ChangeDetectionStrategy, ChangeDetectorRef, OnInit, ViewEncapsulation } from '@angular/core';
import { ChartDataService, FormsHelperService } from '@nimbus/core/src/lib/core';
import { ChartDataAccumulation, ChartType, ChartDataItem } from '@nimbus/core/src/lib/core';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@UntilDestroy()
@Component({
  selector: 'ni-chart-pie-type',
  templateUrl: './chart-pie-type.component.html',
  styleUrls: ['./chart-pie-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ChartPieTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      flexPosition: 'center center',
      legend: {
        visible: true,
        toggleVisibility: false,
        position: 'custom',
        width: '30%',
        height: '80%', //There's no setting for tooltip position, so reducing the height is the next best thing to stop overlap
        location: { x: 240, y: 0 },
      },
      dataLabel: {
        visible: false,
        position: 'Inside',
        name: '${point.value}',
        font: {
          color: 'white',
          fontWeight: 'Bold',
          size: '14px',
        },
      },
      tooltip: {
        enable: true,
        textStyle: { fontFamily: 'inherit' },
      },
      startAngle: 0,
      endAngle: 360,
      innerRadius: '0%',
      enableAnimation: true,
      title: '',
      subTitle: '',
      titleProportion: 0.2,
      subTitleProportion: 0.08,
      centerTitles: false,
      chartLegendPadding: '5%',
      width: '100%',
      height: '100%',
    },
  };

  model$?: Observable<ChartDataAccumulation | undefined>;
  data$?: Observable<ChartDataItem[] | undefined>;
  hasData$?: Observable<boolean>;
  title$?: Observable<string>;
  subTitle$?: Observable<string>;

  constructor(
    public override change: ChangeDetectorRef,
    private dataSourceSerivce: ChartDataService,
    private formsHelper: FormsHelperService
  ) {
    super(change);
  }

  override ngOnInit() {
    super.ngOnInit();

    this.handleData();

    this.formControl.valueChanges
      .pipe(
        untilDestroyed(this),
        tap(() => this.handleData())
      )
      .subscribe();
  }

  handleData() {
    const params = this.dataSourceSerivce.getParams(this.props['params'], this.formState);
    this.model$ = <Observable<ChartDataAccumulation>>(
      this.dataSourceSerivce.getChartData(ChartType.Pie, this.formControl.value, this.props['dataSource'], params)
    );
    this.data$ = this.model$.pipe(map((model) => model?.data));
    this.hasData$ = this.data$.pipe(map((data) => (data && data.length > 0 ? true : false)));
    this.title$ = this.model$.pipe(map((model) => this.formsHelper.simpleTemplate(this.props['title'], model, '')));
    this.subTitle$ = this.model$.pipe(
      map((model) => this.formsHelper.simpleTemplate(this.props['subTitle'], model, ''))
    );
  }
}
