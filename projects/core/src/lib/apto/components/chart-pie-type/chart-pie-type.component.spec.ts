import { MockStateService, StateService } from '@nimbus/core/src/lib/core';
import { MockChartDataService } from './../../../core/services/chart-data.service.spec';
import { ChartDataService } from './../../../core/services/chart-data.service';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChartPieTypeComponent } from './chart-pie-type.component';
import { FormlyTestComponent, buildFormlyTestModule } from '@nimbus/core/src/lib/core';

describe('ChartPieTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'chartpie',
              component: ChartPieTypeComponent,
            },
          ],
        },
        imports: [RouterTestingModule],
        providers: [
          { provide: StateService, useClass: MockStateService },
          { provide: ChartDataService, useClass: MockChartDataService },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'chartpie',
        type: 'chartpie',
      },
    ];
  });

  it('should create', () => {
    // Assert
    expect(component).toBeTruthy();
  });
});
