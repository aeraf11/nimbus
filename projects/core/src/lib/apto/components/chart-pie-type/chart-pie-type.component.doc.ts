export const ChartPieDoc = `
<h3>Available Data Sources</h3>
<ul>
  <li>PhanerosSubmissionExhibitsByType</li>
  <li>PhanerosSubmissionExhibitsByStatus</li>
  <li>PhanerosSubmissionExhibitsByLocation</li>
  <li>PhanerosSubmissionExhibitsByPriority</li>
</ul>
<h3>Template Options</h3>
<ul>
  <li>legend: object - see <a href="https://ej2.syncfusion.com/angular/documentation/api/chart/legendSettings/">SyncFusion docs</a></li>
  <li>dataLabel: object - see <a href="https://ej2.syncfusion.com/angular/documentation/api/chart/dataLabelSettings/">SyncFusion docs</a></li>
  <li>tooltip: object - see <a href="https://ej2.syncfusion.com/angular/documentation/api/chart/tooltipSettings/">SyncFusion docs</a></li>
  <li>startAngle: number</li>
  <li>endAngle: number</li>
  <li>innerRadius: string (as pixel or percent, turns pie into donut)</li>
  <li>enableAnimation: boolean</li>
  <li>title: string (templated, can use \${property} to access rootModel)</li>
  <li>subTitle: string (templated, can use \${property} to access rootModel)</li>
  <li>titleSize: string (font size)</li>
  <li>subTitleSize: string (font size)</li>
  <li>titleProportion: number (font size as a proportion of the diameter of the graph, e.g. 0.2)</li>
  <li>subTitleProportion: number (font size as a proportion of the diameter of the graph, e.g. 0.08)</li>
  <li>centerTitles: boolean (turn the center titles on or off)</li>
  <li>chartLegendPadding: string (as pixel or percent, gap between pie and legend)</li>
  <li>width: string (as pixel or percent of parent container)</li>
  <li>height: string (as pixel or percent of parent container)</li>
  <li>dataSource: string (either use this in conjunction with params, OR map the datasource in the model)</li>
  <li>params: array (name the properties you want brought in to the datasource arguments, both page's context and rootModel will be searched)</li>
  <li>flexPosition: string</li>
</ul>
<h3>Default Options</h3>
<pre>props: {
  legend: {
    visible: true,
    toggleVisibility: false,
    position: 'custom',
    width: '30%',
    height: '80%', (100% may cause problems with hover overflow)
    location: { x: 240, y: 0 },
  },
  dataLabel: {
    visible: false,
    position: 'Inside',
    name: '\${point.value}',
    font: {
      color: 'white',
      fontWeight: 'Bold',
      size: '14px',
    },
  },
  tooltip: {
    enable: true,
  },
  startAngle: 0,
  endAngle: 360,
  innerRadius: '0%',
  enableAnimation: true,
  title: '',
  subTitle: '',
  titleSize: undefined,
  subTitleSize: undefined,
  titleProportion: 0.2,
  subTitleProportion: 0.08,
  centerTitles: false,
  chartLegendPadding: '5%',
  width: '100%',
  height: '100%',
}</pre>
`;
