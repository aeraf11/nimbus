import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseViewTypeComponent } from './../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-team-member-view-type',
  templateUrl: './team-member-view-type.component.html',
  styleUrls: ['./team-member-view-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeamMemberViewTypeComponent extends BaseViewTypeComponent {}
