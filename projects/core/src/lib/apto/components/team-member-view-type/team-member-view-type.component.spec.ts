import { ComponentFixture, TestBed } from '@angular/core/testing';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { TeamMemberViewTypeComponent } from './team-member-view-type.component';
import { FormlyTestComponent } from '../../../core/formly/test/formly-test.component';

describe('TeamMemberViewTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'teammember',
              component: TeamMemberViewTypeComponent,
            },
          ],
        },
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'teammember',
        type: 'teammember',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
