export const TableDoc = `
<h3>Props</h3>
<ul>
  <li>columnDefinitions: object ({key: string, columnName: string, tab?: string, dataType?: DateType (see below),  filterParams: { formlyType: 'input' | 'daterange' | 'lookup' | 'multichoice' | 'select2' }})</li>
  <li>formId: string</li>
  <li>enableAdd: boolean</li>
  <li>enableDelete: boolean</li>
  <li>enableEdit: boolean</li>
  <li>enableHide: boolean</li>
  <li>saveText: string</li>
  <li>cancelText: string</li>
  <li>deleteText: string</li>
  <li>addText: string</li>
  <li>hideText: string</li>
  <li>disabled: boolean</li>
  <li>itemLimit: number</li>
  <li>required: boolean</li>
  <li>dialogWidth: string</li>
  <li>label: string</li>
  <li>placeholder: string</li>
  <li>description: string</li>
  <li>focus: boolean</li>
  <li>title: string | null</li>
  <li>isRaised: boolean</li>
  <li>defaultValues: object (add a model object of defaults values for new items)<li>
  <li>ariaTableName: string (an accessible name for the table)</li>
  <li>rowAriaLabel: string (an accessible label for the row)</li>
</ul>

<h4>Data Type</h4>
<ul>
  <li>text</li>
  <li>date</li>
  <li>dateTime</li>
  <li>multiChoice</li>
  <li>lookup</li>
  <li>lookupHierarchical</li>
  <li>selectOption</li>
  <li>profile</li>
  <li>status</li>
  <li>icon</li>
</ul>

<h3>Default Options</h3>
<pre>{
  props: {
    enableAdd: true,
    enableDelete: true,
    enableEdit: true,
    enableHide: false,
    saveText: 'Save',
    deleteText: 'Delete',
    cancelText: 'Cancel',
    addText: 'Add',
    hideText: 'Hide',
    itemLimit: null,
    title: null,
    isRaised: true,
    dialogWidth: undefined,
  },
}</pre>
`;
