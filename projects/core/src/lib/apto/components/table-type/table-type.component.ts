import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { GroupConfig, StateService } from '@nimbus/core/src/lib/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseTypeComponent } from '../base-type/base-type.component';

@UntilDestroy()
@Component({
  selector: 'ni-table-type',
  templateUrl: './table-type.component.html',
  styleUrls: ['./table-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TableTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      enableAdd: true,
      enableDelete: true,
      enableEdit: true,
      enableHide: false,
      saveText: 'Save',
      deleteText: 'Delete',
      cancelText: 'Cancel',
      addText: 'Add',
      hideText: 'Hide',
      itemLimit: null,
      title: null,
      isRaised: true,
      dialogWidth: undefined,
    },
  };

  formDefinition$!: Observable<FormlyFieldConfig[] | undefined>;

  get groupBy(): GroupConfig | null {
    return this.props['enableGroup']
      ? {
          selfKeyPath: this.props['groupByParent'],
          parentKeyPath: this.props['groupByChild'],
        }
      : null;
  }

  constructor(private state: StateService, change: ChangeDetectorRef) {
    super(change);
  }

  override ngOnInit() {
    super.ngOnInit();

    if (this.props['formId']) {
      this.formDefinition$ = this.state.selectForm(this.props['formId']).pipe(
        untilDestroyed(this),
        map((configs) => configs?.fields)
      );
    } else {
      // this.formDefinition$ = of(this.field.fieldArray?.fieldGroup);
    }
  }
}
