import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { FormlyModule } from '@ngx-formly/core';
import { StateService, TableEditorComponent } from '@nimbus/core/src/lib/core';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { TableTypeComponent } from './table-type.component';
import { FormlyTestComponent } from '../../../core/formly/test/formly-test.component';

describe('TableTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatDialogModule,
        FormlyModule.forRoot({
          types: [
            {
              name: 'table',
              component: TableTypeComponent,
            },
          ],
        }),
      ],
      providers: [
        {
          provide: StateService,
          useClass: MockStateService,
        },
      ],
      declarations: [FormlyTestComponent, TableEditorComponent, TableTypeComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
  });

  it('should create', () => {
    const componentInstance = fixture.componentInstance;
    componentInstance.fields = [
      {
        key: 'table',
        type: 'table',
      },
    ];
    fixture.detectChanges();
    expect(componentInstance).toBeTruthy();
  });
});
