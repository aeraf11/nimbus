import { FieldWrapper } from '@ngx-formly/core';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './lat-long-wrapper.component.html',
  styleUrls: ['./lat-long-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LatLongWrapperComponent extends FieldWrapper implements OnInit {
  ngOnInit(): void {
    if (!this.field.fieldGroup?.[0].formControl) throw new Error('Please include a "latitude" field');
    if (!this.field.fieldGroup?.[1].formControl) throw new Error('Please include a "longitude" field');
  }

  getCurrentLocation(): void {
    navigator.geolocation.getCurrentPosition((position) => {
      this.field.fieldGroup?.[0].formControl?.setValue(position.coords.latitude);
      this.field.fieldGroup?.[1].formControl?.setValue(position.coords.longitude);
    });
  }
}
