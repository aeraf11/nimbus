import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, Optional } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FieldType } from '@ngx-formly/material/form-field';
import { CardListView, CardListWrapperComponent, MediaService, OptionsService } from '@nimbus/core/src/lib/core';

@UntilDestroy()
@Component({
  selector: 'ni-card-list-view-type',
  templateUrl: './card-list-view-type.component.html',
  styleUrls: ['./card-list-view-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardListViewTypeComponent extends FieldType<FormlyFieldConfig> implements OnInit {
  type = '';
  noDefaultView = false;
  noBreakpointView = false;

  constructor(
    @Optional() private cardListWrapper: CardListWrapperComponent,
    private change: ChangeDetectorRef,
    private mediaService: MediaService,
    private optionsService: OptionsService
  ) {
    super();

    if (this.cardListWrapper) {
      this.cardListWrapper.viewEmit.subscribe((view: CardListView) => {
        if (this.props['savedViewId']) {
          this.optionsService.saveOptions({ id: this.props['savedViewId'], view });
        }
        this.type = view.type;
        this.field.fieldGroup?.forEach((field) => (field.type = this.type));
        this.change.markForCheck();
      });
    }
  }

  ngOnInit(): void {
    if (this.cardListWrapper) {
      this.cardListWrapper.views = this.props['views'];
    }

    if (this.field.fieldGroup?.length !== 1) {
      throw new Error('Polymorph component requires 1 field');
    }

    this.setDefaultView();
    this.setBreakpointView();

    if (this.noDefaultView && this.noBreakpointView) {
      throw new Error(
        `Polymorph component requires either a "polymorphDefaultView" or a "polymorphBreakpointView" prop`
      );
    }
  }

  getCardListViewByType(type: string): CardListView | null {
    return this.props['views']?.filter((view: CardListView) => view.type === type)[0] ?? null;
  }

  private setDefaultView(): void {
    const savedOptions = this.props['savedViewId'] ? this.optionsService.loadOptions(this.props['savedViewId']) : null;
    if (savedOptions && savedOptions.view && this.cardListWrapper) {
      this.cardListWrapper.viewEmit.emit(savedOptions.view);
      return;
    }

    const defaultViewType = this.props['polymorphDefaultType'] || this.props['defaultViewId'];
    if (defaultViewType) {
      const defaultView = this.getCardListViewByType(defaultViewType);
      if (defaultView && this.cardListWrapper) {
        this.cardListWrapper.viewEmit.emit(defaultView);
      }
    } else {
      this.noDefaultView = true;
    }
  }

  private setBreakpointView(): void {
    if (!this.noDefaultView) {
      console.warn(`You have set a defaultView, this will be overridden by the breakpoint view`);
    }

    const breakpointViewTypes = this.props['polymorphBreakpointTypes'];
    if (breakpointViewTypes) {
      this.mediaService
        .getBreakpointValue(breakpointViewTypes)
        .pipe(untilDestroyed(this))
        .subscribe((breakpointViewType) => {
          const breakpointView = this.getCardListViewByType(breakpointViewType);
          if (breakpointView && this.cardListWrapper) {
            this.cardListWrapper.viewEmit.emit(breakpointView);
          }
        });
    } else {
      this.noBreakpointView = true;
    }
  }
}
