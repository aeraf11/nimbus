import { ComponentFixture, TestBed } from '@angular/core/testing';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { CardListViewTypeComponent } from './card-list-view-type.component';

describe('CardListViewTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'cardlist',
              component: CardListViewTypeComponent,
            },
          ],
        },
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'cardlist',
        type: 'cardlist',
        props: {
          defaultViewId: 'test',
          views: [
            {
              label: 'Test View',
              type: 'test',
              icon: 'fas fa-th',
            },
          ],
        },
        fieldGroup: [
          {
            template: 'test',
          },
        ],
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
