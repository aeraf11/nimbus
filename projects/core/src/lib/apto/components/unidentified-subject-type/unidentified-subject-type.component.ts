import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-unidentified-subject-type',
  templateUrl: './unidentified-subject-type.component.html',
  styleUrls: ['./unidentified-subject-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UnidentifiedSubjectTypeComponent extends BaseViewTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      viewType: 'grid',
    },
  };

  constructor(private cdr: ChangeDetectorRef) {
    super(cdr);
  }

  override ngOnInit(): void {
    super.ngOnInit();
  }
}
