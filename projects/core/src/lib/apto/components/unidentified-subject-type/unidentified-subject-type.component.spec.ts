import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnidentifiedSubjectTypeComponent } from './unidentified-subject-type.component';
import { FormControl } from '@angular/forms';

describe('UnidentifiedSubjectTypeComponent', () => {
  let component: UnidentifiedSubjectTypeComponent;
  let fixture: ComponentFixture<UnidentifiedSubjectTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [UnidentifiedSubjectTypeComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(UnidentifiedSubjectTypeComponent);
    component = fixture.componentInstance;
    component.field = {
      type: 'unidentifiedsubject',
      props: {},
      formControl: new FormControl(''),
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
