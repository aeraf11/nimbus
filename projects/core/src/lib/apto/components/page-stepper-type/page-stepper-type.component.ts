import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-page-stepper-type',
  templateUrl: './page-stepper-type.component.html',
  styleUrls: ['./page-stepper-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PageStepperTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      progressBar: {
        enabled: false,
        conjunction: 'of',
      },
    },
  };
}
