export const PageStepperDoc = `
<h3>Template Options</h3>
<ul>
  <li>stepDivider: object { enabled: boolean; height: string; margin: string; }</li>
  <li>progressBar: object { enabled: boolean; width: string; height: string; subLabel: string; conjunction: string; trackColour: string; progressColour: string;}</li>
</ul>
<h3>Default Options</h3>
<pre>{
  progressBar: {
    enabled: false,
    conjunction: 'of',
  }
}</pre>
`;
