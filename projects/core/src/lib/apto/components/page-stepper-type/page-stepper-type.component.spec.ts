import { ComponentFixture, TestBed } from '@angular/core/testing';
import { StateService } from '../../../core';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { PageStepperTypeComponent } from './page-stepper-type.component';

describe('PageStepperTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'pagestepper',
              component: PageStepperTypeComponent,
            },
          ],
        },
        providers: [{ provide: StateService, useClass: MockStateService }],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.model = {
      entitymedia: {},
    };

    component.fields = [
      {
        key: 'pagestepper',
        type: 'pagestepper',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
