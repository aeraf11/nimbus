export const DisplayGroupDoc = `
<p>Probably this type of function best served by a wrapper panel.</p>
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>description: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
</ul>
<h3>Default Options</h3>
<pre>{}</pre>
`;
