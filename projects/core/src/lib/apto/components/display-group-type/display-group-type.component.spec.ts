import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { DisplayGroupTypeComponent } from './display-group-type.component';

describe('DisplayGroupTypeComponent', () => {
  let component: FormlyTestComponent;
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormlyTestComponent, DisplayGroupTypeComponent],
      imports: [
        ReactiveFormsModule,
        FormlyModule.forRoot({
          types: [
            {
              name: 'displaygroup',
              component: DisplayGroupTypeComponent,
            },
          ],
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
  });

  it('should create', () => {
    const componentInstance = fixture.componentInstance;
    componentInstance.fields = [
      {
        key: 'displaygroup',
        type: 'displaygroup',
      },
    ];
    fixture.detectChanges();
    expect(componentInstance).toBeTruthy();
  });
});
