import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-display-group-type',
  templateUrl: './display-group-type.component.html',
  styleUrls: ['./display-group-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DisplayGroupTypeComponent extends BaseTypeComponent {
  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
