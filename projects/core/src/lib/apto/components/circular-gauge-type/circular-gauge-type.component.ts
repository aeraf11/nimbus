import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import {
  ChartDataAccumulation,
  ChartDataService,
  ChartType,
  FormsHelperService,
  CircularGaugePointer,
  CircularGaugeRange,
  CircularChartLegend,
} from '@nimbus/core/src/lib/core';
import { Observable } from 'rxjs';
import { distinctUntilChanged, map, tap } from 'rxjs/operators';
import { BaseTypeComponent } from './../base-type/base-type.component';

@UntilDestroy()
@Component({
  selector: 'ni-circular-gauge-type',
  templateUrl: './circular-gauge-type.component.html',
  styleUrls: ['./circular-gauge-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CircularGaugeTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      flexPosition: 'center center',
      width: '100%',
      height: '100%',
      colours: ['red', 'green', 'blue'],
      title: '${total}',
      subTitle: '',
      titleProportion: 0.2,
      subTitleProportion: 0.08,
      barWidth: '10',
      barBackground: 'rgba(0, 0, 0, 0.25)',
      legend: {
        visible: true,
        width: '100%',
        height: '20%',
        titleProportion: 0.08,
        subTitleProportion: 0.04,
        padding: '10%',
      },
    },
  };

  colourIndex = 0;

  model$: Observable<ChartDataAccumulation | undefined> = new Observable();
  data$?: Observable<{ pointers: CircularGaugePointer[]; ranges: CircularGaugeRange[] }>;
  title$?: Observable<string>;
  subTitle$?: Observable<string>;
  total$?: Observable<number>;

  constructor(changes: ChangeDetectorRef, private service: ChartDataService, private formsHelper: FormsHelperService) {
    super(changes);
  }

  get legend(): CircularChartLegend {
    return {
      visible: this.props['legend'].visible,
      width: this.props['legend'].width,
      height: this.props['legend'].height,
      padding: this.props['legend'].padding,
      titleProportion: this.props['legend'].titleProportion,
      subTitleProportion: this.props['legend'].subTitleProportion,
      titleSize: this.props['legend'].titleSize,
      subTitleSize: this.props['legend'].subTitleSize,
    };
  }

  override ngOnInit(): void {
    super.ngOnInit();

    this.handleData();

    this.formControl.valueChanges
      .pipe(
        untilDestroyed(this),
        distinctUntilChanged(),
        tap(() => this.handleData())
      )
      .subscribe();
  }

  handleData() {
    const params = this.service.getParams(this.props['params'], this.formState);
    this.model$ = <Observable<ChartDataAccumulation>>(
      this.service.getChartData(ChartType.CircularGauge, this.formControl.value, this.props['dataSource'], params)
    );

    this.data$ = this.model$.pipe(map((model) => this.buildPointersRanges(model)));
    this.total$ = this.model$.pipe(map((model) => model?.total ?? 100));
    this.title$ = this.model$.pipe(map((model) => this.formsHelper.simpleTemplate(this.props['title'], model, '')));
    this.subTitle$ = this.model$.pipe(
      map((model) => this.formsHelper.simpleTemplate(this.props['subTitle'], model, ''))
    );
  }

  private buildPointersRanges(model?: ChartDataAccumulation): {
    pointers: CircularGaugePointer[];
    ranges: CircularGaugeRange[];
  } {
    const pointers: CircularGaugePointer[] = [];
    const ranges: CircularGaugeRange[] = [];
    const data = model?.data;

    if (!model || !data) {
      return { pointers, ranges };
    }

    let radiusProportion = 1;
    data.forEach((item) => {
      const radius = `${50 * radiusProportion}px`;
      ranges.push({
        start: 0,
        end: model.total,
        color: this.props['barBackground'],
        radius,
      });

      pointers.push({
        value: item.value,
        type: 'RangeBar',
        pointerWidth: this.props['barWidth'],
        radius,
        color: this.getColourInSequence(),
        text: item.name,
      });

      radiusProportion = radiusProportion - 0.1;
    });

    return { pointers, ranges };
  }

  private getColourInSequence(): string {
    const colour = this.props['colours'][this.colourIndex];
    this.colourIndex++;
    if (this.colourIndex === this.props['colours'].length) {
      this.colourIndex = 0;
    }
    return colour;
  }
}
