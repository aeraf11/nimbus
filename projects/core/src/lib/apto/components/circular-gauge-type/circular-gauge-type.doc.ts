export const CircularGaugeDoc = `
<h3>Template Options</h3>
<ul>
  <li>required: boolean</li>
  <li>enableRemove: boolean</li>
  <li>colour: string (primary | access | warn)</li>

  <li>flexPosition: string</li>
  <li>width: string (as pixel or percent of parent container)</li>
  <li>height: string (as pixel or percent of parent container)</li>
  <li>colours: string[]</li>
  <li>title: string</li>
  <li>showTitle: boolean</li>
  <li>titleProportion: number (font size as a proportion of the diameter of the graph, e.g. 0.2)</li>
  <li>subTitleProportion: number (font size as a proportion of the diameter of the graph, e.g. 0.08)</li>
  <li>showTotal: boolean</li>
  <li>barWidth: string</li>
  <li>barBackground: string (as hex colour e.g. #848804 or known CSS colour e.g. yellow), is optional as will fallback to theme primary colour</li>
  <li>legend: { visible: boolean, width: string, height: string, titleProportion: number, subTitleProportion: number }</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    flexPosition: 'center center',
    width: '100%',
    height: '100%',
    maxWidth: '100%',
    colours: ['red', 'green', 'blue'],
    title: '',
    showTitle: true,
    showTotal: true,
    barWidth: '10',
    barBackground: 'rgba(0, 0, 0, 0.25)',
    legend: { visible: true, width: '100%', height: '20%' },
  },
}</pre>
`;
