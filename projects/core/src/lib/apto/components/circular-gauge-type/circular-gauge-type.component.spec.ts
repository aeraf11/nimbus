import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ChartDataService, StateService } from '@nimbus/core/src/lib/core';
import { MockChartDataService } from '../../../core/services/chart-data.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { CircularGaugeTypeComponent } from './circular-gauge-type.component';

describe('CircularGaugeTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'circulargauge',
              component: CircularGaugeTypeComponent,
            },
          ],
        },
        providers: [
          { provide: StateService, useClass: MockStateService },
          { provide: ChartDataService, useClass: MockChartDataService },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'circulargauge',
        type: 'circulargauge',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
