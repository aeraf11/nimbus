import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { SlideToggleTypeComponent } from './slide-toggle-type.component';

describe('SlideToggleTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatSlideToggleModule,
        FormlyModule.forRoot({
          types: [
            {
              name: 'toggle',
              component: SlideToggleTypeComponent,
            },
          ],
        }),
      ],
      declarations: [SlideToggleTypeComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
  });

  it('should create', () => {
    const componentInstance = fixture.componentInstance;
    componentInstance.fields = [
      {
        key: 'toggle',
        type: 'toggle',
      },
    ];
    fixture.detectChanges();
    expect(componentInstance).toBeTruthy();
  });
});
