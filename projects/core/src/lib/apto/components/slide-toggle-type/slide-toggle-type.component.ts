import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, ChangeDetectionStrategy, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-slide-toggle-type',
  templateUrl: './slide-toggle-type.component.html',
  styleUrls: ['./slide-toggle-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SlideToggleTypeComponent extends BaseTypeComponent implements OnInit {
  @ViewChild(MatSlideToggle, { static: true }) slideToggle!: MatSlideToggle;
  override defaultOptions = {
    props: {
      hideFieldUnderline: true,
      floatLabel: 'always' as const,
      hideLabel: true,
    },
  };

  override onContainerClick(event: MouseEvent): void {
    this.slideToggle.focus();
    super.onContainerClick(event);
  }

  constructor(private liveAnnouncer: LiveAnnouncer, change: ChangeDetectorRef) {
    super(change);
  }

  override ngOnInit() {
    super.ngOnInit();

    this.slideToggle.checked
      ? this.liveAnnouncer.announce('Slide toggle checked')
      : this.liveAnnouncer.announce('Slide toggle unchecked');
  }
}
