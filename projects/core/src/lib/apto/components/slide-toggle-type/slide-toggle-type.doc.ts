export const SlideToggleDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>description: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>hideFIeldUnderline: boolean</li>
  <li>floatLabel: string ("always" | "never" | "auto")</li>
  <li>hideLabel: boolean</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    hideFieldUnderline: true,
    floatLabel: 'always' as const,
    hideLabel: true,
  },
}</pre>
`;
