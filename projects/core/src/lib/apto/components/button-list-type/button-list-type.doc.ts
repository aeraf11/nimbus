export const ButtonListDoc = `
<h3>Props</h3>
<ul>
  <li>enableClear: boolean</li>
  <li>clearText: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    enableClear: false,
    clearText: 'Clear',
  },
}</pre>
`;
