import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ButtonListTypeButton } from '../../models';
import { BaseTypeComponent } from './../base-type/base-type.component';

@Component({
  selector: 'ni-button-list-type',
  templateUrl: './button-list-type.component.html',
  styleUrls: ['./button-list-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ButtonListTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      enableClear: false,
      clearText: 'Clear',
    },
  };

  buttonsToShow: ButtonListTypeButton[] = [];

  override ngOnInit(): void {
    super.ngOnInit();
    this.buttonsToShow = this.props?.buttons?.filter((button: ButtonListTypeButton) => !button.isHidden) ?? [];
  }
}
