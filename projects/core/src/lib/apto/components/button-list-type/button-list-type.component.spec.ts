import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormlyModule } from '@ngx-formly/core';
import { ButtonListTypeComponent } from './button-list-type.component';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';

describe('ButtonListTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FormlyTestComponent, ButtonListTypeComponent],
      imports: [
        FormlyModule.forRoot({
          types: [
            {
              name: 'buttonlist',
              component: ButtonListTypeComponent,
            },
          ],
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
  });

  it('should create', () => {
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'buttonlist',
        type: 'buttonlist',
      },
    ];
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
