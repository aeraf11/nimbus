import { BaseTypeComponent } from './../base-type/base-type.component';
import { Component, ChangeDetectionStrategy, ElementRef, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'ni-progress-type',
  templateUrl: './progress-type.component.html',
  styleUrls: ['./progress-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProgressTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      progressValue: 0,
      progressMax: 100,
      showValue: true,
    },
  };

  isCustomColour = false;

  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
