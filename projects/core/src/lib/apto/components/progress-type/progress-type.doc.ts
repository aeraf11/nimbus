export const ProgressDoc = `
<h3>Template Options</h3>
<ul>
  <li>progressValue: number</li>
  <li>progressMax: number</li>
  <li>showValue: boolean</li>
  <li>progressColour: string (as hex colour e.g. #848804 or known CSS colour e.g. yellow), is optional as will fallback to theme primary colour</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    progressValue: 0,
    progressMax: 100,
    showValue: true,
  },
}</pre>
`;
