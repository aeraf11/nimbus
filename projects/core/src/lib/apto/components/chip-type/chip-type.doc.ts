export const ChipDoc = `
<h3>Template Options</h3>
<ul>
  <li>required: boolean</li>
  <li>enableRemove: boolean</li>
  <li>removeButtonAriaLabel: string</li>
  <li>listAriaLabel: string</li>
  <li>colour: string (primary | access | warn)</li>
</ul>
<h3>Default Options</h3>
<pre>{}</pre>
`;
