import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseTypeComponent } from './../base-type/base-type.component';

@Component({
  selector: 'ni-chip-type',
  templateUrl: './chip-type.component.html',
  styleUrls: ['./chip-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChipTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      ariaLabel: 'Remove Chip',
      removeButtonAriaLabel: 'List Items',
    },
  };

  onRemove(): void {
    if (this.formState?.selectionChanged) {
      this.formState.selectionChanged(this.value);
    }
  }
}
