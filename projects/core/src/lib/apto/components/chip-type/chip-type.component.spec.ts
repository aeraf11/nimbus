import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { ChipTypeComponent } from './chip-type.component';
import { ChipComponent } from '../../../core/components/chip/chip.component';

describe('ChipTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        declarations: [ChipComponent],
        config: {
          types: [
            {
              name: 'chip',
              component: ChipTypeComponent,
            },
          ],
        },
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.model = {
      chip: {
        value: 'value',
      },
    };

    component.fields = [
      {
        key: 'chip',
        type: 'chip',
        props: {
          valueProp: 'value',
        },
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
