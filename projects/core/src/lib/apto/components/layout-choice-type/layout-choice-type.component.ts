import { StateService } from '@nimbus/core/src/lib/core';
import { ChangeDetectionStrategy, Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { BaseTypeComponent } from './../base-type/base-type.component';
import { take } from 'rxjs/operators';

@Component({
  selector: 'ni-layout-choice-type',
  templateUrl: './layout-choice-type.component.html',
  styleUrls: ['./layout-choice-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LayoutChoiceTypeComponent extends BaseTypeComponent implements OnInit {
  items: any = [];

  constructor(change: ChangeDetectorRef, private state: StateService) {
    super(change);
  }

  override ngOnInit(): void {
    super.ngOnInit();

    this.state
      .selectDashboardLayout()
      .pipe(take(1))
      .subscribe((dashboardLayout: string) => this.setValue(dashboardLayout, dashboardLayout));

    this.formState?.contextChanged?.subscribe(() => {
      if (this.formState.context['isGridExpanded'] === true) {
        this.formControl.disable();
      } else {
        this.formControl.enable();
      }
    });

    this.items = this.props.options ?? [];
    this.change.markForCheck();
  }

  setValue(value: string, layoutKey: string): void {
    if (value !== this.formControl.value) {
      this.formControl.setValue(value);
      this.state.setDashboardLayout(value);
    }
    this.formState.context['layoutKey'] = layoutKey;
  }
}
