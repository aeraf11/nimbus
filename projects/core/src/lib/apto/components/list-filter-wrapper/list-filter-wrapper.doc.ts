export const ListFilterWrapperDoc = `
<h3>Template Options</h3>
<ul>
  <li>listFilterTitle: string</li>
  <li>listFilterEnableAdd: string</li>
  <li>listFilterAddUrl: string</li>
  <li>listFilterAddText: string</li>
  <li>listFilterEnableSearch: boolean</li>
  <li>listFilterSearchText: string</li>
  <li>listFilterEnableButtonIcons: string</li>
  <li>listFilterSearchButtonIcon: string</li>
  <li>listFilterSearchIcon: string</li>
  <li>listFilterSearchSubTitle: string</li>
  <li>listFilterAutoFocus: boolean</li>
  <li>listFilterAddButtonIcon: string</li>
  <li>listFilterSearchDialogWidth: string</li>
  <li>listFilterSearchDialogMaxWidth: string</li>
  <li>listFilterSearchResultOptions: json object</li>
  <li>listFilterIconButtonSize: string</li>
</ul>
<h3>Default Options</h3>
<pre>{}</pre>
`;
