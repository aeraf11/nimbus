import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UntilDestroy } from '@ngneat/until-destroy';
import { FieldWrapper } from '@ngx-formly/core';
import { FilterColumn } from '@nimbus/core/src/lib/core';
import { DialogFormData, EditorMode } from '@nimbus/core/src/lib/core';
import { DialogFormComponent } from '@nimbus/core/src/lib/core';

@UntilDestroy()
@Component({
  selector: 'ni-list-filter-wrapper',
  templateUrl: './list-filter-wrapper.component.html',
  styleUrls: ['./list-filter-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ListFilterWrapperComponent extends FieldWrapper implements OnInit {
  @HostBinding('class') cssClass?: string;
  itemCount = 0;

  filters: FilterColumn[] = [];
  userFilters: FilterColumn[] = [];
  filtersChanged = new EventEmitter<FilterColumn[]>();
  defaults = {
    listFilterAddButtonIcon: 'fas fa-plus',
    searchButtonIcon: 'fas fa-search',
    listFilterIconButtonSize: '16px',
  };

  constructor(private dialog: MatDialog) {
    super();
  }

  ngOnInit(): void {
    this.cssClass = `list-filter-wrapper ${this.to?.classNames}`;
  }

  openSearchDialog(): void {
    const data: DialogFormData = {
      model: {},
      fieldConfig: [
        {
          key: 'searchcases',
          wrappers: ['quicksearch', 'scroll'],
          props: {
            placeholder: 'Enter search term...',
            maxHeight: '400px',
            hideItemsUntilSearched: true,
            subtitle: this.props['listFilterSearchSubTitle'] ?? this.props['searchSubTitle'] ?? '',
            autoFocus: this.props['listFilterAutoFocus'] || this.props['autoFocus'],
          },
          fieldGroup: [
            {
              key: 'searchresults',
              type: 'datacards',
              props: this.props['listFilterSearchResultOptions'] ?? this.props['searchResultOptions'] ?? {},
            },
          ],
        },
      ],
      config: {
        enableDelete: false,
        enableEdit: false,
        titleTemplate: this.props['listFilterSearchText'] ?? this.props['searchText'] ?? 'Search',
        titleIcon: this.props['listFilterSearchIcon'] ?? this.props['searchIcon'] ?? '',
        isChildForm: true,
      },
      editorMode: EditorMode.None,
      disabled: false,
      options: this.options,
      context: { isEdit: false },
    };
    const dialogConfig: MatDialogConfig<DialogFormData> = { data };

    if (this.props['listFilterSearchDialogWidth'] ?? this.props['searchDialogWidth']) {
      dialogConfig.width = this.props['searchDialogWidth'];
    }

    if (this.props['listFilterSearchDialogMaxWidth'] ?? this.props['searchDialogMaxWidth']) {
      dialogConfig.maxWidth = this.props['listFilterSearchDialogMaxWidth'] ?? this.props['searchDialogMaxWidth'];
    }

    this.dialog.open(DialogFormComponent, dialogConfig);
  }
}
