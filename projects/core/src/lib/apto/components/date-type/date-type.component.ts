import { AppDateTimeAdapter } from '@nimbus/core/src/lib/core';
import { Component, ChangeDetectionStrategy } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';
import { ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'ni-date-type',
  templateUrl: './date-type.component.html',
  styleUrls: ['./date-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DateTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      appearance: 'outline',
      color: 'accent' as const, // workaround for https://github.com/angular/components/issues/18465,
      dateFormat: '',
      dateTimeFormat: '',
      disableMinutes: false,
      enableFuture: true,
      options: [],
      placeholderType: '',
      required: false,
      showSpinners: true,
      showTime: false,
      showSeconds: false,
      showTimeZone: false,
      stepHours: 1,
      stepMinutes: 1,
      stepSeconds: 1,
      timeFormat: '',
      touchUi: false,
    },
  };

  constructor(change: ChangeDetectorRef, public dateTimeAdapter: AppDateTimeAdapter) {
    super(change);
  }
}
