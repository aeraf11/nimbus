export const DateDoc = `
<h3>Props</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>description: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>appearance: string ("outline" | "legacy" | "fill" | "standard")</li>
  <li>showTime: boolean</li>
  <li>showTimeZone: boolean</li>
  <li>showSpinners: boolean</li>
  <li>showSeconds: boolean</li>
  <li>stepHours: number</li>
  <li>stepMinutes: number</li>
  <li>stepSeconds: number</li>
  <li>touchUi: boolean</li>
  <li>disableMinutes: boolean</li>
  <li>options: MatDatepickerInputHarness</li>
  <li>color: string ("primary" | "accent" | "warn" | undefined)</li>
  <li>enableFuture: boolean</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    appearance: 'outline',
    showTime: false,
    showTimeZone: false,
    showSpinners: true,
    showSeconds: false,
    stepHours: 1,
    stepMinutes: 1,
    stepSeconds: 1,
    touchUi: false,
    disableMinutes: false,
    options: [],
    color: 'accent' as const,
  },
}</pre>
`;

export const DateTimeDoc = `
<h3>Props</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>description: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>appearance: string ("outline" | "legacy" | "fill" | "standard")</li>
  <li>showTime: boolean</li>
  <li>showTimeZone: boolean</li>
  <li>showSpinners: boolean</li>
  <li>showSeconds: boolean</li>
  <li>stepHours: number</li>
  <li>stepMinutes: number</li>
  <li>stepSeconds: number</li>
  <li>touchUi: boolean</li>
  <li>disableMinutes: boolean</li>
  <li>options: MatDatepickerInputHarness</li>
  <li>color: string ("primary" | "accent" | "warn" | undefined)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    appearance: 'outline',
    showTime: true,
    showTimeZone: true,
    showSpinners: true,
    showSeconds: false,
    stepHours: 1,
    stepMinutes: 1,
    stepSeconds: 1,
    touchUi: false,
    disableMinutes: false,
    options: [],
    color: 'accent' as const,
  },
}</pre>
`;
