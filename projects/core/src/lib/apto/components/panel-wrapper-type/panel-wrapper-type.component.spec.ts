import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyFieldInput } from '@ngx-formly/material/input';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { PanelWrapperTypeComponent } from './panel-wrapper-type.component';

describe('PanelWrapperComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PanelWrapperTypeComponent],
      imports: [
        ReactiveFormsModule,
        FormlyModule.forRoot({
          types: [
            {
              name: 'input',
              component: FormlyFieldInput,
            },
          ],
          wrappers: [{ name: 'panel', component: PanelWrapperTypeComponent }],
        }),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
  });

  it('should create', () => {
    const componentInstance = fixture.componentInstance;
    componentInstance.fields = [
      {
        key: 'panel',
        wrappers: ['panel'],
        fieldGroup: [
          {
            key: 'input',
            type: 'input',
          },
        ],
      },
    ];
    fixture.detectChanges();
    expect(componentInstance).toBeTruthy();
  });
});
