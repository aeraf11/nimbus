import { ChangeDetectionStrategy, Component, HostBinding, ViewEncapsulation } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
  selector: 'ni-panel-wrapper-type-wrapper',
  templateUrl: './panel-wrapper-type.component.html',
  styleUrls: ['./panel-wrapper-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class PanelWrapperTypeComponent extends FieldWrapper {}
