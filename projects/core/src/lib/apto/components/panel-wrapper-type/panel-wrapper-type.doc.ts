export const PanelWrapperTypeDoc = `
<h3>Template Options</h3>
<ul>
  <li>panelLabel: string</li>
  <li>disabled: boolean</li>
  <li>panelHidden: boolean</li>
</ul>
<h3>Default Options</h3>
<pre>{}</pre>
`;
