import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewEncapsulation } from '@angular/core';
import { BaseTypeComponent } from './../base-type/base-type.component';
import { ChartDataService } from '../../../core/services/chart-data.service';
import { ChartType } from '../../../core/models/chart-type';
import { Observable, map, tap } from 'rxjs';
import { ChartDataItem } from '../../../core/models/chart-data-item';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
  selector: 'ni-chart-line-type',
  templateUrl: './chart-line-type.component.html',
  styleUrls: ['./chart-line-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ChartLineTypeComponent extends BaseTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      flexPosition: 'center center',
      lineWidth: 0,
      lineColour: '',
      isAreaGradient: true,
      areaColour: '#c0c0c0',
      width: '100%',
      height: '100%',
      maxWidth: '100%',
      title: '',
      subTitle: '',
      enableAnimation: true,
      chartArea: {
        opacity: 0,
        border: {
          width: 0,
        },
        spacing: 0,
      },
      xAxis: {
        title: '',
        lineWidth: 0,
        gridWidth: 0,
        tickWidth: 0,
        labelFormat: '',
        labelPadding: 0,
        labelPlacement: 'onTicks',
        labelStyle: {},
        valueType: 'Category',
        visible: false,
      },
      yAxis: {
        title: '',
        lineWidth: 0,
        gridWidth: 0,
        tickWidth: 0,
        labelFormat: '',
        visible: true,
      },
      marker: {
        dataLabel: {
          visible: true,
          position: 'Top',
          name: '${point.value}',
          font: {
            color: 'white',
            fontWeight: 'Bold',
            size: '14px',
          },
        },
      },
      tooltip: {
        enable: false,
      },
    },
  };

  data$?: Observable<ChartDataItem[] | undefined>;
  hasData$?: Observable<boolean>;

  constructor(changes: ChangeDetectorRef, private service: ChartDataService) {
    super(changes);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.handleData();

    if (this.formControl) {
      this.formControl.valueChanges
        .pipe(
          untilDestroyed(this),
          tap(() => this.handleData())
        )
        .subscribe();
    }
  }

  handleData(): void {
    const params = this.service.getParams(this.props['params'], this.formState);
    this.data$ = this.service
      .getChartData(ChartType.Line, this.formControl.value, this.props['dataSource'], params)
      .pipe(map((data) => data?.data));
    this.hasData$ = this.data$.pipe(map((data) => (data && data.length > 0 ? true : false)));
  }
}
