import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockChartDataService } from '../../../core/services/chart-data.service.spec';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { ChartDataService } from './../../../core/services/chart-data.service';
import { ChartLineTypeComponent } from './chart-line-type.component';

describe('ChartLineTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'chartline',
              component: ChartLineTypeComponent,
            },
          ],
        },
        providers: [{ provide: ChartDataService, useClass: MockChartDataService }],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'chartline',
        type: 'chartline',
      },
    ];
  });

  it('should create', () => {
    // Assert
    expect(component).toBeTruthy();
  });
});
