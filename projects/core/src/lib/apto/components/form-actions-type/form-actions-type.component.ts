import { FormActionsItem, FormsHelperService, utils } from '@nimbus/core/src/lib/core';
import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-form-actions-type',
  templateUrl: './form-actions-type.component.html',
  styleUrls: ['./form-actions-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormActionsTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      gap: '16px',
    },
  };

  /**
   *
   */
  constructor(private formsHelper: FormsHelperService, public override change: ChangeDetectorRef) {
    super(change);
  }

  itemClicked(clickedItem: FormActionsItem) {
    const fieldGroupItem = this.field.fieldGroup?.find((element) => element.props?.['label'] === clickedItem.label);
    if (fieldGroupItem === undefined) return;
    let model = null;
    if (fieldGroupItem.props?.['modelExpression']) {
      const expression = utils.evalStringFormExpression(fieldGroupItem.props?.['modelExpression']);
      if (expression) {
        model = expression(this.formState.rootModel, this.field.model, this.formState.context, this.formsHelper);
      }
    }
    this.formState.executeAction(fieldGroupItem.props?.['action'], model);
  }

  get items(): FormActionsItem[] {
    return (
      this.field.fieldGroup?.map(
        (fg) =>
          ({
            color: fg.props?.['color'],
            disabled: fg.props?.['disabled'],
            label: fg.props?.['label'],
            ariaLabel: fg.props?.['ariaLabel'],
          } as FormActionsItem)
      ) ?? []
    );
  }
}
