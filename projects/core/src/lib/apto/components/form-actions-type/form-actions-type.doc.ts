export const FormActionsDoc = `
<h3>Props:</h3>
<ul>
  <li>gap: string ("{number}px")</li>
  <li>ariaLabel: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    gap: '16px',
  },
}</pre>
`;
