import { StateService } from '@nimbus/core/src/lib/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { FormActionsTypeComponent } from './form-actions-type.component';
import { MockStateService } from '@nimbus/core/src/lib/core';

describe('FormActionsTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'formactions',
              component: FormActionsTypeComponent,
            },
          ],
        },
        providers: [
          {
            provide: StateService,
            useClass: MockStateService,
          },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'formactions',
        type: 'formactions',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
