import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-type-ahead-type',
  templateUrl: './type-ahead-type.component.html',
  styleUrls: ['./type-ahead-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TypeAheadTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      searchEmpty: true,
      version: 1,
    },
  };

  constructor(public override change: ChangeDetectorRef) {
    super(change);
  }
}
