import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MetaDataService, StateService } from '@nimbus/core/src/lib/core';
import { DataSourceService } from '@nimbus/core/src/lib/core';
import { MockDataSourceService } from '../../../core/services/data-source.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { MockMetaDataService } from './../../../core/services/meta-data.service.spec';
import { ListService } from '../../../core/services/list.service';
import { TypeAheadTypeComponent } from './type-ahead-type.component';
import { MockListService } from '../../../core/services/list.service.spec';
import { FormlyTestComponent } from '../../../core/formly/test/formly-test.component';

describe('TypeAheadTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'typeahead',
              component: TypeAheadTypeComponent,
            },
          ],
        },
        imports: [MatAutocompleteModule, MatFormFieldModule],
        providers: [
          { provide: StateService, useClass: MockStateService },
          { provide: ListService, useClass: MockListService },
          { provide: DataSourceService, useClass: MockDataSourceService },
          { provide: MetaDataService, useClass: MockMetaDataService },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'typeahead',
        type: 'typeahead',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
