export const TypeAheadDoc = `
<h3>Template Options</h3>
<h4>Version 1 Properties<h4>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>description: string</li>
  <li>listName: string</li>
  <li>valueProp: string</li>
  <li>labelProp: string</li>
  <li>filterColumns:
    <ul>
      <li>column: string</li>
      <li>stringFilter: string</li>
      <li>constantFilter: boolean</li>
    </ul>
  </li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>autoFillProp: string (property to use as Id if typeahead used to autofill model (9774))</li>
  <li>autoSyncRouteId: string (DynmaicRoute name to be used to generate data to be autofilled)</li>
  <li>autoFillModelSkipProps: array<string> (list of field groups to be excluded in the autofill (9774)). As of 3.13.0 will walk tree e.g. rootProperty.myValue.mySubValue <li/>
  <li>autoFillModelOverrides: (Json Object see below) overide speccific model properties e.g. to clear a linked entity Id.(9774)</li>
  <li>enableModelSync: boolean (if the typeahead returns an object you can get those properties pushed up to the model property. Names will need to match keys on the model to be handled by the form.)</li>
  <li>modelSyncExclusions: string[] (names of properties from result not to be sync'd across to the model)</li>
  <li>modelSyncMetaDataMap: { propName?: string; items: { key: string; label: string }[] } (if you have a meta data json string or actual json on a result property, you can map meta data items across to the model. Specify the meta data item label and the model key. If the property name is not 'metaData' then specify with the propName property.)</li>
  <li>modelSyncJsonProperties: { name: string, items: { key: string, identifier: { property: string, value: stinrg }, addToRootModel?: boolean }[], returnMode: 'single' | 'multiple', singleReturn?: 'object' | 'property', childName?: string, isMetaData?: boolean; convertToCamelCase?: boolean; innerMetaDataPropName?: string; } }</li>
  <li>offlineConfig: { table: string, type: string } (retrieves data from indexedDB based on the given table and type)</li>
  <li>ariaLabel: string</li>
  <li>defaultLabel: string</li>
  <li>defaultInitiallySelected: boolean</li>
</ul>
<h4>Version 2 Additional Properties<h4>
<ul>
  <li></li>
</ul>
<h4>Version 2 Redundant Properties<h4>
<ul>
  <li>valueProp: string - N.B. In V1 this is used for lists to transform the items to an id/displayName structure, not performed in V2 as whole object is used as value.</li>
</ul>

<h3>Default Options</h3>
<pre>
{
  props: {
    searchEmpty: true,
    version: 1,
  },
}
</pre>
<pre>
autoFillModelOverrides;
{
    "path": [ "people", "addPerson" ],
    "property": [
      {
        "personLinkId": null
      }
    ]
  }
}
</pre>
`;
