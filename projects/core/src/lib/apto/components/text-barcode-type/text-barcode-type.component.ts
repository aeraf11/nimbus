import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-text-barcode-type',
  templateUrl: './text-barcode-type.component.html',
  styleUrls: ['./text-barcode-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextBarcodeTypeComponent extends BaseTypeComponent {
  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
