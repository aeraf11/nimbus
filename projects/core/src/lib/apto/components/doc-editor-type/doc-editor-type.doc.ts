export const DocEditorDoc = `
<h3>Template Options</h3>
<ul>
  <li>height: string</li>
  <li>width: string</li>
  <li>readOnly: boolean</li>
  <li>enableToolbar: boolean</li>
  <li>enableSave: boolean</li>
  <li>enableDownload: boolean</li>
  <li>enablePrint: boolean</li>
  <li>toolbarItems: string[]</li>
  <li>showSpinner: boolean</li>
  <li>ariaLabel: string</li>
  <li>dialogWidth: string</li>
  <li>entitySource: number (0 = DocumentVersion, 1 = AttachmentVersion)</li>
  <li>defaultReason: string</li>
  <li>canModifyDefaultReason: boolean</li>
  <li>defaultCheckInValue: boolean</li>
  <li>canModifyDefaultCheckIn: boolean</li>
  <li>eventService: DocEditorEventService</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    readOnly: false,
    enableToolbar: true,
    enableSave: true,
    enableDownload: true,
    enablePrint: true,
    toolbarItems: undefined,
    showSpinner: true,
    dialogWidth: undefined,
    defaultReason: '',
    canModifyDefaultReason: true,
    defaultCheckInValue: true,
    canModifyDefaultCheckIn: true,
    entitySource: EntitySource.DocumentVersion,
  }
}</pre>
<h3>toolbarItems</h3>
<ul>
  <li>Save</li>
  <li>Cancel</li>
  <li>|</li>
  <li>Download</li>
  <li>Print</li>
  <li>|</li>
  <li>Undo</li>
  <li>Redo</li>
  <li>|</li>
  <li>Image</li>
  <li>Table</li>
  <li>|</li>
  <li>Header</li>
  <li>Footer</li>
  <li>PageSetup</li>
  <li>PageNumber</li>
  <li>Break</li>
  <li>InsertFootnote</li>
  <li>InsertEndnote</li>
  <li>|</li>
  <li>Find</li>
  <li>|</li>
  <li>LocalClipboard</li>
</ul>
`;
