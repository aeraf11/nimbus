import { ChangeDetectionStrategy, Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';
import { DocumentService } from '../../../core/services/document.service';
import { catchError, of } from 'rxjs';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { EntitySource } from '../../../core/models/entity-source';
import { DocumentUploadResult } from '../../../core/models/document-upload-result';
import { DialogFormAction, DialogFormComponent, DialogFormData, DialogFormResult, EditorMode } from '../../../core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { DocumentData } from '../../../core/models/document-data';
import { DocEditorEventService } from '../../../core/components/doc-editor/doc-editor-event.service';
import { validate as uuidValidate } from 'uuid';

@UntilDestroy()
@Component({
  selector: 'ni-doc-editor-type',
  templateUrl: './doc-editor-type.component.html',
  styleUrls: ['./doc-editor-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DocEditorTypeComponent extends BaseTypeComponent implements OnInit {
  fields: FormlyFieldConfig[] = [];

  private dialogWidth?: string = undefined;
  private defaultReason = '';
  private canModifyDefaultReason = true;
  private defaultCheckInValue = true;
  private canModifyDefaultCheckIn = true;
  private entitySource = EntitySource.DocumentVersion;
  private editorHasChanges = false;

  public eventService: DocEditorEventService;

  private get documentVersionId() {
    return this.formControl?.value;
  }

  private defaultDocumentData: DocumentData = {
    source: this.entitySource,
    id: '',
    fileName: '',
    fileType: '',
    fileData: '',
  };

  private get documentData(): DocumentData {
    return this.model['_documentData'] != undefined
      ? (this.model['_documentData'] as DocumentData)
      : this.defaultDocumentData;
  }

  private set documentData(value: DocumentData) {
    this.model['_documentData'] = value;
  }

  private set documentDataFileData(value: string) {
    const newValue = { ...this.documentData, fileData: value };
    this.documentData = newValue;
  }

  private get documentDataFileData() {
    return this.documentData?.fileData;
  }

  private get documentDataId() {
    return this.documentData?.id;
  }

  public fileName = '';
  public fileData = '';

  override defaultOptions = {
    props: {
      readOnly: false,
      enableToolbar: true,
      enableSave: true,
      enableDownload: true,
      enablePrint: true,
      toolbarItems: undefined,
      showSpinner: true,
      dialogWidth: undefined,
      defaultReason: '',
      canModifyDefaultReason: true,
      defaultCheckInValue: true,
      canModifyDefaultCheckIn: true,
      entitySource: EntitySource.DocumentVersion,
    },
  };

  constructor(change: ChangeDetectorRef, private documentService: DocumentService, public dialog: MatDialog) {
    super(change);
    this.eventService = new DocEditorEventService();
  }

  override ngOnInit() {
    this.entitySource = parseInt(this.props['entitySource']);
    this.dialogWidth = this.props['dialogWidth'];
    this.defaultReason = this.props['defaultReason'];
    this.canModifyDefaultReason = this.props['canModifyDefaultReason'];
    this.defaultCheckInValue = this.props['defaultCheckInValue'];
    this.canModifyDefaultCheckIn = this.props['canModifyDefaultCheckIn'];

    this.formControl?.valueChanges.pipe(untilDestroyed(this)).subscribe(() => {
      this.valueChangedSubscription();
    });

    this.getDocument();
  }

  onSave(saveFileData: string) {
    if (this.documentIdValid(this.documentVersionId) && this.fileDataValid(saveFileData)) {
      if (this.showDialog()) {
        this.openAdditionalInfoDialog(saveFileData);
      } else {
        this.saveDocument(saveFileData, this.defaultReason, this.defaultCheckInValue);
      }
    }
  }

  public onContentSerialised(serialisedFileData: string) {
    this.documentDataFileData = serialisedFileData;
  }

  public onContentHasChanges(hasChanged: boolean) {
    this.editorHasChanges = hasChanged;
  }

  private valueChangedSubscription() {
    this.getDocument();
  }

  private getDocument() {
    if (this.entitySource && this.documentIdValid(this.documentVersionId)) {
      this.documentService
        .getDocument(this.entitySource, this.documentVersionId)
        .pipe(
          untilDestroyed(this),
          catchError(() => {
            this.getErrored();
            return of();
          })
        )
        .subscribe((docData) => {
          this.getSuccessfull(docData);
        });
    }
  }

  private saveDocument(saveFileData: string, reasonText: string, checkIn: boolean) {
    if (this.documentIdValid(this.documentVersionId)) {
      this.saveStarted();
      this.documentService
        .saveDocument(this.entitySource, this.documentVersionId, saveFileData, reasonText, checkIn)
        .pipe(
          untilDestroyed(this),
          catchError(() => {
            this.saveErrored();
            return of();
          })
        )
        .subscribe((r) => this.saveSuccessful(r, saveFileData));
    }
  }

  private getErrored() {
    this.fileData = '';
    this.fileName = '';
    this.documentData = this.defaultDocumentData;
    this.change.markForCheck();
  }

  private getSuccessfull(docData: DocumentData) {
    this.fileData = docData.fileData;
    this.fileName = docData.fileName;
    this.documentData = docData;
    this.change.markForCheck();
    this.change.detectChanges();
  }

  private saveStarted() {
    this.eventService.SaveStarted();
    this.change.markForCheck();
  }

  private saveErrored() {
    this.eventService.SaveErrored();
    this.change.markForCheck();
  }

  private saveCancelled() {
    this.eventService.SaveCancelled();
    this.change.markForCheck();
  }

  private saveSuccessful(saveResult: DocumentUploadResult, savedFileData: string) {
    this.eventService.SaveSuccessful();
    this.fileData = savedFileData;
    this.setDocumentVersionId(saveResult.id, false);
    this.updateDocumentData(saveResult, savedFileData);
    this.change.markForCheck();
  }

  public updateDocumentData(saveResult: DocumentUploadResult, savedFileData: string) {
    const newValue = {
      ...this.documentData,
      id: saveResult.id,
      fileName: saveResult.fileName,
      fileData: savedFileData,
    };
    this.documentData = newValue;
  }

  private setDocumentVersionId(value: string, emit = true) {
    this.formControl?.setValue(value, { emitEvent: emit });
  }

  private documentIdValid(id: string): boolean {
    return uuidValidate(id);
  }

  private fileDataValid(fileData: string): boolean {
    return fileData !== undefined && fileData !== '';
  }

  private showDialog(): boolean {
    return (
      (this.entitySource === EntitySource.AttachmentVersion &&
        (this.defaultReason === undefined || this.defaultReason === '' || this.canModifyDefaultReason)) ||
      (this.entitySource === EntitySource.DocumentVersion && this.canModifyDefaultCheckIn)
    );
  }

  getFormDefinition(): FormlyFieldConfig[] | undefined | null {
    if (this.entitySource === EntitySource.DocumentVersion) {
      return [
        {
          key: 'checkIn',
          type: 'checkbox',
          templateOptions: {
            label: 'Check in',
          },
        },
      ];
    } else {
      return [
        {
          key: 'attachmentReason',
          type: 'textarea',
          templateOptions: {
            label: 'Reason',
            required: true,
            rows: 3,
          },
        },
      ];
    }
  }

  openAdditionalInfoDialog(fileData: string): void {
    this.change.markForCheck();

    const data: DialogFormData = {
      model: { attachmentReason: this.defaultReason, checkIn: this.defaultCheckInValue },
      fieldConfig: this.getFormDefinition(),
      config: {
        cancelText: 'Cancel',
        addText: 'Save',
        enableDelete: false,
        enableEdit: false,
        isChildForm: true,
        title: this.entitySource === EntitySource.DocumentVersion ? 'Check document in?' : 'Reason for change',
      },
      editorMode: EditorMode.Add,
      disabled: false,
    };
    const dialogConfig: MatDialogConfig<DialogFormData> = { data };
    if (this.dialogWidth) {
      dialogConfig.width = this.dialogWidth;
      dialogConfig.minWidth = '';
      dialogConfig.maxWidth = '';
    }

    const dialogRef = this.dialog.open(DialogFormComponent, dialogConfig);
    dialogRef.afterClosed().subscribe((result: DialogFormResult) => {
      switch (result.action) {
        case DialogFormAction.Add:
          this.saveDocument(fileData, result.model.attachmentReason, result.model.checkIn);
          break;
        case DialogFormAction.Cancel:
          this.saveCancelled();
          break;
      }
    });
  }
}
