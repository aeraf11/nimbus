import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocEditorTypeComponent } from './doc-editor-type.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormlyTestComponent, SiteConfig, buildFormlyTestModule } from '../../../core';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';

describe('DocEditorTypeComponent', () => {
  let component: FormlyTestComponent;
  let fixture: ComponentFixture<FormlyTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'docversioneditor',
              component: DocEditorTypeComponent,
            },
          ],
        },
        imports: [HttpClientTestingModule, MatDialogModule],
        providers: [MatDialog, { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') }],
      })
    ).compileComponents();

    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
