export const RichTextViewerDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>readOnly: boolean</li>
  <li>tools: string[]</li>
  <li>height: string</li>
  <li>width: string</li>
</ul>
<h3>Default Options</h3>
<pre>{}</pre>
`;
