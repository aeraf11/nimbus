import { ChangeDetectorRef, Component } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-rich-text-viewer-type',
  templateUrl: './rich-text-viewer-type.component.html',
  styleUrls: ['./rich-text-viewer-type.component.scss'],
})
export class RichTextViewerTypeComponent extends BaseTypeComponent {
  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
