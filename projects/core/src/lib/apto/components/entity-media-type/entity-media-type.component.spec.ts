import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { ListFilterService, StateService } from '@nimbus/core/src/lib/core';
import { ListService } from '../../../core/services/list.service';
import { MockListService } from '../../../core/services/list.service.spec';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { EntityMediaTypeComponent } from './entity-media-type.component';
import { DataCardsComponent } from '../../../core/components/data-cards/data-cards.component';
import { MockListFilterService } from '../../../core/services/list-filter-service/list-filter.service.spec';

describe('EntityMediaTypeComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'entitymedia',
              component: EntityMediaTypeComponent,
            },
          ],
        },
        imports: [MatDialogModule],
        providers: [
          DataCardsComponent,
          { provide: StateService, useClass: MockStateService },
          { provide: ListService, useClass: MockListService },
          { provide: ListFilterService, useClass: MockListFilterService },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.model = {
      entitymedia: {},
    };

    component.fields = [
      {
        key: 'entitymedia',
        type: 'entitymedia',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    // Act
    fixture.detectChanges();

    // Assert
    expect(component).toBeTruthy();
  });
});
