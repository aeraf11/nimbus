import { BaseViewTypeComponent } from './../base-view-type/base-view-type.component';
import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'ni-entity-media-type',
  templateUrl: './entity-media-type.component.html',
  styleUrls: ['./entity-media-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntityMediaTypeComponent extends BaseViewTypeComponent {
  
}
