import { Component, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-relates-to-type',
  templateUrl: './relates-to-type.component.html',
  styleUrls: ['./relates-to-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RelatesToTypeComponent extends BaseTypeComponent {
  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
