import { BaseTypeComponent } from '../base-type/base-type.component';
import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'ni-date-time-view',
  templateUrl: './date-time-view.component.html',
  styleUrls: ['./date-time-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DateTimeViewComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      includeTimeZone: true,
      includeSeconds: false,
    },
  };
}
