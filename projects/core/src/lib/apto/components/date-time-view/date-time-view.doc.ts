export const DateTimeViewDoc = `
<h3>Template Options</h3>
<ul>
<li>includeTimeZone: boolean</li>
<li>includeSeconds: boolean</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    includeTimeZone: true,
    includeSeconds: false
  },
}</pre>
`;
