import { StateService, buildFormlyTestModule } from '@nimbus/core/src/lib/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormlyTestComponent } from '@nimbus/core/src/lib/core';
import { DateTimeViewComponent } from './date-time-view.component';
import { MockStateService } from '@nimbus/core/src/lib/core';

describe('DateTimeViewComponent', () => {
  let fixture: ComponentFixture<FormlyTestComponent>;
  let component: FormlyTestComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule(
      buildFormlyTestModule({
        config: {
          types: [
            {
              name: 'datetimeview',
              component: DateTimeViewComponent,
            },
          ],
        },
        providers: [
          {
            provide: StateService,
            useClass: MockStateService,
          },
        ],
      })
    ).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormlyTestComponent);
    component = fixture.componentInstance;

    component.fields = [
      {
        key: 'datetimeview',
        type: 'datetimeview',
      },
    ];

    fixture.detectChanges();
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
