import {
  Component,
  ChangeDetectionStrategy,
  AfterViewInit,
  OnDestroy,
  ViewChild,
  ViewEncapsulation,
  OnInit,
} from '@angular/core';
import { MatRadioButton, MatRadioGroup } from '@angular/material/radio';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { utils } from '@nimbus/core/src/lib/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@UntilDestroy()
@Component({
  selector: 'ni-radio-type',
  templateUrl: './radio-type.component.html',
  styleUrls: ['./radio-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class RadioTypeComponent extends BaseTypeComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild(MatRadioGroup, { static: true }) radioGroup!: MatRadioGroup;
  private focusObserver!: ReturnType<typeof utils.observe>;

  override defaultOptions = {
    props: {
      hideFieldUnderline: true,
      floatLabel: 'always' as const,
      options: [],
      tabindex: -1,
      isDeselectable: false,
    },
  };

  override ngOnInit(): void {
    super.ngOnInit();
    if (this.props['isDeselectable']) {
      this.formControl.valueChanges.pipe(untilDestroyed(this)).subscribe(() => {
        if (this.props['required']) {
          this.formControl.setErrors({ required: true });
        } else {
          this.formControl.setErrors(null);
        }
        this.change.markForCheck();
      });
    }
  }

  ngAfterViewInit(): void {
    this.focusObserver = utils.observe(this.field, ['focus'], ({ currentValue }) => {
      if (this.props['tabindex'] === -1 && currentValue && this.radioGroup._radios.length > 0) {
        // https://github.com/ngx-formly/ngx-formly/issues/2498
        setTimeout(() => {
          const radio = this.radioGroup.selected ? this.radioGroup.selected : this.radioGroup._radios.first;
          radio.focus();
        });
      }
    });
  }

  // TODO: find a solution to prevent scroll on focus
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  override onContainerClick() {}

  override ngOnDestroy(): void {
    super.ngOnDestroy();
    this.focusObserver && this.focusObserver.unsubscribe();
    super.ngOnDestroy();
  }

  checkState(event: Event, element: MatRadioButton): void {
    if (this.props['isDeselectable']) {
      event.preventDefault();
      if (this.formControl.value && this.formControl.value === element.value) {
        element.checked = false;
        this.formControl.reset();
      } else {
        this.formControl.setValue(element.value);
        element.checked = true;
      }
    }
  }
}
