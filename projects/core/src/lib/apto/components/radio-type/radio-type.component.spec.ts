import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatRadioModule } from '@angular/material/radio';
import { FormlyModule } from '@ngx-formly/core';
import { FormlySelectModule } from '@ngx-formly/core/select';
import { RadioTypeComponent } from './radio-type.component';

//TODO: Change when radio is split from radio-type.
describe('RadioTypeComponent', () => {
  let fixture: ComponentFixture<RadioTypeComponent>;
  let componentInstance: RadioTypeComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        MatRadioModule,
        FormlySelectModule,
        FormlyModule.forRoot({
          types: [{ name: 'radio', component: RadioTypeComponent }],
        }),
      ],
      declarations: [RadioTypeComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioTypeComponent);
    componentInstance = fixture.componentInstance;
    (componentInstance.field = {
      key: 'radio',
      type: 'radio',
      props: {
        options: [
          { label: 'Option1', value: 1 },
          { label: 'Option2', value: 2 },
        ],
      },
      formControl: new FormControl(''),
    }),
      fixture.detectChanges();
  });

  it('should create', () => {
    expect(componentInstance).toBeTruthy();
  });

  it('should display radio options', () => {
    const radioButtons = fixture.nativeElement.querySelectorAll('mat-radio-button');
    expect(radioButtons.length).toEqual(2);
    expect(radioButtons[0].textContent.trim()).toEqual('Option1');
    expect(radioButtons[1].textContent.trim()).toEqual('Option2');
  });

  it('should trigger click event', () => {
    // Arrange
    const radioButton = fixture.nativeElement.querySelector('mat-radio-button');
    spyOn(componentInstance, 'checkState');

    // Act
    radioButton.click();

    // Assert
    expect(componentInstance.checkState).toHaveBeenCalled();
  });

  //TODO: This needs fixing I can't get it to work correctly.
  xit('should set value on form control when radio button is clicked', () => {
    // Arrange
    const radioButtons = fixture.nativeElement.querySelectorAll('mat-radio-button');
    const formControl = componentInstance.field.formControl;
    expect(formControl.value).toEqual('');

    // Act
    radioButtons[0].click();

    // Assert
    expect(formControl.value).toEqual(1);
  });
});
