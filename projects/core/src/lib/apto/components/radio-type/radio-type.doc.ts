export const RadioDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>defaultValue: string</li>
  <li>description: string</li>
  <li>valueProp: string</li>
  <li>labelProp: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>options: object ({label: string, value: string})</li>
  <li>hideFieldUnderline: boolean</li>
  <li>floatLabel: string ("always" | "never" | "auto")</li>
  <li>tabIndex: number</li>
  <li>isDeselectable: boolean (whether a radio button can be unticked)</li>
  <li>isColumn: boolean</li>
  <li>width: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    hideFieldUnderline: true,
    floatLabel: 'always' as const,
    options: [],
    tabindex: -1,
    isDeselectable: false,
  },
}</pre>
`;

export const YesNoDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>description: string</li>
  <li>valueProp: string</li>
  <li>labelProp: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>options: object ({label: string, value: string})</li>
  <li>hideFieldUnderline: boolean</li>
  <li>floatLabel: string ("always" | "never" | "auto")</li>
  <li>tabIndex: number</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    hideFieldUnderline: true,
    floatLabel: 'always' as const,
    options: [
      { label: 'Yes', value: 'yes' },
      { label: 'No', value: 'no' },
    ],
    tabindex: -1,
  },
}</pre>
`;
