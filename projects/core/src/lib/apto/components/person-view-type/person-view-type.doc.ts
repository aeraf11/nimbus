export const PersonDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>imageSize: ProfileSize ("xsmall" | "small" | "medium" | "large" | "xlarge")</li>
  <li>noData: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>photoProp: string</li>
  <li>initialsProp: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    noData: 'No data found',
    photoProp: 'photoId',
    initialsProp: 'initials',
  },
}</pre>
`;
