import { OnInit, ChangeDetectorRef } from '@angular/core';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseViewTypeComponent } from '../base-view-type/base-view-type.component';

@Component({
  selector: 'ni-person-view-type',
  templateUrl: './person-view-type.component.html',
  styleUrls: ['./person-view-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PersonViewTypeComponent extends BaseViewTypeComponent implements OnInit {
  override defaultOptions = {
    props: {
      noData: 'No data found',
      photoProp: 'photoId',
      initialsProp: 'initials',
    },
  };

  constructor(change: ChangeDetectorRef) {
    super(change);
  }

  get noImageSize() {
    return this.props['imageSize'] === 'xlarge' ? '124px' : '75px';
  }

  override ngOnInit() {
    super.ngOnInit();
  }
}
