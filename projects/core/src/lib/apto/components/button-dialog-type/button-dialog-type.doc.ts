export const ButtonDialogDoc = `
<h3>Template Options</h3>
<ul>
  <li>formId: string</li>
  <li>formRouteId: string</li>
  <li>formActionKey: string</li>
  <li>label: string</li>
  <li>ariaLabel: string</li>
  <li>disabled: boolean</li>
  <li>dialogTitle: string</li>
  <li>dialogWidth: string</li>
  </ul>
<h3>Default Options</h3>
<pre>{
  props: {
    formId: 'Feedback.Add',
    formRouteId: 'Feedback.Add',
    formActionKey: 'save',
    label: 'Feedback',
    ariaLabel: 'Feedback',
    disabled: false,
    dialogTitle: 'Add Feedback',
    dialogWidth: '80%',
  },
}</pre>
`;
