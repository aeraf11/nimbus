import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';
import { StateService, FormDefinition } from '@nimbus/core/src/lib/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
@Component({
  selector: 'ni-button-dialog-type',
  templateUrl: './button-dialog-type.component.html',
  styleUrls: ['./button-dialog-type.component.scss'],
})
@UntilDestroy()
export class ButtonDialogTypeComponent extends BaseTypeComponent implements OnInit {
  constructor(private state: StateService, changes: ChangeDetectorRef) {
    super(changes);
  }

  override defaultOptions = {
    props: {
      formRouteId: 'Feedback.Add',
      formActionKey: 'save',
      label: 'Feedback',
      ariaLabel: 'Feedback',
      disabled: false,
      dialogTitle: 'Add Feedback',
      dialogWidth: '80%',
    },
  };

  form$?: Observable<FormDefinition | undefined>;

  override ngOnInit() {
    super.ngOnInit();
    if (this.props['formId']) {
      this.form$ = this.state.selectForm(this.props['formId']);
    } else {
      console.error(`ButtonDialog requires formId`);
    }
  }
}
