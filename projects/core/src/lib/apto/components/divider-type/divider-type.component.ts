import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BaseTypeComponent } from './../base-type/base-type.component';

@Component({
  selector: 'ni-divider-type',
  templateUrl: './divider-type.component.html',
  styleUrls: ['./divider-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DividerTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      width: '1px',
      style: 'solid',
      marginTop: '',
      marginRight: '',
      marginBottom: '',
      marginLeft: '',
    },
  };
}
