export const DividerDoc = `
<h3>Props</h3>
<ul>
  <li>width: string (in px/em/rem)</li>
  <li>style: string (must match css border-style property e.g. solid/dotted)</li>
  <li>marginTop: string (in px/em/rem)</li>
  <li>marginRight: string (in px/em/rem)</li>
  <li>marginBottom: string (in px/em/rem)</li>
  <li>marginLeft: string (in px/em/rem)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    width: '1px',
    style: 'solid',
    marginTop: '',
    marginRight: '',
    marginBottom: '',
    marginLeft: '',
  },
}</pre>
`;
