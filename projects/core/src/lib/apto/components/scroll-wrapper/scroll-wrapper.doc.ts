export const ScrollWrapperDoc = `
<h3>Template Options</h3>
<ul>
  <li>scrollFixedWidth: string (as a px or % relative to parent)</li>
  <li>scrollFixedHeight: string (as a px or % relative to parent)</li>
  <li>scrollMaxWidth: string (as a px or % relative to parent)</li>
  <li>scrollMaxHeight: string (as a px or % relative to parent)</li>
  <li>scrollBarBorderRadius: string (in px/em/rem)</li>
  <li>scrollBarWidth: string (in px/em/rem)</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    scrollFixedWidth: '100%',
    scrollFixedHeight: '100%',
    scrollBarBorderRadius: '10px',
    scrollBarWidth: '0.75rem',
  },
}</pre>
`;
