import { ChangeDetectionStrategy, Component, ElementRef, OnInit } from '@angular/core';
import { FieldWrapper } from '@ngx-formly/core';

@Component({
  selector: 'ni-scroll-wrapper',
  templateUrl: './scroll-wrapper.component.html',
  styleUrls: ['./scroll-wrapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ScrollWrapperComponent extends FieldWrapper implements OnInit {
  override defaultOptions = {
    props: {
      scrollFixedWidth: '100%',
      scrollFixedHeight: '100%',
      scrollBarBorderRadius: '10px',
      scrollBarWidth: '0.75rem',
    },
  };

  constructor(private element: ElementRef) {
    super();
  }

  ngOnInit(): void {
    this.setDefaultProps();

    this.element.nativeElement.style.setProperty(
      '--scroll-wrapper-border-radius',
      this.props['scrollBarBorderRadius'] ?? this.props['scrollBorderRadius']
    );
    this.element.nativeElement.style.setProperty(
      '--scroll-wrapper-width',
      this.props['scrollBarWidth'] ?? this.props['scrollWidth']
    );
  }

  // Required as FieldWrapper does not support defaultOptions like FieldType does
  private setDefaultProps(): void {
    const to: any = this.defaultOptions.props;
    Object.keys(to).forEach((key) => {
      if (!this.to[key]) {
        this.to[key] = to[key];
      }
    });
  }
}
