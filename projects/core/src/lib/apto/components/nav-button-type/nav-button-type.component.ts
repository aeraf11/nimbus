import { Component } from '@angular/core';
import { BaseTypeComponent } from '../base-type/base-type.component';

@Component({
  selector: 'ni-nav-button-type',
  templateUrl: './nav-button-type.component.html',
  styleUrls: ['./nav-button-type.component.scss'],
})
export class NavButtonTypeComponent extends BaseTypeComponent {
  override defaultOptions = {
    props: {
      label: '',
      appearance: 'outline',
      ariaLabel: 'Button navigation',
      text: 'Identify',
      route: '',
      param: '',
    },
  };
}
