export const NavButtonDoc = `
<h3>Props</h3>
<ul>
  <li>label: string</li>
  <li>required: boolean</li>
  <li>description: string</li>
  <li>placeholder: string</li>
  <li>text: string</li>
  <li>route: string</li>
  <li>param: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    label: 'Navigate',
    ariaLabel: 'Nav button input field',
    text: 'Navigate',
    route: 'ident/add?unidentifiedSubjectId=',
    param: 'formState?.context?.unidentifiedSubjectId',
    disabled: false
  },
}</pre>
`;
