import { BaseViewTypeComponent } from './../base-view-type/base-view-type.component';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ViewEncapsulation } from '@angular/core';
import { OnChange } from 'property-watch-decorator';
import { FormlyFieldConfig } from '@ngx-formly/core';
import deepClone from 'deep.clone';

@Component({
  selector: 'ni-date-list-view-type',
  templateUrl: './date-list-view-type.component.html',
  styleUrls: ['./date-list-view-type.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class DateListViewTypeComponent extends BaseViewTypeComponent {
  override defaultOptions = {
    props: {
      noData: 'No key dates found',
    },
  };

  @OnChange<FormlyFieldConfig>(function (this: DateListViewTypeComponent, value) {
    deepClone(value.fieldGroup).then((fieldConfig) => {
      this.fieldConfig = fieldConfig;
      this.change.detectChanges();
    });
  })
  override field: any;

  fieldConfig?: FormlyFieldConfig[];

  constructor(change: ChangeDetectorRef) {
    super(change);
  }
}
