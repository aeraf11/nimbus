export const DateListDoc = `
<h3>Template Options</h3>
<ul>
  <li>label: string</li>
  <li>placeholder: string</li>
  <li>focus: boolean</li>
  <li>disabled: boolean</li>
  <li>noData: string</li>
</ul>
<h3>Default Options</h3>
<pre>{
  props: {
    noData: 'No key dates found',
  },
}</pre>
`;
