// NOTE duplicated over in projects\core\src\lib\core\models\app-configuration.ts ngrx hack
export interface HomeItemConfig {
  name: string;
  icon: string;
  routeId: string;
  separator?: boolean;
  standalone?: boolean;
}
