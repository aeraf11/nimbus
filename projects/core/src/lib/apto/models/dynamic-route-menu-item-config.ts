import { MenuItemConfig } from './menu-item-config';

export interface DynamicRouteMenuItemConfig extends MenuItemConfig {
  parentMenuId?: string;
}
