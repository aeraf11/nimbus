export interface MenuItemConfig {
  icon: string;
  items?: MenuItemConfig[] | null;
  name: string;
  routeId: string | null;
  navigateFromRootOrigin?: boolean | undefined;
  queryParams: any;
}
