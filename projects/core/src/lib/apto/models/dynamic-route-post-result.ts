export interface DynamicRoutePostResult {
  context?: any;
  effects?: {
    message?: string;
    reloadModel?: boolean;
    reloadLists?: string[];
    clearModel?: boolean;
    redirectUrl?: string;
    retainModel?: boolean;
    removeOfflineEntity?: {
      id: string;
      table: string;
      refreshTableId?: string;
    };
  };
}
