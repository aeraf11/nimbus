export interface ButtonListTypeButton {
  text: string;
  value: string;
  isHidden?: boolean;
}
