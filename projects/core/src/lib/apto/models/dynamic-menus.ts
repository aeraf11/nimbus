import { HomeItemConfig } from './home-item-config';
import { MenuItemConfig } from './menu-item-config';
export interface DynamicMenus {
  home: HomeItemConfig[];
  main: MenuItemConfig[];
}
