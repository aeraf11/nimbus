import { APP_INITIALIZER, Injector, ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { EffectsModule } from '@ngrx/effects';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './core/containers/app/app.component';
import { CoreModule } from './core/core.module';
import { StoreModule } from '@ngrx/store';
import { metaReducers, ROOT_REDUCERS } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import {
  AppConfigurationEffects,
  FormEffects,
  LayoutEffects,
  LookupEffects,
  RouterEffects,
  StatusEffects,
  DynamicRouteEffects,
} from './core/effects';
import { HttpClientModule } from '@angular/common/http';
import { InitializerService } from './core/services/initializer.service';
import {
  ConfigurationService,
  StateService,
  ErrorHandlerService,
  AppInsightsService,
  TranslationService,
} from '@nimbus/core/src/lib/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';
import { AppInjector } from './core/services';
import { AuthConfigModule } from './auth/auth-config.module';

export function AppInitializerServiceFactory(service: InitializerService) {
  return () => service.initialize();
}

@NgModule({
  declarations: [],
  exports: [],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    CoreModule,
    StoreModule.forRoot(ROOT_REDUCERS, {
      metaReducers,
      runtimeChecks: {
        strictStateSerializability: true,
        strictActionSerializability: false,
        strictActionWithinNgZone: true,
        strictActionTypeUniqueness: true,
      },
    }),
    StoreRouterConnectingModule.forRoot(),
    !environment.production
      ? StoreDevtoolsModule.instrument({
          name: 'Phaneros',
        })
      : [],
    EffectsModule.forRoot([
      RouterEffects,
      AppConfigurationEffects,
      LookupEffects,
      StatusEffects,
      FormEffects,
      LayoutEffects,
      DynamicRouteEffects,
    ]),
    AuthConfigModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    }),
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: AppInitializerServiceFactory,
      deps: [InitializerService, ConfigurationService, StateService, AppInsightsService, TranslationService],
      multi: true,
    },
    {
      provide: ErrorHandler,
      useClass: ErrorHandlerService,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(injector: Injector) {
    AppInjector.setInjector(injector);
  }
}
