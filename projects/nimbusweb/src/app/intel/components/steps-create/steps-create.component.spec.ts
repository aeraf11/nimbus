import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';

import { StepsCreateComponent } from './steps-create.component';
import { forwardRef, Pipe, PipeTransform } from '@angular/core';
import { Store, StoreModule } from '@ngrx/store';
@Pipe({ name: 'translate' })
class TranslatePipe implements PipeTransform {
  transform(translate: string | undefined | null | '') {
    return translate;
  }
}

describe('StepsCreateComponent', () => {
  let component: StepsCreateComponent;
  let fixture: ComponentFixture<StepsCreateComponent>;
  let store: Store;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StepsCreateComponent, TranslatePipe],
      imports: [StoreModule.forRoot({}), ReactiveFormsModule, FormsModule],
      providers: [
        {
          provide: NG_VALUE_ACCESSOR,
          multi: true,
          useExisting: forwardRef(() => StepsCreateComponent),
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(StepsCreateComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
