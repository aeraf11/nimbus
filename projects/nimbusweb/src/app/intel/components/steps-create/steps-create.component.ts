import {
  INITIAL_VALUES_STEP1,
  INITIAL_VALUES_STEP2,
  PROGRESS_BAR,
  REPORTING_OFFICER_PROPS,
  STEP_DIVIDER,
  REPORTING_OFFICER_SELECT_COLUMNS,
  BUTTON_CONFIG,
} from '../../constants/intel-report';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  HostListener,
  Input,
  ViewChild,
  forwardRef,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatDatepickerInputEvent } from '@nimbus/material';

@Component({
  selector: 'nw-steps-create',
  templateUrl: './steps-create.component.html',
  styleUrls: ['./steps-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => StepsCreateComponent),
      multi: true,
    },
  ],
})
export class StepsCreateComponent implements ControlValueAccessor {
  constructor(private change: ChangeDetectorRef) {}

  stepDivider = STEP_DIVIDER;
  progressBar = PROGRESS_BAR;
  reportingOfficerProps = REPORTING_OFFICER_PROPS;
  sensitivityOptions = INITIAL_VALUES_STEP1.sensitivity.options;
  sourceEvaluationOptions = INITIAL_VALUES_STEP1.sourceEvaluation.options;
  selectColumns = REPORTING_OFFICER_SELECT_COLUMNS;
  assessmentOptions = INITIAL_VALUES_STEP2.assessment.options;
  handlingCodeOptions = INITIAL_VALUES_STEP2.handlingCode.options;
  actionCodeOptions = INITIAL_VALUES_STEP2.actionCode.options;
  senitisationCodeOptions = INITIAL_VALUES_STEP2.senitisationCode.options;
  entities = [
    {
      icon: 'fal fa-flag',
      subIcon: '',
      text: 'Flags',
      // formId: 'Investigator.Cases.Nominal.Add',
    },
    {
      icon: 'fal fa-upload',
      subIcon: '',
      text: 'Upload',
      // formId: 'Investigator.Cases.Scene.Add',
    },
  ];

  frmValues: object = {};

  @Input() formIntelReport!: FormGroup;
  @ViewChild('fileUpload')
  fileUpload?: ElementRef;

  @ViewChild('flagsDailog')
  flagsDailog?: ElementRef;

  get formArray(): any {
    return this.formIntelReport?.get('steps');
  }

  buttonConfig = BUTTON_CONFIG;

  @HostListener('window:beforeunload', ['$event']) beforeUnloadHander(event: any) {
    return !this.formIntelReport.dirty;
  }

  submit(): void {
    this.frmValues = this.formIntelReport?.value;
  }

  getFormValue(field: string, form: any) {
    const targetField = form.get(field);
    if (targetField) {
      return targetField.value || '';
    }
    return '';
  }

  setFieldValue(fieldGroup: any, field: string, val: any) {
    const targetField = fieldGroup.get(field);
    targetField?.setValue(val);
  }

  cancelStep(): void {
    window.history.go(-1);
  }

  addFlags(event: any): void {
    if (this.flagsDailog) this.flagsDailog.nativeElement?.click();
  }

  onClickUploadFiles(event: any) {
    if (this.fileUpload) this.fileUpload.nativeElement.click();
  }

  uploadFiles(event: any): void {
    const sd: any = this.formArray.get([1]).get('supportingDocuments');
    sd?.setValue(event.target.files);
  }

  onInput(event: any) {}

  onButtonPressed(buttonName: any) {
    switch (buttonName) {
      case 'cancel':
        this.cancelStep();
        break;
      case 'saveDraft':
        this.submit();
        break;
      case 'submitForm':
        this.submit();
        break;
      default:
        break;
    }
  }

  addEvent(form: FormGroup, field: string, event: MatDatepickerInputEvent<Date>) {
    this.setFieldValue(form, field, event.value);
  }

  openDialog(formGroup: FormGroup) {}

  private onChange!: (value: boolean) => void;

  valueChanged(value: boolean) {
    this.onChange(value);
  }

  writeValue(value: boolean) {
    this.change.markForCheck();
  }

  registerOnChange(fn: (value: boolean) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(_fn: () => void): void {
    return;
  }
}
