import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyIntelManagementComponent } from './my-intel-management.component';
import { NO_ERRORS_SCHEMA, Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'translate' })
class MockTranslatePipe implements PipeTransform {
  transform(text: string): string {
    return text;
  }
}

describe('MyIntelManagementComponent', () => {
  let component: MyIntelManagementComponent;
  let fixture: ComponentFixture<MyIntelManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MyIntelManagementComponent, MockTranslatePipe],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(MyIntelManagementComponent);
    component = fixture.componentInstance;
    component.authoriseCount = 0;
    component.sanitiseCount = 0;
    component.manageCount = 0;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('authoriseCount should be defined', () => {
    expect(component.authoriseCount).toBeDefined();
  });

  it('sanitiseCount should be defined', () => {
    expect(component.sanitiseCount).toBeDefined();
  });

  it('manageCount should be defined', () => {
    expect(component.manageCount).toBeDefined();
  });
});
