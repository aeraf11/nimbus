import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'nw-my-intel-management',
  templateUrl: './my-intel-management.component.html',
  styleUrls: ['./my-intel-management.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyIntelManagementComponent {
  @Input() authoriseCount!: number;
  @Input() sanitiseCount!: number;
  @Input() manageCount!: number;
}
