import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MyIntelComponent } from './my-intel.component';

describe('MyIntelComponent', () => {
  let component: MyIntelComponent;
  let fixture: ComponentFixture<MyIntelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MyIntelComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(MyIntelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
