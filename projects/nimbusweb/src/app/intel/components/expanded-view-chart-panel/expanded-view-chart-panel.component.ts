import { ChartDataItem } from '@nimbus/core/src/lib/core';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
@Component({
  selector: 'nw-expanded-view-chart-panel',
  templateUrl: './expanded-view-chart-panel.component.html',
  styleUrls: ['./expanded-view-chart-panel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExpandedViewChartPanelComponent {
  @Input() ageChartConfig: any;
  @Input() ageChartData: ChartDataItem[] | null | undefined;
  @Input() ageChartHasData: boolean | null | undefined;

  @Input() orgChartConfig: any;
  @Input() orgChartDataItems: ChartDataItem[] | null | undefined;
  @Input() orgChartHasData: boolean | null | undefined;
  @Input() orgChartTitle: string | null | undefined;

  @Input() userChartConfig: any;
  @Input() userChartData: ChartDataItem[] | null | undefined;
  @Input() userChartHasData: boolean | null | undefined;
}
