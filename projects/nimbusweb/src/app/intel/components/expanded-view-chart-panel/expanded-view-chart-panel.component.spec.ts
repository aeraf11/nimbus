import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandedViewChartPanelComponent } from './expanded-view-chart-panel.component';
import { AGE_CHART_CONFIG, ORG_CHART_CONFIG, PERFORMANCE_CHART_CONFIG } from '../../constants/expanded-view-charts';

describe('AuthoriseChartPanelComponent', () => {
  let component: ExpandedViewChartPanelComponent;
  let fixture: ComponentFixture<ExpandedViewChartPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ExpandedViewChartPanelComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(ExpandedViewChartPanelComponent);
    component = fixture.componentInstance;
    component.orgChartConfig = ORG_CHART_CONFIG;
    component.ageChartConfig = AGE_CHART_CONFIG;
    component.userChartConfig = PERFORMANCE_CHART_CONFIG;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
