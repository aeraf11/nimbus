import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  AuthoriseComponent,
  ExpandedViewComponent,
  HomeComponent,
  IntelReportComponent,
  SanitiseQueueComponent,
} from './containers';
import { IntelRoutingModule } from './intel-routing.module';
import { CoreModule } from '@nimbus/core/src/lib/core';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AptoModule } from '@nimbus/core/src/lib/apto';
import {
  ExpandedViewChartPanelComponent,
  MyIntelManagementComponent,
  MyIntelComponent,
  StepsCreateComponent,
} from './components';
import { MAT_DATE_LOCALE } from '@angular/material/core';

const COMPONENTS = [
  AuthoriseComponent,
  ExpandedViewComponent,
  ExpandedViewChartPanelComponent,
  HomeComponent,
  IntelReportComponent,
  MyIntelManagementComponent,
  MyIntelComponent,
  SanitiseQueueComponent,
  StepsCreateComponent,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [
    IntelRoutingModule,
    CommonModule,
    CoreModule,
    CdkStepperModule,
    FormsModule,
    ReactiveFormsModule,
    AptoModule,
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'en-GB' }],
})
export class IntelModule {}
