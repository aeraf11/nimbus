import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthoriseComponent } from './containers/authorise/authorise.component';
import { HomeComponent } from './containers/home/home.component';
import { IntelReportComponent } from './containers/intel-report/intel-report.component';
import { SanitiseQueueComponent } from './containers/sanitise-queue/sanitise-queue.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    data: { title: 'Intel', id: 'Intel.View', hideToolbars: false },
  },
  {
    path: 'add',
    component: IntelReportComponent,
    data: { title: 'Create Report', id: 'Intel.Create', hideToolbars: false },
  },
  {
    path: 'authorise',
    component: AuthoriseComponent,
    data: { title: 'Authorise Queue', id: 'Authorise.View', hideToolbars: false },
  },
  {
    path: 'sanitise',
    component: SanitiseQueueComponent,
    data: { title: 'Sanitise Queue', id: 'Sanitise.View', hideToolbars: false },
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IntelRoutingModule {}
