import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';
import { DataSourceService, StateService, User } from '@nimbus/core/src/lib/core';
import { Subject, of } from 'rxjs';
import { NO_ERRORS_SCHEMA, Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'userHasPermissions' })
class MockUserHasPermissionsPipe implements PipeTransform {
  transform(): boolean {
    return true;
  }
}

@Pipe({ name: 'translate' })
class MockTranslatePipe implements PipeTransform {
  transform(text: string): string {
    return text;
  }
}

describe('HomeComponent', () => {
  let userSubject: Subject<User | null>;
  let siteIdSubject: Subject<string | undefined>;

  let spyStateService: jasmine.SpyObj<StateService>;
  let spyDataSourceService: jasmine.SpyObj<DataSourceService>;

  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    userSubject = new Subject<User | null>();
    siteIdSubject = new Subject<string | undefined>();

    spyStateService = jasmine.createSpyObj<StateService>('StateService', {
      selectUser: userSubject.asObservable(),
      selectSiteId: siteIdSubject.asObservable(),
    });
    spyDataSourceService = jasmine.createSpyObj<DataSourceService>('DataSourceService', ['getData']);

    await TestBed.configureTestingModule({
      declarations: [HomeComponent, MockUserHasPermissionsPipe, MockTranslatePipe],
      providers: [
        { provide: StateService, useValue: spyStateService },
        { provide: DataSourceService, useValue: spyDataSourceService },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should initialise oirActionCounts$', () => {
      expect(component.oirActionCounts$).toBeDefined();
    });
  });

  describe('oirActionCounts$', () => {
    it('should call DataSourceService.getData when state emits user and applicationId', () => {
      spyDataSourceService.getData.and.returnValue(of([{}]));

      userSubject.next({ userProfileId: 'userProfileId' } as User);
      siteIdSubject.next('applicationId');

      expect(spyDataSourceService.getData).toHaveBeenCalledWith('OIRActionCounts', {
        authenticatedUserProfileId: 'userProfileId',
        applicationId: 'applicationId',
      });
    });

    it('should return OIR action counts from the data source', () => {
      const oirActionCounts = { authorise: 1, sanitise: 2, manage: 3 };
      spyDataSourceService.getData.and.returnValue(of([oirActionCounts]));

      userSubject.next({ userProfileId: 'userProfileId' } as User);
      siteIdSubject.next('applicationId');

      component.oirActionCounts$?.subscribe((counts) => expect(counts).toEqual(oirActionCounts));
    });
  });
});
