import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { DataSourceService, StateService } from '@nimbus/core/src/lib/core';
import { map, switchMap, forkJoin, take, Observable, filter, share } from 'rxjs';

interface OIRActionCounts {
  authorise: number;
  sanitise: number;
  manage: number;
}

@Component({
  selector: 'nw-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent implements OnInit {
  oirActionCounts$?: Observable<OIRActionCounts>;

  constructor(private dataSourceService: DataSourceService, private state: StateService) {}

  ngOnInit(): void {
    const userProfileId$ = this.state.selectUser().pipe(
      map((user) => user?.userProfileId),
      filter((userProfileId) => userProfileId !== undefined),
      take(1)
    );

    const applicationId$ = this.state.selectSiteId().pipe(
      filter((applicationId) => applicationId !== undefined),
      take(1)
    );

    this.oirActionCounts$ = forkJoin({
      authenticatedUserProfileId: userProfileId$,
      applicationId: applicationId$,
    }).pipe(
      switchMap((params) => this.dataSourceService.getData<OIRActionCounts>('OIRActionCounts', params)),
      map((counts) => counts[0]),
      share()
    );
  }
}
