import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandedViewComponent } from './expanded-view.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ChartData, ChartDataService, ChartType } from '@nimbus/core/src/lib/core';
import { Observable, of } from 'rxjs';
import { AGE_CHART_CONFIG, ORG_CHART_CONFIG, PERFORMANCE_CHART_CONFIG } from '../../constants/expanded-view-charts';

class MockChartDataService {
  getChartData(type: ChartType, data?: any, dataSource?: string, params?: object): Observable<ChartData> {
    return of();
  }

  getParams(keys: string[], formState: any): any {
    return {};
  }

  useThemeForLabels(props: any): boolean {
    return true;
  }
}

describe('ExpandedViewComponent', () => {
  let component: ExpandedViewComponent;
  let fixture: ComponentFixture<ExpandedViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ExpandedViewComponent],
      providers: [{ provide: ChartDataService, useClass: MockChartDataService }],
    }).compileComponents();

    fixture = TestBed.createComponent(ExpandedViewComponent);
    component = fixture.componentInstance;
    component.ageChartConfig = AGE_CHART_CONFIG;
    component.orgChartConfig = ORG_CHART_CONFIG;
    component.userChartConfig = PERFORMANCE_CHART_CONFIG;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
