import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ChartDataAccumulation, ChartDataItem, ChartDataService, ChartType } from '@nimbus/core/src/lib/core';
import { Observable, map, of } from 'rxjs';

// used to demonstrate the line chart until the user defined datasource is implemented
const MOCK_PERFORMANCE_DATA = [
  {
    name: '2023-07-14T00:00:00',
    value: 9,
  },
  {
    name: '2023-07-15T00:00:00',
    value: 7,
  },
  {
    name: '2023-07-16T00:00:00',
    value: 9,
  },
  {
    name: '2023-07-17T00:00:00',
    value: 7,
  },
  {
    name: '2023-07-18T00:00:00',
    value: 15,
  },
  {
    name: '2023-07-19T00:00:00',
    value: 17,
  },
  {
    name: '2023-07-20T00:00:00',
    value: 18,
  },
  {
    name: '2023-07-21T00:00:00',
    value: 12,
  },
  {
    name: '2023-07-22T00:00:00',
    value: 15,
  },
  {
    name: '2023-07-23T00:00:00',
    value: 11,
  },
  {
    name: '2023-07-24T00:00:00',
    value: 11,
  },
  {
    name: '2023-07-25T00:00:00',
    value: 15,
  },
  {
    name: '2023-07-26T00:00:00',
    value: 8,
  },
  {
    name: '2023-07-27T00:00:00',
    value: 7,
  },
  {
    name: '2023-07-28T00:00:00',
    value: 13,
  },
  {
    name: '2023-07-29T00:00:00',
    value: 15,
  },
  {
    name: '2023-07-30T00:00:00',
    value: 18,
  },
  {
    name: '2023-07-31T00:00:00',
    value: 13,
  },
  {
    name: '2023-08-01T00:00:00',
    value: 31,
  },
  {
    name: '2023-08-02T00:00:00',
    value: 45,
  },
  {
    name: '2023-08-03T00:00:00',
    value: 50,
  },
  {
    name: '2023-08-04T00:00:00',
    value: 52,
  },
  {
    name: '2023-08-05T00:00:00',
    value: 41,
  },
  {
    name: '2023-08-06T00:00:00',
    value: 48,
  },
  {
    name: '2023-08-07T00:00:00',
    value: 43,
  },
  {
    name: '2023-08-08T00:00:00',
    value: 15,
  },
  {
    name: '2023-08-09T00:00:00',
    value: 15,
  },
  {
    name: '2023-08-10T00:00:00',
    value: 93,
  },
];

@Component({
  selector: 'nw-expanded-view',
  templateUrl: './expanded-view.component.html',
  styleUrls: ['./expanded-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExpandedViewComponent implements OnInit {
  @Input() title: string | undefined;
  @Input() titleIcon = '';
  @Input() statusToRetrieve: string[] = [];
  @Input() ageChartConfig: any;
  @Input() orgChartConfig: any;
  @Input() userChartConfig: any;

  ageChartData$?: Observable<ChartDataItem[] | undefined>;
  ageChartHasData$?: Observable<boolean>;

  orgChartData$?: Observable<ChartDataAccumulation | undefined> = new Observable();
  orgChartDataItems$?: Observable<ChartDataItem[] | undefined>;
  orgChartHasData$?: Observable<boolean>;
  orgChartTitle$?: Observable<string>;

  userChartData$?: Observable<ChartDataItem[] | undefined>;
  userChartHasData$?: Observable<boolean>;

  constructor(private chartDataService: ChartDataService) {}

  ngOnInit(): void {
    [this.ageChartData$, this.ageChartHasData$] = this.loadChart(
      ChartType.Bar,
      this.ageChartConfig.dataSource,
      this.params
    );
    this.loadOrgChart();

    // TODO - This will become a user defined data source at some point.
    this.userChartData$ = of(MOCK_PERFORMANCE_DATA);
    this.userChartHasData$ = of(true);
  }

  loadChart(
    chartType: ChartType,
    dataSource: string,
    params: any
  ): [Observable<ChartDataItem[] | undefined>, Observable<boolean>, Observable<string>?] {
    const chartData = this.chartDataService
      .getChartData(chartType, undefined, dataSource, params)
      .pipe(map((data) => data?.data));
    const hasData = chartData.pipe(map((data) => (data && data.length > 0 ? true : false)));
    return [chartData, hasData];
  }

  loadOrgChart() {
    this.orgChartData$ = <Observable<ChartDataAccumulation>>(
      this.chartDataService.getChartData(ChartType.Pie, undefined, this.orgChartConfig.dataSource, this.params)
    );
    this.orgChartDataItems$ = this.orgChartData$.pipe(map((data) => data?.data));
    this.orgChartTitle$ = this.orgChartData$.pipe(map((data) => data?.total.toString() ?? ''));
    this.orgChartHasData$ = this.orgChartData$.pipe(map((data) => (data && data.data.length > 0 ? true : false)));
  }

  get params(): any {
    return {
      StatusToRetreive: this.statusToRetrieve.join(','),
    };
  }
}
