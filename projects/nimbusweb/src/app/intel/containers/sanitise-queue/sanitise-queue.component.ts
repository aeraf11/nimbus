import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AGE_CHART_CONFIG, ORG_CHART_CONFIG, PERFORMANCE_CHART_CONFIG } from '../../constants/expanded-view-charts';
import { OIRStatus } from '../../constants/oir-status';

@Component({
  selector: 'nw-sanitise-queue',
  templateUrl: './sanitise-queue.component.html',
  styleUrls: ['./sanitise-queue.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SanitiseQueueComponent {
  title = 'Sanitise';
  titleIcon = 'fa-solid fa-soap';
  statusToRetrieve = [OIRStatus.Accepted];

  ageChartConfig = AGE_CHART_CONFIG;
  orgChartConfig = ORG_CHART_CONFIG;
  //  TODO - This will be user defined data in the future
  userChartConfig = PERFORMANCE_CHART_CONFIG;
}
