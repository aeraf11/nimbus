import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SanitiseQueueComponent } from './sanitise-queue.component';

describe('SanitiseQueueComponent', () => {
  let component: SanitiseQueueComponent;
  let fixture: ComponentFixture<SanitiseQueueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SanitiseQueueComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SanitiseQueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
