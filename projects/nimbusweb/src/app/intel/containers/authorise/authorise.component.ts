import { Component } from '@angular/core';
import { AGE_CHART_CONFIG, ORG_CHART_CONFIG, PERFORMANCE_CHART_CONFIG } from '../../constants/expanded-view-charts';
import { OIRStatus } from '../../constants/oir-status';

@Component({
  selector: 'nw-authorise',
  templateUrl: './authorise.component.html',
  styleUrls: ['./authorise.component.scss'],
})
export class AuthoriseComponent {
  title = 'Authorise';
  titleIcon = 'fa-solid fa-file-signature';
  statusToRetrieve = [OIRStatus.New, OIRStatus.Acknowledged];

  ageChartConfig = AGE_CHART_CONFIG;
  orgChartConfig = ORG_CHART_CONFIG;
  //  TODO - This will be user defined data in the future
  userChartConfig = PERFORMANCE_CHART_CONFIG;
}
