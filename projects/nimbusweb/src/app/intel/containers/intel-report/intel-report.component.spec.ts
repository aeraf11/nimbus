import { ComponentFixture, TestBed } from '@angular/core/testing';
import { forwardRef, Pipe, PipeTransform } from '@angular/core';
import { Store, StoreModule } from '@ngrx/store';
import { IntelReportComponent } from './intel-report.component';
import { ReactiveFormsModule, FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
@Pipe({ name: 'translate' })
class TranslatePipe implements PipeTransform {
  transform(translate: string | undefined | null | '') {
    return translate;
  }
}
describe('IntelReportComponent', () => {
  let component: IntelReportComponent;
  let fixture: ComponentFixture<IntelReportComponent>;
  let store: Store;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [IntelReportComponent, TranslatePipe],
      imports: [StoreModule.forRoot({}), ReactiveFormsModule, FormsModule],
      providers: [
        {
          provide: NG_VALUE_ACCESSOR,
          multi: true,
          useExisting: forwardRef(() => IntelReportComponent),
        },
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(IntelReportComponent);
    component = fixture.componentInstance;
    store = TestBed.inject(Store);
    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });
  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
