import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromAuth from '../../../auth/reducers';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { User } from '../../models/intel-report';
import { INITIAL_VALUES_STEP1, INITIAL_VALUES_STEP2 } from '../../constants/intel-report';
@UntilDestroy()
@Component({
  selector: 'nw-intel-report',
  templateUrl: './intel-report.component.html',
  styleUrls: ['./intel-report.component.scss'],
  providers: [{ provide: FormBuilder }],
})
export class IntelReportComponent implements OnInit {
  defaultUser?: User;
  constructor(private fb: FormBuilder, private store: Store<fromAuth.State>, private cd: ChangeDetectorRef) {}
  formIntelReport!: FormGroup;
  get formArray(): any {
    return this.formIntelReport?.get('steps');
  }

  ngOnInit(): void {
    this.formIntelReport = this.fb.group({
      steps: this.fb.array([
        this.fb.group<any>({
          title: new FormControl<string>(INITIAL_VALUES_STEP1.title, { nonNullable: true }),
          reportingOfficer: new FormControl<any>(this.defaultUser, { nonNullable: false }),
          organization: new FormControl<string>(INITIAL_VALUES_STEP1.organization, { nonNullable: true }),
          dateTimeReported: new FormControl<Date>(INITIAL_VALUES_STEP1.dateTimeReported, { nonNullable: true }),
          sensitivity: new FormControl<string>(INITIAL_VALUES_STEP1.sensitivity.selectedValue, { nonNullable: true }),
          urn: new FormControl<string>(INITIAL_VALUES_STEP1.urn, { nonNullable: true }),
          isr: new FormControl<string>(INITIAL_VALUES_STEP1.isr, { nonNullable: true }),
          sourceEvaluation: new FormControl<string>(INITIAL_VALUES_STEP1.sourceEvaluation.selectedValue, {
            nonNullable: true,
          }),
          provenanceStatement: new FormControl<string>(INITIAL_VALUES_STEP1.provenanceStatement, {
            nonNullable: true,
          }),
          heading: `Create Intelligence Report`,
          label: `Details`,
          subLabel: `Add Some Details`,
          icon: 'fal fa-file-alt',
          buttonLabel: `Details`,
          buttonIcon: 'fal fa-file-alt',
          buttonColour: 'primary',
        }),
        this.fb.group({
          intelligenceItem: new FormControl<string>(INITIAL_VALUES_STEP2.intelligenceItem, { nonNullable: true }),
          eventDate: new FormControl<Date>(INITIAL_VALUES_STEP2.eventDate, { nonNullable: true }),
          flags: new FormControl<any[]>([], { nonNullable: true }),
          supportingDocuments: new FormControl<any>([], { nonNullable: true }),
          assessment: new FormControl<string>(INITIAL_VALUES_STEP2.assessment.selectedValue, { nonNullable: true }),
          handlingCode: new FormControl<string>(INITIAL_VALUES_STEP2.handlingCode.selectedValue, { nonNullable: true }),
          detailedHandlingInstructions: new FormControl<string>(INITIAL_VALUES_STEP2.detailedHandlingInstructions, {
            nonNullable: true,
          }),
          informationUsedFor: new FormControl<string>(INITIAL_VALUES_STEP2.informationUsedFor, {
            nonNullable: true,
          }),
          actionCode: new FormControl<string>(INITIAL_VALUES_STEP2.actionCode.selectedValue, { nonNullable: true }),
          senitisationCode: new FormControl<string>(INITIAL_VALUES_STEP2.senitisationCode.selectedValue, {
            nonNullable: true,
          }),
          heading: `Add a Report Item`,
          label: `Items`,
          subLabel: `Add Item Information`,
          icon: 'fal fa-list',
          buttonLabel: `Items`,
          buttonIcon: 'fal fa-plus',
          buttonColour: 'primary',
        }),
        this.fb.group({
          heading: `Review and submit`,
          label: `Submit`,
          subLabel: `Review and submit`,
          icon: 'fal fa-long-arrow-right',
          buttonLabel: `Review and submit`,
          buttonIcon: 'fal fa-long-arrow-right',
          buttonColour: 'primary',
        }),
      ]),
    });
    this.store
      .select(fromAuth.selectUser)
      .pipe(untilDestroyed(this))
      .subscribe((user) => {
        this.defaultUser = { fullName: user?.fullName, userProfileId: user?.userProfileId };
        this.formArray.get([0]).get('reportingOfficer').setValue(this.defaultUser);
        this.cd.markForCheck();
      });
  }
}
