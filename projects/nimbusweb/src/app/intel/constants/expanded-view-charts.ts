export const AGE_CHART_CONFIG = {
  dataSource: 'IntelOIR_ByAge',
  ariaLabel: 'Originating Intel Reports by Age',
  flexPosition: 'center center',
  width: '100%',
  height: '100%',
  title: '',
  subTitle: '',
  enableAnimation: true,
  columnWidth: 0.75,
  columnSpacing: 0,
  tooltip: {
    enable: true,
    textStyle: {
      fontFamily: 'inherit',
    },
  },
  chartArea: {
    border: {
      width: 0,
    },
    background: 'transparent',
    opacity: 1,
    backgroundImage: null,
    spacing: 0,
  },
  xAxis: {
    title: '',
    lineWidth: 0,
    tickWidth: 0,
    labelFormat: '',
    labelStyle: {
      color: 'white',
      fontWeight: 'Bold',
      size: '12px',
    },
    visible: true,
  },
  yAxis: {
    title: '',
    lineWidth: 0,
    tickWidth: 0,
    labelFormat: '',
    visible: false,
    minimum: 0,
  },
};

export const ORG_CHART_CONFIG = {
  dataSource: 'IntelOIR_ByOrg',
  ariaLabel: 'Originating Intel Reports by Organisation',
  legend: {
    visible: false,
    toggleVisibility: false,
    position: 'custom',
    width: '30%',
    height: '80%', //There's no setting for tooltip position, so reducing the height is the next best thing to stop overlap
    location: { x: 240, y: 0 },
  },
  dataLabel: {
    visible: false,
    position: 'Inside',
    name: '${point.value}',
    font: {
      color: 'white',
      fontWeight: 'Bold',
      size: '14px',
    },
  },
  tooltip: {
    enable: true,
    textStyle: {
      fontFamily: 'inherit',
    },
  },
  flexPosition: 'center center',
  startAngle: 0,
  endAngle: 360,
  innerRadius: '70%',
  enableAnimation: true,
  title: '',
  subTitle: '',
  titleProportion: 0.2,
  subTitleProportion: 0.08,
  centerTitles: true,
  titleSize: '2rem',
  chartLegendPadding: '5%',
  width: '170px',
  height: '100%',
};

export const PERFORMANCE_CHART_CONFIG = {
  dataSource: 'IntelOIR_SubmittedLast28Days',
  width: '100%',
  height: '100%',
  ariaLabel: 'Performance Data Line Chart',
  lineWidth: 1,
  lineColour: '#ffffff',
  isAreaGradient: true,
  areaColour: '#08e25f',
  tooltip: {
    enable: true,
    textStyle: {
      fontFamily: 'inherit',
    },
  },
  chartArea: {
    border: {
      width: 0,
    },
    background: 'transparent',
    opacity: 1,
    backgroundImage: null,
    spacing: 0,
  },
  xAxis: {
    title: 'Last 28 Days',
    titleStyle: {
      size: '12px',
      fontFamily: 'inherit',
      fontWeight: 'lighter',
    },
    lineWidth: 0,
    gridWidth: 0,
    tickWidth: 0,
    labelFormat: 'dd/MM/yyyy',
    labelPadding: 0,
    labelPlacement: 'onTicks',
    labelStyle: {},
    valueType: 'DateTime',
    visible: true,
    interval: 2,
    intervalType: 'Weeks',
    rangePadding: 'additional',
  },
  yAxis: {
    title: '',
    lineWidth: 0,
    gridWidth: 0,
    tickWidth: 0,
    labelFormat: '',
    visible: true,
  },
};
