import { FormDefinition } from '@nimbus/core/src/lib/core';
import { createReducer, on } from '@ngrx/store';
import { FormActions } from '../actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export const formFeatureKey = 'form';

export interface State extends EntityState<FormDefinition> {
  submissionResult: { success: boolean; errorMessage: string | null } | null;
}

export const adapter: EntityAdapter<FormDefinition> = createEntityAdapter<FormDefinition>({
  selectId: (form: FormDefinition) => form.id,
  sortComparer: false,
});

export const initialState: State = adapter.getInitialState({
  submissionResult: null,
});

// get the selectors
const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();

export const reducer = createReducer(
  initialState,
  on(FormActions.loadFormsSuccess, (state, { forms }): State => {
    return adapter.addMany(forms, state);
  }),
  on(FormActions.submit, (state, { formId, model, formAction }): State => {
    return { ...state, submissionResult: null };
  }),
  on(FormActions.submitClear, (state): State => {
    return { ...state, submissionResult: null };
  }),
  on(FormActions.submitSuccess, (state, { model }): State => {
    return { ...state, submissionResult: { success: true, errorMessage: null } };
  }),
  on(FormActions.submitFailure, (state, { error }): State => {
    return { ...state, submissionResult: { success: false, errorMessage: error } };
  })
);

export const selectFormEntities = selectEntities;
