import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { MetaDataColumnMatch } from '@nimbus/core/src/lib/core';
import { MetaDataKey } from '@nimbus/core/src/public-api';
import { MetaDataActions } from '../actions';

export const metaDataFeatureKey = 'metadata';
export type State = EntityState<MetaDataKey>;

export const adapter: EntityAdapter<MetaDataKey> = createEntityAdapter<MetaDataKey>({
  selectId: (key: MetaDataKey) => key.entityTypeLabelId,
  sortComparer: false,
});

export const initialState: State = adapter.getInitialState();

// get the selectors
const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();

export const reducer = createReducer(
  initialState,
  on(MetaDataActions.loadMetadatakeysSuccess, (state, { keys }) => {
    return adapter.addMany(keys || [], state);
  })
);

export const getMetaDataColumnMatches = (
  metaDataKeys: MetaDataKey[],
  listColumns: string[],
  entityTypeId: string
): MetaDataColumnMatch[] => {
  const returnColumns: MetaDataColumnMatch[] = listColumns.map((col) => ({
    columnName: col,
    uuid: '',
    entityTypeLabelId: entityTypeId + col,
  }));
  const metaMatches = metaDataKeys
    .filter((key) =>
      returnColumns.some((col) => col.entityTypeLabelId === key.entityTypeLabelId && key.uuid !== col.columnName)
    )
    .map((col) => ({
      columnName: col.label,
      uuid: col.uuid,
      entityTypeLabelId: entityTypeId + col.label,
    }));
  return metaMatches;
};
export const selectMetaDataItems = selectIds;
export const selectMetaDataEntities = selectEntities;
export const selectAllMetaDataDetails = selectAll;
export const selectMetaDataTotal = selectTotal;
