import { createReducer, on } from '@ngrx/store';
import { LayoutActions } from '../actions';

export const layoutFeatureKey = 'layout';

export interface State {
  menuState: 'open' | 'closed';
  menuPinned: boolean;
  showToolbars: boolean;
  contentClass: string;
  dashboardLayout: string;
  breakpoint: 'sm' | 'md' | 'lg';
}

const initialState: State = {
  menuState: 'closed',
  menuPinned: false,
  showToolbars: false,
  contentClass: 'content',
  dashboardLayout: 'grid',
  breakpoint: 'lg',
};

export const reducer = createReducer(
  initialState,
  on(
    LayoutActions.setMenuState,
    (state, { menuState }): State => ({
      ...state,
      menuState: menuState === 'closed' && state.menuPinned ? state.menuState : menuState,
    })
  ),
  on(LayoutActions.pinMenu, (state): State => ({ ...state, menuPinned: true })),
  on(LayoutActions.unpinMenu, (state): State => ({ ...state, menuPinned: false })),
  on(LayoutActions.setContentClass, (state, { contentClass }): State => ({ ...state, contentClass })),
  on(LayoutActions.showToolbars, (state): State => ({ ...state, showToolbars: true })),
  on(LayoutActions.hideToolbars, (state): State => ({ ...state, showToolbars: false })),
  on(LayoutActions.changeDashboardLayout, (state, { dashboardLayout }): State => ({ ...state, dashboardLayout })),
  on(LayoutActions.setBreakpoint, (state, { breakpoint }): State => ({ ...state, breakpoint }))
);
