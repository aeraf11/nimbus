import {
  RouteConfig,
  AppConfiguration,
  User,
  Theme,
  MetaDataItem,
  MenuItem,
  HomeItem,
} from '@nimbus/core/src/lib/core';
import { MenuItemConfig, HomeItemConfig } from '@nimbus/core/src/lib/apto';
import { createReducer, on } from '@ngrx/store';
import { AppConfigurationActions } from '../actions';
import { PermissionLogic } from '../../auth/models/permission-logic';
import { TimeZone } from '@nimbus/material';
import { UserProfile } from '../../auth/models/user-profile';

export const appConfigurationFeatureKey = 'appConfiguration';

export interface State {
  config: AppConfiguration | null;
  timeZones: TimeZone[] | null;
  selectedThemeId: string | null;
  isConfigured: boolean;
  upgradeMessage: string;
  defaultMetaData: { [key: number]: MetaDataItem[] } | null;
}

const initialState: State = {
  config: null,
  timeZones: null,
  selectedThemeId: null,
  isConfigured: false,
  upgradeMessage: '',
  defaultMetaData: null,
};

export const reducer = createReducer(
  initialState,
  on(
    AppConfigurationActions.loadConfigSuccess,
    (state, { config }): State => ({
      ...state,
      isConfigured: true,
      config: { ...state.config, ...config },
    })
  ),
  on(
    AppConfigurationActions.loadTimeZoneSuccess,
    (state, { timeZones }): State => ({
      ...state,
      timeZones,
    })
  ),
  on(
    AppConfigurationActions.setTheme,
    (state, { themeId }): State => ({
      ...state,
      selectedThemeId: themeId,
    })
  ),
  on(
    AppConfigurationActions.addHomeItems,
    (state, { homeConfig }): State => ({
      ...state,
      config: { ...state.config, homeConfig },
    })
  ),
  on(
    AppConfigurationActions.addMenuItems,
    (state, { menuConfig }): State => ({
      ...state,
      config: { ...state.config, menuConfig },
    })
  ),
  on(
    AppConfigurationActions.loadEntityDefaultMetaData,
    (state, { defaultMetaData }): State => ({
      ...state,
      defaultMetaData,
    })
  )
);

export const getMenuItems = (
  menuConfigs: MenuItemConfig[] | undefined,
  routes: RouteConfig[] | null,
  userPermissions: string[] | null,
  forceAgencyLookupId: string | null
): MenuItem[] => {
  if (!menuConfigs || !routes || !userPermissions) return [];
  const menuItems: MenuItemConfig[] = [];

  menuConfigs.forEach((config) => {
    let items: MenuItemConfig[] | undefined | null;
    if (config.items?.length || 0 > 0) {
      items = config.items;
    }
    menuItems.push({ ...config, items: items });
  });

  return getMenuItemsRecursive(menuItems, routes, userPermissions, forceAgencyLookupId);
};

export const getHeaderMenuItem = (
  menuConfigs: MenuItemConfig[] | undefined,
  routes: RouteConfig[] | null,
  userPermissions: string[] | null,
  forceAgencyLookupId: string | null
): MenuItem => {
  return getMenuItems(menuConfigs, routes, userPermissions, forceAgencyLookupId)[0];
};

export const getBodyMenuItems = (
  menuConfigs: MenuItemConfig[] | undefined,
  routes: RouteConfig[] | null,
  userPermissions: string[] | null,
  forceAgencyLookupId: string | null
): MenuItem[] => {
  return getMenuItems(menuConfigs, routes, userPermissions, forceAgencyLookupId).slice(1);
};

const getMenuItemsRecursive = (
  configs: MenuItemConfig[],
  routes: RouteConfig[],
  userPermissions: string[],
  forceAgencyLookupId: string | null
): MenuItem[] => {
  const permitted = configs.filter((config) => {
    if (!config.routeId) return true;
    let permittedRoute = true;

    const routeConfig = routes.find((r) => r.routeId === config.routeId);
    if (routeConfig?.permissionKeys) {
      switch (routeConfig?.permissionLogic || PermissionLogic.All) {
        case PermissionLogic.All:
          permittedRoute = routeConfig?.permissionKeys?.every((key) => userPermissions.includes(key));
          break;
        case PermissionLogic.Any:
          permittedRoute = routeConfig?.permissionKeys?.some((key) => userPermissions.includes(key));
          break;
        default:
          permittedRoute = false;
      }
    }
    if (routeConfig?.forceAgencyPermissions && permittedRoute) {
      if (!routeConfig?.forceAgencyPermissions.includes(forceAgencyLookupId || '')) {
        permittedRoute = false;
      }
    }
    return permittedRoute;
  });

  const mapped = permitted.map((config) => {
    const routeConfig = config.routeId ? routes.find((r) => r.routeId === config.routeId) : undefined;
    // Need to make sure of leading '/' to ensure route is not interpreted relatively
    let route = !routeConfig?.route
      ? '/'
      : routeConfig.route.startsWith('/')
      ? routeConfig.route
      : `/${routeConfig.route}`;

    if (routeConfig?.navigateFromRootOrigin) {
      route = window.location.origin + route;
    }

    return {
      name: config.name,
      icon: config.icon,
      route: route,
      routeId: config.routeId,
      navigateFromRootOrigin: routeConfig?.navigateFromRootOrigin ? routeConfig?.navigateFromRootOrigin : false,
      items: config.items ? getMenuItemsRecursive(config.items, routes, userPermissions, forceAgencyLookupId) : null,
      queryParams: config.queryParams,
    };
  });

  // Filter out empty items
  return mapped.filter((item) => item.items || item.route);
};

export const getHomeItems = (
  homeConfigs: HomeItemConfig[] | undefined,
  routes: RouteConfig[] | null,
  userPermissions: string[] | null
): HomeItem[] => {
  if (!homeConfigs || !routes || !userPermissions) return [];

  const items: HomeItem[] = [];
  homeConfigs.forEach((config) => {
    let permitted = false;

    const routeConfig = routes.find((r) => r.routeId === config.routeId);
    if (routeConfig) {
      if (!routeConfig.permissionKeys) {
        permitted = true;
      } else {
        switch (routeConfig.permissionLogic || PermissionLogic.All) {
          case PermissionLogic.All:
            permitted = routeConfig.permissionKeys.every((key) => userPermissions.includes(key));
            break;

          case PermissionLogic.Any:
            permitted = routeConfig.permissionKeys.some((key) => userPermissions.includes(key));
            break;

          default:
            permitted = false;
        }
      }
      if (permitted) {
        items.push({
          name: config.name,
          icon: config.icon,
          // Need to make sure of leading '/' to ensure route is not interpreted relatively
          route: routeConfig.route.startsWith('/') ? routeConfig.route : `/${routeConfig.route}`,
          separator: config.separator ?? false,
          standalone: config.standalone ?? false,
        });
      }
    }
  });
  return items;
};

export const getProfileModel = (user: User | null, themeId: string | undefined): UserProfile | null => {
  if (user) {
    return { ...user, themeId };
  }
  return null;
};
