import { createReducer } from '@ngrx/store';
import { Translations } from '@nimbus/core/src/lib/core';

export const translationFeatureKey = 'translation';

export interface State {
  translations: Translations;
}

const initialState: State = {
  translations: {
    _root: {
      'Oliver is logged in as {0}': 'Luke is signed in as {0}',
    },
    operation: {
      name: 'test',
    },
    intelreport: {
      'Add Intelligence report': 'Add Intelligence report',
      Step: 'Step',
      of: 'of',
      'Report Title': 'Report Title',
      'Add the Report Title': 'Add the Report Title',
      'Reporting Officer': 'Reporting Officer',
      'Add the Reporting Officer': 'Add the Reporting Officer',
      Organization: 'Organization',
      'Add the Organization': 'Add the Organization',
      'Date / Time Reported': 'Date / Time Reported',
      'Choose your Date / Time': 'Choose your Date / Time',
      Sensitivity: 'Sensitivity',
      Official: 'Official',
      Secret: 'Secret',
      'Top Secret': 'Top Secret',
      'Report URN': 'Report URN',
      'Add the Report URN': 'Add the Report URN',
      'Source / Source Reference Number (ISR)': 'Source / Source Reference Number (ISR)',
      'Add the Source / Source Reference Number': 'Add the Source / Source Reference Number',
      'Source Evaluation': 'Source Evaluation',
      Reliable: 'Reliable',
      Untested: 'Untested',
      'Not Reliable': 'Not Reliable',
      'Provenance Statement': 'Provenance Statement',
      'Add your Provenance Statement': 'Add your Provenance Statement',
      'A: Known Directly': 'A: Known Directly',
      'B: Known indirectly / Corroborated': 'B: Known indirectly / Corroborated',
      'C: Known Indirectly': 'C: Known Indirectly',
      'D: Not Known': 'D: Not Known',
      'E: Suspected to be False': 'E: Suspected to be False',
      'P: Lawful Sharing Permitted': 'P: Lawful Sharing Permitted',
      'C: Lawful Sharing Permitted With Conditions': 'C: Lawful Sharing Permitted With Conditions',
      'A1: Covert Development': 'A1: Covert Development',
      'A2: Covert Use': 'A2: Covert Use',
      'A3: Overt User': 'A3: Overt User',
      'S1: Delegated Authority': 'S1: Delegated Authority',
      'S2: Consult Originator': 'S2: Consult Originator',
      'Intelligence Item / Information': 'Intelligence Item / Information',
      'Event Date / Time': 'Event Date / Time',
      'Intelligence Assessment': 'Intelligence Assessment',
      'Handling Code': 'Handling Code',
      'Detailed Handling Instructions': 'Detailed Handling Instructions',
      'Action Code': 'Action Code',
      'Sanitisation Code': 'Sanitisation Code',
      'Create Intelligence Report': 'Create Intelligence Report',
      Details: 'Details',
      'Add Some Details': 'Add Some Details',
      'Add Details': 'Add Details',
      Items: 'Items',
      'Add a Report Item': 'Add a Report Item',
      'Add Item Information': 'Add Item Information',
      'Add Items': 'Add Items',
      Submit: 'Submit',
      'Review and submit': 'Review and submit',
    },
  },
};

export const reducer = createReducer(initialState);
