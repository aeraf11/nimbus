import { createReducer, on } from '@ngrx/store';
import { StatusActions } from '../actions';

export const statusFeatureKey = 'status';

export interface State {
  online: boolean;
  loading: boolean;
}

const initialState: State = {
  online: true,
  loading: false,
};

export const reducer = createReducer(
  initialState,
  on(StatusActions.setOnline, (state): State => ({ ...state, online: true })),
  on(StatusActions.setOffline, (state): State => ({ ...state, online: false })),
  on(StatusActions.setLoading, (state, { loading }): State => ({ ...state, loading }))
);

export const selectOnline = (state: State) => state.online;
export const selectLoading = (state: State) => state.loading;
