import { createActionGroup, props } from '@ngrx/store';

export const TranslationActions = createActionGroup({
  source: 'Translation',
  events: {
    'Translation Load Success': props<{ loading: boolean }>(),
  },
});
