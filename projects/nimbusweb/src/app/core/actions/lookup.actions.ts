import { LookupDetail } from '@nimbus/core/src/lib/core';
import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const LookupActions = createActionGroup({
  source: 'Lookup',
  events: {
    'Load Lookups': emptyProps,
    'Load Lookups Success': props<{ lookups: LookupDetail[] | undefined }>(),
    'Add Lookup': props<{ lookup: LookupDetail }>(),
  },
});
