import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const StatusActions = createActionGroup({
  source: 'Status',
  events: {
    'Set Loading': props<{ loading: boolean }>(),
    'Update Online Status': props<{ online: boolean }>(),
    'Set Online': emptyProps,
    'Set Offline': emptyProps,
  },
});
