import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const LayoutActions = createActionGroup({
  source: 'Layout',
  events: {
    'Open Menu': emptyProps,
    'Close Menu': emptyProps,
    'Pin Menu': emptyProps,
    'Unpin Menu': emptyProps,
    'Set Menu State': props<{ menuState: 'closed' | 'open' }>(),
    'Set Content Class': props<{ contentClass: string }>(),
    'Show Toolbars': emptyProps,
    'Hide Toolbars': emptyProps,
    'Change Dashboard Layout': props<{ dashboardLayout: string }>(),
    'Set Breakpoint': props<{ breakpoint: 'sm' | 'md' | 'lg' }>(),
  },
});
