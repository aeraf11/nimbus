import { DialogFormAction, FormDefinition } from '@nimbus/core/src/lib/core';
import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const FormActions = createActionGroup({
  source: 'Form',
  events: {
    'Load Forms': emptyProps,
    'Load Forms Success': props<{ forms: FormDefinition[] }>(),
    'Load Forms Failure': props<{ error: any }>(),
    'Submit Clear': emptyProps,
    Submit: props<{ model: any; formId: string; formAction: DialogFormAction; context: any }>(),
    'Submit Success': props<{ model: any; formId: string; formAction: DialogFormAction; context: any }>(),
    'Submit Failure': props<{
      model: any;
      formId: string;
      formAction: DialogFormAction;
      context: any;
      error: string;
    }>(),
  },
});
