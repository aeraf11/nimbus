import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { DynamicRoute, DataListOptions, CardOptions } from '@nimbus/core/src/lib/core';
import { DynamicRoutePostResult } from '@nimbus/core/src/lib/apto';

export const DynamicRouteActions = createActionGroup({
  source: 'DynamicRoutes',
  events: {
    'Load Dynamic Routes': emptyProps,
    'Load Dynamic Routes Success': props<{ dynamicRoutes: DynamicRoute[] }>(),
    'Load Dynamic Routes Failure': props<{ error: any }>(),
    'Load Current Dynamic Route Model': props<{ routeId: string; context: any }>(),
    'Load Current Dynamic Route Model Success': props<{ model: any }>(),
    'Load Current Dynamic Route Model Failure': props<{ error: any }>(),
    'Clear Current Dynamic Route Model': emptyProps,
    'Post Form': props<{ model: any; routeId: string; onSuccess?: () => void; showErrorSnack?: boolean }>(),
    'Post Form Success': props<{
      routeId: string;
      result: DynamicRoutePostResult;
      model: any;
      onSuccess?: () => void;
    }>(),
    'Post Form Failure': props<{ error: any }>(),
    'Set Data List Options': props<{ savedOptions: { [key: string]: DataListOptions } }>(),
    'Save Data List Options': props<{ options: DataListOptions }>(),
    'Save Card Options': props<{ options: CardOptions }>(),
    'Merge Dynamic Route Model': props<{
      model: any;
      routeId: string;
      routeParams: any;
      autoFillModelSkipProps?: string[];
      autoFillModelOverrides?: any[];
    }>(),
  },
});
