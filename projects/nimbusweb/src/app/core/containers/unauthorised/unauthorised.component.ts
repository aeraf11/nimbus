import { Component } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngrx/store';
import * as fromAuth from '../../../auth/reducers';

@UntilDestroy()
@Component({
  selector: 'nw-unauthorised',
  templateUrl: './unauthorised.component.html',
  styleUrls: ['./unauthorised.component.scss'],
})
export class UnauthorisedComponent {
  showContent = false;

  constructor(private store: Store<fromAuth.State>) {
    store
      .select(fromAuth.selectIsLoggedIn)
      .pipe(untilDestroyed(this))
      .subscribe((isLoggedIn) => (this.showContent = isLoggedIn));
  }
}
