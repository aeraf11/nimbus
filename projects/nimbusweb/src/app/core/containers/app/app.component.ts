import { animate, animateChild, group, query, state, style, transition, trigger } from '@angular/animations';
import { Location } from '@angular/common';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngrx/store';
import { FormActionService } from '@nimbus/core/src/lib/apto';
import {
  AppInsightsService,
  MediaService,
  MenuItem,
  selectClone,
  ServiceWorkerConfig,
  SwUpdatesService,
} from '@nimbus/core/src/lib/core';
import deepClone from 'deep.clone';
import { fromEvent, merge, Observable, of } from 'rxjs';
import { distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';
import { AuthActions } from '../../../auth/actions';
import * as fromAuth from '../../../auth/reducers';
import * as fromRoot from '../../../reducers';
import { LayoutActions, StatusActions } from '../../actions';

@UntilDestroy()
@Component({
  selector: 'nw-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('menuToggle', [
      state(
        'open',
        style({
          width: '330px',
        })
      ),
      state(
        'closed',
        style({
          width: '100px',
        })
      ),
      transition('closed => open', [
        style({
          width: '100px',
        }),
        group([
          animate(
            '225ms cubic-bezier(0.4,0.0,0.2,1)',
            style({
              width: '330px',
            })
          ),
          query('@toggleChild', [animateChild()], { optional: true }),
          query('@menuToggleFade', [animateChild()], { optional: true }),
        ]),
      ]),
      transition('open => closed', [
        style({
          width: '330px',
        }),
        group([
          animate(
            '225ms cubic-bezier(0.4,0.0,0.2,1)',
            style({
              width: '100px',
            })
          ),
          query('@toggleChild', [animateChild()], { optional: true }),
          query('@menuToggleFade', [animateChild()], { optional: true }),
        ]),
      ]),
    ]),
    trigger('menuToggleFade', [
      state(
        'open',
        style({
          opacity: '100%',
        })
      ),
      state(
        'closed',
        style({
          opacity: '0%',
        })
      ),
      transition('closed => open', [animate('250ms')]),
      transition('open => closed', [animate('400ms')]),
    ]),
    trigger('menuPinned', [
      state(
        'true',
        style({
          'margin-left': '330px',
        })
      ),
      state(
        'false',
        style({
          'margin-left': '100px',
        })
      ),
      transition('false => true', [animate('250ms')]),
      transition('true => false', [animate('400ms')]),
    ]),
  ],
})
export class AppComponent implements OnInit {
  menuState$ = this.store.select(fromRoot.selectMenuState);
  menuPinned$ = this.store.select(fromRoot.selectMenuPinned);
  loading$ = this.store.select(fromRoot.selectLoading);
  contentClasses$ = this.store.select(fromRoot.selectContentClass);
  contentClass$: Observable<string>;
  user$ = this.store.select(fromAuth.selectUser);
  showToolbars$ = this.store.select(fromRoot.selectShowToolbars);
  serviceWorkerConfig!: ServiceWorkerConfig;
  menuHeaderItem$ = this.store.select(fromRoot.selectHeaderMenuItem);
  menuBodyItems$ = this.store.select(fromRoot.selectBodyMenuItems);
  routeId$ = this.store.select(fromRoot.selectRouteId);
  profileFormDefinitions$ = this.store.pipe(selectClone(fromRoot.selectFormFields('Investigator.Profile.View')));
  profileFormModel$ = this.store.pipe(selectClone(fromRoot.selectProfileModel));

  constructor(
    private store: Store<fromRoot.State & fromAuth.State>,
    private location: Location,
    private router: Router,
    public mediaService: MediaService,
    private mediaObserver: MediaObserver,
    private swUpdate: SwUpdatesService,
    private formActionService: FormActionService,
    private appInsights: AppInsightsService
  ) {
    this.contentClass$ = this.contentClasses$.pipe(switchMap((classes) => mediaService.getBreakpointValue(classes)));
  }

  ngOnInit() {
    const path = this.location.path();
    if (path !== '/loggedout')
      this.store.dispatch(AuthActions.login({ redirect: path === '/landing' ? '/investigator' : path }));

    merge(of(null), fromEvent(window, 'online'), fromEvent(window, 'offline'))
      .pipe(
        untilDestroyed(this),
        map(() => navigator.onLine)
      )
      .subscribe((online) => this.store.dispatch(StatusActions.updateOnlineStatus({ online })));

    this.store
      .select(fromRoot.selectServiceWorkerConfig)
      .pipe(untilDestroyed(this))
      .subscribe((serviceWorkerConfig) => this.swUpdate.start(serviceWorkerConfig));

    this.listenToBreakpoint();
  }

  setMenuState(value: 'open' | 'closed') {
    this.store.dispatch(LayoutActions.setMenuState({ menuState: value }));
  }

  setMenuPinned(state: boolean) {
    if (state) {
      this.store.dispatch(LayoutActions.pinMenu());
    } else {
      this.store.dispatch(LayoutActions.unpinMenu());
    }
  }

  onMouseEnter(event: any) {
    this.setMenuState('open');
  }

  onMouseLeave(event: any) {
    if (event.clientX > 0) {
      this.setMenuState('closed');
    }
  }

  onMenuItemSelected(item: Partial<MenuItem>) {
    if (item.route) {
      if (!item.navigateFromRootOrigin) {
        this.router.navigate([item.route], { queryParams: item.queryParams });
      } else {
        window.location.href = item.route;
      }
    }
  }

  menuHeaderItemRouteMatch(item: MenuItem, routeId: string): boolean {
    if (item && routeId) {
      if (item?.routeId && routeId && item.routeId === routeId) return true;
    }
    return false;
  }

  private listenToBreakpoint(): void {
    this.mediaObserver
      .asObservable()
      .pipe(
        distinctUntilChanged(),
        tap((media: MediaChange[]) => {
          if (media.find((m) => m.mqAlias === 'lt-md')) {
            this.store.dispatch(LayoutActions.setBreakpoint({ breakpoint: 'sm' }));
          }
          if (media.find((m) => m.mqAlias === 'md')) {
            this.store.dispatch(LayoutActions.setBreakpoint({ breakpoint: 'md' }));
          }
          if (media.find((m) => m.mqAlias === 'gt-md')) {
            this.store.dispatch(LayoutActions.setBreakpoint({ breakpoint: 'lg' }));
          }
        })
      )
      .subscribe();
  }

  handleFormAction(event: { model: any; action?: string; routeId: string }) {
    deepClone(event.model).then((modelClone) =>
      this.formActionService.handleFormAction(modelClone, event.action, event.routeId)
    );
  }
}
