import { MockStateService } from '@nimbus/core/src/lib/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Platform } from '@angular/cdk/platform';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import { RouterTestingModule } from '@angular/router/testing';
import { ServiceWorkerModule } from '@angular/service-worker';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { AppDateTimeAdapter, MaterialModule, SiteConfig, StateService } from '@nimbus/core/src/lib/core';
import { DateTimeAdapter } from '@nimbus/material';
import { selectAuthUserState } from '../../../auth/reducers';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let store: MockStore;
  const initialState = {};

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ServiceWorkerModule.register('ngsw-worker.js', { enabled: false }),
        MaterialModule,
        HttpClientTestingModule,
      ],
      declarations: [AppComponent],
      providers: [
        provideMockStore({ initialState }),
        {
          provide: DateTimeAdapter,
          useClass: AppDateTimeAdapter,
          deps: [MAT_DATE_LOCALE, Platform, MockStore],
        },
        {
          provide: DateAdapter,
          useClass: AppDateTimeAdapter,
          deps: [MAT_DATE_LOCALE, Platform, MockStore],
        },
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
        { provide: StateService, useClass: MockStateService },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    store = TestBed.inject(MockStore);
    store.overrideSelector(selectAuthUserState, {
      user: {
        userProfileId: '',
        fullName: '',
        groupName: '',
        email: '',
        contactNumber: '',
        branch: '',
        permissions: [],
        dateFormat: '',
        timeFormat: '',
        thumbnailBlobId: '',
        initials: '',
        forceAgencyLookupId: '',
        timeZoneId: '',
      },
    });
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
