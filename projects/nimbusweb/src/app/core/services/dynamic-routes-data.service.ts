import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
import { DynamicRoutePostResult } from '@nimbus/core/src/lib/apto';
import { DynamicRoute, StateService } from '@nimbus/core/src/lib/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class DynamicRoutesDataService {
  private routesUrl = `${environment.apiUrl}/dynamicroute/all?siteId=`;
  private mainUrl = `${environment.apiUrl}/dynamicroute`;
  private siteId?: string;

  constructor(private httpClient: HttpClient, private stateService: StateService) {
    this.stateService
      .selectSiteId()
      .pipe(untilDestroyed(this))
      .subscribe((id) => {
        this.siteId = id;
      });
  }

  loadRoutes(siteId?: string): Observable<DynamicRoute[]> {
    if (!siteId) {
      siteId = this.siteId ?? '';
    }
    return this.httpClient.get<DynamicRoute[]>(this.routesUrl + siteId);
  }

  loadModel(name: string, context: any): Observable<any> {
    const paramsArray = [`name=${name}`, `siteId=${this.siteId}`];
    if (context) {
      Object.entries(context).forEach(([key, value]) => {
        paramsArray.push(`${key}=${value}`);
      });
    }
    const url = `${this.mainUrl}?${paramsArray.join('&')}`;

    return this.httpClient.get<any>(url);
  }

  postForm(name: string, model: any): Observable<DynamicRoutePostResult> {
    return this.httpClient.post<DynamicRoutePostResult>(`${this.mainUrl}?siteId=${this.siteId}&name=${name}`, model);
  }
}
