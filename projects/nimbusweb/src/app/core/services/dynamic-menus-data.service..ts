import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DynamicMenus } from '@nimbus/core/src/lib/apto';
import { environment } from '../../../environments/environment';
import { StateService } from '@nimbus/core/src/lib/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class DynamicMenusDataService {
  private menusUrl = `${environment.apiUrl}/dynamicmenu/all?siteId=`;
  private siteId?: string;
  constructor(private httpClient: HttpClient, private stateService: StateService) {
    this.stateService
      .selectSiteId()
      .pipe(untilDestroyed(this))
      .subscribe((id) => {
        this.siteId = id;
      });
  }

  loadMenus(siteId?: string): Observable<DynamicMenus> {
    if (!siteId) {
      siteId = this.siteId ?? '';
    }
    return this.httpClient.get<DynamicMenus>(this.menusUrl + siteId);
  }
}
