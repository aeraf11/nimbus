import { Injectable } from '@angular/core';
import { Dictionary } from '@ngrx/entity';
import { Store } from '@ngrx/store';
import {
  ApplicationInsightsConfig,
  CardOptions,
  DataListOptions,
  DebugOptions,
  DialogFormAction,
  DynamicRoute,
  FileUploadConfig,
  FormDefinition,
  Lookup,
  LookupDetail,
  LookupDictionary,
  LookupHierarchy,
  LookupNode,
  MetaDataColumnMatch,
  MetaDataItem,
  MetaDataKey,
  ScheduledJob,
  selectClone,
  SignalRHub,
  StateService,
  Theme,
  User,
} from '@nimbus/core/src/lib/core';
import { TimeZone } from '@nimbus/material';
import { Observable, of } from 'rxjs';
import * as fromAuth from '../../auth/reducers';
import * as fromRoot from '../../reducers';
import {
  AppConfigurationActions,
  DynamicRouteActions,
  FormActions,
  LayoutActions,
  LookupActions,
  StatusActions,
} from '../actions';
import { Translations } from '@nimbus/core/src/lib/core';

@Injectable({
  providedIn: 'root',
})
export class AppStateService extends StateService {
  private readonly notImplemented = 'Not implemented in Nimbusweb.';

  constructor(private store: Store<fromRoot.State & fromAuth.State>) {
    super();
  }

  // Selectors
  selectDataListOptions(savedOptionsId: string): Observable<DataListOptions> {
    return this.store.select(fromRoot.selectDataListOptions(savedOptionsId));
  }

  selectUser(): Observable<User | null> {
    return this.store.select(fromAuth.selectUser);
  }

  selectForm(formId: string): Observable<FormDefinition | undefined> {
    return this.store.pipe(selectClone(fromRoot.selectForm(formId)));
  }

  selectDateTimeFormats(): Observable<{ dateFormat?: string; timeFormat?: string; timeZoneId?: string }> {
    return this.store.select(fromAuth.selectDateTimeFormats);
  }

  selectTimeZones(): Observable<TimeZone[] | null> {
    return this.store.select(fromRoot.selectTimeZones);
  }

  selectLookupDetailEntities(): Observable<Dictionary<LookupDetail>> {
    return this.store.select(fromRoot.selectLookupDetailEntities);
  }

  selectLookupsHierarchy(): Observable<LookupHierarchy> {
    return this.store.select(fromRoot.selectLookupsHierarchy);
  }

  selectLookupsFlat(): Observable<LookupDictionary> {
    return this.store.select(fromRoot.selectLookupsFlat);
  }

  selectLookupsFlatFiltered(filters: string[]): Observable<Lookup[]> {
    return this.store.select(fromRoot.selectLookupsFlatFiltered(filters));
  }

  selectLookupsHierarchyFiltered(filters: string[]): Observable<LookupNode[]> {
    return this.store.select(fromRoot.selectLookupsHierarchyFiltered(filters));
  }

  selectLookupsForList(filters: string[]): Observable<LookupNode[]> {
    return this.store.select(fromRoot.selectLookupsForList(filters));
  }

  selectThemes(): Observable<Theme[] | undefined> {
    return this.store.select(fromRoot.selectThemes);
  }

  selectSelectedThemeIsDark(): Observable<boolean | undefined> {
    return this.store.select(fromRoot.selectSelectedThemeIsDark);
  }

  selectCurrentDynamicForm(): Observable<FormDefinition | undefined> {
    return this.store.pipe(selectClone(fromRoot.selectCurrentDynamicForm));
  }

  selectDynamicRouteCurrentModel(): Observable<any> {
    return this.store.pipe(selectClone(fromRoot.selectDynamicRouteCurrentModel));
  }

  selectDynamicRouteCurrentError(): Observable<string | null> {
    return this.store.pipe(selectClone(fromRoot.selectDynamicRouteCurrentError));
  }

  selectCurrentDynamicRoute(): Observable<DynamicRoute | undefined> {
    return this.store.select(fromRoot.selectCurrentDynamicRoute);
  }

  selectFormSubmissionResult(): Observable<{ success: boolean; errorMessage: string | null } | null> {
    return this.store.select(fromRoot.selectFormSubmissionResult);
  }

  selectCurrentRoute(): Observable<any> {
    return this.store.select(fromRoot.selectCurrentRoute);
  }

  selectDebugOptions(): Observable<DebugOptions | undefined> {
    return this.store.select(fromRoot.selectDebugOptions);
  }

  selectAppInsightsConfig(): Observable<ApplicationInsightsConfig | undefined> {
    return this.store.select(fromRoot.selectAppInsightsConfig);
  }

  selectFileUploadConfig(): Observable<FileUploadConfig | undefined> {
    return this.store.select(fromRoot.selectFileUploadConfig);
  }

  selectSignalRConfig(): Observable<SignalRHub[] | undefined> {
    return this.store.select(fromRoot.selectSignalRConfig);
  }

  selectFormStateSettings(): Observable<{ [key: string]: any } | undefined> {
    return this.store.select(fromRoot.selectFormStateSettings);
  }

  selectDashboardLayout(): Observable<string> {
    return this.store.select(fromRoot.selectDashboardLayout);
  }

  selectMenuState(): Observable<'open' | 'closed'> {
    return this.store.select(fromRoot.selectMenuState);
  }

  selectCurrentBreakpoint(): Observable<'sm' | 'md' | 'lg'> {
    return this.store.select(fromRoot.selectBreakpoint);
  }

  selectRoutesWithPermission(routes: any[]) {
    return this.store.select(fromRoot.selectRoutesWithPermission(routes));
  }

  selectConnectionOnline(): Observable<boolean> {
    return of(true);
  }

  selectWorkOnline(): Observable<boolean> {
    return of(true);
  }

  selectSiteId(): Observable<string | undefined> {
    return this.store.select(fromRoot.selectSiteId);
  }

  selectScheduledJobs(): Observable<ScheduledJob[]> {
    return this.store.select(fromRoot.selectScheduledJobs);
  }

  selectScheduledJob(id: string): Observable<ScheduledJob | undefined> {
    return this.store.select(fromRoot.selectScheduledJob(id));
  }

  selectUpgradeConfigMessage(): Observable<string | undefined> {
    return this.store.select(fromRoot.selectUpgradeConfigMessage);
  }

  selectMetaDataKeyColumns(listColumns: string[], entityType: string): Observable<MetaDataColumnMatch[]> {
    return this.store.select(fromRoot.selectMetaDataKeyColumns(listColumns, entityType));
  }

  selectMetaDateKeyDetails(): Observable<MetaDataKey[]> {
    return this.store.select(fromRoot.selectMetaDataKeyDetails);
  }

  selectDefaultMetaDataByEntityTypeId(id: number): Observable<MetaDataItem[]> {
    return this.store.select(fromRoot.selectDefaultMetaDataByEntityTypeId(id));
  }

  selectAllTranslations(): Observable<Translations> {
    return this.store.select(fromRoot.selectAllTranslations);
  }

  selectEnableFormTranslations(): Observable<boolean | undefined> {
    return this.store.select(fromRoot.selectEnableFormTranslations);
  }

  // Actions
  mergeDynamicRouteModel(
    model: any,
    routeId: string,
    routeParams: { [x: string]: any },
    autoFillModelOverrides: any[] | undefined,
    autoFillModelSkipProps: any[] | undefined
  ): void {
    this.store.dispatch(
      DynamicRouteActions.mergeDynamicRouteModel({
        model,
        routeId,
        routeParams,
        autoFillModelOverrides,
        autoFillModelSkipProps,
      })
    );
  }

  updateDynamicRouteModel(model: unknown): void {
    this.store.dispatch(DynamicRouteActions.loadCurrentDynamicRouteModelSuccess({ model: model }));
  }

  saveOptions(options: DataListOptions): void {
    this.store.dispatch(DynamicRouteActions.saveDataListOptions({ options }));
  }

  saveCardOptions(options: CardOptions): void {
    this.store.dispatch(DynamicRouteActions.saveCardOptions({ options }));
  }

  saveBookmark(routeId: string, bookmark: any): void {
    throw new Error(this.notImplemented);
  }

  setLoading(loading: boolean): void {
    this.store.dispatch(StatusActions.setLoading({ loading }));
  }

  loadCurrentDynamicRouteModel(routeId: string, context: any): void {
    this.store.dispatch(DynamicRouteActions.loadCurrentDynamicRouteModel({ routeId, context }));
  }

  loadCurrentDynamicRouteModelOffline(routeId: string, context: any): void {
    throw new Error(this.notImplemented);
  }

  postForm(model: any, routeId: string, onSuccess?: () => void, showErrorSnack?: boolean): void {
    this.store.dispatch(DynamicRouteActions.postForm({ model, routeId, onSuccess, showErrorSnack }));
  }

  postFormOffline(model: any, context: any, routeId: string): void {
    throw new Error(this.notImplemented);
  }

  clearCurrentDynamicRouteModel(): void {
    this.store.dispatch(DynamicRouteActions.clearCurrentDynamicRouteModel());
  }

  addLookup(lookup: LookupDetail): void {
    this.store.dispatch(LookupActions.addLookup({ lookup }));
  }

  submitFormClear(): void {
    this.store.dispatch(FormActions.submitClear());
  }

  submitForm(model: any, formAction: DialogFormAction, formId: string, context: any): void {
    this.store.dispatch(FormActions.submit({ model, formAction, formId, context }));
  }

  setDashboardLayout(dashboardLayout: string): void {
    this.store.dispatch(LayoutActions.changeDashboardLayout({ dashboardLayout }));
  }

  setTheme(themeId: string): void {
    this.store.dispatch(AppConfigurationActions.setTheme({ themeId }));
  }

  upgradeConfig(): void {
    this.store.dispatch(AppConfigurationActions.upgradeConfig());
  }
}
