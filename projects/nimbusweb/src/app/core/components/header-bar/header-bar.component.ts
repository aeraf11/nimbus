import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { UntilDestroy } from '@ngneat/until-destroy';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { DialogFormComponent, DialogFormData, EditorMode } from '@nimbus/core/src/lib/core';
import { StateService } from '@nimbus/core/src/lib/core';
import { UserProfile } from '../../../auth/models/user-profile';

@UntilDestroy()
@Component({
  selector: 'nw-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderBarComponent {
  @Input() formDefinition: FormlyFieldConfig[] = [];
  @Input() userProfile: UserProfile | null = null;
  @Output() formAction: EventEmitter<{ model: any; action?: string; routeId: string }> = new EventEmitter();

  constructor(private dialog: MatDialog, private state: StateService) {}

  openProfileDialog(): void {
    const data: DialogFormData = {
      model: this.userProfile,
      fieldConfig: this.formDefinition,
      config: {
        enableDelete: false,
        enableEdit: false,
        hideTitle: true,
        isChildForm: true,
      },
      editorMode: EditorMode.None,
      disabled: false,
      options: { formState: { executeAction: (action: string, model: unknown) => this.executeAction(action, model) } },
    };
    const dialogConfig: MatDialogConfig<DialogFormData> = { data, width: '100%', maxWidth: '800px' };
    this.dialog.open(DialogFormComponent, dialogConfig);
  }

  executeAction(action: string, model: unknown) {
    this.formAction.emit({ action, model, routeId: 'Investigator.Profile.View' });
  }
}
