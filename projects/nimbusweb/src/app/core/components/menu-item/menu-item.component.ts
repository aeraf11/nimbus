import { Component, ChangeDetectionStrategy, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { OnChange } from 'property-watch-decorator';
import { MenuItem } from '@nimbus/core/src/lib/core';
@Component({
  selector: 'nw-menu-item',
  templateUrl: './menu-item.component.html',
  styleUrls: ['./menu-item.component.scss'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(-90deg)' })),
      state('expanded', style({ transform: 'rotate(0deg)' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4,0.0,0.2,1)')),
    ]),
    trigger('toggleChild', [
      state('collapsed', style({ height: '0px' })),
      state('expanded', style({ height: '*' })),
      transition('expanded => collapsed', [
        style({ height: '*' }),
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)', style({ height: '0px' })),
      ]),
      transition('collapsed => expanded', [
        style({ height: '0px' }),
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)', style({ height: '*' })),
      ]),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuItemComponent {
  active!: boolean;

  @OnChange<string>(function (this: MenuItemComponent, value) {
    const match = this.routeMatch(this.item, value);
    if ((match.childMatch || match.match) && this.item.items && this.item.items.length) this.expanded = true;
    this.active = match.match;
  })
  @Input()
  routeId!: string;

  @OnChange(function (this: MenuItemComponent, value: MenuItem) {
    if (this.routeMatch(value, this.routeId).childMatch) this.expanded = true;
  })
  @Input()
  item!: MenuItem;

  @Input() depth!: number;
  @Input() menuState: 'open' | 'closed' = 'closed';
  @Input() expanded = false;
  @Output() itemSelected = new EventEmitter<Partial<MenuItem>>();

  @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
  @HostBinding('class.nw-menu-item') hostClass = true;
  @HostBinding('class.child') @Input() child = false;

  onItemSelected(item: Partial<MenuItem>) {
    if (item.items && item.items.length) {
      this.expanded = !this.expanded;
      if (this.menuState === 'closed') {
        this.expanded = true;
      }
    } else {
      this.itemSelected.emit(item);
    }
  }

  routeMatch(item: MenuItem, routeId: string): { match: boolean; childMatch: boolean } {
    if (item && routeId) {
      if (item?.routeId && routeId && item.routeId === routeId) return { match: true, childMatch: false };
      if (item.items) {
        const childMatch = item.items.some((child) => {
          const result = this.routeMatch(child, routeId);
          return result.match || result.childMatch;
        });
        if (childMatch) return { match: false, childMatch };
      }
    }
    return { match: false, childMatch: false };
  }
}
