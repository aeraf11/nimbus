import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { FormDataService, utils } from '@nimbus/core/src/lib/core';
import { of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { FormActions, StatusActions } from '../actions';

@Injectable()
export class FormEffects {
  getForms$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FormActions.loadForms),
      mergeMap(() =>
        this.service.loadForms().pipe(
          map((forms) => (forms = utils.formsSectionLoader(forms))),
          map((forms) => FormActions.loadFormsSuccess({ forms })),
          catchError((error) => {
            console.error(error);
            return of(FormActions.loadFormsFailure({ error }));
          })
        )
      )
    );
  });

  submitFormLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FormActions.submit),
      map(() => StatusActions.setLoading({ loading: true }))
    );
  });

  submitFormCompleteLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FormActions.submitSuccess, FormActions.submitFailure),
      map(() => StatusActions.setLoading({ loading: false }))
    );
  });

  constructor(private actions$: Actions, private service: FormDataService) {}
}
