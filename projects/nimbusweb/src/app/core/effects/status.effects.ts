import { StatusActions } from '../actions';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import { SnackMessageComponent, SnackMessageType } from '@nimbus/core/src/lib/core';

@Injectable()
export class StatusEffects {
  online$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(StatusActions.updateOnlineStatus),
      concatLatestFrom(() => this.store.select(fromRoot.selectOnline)),
      tap(([{ online }, currentStatus]) => {
        if (online !== currentStatus) {
          if (online)
            this.snackbar.openFromComponent(SnackMessageComponent, {
              duration: 3000,
              data: { type: SnackMessageType.Info, message: 'You are online', action: 'OK' },
            });
          else
            this.snackbar.openFromComponent(SnackMessageComponent, {
              duration: 10000,
              data: { type: SnackMessageType.Warn, message: 'You are offline', action: 'OK' },
            });
        }
      }),
      map(([{ online }]) => {
        return online ? StatusActions.setOnline() : StatusActions.setOffline();
      })
    );
  });

  constructor(private actions$: Actions, private store: Store<fromRoot.State>, private snackbar: MatSnackBar) {}
}
