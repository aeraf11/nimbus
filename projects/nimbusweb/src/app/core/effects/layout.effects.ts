import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, tap } from 'rxjs/operators';
import { UserActions } from '../../auth/actions';
import { LayoutActions } from '../actions';

const MENU_STATE_KEY = 'menuState';

const DASHBOARD_LAYOUT_KEY = 'dashboardLayout';

@Injectable()
export class LayoutEffects {
  openMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(LayoutActions.openMenu),
      map(() => LayoutActions.setMenuState({ menuState: 'open' }))
    );
  });

  closeMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(LayoutActions.closeMenu),
      map(() => LayoutActions.setMenuState({ menuState: 'closed' }))
    );
  });

  saveState$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(LayoutActions.setMenuState),
        tap((action) => localStorage.setItem(MENU_STATE_KEY, action.menuState))
      );
    },
    { dispatch: false }
  );

  loadState$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.loadUserDetailsSuccess),
      map(() => {
        const menuState = localStorage.getItem(MENU_STATE_KEY);
        switch (menuState) {
          case 'open':
            return LayoutActions.openMenu();
          case 'closed':
          default:
            return LayoutActions.closeMenu();
        }
      })
    );
  });

  changeDashboardLayout$ = createEffect(
    () => () => {
      return this.actions$.pipe(
        ofType(LayoutActions.changeDashboardLayout),
        tap((action) => localStorage.setItem(DASHBOARD_LAYOUT_KEY, action.dashboardLayout))
      );
    },
    { dispatch: false }
  );

  loadDashboardLayout$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.loadUserDetailsSuccess),
      map(() => {
        const layoutState = localStorage.getItem(DASHBOARD_LAYOUT_KEY);
        return LayoutActions.changeDashboardLayout({ dashboardLayout: layoutState ?? 'grid' });
      })
    );
  });

  constructor(private actions$: Actions) {}
}
