import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Route, Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { DynamicRouteComponent } from '@nimbus/core/src/lib/apto';
import {
  AppInsightsService,
  CanDeactivateGuard,
  OptionsService,
  ListService,
  SnackMessageComponent,
  SnackMessageType,
  utils,
} from '@nimbus/core/src/lib/core';
import { of } from 'rxjs';
import { catchError, filter, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import * as fromRoot from '../../reducers';
import { DynamicRouteActions, StatusActions } from '../actions';
import { DynamicRoutesDataService } from '../services/dynamic-routes-data.service';
import { AuthGuard } from './../../auth/services/auth.guard';
import { NotFoundPageComponent } from '../containers';

@Injectable()
export class DynamicRouteEffects {
  getRoutes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.loadDynamicRoutes),
      mergeMap(() =>
        this.drService.loadRoutes().pipe(
          map((dynamicRoutes) => DynamicRouteActions.loadDynamicRoutesSuccess({ dynamicRoutes })),
          catchError((error) => {
            console.error(error);
            this.appInsightService.logException(error, 3);
            return of(DynamicRouteActions.loadDynamicRoutesFailure({ error }));
          })
        )
      )
    );
  });

  pushRoutesToConfig$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DynamicRouteActions.loadDynamicRoutesSuccess),
        tap(({ dynamicRoutes }) => {
          const config: Route[] = [];
          dynamicRoutes.forEach((route) => {
            config.push({
              path: route.route,
              component: DynamicRouteComponent,
              data: {
                id: route.routeId,
                title: route.pageTitle,
                contentClass: route.cssClass,
              },
              canActivate: [AuthGuard],
              canDeactivate: [CanDeactivateGuard],
            });
          });
          const clean = this.router.config.filter((r) => r.path !== '**');
          const notfound = [
            {
              path: '**',
              component: NotFoundPageComponent,
              data: { title: 'Not Found' },
            },
          ];
          this.router.resetConfig([...config, ...clean, ...notfound]);
        })
      );
    },
    { dispatch: false }
  );

  getCurrentRouteModel$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.loadCurrentDynamicRouteModel),
      mergeMap(({ routeId: name, context }) =>
        this.drService.loadModel(name, context).pipe(
          map((model) => DynamicRouteActions.loadCurrentDynamicRouteModelSuccess({ model })),
          catchError((error) => {
            console.error(error);
            this.appInsightService.logException(error, 3);
            return of(DynamicRouteActions.loadCurrentDynamicRouteModelFailure({ error }));
          })
        )
      )
    );
  });

  modelStartLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.loadCurrentDynamicRouteModel),
      map(() => {
        return StatusActions.setLoading({ loading: true });
      })
    );
  });

  modelStopLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        DynamicRouteActions.loadCurrentDynamicRouteModelSuccess,
        DynamicRouteActions.loadCurrentDynamicRouteModelFailure
      ),
      map(() => {
        return StatusActions.setLoading({ loading: false });
      })
    );
  });

  postForm$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.postForm),
      mergeMap(({ model, routeId, onSuccess, showErrorSnack }) =>
        this.drService.postForm(routeId, model).pipe(
          map((result) => DynamicRouteActions.postFormSuccess({ routeId, result, model, onSuccess })),
          catchError((error) => {
            console.error(error);
            this.appInsightService.logException(error, 3);
            if (showErrorSnack) {
              this.showSnack(error?.error?.message);
            }
            return of(DynamicRouteActions.postFormFailure({ error: error?.error?.message ?? error?.message ?? error }));
          })
        )
      )
    );
  });

  postFormSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DynamicRouteActions.postFormSuccess),
        tap(({ result, onSuccess }) => {
          if (onSuccess) onSuccess();
          const effects = result.effects;
          if (effects) {
            if (effects.message) {
              this.showSnack(effects.message);
            }
            if (effects.redirectUrl) {
              this.router.navigateByUrl(effects.redirectUrl);
            }
          }
        })
      );
    },
    { dispatch: false }
  );

  postFormSuccessClear$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.postFormSuccess),
      filter(({ result }) => !!result.effects?.clearModel),
      map(() => DynamicRouteActions.clearCurrentDynamicRouteModel())
    );
  });

  postFormSuccessReloadModel$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.postFormSuccess),
      filter(({ result }) => !!result.effects?.reloadModel),
      map(({ routeId, result }) =>
        DynamicRouteActions.loadCurrentDynamicRouteModel({ routeId, context: result.context })
      )
    );
  });

  postFormSuccessReloadLists$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DynamicRouteActions.postFormSuccess),
        filter(({ result }) => (result.effects?.reloadLists || []).length > 0),
        tap(({ result }) => {
          (result.effects?.reloadLists || []).forEach((listName) => {
            this.listService.reloadList(listName);
          });
        })
      );
    },
    { dispatch: false }
  );

  postFormLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.postForm),
      map(() => {
        return StatusActions.setLoading({ loading: true });
      })
    );
  });

  postFormStopLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.postFormSuccess, DynamicRouteActions.postFormFailure),
      map(() => {
        return StatusActions.setLoading({ loading: false });
      })
    );
  });

  saveOptions$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DynamicRouteActions.saveDataListOptions),
        tap(({ options }) => this.listService.saveOptions(options))
      );
    },
    { dispatch: false }
  );

  loadOptions$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.loadDynamicRoutesSuccess),
      map(() => {
        const savedOptions = this.listService.loadOptions();
        return DynamicRouteActions.setDataListOptions({ savedOptions });
      })
    );
  });

  saveCardOptions$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DynamicRouteActions.saveCardOptions),
        tap((payload) => this.cardService.saveOptions(payload.options))
      );
    },
    { dispatch: false }
  );

  mergeModels$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.mergeDynamicRouteModel),
      switchMap(({ model, routeId, routeParams, autoFillModelSkipProps, autoFillModelOverrides }) =>
        this.routeService.loadModel(routeId, routeParams).pipe(
          map((newModel) => {
            const mergedModel = utils.autopopulateModel(model, newModel, {
              autoFillModelSkipProps: autoFillModelSkipProps || [],
              autoFillModelOverrides: autoFillModelOverrides || [],
            });
            return DynamicRouteActions.loadCurrentDynamicRouteModelSuccess({ model: mergedModel });
          })
        )
      )
    );
  });

  constructor(
    private actions$: Actions,
    private drService: DynamicRoutesDataService,
    private listService: ListService,
    private router: Router,
    private snackbar: MatSnackBar,
    private store: Store<fromRoot.State>,
    private routeService: DynamicRoutesDataService,
    private cardService: OptionsService,
    private appInsightService: AppInsightsService
  ) {}

  showSnack(message: string) {
    this.snackbar.openFromComponent(SnackMessageComponent, {
      duration: 5000,
      data: { type: SnackMessageType.Info, message },
    });
  }
}
