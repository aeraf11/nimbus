import { Store } from '@ngrx/store';
import { IndexedDbService, MetaDataService, ThemeService } from '@nimbus/core/src/lib/core';
import { AppConfigurationActions, LookupActions, MetaDataActions } from '../actions';
import { ConfigurationService } from '@nimbus/core/src/lib/core';
import { Injectable } from '@angular/core';
import { map, switchMap, tap } from 'rxjs/operators';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { registerLicense as registerSyncfusionLicense } from '@syncfusion/ej2-base';
import * as fromRoot from '../../reducers';
import { MetaDataRefreshService } from '../services/meta-data-refresh.service';

const THEME_STATE_KEY = 'themeState';
@Injectable()
export class AppConfigurationEffects {
  timeZone$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppConfigurationActions.loadTimeZone),
      switchMap(() =>
        this.appConfigurationService.getTimeZones().pipe(
          map((timeZones) => {
            return AppConfigurationActions.loadTimeZoneSuccess({ timeZones });
          })
        )
      )
    );
  });

  theme$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AppConfigurationActions.setTheme),
        tap(({ themeId }) => {
          if (themeId && themeId !== '') {
            localStorage.setItem(THEME_STATE_KEY, themeId);
            this.themeService.setTheme(themeId);
          }
        })
      );
    },
    { dispatch: false }
  );

  themeLoad$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppConfigurationActions.loadTheme),
      concatLatestFrom(() => this.store.select(fromRoot.selectDefaultThemeId)),
      map(([, defaultThemeId]) => {
        const themeId = localStorage.getItem(THEME_STATE_KEY) || defaultThemeId || '';
        return AppConfigurationActions.setTheme({ themeId });
      })
    );
  });

  themeLoadLookups$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppConfigurationActions.loadConfigSuccess),
      map(({ config }) => {
        const lookups = config.themeConfig?.map((x) => ({
          displayValue: x.label,
          hierarchicalDisplayValue: x.label,
          lookupId: x.id,
          lookupKey: 'Themes',
        }));
        return LookupActions.loadLookupsSuccess({ lookups });
      })
    );
  });

  loadSyncfusionConfiguration$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AppConfigurationActions.loadConfigSuccess),
        map(({ config }) => {
          if (config.syncfusionLicenseKey) {
            registerSyncfusionLicense(config.syncfusionLicenseKey);
          }
        })
      );
    },
    { dispatch: false }
  );

  initialiseIndexedDb$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AppConfigurationActions.loadConfigSuccess),
        map(({ config }) => {
          if (config.indexedDbConfig) {
            this.indexedDbService.init(config.indexedDbConfig);
          }
        })
      );
    },
    { dispatch: false }
  );

  initialiseMetaDataCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppConfigurationActions.loadMetadataCacheKeys),
      switchMap(() =>
        this.metaDataService.loadMetaDataCacheKeys().pipe(
          map((keys) => {
            return MetaDataActions.loadMetadatakeysSuccess({ keys });
          })
        )
      )
    );
  });

  setMetaDataRefresh$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AppConfigurationActions.loadConfigSuccess),
        map(({ config }) => {
          if (config.metaDataCacheRefreshInterval) {
            this.metaDataRefreshService.initialize(config.metaDataCacheRefreshInterval);
          }
        })
      );
    },
    { dispatch: false }
  );

  checkFormStateSettings$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AppConfigurationActions.loadConfigSuccess),
        map(({ config }) => {
          if (!config.formStateSettings || config.formStateSettings?.keys?.length === 0) {
            console.warn(
              'Form state settings are not configured, these should be set for this environment in the app settings'
            );
          }
        })
      );
    },
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private appConfigurationService: ConfigurationService,
    private themeService: ThemeService,
    private store: Store,
    private indexedDbService: IndexedDbService,
    private metaDataService: MetaDataService,
    private metaDataRefreshService: MetaDataRefreshService
  ) {}
}
