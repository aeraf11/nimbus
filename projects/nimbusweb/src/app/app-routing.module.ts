import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  AdminComponent,
  BustCacheComponent,
  ConfigComponent,
  FormGalleryComponent,
  FormlySandboxComponent,
  LandingComponent,
  LookupListComponent,
  MappingSandboxComponent,
  ThemeComponent,
} from '@nimbus/core/src/lib/apto';
import { AuthGuard } from './auth/services';
import { NotFoundPageComponent, UnauthorisedComponent } from './core/containers';
import { Preloader } from './core/services';

const routes: Routes = [
  { path: '', redirectTo: '/landing', pathMatch: 'full' },
  {
    path: 'landing',
    component: LandingComponent,
    data: { title: 'Home', hideToolbars: true, id: 'Landing' },
  },
  {
    path: 'admin',
    component: AdminComponent,
    data: { title: 'Admin', id: 'Admin', hideToolbars: false },
    canActivate: [AuthGuard],
  },
  {
    path: 'form-gallery',
    component: FormGalleryComponent,
    data: { title: 'Form Gallery', id: 'FormGallery', hideToolbars: true, contentClass: 'content-full-width' },
    canActivate: [AuthGuard],
  },
  {
    path: 'theme',
    component: ThemeComponent,
    data: { title: 'Theme', id: 'Theme', hideToolbars: false },
    canActivate: [AuthGuard],
  },
  {
    path: 'lookuplist',
    component: LookupListComponent,
    data: { title: 'List Lookups', id: 'LookupList', hideToolbars: false },
    canActivate: [AuthGuard],
  },
  {
    path: 'formly-sandbox',
    component: FormlySandboxComponent,
    data: { title: 'Formly Sandbox', id: 'FormlySandbox', hideToolbars: false, contentClass: 'content-wide' },
    canActivate: [AuthGuard],
  },
  {
    path: 'mapping-sandbox',
    component: MappingSandboxComponent,
    data: { title: 'Mapping Sandbox', id: 'MappingSandbox', hideToolbars: false },
    canActivate: [AuthGuard],
  },
  {
    path: 'bustcache',
    component: BustCacheComponent,
    data: { title: 'Bust Cache', id: 'BustCache', hideToolbars: false },
    canActivate: [AuthGuard],
  },
  {
    path: 'config',
    component: ConfigComponent,
    data: { title: 'Site Configuration', id: 'Config' },
    canActivate: [AuthGuard],
  },
  {
    path: 'intel',
    loadChildren: () => import('./intel/intel.module').then((m) => m.IntelModule),
    data: { preload: true },
  },
  {
    path: 'unauthorised',
    component: UnauthorisedComponent,
    data: { title: 'Unauthorised', hideToolbars: true, contentClass: 'no-pad', id: 'Unauthorised' },
  },
  {
    path: '**',
    component: LandingComponent,
    data: { title: '' },
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      paramsInheritanceStrategy: 'always',
      preloadingStrategy: Preloader,
    }),
  ],
  providers: [Preloader],
  exports: [RouterModule],
})
export class AppRoutingModule {}
