import { selectForceAgencyLookupId, selectPermissions, selectUser } from '../auth/reducers';
import { Action, ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { InjectionToken } from '@angular/core';
import { environment } from '../../environments/environment';
import * as fromLayout from '../core/reducers/layout.reducer';
import * as fromAppConfig from '../core/reducers/app-configuration.reducer';
import * as fromLookups from '../core/reducers/lookups.reducer';
import * as fromStatus from '../core/reducers/status.reducer';
import * as fromForm from '../core/reducers/form.reducer';
import * as fromDynamicRoutes from '../core/reducers/dynamic-routes.reducer';
import * as fromMetaData from '../core/reducers/meta-data.reducer';
import * as fromTranslations from '../core/reducers/translation.reducer';

export interface State {
  [fromLayout.layoutFeatureKey]: fromLayout.State;
  [fromAppConfig.appConfigurationFeatureKey]: fromAppConfig.State;
  [fromLookups.lookupsFeatureKey]: fromLookups.State;
  [fromMetaData.metaDataFeatureKey]: fromMetaData.State;
  [fromStatus.statusFeatureKey]: fromStatus.State;
  [fromForm.formFeatureKey]: fromForm.State;
  [fromTranslations.translationFeatureKey]: fromTranslations.State;
  [fromDynamicRoutes.dynamicRoutesFeatureKey]: fromDynamicRoutes.State;
  router: fromRouter.RouterReducerState<any>;
}

export const ROOT_REDUCERS = new InjectionToken<ActionReducerMap<State, Action>>('Root reducers token', {
  factory: () => ({
    [fromLayout.layoutFeatureKey]: fromLayout.reducer,
    [fromAppConfig.appConfigurationFeatureKey]: fromAppConfig.reducer,
    [fromLookups.lookupsFeatureKey]: fromLookups.reducer,
    [fromMetaData.metaDataFeatureKey]: fromMetaData.reducer,
    [fromStatus.statusFeatureKey]: fromStatus.reducer,
    [fromForm.formFeatureKey]: fromForm.reducer,
    [fromTranslations.translationFeatureKey]: fromTranslations.reducer,
    [fromDynamicRoutes.dynamicRoutesFeatureKey]: fromDynamicRoutes.reducer,
    router: fromRouter.routerReducer,
  }),
});

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

/**
 * Layout Selectors
 */
export const selectLayoutState = createFeatureSelector<fromLayout.State>(fromLayout.layoutFeatureKey);
export const selectContentClass = createSelector(selectLayoutState, (state) => state.contentClass);
export const selectMenuState = createSelector(selectLayoutState, (state) => state.menuState);
export const selectMenuPinned = createSelector(selectLayoutState, (state) => state.menuPinned);
export const selectShowToolbars = createSelector(selectLayoutState, (state) => state.showToolbars);
export const selectDashboardLayout = createSelector(selectLayoutState, (state) => state.dashboardLayout);
export const selectBreakpoint = createSelector(selectLayoutState, (state) => state.breakpoint);

/**
 * Router Selectors
 */
export const selectRouter = createFeatureSelector<fromRouter.RouterReducerState>('router');
export const { selectRouteData, selectUrl, selectCurrentRoute } = fromRouter.getSelectors(selectRouter);
export const selectRouteId = createSelector(selectRouteData, (data) => data?.id);

/**
 * Form Selectors
 */
export const selectFormState = createFeatureSelector<fromForm.State>(fromForm.formFeatureKey);
export const selectFormEntities = createSelector(selectFormState, fromForm.selectFormEntities);
export const selectFormSubmissionResult = createSelector(selectFormState, (state) => state.submissionResult);
export const selectForm = (formId: string) => createSelector(selectFormEntities, (forms) => forms[formId]);
export const selectFormFields = (formId: string) =>
  createSelector(selectFormEntities, (forms) => forms[formId]?.fields);

/**
 * Dynamic Route Selectors
 */
export const selectDynamicRouteState = createFeatureSelector<fromDynamicRoutes.State>(
  fromDynamicRoutes.dynamicRoutesFeatureKey
);
export const selectDynamicRouteEntities = createSelector(
  selectDynamicRouteState,
  fromDynamicRoutes.selectDynamicRouteEntities
);
export const selectAllDynamicRoutes = createSelector(selectDynamicRouteState, fromDynamicRoutes.selectAllDynamicRoutes);
export const selectDynamicRoute = (routeId: string) =>
  createSelector(selectDynamicRouteEntities, (routes) => routes[routeId]);
export const selectCurrentDynamicRoute = createSelector(
  selectRouteId,
  selectDynamicRouteEntities,
  (routeId, routes) => routes[routeId]
);
export const selectCurrentDynamicForm = createSelector(selectCurrentDynamicRoute, selectFormEntities, (route, forms) =>
  forms && route ? forms[route?.formId] : undefined
);
export const selectDynamicRouteCurrentModel = createSelector(selectDynamicRouteState, (state) => state.currentModel);
export const selectDynamicRouteCurrentError = createSelector(selectDynamicRouteState, (state) => state.error);

export const selectDataListOptions = (savedOptionsId: string) =>
  createSelector(selectDynamicRouteState, (state) => state.savedOptions[savedOptionsId]);

export const selectRoutesWithPermission = (routes: any[]) =>
  createSelector(selectAllRoutes, selectPermissions, (state, permissions) =>
    fromDynamicRoutes.getRoutesWithPermissions(state, routes, permissions)
  );

/**
 * Config Selectors
 */
export const selectAppConfigState = createFeatureSelector<fromAppConfig.State>(
  fromAppConfig.appConfigurationFeatureKey
);
export const selectConfig = createSelector(selectAppConfigState, (state) => state.config);
export const selectTimeZones = createSelector(selectAppConfigState, (state) => state?.timeZones);
export const selectOidcConfig = createSelector(selectConfig, (config) => config?.oidc);
export const selectMenuConfig = createSelector(selectConfig, (config) => config?.menuConfig);
export const selectStaticRoutes = createSelector(selectConfig, (config) => config?.staticRoutes);
export const selectAllRoutes = createSelector(selectStaticRoutes, selectAllDynamicRoutes, (statics, dynamics) => [
  ...(statics || []),
  ...(dynamics || []),
]);
export const selectThemes = createSelector(selectConfig, (config) => config?.themeConfig);
export const selectSelectedThemeId = createSelector(selectAppConfigState, (config) => config?.selectedThemeId);
export const selectSelectedTheme = createSelector(selectThemes, selectSelectedThemeId, (themes, themeId) =>
  themes?.find((x) => x.id === themeId)
);
export const selectDefaultThemeId = createSelector(selectThemes, (themes) => (themes ? themes[0].id : undefined));
export const selectSelectedThemeIsDark = createSelector(selectSelectedTheme, (theme) => theme?.isDark);
export const selectEnableFormTranslations = createSelector(selectConfig, (config) => config?.enableFormTranslations);
export const selectHomeConfigs = createSelector(selectConfig, (config) => config?.homeConfig);
export const selectHomeItems = createSelector(
  selectHomeConfigs,
  selectAllRoutes,
  selectPermissions,
  fromAppConfig.getHomeItems
);
export const selectHeaderMenuItem = createSelector(
  selectMenuConfig,
  selectAllRoutes,
  selectPermissions,
  selectForceAgencyLookupId,
  fromAppConfig.getHeaderMenuItem
);
export const selectBodyMenuItems = createSelector(
  selectMenuConfig,
  selectAllRoutes,
  selectPermissions,
  selectForceAgencyLookupId,
  fromAppConfig.getBodyMenuItems
);
export const selectFileUploadConfig = createSelector(selectConfig, (config) => config?.fileUploadConfig);
export const selectSignalRConfig = createSelector(selectConfig, (config) => config?.signalRHubs);
export const selectDebugOptions = createSelector(selectConfig, (config) => config?.debugOptions);
export const selectServiceWorkerConfig = createSelector(selectConfig, (config) => config?.serviceWorkerConfig);
export const selectAppInsightsConfig = createSelector(selectConfig, (config) => config?.applicationInsightsConfig);
export const selectSiteId = createSelector(selectConfig, (config) => config?.siteId);
export const selectScheduledJobs = createSelector(selectConfig, (config) => config?.scheduledJobs ?? []);
export const selectScheduledJob = (id: string) =>
  createSelector(selectConfig, (config) => config?.scheduledJobs?.find((job) => job.id === id));
export const selectUpgradeConfigMessage = createSelector(selectAppConfigState, (config) => config?.upgradeMessage);
export const selectAllDefaultMetaData = createSelector(selectAppConfigState, (config) => config?.defaultMetaData);
export const selectDefaultMetaDataByEntityTypeId = (id: number) =>
  createSelector(selectAllDefaultMetaData, (metaData) => (metaData ? metaData[id] : []));
export const selectFormStateSettings = createSelector(selectConfig, (config) => config?.formStateSettings);

/**
 * Lookup Selectors
 */
export const selectLookupsState = createFeatureSelector<fromLookups.State>(fromLookups.lookupsFeatureKey);
export const selectAllLookupDetails = createSelector(selectLookupsState, fromLookups.selectAllLookupDetails);
export const selectLookupDetailEntities = createSelector(selectLookupsState, fromLookups.selectLookupDetailEntities);

export const selectLookupsHierarchyFiltered = (filters: string[]) =>
  createSelector(selectAllLookupDetails, (lookups: any) => fromLookups.getHierarchicalFiltered(lookups, filters));

export const selectLookupsFlatFiltered = (filters: string[]) =>
  createSelector(selectAllLookupDetails, (lookups: any) => fromLookups.getFlatFiltered(lookups, filters));

export const selectLookupsHierarchy = createSelector(selectAllLookupDetails, fromLookups.getHierarchy);
export const selectLookupsFlat = createSelector(selectAllLookupDetails, fromLookups.getFlat);

export const selectLookupsForList = (filters: string[]) =>
  createSelector(selectAllLookupDetails, (lookups: any) => fromLookups.getHierarchicalForList(lookups, filters));

/**
 *  Status Selectors
 */
export const selectStatusState = createFeatureSelector<fromStatus.State>(fromStatus.statusFeatureKey);
export const selectOnline = createSelector(selectStatusState, fromStatus.selectOnline);
export const selectLoading = createSelector(selectStatusState, fromStatus.selectLoading);

/**
 *  Profile Selectors
 */
export const selectProfileModel = createSelector(selectUser, selectSelectedThemeId, (user, themeId) =>
  fromAppConfig.getProfileModel(user, themeId ?? undefined)
);

/**
 * MetaDateKey Selectors
 */
export const selectMetaDataKeyState = createFeatureSelector<fromMetaData.State>(fromMetaData.metaDataFeatureKey);
export const selectMetaDataKeyDetails = createSelector(selectMetaDataKeyState, fromMetaData.selectAllMetaDataDetails);
export const selectMetaDateKeyEntities = createSelector(selectMetaDataKeyState, fromMetaData.selectMetaDataEntities);

export const selectMetaDataKeyColumns = (listColumns: string[], entityTypeId: string) =>
  createSelector(selectMetaDataKeyDetails, (metaDataKeys: any) =>
    fromMetaData.getMetaDataColumnMatches(metaDataKeys, listColumns, entityTypeId)
  );

/**
 * Translation Selectors
 */
export const selectTranslationState = createFeatureSelector<fromTranslations.State>(
  fromTranslations.translationFeatureKey
);

export const selectAllTranslations = createSelector(selectTranslationState, (state) => state.translations);
