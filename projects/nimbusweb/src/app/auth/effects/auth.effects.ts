import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ConfigurationService, MetaDataItem, MetaDataService, utils } from '@nimbus/core/src/lib/core';
import { of } from 'rxjs';
import { catchError, map, switchMap, take, tap } from 'rxjs/operators';
import { AppConfigurationActions, FormActions } from '../../core/actions';
import { AuthActions, UserActions } from '../actions';
import { UserService } from '../services';
import { InitializerService } from '../../core/services';

@Injectable()
export class AuthEffects {
  login$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.login),
      switchMap(({ redirect }) =>
        this.userService.getUserDetails().pipe(
          take(1),
          map((user) => AuthActions.loginSuccess({ user, redirect })),
          catchError((error) => of(UserActions.loadUserDetailsFailure({ error })))
        )
      )
    );
  });

  loginSuccessUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      map(({ user, redirect }) => UserActions.loadUserDetailsSuccess({ user, redirect }))
    );
  });

  loginSuccessForm$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      switchMap(() => {
        return of(FormActions.loadForms());
      })
    );
  });

  loginSuccessDefaultMetaData$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      switchMap(() => {
        return this.appConfig.getAppConfig().pipe(
          switchMap((config) => this.metaData.getDefaultMetaDataForEntities(config.defaultEntityMetaData ?? [])),
          map((defaultMetaDataJson) => {
            const defaultMetaData: { [key: number]: MetaDataItem[] } = {};
            for (const key in defaultMetaDataJson) {
              defaultMetaData[key] = utils.parseMetaJson(defaultMetaDataJson[key]);
            }
            return AppConfigurationActions.loadEntityDefaultMetaData({ defaultMetaData });
          })
        );
      })
    );
  });

  setMetaDataCacheKeys$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      switchMap(() => {
        return of(AppConfigurationActions.loadMetadataCacheKeys());
      })
    );
  });

  setPostAuthConfig$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      switchMap(({ redirect }) => {
        return this.initializer.getPostAuthConfig().pipe(map(() => AuthActions.loginSuccessLoadDetails({ redirect })));
      })
    );
  });

  loginSuccessRedirect$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AuthActions.loginSuccessLoadDetails),
        tap(({ redirect }) => this.router.navigateByUrl(redirect))
      );
    },
    { dispatch: false }
  );

  loginFailed$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.loginFailed),
      tap(({ redirect }) => (window.location.href = '/home/login?returnUrl=' + redirect))
    );
  });

  logout$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AuthActions.logout),
        tap(() => (window.location.href = '/home/logout'))
      );
    },
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private userService: UserService,
    private router: Router,
    private metaData: MetaDataService,
    private appConfig: ConfigurationService,
    private initializer: InitializerService
  ) {}
}
