import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { User } from '@nimbus/core/src/lib/core';

export const AuthActions = createActionGroup({
  source: 'Auth',
  events: {
    Login: props<{ redirect: string }>(),
    'Login Success': props<{ user: User; redirect: string }>(),
    'Login Failed': props<{ error: string; redirect: string }>(),
    'Login Success Load Details': props<{ redirect: string }>(),
    Logout: emptyProps,
  },
});
