import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { User } from '@nimbus/core/src/lib/core';

export const UserActions = createActionGroup({
  source: 'User',
  events: {
    'Load User Details Success': props<{ user: User; redirect: string }>(),
    'Load User Details Failure': props<{ error: any }>(),
    'Clear User Details': emptyProps,
  },
});
