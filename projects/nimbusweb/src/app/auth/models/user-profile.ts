import { User } from '@nimbus/core/src/lib/core';

export interface UserProfile extends User {
  themeId?: string;
}
