import { createReducer, on } from '@ngrx/store';
import { UserActions } from '../actions';
import { User } from '@nimbus/core/src/lib/core';

export const detailsFeatureKey = 'details';

export interface State {
  user: User | null;
}

export const initialState: State = {
  user: null,
};

export const reducer = createReducer(
  initialState,
  on(
    UserActions.loadUserDetailsSuccess,
    (state, { user }): State => ({
      ...state,
      user,
    })
  ),
  on(UserActions.clearUserDetails, (state): State => initialState)
);

export const getUser = (state: State) => state?.user;
