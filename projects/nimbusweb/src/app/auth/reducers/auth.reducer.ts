import { createReducer, on } from '@ngrx/store';
import { AuthActions } from '../actions';

export const statusFeatureKey = 'status';

export interface State {
  isLoggedIn: boolean;
  isLoggingIn: boolean;
  loginFailed: boolean;
}

export const initialState: State = {
  isLoggedIn: false,
  isLoggingIn: false,
  loginFailed: false,
};

export const reducer = createReducer(
  initialState,
  on(AuthActions.login, (state): State => {
    return {
      ...state,
      isLoggingIn: true,
    };
  }),
  on(AuthActions.loginSuccess, (state, { user, redirect }): State => {
    return {
      ...state,
      isLoggedIn: true,
      isLoggingIn: false,
    };
  }),
  on(AuthActions.logout, (state, {}): State => {
    return {
      ...state,
      isLoggedIn: false,
    };
  })
);
