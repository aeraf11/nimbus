import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { CookieService } from 'ngx-cookie-service';
import { of } from 'rxjs';
import * as fromAuth from '../reducers';

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor(private cookies: CookieService, private store: Store<fromAuth.State>) {}

  get isAuthenticated$() {
    return this.store.select(fromAuth.selectIsLoggedIn);
  }
}
