import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';
import * as fromAuth from '../../auth/reducers';
import * as fromRoot from '../../reducers';
import { Store } from '@ngrx/store';
import { PermissionLogic } from '@nimbus/core/src/lib/core';
import { AuthActions } from '../actions';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private store: Store<fromAuth.State & fromRoot.State>
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const userPermissions$ = this.store.select(fromAuth.selectPermissions);
    const forceAgencyLookupId$ = this.store.select(fromAuth.selectForceAgencyLookupId);
    const routes$ = this.store.select(fromRoot.selectAllRoutes);

    //This has to dig into the first child for routes in lazy loaded modules
    const routeId = route.data?.id || route.firstChild?.data?.id;

    return this.authService.isAuthenticated$.pipe(
      withLatestFrom(userPermissions$, routes$, forceAgencyLookupId$),
      map(([isAuthenticated, userPermissions, routes, forceAgencyLookupId]) => {
        const routeConfig = routes?.find((r) => r.routeId === routeId);
        if (isAuthenticated && !userPermissions) {
          this.router.navigate(['/landing'], { queryParams: { redirect: routeConfig?.route } });
          return false;
        }
        if (!isAuthenticated) {
          const redirect = routeConfig?.route ?? '/landing';
          this.store.dispatch(AuthActions.login({ redirect }));
          return false;
        }
        if (!userPermissions || !routeConfig) {
          this.router.navigate(['/unauthorised']);
          return false;
        }

        let result = false;
        switch (routeConfig.permissionLogic || PermissionLogic.All) {
          case PermissionLogic.All:
            result = !routeConfig.permissionKeys
              ? true
              : routeConfig.permissionKeys.every((key) => userPermissions.includes(key));
            break;

          case PermissionLogic.Any:
            result = !routeConfig.permissionKeys
              ? true
              : routeConfig.permissionKeys.some((key) => userPermissions.includes(key));
            break;

          default:
            result = false;
        }
        if (routeConfig.forceAgencyPermissions) {
          if (!routeConfig.forceAgencyPermissions.includes(forceAgencyLookupId || '')) {
            result = false;
          }
        }
        if (!result) {
          this.router.navigate(['/unauthorised']);
        }
        return result;
      })
    );
  }
}
