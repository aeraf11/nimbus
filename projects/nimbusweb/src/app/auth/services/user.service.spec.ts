import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { UserService } from './user-service';
import { HttpClient } from '@angular/common/http';
import { environment } from 'projects/nimbusweb/src/environments/environment';

describe('UserServiceService', () => {
  let service: UserService;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(UserService);
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    const dummyUser = {
      userProfileId: '',
      fullName: '',
      groupName: '',
      email: '',
      contactNumber: '',
      branch: '',
      permissions: [],
      dateFormat: '',
      timeFormat: '',
    };

    service.getUserDetails().subscribe((user) => {
      expect(user).toBeTruthy();
    });

    const req = httpTestingController.expectOne(`${environment.apiUrl}/account`);
    expect(req.request.method).toBe('GET');
    req.flush(dummyUser);
  });
});
