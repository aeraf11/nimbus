import { AuthEffects } from './effects/auth.effects';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { Store, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UserService } from './services/user-service';
import { HttpClient } from '@angular/common/http';
import * as fromAuth from '../auth/reducers/';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    StoreModule.forFeature({
      name: fromAuth.authFeatureKey,
      reducer: fromAuth.reducers,
    }),
    EffectsModule.forFeature([AuthEffects]),
  ],
  providers: [UserService, HttpClient, Store],
})
export class AuthConfigModule {}
