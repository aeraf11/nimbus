# Theming

This is an overview guide for theming the Phaneros app basd around the Angular Material colour and typographic theming framework.

## Customising Components

For a detailed guide refer to https://material.angular.io/guide/customizing-component-styles.

To see our component themes within a page you can use the new [/theme](https://phaneros-uk-dev.azurewebsites.net/theme) gallery.

In brief, with regard to changing material component styles, fill your boots with the safe elements below, but you should _NOT_ be changing the dangerous elements listed.
By changing styles from the dangerous list you are creating unstability in the entire suite of components. Dangerous changes may work some of the time and not other times as many classes are added and removed dynamically, also classes you change for a specific component may well be used in other components which will then break, or break a feature not currently being used/seen, e.g. RTL, captions, placeholders.

Where ever possible avoid dangerous changes and try to use the the typograhic and colour facilities of the framework, this will ensure maintence and adding of themes goes as smooth as possible, with as few bugs as possible.

### Safe

- Host element manipulation using properties such as margin, position, top, left, transform, and z-index.

### Dangerous

- Internal changes including padding, height, width, or overflow.

# Typography

The typography refers to the appearance of the type in the app, specifically font-family, font-weight, font-size, line-height & leter-spacing.
The material specification defines a default set of typographic levels as below:

| Name         | Description                                                                 |
| ------------ | --------------------------------------------------------------------------- |
| display-4    | 112px, one-off header, usually at the top of the page (e.g. a hero header). |
| display-3    | 56px, one-off header, usually at the top of the page (e.g. a hero header).  |
| display-2    | 45px, one-off header, usually at the top of the page (e.g. a hero header).  |
| display-1    | 34px, one-off header, usually at the top of the page (e.g. a hero header).  |
| headline     | Section heading corresponding to the \<h1\> tag.                            |
| title        | Section heading corresponding to the \<h2\> tag.                            |
| subheading-2 | Section heading corresponding to the \<h3\> tag.                            |
| subheading-1 | Section heading corresponding to the \<h4\> tag.                            |
| body-1       | Base body text.                                                             |
| body-2       | Bolder body text.                                                           |
| caption      | Smaller body and hint text.                                                 |
| button       | Buttons and anchors.                                                        |
| input        | Form input fields.                                                          |

https://material.angular.io/guide/typography

To allow for easier use of theme based variaitions in typography we should utilise the below classes where possible. Where this is not possible
we can use the component's theme based scss file which will be fed the typograpic config for the theme. The config can be used to extract
typographic settings for the various levels using the various mixins/functions detailed below in the useful snippets.

| CSS class                       | Level name   | Native elements |
| ------------------------------- | ------------ | --------------- |
| .mat-display-4                  | display-4    | None            |
| .mat-display-3                  | display-3    | None            |
| .mat-display-2                  | display-2    | None            |
| .mat-display-1                  | display-1    | None            |
| .mat-h1 or .mat-headline        | headline     | <h1>            |
| .mat-h2 or .mat-title           | title        | <h2>            |
| .mat-h3 or .mat-subheading-2    | subheading-2 | <h3>            |
| .mat-h4 or .mat-subheading-1    | subheading-1 | <h4>            |
| .mat-h5                         | None         | <h5>            |
| .mat-h6                         | None         | <h6>            |
| .mat-body or .mat-body-1        | body-1       | Body text       |
| .mat-body-strong or .mat-body-2 | body-2       | None            |
| .mat-small or .mat-caption      | caption      | None            |

## Useful Snippets

**Get Default Typography**

> $my-typography: mat.define-typography-config();

**Include a Typographic Level in Your Class**

> .some-class-name {
> @include mat.typography-level($my-typography, 'body-1');
> }

**Use font-size From a Typographic Level in Your Class**

> .my-font-size {
> font-size: mat.font-size($config, 'body-1');
> }

**Use font-family From a Typographic Level in Your Class**

> .my-font-family {
> font-family: mat.font-family($config, 'body-1');
> }

**Use font-weight from a Typographic Level in Your Class**

> .my-font-weight {
> font-weight: mat.font-weight($config, 'body-1');
> }

**Use line-height from a Typographic Level in Your Class**

> .my-line-height {
> line-height: mat.line-height($config, 'body-1');
> }

**Use letter-spacing from a Typographic Level in Your Class**

> .my-letter-spacing {
> letter-spacing: mat.letter-spacing($config, 'body-1');
> }

# Colours

The theme contains a primary, accent and warn palletes. You can extract colours from the theme pallets for colouring components.
You can also extract if the theme is dark, this can be useful for coloring elements appropiately in dark modes, Material uses this
techinque. If you component's colouring looks off you can find the equivalent element from an existing material component and look
at the scss file generating that in the node_modules folder, from there you can see how you should derive that colour from the config.
See example below:

> $outline-color: mat.get-color-from-palette($foreground, divider, if($is-dark-theme, 0.3, 0.12));

## Useful Snippets

**Get Pallette**

> $primary-palette: map.get($config, 'primary');

> $accent-palette:  map.get($config, 'accent');

> $warn-palette:    map.get($config, 'warn');

**Get IsDark Flag**

> $is-dark-theme:   map.get($config, 'is-dark');
