export const environment = {
  production: true,
  apiUrl: '/api/v2',
  initUrl: '/api/v2/aptoinit',
};
