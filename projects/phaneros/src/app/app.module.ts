import { APP_INITIALIZER, ErrorHandler, Injector, NgModule } from '@angular/core';
import { AppComponent } from './core/containers/app/app.component';
import { AppInjector } from './core/services';
import {
  AppInsightsService,
  ConfigurationService,
  ErrorHandlerService,
  TranslationService,
} from '@nimbus/core/src/lib/core';
import { AppRoutingModule } from './app-routing.module';
import { AuthConfigModule } from './auth/auth-config.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from './core/core.module';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';
import { InitializerService } from './core/services/initializer.service';
import { ServiceWorkerModule } from '@angular/service-worker';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { environment } from '../environments/environment';
import { metaReducers, ROOT_REDUCERS } from './reducers';

import {
  AppConfigurationEffects,
  DynamicRouteEffects,
  FormEffects,
  LayoutEffects,
  LookupEffects,
  RouterEffects,
  StatusEffects,
} from './core/effects';

export function AppInitializerServiceFactory(service: InitializerService) {
  return () => service.initialize();
}

@NgModule({
  declarations: [],
  exports: [],
  imports: [
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,
    CoreModule,
    StoreModule.forRoot(ROOT_REDUCERS, {
      metaReducers,
      runtimeChecks: {
        strictStateSerializability: true,
        strictActionSerializability: false,
        strictActionWithinNgZone: true,
        strictActionTypeUniqueness: true,
      },
    }),
    StoreRouterConnectingModule.forRoot(),
    !environment.production
      ? StoreDevtoolsModule.instrument({
          name: 'Phaneros',
        })
      : [],
    EffectsModule.forRoot([
      RouterEffects,
      AppConfigurationEffects,
      LookupEffects,
      StatusEffects,
      FormEffects,
      LayoutEffects,
      DynamicRouteEffects,
    ]),
    AuthConfigModule,
    BrowserAnimationsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000',
    }),
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: AppInitializerServiceFactory,
      deps: [InitializerService, ConfigurationService, AppInsightsService, TranslationService],
      multi: true,
    },
    {
      provide: ErrorHandler,
      useClass: ErrorHandlerService,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(injector: Injector) {
    AppInjector.setInjector(injector);
  }
}
