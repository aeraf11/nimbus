import { AbstractControl, ValidationErrors } from '@angular/forms';
import { ConfigOption, FormlyFieldConfig } from '@ngx-formly/core';
import {
  ActionBarTypeComponent,
  BinarySizeTypeComponent,
  ButtonDialogTypeComponent,
  ButtonListTypeComponent,
  CardListViewTypeComponent,
  CardWrapperComponent,
  CaseSummaryTypeComponent,
  ChartBarTypeComponent,
  ChartLineTypeComponent,
  ChartPieTypeComponent,
  CheckboxTypeComponent,
  ChipTypeComponent,
  CircularGaugeTypeComponent,
  ContextTypeComponent,
  CrimeSceneTypeComponent,
  DataCardsTypeComponent,
  DataDownloadablesTypeComponent,
  DataTableTypeComponent,
  DateListViewTypeComponent,
  DateRangeTypeComponent,
  DateTimeViewComponent,
  DateTypeComponent,
  DisplayGroupTypeComponent,
  DividerTypeComponent,
  DocEditorTypeComponent,
  EntityAddTypeComponent,
  EntityMediaTypeComponent,
  EntitySummaryTypeComponent,
  FieldsViewTypeComponent,
  FileManagerTypeComponent,
  FlexWrapperComponent,
  FormActionsTypeComponent,
  FormFieldWrapperComponent,
  FormGalleryGroupWrapperComponent,
  FormGalleryWrapperComponent,
  GridWrapperComponent,
  IconTextInlineTypeComponent,
  IdentificationTypeComponent,
  ImageCheckTypeComponent,
  ImageChoiceTypeComponent,
  ImageViewTypeComponent,
  InlineCalendarTypeComponent,
  InputTypeComponent,
  JobTimeTypeComponent,
  JumboLinkTypeComponent,
  LatLongWrapperComponent,
  LayoutChoiceTypeComponent,
  ListFilterWrapperComponent,
  ListSummaryViewTypeComponent,
  LookupTypeComponent,
  MediaUploadTypeComponent,
  MessageTypeComponent,
  MessagesTypeComponent,
  MetaDataListTypeComponent,
  MultiCheckboxTypeComponent,
  MultiChoiceTypeComponent,
  NavButtonTypeComponent,
  PanelWrapperTypeComponent,
  PersonListViewTypeComponent,
  PersonViewTypeComponent,
  ProgressTypeComponent,
  RadioTypeComponent,
  RelatesToTypeComponent,
  RepeatTypeComponent,
  ReportButtonTypeComponent,
  RichFieldsViewTypeComponent,
  RichTextTypeComponent,
  RichTextViewerTypeComponent,
  RouteListTypeComponent,
  ScrollWrapperComponent,
  SearchResultCaseViewTypeComponent,
  SectionEditWrapperComponent,
  SelectTypeComponent,
  SignatureTypeComponent,
  SlideToggleTypeComponent,
  StepperTypeComponent,
  TableTypeComponent,
  TaskSummaryTypeComponent,
  TextAreaTypeComponent,
  TextBarcodeTypeComponent,
  TextPanelTypeComponent,
  TextViewTypeComponent,
  TimeTypeComponent,
  TypeAheadTypeComponent,
  UnidentifiedSubjectDetailTypeComponent,
  UnidentifiedSubjectTypeComponent,
  UploaderOfflineTypeComponent,
  UploaderTypeComponent,
  UuidTypeComponent,
  VideoTypeComponent,
  EntityMediaRequestWrapperComponent,
} from '@nimbus/core/src/lib/apto';
import {
  CardListWrapperComponent,
  FormsHelperService,
  QuickSearchWrapperComponent,
  utils,
} from '@nimbus/core/src/lib/core';
import { AppInjector } from './../services/app-injector';

export function PatternValidator(control: AbstractControl): ValidationErrors | null {
  const regexPattern = control?.errors?.pattern?.requiredPattern;
  return control.value !== regexPattern ? null : { pattern: true };
}

export function YesValidator(control: AbstractControl): ValidationErrors | null {
  return control.value === 'yes' ? null : { yes: true };
}

export function NoValidator(control: AbstractControl): ValidationErrors | null {
  return control.value === 'no' ? null : { no: true };
}

export function ArrayRequiredValidator(control: AbstractControl): ValidationErrors | null {
  return control.value && control.value.length > 0 ? null : { arrayRequired: true };
}

export function ObjectRequiredValidator(control: any): ValidationErrors | null {
  const props = { ...control?._fields[0]?.props, ...control?._fields[0]?.parent?.props };

  if (props.hidden) {
    return null;
  }

  return utils.isObject(control.value) &&
    Object.values(control.value).filter((value) => value !== null && value !== undefined).length ===
      Object.keys(control.value).length
    ? null
    : { objectRequired: true };
}

export function ExpressionValidator(
  control: AbstractControl,
  field: FormlyFieldConfig,
  options: unknown
): ValidationErrors | null {
  const expression = utils.evalStringExpression<boolean>(field.props?.['validationExpression'], [
    'control',
    'field',
    'options',
  ]);

  if (expression) {
    const valid = expression(control, field, options);

    if (valid) {
      return null;
    }
    return { expression: true };
  }
  return null;
}

//#region Validation Messages
export function minItemsValidationMessage(err: any, field: FormlyFieldConfig) {
  return `should NOT have fewer than ${field.props?.['minItems']} items`;
}

export function maxItemsValidationMessage(err: any, field: FormlyFieldConfig) {
  return `should NOT have more than ${field.props?.['maxItems']} items`;
}

export function minLengthValidationMessage(err: any, field: FormlyFieldConfig) {
  return `should NOT be shorter than ${field.props?.['minLength']} characters`;
}

export function maxLengthValidationMessage(err: any, field: FormlyFieldConfig) {
  return `should NOT be longer than ${field.props?.['maxLength']} characters`;
}

export function minValidationMessage(err: any, field: FormlyFieldConfig) {
  return `should be >= ${field.props?.['min']}`;
}

export function maxValidationMessage(err: any, field: FormlyFieldConfig) {
  return `should be <= ${field.props?.['max']}`;
}

export function multipleOfValidationMessage(err: any, field: FormlyFieldConfig) {
  return `should be multiple of ${field.props?.['step']}`;
}

export function exclusiveMinimumValidationMessage(err: any, field: FormlyFieldConfig) {
  return `should be > ${field.props?.['step']}`;
}

export function exclusiveMaximumValidationMessage(err: any, field: FormlyFieldConfig) {
  return `should be < ${field.props?.['step']}`;
}

export function constValidationMessage(err: any, field: FormlyFieldConfig) {
  return `should be equal to constant '${field.props?.['const']}'`;
}

export function yesValidationMessage(err: any, field: FormlyFieldConfig) {
  return field.props?.['yesValidationMessage'] ? field.props?.['yesValidationMessage'] : 'You must select Yes';
}

export function noValidationMessage(err: any, field: FormlyFieldConfig) {
  return field.props?.['noValidationMessage'] ? field.props?.['noValidationMessage'] : 'You must select No';
}

export function validationExpressionMessage(err: any, field: FormlyFieldConfig) {
  const helper = AppInjector.getInjector().get(FormsHelperService);
  return helper.parseTemplate(field.props?.['validationExpressionMessage'], field, '');
}

export function matDatepickerParseValidationMessage(err: any, field: FormlyFieldConfig) {
  let msg = field.props?.['showTime'] ? 'Please enter a valid date time' : 'Please enter a valid date';
  if (err.format) msg = msg + ` (${err.format})`;
  return field.props?.['dateParseValidationMessage'] ? field.props?.['dateParseValidationMessage'] : msg;
}

export function patternValidationMessage(err: any, field: FormlyFieldConfig) {
  return field.props?.['patternValidationMessage']
    ? field.props?.['patternValidationMessage']
    : 'Please enter a valid format';
}
//#endregion

export const FORMLY_CONFIG: ConfigOption = {
  extras: { resetFieldOnHide: true },
  validators: [
    { name: 'arrayRequired', validation: ArrayRequiredValidator },
    { name: 'objectRequired', validation: ObjectRequiredValidator },
    { name: 'pattern', validation: PatternValidator },
    { name: 'yes', validation: YesValidator },
    { name: 'no', validation: NoValidator },
    { name: 'expression', validation: ExpressionValidator },
  ],
  validationMessages: [
    { name: 'required', message: 'This field is required' },
    { name: 'arrayRequired', message: 'This field is required' },
    { name: 'objectRequired', message: 'This field is required' },
    { name: 'null', message: 'should be null' },
    { name: 'minLength', message: minLengthValidationMessage },
    { name: 'maxLength', message: maxLengthValidationMessage },
    { name: 'min', message: minValidationMessage },
    { name: 'max', message: maxValidationMessage },
    { name: 'multipleOf', message: multipleOfValidationMessage },
    {
      name: 'exclusiveMinimum',
      message: exclusiveMinimumValidationMessage,
    },
    {
      name: 'exclusiveMaximum',
      message: exclusiveMaximumValidationMessage,
    },
    { name: 'minItems', message: minItemsValidationMessage },
    { name: 'maxItems', message: maxItemsValidationMessage },
    { name: 'uniqueItems', message: 'should NOT have duplicate items' },
    { name: 'const', message: constValidationMessage },
    { name: 'dateFormat', message: 'This field should be a valid date' },
    { name: 'pattern', message: patternValidationMessage },
    { name: 'yes', message: yesValidationMessage },
    { name: 'no', message: noValidationMessage },
    { name: 'expression', message: validationExpressionMessage },
    { name: 'matDatepickerParse', message: matDatepickerParseValidationMessage },
  ],
  types: [
    { name: 'actionbar', component: ActionBarTypeComponent },
    { name: 'filemanager', component: FileManagerTypeComponent },
    { name: 'fieldview', component: FieldsViewTypeComponent },
    { name: 'boolean', extends: 'checkbox' },
    { name: 'binarysize', component: BinarySizeTypeComponent, wrappers: ['formfield'] },
    { name: 'buttonlist', component: ButtonListTypeComponent },
    { name: 'buttondialog', component: ButtonDialogTypeComponent },
    { name: 'cardlistview', component: CardListViewTypeComponent },
    { name: 'casesummary', component: CaseSummaryTypeComponent },
    { name: 'checkbox', component: CheckboxTypeComponent, wrappers: ['formfield'] },
    { name: 'chartbar', component: ChartBarTypeComponent },
    {
      name: 'chartdonut',
      extends: 'chartpie',
      defaultOptions: {
        props: {
          innerRadius: '70%',
          centerTitles: true,
        },
      },
    },
    { name: 'chartpie', component: ChartPieTypeComponent },
    { name: 'chartline', component: ChartLineTypeComponent },
    { name: 'chip', component: ChipTypeComponent },
    { name: 'circulargauge', component: CircularGaugeTypeComponent },
    { name: 'context', component: ContextTypeComponent },
    { name: 'crimescene', component: CrimeSceneTypeComponent },
    { name: 'datatable', component: DataTableTypeComponent },
    { name: 'datacards', component: DataCardsTypeComponent },
    { name: 'datadownloadables', component: DataDownloadablesTypeComponent },
    { name: 'date', component: DateTypeComponent, wrappers: ['formfield'] },
    { name: 'daterange', component: DateRangeTypeComponent, wrappers: ['formfield'] },
    { name: 'datetimeview', component: DateTimeViewComponent, wrappers: ['formfield'] },
    {
      name: 'datetime',
      extends: 'date',
      defaultOptions: {
        props: {
          showTime: true,
          showTimeZone: true,
        },
      },
    },
    {
      name: 'datelistview',
      component: DateListViewTypeComponent,
    },
    { name: 'displaygroup', component: DisplayGroupTypeComponent, wrappers: ['formfield'] },
    { name: 'divider', component: DividerTypeComponent },
    { name: 'doceditor', component: DocEditorTypeComponent },
    {
      name: 'docviewer',
      extends: 'doceditor',
      defaultOptions: {
        props: {
          readOnly: true,
          enableToolbar: false,
          enableSave: false,
        },
      },
    },
    { name: 'entitymedia', component: EntityMediaTypeComponent },
    { name: 'entitysummary', component: EntitySummaryTypeComponent },
    { name: 'enum', extends: 'select' },
    {
      name: 'examinationdatatable',
      extends: 'datatable',
      defaultOptions: {
        props: {
          listName: 'Examination',
          selectMode: 'none',
          sortColumn: 'reference',
          idColumn: 'ExaminationId',
          columnDefinitions: [
            {
              id: 'reference',
              value: 'Reference',
              isSortable: true,
              filterParams: {
                formlyType: 'input',
                props: {
                  label: 'Reference',
                },
              },
            },
            {
              id: 'requestDetails',
              value: 'Details',
              isSortable: true,
              filterParams: {
                formlyType: 'input',
                props: {
                  label: 'Details',
                },
              },
            },
            {
              id: 'examinationType',
              value: 'Type',
              dataType: 'status',
              isSortable: true,
              filterParams: {
                formlyType: 'multichoice',
                props: {
                  lookupKey: 'ExaminationType',
                  leafSelection: false,
                },
              },
              computedValue: {
                value: '${examinationType}',
                cssClass: '${examinationTypeCssClass}',
              },
            },
            {
              id: 'status',
              value: 'Status',
              dataType: 'status',
              isSortable: true,
              filterParams: {
                formlyType: 'multichoice',
                props: {
                  lookupKey: 'ExaminationStatus',
                },
              },
              computedValue: {
                value: '${status}',
                cssClass: '${statusCssClass}',
              },
            },
            {
              id: 'exhibitReference',
              value: 'Exhibit',
              isSortable: true,
              filterParams: {
                formlyType: 'multichoice',
                searchValueColumn: 'displayValue',
                props: {
                  listName: 'EntityLink',
                  valueProp: 'displayValue',
                  labelProp: 'displayValue',
                  sortColumn: 'displayValue',
                  selectColumns: ['displayValue', 'displayName', 'entityTypeId', 'isHierarchyTopLevel'],
                  templatedProperties: {
                    filterColumns: [
                      {
                        column: '',
                        stringFilter: 'bc92ee94-0190-49fe-959b-5f45d8cf30e8',
                        entityTypeId: 2,
                        constantFilter: true,
                      },
                      {
                        column: 'displayName',
                        stringFilter: 'Exhibit',
                        constantFilter: true,
                      },
                      {
                        column: 'isHierarchyTopLevel',
                        stringFilter: '0',
                        constantFilter: true,
                      },
                    ],
                  },
                },
              },
            },
          ],
        },
      },
    },
    { name: 'filemanager', component: FileManagerTypeComponent },
    { name: 'formactions', component: FormActionsTypeComponent },
    { name: 'icontextinline', component: IconTextInlineTypeComponent },
    { name: 'input', component: InputTypeComponent, wrappers: ['formfield'] },
    {
      name: 'integer',
      extends: 'input',
      defaultOptions: {
        props: {
          type: 'number',
        },
      },
    },
    { name: 'imagechoice', component: ImageChoiceTypeComponent, wrappers: ['formfield'] },
    { name: 'imageview', component: ImageViewTypeComponent, wrappers: ['formfield'] },
    { name: 'inlinecalendar', component: InlineCalendarTypeComponent },
    { name: 'jobtime', component: JobTimeTypeComponent, wrappers: ['formfield'] },
    { name: 'jumbolink', component: JumboLinkTypeComponent },
    { name: 'layoutchoice', component: LayoutChoiceTypeComponent },
    { name: 'listsummaryview', component: ListSummaryViewTypeComponent, wrappers: ['card'] },
    { name: 'lookup', component: LookupTypeComponent, wrappers: ['formfield'] },
    { name: 'mediaupload', component: MediaUploadTypeComponent, wrappers: ['formfield'] },
    { name: 'message', component: MessageTypeComponent },
    { name: 'messages', component: MessagesTypeComponent },
    { name: 'multicheckbox', component: MultiCheckboxTypeComponent, wrappers: ['formfield'] },
    { name: 'multichoice', component: MultiChoiceTypeComponent, wrappers: ['formfield'] },
    { name: 'navbutton', component: NavButtonTypeComponent },
    {
      name: 'number',
      extends: 'input',
      defaultOptions: {
        props: {
          type: 'number',
        },
      },
    },
    {
      name: 'entityadd',
      component: EntityAddTypeComponent,
    },
    {
      name: 'entitymediagrid',
      extends: 'datatable',
      defaultOptions: {
        props: {
          listName: 'EntityMedia',
          selectMode: 'single',
          idColumn: 'entityMediaId',
          sortColumn: 'creationDate',
          sortDirection: 'desc',
          pageSize: 10,
          columnDefinitions: [
            {
              id: 'thumbnailBlobId',
              value: 'Thumb',
              dataType: 'blobView',
            },
            {
              id: 'fileName',
              value: 'File',
              filterParams: {
                formlyType: 'input',
                props: {
                  label: 'File Name',
                },
              },
            },
            {
              id: 'description',
              value: 'Description',
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'creationDate',
              value: 'Created Date',
              dataType: 'dateTime',
              filterParams: {
                formlyType: 'daterange',
              },
            },
            {
              id: 'createdByFullName',
              value: 'Created By',
              filterParams: {
                formlyType: 'select2',
                searchValueColumn: 'fullName',
                props: {
                  listName: 'Persons',
                  valueProp: 'personId',
                  labelProp: 'fullName',
                  sortColumn: 'fullName',
                  selectColumns: ['personId', 'fullName', 'personTypeDisplay'],
                },
              },
            },
            {
              id: 'entityMediaEntitiesJson',
              value: 'EntityMediaLinks',
              filterParams: {
                formlyType: 'multichoice',
                searchValueColumn: 'displayValue',
                props: {
                  listName: 'EntityLink',
                  valueProp: 'displayValue',
                  labelProp: 'displayValue',
                  sortColumn: 'displayValue',
                  prefixColumn: 'displayName',
                  selectColumns: ['displayValue', 'displayName', 'isHierarchyTopLevel'],
                  templatedProperties: {
                    filterColumns: [
                      {
                        column: '',
                        stringFilter: '${context.operationId}',
                        entityTypeId: 1,
                        constantFilter: true,
                      },
                      {
                        column: 'displayName',
                        stringFilter: '!"Media"!"Note"',
                        constantFilter: true,
                      },
                    ],
                  },
                },
              },
            },
            {
              id: 'entityTypeId',
              isHidden: true,
            },
            {
              id: 'entityTypeIcon',
              isHidden: true,
            },
          ],
          showPaging: true,
          itemTemplate: {
            type: 'entitymedia',
          },
          itemsUi: {
            columnCounts: [1, 2, 4],
            gap: '1rem',
          },
          detailDialogFormId: 'Media.Detail',
          detailDialogTriggerIds: ['thumbnailBlobId'],
          detailDialogTitleTemplate: '${fileName}',
          detailDialogMaxWidth: '730px',
          detailDialogMinWidth: '20vw',
          detailDialogWidth: '90vw',
          previewDialogFormId: 'Media.Preview',
          previewDialogTitle: 'Media Request Review',
          previewDialogMaxWidth: '730px',
          previewDialogWidth: '100%',
          requestDialogFormId: 'Media.Request',
          requestDialogTitle: 'Media Request Form',
          requestDialogMaxWidth: '570px',
          requestDialogWidth: '100%',
          enableSelectAll: false,
        },
      },
    },
    {
      name: 'entitymedialist',
      extends: 'datatable',
      defaultOptions: {
        props: {
          listName: 'EntityMedia',
          selectMode: 'multi',
          idColumn: 'entityMediaId',
          sortColumn: 'creationDate',
          sortDirection: 'desc',
          filterTitle: 'Filter Media',
          pageSize: 10,
          columnDefinitions: [
            {
              id: 'thumbnailBlobId',
              value: 'Thumb',
              dataType: 'blobView',
            },
            {
              id: 'fileName',
              value: 'File',
              isSortable: true,
              filterParams: {
                formlyType: 'input',
                props: {
                  label: 'File Name',
                },
              },
            },
            {
              id: 'description',
              value: 'Description',
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'creationDate',
              value: 'Created Date',
              dataType: 'date',
              isSortable: true,
              filterParams: {
                formlyType: 'daterange',
              },
            },
            {
              id: 'createdByFullName',
              value: 'Created By',
              filterParams: {
                formlyType: 'select2',
                searchValueColumn: 'fullName',
                props: {
                  listName: 'Persons',
                  valueProp: 'personId',
                  labelProp: 'fullName',
                  sortColumn: 'fullName',
                  selectColumns: ['personId', 'fullName', 'personTypeDisplay'],
                },
              },
            },
            {
              id: 'entityMediaEntitiesJson',
              value: 'Linked',
              dataType: 'iconText',
              filterParams: {
                formlyType: 'multichoice',
                searchValueColumn: 'displayValue',
                filterAlias: 'EntityMediaByDisplayName',
                props: {
                  listName: 'EntityLink',
                  valueProp: 'displayValue',
                  labelProp: 'displayValue',
                  sortColumn: 'displayValue',
                  prefixColumn: 'displayName',
                  parentIdProp: 'entityTypeId',
                  selectColumns: ['displayValue', 'displayName', 'entityTypeId', 'isHierarchyTopLevel'],
                  templatedProperties: {
                    filterColumns: [
                      {
                        column: '',
                        stringFilter: '${context.operationId}',
                        entityTypeId: 1,
                        constantFilter: true,
                      },
                      {
                        column: 'displayName',
                        stringFilter: '!"Media"!"Note"',
                        constantFilter: true,
                      },
                    ],
                  },
                },
              },
              computedValue: {
                text: '${DisplayValue}',
                icon: '${EntityTypeIcon}',
                enableDefault: true,
                itemProp: 'entityMediaEntitiesJson',
              },
            },
            {
              id: 'entityTypeId',
              isHidden: true,
            },
            {
              id: 'entityTypeIcon',
              isHidden: true,
            },
          ],
          showPaging: true,
          showQuickSearch: false,
          detailDialogFormId: 'Media.Detail',
          detailDialogTriggerIds: ['thumbnailBlobId'],
          detailDialogTitleTemplate: '${fileName}',
          detailDialogMaxWidth: '730px',
          detailDialogMinWidth: '20vw',
          detailDialogWidth: '90vw',
          previewDialogFormId: 'Media.Preview',
          previewDialogTitle: 'Media Request Review',
          previewDialogMaxWidth: '730px',
          previewDialogWidth: '100%',
          requestDialogFormId: 'Media.Request',
          requestDialogTitle: 'Media Request Form',
          requestDialogMaxWidth: '570px',
          requestDialogWidth: '100%',
          enableSelectAll: true,
        },
      },
    },
    { name: 'identification', component: IdentificationTypeComponent },
    { name: 'identificationrow', extends: 'identification', defaultOptions: { props: { viewType: 'row' } } },
    {
      name: 'identificationlist',
      extends: 'datacards',
      defaultOptions: {
        props: {
          listName: 'MyIdents',
          idColumn: 'identId',
          sortColumn: 'creationDate',
          sortDirection: 'desc',
          pageSize: 10,
          columnDefinitions: [
            {
              id: 'reference',
              value: 'Reference',
            },
            {
              id: 'nameOfIndividual',
              value: 'Name',
            },
            {
              id: 'defaultPhotoThumbnailBlobId',
            },
            {
              id: 'defaultEntityMediaBlobId',
            },
            {
              value: 'Has Attachments',
              id: 'hasAttachments',
            },
            {
              id: 'actionRequired',
            },
            {
              id: 'entityTypeId',
              isHidden: true,
            },
            {
              id: 'entityTypeIcon',
              isHidden: true,
            },
          ],
          showPaging: false,
          itemTemplate: {
            type: 'identificationrow',
          },
          itemsUi: {
            columnCounts: [1, 1, 1],
            gap: '1rem',
          },
        },
      },
    },
    {
      name: 'identificationgrid',
      extends: 'datacards',
      defaultOptions: {
        props: {
          listName: 'MyIdents',
          idColumn: 'unidentifiedSubjectId',
          sortColumn: 'creationDate',
          sortDirection: 'desc',
          pageSize: 10,
          columnDefinitions: [
            {
              id: 'reference',
              value: 'Reference',
              dataType: 'input',
            },
            {
              id: 'nameOfIndividual',
              value: 'Name',
            },
            {
              id: 'defaultPhotoThumbnailBlobId',
            },
            {
              id: 'defaultEntityMediaBlobId',
            },
            {
              value: 'Has Attachments',
              id: 'hasAttachments',
            },
            {
              id: 'actionRequired',
            },
            {
              id: 'entityTypeId',
              isHidden: true,
            },
            {
              id: 'entityTypeIcon',
              isHidden: true,
            },
          ],
          showPaging: false,
          itemTemplate: {
            type: 'identification',
          },
          itemsUi: {
            columnCounts: [1, 2, 4],
            gap: '1rem',
          },
        },
      },
    },
    {
      name: 'imageview',
      component: ImageViewTypeComponent,
    },
    {
      name: 'imagecheck',
      component: ImageCheckTypeComponent,
    },
    {
      name: 'metadatalist',
      component: MetaDataListTypeComponent,
    },
    {
      name: 'operationsdatatable',
      extends: 'datatable',
      defaultOptions: {
        props: {
          savedOptionsId: 'operations',
          listName: 'Operations',
          selectMode: 'none',
          sortColumn: 'creationDate',
          sortDirection: 'desc',
          columnDefinitions: [
            {
              id: 'operationName',
              value: 'Name',
              isSortable: true,
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'operationAlias',
              value: 'Reference',
              isSortable: true,
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'operationType',
              value: 'Type',
              isSortable: true,
            },
            {
              id: 'creationDate',
              value: 'Start Date',
              dataType: 'date',
              isSortable: true,
              maxWidth: '100px',
              filterParams: {
                formlyType: 'daterange',
                props: {
                  showQuickLinks: true,
                  label: 'Start Date',
                },
              },
            },
            {
              id: 'assignedTo',
              value: 'Assigned',
              dataType: 'profile',
              isSortable: true,
              filterParams: {
                formlyType: 'multichoice',
                searchValueColumn: 'fullName',
                props: {
                  listName: 'PersonGrouped',
                  valueProp: 'personId',
                  labelProp: 'fullName',
                  sortColumn: 'fullName',
                  selectColumns: ['personId', 'fullName'],
                },
              },
              computedValue: {
                name: '${assignedTo}',
                blobId: '${assignedToBlobId}',
              },
            },
            {
              id: 'assignedToBlobId',
              isHidden: true,
            },
            {
              id: 'operationStatusName',
              value: 'Status',
              dataType: 'status',
              isSortable: true,
              filterParams: {
                formlyType: 'multichoice',
                props: {
                  lookupKey: 'OperationStatus',
                },
              },
              computedValue: {
                value: '${operationStatusName}',
                cssClass: '${operationStatusCssClass}',
              },
            },
            {
              id: 'operationStatusCssClass',
              isHidden: true,
            },
            {
              id: 'submissionCount',
              value: 'Submissions',
              maxWidth: '100px',
            },
            {
              id: 'exhibitCount',
              value: 'Exhibits',
              maxWidth: '100px',
            },
          ],
          idColumn: 'operationId',
          filterColumns: [],
          showPaging: true,
          showQuickSearch: true,
          filterTitle: 'Filter Operations',
          actionsMinWidth: '100px',
          actionsMaxWidth: '100px',
        },
      },
    },
    {
      name: 'operationtypeahead',
      extends: 'typeahead',
      defaultOptions: {
        props: {
          listName: 'Operations',
          valueProp: 'operationId',
          labelProp: 'name',
          filterColumns: [{ column: 'operationName', stringFilter: '', constantFilter: false }],
        },
      },
    },
    {
      name: 'password',
      extends: 'input',
      defaultOptions: {
        props: {
          type: 'password',
        },
      },
    },
    {
      name: 'personview',
      component: PersonViewTypeComponent,
    },
    {
      name: 'personlistview',
      component: PersonListViewTypeComponent,
    },
    {
      name: 'progress',
      component: ProgressTypeComponent,
    },
    { name: 'radio', component: RadioTypeComponent, wrappers: ['formfield'] },
    {
      name: 'readonly',
      extends: 'input',
      defaultOptions: {
        props: {
          readonly: true,
        },
      },
    },
    { name: 'relatesto', component: RelatesToTypeComponent, wrappers: ['formfield'] },
    { name: 'repeat', component: RepeatTypeComponent },
    { name: 'reportbutton', component: ReportButtonTypeComponent },
    { name: 'richfieldsview', component: RichFieldsViewTypeComponent },
    { name: 'richtext', component: RichTextTypeComponent, wrappers: ['formfield'] },
    { name: 'richtextviewer', component: RichTextViewerTypeComponent, wrappers: ['formfield'] },
    { name: 'routelist', component: RouteListTypeComponent },
    { name: 'searchresultcase', component: SearchResultCaseViewTypeComponent },
    { name: 'select', component: SelectTypeComponent, wrappers: ['formfield'] },
    {
      name: 'select2',
      extends: 'select',
      defaultOptions: {
        props: {
          version: 2,
        },
      },
    },
    { name: 'signature', component: SignatureTypeComponent, wrappers: ['formfield'] },
    { name: 'stepper', component: StepperTypeComponent },
    { name: 'string', extends: 'input' },
    {
      name: 'submissionsdatatable',
      extends: 'datatable',
      defaultOptions: {
        props: {
          savedOptionsId: 'submissions',
          listName: 'Submissions',
          selectMode: 'none',
          sortColumn: 'reference',
          columnDefinitions: [
            {
              id: 'reference',
              value: 'Reference',
              isSortable: true,
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'operationName',
              value: 'Operation',
              isSortable: true,
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'creationDate',
              value: 'Date Created',
              dataType: 'date',
              isSortable: true,
              filterParams: {
                formlyType: 'daterange',
                props: {
                  label: 'Date Created',
                  showQuickLinks: true,
                },
              },
            },
            {
              id: 'submittingOfficer',
              value: 'Submitting Officer',
              dataType: 'profile',
              isSortable: true,
              filterParams: {
                formlyType: 'multichoice',
                searchValueColumn: 'fullName',
                props: {
                  listName: 'Users',
                  valueProp: 'personId',
                  labelProp: 'fullName',
                  sortColumn: 'fullName',
                  selectColumns: ['personId', 'fullName'],
                },
              },
              computedValue: {
                name: '${submittingOfficer}',
                blobId: '${submittingOfficerBlobId}',
              },
            },
            {
              id: 'submittingOfficerBlobId',
              isHidden: true,
            },
            {
              id: 'statusName',
              value: 'Status',
              dataType: 'status',
              isSortable: true,
              minWidth: '200px',
              filterParams: {
                formlyType: 'multichoice',
                props: {
                  lookupKey: 'SubmissionStatus',
                },
              },
              computedValue: {
                value: '${statusName}',
                cssClass: '${cssClass}',
              },
            },
            {
              id: 'canEdit',
              isHidden: true,
            },
            {
              id: 'submissionTypeLookupId',
              value: 'Submission Type',
              dataType: 'lookup',
              isSortable: true,
              isfilterable: true,
              filterParams: {
                filterAlias: 'submissionType',
                formlyType: 'multichoice',
                props: {
                  lookupKey: 'SubmissionType',
                },
              },
            },
            {
              id: 'unreadMessages',
              dataType: 'messages',
            },
          ],
          idColumn: 'submissionId',
          filterColumns: [],
          showPaging: true,
          showQuickSearch: true,
          filterTitle: 'Filter Submissions',
        },
      },
    },
    {
      name: 'submissiontypeahead',
      extends: 'typeahead',
      defaultOptions: {
        props: {
          listName: 'Submissions',
          valueProp: 'submissionId',
          labelProp: 'reference',
          filterColumns: [{ column: 'reference', stringFilter: '' }],
        },
      },
    },
    { name: 'table', component: TableTypeComponent, wrappers: ['formfield'] },
    {
      name: 'tableview',
      extends: 'table',
      wrappers: [],
      defaultOptions: {
        props: {
          enableAdd: false,
          enableDelete: false,
          enableEdit: false,
          isRaised: false,
        },
      },
    },
    { name: 'tasksummary', component: TaskSummaryTypeComponent, wrappers: ['formfield'] },
    { name: 'text', extends: 'input' },
    { name: 'textarea', component: TextAreaTypeComponent, wrappers: ['formfield'] },
    {
      name: 'textareareadonly',
      extends: 'textarea',
      defaultOptions: {
        props: {
          readonly: true,
        },
      },
    },
    { name: 'textpanel', component: TextPanelTypeComponent, wrappers: ['formfield'] },
    { name: 'textwithbarcode', component: TextBarcodeTypeComponent, wrappers: ['formfield'] },
    { name: 'textview', component: TextViewTypeComponent },
    { name: 'toggle', component: SlideToggleTypeComponent, wrappers: ['formfield'] },
    { name: 'time', component: TimeTypeComponent, wrappers: ['formfield'] },
    { name: 'typeahead', component: TypeAheadTypeComponent, wrappers: ['formfield'] },
    {
      name: 'typeahead2',
      extends: 'typeahead',
      defaultOptions: {
        props: {
          version: 2,
        },
      },
    },
    { name: 'unidentifiedsubject', component: UnidentifiedSubjectTypeComponent },
    { name: 'unidentifiedsubjectrow', extends: 'unidentifiedsubject', defaultOptions: { props: { viewType: 'row' } } },
    { name: 'unidentifiedsubjectdetail', component: UnidentifiedSubjectDetailTypeComponent },
    {
      name: 'unidentifiedsubjectgrid',
      extends: 'datacards',
      defaultOptions: {
        props: {
          listName: 'UnidentifiedSubject',
          idColumn: 'unidentifiedSubjectId',
          sortColumn: 'creationDate',
          pageSize: 10,
          filterColumns: [
            {
              column: 'Status',
              stringFilter: 'Published',
              constantFilter: true,
            },
          ],
          columnDefinitions: [
            {
              id: 'Viewed',
            },
            {
              id: 'unitId',
              value: 'Division',
              dataType: 'lookup',
              isSortable: true,
              isfilterable: true,
              filterParams: {
                filterAlias: 'UnitName',
                formlyType: 'multichoice',
                props: {
                  lookupKey: 'InvestigatingUnit',
                  leafSelection: false,
                },
              },
            },
            {
              id: 'crimeTypeId',
              value: 'Crime Type',
              dataType: 'lookup',
              isSortable: true,
              isfilterable: true,
              filterParams: {
                filterAlias: 'CrimeType',
                formlyType: 'multichoice',
                props: {
                  lookupKey: 'CMOCrimeCategoryOffense',
                  leafSelection: false,
                },
              },
            },
            {
              id: 'crimeNumber',
              value: 'Crime No',
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'creationDate',
              value: 'Offence Date',
              filterParams: {
                formlyType: 'daterange',
              },
            },
            {
              id: 'premisesName',
              value: 'Premises Name',
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'premisesAddress',
              value: 'Premises Address',
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'reference',
              value: 'Reference',
              filterParams: {
                formlyType: 'input',
              },
            },

            {
              id: 'photoCount',
            },
            {
              id: 'audioCount',
            },
            {
              id: 'videoCount',
            },
            {
              id: 'mediaJson',
            },
          ],
          showPaging: true,
          itemTemplate: {
            type: 'unidentifiedsubject',
          },
          itemsUi: {
            columnCounts: [1, 2, 4],
            gap: '1rem',
          },
          detailDialogFormId: 'UnidentifiedSubjects.View',
          detailDialogTriggerIds: ['unidentifiedSubjectId'],
          detailDialogueTitleTemplate: '${thumbnailBlobId}',
          detailDialogMaxWidth: '100vw',
          detailDialogMinWidth: '20vw',
          detailDialogeEnableTitle: false,
          detailDialogWidth: '90vw',
          detailDialogTitle: 'Unidentified Subject',
          previewDialogFormId: 'Media.Preview',
          previewDialogTitle: 'Media Request Review',
          previewDialogMaxWidth: '730px',
          previewDialogWidth: '100%',
          requestDialogFormId: 'Media.Request',
          requestDialogTitle: 'Media Request Form',
          requestDialogMaxWidth: '570px',
          requestDialogWidth: '100%',
          requestDialogPosition: 'relative',
          enableSelectAll: false,
        },
      },
    },
    {
      name: 'unidentifiedsubjectlist',
      extends: 'datacards',
      defaultOptions: {
        props: {
          listName: 'UnidentifiedSubject',
          idColumn: 'unidentifiedSubjectId',
          sortColumn: 'creationDate',
          sortDirection: 'desc',
          pageSize: 10,
          filterColumns: [
            {
              column: 'Status',
              stringFilter: 'Published',
              constantFilter: true,
            },
          ],
          columnDefinitions: [
            {
              id: 'unitId',
              value: 'Division',
              dataType: 'lookup',
              isSortable: true,
              isfilterable: true,
              filterParams: {
                filterAlias: 'UnitName',
                formlyType: 'multichoice',
                props: {
                  lookupKey: 'InvestigatingUnit',
                  leafSelection: false,
                },
              },
            },
            {
              id: 'crimeTypeId',
              value: 'Crime Type',
              dataType: 'lookup',
              isSortable: true,
              isfilterable: true,
              filterParams: {
                filterAlias: 'CrimeType',
                formlyType: 'multichoice',
                props: {
                  lookupKey: 'CMOCrimeCategoryOffense',
                  leafSelection: false,
                },
              },
            },
            {
              id: 'crimeNumber',
              value: 'Crime No',
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'creationDate',
              value: 'Offence Date',
              filterParams: {
                formlyType: 'daterange',
              },
            },
            {
              id: 'premisesName',
              value: 'Premises Name',
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'premisesAddress',
              value: 'Premises Address',
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'reference',
              value: 'Reference',
              filterParams: {
                formlyType: 'input',
              },
            },
            {
              id: 'photoCount',
            },
            {
              id: 'audioCount',
            },
            {
              id: 'videoCount',
            },
            {
              id: 'mediaJson',
            },
            {
              id: 'viewed',
            },
          ],
          showPaging: true,
          itemTemplate: {
            type: 'unidentifiedsubjectrow',
          },
          itemsUi: {
            columnCounts: [1, 1, 1],
            gap: '1rem',
          },
        },
      },
    },
    { name: 'uploader', component: UploaderTypeComponent, wrappers: ['formfield'] },
    { name: 'uploaderoffline', component: UploaderOfflineTypeComponent, wrappers: ['formfield'] },
    { name: 'uuid', component: UuidTypeComponent },
    { name: 'video', component: VideoTypeComponent },
    {
      name: 'watermarkimagecheck',
      extends: 'imagecheck',
      defaultOptions: {
        props: {
          watermark: true,
        },
      },
    },
    {
      name: 'watermarkimageview',
      extends: 'imageview',
      defaultOptions: {
        props: {
          watermark: true,
        },
      },
    },
    {
      name: 'yesno',
      extends: 'radio',
      defaultOptions: {
        props: {
          options: [
            { label: 'Yes', value: 'yes' },
            { label: 'No', value: 'no' },
          ],
        },
      },
    },
  ],
  wrappers: [
    { name: 'card', component: CardWrapperComponent },
    { name: 'cardlist', component: CardListWrapperComponent },
    { name: 'entitymediarequest', component: EntityMediaRequestWrapperComponent },
    { name: 'flex', component: FlexWrapperComponent },
    { name: 'formfield', component: FormFieldWrapperComponent },
    { name: 'formgallery', component: FormGalleryWrapperComponent },
    { name: 'formgallerygroup', component: FormGalleryGroupWrapperComponent },
    { name: 'grid', component: GridWrapperComponent },
    { name: 'latlong', component: LatLongWrapperComponent },
    { name: 'panel', component: PanelWrapperTypeComponent },
    { name: 'quicksearch', component: QuickSearchWrapperComponent },
    { name: 'scroll', component: ScrollWrapperComponent },
    { name: 'sectionedit', component: SectionEditWrapperComponent },
    { name: 'listfilter', component: ListFilterWrapperComponent },
  ],
};
