import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { LookupDetail } from '@nimbus/core/src/lib/core';

export const LookupActions = createActionGroup({
  source: 'Lookup',
  events: {
    'Load Lookups': emptyProps,
    'Load Lookups Success': props<{ lookups: LookupDetail[] | undefined }>(),
    'Add Lookup': props<{ lookup: LookupDetail }>(),
  },
});
