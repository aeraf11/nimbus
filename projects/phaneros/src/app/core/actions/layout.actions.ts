import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const LayoutActions = createActionGroup({
  source: 'Layout',
  events: {
    'Open Menu': emptyProps,
    'Close Menu': emptyProps,
    'Menu State Change': props<{ menuState: 'closed' | 'open' }>(),
    'Hover Menu': emptyProps,
    'Show Toolbars': emptyProps,
    'Hide Toolbars': emptyProps,
    'Set Content Class': props<{ contentClass: string }>(),
    'Set Breakpoint': props<{ breakpoint: 'sm' | 'md' | 'lg' }>(),
  },
});
