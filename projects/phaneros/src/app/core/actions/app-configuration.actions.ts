import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { AppConfiguration, MetaDataItem } from '@nimbus/core/src/lib/core';
import { HomeItemConfig, MenuItemConfig, DynamicMenus } from '@nimbus/core/src/lib/apto';
import { TimeZone } from '@nimbus/material';

export const AppConfigurationActions = createActionGroup({
  source: 'AppConfiguration',
  events: {
    'Load Config Success': props<{ config: AppConfiguration }>(),
    'Load Config Failed': emptyProps,
    'Load Dynamic Menus': emptyProps,
    'Load Dynamic Menus Success': props<{ config: DynamicMenus }>(),
    'Add Menu Items': props<{ menuConfig: MenuItemConfig[] }>(),
    'Add Home Items': props<{ homeConfig: HomeItemConfig[] }>(),
    'Load Time Zone': emptyProps,
    'Load Time Zone Success': props<{ timeZones: TimeZone[] }>(),
    'Load Theme': emptyProps,
    'Set Theme': props<{ themeId: string }>(),
    'Upgrade Config': emptyProps,
    'Upgrade Config Success': props<{ upgradeMessage: string }>(),
    'Upgrade Config Failed': props<{ upgradeMessage: string }>(),
    'Load Entity Default Meta Data': props<{ defaultMetaData: { [key: number]: MetaDataItem[] } }>(),
    'Load MetaData Cache Keys': emptyProps,
  },
});
