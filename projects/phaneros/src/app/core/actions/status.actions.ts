import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const StatusActions = createActionGroup({
  source: 'Status',
  events: {
    'Set Loading': props<{ loading: boolean }>(),
    'Update Connection Status': props<{ connectionOnline: boolean }>(),
    'Set Connection Online': emptyProps,
    'Set Connection Offline': emptyProps,
    'Set Work Online': emptyProps,
    'Set Work Offline': emptyProps,
    'Update Connection Timeout Pointer': props<{ connectionTimeoutPointer: number }>(),
  },
});
