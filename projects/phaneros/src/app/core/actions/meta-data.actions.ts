import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { MetaDataKey } from '@nimbus/core/src/lib/core';

export const MetaDataActions = createActionGroup({
  source: 'MetaDataKeys',
  events: {
    'Load MetaDataKeys': emptyProps,
    'Load MetaDataKeys Success': props<{ keys: MetaDataKey[] | undefined }>(),
    'Load MetaDataKeys Failed': emptyProps,
    'Refresh MetaDataKeys': emptyProps,
  },
});
