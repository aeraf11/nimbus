import { StatusActions } from '../actions';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import { DialogService, DialogConfig, DialogType } from '@nimbus/core/src/lib/core';
import { Observable } from 'rxjs';

@Injectable()
export class StatusEffects {
  online$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(StatusActions.updateConnectionStatus),
      concatLatestFrom(() => [
        this.store.select(fromRoot.selectConnectionOnline),
        this.store.select(fromRoot.selectWorkOnline),
        this.store.select(fromRoot.selectWorkOnlinePromptTimeout),
      ]),
      tap(([{ connectionOnline }, currentStatus, workOnline, promptTimeout]) => {
        if (connectionOnline !== currentStatus) {
          if (connectionOnline && !workOnline) {
            this.setStatusMessage('Online', 'Your connection is online', DialogType.OK);
            //Needs to be window.setTimeout otherwise will error on Timeout to number.
            const timeout = window.setTimeout(() => {
              const dialogRef = this.setStatusMessage(
                'Offline',
                'You are working offline, would you like to work online?',
                DialogType.OKCancel
              );
              dialogRef.subscribe((result) => {
                if (result) this.setWorkOnline();
              });
            }, promptTimeout);
            this.updateConnectionPointer(timeout);
          } else if (!connectionOnline && workOnline) {
            const dialogRef = this.setStatusMessage(
              'Offline',
              'Your connection is offline, would you like to work offline?',
              DialogType.OKCancel
            );
            dialogRef.subscribe((result) => {
              if (result) this.setWorkOffline();
            });
          }
        }
      }),
      map(([{ connectionOnline }]) => {
        return connectionOnline ? StatusActions.setConnectionOnline() : StatusActions.setConnectionOffline();
      })
    );
  });

  workOnline$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(StatusActions.setWorkOnline),
        concatLatestFrom(() => [this.store.select(fromRoot.selectConnectionTimeoutPointer)]),
        tap(([, currentConnectionTimeoutPointer]) => {
          if (currentConnectionTimeoutPointer) {
            clearTimeout(currentConnectionTimeoutPointer);
          }
        })
      );
    },
    { dispatch: false }
  );

  updateConnectionPointer(timeoutPointer: number): void {
    this.store.dispatch(StatusActions.updateConnectionTimeoutPointer({ connectionTimeoutPointer: timeoutPointer }));
  }

  setWorkOnline(): void {
    this.store.dispatch(StatusActions.setWorkOnline());
  }

  setWorkOffline(): void {
    this.store.dispatch(StatusActions.setWorkOffline());
  }

  setStatusMessage(title: string, bodyText: string, type: DialogType): Observable<unknown> {
    return this.dialogService.openDialog(<DialogConfig>{
      title: title,
      bodyText: bodyText,
      type: type,
    });
  }

  constructor(private actions$: Actions, private store: Store<fromRoot.State>, private dialogService: DialogService) {}
}
