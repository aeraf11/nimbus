import { LookupActions } from '../actions';
import { Injectable } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { LookupDataService } from '@nimbus/core/src/lib/core';

@Injectable()
export class LookupEffects {
  lookups$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(LookupActions.loadLookups),
      switchMap(() =>
        this.service.loadLookups().pipe(
          map((lookups) => {
            return LookupActions.loadLookupsSuccess({ lookups });
          })
        )
      )
    );
  });

  constructor(private actions$: Actions, private service: LookupDataService) {}
}
