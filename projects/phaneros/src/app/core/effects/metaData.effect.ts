import { MetaDataActions } from '../actions';
import { Injectable } from '@angular/core';
import { map, switchMap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { MetaDataService } from '@nimbus/core/src/public-api';

@Injectable()
export class MetaDataEffects {
  metaData$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MetaDataActions.loadMetadatakeys),
      switchMap(() =>
        this.service.loadMetaDataCacheKeys().pipe(
          map((keys) => {
            return MetaDataActions.loadMetadatakeysSuccess({ keys });
          })
        )
      )
    );
  });

  constructor(private actions$: Actions, private service: MetaDataService) {}
}
