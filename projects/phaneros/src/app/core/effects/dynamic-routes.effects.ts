import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Route, Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { BookmarkService, DynamicRouteComponent } from '@nimbus/core/src/lib/apto';
import {
  AppInsightsService,
  CanDeactivateGuard,
  OptionsService,
  DynamicRoute,
  IndexedDbService,
  ListService,
  SnackMessageComponent,
  SnackMessageType,
  utils,
} from '@nimbus/core/src/lib/core';
import { of } from 'rxjs';
import { catchError, filter, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { DynamicRouteActions, StatusActions } from '../actions';
import { HomeComponent, NotFoundPageComponent } from '../containers';
import { DynamicRoutesDataService } from '../services/dynamic-routes-data.service';
import { AuthGuard } from './../../auth/services/auth.guard';

@Injectable()
export class DynamicRouteEffects {
  getRoutes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.loadDynamicRoutes),
      mergeMap(() =>
        this.drService.loadRoutes().pipe(
          map((dynamicRoutes) => DynamicRouteActions.loadDynamicRoutesSuccess({ dynamicRoutes })),
          catchError((error) => {
            console.error(error);
            this.appInsightService.logException(error, 3);
            return of(DynamicRouteActions.loadDynamicRoutesFailure({ error }));
          })
        )
      )
    );
  });

  pushRoutesToConfig$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DynamicRouteActions.loadDynamicRoutesSuccess),
        tap(({ dynamicRoutes }) => {
          const config: Route[] = [];

          if (!this.isDynamicHomeRoute(dynamicRoutes)) {
            config.push({
              path: 'home',
              component: HomeComponent,
              data: { title: 'Home', hideToolbars: true, id: 'Home', contentClass: 'no-pad home-wrapper' },
              canActivate: [AuthGuard],
            });
          }

          dynamicRoutes.forEach((route) => {
            if (route.route) {
              config.push({
                path: route.route,
                component: DynamicRouteComponent,
                data: {
                  id: route.routeId,
                  title: route.pageTitle,
                  contentClass: route.cssClass,
                },
                canActivate: [AuthGuard],
                canDeactivate: [CanDeactivateGuard],
              });
            }
          });
          const clean = this.router.config.filter((r) => r.path !== '**');
          const notfound = [
            {
              path: '**',
              component: NotFoundPageComponent,
              data: { title: 'Not Found' },
            },
          ];
          this.router.resetConfig([...config, ...clean, ...notfound]);
        })
      );
    },
    { dispatch: false }
  );

  getCurrentRouteModel$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.loadCurrentDynamicRouteModel),
      mergeMap(({ routeId: name, context }) =>
        this.drService.loadModel(name, context).pipe(
          map((model) => DynamicRouteActions.loadCurrentDynamicRouteModelSuccess({ model })),
          catchError((error) => {
            console.error(error);
            this.appInsightService.logException(error);
            return of(DynamicRouteActions.loadCurrentDynamicRouteModelFailure({ error }));
          })
        )
      )
    );
  });

  getCurrentRouteModelOffline$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.loadCurrentDynamicRouteModelOffline),
      mergeMap(({ routeId: name, context }) =>
        this.drService.loadModelOffline(name, context).pipe(
          map((model) => DynamicRouteActions.loadCurrentDynamicRouteModelOfflineSuccess({ model })),
          catchError((error) => {
            console.error(error);
            this.appInsightService.logException(error);
            return of(DynamicRouteActions.loadCurrentDynamicRouteModelOfflineFailure({ error }));
          })
        )
      )
    );
  });

  modelStartLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.loadCurrentDynamicRouteModel, DynamicRouteActions.loadCurrentDynamicRouteModelOffline),
      map(() => {
        return StatusActions.setLoading({ loading: true });
      })
    );
  });

  modelStopLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(
        DynamicRouteActions.loadCurrentDynamicRouteModelSuccess,
        DynamicRouteActions.loadCurrentDynamicRouteModelFailure,
        DynamicRouteActions.loadCurrentDynamicRouteModelOfflineSuccess,
        DynamicRouteActions.loadCurrentDynamicRouteModelOfflineFailure
      ),
      map(() => {
        return StatusActions.setLoading({ loading: false });
      })
    );
  });

  postForm$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.postForm),
      mergeMap(({ model, routeId, onSuccess, showErrorSnack }) =>
        this.drService.postForm(routeId, model).pipe(
          map((result) => DynamicRouteActions.postFormSuccess({ routeId, result, model, onSuccess })),
          catchError((error) => {
            console.error(error);
            this.appInsightService.logException(error);
            if (showErrorSnack) {
              this.showSnack(error?.error?.message);
            }
            return of(DynamicRouteActions.postFormFailure({ error: error?.error?.message ?? error?.message ?? error }));
          })
        )
      )
    );
  });

  postFormSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DynamicRouteActions.postFormSuccess),
        tap(({ result, onSuccess }) => {
          if (onSuccess) onSuccess();
          const effects = result.effects;
          if (effects) {
            if (effects.message) {
              this.showSnack(effects.message);
            }
            if (effects.redirectUrl) {
              this.router.navigateByUrl(effects.redirectUrl);
            }
            if (effects.removeOfflineEntity) {
              this.dbService
                .table(effects.removeOfflineEntity.table)
                .delete(effects.removeOfflineEntity.id)
                .then(() => {
                  const refreshId = effects.removeOfflineEntity?.refreshTableId;
                  if (refreshId) {
                    this.listService.reloadList(refreshId);
                  }
                });
            }
          }
        })
      );
    },
    { dispatch: false }
  );

  postFormSuccessClear$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.postFormSuccess),
      filter(({ result }) => !!result.effects?.clearModel),
      map(() => DynamicRouteActions.clearCurrentDynamicRouteModel())
    );
  });

  postFormSuccessReloadModel$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.postFormSuccess),
      filter(({ result }) => !!result.effects?.reloadModel),
      map(({ routeId, result }) =>
        DynamicRouteActions.loadCurrentDynamicRouteModel({ routeId, context: result.context })
      )
    );
  });

  postFormSuccessReloadLists$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DynamicRouteActions.postFormSuccess),
        filter(({ result }) => (result.effects?.reloadLists || []).length > 0),
        tap(({ result }) => {
          (result.effects?.reloadLists || []).forEach((listName) => {
            this.listService.reloadList(listName);
          });
        })
      );
    },
    { dispatch: false }
  );

  postFormOffline$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.postFormOffline),
      mergeMap(({ model, context, routeId }) =>
        this.drService.postFormOffline(model, context, routeId).pipe(
          map((offlineConfig) => DynamicRouteActions.postFormOfflineSuccess({ offlineConfig })),
          catchError((error) => {
            console.error(error);
            this.appInsightService.logException(error);
            return of(
              DynamicRouteActions.postFormOfflineFailure({ error: error?.error?.message ?? error?.message ?? error })
            );
          })
        )
      )
    );
  });

  postFormOfflineSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DynamicRouteActions.postFormOfflineSuccess),
        tap(({ offlineConfig }) => {
          if (offlineConfig?.redirectUrl) {
            this.router.navigateByUrl(offlineConfig?.redirectUrl);
          }
          if (offlineConfig?.successSnackMessage) {
            this.showSnack(offlineConfig?.successSnackMessage);
          }
        })
      );
    },
    { dispatch: false }
  );

  postFormLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.postForm),
      map(() => {
        return StatusActions.setLoading({ loading: true });
      })
    );
  });

  postFormStopLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.postFormSuccess, DynamicRouteActions.postFormFailure),
      map(() => {
        return StatusActions.setLoading({ loading: false });
      })
    );
  });

  saveOptions$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DynamicRouteActions.saveDataListOptions),
        tap(({ options }) => this.listService.saveOptions(options))
      );
    },
    { dispatch: false }
  );

  loadOptions$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.loadDynamicRoutesSuccess),
      map(() => {
        const savedOptions = this.listService.loadOptions();
        return DynamicRouteActions.setDataListOptions({ savedOptions });
      })
    );
  });

  saveCardOptions$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DynamicRouteActions.saveCardOptions),
        tap((payload) => this.optionsService.saveOptions(payload.options))
      );
    },
    { dispatch: false }
  );

  saveBookmark$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(DynamicRouteActions.saveBookmark),
        tap(({ routeId, bookmark }) => this.bookmarks.saveBookmark(routeId, bookmark))
      );
    },
    { dispatch: false }
  );

  mergeModels$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(DynamicRouteActions.mergeDynamicRouteModel),
      switchMap(({ model, routeId, routeParams, autoFillModelSkipProps, autoFillModelOverrides }) =>
        this.routeService.loadModel(routeId, routeParams).pipe(
          map((newModel) => {
            const mergedModel = utils.autopopulateModel(model, newModel, {
              autoFillModelSkipProps: autoFillModelSkipProps || [],
              autoFillModelOverrides: autoFillModelOverrides || [],
            });
            return DynamicRouteActions.loadCurrentDynamicRouteModelSuccess({ model: mergedModel });
          })
        )
      )
    );
  });

  constructor(
    private actions$: Actions,
    private drService: DynamicRoutesDataService,
    private listService: ListService,
    private router: Router,
    private snackbar: MatSnackBar,
    private routeService: DynamicRoutesDataService,
    private bookmarks: BookmarkService,
    private optionsService: OptionsService,
    private dbService: IndexedDbService,
    private appInsightService: AppInsightsService
  ) {}

  showSnack(message: string) {
    this.snackbar.openFromComponent(SnackMessageComponent, {
      duration: 5000,
      data: { type: SnackMessageType.Info, message },
    });
  }

  private isDynamicHomeRoute(dynamicRoutes: DynamicRoute[]): boolean {
    return dynamicRoutes.filter((route) => route.routeId === 'Phaneros.Home')?.length > 0;
  }
}
