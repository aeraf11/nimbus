import { SiteConfigurationService } from './../../../../../core/src/lib/apto/services/site-configuration.service';
import { IndexedDbService, MetaDataService, ThemeService } from '@nimbus/core/src/lib/core';
import { AppConfigurationActions, LookupActions, MetaDataActions } from '../actions';
import { ConfigurationService } from '@nimbus/core/src/lib/core';
import { Injectable } from '@angular/core';
import { map, switchMap, tap } from 'rxjs/operators';
import { Actions, concatLatestFrom, createEffect, ofType } from '@ngrx/effects';
import { registerLicense as registerSyncfusionLicense } from '@syncfusion/ej2-base';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import { MetaDataRefreshService } from '../services/meta-data-refresh.service';
import { DynamicMenusDataService } from '../services/dynamic-menus-data.service.';

const THEME_STATE_KEY = 'themeState';

@Injectable()
export class AppConfigurationEffects {
  timeZone$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppConfigurationActions.loadTimeZone),
      switchMap(() =>
        this.appConfigurationService.getTimeZones().pipe(
          map((timeZones) => {
            return AppConfigurationActions.loadTimeZoneSuccess({ timeZones });
          })
        )
      )
    );
  });

  theme$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AppConfigurationActions.setTheme),
        tap(({ themeId }) => {
          localStorage.setItem(THEME_STATE_KEY, themeId);
          this.themeService.setTheme(themeId);
        })
      );
    },
    { dispatch: false }
  );

  themeLoad$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppConfigurationActions.loadTheme),
      concatLatestFrom(() => this.store.select(fromRoot.selectDefaultThemeId)),
      map(([, defaultThemeId]) => {
        const themeId = localStorage.getItem(THEME_STATE_KEY) ?? defaultThemeId ?? '';
        return AppConfigurationActions.setTheme({ themeId });
      })
    );
  });

  themeLoadLookups$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppConfigurationActions.loadConfigSuccess),
      map(({ config }) => {
        const lookups = config.themeConfig?.map((x) => ({
          displayValue: x.label,
          hierarchicalDisplayValue: x.label,
          lookupId: x.id,
          lookupKey: 'Themes',
        }));
        return LookupActions.loadLookupsSuccess({ lookups });
      })
    );
  });

  loadSyncfusionConfiguration$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AppConfigurationActions.loadConfigSuccess),
        map(({ config }) => {
          if (config.syncfusionLicenseKey) {
            registerSyncfusionLicense(config.syncfusionLicenseKey);
          }
        })
      );
    },
    { dispatch: false }
  );

  initialiseMetaDataCache$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppConfigurationActions.loadMetadataCacheKeys),
      switchMap(() =>
        this.metaDataService.loadMetaDataCacheKeys().pipe(
          map((keys) => {
            return MetaDataActions.loadMetadatakeysSuccess({ keys });
          })
        )
      )
    );
  });

  initialiseIndexedDb$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AppConfigurationActions.loadConfigSuccess),
        map(({ config }) => {
          if (config.indexedDbConfig) {
            this.indexedDbService.init(config.indexedDbConfig);
          }
        })
      );
    },
    { dispatch: false }
  );

  setMetaDataRefresh$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AppConfigurationActions.loadConfigSuccess),
        map(({ config }) => {
          if (config.metaDataCacheRefreshInterval) {
            this.metaDataRefreshService.initialize(config.metaDataCacheRefreshInterval);
          }
        })
      );
    },
    { dispatch: false }
  );

  loadMenus$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppConfigurationActions.loadDynamicMenus),
      switchMap(() =>
        this.dynamicMenusService.loadMenus().pipe(
          map((dynamicMenus) => {
            return AppConfigurationActions.loadDynamicMenusSuccess({ config: dynamicMenus });
          })
        )
      )
    );
  });

  loadMenusMainSuccess$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppConfigurationActions.loadDynamicMenusSuccess),
      tap((dynamicMenus) => console.log(dynamicMenus)),
      map((dynamicMenus) => {
        return AppConfigurationActions.addMenuItems({ menuConfig: dynamicMenus.config.main });
      })
    );
  });

  loadMenusHomeSuccess$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppConfigurationActions.loadDynamicMenusSuccess),
      map((dynamicMenus) => {
        return AppConfigurationActions.addHomeItems({ homeConfig: dynamicMenus.config.home });
      })
    );
  });

  upgrade$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AppConfigurationActions.upgradeConfig),
      switchMap(() =>
        this.siteConfigurationService.upgrade().pipe(
          map((success) => {
            if (success) return AppConfigurationActions.upgradeConfigSuccess({ upgradeMessage: 'Upgrade successful.' });
            else
              return AppConfigurationActions.upgradeConfigFailed({ upgradeMessage: 'Upgrade failed, check the logs.' });
          })
        )
      )
    );
  });

  constructor(
    private actions$: Actions,
    private appConfigurationService: ConfigurationService,
    private themeService: ThemeService,
    private store: Store,
    private indexedDbService: IndexedDbService,
    private siteConfigurationService: SiteConfigurationService,
    private metaDataService: MetaDataService,
    private metaDataRefreshService: MetaDataRefreshService,
    private dynamicMenusService: DynamicMenusDataService
  ) {}
}
