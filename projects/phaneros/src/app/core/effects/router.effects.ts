import { ApplicationInsights } from '@microsoft/applicationinsights-web';
import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { concatMap, map, tap, withLatestFrom } from 'rxjs/operators';
import * as fromRoot from '../../reducers';
import { LayoutActions } from '../actions';
import { AppInsightsService } from '@nimbus/core/src/public-api';

@Injectable()
export class RouterEffects {
  updateTitle$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(routerNavigatedAction),
        concatMap((action) => of(action).pipe(withLatestFrom(this.store.select(fromRoot.selectRouteData)))),
        map(([, data]) => `${data['title']} - Phaneros`),
        tap((title) => this.titleService.setTitle(title))
      );
    },
    {
      dispatch: false,
    }
  );

  updateToolBars$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      concatMap((action) => of(action).pipe(withLatestFrom(this.store.select(fromRoot.selectRouteData)))),
      map(([, data]) => {
        if (data['hideToolbars']) return LayoutActions.hideToolbars();
        else return LayoutActions.showToolbars();
      })
    );
  });

  updateContentClass$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(routerNavigatedAction),
      concatMap((action) => of(action).pipe(withLatestFrom(this.store.select(fromRoot.selectRouteData)))),
      map(([, data]) =>
        LayoutActions.setContentClass({
          contentClass: data['contentClass'] ? `content ${data['contentClass']}` : 'content',
        })
      )
    );
  });

  appInsights$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(routerNavigatedAction),
        concatMap((action) => of(action).pipe(withLatestFrom(this.store.select(fromRoot.selectRouteData)))),
        tap((insight) => {
          this.appInsights.logPageView(insight[1].title, insight[0].payload.routerState.url);
        })
      );
    },
    {
      dispatch: false,
    }
  );

  constructor(
    private actions$: Actions,
    private readonly store: Store<fromRoot.State>,
    private titleService: Title,
    private appInsights: AppInsightsService
  ) {}
}
