import { UserActions } from '../../auth/actions';
import { LayoutActions } from '../actions';
import { Injectable } from '@angular/core';
import { tap, map } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';

const MENU_STATE_KEY = 'menuState';

@Injectable()
export class LayoutEffects {
  openMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(LayoutActions.openMenu),
      map(() => LayoutActions.menuStateChange({ menuState: 'open' }))
    );
  });

  closeMenu$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(LayoutActions.closeMenu),
      map(() => LayoutActions.menuStateChange({ menuState: 'closed' }))
    );
  });

  saveState$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(LayoutActions.menuStateChange),
        tap((action) => localStorage.setItem(MENU_STATE_KEY, action.menuState))
      );
    },
    { dispatch: false }
  );

  loadState$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.loadUserDetailsSuccess),
      map(() => {
        const menuState = localStorage.getItem(MENU_STATE_KEY);
        switch (menuState) {
          case 'open':
            return LayoutActions.openMenu();
          case 'closed':
          default:
            return LayoutActions.closeMenu();
        }
      })
    );
  });

  constructor(private actions$: Actions) {}
}
