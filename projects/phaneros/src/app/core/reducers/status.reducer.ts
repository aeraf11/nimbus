import { createReducer, on } from '@ngrx/store';
import { StatusActions } from '../actions';

export const statusFeatureKey = 'status';

export interface State {
  connectionOnline: boolean;
  workOnline: boolean;
  loading: boolean;
  connectionTimeoutPointer: number | undefined;
}

const initialState: State = {
  connectionOnline: true,
  workOnline: true,
  loading: false,
  connectionTimeoutPointer: undefined,
};

export const reducer = createReducer(
  initialState,
  on(StatusActions.setConnectionOnline, (state): State => ({ ...state, connectionOnline: true })),
  on(StatusActions.setConnectionOffline, (state): State => ({ ...state, connectionOnline: false })),
  on(StatusActions.setLoading, (state, { loading }): State => ({ ...state, loading })),
  on(StatusActions.setWorkOnline, (state): State => ({ ...state, workOnline: true })),
  on(StatusActions.setWorkOffline, (state): State => ({ ...state, workOnline: false })),
  on(
    StatusActions.updateConnectionTimeoutPointer,
    (state, { connectionTimeoutPointer }): State => ({ ...state, connectionTimeoutPointer })
  )
);

export const selectConnectionOnline = (state: State) => state.connectionOnline;
export const selectLoading = (state: State) => state.loading;
export const selectWorkOnline = (state: State) => state.workOnline;
export const selectConnectionTimeoutPointer = (state: State) => state.connectionTimeoutPointer;
