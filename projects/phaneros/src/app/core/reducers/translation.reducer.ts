import { createReducer } from '@ngrx/store';
import { Translations } from '@nimbus/core/src/lib/core';

export const translationFeatureKey = 'translation';

export interface State {
  translations: Translations;
}

const initialState: State = {
  translations: {
    _root: {
      'Oliver is logged in as {0}': 'Luke is signed in as {0}',
    },
    operation: {
      name: 'test',
    },
  },
};

export const reducer = createReducer(initialState);
