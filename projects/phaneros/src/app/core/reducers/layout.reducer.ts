import { createReducer, on } from '@ngrx/store';
import { LayoutActions } from '../actions';

export const layoutFeatureKey = 'layout';

export interface State {
  menuState: 'open' | 'closed';
  showToolbars: boolean;
  contentClass: string;
  breakpoint: 'sm' | 'md' | 'lg';
}

const initialState: State = {
  menuState: 'closed',
  showToolbars: false,
  contentClass: 'content',
  breakpoint: 'lg',
};

export const reducer = createReducer(
  initialState,
  on(LayoutActions.closeMenu, (state): State => ({ ...state, menuState: 'closed' })),
  on(LayoutActions.openMenu, (state): State => ({ ...state, menuState: 'open' })),
  on(LayoutActions.showToolbars, (state): State => ({ ...state, showToolbars: true })),
  on(LayoutActions.hideToolbars, (state): State => ({ ...state, showToolbars: false })),
  on(LayoutActions.setContentClass, (state, { contentClass }): State => ({ ...state, contentClass })),
  on(LayoutActions.setBreakpoint, (state, { breakpoint }): State => ({ ...state, breakpoint }))
);

export const selectOpenSidenav = (state: State) => state.menuState;
export const selectShowToolbars = (state: State) => state.showToolbars;
export const selectContentClass = (state: State) => state.contentClass;
export const selectBreakpoint = (state: State) => state.breakpoint;
