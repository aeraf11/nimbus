import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { Lookup, LookupDetail, LookupDictionary, LookupHierarchy, LookupNode } from '@nimbus/core/src/lib/core';
import { LookupActions } from '../actions';

export const lookupsFeatureKey = 'lookups';

export type State = EntityState<LookupDetail>;

export const adapter: EntityAdapter<LookupDetail> = createEntityAdapter<LookupDetail>({
  selectId: (detail: LookupDetail) => detail.lookupId,
  sortComparer: false,
});

export const initialState: State = adapter.getInitialState();

// get the selectors
const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();

export const reducer = createReducer(
  initialState,
  on(LookupActions.loadLookupsSuccess, (state, { lookups }) => {
    return adapter.addMany(lookups || [], state);
  }),
  on(LookupActions.addLookup, (state, { lookup }) => {
    const result = adapter.addOne(lookup, state);
    return result;
  })
);

export const getHierarchy = (lookupDetails: LookupDetail[]): LookupHierarchy => {
  const topLevel = lookupDetails.filter((ld) => !ld.parentLookupId);
  return _getHierarchy(topLevel, lookupDetails);
};

export const _getHierarchy = (topLevel: LookupDetail[], lookupDetails: LookupDetail[]): LookupHierarchy => {
  const result: LookupHierarchy = {};

  for (const {
    lookupKey,
    lookupId,
    displayValue,
    hierarchicalDisplayValue,
    cssClass,
    isNew,
    parentLookupId,
    entityType,
  } of topLevel) {
    if (!result[lookupKey]) result[lookupKey] = [];
    const item = {
      lookupId,
      lookupKey,
      displayValue,
      hierarchicalDisplayValue: hierarchicalDisplayValue ?? displayValue,
      cssClass,
      isNew,
      isLeaf: true,
      level: 0,
      parentLookupId,
      entityType,
    };
    result[lookupKey].push({
      item,
      children: getHierachyChildren(lookupDetails, lookupId, item, 1),
    });
  }
  return result;
};

const getHierachyChildren = (
  lookupDetails: LookupDetail[],
  lookupId: string,
  parent: Lookup,
  level: number
): LookupNode[] => {
  const children = lookupDetails.filter((ld) => ld.parentLookupId === lookupId);
  if (children.length > 0) parent.isLeaf = false;
  return children.map(
    ({ lookupId, displayValue, hierarchicalDisplayValue, cssClass, isNew, lookupKey, parentLookupId, entityType }) => {
      const item = {
        lookupId,
        lookupKey,
        displayValue,
        hierarchicalDisplayValue: hierarchicalDisplayValue ?? displayValue,
        cssClass,
        isNew,
        isLeaf: true,
        level,
        parentLookupId,
        entityType,
      };
      return {
        item,
        children: getHierachyChildren(lookupDetails, lookupId, item, level + 1),
      };
    }
  );
};

export const getHierarchicalFiltered = (lookupDetails: LookupDetail[], filters: string[]): LookupNode[] => {
  const formattedFilters = formatFilters(filters);
  const topLevel = lookupDetails.filter((lookup) => matchFilters(lookup, formattedFilters));
  const result: LookupHierarchy = _getHierarchy(topLevel, lookupDetails);
  const lookupArray: LookupNode[] = [];
  if (result) {
    for (const key in result) {
      lookupArray.push(...result[key]);
    }
  }
  return lookupArray;
};

export const getFlatFiltered = (lookupDetails: LookupDetail[], filters: string[]): Lookup[] => {
  const formattedFilters = formatFilters(filters);
  const topLevel = lookupDetails.filter((lookup) => matchFilters(lookup, formattedFilters));
  const result: LookupDictionary = getFlat(topLevel);
  const lookupArray: Lookup[] = [];
  if (result) {
    for (const key in result) {
      lookupArray.push(...result[key]);
    }
  }
  return lookupArray;
};

export const getHierarchicalForList = (lookupDetails: LookupDetail[], filters: string[]): LookupNode[] => {
  let topLevel = null;
  const formattedFilters = formatFilters(filters);
  if (formattedFilters) {
    topLevel = lookupDetails.filter((lookup) => matchFilters(lookup, formattedFilters));
  } else {
    topLevel = lookupDetails.filter((ld) => !ld.parentLookupId);
  }
  const result: LookupHierarchy = _getHierarchy(topLevel, lookupDetails);
  const lookupArray: LookupNode[] = [];
  if (result) {
    for (const key in result) {
      lookupArray.push(...result[key]);
    }
  }
  return lookupArray;
};

export const formatFilters = (filters: string[]): LookUpFilters | null => {
  if (filters && filters.length === 0) {
    return null;
  }
  const ret = {
    guidFilters: filters.filter((f) => f.indexOf('/') !== 0).map((f) => f.toLowerCase()),
    regexFilters: filters
      .filter((f) => f.indexOf('/') === 0)
      .map((val) => {
        val = val.substring(1);
        const lastIndex = val.lastIndexOf('/');
        return new RegExp(escapeRegExp(val.substring(0, lastIndex)), val.substring(lastIndex + 1));
      }),
  };
  return ret;
};

function escapeRegExp(text: string) {
  if (!text) return '';
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
}

const matchFilters = (lookupDetails: LookupDetail, filters: LookUpFilters | null): boolean => {
  let ret = false;
  if (!filters) return false;
  if (filters.guidFilters && filters.guidFilters.includes(lookupDetails.lookupId.toLowerCase())) return true;

  if (filters.regexFilters) {
    filters.regexFilters.forEach((regex) => {
      try {
        if (!ret && regex.test(lookupDetails.hierarchicalDisplayValue)) {
          ret = true;
        }
      } catch {}
    });
  }
  return ret;
};

export const getFlat = (lookupDetails: LookupDetail[]): LookupDictionary => {
  const topLevel = lookupDetails.filter((ld) => !ld.parentLookupId);
  const dict: LookupDictionary = {};

  for (const {
    lookupKey,
    lookupId,
    displayValue,
    hierarchicalDisplayValue,
    cssClass,
    shortCode,
    parentLookupId,
    entityType,
  } of topLevel) {
    if (!dict[lookupKey]) dict[lookupKey] = [];
    const item = {
      lookupId,
      lookupKey,
      displayValue,
      shortCode,
      hierarchicalDisplayValue: hierarchicalDisplayValue ?? displayValue,
      cssClass,
      isLeaf: true,
      level: 0,
      parentLookupId,
      entityType,
    };
    dict[lookupKey].push(item);
    getFlatChildren(lookupDetails, dict, lookupKey, lookupId, item);
  }
  return dict;
};

const getFlatChildren = (
  lookupDetails: LookupDetail[],
  dict: LookupDictionary,
  lookupKey: string,
  lookupId: string,
  parent: Lookup
) => {
  if (dict) {
    const items = lookupDetails.filter((item) => item.parentLookupId === lookupId);
    if (items.length > 0) parent.isLeaf = false;
    for (const {
      lookupId,
      displayValue,
      hierarchicalDisplayValue,
      cssClass,
      isNew,
      shortCode,
      parentLookupId,
      entityType,
    } of items) {
      const item = {
        lookupId: lookupId,
        lookupKey,
        displayValue,
        shortCode,
        hierarchicalDisplayValue: hierarchicalDisplayValue ?? displayValue,
        cssClass,
        isNew,
        isLeaf: true,
        level: 0,
        parentLookupId,
        entityType,
      };
      dict[lookupKey].push(item);
      getFlatChildren(lookupDetails, dict, lookupKey, lookupId, item);
    }
  }
};

export const selectLookupDetailIds = selectIds;
export const selectLookupDetailEntities = selectEntities;
export const selectAllLookupDetails = selectAll;
export const selectLookupDetailsTotal = selectTotal;

export interface LookUpFilters {
  guidFilters: string[];
  regexFilters: RegExp[];
}
