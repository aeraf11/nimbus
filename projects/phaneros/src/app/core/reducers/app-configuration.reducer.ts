import { createReducer, on } from '@ngrx/store';
import { HomeItemConfig, MenuItemConfig } from '@nimbus/core/src/lib/apto';
import {
  AppConfiguration,
  HomeItem,
  LookupHierarchy,
  MenuItem,
  MetaDataItem,
  RouteConfig,
} from '@nimbus/core/src/lib/core';
import { TimeZone } from '@nimbus/material';
import { PermissionLogic } from '../../auth/models/permission-logic';
import { AppConfigurationActions } from '../actions';

export const appConfigurationFeatureKey = 'appConfiguration';

export interface State {
  config: AppConfiguration | null;
  timeZones: TimeZone[] | null;
  selectedThemeId: string | null;
  isConfigured: boolean;
  lookups: LookupHierarchy;
  upgradeMessage: string;
  defaultMetaData: { [key: number]: MetaDataItem[] } | null;
}

const initialState: State = {
  config: null,
  timeZones: null,
  selectedThemeId: null,
  isConfigured: false,
  lookups: null,
  upgradeMessage: '',
  defaultMetaData: null,
};

export const reducer = createReducer(
  initialState,
  on(
    AppConfigurationActions.loadConfigSuccess,
    (state, { config }): State => ({
      ...state,
      isConfigured: true,
      config: { ...state.config, ...config },
    })
  ),
  on(
    AppConfigurationActions.loadTimeZoneSuccess,
    (state, { timeZones }): State => ({
      ...state,
      timeZones,
    })
  ),
  on(
    AppConfigurationActions.setTheme,
    (state, { themeId }): State => ({
      ...state,
      selectedThemeId: themeId,
    })
  ),
  on(
    AppConfigurationActions.addHomeItems,
    (state, { homeConfig }): State => ({
      ...state,
      config: { ...state.config, homeConfig },
    })
  ),
  on(
    AppConfigurationActions.addMenuItems,
    (state, { menuConfig }): State => ({
      ...state,
      config: { ...state.config, menuConfig },
    })
  ),
  on(AppConfigurationActions.upgradeConfig, (state): State => ({ ...state, upgradeMessage: 'Upgrading...' })),
  on(
    AppConfigurationActions.upgradeConfigSuccess,
    (state, { upgradeMessage }): State => ({ ...state, upgradeMessage })
  ),
  on(AppConfigurationActions.upgradeConfigFailed, (state, { upgradeMessage }): State => ({ ...state, upgradeMessage })),
  on(
    AppConfigurationActions.loadEntityDefaultMetaData,
    (state, { defaultMetaData }): State => ({
      ...state,
      defaultMetaData,
    })
  )
);

export const getMenuItems = (
  menuConfigs: MenuItemConfig[] | undefined,
  routes: RouteConfig[] | null,
  userPermissions: string[] | null,
  forceAgencyLookupId: string | null,
  online: boolean
): MenuItem[] => {
  if (!menuConfigs || !routes || !userPermissions) return [];
  const menuItems: MenuItemConfig[] = [];

  menuConfigs.forEach((config) => {
    let items: MenuItemConfig[] | undefined | null;
    if (config.items?.length || 0 > 0) {
      items = config.items;
    }
    menuItems.push({ ...config, items: items });
  });

  return getMenuItemsRecursive(menuItems, routes, userPermissions, forceAgencyLookupId, online);
};

const getMenuItemsRecursive = (
  configs: MenuItemConfig[],
  routes: RouteConfig[],
  userPermissions: string[],
  forceAgencyLookupId: string | null,
  online: boolean
): MenuItem[] => {
  const permitted = configs.filter((config) => {
    if (!config.routeId) return true;
    let permittedRoute = true;

    const routeConfig = routes.find((r) => r.routeId === config.routeId);
    if (routeConfig?.permissionKeys) {
      switch (routeConfig?.permissionLogic || PermissionLogic.All) {
        case PermissionLogic.All:
          permittedRoute = routeConfig?.permissionKeys.every((key) => userPermissions.includes(key));
          break;
        case PermissionLogic.Any:
          permittedRoute = routeConfig.permissionKeys.some((key) => userPermissions.includes(key));
          break;
        default:
          permittedRoute = false;
      }
    }
    if (routeConfig?.forceAgencyPermissions && permittedRoute) {
      if (!routeConfig?.forceAgencyPermissions.includes(forceAgencyLookupId || '')) {
        permittedRoute = false;
      }
    }

    if ((online && routeConfig?.showOnline === false) || (!online && routeConfig?.showOffline === false)) {
      permittedRoute = false;
    }
    return permittedRoute;
  });

  const mapped = permitted.map((config) => {
    return {
      name: config.name,
      icon: config.icon,
      route: getRoute(config.routeId, routes),
      routeId: config.routeId,
      items: config.items
        ? getMenuItemsRecursive(config.items, routes, userPermissions, forceAgencyLookupId, online)
        : null,
    };
  });

  // Filter out empty items
  return mapped.filter((item) => item.items || item.route);
};

export const getHomeItems = (
  homeConfigs: HomeItemConfig[] | undefined,
  routes: RouteConfig[] | null,
  userPermissions: string[] | null,
  forceAgencyLookupId: string | null,
  online: boolean
): HomeItem[] => {
  if (!homeConfigs || !routes || !userPermissions) return [];

  const items: HomeItem[] = [];
  homeConfigs.forEach((config) => {
    let permitted = false;

    const routeConfig = routes.find((r) => r.routeId === config.routeId);
    if (routeConfig) {
      if (!routeConfig.permissionKeys || !routeConfig.forceAgencyPermissions) {
        permitted = true;
      } else {
        if (routeConfig.permissionKeys) {
          switch (routeConfig.permissionLogic || PermissionLogic.All) {
            case PermissionLogic.All:
              permitted = routeConfig.permissionKeys.every((key) => userPermissions.includes(key));
              break;

            case PermissionLogic.Any:
              permitted = routeConfig.permissionKeys.some((key) => userPermissions.includes(key));
              break;

            default:
              permitted = false;
          }
        }
        if (routeConfig?.forceAgencyPermissions) {
          if (!routeConfig?.forceAgencyPermissions.includes(forceAgencyLookupId || '')) {
            permitted = false;
          }
        }
        if ((online && routeConfig?.showOnline === false) || (!online && routeConfig?.showOffline === false)) {
          permitted = false;
        }
      }

      if (permitted) {
        items.push({
          name: config.name,
          icon: config.icon,
          // Need to make sure of leading '/' to ensure route is not interpreted relatively
          route: routeConfig.route.startsWith('/') ? routeConfig.route : `/${routeConfig.route}`,
          separator: config.separator ?? false,
          standalone: config.standalone ?? false,
        });
      }
    }
  });
  return items;
};

function getRoute(routeId: string | null, routes: RouteConfig[]): any {
  const route = routeId ? routes.find((r) => r.routeId === routeId)?.route : undefined;
  // Need to make sure of leading '/' to ensure route is not interpreted relatively
  return !route ? route : route.startsWith('/') ? route : `/${route}`;
}
