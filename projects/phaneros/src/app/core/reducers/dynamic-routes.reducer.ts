import { DynamicRoute, DataListOptions, RouteConfig, PermissionLogic } from '@nimbus/core/src/lib/core';
import { createReducer, on } from '@ngrx/store';
import { DynamicRouteActions } from '../actions';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';

export const dynamicRoutesFeatureKey = 'dynamicRoutes';

export interface State extends EntityState<DynamicRoute> {
  currentModel: any;
  error: string | null;
  savedOptions: { [key: string]: DataListOptions };
}

export const adapter: EntityAdapter<DynamicRoute> = createEntityAdapter<DynamicRoute>({
  selectId: (route: DynamicRoute) => route.routeId,
  sortComparer: false,
});

export const initialState: State = adapter.getInitialState({
  currentModel: undefined,
  error: null,
  savedOptions: {},
});

// get the selectors
const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();

export const reducer = createReducer(
  initialState,
  on(DynamicRouteActions.loadDynamicRoutesSuccess, (state, { dynamicRoutes }): State => {
    return adapter.addMany(
      dynamicRoutes.map((route) => {
        return {
          ...route,
          showOnline: route.showOnline === false ? false : true,
          showOffline: route.showOffline === false ? false : true,
        };
      }),
      state
    );
  }),
  on(DynamicRouteActions.loadCurrentDynamicRouteModelSuccess, (state, { model }): State => {
    return { ...state, currentModel: model, error: null };
  }),
  on(DynamicRouteActions.loadCurrentDynamicRouteModelOfflineSuccess, (state, { model }): State => {
    return { ...state, currentModel: model, error: null };
  }),
  on(DynamicRouteActions.clearCurrentDynamicRouteModel, (state): State => {
    return { ...state, currentModel: undefined, error: null };
  }),
  on(
    DynamicRouteActions.postForm,
    (state): State => ({
      ...state,
      error: null,
    })
  ),
  on(
    DynamicRouteActions.postFormFailure,
    (state, { error }): State => ({
      ...state,
      error,
    })
  ),
  on(DynamicRouteActions.postFormSuccess, (state, { result, model }): State => {
    if (result.effects?.retainModel) {
      return {
        ...state,
        error: null,
      };
    } else {
      let currentModel = null;
      const context = result.context;
      if (context && Object.keys(context).length) {
        currentModel = { ...model, ...context };
      } else {
        currentModel = { ...model };
      }
      return {
        ...state,
        currentModel,
        error: null,
      };
    }
  }),
  on(
    DynamicRouteActions.setDataListOptions,
    (state, { savedOptions }): State => ({
      ...state,
      savedOptions,
    })
  ),
  on(DynamicRouteActions.saveDataListOptions, (state, { options }): State => {
    if (!options.id) return state;
    const savedOptions = { ...state.savedOptions };
    savedOptions[options.id] = options;
    return {
      ...state,
      savedOptions,
    };
  })
);

export const selectDynamicRouteEntities = selectEntities;
export const selectAllDynamicRoutes = selectAll;

export const getRoutesWithPermissions = (
  allRoutes: RouteConfig[] | null,
  inputRoutes: any,
  userPermissions: any,
  forceAgencyLookupId: string | null,
  online: boolean
): any => {
  if (!allRoutes || !inputRoutes || !userPermissions) {
    return [];
  }

  const permittedRoutes: any[] = [];
  inputRoutes.forEach((route: any) => {
    let permitted = false;

    if (!route.hasOwnProperty('routeId') && route.hasOwnProperty('url')) {
      permittedRoutes.push(route);
    }

    const routeConfig = allRoutes.find((r) => r.routeId === route.routeId);
    if (routeConfig) {
      if (!routeConfig.permissionKeys && !routeConfig.forceAgencyPermissions) {
        permitted = true;
      } else {
        if (routeConfig.permissionKeys) {
          switch (routeConfig.permissionLogic ?? PermissionLogic.All) {
            case PermissionLogic.All:
              permitted = routeConfig.permissionKeys.every((key) => userPermissions.includes(key));
              break;
            case PermissionLogic.Any:
              permitted = routeConfig.permissionKeys.some((key) => userPermissions.includes(key));
              break;
            default:
              permitted = false;
          }
        }
        if (routeConfig.forceAgencyPermissions) {
          if (!routeConfig.forceAgencyPermissions.includes(forceAgencyLookupId || '')) {
            permitted = false;
          }
        }

        if ((online && routeConfig?.showOnline === false) || (!online && routeConfig?.showOffline === false)) {
          permitted = false;
        }
        if (permitted) {
          permittedRoutes.push({
            ...route,
            ...{ url: ensureLeadingSlashInUrl(routeConfig.route) },
          });
        }
      }
    }
  });

  return permittedRoutes;
};

const ensureLeadingSlashInUrl = (url: string) => {
  if (!url) {
    return '';
  }

  return url.charAt(0) === '/' ? url : `/${url}`;
};
