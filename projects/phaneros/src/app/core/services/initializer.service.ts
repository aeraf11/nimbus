import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppInsightsService, ConfigurationService, LookupDataService, StateService } from '@nimbus/core/src/lib/core';
import { forkJoin, Observable } from 'rxjs';
import { concatMap, take, tap } from 'rxjs/operators';
import * as fromRoot from '../../reducers';
import { AppConfigurationActions, DynamicRouteActions, LookupActions } from '../actions';
import { DynamicMenusDataService } from './dynamic-menus-data.service.';
import { DynamicRoutesDataService } from './dynamic-routes-data.service';

@Injectable({
  providedIn: 'root',
})
export class InitializerService {
  constructor(
    private appConfigService: ConfigurationService,
    private dynamicRouteService: DynamicRoutesDataService,
    private dynamicMenusService: DynamicMenusDataService,
    private lookupService: LookupDataService,
    private configService: ConfigurationService,
    private store: Store<fromRoot.State>,
    private state: StateService,
    private appInsights: AppInsightsService
  ) {}

  initialize(): Observable<any> {
    const appConfig$ = this.appConfigService.getAppConfig().pipe(
      tap((config) => {
        this.store.dispatch(AppConfigurationActions.loadConfigSuccess({ config }));
        this.appInsights.onInit(config.applicationInsightsConfig);
      })
    );

    return appConfig$.pipe(
      concatMap(() => {
        const theme$ = this.state.selectThemes().pipe(
          take(1),
          tap(() => this.store.dispatch(AppConfigurationActions.loadTheme()))
        );
        return forkJoin([theme$]);
      })
    );
  }

  getPostAuthConfig(): Observable<any> {
    const dynamicRoutes$ = this.dynamicRouteService
      .loadRoutes()
      .pipe(
        tap((dynamicRoutes) => this.store.dispatch(DynamicRouteActions.loadDynamicRoutesSuccess({ dynamicRoutes })))
      );

    const dynamicMenus$ = this.dynamicMenusService.loadMenus().pipe(
      tap((dynamicMenus) => {
        this.store.dispatch(AppConfigurationActions.addHomeItems({ homeConfig: dynamicMenus.home }));
        this.store.dispatch(AppConfigurationActions.addMenuItems({ menuConfig: dynamicMenus.main }));
      })
    );

    const lookup$ = this.lookupService
      .loadLookups()
      .pipe(tap((lookups) => this.store.dispatch(LookupActions.loadLookupsSuccess({ lookups }))));

    return forkJoin([dynamicRoutes$, dynamicMenus$, lookup$]);
  }
}
