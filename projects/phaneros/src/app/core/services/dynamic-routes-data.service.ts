import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { DynamicRoutePostResult } from '@nimbus/core/src/lib/apto';
import { AppDateTimeAdapter, DynamicRoute, IndexedDbService, StateService, utils } from '@nimbus/core/src/lib/core';
import { catchError, EMPTY, from, map, Observable } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { environment } from '../../../environments/environment';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class DynamicRoutesDataService {
  private routesUrl = `${environment.apiUrl}/dynamicroute/all?siteId=`;
  private mainUrl = `${environment.apiUrl}/dynamicroute`;
  private offlineTable = 'offlineEntities';
  private siteId?: string;
  private allowedContextParams: string[] = [
    'name',
    'siteId',
    'operationId',
    'submissionId',
    'examinationId',
    'crimeSceneId',
    'unidentifiedSubjectId',
  ];

  constructor(
    private httpClient: HttpClient,
    private stateService: StateService,
    private dbService: IndexedDbService,
    private dateTime: AppDateTimeAdapter
  ) {
    this.stateService
      .selectSiteId()
      .pipe(untilDestroyed(this))
      .subscribe((id) => {
        this.siteId = id;
      });
  }

  loadRoutes(siteId?: string): Observable<DynamicRoute[]> {
    if (!siteId) {
      siteId = this.siteId ?? '';
    }
    return this.httpClient.get<DynamicRoute[]>(this.routesUrl + siteId);
  }

  loadModel(name: string, context: any): Observable<any> {
    if (context?.offlineId) {
      return from(this.dbService.table(this.offlineTable).where('id').equals(context.offlineId).first())?.pipe(
        map((data: any) => data.model)
      );
    } else {
      const paramsArray = [`name=${name}`, `siteId=${this.siteId}`];
      if (context) {
        Object.entries(context).forEach(([key, value]) => {
          if (this.allowedContextParams.indexOf(key) !== -1) {
            paramsArray.push(`${key}=${value}`);
          }
        });
      }
      const url = `${this.mainUrl}?${paramsArray.join('&')}`;

      return this.httpClient.get<any>(url);
    }
  }

  loadModelOffline(name: string, context: any): Observable<any> {
    return this.loadModel(name, context).pipe(
      catchError(() => {
        throw new Error(`The model for ${name} has not been cached`);
      })
    );
  }

  postForm(name: string, model: any): Observable<DynamicRoutePostResult> {
    return this.httpClient.post<DynamicRoutePostResult>(`${this.mainUrl}?siteId=${this.siteId}&name=${name}`, model);
  }

  postFormOffline(model: any, context: any, routeId: string): Observable<any> {
    const offlineId = context.offlineId ?? '';
    const savedModel = { ...model, isSavedOffline: true };

    // Entites that are related to the main model that may need to be created
    this.saveOfflineCreateableEntities(model);

    if (routeId.includes('.Add')) {
      return from(
        this.dbService
          .table(this.offlineTable)
          .add({ id: model.tempId ?? uuidv4(), model: savedModel, routeId, dateSaved: this.dateTime.today() })
      ).pipe(map(() => model.offlineConfig ?? {}));
    }

    if (routeId.includes('.Edit') && offlineId) {
      return from(
        this.dbService
          .table(this.offlineTable)
          .update(offlineId, { model: savedModel, dateSaved: this.dateTime.today() })
      ).pipe(map(() => model.offlineConfig ?? {}));
    }

    return EMPTY;
  }

  private saveOfflineCreateableEntities(model: any): void {
    if (!model) {
      return;
    }

    const creatable: CreatableEntity[] = model.offlineConfig?.creatableEntities;

    if (creatable?.length > 0) {
      creatable.forEach((config: CreatableEntity) => {
        if (config) {
          const entity = config.path ? utils.resolveObjectPath(config.path, model) : null;
          const id = entity?.[config.idProp];
          const displayName = entity?.[config.displayNameProp];
          const type = config.type ?? '';
          if (id && displayName) {
            this.dbService.table('offlineCreatableEntities').put({ id, displayName, type }, id);
          }
        }
      });
    }
  }
}

interface CreatableEntity {
  path: string;
  idProp: string;
  displayNameProp: string;
  type: string;
}
