import { Injectable } from '@angular/core';
import { Dictionary } from '@ngrx/entity';
import { Store } from '@ngrx/store';
import {
  ApplicationInsightsConfig,
  CardOptions,
  DataListOptions,
  DebugOptions,
  DialogFormAction,
  DynamicRoute,
  FileUploadConfig,
  FormDefinition,
  LookupDetail,
  LookupDictionary,
  LookupHierarchy,
  LookupNode,
  MetaDataColumnMatch,
  ScheduledJob,
  selectClone,
  StateService,
  Theme,
  User,
  SignalRHub,
  MetaDataItem,
  MetaDataKey,
  Lookup,
  Translations,
} from '@nimbus/core/src/lib/core';

import { TimeZone } from '@nimbus/material';
import { Observable } from 'rxjs';
import * as fromAuth from '../../auth/reducers';
import * as fromRoot from '../../reducers';
import { AppConfigurationActions, DynamicRouteActions, FormActions, LookupActions, StatusActions } from '../actions';

@Injectable({
  providedIn: 'root',
})
export class AppStateService extends StateService {
  private readonly notImplemented = 'Not implemented in Phaneros.';

  constructor(private store: Store<fromRoot.State & fromAuth.State>) {
    super();
  }

  // Selectors
  selectDataListOptions(savedOptionsId: string): Observable<DataListOptions> {
    return this.store.select(fromRoot.selectDataListOptions(savedOptionsId));
  }

  selectUser(): Observable<User | null> {
    return this.store.select(fromAuth.selectUser);
  }

  selectForm(formId: string): Observable<FormDefinition | undefined> {
    return this.store.pipe(selectClone(fromRoot.selectForm(formId)));
  }

  selectDateTimeFormats(): Observable<{ dateFormat?: string; timeFormat?: string; timeZoneId?: string }> {
    return this.store.select(fromAuth.selectDateTimeFormats);
  }

  selectTimeZones(): Observable<TimeZone[] | null> {
    return this.store.select(fromRoot.selectTimeZones);
  }

  selectLookupDetailEntities(): Observable<Dictionary<LookupDetail>> {
    return this.store.select(fromRoot.selectLookupDetailEntities);
  }

  selectLookupsHierarchy(): Observable<LookupHierarchy> {
    return this.store.select(fromRoot.selectLookupsHierarchy);
  }

  selectLookupsFlat(): Observable<LookupDictionary> {
    return this.store.select(fromRoot.selectLookupsFlat);
  }

  selectLookupsFlatFiltered(filters: string[]): Observable<Lookup[]> {
    return this.store.select(fromRoot.selectLookupsFlatFiltered(filters));
  }

  selectLookupsHierarchyFiltered(filters: string[]): Observable<LookupNode[]> {
    return this.store.select(fromRoot.selectLookupsHierarchyFiltered(filters));
  }

  selectLookupsForList(filters: string[]): Observable<LookupNode[]> {
    return this.store.select(fromRoot.selectLookupsForList(filters));
  }

  selectThemes(): Observable<Theme[] | undefined> {
    return this.store.select(fromRoot.selectThemes);
  }

  selectSelectedThemeIsDark(): Observable<boolean | undefined> {
    return this.store.select(fromRoot.selectSelectedThemeIsDark);
  }

  selectCurrentDynamicForm(): Observable<FormDefinition | undefined> {
    return this.store.pipe(selectClone(fromRoot.selectCurrentDynamicForm));
  }

  selectDynamicRouteCurrentModel(): Observable<any> {
    return this.store.pipe(selectClone(fromRoot.selectDynamicRouteCurrentModel));
  }

  selectDynamicRouteCurrentError(): Observable<string | null> {
    return this.store.pipe(selectClone(fromRoot.selectDynamicRouteCurrentError));
  }

  selectCurrentDynamicRoute(): Observable<DynamicRoute | undefined> {
    return this.store.select(fromRoot.selectCurrentDynamicRoute);
  }

  selectFormSubmissionResult(): Observable<{ success: boolean; errorMessage: string | null } | null> {
    return this.store.select(fromRoot.selectFormSubmissionResult);
  }

  selectCurrentRoute(): Observable<any> {
    return this.store.select(fromRoot.selectCurrentRoute);
  }

  selectDebugOptions(): Observable<DebugOptions | undefined> {
    return this.store.select(fromRoot.selectDebugOptions);
  }

  selectFileUploadConfig(): Observable<FileUploadConfig | undefined> {
    return this.store.select(fromRoot.selectFileUploadConfig);
  }

  selectSignalRConfig(): Observable<SignalRHub[] | undefined> {
    return this.store.select(fromRoot.selectSignalRConfig);
  }

  selectFormStateSettings(): Observable<{ [key: string]: any } | undefined> {
    return this.store.select(fromRoot.selectFormStateSettings);
  }

  selectAppInsightsConfig(): Observable<ApplicationInsightsConfig | undefined> {
    return this.store.select(fromRoot.selectAppInsightsConfig);
  }

  selectDashboardLayout(): Observable<string> {
    throw new Error(this.notImplemented);
  }

  selectMenuState(): Observable<'open' | 'closed'> {
    return this.store.select(fromRoot.selectOpenSidenav);
  }

  selectCurrentBreakpoint(): Observable<'sm' | 'md' | 'lg'> {
    return this.store.select(fromRoot.selectBreakpoint);
  }

  selectRoutesWithPermission(routes: any[]) {
    return this.store.select(fromRoot.selectRoutesWithPermission(routes));
  }

  selectConnectionOnline(): Observable<boolean> {
    return this.store.select(fromRoot.selectConnectionOnline);
  }

  selectWorkOnline(): Observable<boolean> {
    return this.store.select(fromRoot.selectWorkOnline);
  }

  selectSiteId(): Observable<string | undefined> {
    return this.store.select(fromRoot.selectSiteId);
  }

  selectScheduledJobs(): Observable<ScheduledJob[]> {
    return this.store.select(fromRoot.selectScheduledJobs);
  }

  selectScheduledJob(id: string): Observable<ScheduledJob | undefined> {
    return this.store.select(fromRoot.selectScheduledJob(id));
  }

  selectUpgradeConfigMessage(): Observable<string | undefined> {
    return this.store.select(fromRoot.selectUpgradeConfigMessage);
  }

  selectMetaDataKeyColumns(listColumns: string[], entityType: string): Observable<MetaDataColumnMatch[]> {
    return this.store.select(fromRoot.selectMetaDataKeyColumns(listColumns, entityType));
  }

  selectMetaDateKeyDetails(): Observable<MetaDataKey[]> {
    return this.store.select(fromRoot.selectMetaDataKeyDetails);
  }

  selectDefaultMetaDataByEntityTypeId(id: number): Observable<MetaDataItem[]> {
    return this.store.select(fromRoot.selectDefaultMetaDataByEntityTypeId(id));
  }

  selectAllTranslations(): Observable<Translations> {
    return this.store.select(fromRoot.selectAllTranslations);
  }

  selectEnableFormTranslations(): Observable<boolean | undefined> {
    return this.store.select(fromRoot.selectEnableFormTranslations);
  }

  // Actions
  mergeDynamicRouteModel(
    model: any,
    routeId: string,
    routeParams: { [x: string]: any },
    autoFillModelOverrides: any[] | undefined,
    autoFillModelSkipProps: any[] | undefined
  ): void {
    this.store.dispatch(
      DynamicRouteActions.mergeDynamicRouteModel({
        model,
        routeId,
        routeParams,
        autoFillModelOverrides,
        autoFillModelSkipProps,
      })
    );
  }

  updateDynamicRouteModel(model: unknown): void {
    this.store.dispatch(DynamicRouteActions.loadCurrentDynamicRouteModelSuccess({ model: model }));
  }

  saveOptions(options: DataListOptions): void {
    this.store.dispatch(DynamicRouteActions.saveDataListOptions({ options }));
  }

  saveCardOptions(options: CardOptions): void {
    this.store.dispatch(DynamicRouteActions.saveCardOptions({ options }));
  }

  saveBookmark(routeId: string, bookmark: any): void {
    this.store.dispatch(DynamicRouteActions.saveBookmark({ routeId, bookmark }));
  }

  setLoading(loading: boolean): void {
    this.store.dispatch(StatusActions.setLoading({ loading }));
  }

  loadCurrentDynamicRouteModel(routeId: string, context: any): void {
    this.store.dispatch(DynamicRouteActions.loadCurrentDynamicRouteModel({ routeId, context }));
  }

  loadCurrentDynamicRouteModelOffline(routeId: string, context: any): void {
    this.store.dispatch(DynamicRouteActions.loadCurrentDynamicRouteModelOffline({ routeId, context }));
  }

  postForm(model: any, routeId: string, onSuccess?: () => void, showErrorSnack?: boolean): void {
    this.store.dispatch(DynamicRouteActions.postForm({ model, routeId, onSuccess, showErrorSnack }));
  }

  postFormOffline(model: any, context: any, routeId: string): void {
    this.store.dispatch(DynamicRouteActions.postFormOffline({ model, context, routeId }));
  }

  clearCurrentDynamicRouteModel(): void {
    this.store.dispatch(DynamicRouteActions.clearCurrentDynamicRouteModel());
  }

  addLookup(lookup: LookupDetail): void {
    this.store.dispatch(LookupActions.addLookup({ lookup }));
  }

  submitFormClear(): void {
    this.store.dispatch(FormActions.submitClear());
  }

  submitForm(model: any, formAction: DialogFormAction, formId: string, context: any): void {
    this.store.dispatch(FormActions.submit({ model, formAction, formId, context }));
  }

  setDashboardLayout(dashboardLayout: string): void {
    throw new Error(this.notImplemented);
  }

  setTheme(themeId: string): void {
    this.store.dispatch(AppConfigurationActions.setTheme({ themeId }));
  }

  upgradeConfig(): void {
    this.store.dispatch(AppConfigurationActions.upgradeConfig());
  }
}
