import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import { AppConfigurationActions } from '../actions';

@Injectable({
  providedIn: 'root',
})
export class MetaDataRefreshService {
  private intervalTimer: any;
  constructor(private store: Store<fromRoot.State>) {}

  initialize(interval: number) {
    this.intervalTimer = setInterval(() => {
      this.store.dispatch(AppConfigurationActions.loadMetadataCacheKeys());
    }, interval);
  }
}
