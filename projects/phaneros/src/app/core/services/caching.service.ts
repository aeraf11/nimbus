import { HttpClient } from '@angular/common/http';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import { StateService } from '@nimbus/core/src/lib/core';

@UntilDestroy()
@Injectable({
  providedIn: 'root',
})
export class CachingService {
  private siteId?: string;

  constructor(
    private store: Store<fromRoot.State>,
    private stateService: StateService,
    private httpClient: HttpClient
  ) {
    this.stateService
      .selectSiteId()
      .pipe(untilDestroyed(this))
      .subscribe((id) => {
        this.siteId = id;
      });
  }

  loadConfiguredRoutes(): void {
    this.store
      .select(fromRoot.selectCachedRoutesConfig)
      .pipe(untilDestroyed(this))
      .subscribe((urls) => {
        if (urls?.length) {
          urls?.forEach((url) => {
            this.getResponse(url).pipe(take(1)).subscribe();
          });
        }
      });
  }

  getResponse(url: string): Observable<unknown> {
    return this.httpClient.get<unknown>(url.replace('{siteId}', this.siteId + ''));
  }
}
