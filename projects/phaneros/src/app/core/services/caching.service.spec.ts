import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { StateService } from '@nimbus/core/src/lib/core';
// eslint-disable-next-line no-restricted-imports
import { MockStateService } from '@nimbus/core/src/lib/core';

import { CachingService } from './caching.service';

describe('CachingService', () => {
  let service: CachingService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        provideMockStore({ initialState: {} }),
        {
          provide: StateService,
          useClass: MockStateService,
        },
      ],
    });
    service = TestBed.inject(CachingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
