import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import {
  AppConfiguration,
  ConfigurationService,
  LookupDataService,
  LookupDetail,
  StateService,
} from '@nimbus/core/src/lib/core';
import { TimeZone } from '@nimbus/material';
import { Observable, of } from 'rxjs';
import { MockStateService } from '@nimbus/core/src/lib/core';
import { InitializerService } from './initializer.service';

class MockConfigurationService {
  getAppConfig(): Observable<AppConfiguration> {
    return <any>of({});
  }

  getTimeZones(): Observable<TimeZone[]> {
    return of([]);
  }
}

class MockLookupDataService {
  loadLookups(): Observable<LookupDetail[]> {
    return of([]);
  }
}

describe('InitializerService', () => {
  let service: InitializerService;
  const initialState = { auth: { details: { user: {} } } };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: ConfigurationService,
          useClass: MockConfigurationService,
        },
        {
          provide: LookupDataService,
          useClass: MockLookupDataService,
        },
        {
          provide: StateService,
          useClass: MockStateService,
        },
        provideMockStore({ initialState }),
      ],
    });

    service = TestBed.inject(InitializerService);
  });

  it('should be created', () => {
    service.initialize().subscribe(() => {
      expect().nothing;
    });
  });
});
