import { animate, state, style, transition, trigger } from '@angular/animations';
import { ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { OnChange } from 'property-watch-decorator';
import * as fromRoot from '../../../reducers';
import { LayoutActions } from '../../actions';
import { MenuItem } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ph-menu-list-item',
  templateUrl: './menu-list-item.component.html',
  styleUrls: ['./menu-list-item.component.scss'],
  animations: [
    trigger('indicatorRotate', [
      state('collapsed', style({ transform: 'rotate(-90deg)' })),
      state('expanded', style({ transform: 'rotate(0deg)' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4,0.0,0.2,1)')),
    ]),
    trigger('toggleChild', [
      state('collapsed', style({ 'max-height': '0' })),
      state('expanded', style({ 'max-height': 'auto' })),
      transition('expanded => collapsed', [
        style({ 'max-height': '500px' }),
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)', style({ 'max-height': '0' })),
      ]),
      transition('collapsed => expanded', [
        style({ 'max-height': '0' }),
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)', style({ 'max-height': '500px' })),
      ]),
    ]),
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuListItemComponent {
  active!: boolean;

  @OnChange<string>(function (this: MenuListItemComponent, value) {
    const match = this.routeMatch(this.item, value);
    if ((match.childMatch || match.match) && this.item.items && this.item.items.length) this.expanded = true;
    this.active = match.match;
  })
  @Input()
  routeId!: string | null;

  @Input() item!: MenuItem;
  @Input() depth!: number;
  @Input() menuState: 'open' | 'closed' | 'hover' = 'closed';
  @Input() expanded = false;
  @Output() itemSelected = new EventEmitter<MenuItem>();

  @HostBinding('attr.aria-expanded') ariaExpanded = this.expanded;
  @HostBinding('class.menu-list-item') hostClass = true;
  @HostBinding('class.child') @Input() child = false;

  constructor(public router: Router, private store: Store<fromRoot.State>) {}

  onItemSelected(item: MenuItem) {
    if (item.items && item.items.length) {
      this.expanded = !this.expanded;
      if (this.menuState === 'closed') {
        this.store.dispatch(LayoutActions.openMenu());
        this.expanded = true;
      }
    } else {
      this.itemSelected.emit(item);
    }
  }

  routeMatch(item: MenuItem, routeId: string): { match: boolean; childMatch: boolean } {
    if (item.routeId && routeId && item.routeId === routeId) return { match: true, childMatch: false };
    if (item.items) {
      const childMatch = item.items.some((child) => {
        const result = this.routeMatch(child, routeId);
        return result.match || result.childMatch;
      });
      if (childMatch) return { match: false, childMatch };
    }
    return { match: false, childMatch: false };
  }
}
