import { MockStateService } from '@nimbus/core/src/lib/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { IconDirective, UserPermissionsPipe, StateService } from '@nimbus/core/src/lib/core';
import { ThemeMenuComponent } from '../theme-menu/theme-menu.component';
import { HeaderBarComponent } from './header-bar.component';

describe('HeaderBarComponent', () => {
  let component: HeaderBarComponent;
  let fixture: ComponentFixture<HeaderBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderBarComponent, ThemeMenuComponent, IconDirective, UserPermissionsPipe],
      imports: [MatMenuModule, MatToolbarModule, MatIconModule],
      providers: [{ provide: StateService, useClass: MockStateService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
