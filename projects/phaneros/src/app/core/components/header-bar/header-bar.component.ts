import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MenuItem, Theme, User } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ph-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss'],
})
export class HeaderBarComponent {
  @Input() menuState: 'open' | 'closed' | 'hover' = 'closed';
  @Input() online: boolean | null = false;
  @Input() workOnline: boolean | null = false;
  @Input() user: User | null = null;
  @Input() menuItems: MenuItem[] | null = [];
  @Input() themes: Theme[] | null = [];
  @Output() themeChange = new EventEmitter<string>();
  @Output() menuStateToggled = new EventEmitter<'open' | 'closed' | 'hover'>();
  @Output() setWorkOnlineStatus = new EventEmitter<boolean>();

  onlineWifiIcon = 'fas fa-wifi';
  offlineWifiIcon = 'fas fa-wifi-slash';
  menuLabel = 'Toggle side menu';

  onThemeChange(themeId: string) {
    this.themeChange.emit(themeId);
  }

  onMenuButtonClicked() {
    this.menuStateToggled.emit(this.menuState);
  }

  setOnlinePreference() {
    if (this.workOnline != null) {
      this.setWorkOnlineStatus.emit(false);
    }
  }

  setOfflinePreference() {
    if (this.workOnline != null) {
      this.setWorkOnlineStatus.emit(true);
    }
  }
}
