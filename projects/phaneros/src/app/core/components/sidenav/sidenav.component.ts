import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { MenuItem } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ph-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SidenavComponent {
  @Input() items: MenuItem[] | null = [];
}
