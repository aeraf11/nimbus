import { Component, ChangeDetectionStrategy, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { MenuItem } from '@nimbus/core/src/lib/core';

@Component({
  selector: 'ph-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuComponent {
  @Input() items: MenuItem[] | null = [];

  @Output() logoutClicked = new EventEmitter();

  @ViewChild('menu') public menu: any;
}
