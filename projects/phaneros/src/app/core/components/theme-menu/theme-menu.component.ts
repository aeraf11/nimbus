import { Component, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { Theme } from '@nimbus/core/src/lib/core';

type NewType = EventEmitter<string>;

@Component({
  selector: 'ph-theme-menu',
  templateUrl: './theme-menu.component.html',
  styleUrls: ['./theme-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ThemeMenuComponent {
  @Input() themes: Theme[] | null = [];
  @Output() themeChange = new EventEmitter<string>();

  onThemeChange(themeId: string) {
    this.themeChange.emit(themeId);
  }
}
