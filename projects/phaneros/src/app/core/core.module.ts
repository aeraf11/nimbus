import { AppStateService } from './services/app-state.service';
import { Store } from '@ngrx/store';
import { Platform } from '@angular/cdk/platform';
import {
  AppComponent,
  HomeComponent,
  LogoutComponent,
  NotFoundPageComponent,
  UnauthorisedComponent,
  LoggedOutComponent,
} from './containers';
import {
  HeaderBarComponent,
  MenuComponent,
  ThemeMenuComponent,
  SidenavComponent,
  MenuListItemComponent,
} from './components';

import { CommonModule } from '@angular/common';
import { FormlyModule, FORMLY_CONFIG } from '@ngx-formly/core';
import { FormlySelectModule } from '@ngx-formly/core/select';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BREAKPOINT, FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldDefaultOptions, MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { DateTimeAdapter } from '@nimbus/material';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { registerExtensions, ENABLE_DYNAMIC_ROUTE_PAGE_COMPONENT } from '@nimbus/core/src/lib/apto';
import { AppDateTimeAdapter, StateService, SiteConfig, FormsHelperService } from '@nimbus/core/src/lib/core';
import { FORMLY_CONFIG as PH_FORMLY_CONFIG } from './formly/formly-config';
import { CoreModule as NimbusCoreModule } from '@nimbus/core/src/lib/core';
import { AptoModule } from '@nimbus/core/src/lib/apto';
import { environment } from '../../environments/environment';

export const COMPONENTS = [
  AppComponent,
  NotFoundPageComponent,
  UnauthorisedComponent,
  HomeComponent,
  HeaderBarComponent,
  LogoutComponent,
  MenuComponent,
  ThemeMenuComponent,
  SidenavComponent,
  MenuListItemComponent,
  LoggedOutComponent,
];

export function SiteConfigFactory() {
  return new SiteConfig(environment.apiUrl, environment.initUrl);
}

const appearance: MatFormFieldDefaultOptions = {
  appearance: 'outline',
};

const dialogDefaultOptions = {
  hasBackdrop: true,
  disableClose: true,
  maxWidth: '90vw',
  maxHeight: '90vh',
  minWidth: '30vw',
  closeOnNavigation: true,
};

const PHANEROS_BREAKPOINTS = [
  { alias: 'xs', mediaQuery: 'screen and (max-width: 599px)' },
  { alias: 'sm', mediaQuery: 'screen and (min-width: 600px) and (max-width: 959px)' },
  { alias: 'md', mediaQuery: 'screen and (min-width: 960px) and (max-width: 1279px)' },
  { alias: 'lg', mediaQuery: 'screen and (min-width: 1280px) and (min-width: 1919px) ' },
  { alias: 'xl', mediaQuery: 'screen and (min-width: 1920px) and (max-width: 5000px)' },

  { alias: 'lt-sm', mediaQuery: 'screen and (max-width: 599px)' },
  { alias: 'lt-md', mediaQuery: 'screen and (max-width: 959px)' },
  { alias: 'lt-lg', mediaQuery: 'screen and (max-width: 1279px)' },
  { alias: 'lt-xl', mediaQuery: 'screen and (max-width: 1919px)' },

  { alias: 'gt-xs', mediaQuery: 'screen and (min-width: 600px)' },
  { alias: 'gt-sm', mediaQuery: 'screen and (min-width: 960px)' },
  { alias: 'gt-md', mediaQuery: 'screen and (min-width: 1280px)' },
  { alias: 'gt-lg', mediaQuery: 'screen and (min-width: 1920px)' },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormlyModule.forRoot(PH_FORMLY_CONFIG),
    FormlySelectModule,
    FormlyMaterialModule,
    FlexLayoutModule,
    NimbusCoreModule,
    AptoModule,
  ],
  declarations: COMPONENTS,
  exports: [...COMPONENTS],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: appearance,
    },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: dialogDefaultOptions,
    },
    {
      provide: DateTimeAdapter,
      useClass: AppDateTimeAdapter,
      deps: [MAT_DATE_LOCALE, Platform, StateService],
    },
    {
      provide: DateAdapter,
      useClass: AppDateTimeAdapter,
      deps: [MAT_DATE_LOCALE, Platform, StateService],
    },
    {
      provide: BREAKPOINT,
      useValue: PHANEROS_BREAKPOINTS,
      multi: true,
    },
    {
      provide: FORMLY_CONFIG,
      multi: true,
      useFactory: registerExtensions,
      deps: [FormsHelperService, StateService],
    },
    {
      provide: SiteConfig,
      useFactory: SiteConfigFactory,
      multi: false,
    },
    {
      provide: ENABLE_DYNAMIC_ROUTE_PAGE_COMPONENT,
      useValue: true,
    },
    {
      provide: StateService,
      useClass: AppStateService,
      deps: [Store],
    },
  ],
})
export class CoreModule {}
