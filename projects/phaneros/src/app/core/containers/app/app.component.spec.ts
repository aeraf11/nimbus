import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { MatSidenavModule } from '@angular/material/sidenav';
import { RouterTestingModule } from '@angular/router/testing';
import { ServiceWorkerModule } from '@angular/service-worker';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { DialogService, SiteConfig, StateService } from '@nimbus/core/src/lib/core';
// eslint-disable-next-line no-restricted-imports
import { MockStateService } from '@nimbus/core/src/lib/core';
import { AuthModule, StsConfigLoader } from 'angular-auth-oidc-client';
import { MockDialogService } from '@nimbus/core/src/lib/core';
import { httpLoaderFactory } from '../../../auth/auth-config.module';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [
        RouterTestingModule,
        ServiceWorkerModule.register('', { enabled: false }),
        MatSidenavModule,
        HttpClientTestingModule,
        AuthModule.forRoot({
          loader: {
            provide: StsConfigLoader,
            useFactory: httpLoaderFactory,
            deps: [MockStore],
          },
        }),
      ],
      providers: [
        provideMockStore({ initialState: {} }),
        { provide: StateService, useClass: MockStateService },
        { provide: DialogService, useClass: MockDialogService },
        { provide: SiteConfig, useValue: new SiteConfig('someApiUrl', 'someInitUrl') },
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
