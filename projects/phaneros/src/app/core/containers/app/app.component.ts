import { animate, animateChild, group, query, state, style, transition, trigger } from '@angular/animations';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { Router } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Store } from '@ngrx/store';
import { ScheduledJobService } from '@nimbus/core/src/lib/apto';
import { MediaService, MenuItem, SwUpdatesService } from '@nimbus/core/src/lib/core';
import { EventTypes, PublicEventsService } from 'angular-auth-oidc-client';
import { fromEvent, merge, Observable, of } from 'rxjs';
import { distinctUntilChanged, filter, map, switchMap, take, tap } from 'rxjs/operators';
import { AuthActions } from '../../../auth/actions';
import * as fromAuth from '../../../auth/reducers';
import { AuthService } from '../../../auth/services/auth.service';
import * as fromRoot from '../../../reducers';
import { AppConfigurationActions, LayoutActions, StatusActions } from '../../actions';
import { CachingService } from './../../services/caching.service';

@UntilDestroy()
@Component({
  selector: 'ph-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('menuToggle', [
      state(
        'open',
        style({
          width: '280px',
        })
      ),
      state(
        'closed',
        style({
          width: '56px',
        })
      ),
      transition('closed => open', [
        style({
          width: '56px',
        }),
        group([
          animate(
            '225ms cubic-bezier(0.4,0.0,0.2,1)',
            style({
              width: '280px',
            })
          ),
          query('@toggleChild', [animateChild()], { optional: true }),
        ]),
      ]),
      transition('open => closed', [
        style({
          width: '280px',
        }),
        group([
          animate(
            '225ms cubic-bezier(0.4,0.0,0.2,1)',
            style({
              width: '56px',
            })
          ),
          query('@toggleChild', [animateChild()], { optional: true }),
        ]),
      ]),
    ]),
  ],
})
export class AppComponent implements OnInit {
  menuState: 'open' | 'closed' | 'hover' = 'closed';
  loading = false;
  showToolbars$ = this.store.select(fromRoot.selectShowToolbars);
  online$ = this.store.select(fromRoot.selectConnectionOnline);
  workOnline$ = this.store.select(fromRoot.selectWorkOnline);
  contentClasses$ = this.store.select(fromRoot.selectContentClass);
  contentClass$: Observable<string>;
  user$ = this.store.select(fromAuth.selectUser);
  menuItems$ = this.store.select(fromRoot.selectMenuItems);
  themes$ = this.store.select(fromRoot.selectThemes);
  routeId$ = this.store.select(fromRoot.selectRouteId);
  wide$ = this.mediaObserver.asObservable().pipe(
    map((media) => {
      const match = media.find((m) => m.mqAlias === 'gt-md');
      return !!match;
    })
  );

  onlineWifiIcon = 'fas fa-wifi';
  offlineWifiIcon = 'fas fa-wifi-slash';
  workOnlineLabel = 'Switch to work ';

  constructor(
    private store: Store<fromRoot.State & fromAuth.State>,
    private location: Location,
    private router: Router,
    public mediaObserver: MediaObserver,
    public mediaService: MediaService,
    private swUpdate: SwUpdatesService,
    private scheduledJobs: ScheduledJobService,
    private cachingService: CachingService,
    private authService: AuthService,
    private eventService: PublicEventsService
  ) {
    this.contentClass$ = this.contentClasses$.pipe(switchMap((classes) => mediaService.getBreakpointValue(classes)));
    store
      .select(fromRoot.selectOpenSidenav)
      .pipe(untilDestroyed(this))
      .subscribe((state: any) => (this.menuState = state));

    store
      .select(fromRoot.selectLoading)
      .pipe(untilDestroyed(this))
      .subscribe(async (loading: any) => {
        this.loading = await loading;
      });

    this.cachingService.loadConfiguredRoutes();
  }

  ngOnInit() {
    const path = this.location.path();

    this.eventService
      .registerForEvents()
      .pipe(filter((notification) => notification.type === EventTypes.NewAuthenticationResult))
      .subscribe(() => {
        this.authService.token.pipe(take(1)).subscribe((token) => {
          this.store.dispatch(AuthActions.authTokenReceived({ token: token }));
        });
      });

    if (path !== '/loggedout') this.store.dispatch(AuthActions.autoLogin({ redirect: path }));

    merge(of(null), fromEvent(window, 'online'), fromEvent(window, 'offline'))
      .pipe(
        untilDestroyed(this),
        map(() => navigator.onLine)
      )
      .subscribe((connectionOnline) => this.store.dispatch(StatusActions.updateConnectionStatus({ connectionOnline })));
    this.store
      .select(fromRoot.selectServiceWorkerConfig)
      .pipe(untilDestroyed(this))
      .subscribe((serviceWorkerConfig) => this.swUpdate.start(serviceWorkerConfig));

    this.listenToBreakpoint();
    this.scheduledJobs.init();
  }

  onThemeChange(themeId: string) {
    this.store.dispatch(AppConfigurationActions.setTheme({ themeId }));
  }

  onMenuButtonClicked(currentState: 'open' | 'closed' | 'hover') {
    if (currentState === 'closed') {
      this.store.dispatch(LayoutActions.openMenu());
    } else {
      this.store.dispatch(LayoutActions.closeMenu());
    }
  }

  setWorkOnlineStatus(status: boolean) {
    status ? this.store.dispatch(StatusActions.setWorkOnline()) : this.store.dispatch(StatusActions.setWorkOffline());
  }

  onMenuItemSelected(menuItem: MenuItem, closeSideNav: boolean) {
    if (closeSideNav) this.store.dispatch(LayoutActions.closeMenu());
    if (menuItem.route) this.router.navigateByUrl(menuItem.route);
  }

  onSideBarClosedStart() {
    this.store.dispatch(LayoutActions.closeMenu());
  }

  private listenToBreakpoint(): void {
    this.mediaObserver
      .asObservable()
      .pipe(
        distinctUntilChanged(),
        tap((media: MediaChange[]) => {
          if (media.find((m) => m.mqAlias === 'lt-md')) {
            this.store.dispatch(LayoutActions.setBreakpoint({ breakpoint: 'sm' }));
          }
          if (media.find((m) => m.mqAlias === 'md')) {
            this.store.dispatch(LayoutActions.setBreakpoint({ breakpoint: 'md' }));
          }
          if (media.find((m) => m.mqAlias === 'gt-md')) {
            this.store.dispatch(LayoutActions.setBreakpoint({ breakpoint: 'lg' }));
          }
        })
      )
      .subscribe();
  }
}
