import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthActions } from '../../../auth/actions';
import * as fromAuth from '../../../auth/reducers';

@Component({
  selector: 'ph-logged-out',
  templateUrl: './logged-out.component.html',
  styleUrls: ['./logged-out.component.scss'],
})
export class LoggedOutComponent {
  constructor(private store: Store<fromAuth.State>) {}

  login() {
    this.store.dispatch(AuthActions.login());
  }
}
