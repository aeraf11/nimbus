import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthActions } from '../../../auth/actions';
import * as fromAuth from '../../../auth/reducers';

@Component({
  selector: 'ph-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss'],
})
export class LogoutComponent implements OnInit {
  constructor(private store: Store<fromAuth.State>) {}

  ngOnInit(): void {
    this.store.dispatch(AuthActions.logout());
  }
}
