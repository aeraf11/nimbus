import { Component, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as fromRoot from '../../../reducers';
import { HomeItem, User } from '@nimbus/core/src/lib/core';
import * as fromAuth from '../../../auth/reducers';

@Component({
  selector: 'ph-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent {
  user$: Observable<User | null>;
  online$: Observable<boolean>;
  homeItems$: Observable<HomeItem[] | undefined>;

  constructor(private store: Store<fromRoot.State & fromAuth.State>) {
    this.user$ = store.select(fromAuth.selectUser);
    this.online$ = store.select(fromRoot.selectConnectionOnline);
    this.homeItems$ = store.select(fromRoot.selectHomeItems);
  }
}
