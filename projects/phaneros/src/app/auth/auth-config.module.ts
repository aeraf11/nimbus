import { LocalSecurityStorageService } from './services/local-security-storage.service';
import { take, map } from 'rxjs/operators';
import { AuthEffects } from './effects/auth.effects';
import { NgModule } from '@angular/core';
import {
  AbstractSecurityStorage,
  AuthModule,
  LogLevel,
  OpenIdConfiguration,
  StsConfigHttpLoader,
  StsConfigLoader,
} from 'angular-auth-oidc-client';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { Store, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from './effects';
import { UserService } from './services/user-service';
import { HttpClient } from '@angular/common/http';
import * as fromAuth from '../auth/reducers/';
import * as fromRoot from '../reducers';

export function httpLoaderFactory(store: Store<fromRoot.State>) {
  const config$ = store.select(fromRoot.selectOidcConfig).pipe(
    take(1),
    map((oidc) => {
      const config: OpenIdConfiguration = {
        authority: oidc?.stsServer,
        redirectUrl: window.location.origin,
        postLogoutRedirectUri: `${window.location.origin}/loggedout`,
        clientId: oidc?.clientId,
        scope: oidc?.scope,
        responseType: oidc?.responseType,
        silentRenew: oidc?.silentRenew,
        silentRenewUrl: `${window.location.origin}/silent-refresh.html`,
        renewTimeBeforeTokenExpiresInSeconds: oidc?.renewTimeBeforeTokenExpiresInSeconds,
        unauthorizedRoute: '/unauthorised',
        logLevel: oidc?.logLevel ?? LogLevel.None,
      };
      return config;
    })
  );
  return new StsConfigHttpLoader(config$);
}

@NgModule({
  imports: [
    AuthModule.forRoot({
      loader: {
        provide: StsConfigLoader,
        useFactory: httpLoaderFactory,
        deps: [Store],
      },
    }),
    CommonModule,
    AuthRoutingModule,
    StoreModule.forFeature({
      name: fromAuth.authFeatureKey,
      reducer: fromAuth.reducers,
    }),
    EffectsModule.forFeature([UserEffects, AuthEffects]),
  ],
  exports: [AuthModule],
  providers: [
    UserService,
    HttpClient,
    Store,
    {
      provide: AbstractSecurityStorage,
      useClass: LocalSecurityStorageService,
    },
  ],
})
export class AuthConfigModule {}
