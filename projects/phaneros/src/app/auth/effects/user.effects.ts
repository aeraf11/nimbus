import { UserService } from '../services/user-service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UserActions } from '../actions';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { SignalRService } from '@nimbus/core/src/lib/core';

@Injectable()
export class UserEffects {
  loadUserDetails$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UserActions.loadUserDetails),
      mergeMap(({ redirect }) =>
        this.service.getUserDetails().pipe(
          map((user) => UserActions.loadUserDetailsSuccess({ user, redirect })),
          catchError((error) => of(UserActions.loadUserDetailsFailure({ error })))
        )
      )
    );
  });

  loadUserDetailsSuccess$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(UserActions.loadUserDetailsSuccess),
        tap(({ redirect }) => {
          this.router.navigateByUrl(redirect);
        })
      );
    },
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private service: UserService,
    private router: Router,
    private signalr: SignalRService
  ) {}
}
