import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { ConfigurationService, MetaDataItem, MetaDataService, SignalRService, utils } from '@nimbus/core/src/lib/core';
import { CookieService } from 'ngx-cookie-service';
import { of } from 'rxjs';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { AppConfigurationActions, DynamicRouteActions, FormActions, LookupActions } from '../../core/actions';
import { AuthActions, UserActions } from '../actions';
import { AuthService } from '../services/auth.service';
import { InitializerService } from '../../core/services';

const REDIRECT_STORAGE_KEY = 'redirectUrl';
const STOP_STORAGE_KEY = 'stop';

@Injectable()
export class AuthEffects {
  login$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AuthActions.login),
        switchMap(() => this.authService.login())
      );
    },
    { dispatch: false }
  );

  autoLogin$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.autoLogin),
      switchMap(({ redirect }) =>
        this.authService.checkAuth().pipe(
          take(1),
          map(({ isAuthenticated, accessToken }) => {
            //this jiggery pokery with the redirect is necessary because the login actually redirects to auth server and comes back fresh
            if (['/', '/landing'].includes(redirect)) redirect = '/home';
            if (!localStorage.getItem(REDIRECT_STORAGE_KEY) && !redirect.startsWith('?code')) {
              localStorage.setItem(REDIRECT_STORAGE_KEY, redirect);
            }
            if (isAuthenticated) {
              const cookieCheck = this.cookies.get('auth');
              const url = localStorage.getItem(REDIRECT_STORAGE_KEY) || '/home';
              localStorage.removeItem(REDIRECT_STORAGE_KEY);

              if (cookieCheck) {
                return AuthActions.checkAuthComplete({ token: accessToken, redirect: url });
              } else {
                return AuthActions.restoreAuthCookie({ token: accessToken, redirect: url });
              }
            } else {
              const stop = localStorage.getItem(STOP_STORAGE_KEY) === '1';
              if (stop) {
                localStorage.removeItem(STOP_STORAGE_KEY);
                return AuthActions.loginComplete({
                  profile: null,
                  isLoggedIn: false,
                });
              } else {
                localStorage.setItem(STOP_STORAGE_KEY, '1');
                return AuthActions.login();
              }
            }
          })
        )
      )
    );
  });

  loginComplete$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AuthActions.loginComplete),
        tap(({ isLoggedIn }) => {
          if (!isLoggedIn) {
            this.router.navigate(['/loggedout']);
          }
        })
      );
    },
    { dispatch: false }
  );

  checkAuthComplete$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.checkAuthComplete),
      switchMap(() => {
        return this.authService.userData.pipe(
          map((profile) => AuthActions.loginComplete({ profile, isLoggedIn: true }))
        );
      })
    );
  });

  checkAuthCompleteUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.authCompleteLoadDetails),
      switchMap(({ redirect }) => {
        return of(UserActions.loadUserDetails({ redirect }));
      })
    );
  });

  checkAuthCompleteForm$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.checkAuthComplete),
      switchMap(() => {
        return of(FormActions.loadForms());
      })
    );
  });

  setMetaDataCacheKeys$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.checkAuthComplete),
      switchMap(() => {
        return of(AppConfigurationActions.loadMetadataCacheKeys());
      })
    );
  });

  setTimezone$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.checkAuthComplete),
      switchMap(() => {
        return of(AppConfigurationActions.loadTimeZone());
      })
    );
  });

  setPostAuthConfig$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.checkAuthComplete),
      switchMap(({ redirect }) => {
        return this.initializer.getPostAuthConfig().pipe(map(() => AuthActions.authCompleteLoadDetails({ redirect })));
      })
    );
  });

  checkAuthCompleteDefaultMetaData$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.checkAuthComplete),
      switchMap(() => {
        return this.appConfig.getAppConfig().pipe(
          switchMap((config) => this.metaData.getDefaultMetaDataForEntities(config.defaultEntityMetaData ?? [])),
          map((defaultMetaDataJson) => {
            const defaultMetaData: { [key: number]: MetaDataItem[] } = {};
            for (const key in defaultMetaDataJson) {
              defaultMetaData[+key] = utils.parseMetaJson(defaultMetaDataJson[key]);
            }
            return AppConfigurationActions.loadEntityDefaultMetaData({ defaultMetaData });
          })
        );
      })
    );
  });

  checkAuthCompleteSignalR$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AuthActions.checkAuthComplete),
        tap(({ token }) => {
          this.signalR.setAccessToken(token);
        })
      );
    },
    { dispatch: false }
  );

  logout$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.logout),
      switchMap(() =>
        this.authService.logout().pipe(
          map(() => {
            return AuthActions.logoutComplete();
          })
        )
      )
    );
  });

  logoutComplete$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.logoutComplete),
      tap(() => this.router.navigate(['/loggedout'])),
      tap(() => this.cookies.delete('auth')),
      map(() => UserActions.clearUserDetails())
    );
  });

  authTokenReceived$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(AuthActions.authTokenReceived),
        tap(({ token }) => {
          this.setAuthCookie(token);
        })
      );
    },
    { dispatch: false }
  );

  restoreAuthCookie$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.restoreAuthCookie),
      tap(({ token }) => {
        this.setAuthCookie(token);
      }),
      map(({ redirect, token }) => AuthActions.checkAuthComplete({ redirect, token }))
    );
  });

  setAuthCookie = (token: string) => {
    this.cookies.set('auth', `Bearer ${token}`, {
      secure: true,
      sameSite: 'Strict',
      path: '/',
    });
  };

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private router: Router,
    private cookies: CookieService,
    private metaData: MetaDataService,
    private appConfig: ConfigurationService,
    private signalR: SignalRService,
    private initializer: InitializerService
  ) {}
}
