import { AuthService } from './auth.service';
import { Injectable, OnInit } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';
import * as fromAuth from '../../auth/reducers';
import * as fromRoot from '../../reducers';
import { Store } from '@ngrx/store';
import { PermissionLogic } from '@nimbus/core/src/lib/core';
import { AuthActions } from '../actions';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private store: Store<fromAuth.State & fromRoot.State>
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const userPermissions$ = this.store.select(fromAuth.selectPermissions);
    const forceAgencyLookupId$ = this.store.select(fromAuth.selectForceAgencyLookupId);
    const routes$ = this.store.select(fromRoot.selectAllRoutes);
    const online$ = this.store.select(fromRoot.selectWorkOnline);

    //This has to dig into the first child for routes in lazy loaded modules
    const routeId = route.data?.id || route.firstChild?.data?.id;

    return this.authService.isAuthenticated$.pipe(
      withLatestFrom(userPermissions$, routes$, forceAgencyLookupId$, online$),
      map(([{ isAuthenticated }, userPermissions, routes, forceAgencyLookupId, online]) => {
        const routeConfig = routes?.find((r) => r.routeId === routeId);
        if (isAuthenticated && !userPermissions) {
          this.router.navigate(['/landing'], { queryParams: { redirect: routeConfig?.route } });
          return false;
        }
        if (!isAuthenticated) {
          const redirect = routeConfig?.route ?? '/landing';
          this.store.dispatch(AuthActions.autoLogin({ redirect }));
          return false;
        }
        if (!userPermissions || !routeConfig) {
          this.router.navigate(['/unauthorised']);
          return false;
        }
        let result = false;
        switch (routeConfig.permissionLogic || PermissionLogic.All) {
          case PermissionLogic.All:
            result = !routeConfig.permissionKeys
              ? true
              : routeConfig.permissionKeys.every((key) => userPermissions.includes(key));
            break;

          case PermissionLogic.Any:
            result = !routeConfig.permissionKeys
              ? true
              : routeConfig.permissionKeys.some((key) => userPermissions.includes(key));
            break;

          default:
            result = false;
        }

        if (routeConfig.acceptanceGuardConfig) {
          if (!sessionStorage.getItem(routeConfig.acceptanceGuardConfig.storageKey)) {
            const acceptanceRoute = routes?.find(
              (r) => r.routeId === routeConfig.acceptanceGuardConfig.acceptanceRouteId
            );
            if (acceptanceRoute) {
              this.router.navigate([acceptanceRoute.route], {
                queryParams: {
                  storageKey: routeConfig.acceptanceGuardConfig.storageKey,
                  returnUrl: routeConfig.route,
                },
                queryParamsHandling: 'merge',
              });
            } else {
              console.warn(
                `Acceptance route not found for route: ${routeConfig.route}, looking for ${routeConfig.acceptanceGuardConfig.acceptanceRouteId}`
              );
              this.router.navigate(['/unauthorised']);
            }
          } else if (routeConfig.acceptanceGuardConfig.frequency === 'always') {
            sessionStorage.removeItem(routeConfig.acceptanceGuardConfig.storageKey);
          }
        }
        if (routeConfig.forceAgencyPermissions) {
          if (!routeConfig.forceAgencyPermissions.includes(forceAgencyLookupId || '')) {
            result = false;
          }
        }
        if ((online && routeConfig?.showOnline === false) || (!online && routeConfig?.showOffline === false)) {
          result = false;
        }
        if (!result) {
          this.router.navigate(['/unauthorised']);
        }
        return result;
      })
    );
  }
}
