import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { User } from '@nimbus/core/src/lib/core';
import { of } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { UserService } from './user-service';

describe('UserServiceService', () => {
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    service = TestBed.inject(UserService);
  });

  describe('getUserDetails()', () => {
    it('calls the account endpoint', waitForAsync(() => {
      // Arrange
      const http = TestBed.inject(HttpClient);
      const someEndpointUrl = `${environment.apiUrl}/account`;
      spyOn(http, 'get').and.stub();

      // Act
      service.getUserDetails();

      // Assert
      expect(http.get).toHaveBeenCalledWith(someEndpointUrl);
    }));

    it('returns a User object', () => {
      // Arrange
      const dummyUser: User = {
        userProfileId: '',
        fullName: '',
        groupName: '',
        email: '',
        contactNumber: '',
        branch: '',
        permissions: [],
        dateFormat: '',
        timeFormat: '',
        thumbnailBlobId: '',
        initials: '',
        forceAgencyLookupId: '',
        timeZoneId: '',
      };
      spyOn(service, 'getUserDetails').and.callFake(() => of(dummyUser));

      // Act
      const result = service.getUserDetails();

      // Assert
      result.pipe(take(1)).subscribe((user) => expect(user).toEqual(dummyUser));
    });
  });
});
