import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const AuthActions = createActionGroup({
  source: 'Auth',
  events: {
    'Check Auth Complete': props<{ redirect: string; token: string }>(),
    'auth Complete Load Details': props<{ redirect: string }>(),
    Login: emptyProps,
    'Auto Login': props<{ redirect: string }>(),
    'Login Complete': props<{ profile: any; isLoggedIn: boolean }>(),
    Logout: emptyProps,
    'Logout Complete': emptyProps,
    'Auth Token Received': props<{ token: string }>(),
    'Restore Auth Cookie': props<{ token: string; redirect: string }>(),
  },
});
