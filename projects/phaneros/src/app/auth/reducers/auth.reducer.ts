import { createReducer, on } from '@ngrx/store';
import { AuthActions } from '../actions';

export const statusFeatureKey = 'status';

export interface State {
  profile: any | null;
  isLoggedIn: boolean;
  isLoggingIn: boolean;
  loginFailed: boolean;
}

export const initialState: State = {
  profile: null,
  isLoggedIn: false,
  isLoggingIn: false,
  loginFailed: false,
};

export const reducer = createReducer(
  initialState,
  on(AuthActions.login, (state): State => {
    return {
      ...state,
      isLoggingIn: true,
    };
  }),
  on(AuthActions.loginComplete, (state, { profile, isLoggedIn }): State => {
    return {
      ...state,
      profile,
      isLoggedIn,
      isLoggingIn: false,
    };
  }),
  on(AuthActions.logout, (state, {}): State => {
    return {
      ...state,
      profile: null,
      isLoggedIn: false,
    };
  })
);
