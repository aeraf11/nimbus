import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from '../../reducers';
import * as fromUser from './user.reducer';
import * as fromAuth from './auth.reducer';

export const authFeatureKey = 'auth';

export interface AuthState {
  [fromAuth.statusFeatureKey]: fromAuth.State;
  [fromUser.detailsFeatureKey]: fromUser.State;
}

export interface State extends fromRoot.State {
  [authFeatureKey]: AuthState;
}

export function reducers(state: AuthState | undefined, action: Action) {
  return combineReducers({
    [fromAuth.statusFeatureKey]: fromAuth.reducer,
    [fromUser.detailsFeatureKey]: fromUser.reducer,
  })(state, action);
}

export const selectAuthState = createFeatureSelector<AuthState>(authFeatureKey);

export const selectAuthStatusState = createSelector(selectAuthState, (state) => state.status);

export const selectAuthUserState = createSelector(selectAuthState, (state) => state?.details);

export const selectUser = createSelector(selectAuthUserState, fromUser.getUser);

export const selectUserProfileId = createSelector(selectUser, (user) => user?.userProfileId);

export const selectPermissions = createSelector(selectUser, (user) => user?.permissions || null);

export const selectProfile = createSelector(selectAuthStatusState, (state) => state.profile);

export const selectIsLoggedIn = createSelector(selectAuthStatusState, (state) => state.isLoggedIn);

export const selectDateTimeFormats = createSelector(selectUser, (user) => {
  return { dateFormat: user?.dateFormat, timeFormat: user?.timeFormat, timeZoneId: user?.timeZoneId };
});

export const selectForceAgencyLookupId = createSelector(
  selectUser,
  (user) => user?.forceAgencyLookupId?.toUpperCase() || null
);
