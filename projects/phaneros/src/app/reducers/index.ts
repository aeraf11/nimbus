import { selectForceAgencyLookupId, selectPermissions } from '../auth/reducers';
import { Action, ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { InjectionToken } from '@angular/core';
import { environment } from '../../environments/environment';
import * as fromLayout from '../core/reducers/layout.reducer';
import * as fromAppConfig from '../core/reducers/app-configuration.reducer';
import * as fromLookups from '../core/reducers/lookups.reducer';
import * as fromStatus from '../core/reducers/status.reducer';
import * as fromForm from '../core/reducers/form.reducer';
import * as fromDynamicRoutes from '../core/reducers/dynamic-routes.reducer';
import * as fromMetaData from '../core/reducers/meta-data.reducer';
import * as fromTranslations from '../core/reducers/translation.reducer';

export interface State {
  [fromLayout.layoutFeatureKey]: fromLayout.State;
  [fromAppConfig.appConfigurationFeatureKey]: fromAppConfig.State;
  [fromLookups.lookupsFeatureKey]: fromLookups.State;
  [fromMetaData.metaDataFeatureKey]: fromMetaData.State;
  [fromStatus.statusFeatureKey]: fromStatus.State;
  [fromForm.formFeatureKey]: fromForm.State;
  [fromTranslations.translationFeatureKey]: fromTranslations.State;
  [fromDynamicRoutes.dynamicRoutesFeatureKey]: fromDynamicRoutes.State;
  router: fromRouter.RouterReducerState<any>;
}

export const ROOT_REDUCERS = new InjectionToken<ActionReducerMap<State, Action>>('Root reducers token', {
  factory: () => ({
    [fromLayout.layoutFeatureKey]: fromLayout.reducer,
    [fromAppConfig.appConfigurationFeatureKey]: fromAppConfig.reducer,
    [fromLookups.lookupsFeatureKey]: fromLookups.reducer,
    [fromStatus.statusFeatureKey]: fromStatus.reducer,
    [fromMetaData.metaDataFeatureKey]: fromMetaData.reducer,
    [fromForm.formFeatureKey]: fromForm.reducer,
    [fromTranslations.translationFeatureKey]: fromTranslations.reducer,
    [fromDynamicRoutes.dynamicRoutesFeatureKey]: fromDynamicRoutes.reducer,
    router: fromRouter.routerReducer,
  }),
});

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

/**
 * Layout Selectors
 */
export const selectLayoutState = createFeatureSelector<fromLayout.State>(fromLayout.layoutFeatureKey);

export const selectOpenSidenav = createSelector(selectLayoutState, fromLayout.selectOpenSidenav);

export const selectShowToolbars = createSelector(selectLayoutState, fromLayout.selectShowToolbars);

export const selectContentClass = createSelector(selectLayoutState, fromLayout.selectContentClass);

export const selectBreakpoint = createSelector(selectLayoutState, fromLayout.selectBreakpoint);

/**
 * Router Selectors
 */
export const selectRouter = createFeatureSelector<fromRouter.RouterReducerState>('router');
export const { selectRouteData, selectUrl, selectCurrentRoute } = fromRouter.getSelectors(selectRouter);
export const selectRouteId = createSelector(selectRouteData, (data) => data.id);

/**
 * Form Selectors
 */
export const selectFormState = createFeatureSelector<fromForm.State>(fromForm.formFeatureKey);
export const selectFormEntities = createSelector(selectFormState, fromForm.selectFormEntities);
export const selectFormSubmissionResult = createSelector(selectFormState, (state) => state.submissionResult);
export const selectForm = (formId: string) => createSelector(selectFormEntities, (forms) => forms[formId]);

/**
 * Dynamic Route Selectors
 */
export const selectDynamicRouteState = createFeatureSelector<fromDynamicRoutes.State>(
  fromDynamicRoutes.dynamicRoutesFeatureKey
);
export const selectDynamicRouteEntities = createSelector(
  selectDynamicRouteState,
  fromDynamicRoutes.selectDynamicRouteEntities
);
export const selectAllDynamicRoutes = createSelector(selectDynamicRouteState, fromDynamicRoutes.selectAllDynamicRoutes);
export const selectDynamicRoute = (routeId: string) =>
  createSelector(selectDynamicRouteEntities, (routes) => routes[routeId]);
export const selectCurrentDynamicRoute = createSelector(
  selectRouteId,
  selectDynamicRouteEntities,
  (routeId, routes) => routes[routeId]
);
export const selectCurrentDynamicForm = createSelector(selectCurrentDynamicRoute, selectFormEntities, (route, forms) =>
  forms && route ? forms[route?.formId] : undefined
);
export const selectDynamicRouteCurrentModel = createSelector(selectDynamicRouteState, (state) => state.currentModel);
export const selectDynamicRouteCurrentError = createSelector(selectDynamicRouteState, (state) => state.error);
export const selectDataListOptions = (savedOptionsId: string) =>
  createSelector(selectDynamicRouteState, (state) => state.savedOptions[savedOptionsId]);

export const selectRoutesWithPermission = (routes: any[]) =>
  createSelector(
    selectAllRoutes,
    selectPermissions,
    selectForceAgencyLookupId,
    selectWorkOnline,
    (state, permissions, forceAgencyLookupId, online) =>
      fromDynamicRoutes.getRoutesWithPermissions(state, routes, permissions, forceAgencyLookupId, online)
  );

/**
 *  Status Selectors
 */
export const selectStatusState = createFeatureSelector<fromStatus.State>(fromStatus.statusFeatureKey);
export const selectConnectionOnline = createSelector(selectStatusState, fromStatus.selectConnectionOnline);
export const selectLoading = createSelector(selectStatusState, fromStatus.selectLoading);
export const selectWorkOnline = createSelector(selectStatusState, fromStatus.selectWorkOnline);
export const selectConnectionTimeoutPointer = createSelector(
  selectStatusState,
  fromStatus.selectConnectionTimeoutPointer
);

/**
 * Config Selectors
 */
export const selectAppConfigState = createFeatureSelector<fromAppConfig.State>(
  fromAppConfig.appConfigurationFeatureKey
);
export const selectConfig = createSelector(selectAppConfigState, (state) => state.config);
export const selectTimeZones = createSelector(selectAppConfigState, (state) => state?.timeZones);
export const selectOidcConfig = createSelector(selectConfig, (config) => config?.oidc);
export const selectMenuConfig = createSelector(selectConfig, (config) => config?.menuConfig);
export const selectStaticRoutes = createSelector(selectConfig, (config) => config?.staticRoutes);
export const selectAllRoutes = createSelector(selectStaticRoutes, selectAllDynamicRoutes, (statics, dynamics) => [
  ...(statics || []),
  ...(dynamics || []),
]);
export const selectThemes = createSelector(selectConfig, (config) => config?.themeConfig);
export const selectSelectedThemeId = createSelector(selectAppConfigState, (config) => config?.selectedThemeId);
export const selectSelectedTheme = createSelector(selectThemes, selectSelectedThemeId, (themes, themeId) =>
  themes?.find((x) => x.id === themeId)
);
export const selectDefaultThemeId = createSelector(selectThemes, (themes) => (themes ? themes[0].id : undefined));
export const selectSelectedThemeIsDark = createSelector(selectSelectedTheme, (theme) => theme?.isDark);
export const selectEnableFormTranslations = createSelector(selectConfig, (config) => config?.enableFormTranslations);
export const selectHomeConfigs = createSelector(selectConfig, (config) => config?.homeConfig);
export const selectHomeItems = createSelector(
  selectHomeConfigs,
  selectAllRoutes,
  selectPermissions,
  selectForceAgencyLookupId,
  selectWorkOnline,
  fromAppConfig.getHomeItems
);
export const selectMenuItems = createSelector(
  selectMenuConfig,
  selectAllRoutes,
  selectPermissions,
  selectForceAgencyLookupId,
  selectWorkOnline,
  fromAppConfig.getMenuItems
);
export const selectFileUploadConfig = createSelector(selectConfig, (config) => config?.fileUploadConfig);
export const selectSignalRConfig = createSelector(selectConfig, (config) => config?.signalRHubs);
export const selectDebugOptions = createSelector(selectConfig, (config) => config?.debugOptions);
export const selectServiceWorkerConfig = createSelector(selectConfig, (config) => config?.serviceWorkerConfig);
export const selectAppInsightsConfig = createSelector(selectConfig, (config) => config?.applicationInsightsConfig);
export const selectWorkOnlinePromptTimeout = createSelector(selectConfig, (config) => config?.workOnlinePromptTimeout);
export const selectSiteId = createSelector(selectConfig, (config) => config?.siteId);
export const selectScheduledJobs = createSelector(selectConfig, (config) => config?.scheduledJobs ?? []);
export const selectScheduledJob = (id: string) =>
  createSelector(selectConfig, (config) => config?.scheduledJobs?.find((job) => job.id === id));
export const selectCachedRoutesConfig = createSelector(selectConfig, (config) => config?.offlineCachedRoutes);
export const selectUpgradeConfigMessage = createSelector(selectAppConfigState, (config) => config?.upgradeMessage);
export const selectAllDefaultMetaData = createSelector(selectAppConfigState, (config) => config?.defaultMetaData);
export const selectDefaultMetaDataByEntityTypeId = (id: number) =>
  createSelector(selectAllDefaultMetaData, (metaData) => (metaData ? metaData[id] || [] : []));
export const selectFormStateSettings = createSelector(selectConfig, (config) => config?.formStateSettings);

/**
 * Lookup Selectors
 */
export const selectLookupsState = createFeatureSelector<fromLookups.State>(fromLookups.lookupsFeatureKey);
export const selectAllLookupDetails = createSelector(selectLookupsState, fromLookups.selectAllLookupDetails);
export const selectLookupDetailEntities = createSelector(selectLookupsState, fromLookups.selectLookupDetailEntities);

export const selectLookupsHierarchyFiltered = (filters: string[]) =>
  createSelector(selectAllLookupDetails, (lookups: any) => fromLookups.getHierarchicalFiltered(lookups, filters));

export const selectLookupsFlatFiltered = (filters: string[]) =>
  createSelector(selectAllLookupDetails, (lookups: any) => fromLookups.getFlatFiltered(lookups, filters));

export const selectLookupsHierarchy = createSelector(selectAllLookupDetails, fromLookups.getHierarchy);
export const selectLookupsFlat = createSelector(selectAllLookupDetails, fromLookups.getFlat);

export const selectLookupsForList = (filters: string[]) =>
  createSelector(selectAllLookupDetails, (lookups: any) => fromLookups.getHierarchicalForList(lookups, filters));

/**
 * MetaDateKey Selectors
 */
export const selectMetaDataKeyState = createFeatureSelector<fromMetaData.State>(fromMetaData.metaDataFeatureKey);
export const selectMetaDataKeyDetails = createSelector(selectMetaDataKeyState, fromMetaData.selectAllMetaDataDetails);
export const selectMetaDateKeyEntities = createSelector(selectMetaDataKeyState, fromMetaData.selectMetaDataEntities);

export const selectMetaDataKeyColumns = (listColumns: string[], entityTypeId: string) =>
  createSelector(selectMetaDataKeyDetails, (metaDataKeys: any) =>
    fromMetaData.getMetaDataColumnMatches(metaDataKeys, listColumns, entityTypeId)
  );

/**
 * Translation Selectors
 */
export const selectTranslationState = createFeatureSelector<fromTranslations.State>(
  fromTranslations.translationFeatureKey
);

export const selectAllTranslations = createSelector(selectTranslationState, (state) => state.translations);
