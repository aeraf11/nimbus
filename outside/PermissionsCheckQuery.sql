--USE [Jarvis_NCA_Dev]
--GO
--/****** Object:  UserDefinedFunction [perms].[CanViewEntity_Bitwise_TVF]    Script Date: 23/05/2023 13:13:40 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--ALTER FUNCTION [perms].[CanViewEntity_Bitwise_TVF](
	Declare  @entityId            UNIQUEIDENTIFIER = '01A87F8B-CBBB-43AF-9158-79A4977BA8ED'
            , @entityTypeId        INT				= 2
            , @targetUserProfileId UNIQUEIDENTIFIER  = 'CF1D982D-D337-42FE-B8D6-98640D625E14'
            , @applicationId       UNIQUEIDENTIFIER  = 'B4A646A8-10CE-4669-8010-27857D405AE9'
        
		--SELECT TOP 1 visible FROM
  --      (
  --          SELECT CASE
  --          -- USER SPECIFIC PERMISSION
  --          WHEN EXISTS (
  --              select 1 from perms.[UserProfileEntityDenyPermission] as v 
  --              where v.EntityId = @entityId AND v.UserProfileId = @targetUserProfileId
  --              AND  forceCheck.isMatch = 1 
  --          )   THEN -1
  --          WHEN EXISTS (
  --              select 1 from perms.[UserProfileEntityDenyPermission] as v 
  --              where v.EntityId = @entityId AND v.UserProfileId = @targetUserProfileId
  --              AND  pb.ForceB1 is null
  --              AND  pb.ForceB2 is null
  --          )   THEN -2
  --          WHEN forceCheck.isMatch = 1 
  --              AND roleCheck.isMatch = 1 
  --              AND unitCheck.isMatch = 1
  --              AND tppCheck.isMatch = 1
  --              THEN -3
  --          ELSE
  --              1
  --          END
  --              AS visible
  --          from 
  --              perms.UserProfileBitMask up

  --              left join perms.[EntityPermissionBitMask] pb
  --                  on pb.EntityId = @entityId AND pb.IsDeny = 1

  --              CROSS APPLY perms.Match2QWords(up.ForceB1, up.ForceB2, 
  --                                             pb.ForceB1, pb.ForceB2) forceCheck

  --              CROSS APPLY perms.Match6QWords(up.RoleB1, up.RoleB2, up.RoleB3, up.RoleB4, up.RoleB5, up.RoleB6,
  --                                             pb.RoleB1, pb.RoleB2, pb.RoleB3, pb.RoleB4, pb.RoleB5, pb.RoleB6) roleCheck

  --              CROSS APPLY perms.Match2QWords(up.UnitB1, up.UnitB2,
  --                                             pb.UnitB1, pb.UnitB2) unitCheck

  --              CROSS APPLY perms.Match2QWords(up.ThirdPartyProviderB1, up.ThirdPartyProviderB2,
  --                                             pb.ThirdPartyProviderB1, pb.ThirdPartyProviderB2) tppCheck

  --          where
  --              up.UserProfileId = @targetUserProfileId
  --          )  as q
  --          WHERE visible = 0
    --UNION ALL
        SELECT TOP 1 visible FROM
        (
            SELECT CASE
            -- 1 - ENTITY NOT VISIBLE IN APPLICATION?
            -- When @applicationId is null, we don't care about the filter
            -- When the entity doesn't have an EntityVisibilityBitMask row, it's visible everywhere
            WHEN @applicationId IS NOT NULL 
                AND vb.ApplicationB1 IS NOT NULL 
                AND NOT EXISTS(
                    SELECT 0 from perms.ApplicationBit ab
                    CROSS APPLY perms.MatchQWord(ab.B1, vb.ApplicationB1) AS x
                    WHERE ab.ApplicationId = @applicationId
                    AND x.isMatch = 1
                )
                THEN -1
            WHEN @applicationId IS NOT NULL 
                AND vb.ApplicationB1 IS NULL 
                AND tvb.ApplicationB1 IS NOT NULL 
                AND NOT EXISTS(
                    SELECT 0 from perms.ApplicationBit ab
                    CROSS APPLY perms.MatchQWord(ab.B1, tvb.ApplicationB1) AS x
                    WHERE ab.ApplicationId = @applicationId
                    AND x.isMatch = 1
                )
                THEN -2
            -- 2 - IS DCL AND THE USER CAN'T VIEW SENSITIVE?
            WHEN dcl.EntityId IS NOT NULL AND dcl.IsSensitive = 1 AND up.CanViewOfficialSensitive = 0
                THEN -3
            -- 3 - DOES THE USER HAVE VIEW ACCESS TO THIS ENTITY TYPE?
            WHEN pb.EntityId is null AND 
                -- There are NO direct permissions (no point doing check 6)
                NOT EXISTS (
                            select 1 from perms.[UserProfileEntityViewPermission] as v 
                            where v.EntityId = @entityId
                            )
                -- and user matches on the entitytype
                AND entityTypeCheck.isMatch = 1
                THEN 1
            -- 4 - GOD mode
            WHEN up.IsGod = 1
                THEN 2
            -- 5 - ENTITY OWNER
            WHEN EXISTS
            (
                select 1 from dbo.EntityOwner as v 
                    where 
                        (up.ThirdPartyProviderB1 = 0 AND up.ThirdPartyProviderB2 = 0)
                    AND v.EntityId = @entityId
                    AND v.EntityTypeId = @EntityTypeId
                    AND v.OwnerUserProfileId = @targetUserProfileId
                    AND v.IsDeleted = 0
            )   THEN 3
            -- 6 - USER SPECIFIC PERMISSION
            WHEN EXISTS (
                select 1 from perms.[UserProfileEntityViewPermission] as v 
                where v.EntityId = @entityId AND v.UserProfileId = @targetUserProfileId
                AND  forceCheck.isMatch = 1 
            )   THEN 4
            WHEN EXISTS (
                select 1 from perms.[UserProfileEntityViewPermission] as v 
                where v.EntityId = @entityId AND v.UserProfileId = @targetUserProfileId
                AND  pb.ForceB1 is null
                AND  pb.ForceB2 is null
            )   THEN 5
            -- 7 - ROLE/UNIT/TPP/EntityType check
            WHEN forceCheck.isMatch = 1 
                AND roleCheck.isMatch = 1 
                AND unitCheck.isMatch = 1
                AND tppCheck.isMatch = 1
                AND entityTypeCheck.isMatch = 1
                THEN 6
            ELSE
                -- CANT SEE
                0
            END
                AS visible
            from 
                --join 
                perms.UserProfileBitMask up

                left join [perms].[EntityTypeBit] etb
                    on etb.EntityTypeId = @entityTypeId

                left join perms.[EntityPermissionBitMask] pb
                    on pb.EntityId = @entityId AND pb.IsDeny = 0
                
                left join perms.[EntityVisibilityBitMask] vb
                    on vb.EntityId = @entityId
                
                left join perms.[EntityTypeVisibilityBitMask] tvb
                    on tvb.EntityTypeId = @entityTypeId

                left join perms.[EntityIsSensitiveMask] dcl
                    on dcl.EntityId = @entityId

                CROSS APPLY perms.Match2QWords(up.ForceB1, up.ForceB2, 
                                               pb.ForceB1, pb.ForceB2) forceCheck

                CROSS APPLY perms.Match6QWords(up.RoleB1, up.RoleB2, up.RoleB3, up.RoleB4, up.RoleB5, up.RoleB6,
                                               pb.RoleB1, pb.RoleB2, pb.RoleB3, pb.RoleB4, pb.RoleB5, pb.RoleB6) roleCheck

                CROSS APPLY perms.Match2QWords(up.UnitB1, up.UnitB2,
                                               pb.UnitB1, pb.UnitB2) unitCheck

                CROSS APPLY perms.Match2QWords(up.ThirdPartyProviderB1, up.ThirdPartyProviderB2,
                                               pb.ThirdPartyProviderB1, pb.ThirdPartyProviderB2) tppCheck

                -- entity type - we only do this check on view, not deny
                CROSS APPLY perms.Match3QWords(up.EntityTypeB1, up.EntityTypeB2, up.EntityTypeB3,
                                               etb.B1,		    etb.B2,			 etb.B3) entityTypeCheck

            where
                up.UserProfileId = @targetUserProfileId
            )  as q
            ORDER BY visible desc
    --) as x
--)